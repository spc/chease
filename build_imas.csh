#!/bin/tcsh -fe
# Copyright 2024 SPC-EPFL
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#----------------------------------------------------------------------

setenv IMAS_HOME_eff 'x'$IMAS_HOME'x'
if ($IMAS_HOME_eff != 'xx') then
  cd src-f90
  make clean
  make libchease_module_imas
  cd ..

  set extra_string='_choices'
  if ($XML_USE_CHOICE == 'NO') then
    set extra_string='_reflist'
  endif
  echo " "
  echo "Now execute: "
  echo "iwrap -f iwrap/chease$extra_string.yaml -i ~/public/PYTHON_ACTORS"
  echo " "
  echo "to test then:"
  echo "python python/run_chease_iwrap$extra_string.py > /tmp/$USER/o.python_chease$extra_string"
  echo " "

else
  echo "error IMAS environment not available. Cant use makefile option 'IMAS_HOME=yes'"
endif
