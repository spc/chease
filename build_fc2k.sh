# Copyright 2024 SPC-EPFL
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#----------------------------------------------------------------------
# Start from clean environment
module purge

host_at_iter=x`echo $HOSTNAME | grep -i iter`x
if [ $host_at_iter = 'xx' ]; then
# assume gateway (not iter) at this stage
module load cineca
module load imasenv
module switch matlab/2018b

else
# IMAS and FC2K
module load IMAS iWrap FC2K
# Libraries needed for the compilation of the H&CD codes themselves
# should avoid specific modules, defaults should follow and be consistent with generic IMAS, otherwise mke an issue
module load INTERPOS # expects /9.1.0-intel-2020b when tested
module load XMLlib # expects /3.3.1-intel-2020b

fi

# Intel as default compiler
export FCOMPILER=ifort

# Actor folder
export ACTOR_FOLDER=~/public/PYTHON_ACTORS
mkdir -p $ACTOR_FOLDER
# EXTEND PYTHON PATH AND AVOID DOUBLONS
export PYTHONPATH=$ACTOR_FOLDER/chease:$PYTHONPATH # FOR FC2K
export PYTHONPATH="$(perl -e 'print join(":", grep { not $seen{$_}++ } split(/:/, $ENV{PYTHONPATH}))')"

cd src-f90
#make clean
make libchease_kepler_imas
cd ..

echo " "
echo "Can execute: fc2k fc2k_imas/chease.xml -nokepler -pyworkspace ~/public/PYTHON_ACTORS"
echo " "
echo "to test then:"
echo "    export PYTHONPATH=~/public/PYTHON_ACTORS/chease:\$PYTHONPATH # already done"
echo "    mkdir -p /tmp/$USER; cd /tmp/$USER; python $PWD/python/run_chease_fc2k.py > /tmp/$USER/o.run_chease_fc2k.py"
echo " "
