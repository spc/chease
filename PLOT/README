C Copyright 2024 SPC-EPFL
C
C Licensed under the Apache License, Version 2.0 (the "License");
C you may not use this file except in compliance with the License.
C You may obtain a copy of the License at
C
C     http://www.apache.org/licenses/LICENSE-2.0
C
C Unless required by applicable law or agreed to in writing, software
C distributed under the License is distributed on an "AS IS" BASIS,
C WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
C See the License for the specific language governing permissions and
C limitations under the License.
C ----------------------------------------------------------------------


Plot routines for chease with UNIRAS and NCAR interfaces:
-----------------------------------------------------------
(for BNDFIT and PROFFIT plots, see creapict below)

files:

1) pchease.V12.f

2) pchease_uni.f

3) uni_to_ncar.f

4) uni_to_fort8.f

5) compile.pchease.ncar

6) compile.pchease.uni

7) compile.pchease.fort8

8) readpict.f

9) load.readpict

The file (1) is the standard pchease, except that one changes the name of the
subroutine statement for GRAPHE (to GRAPHE2 for example) and GRAPHEN. In this way
the routines in the interface files are used.


NCAR:
=====

compile pchease with the file (5), which uses the files 1, 2 and 3
(with this option, it will create the metafile gmeta which you can
process with ctrans, gplot or others)


UNIRAS:
=======

compile pchease with the file (6), which uses the files 1 and 2
compile readpict with the file (9)
(with this option, it will create a unipict file, which you can process with
the program readpict)

fort.8:
=======

compile pchease with the file (7), which uses the files 1, 2 and 4
(with this option, it will create an ascii file fort.8 which can be
used by creapict also)


C-----------------------------------------------------------------------
C-----------------------------------------------------------------------


Other files:
============

creapict.*.f:
------------

creates a gmeta or unipict file from an ascii file fort.13 which
has the same format as fort.8 above. It general use as follow:

cp fort.8 fort.13  (for example fort.8 is obtained from BNDFIT and PROFFIT codes)
creapict.exe  (=> gmeta or unipict to be used with ctrans or readpict)

compile.creapict.ncar:
---------------------

to compile creapict.*.f for NCAR

compile.creapict.uniras:
-----------------------

same but for UNIRAS
