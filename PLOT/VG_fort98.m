% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

% execute a.out from uni_to_fort8_pchease_uni_pchease.V12.f on NUPLO to extract fort.98
doload = 1;
if doload
  fid = fopen('fort.98','r');
  n_vals = fscanf(fid,'%d',7)
  i=0;
  i=i+1;NCHI = n_vals(i);
  i=i+1;NPSI1 = n_vals(i);
  i=i+1;NCURV = n_vals(i);
  i=i+1;INBCHI = n_vals(i);
  i=i+1;NMGAUS = n_vals(i);
  i=i+1;NT1 = n_vals(i);
  i=i+1;INSUR = n_vals(i);
  RRCURV = fscanf(fid,'%f',NCURV);
  RZCURV = fscanf(fid,'%f',NCURV);
  tmp = fscanf(fid,'%f',INBCHI*NPSI1);
  ii=0;
  for I=1:NPSI1
    for J=1:INBCHI
      ii=ii+1;
      ZRCHI(J,I) = tmp(ii);
    end
  end
  tmp = fscanf(fid,'%f',INBCHI*NPSI1);
  ii=0;
  for I=1:NPSI1
    for J=1:INBCHI
      ii=ii+1;
      ZZCHI(J,I) = tmp(ii);
    end
  end
  tmp = fscanf(fid,'%f',NCHI*NPSI1);
  ii=0;
  for I=1:NPSI1
    for J=1:INBCHI
      ii=ii+1;
      RSHEAR(J,I) = tmp(ii);
    end
  end
  tmp = fscanf(fid,'%f',NCHI*NPSI1);
  ii=0;
  for I=1:NPSI1
    for J=1:INBCHI
      ii=ii+1;
      CR(J,I) = tmp(ii);
    end
  end
  tmp = fscanf(fid,'%f',NCHI*NPSI1);
  ii=0;
  for I=1:NPSI1
    for J=1:INBCHI
      ii=ii+1;
      CZ(J,I) = tmp(ii);
    end
  end
  tmp = fscanf(fid,'%f',NMGAUS*NT1*NPSI1);
  ii=0;
  for I=1:NPSI1
    for J=1:NMGAUS*NT1
      ii=ii+1;
      RRISO(J,I) = tmp(ii);
    end
  end
  tmp = fscanf(fid,'%f',NMGAUS*NT1*NPSI1);
  ii=0;
  for I=1:NPSI1
    for J=1:NMGAUS*NT1
      ii=ii+1;
      RZISO(J,I) = tmp(ii);
    end
  end
  ZRSUR = fscanf(fid,'%f',INSUR);
  ZTSUR = fscanf(fid,'%f',INSUR);
  ZZSUR = fscanf(fid,'%f',INSUR);

  CRp=CR;CRp(end+1,:)=CRp(1,:);
  CZp=CZ;CZp(end+1,:)=CZp(1,:);
  RSHEARp=RSHEAR;RSHEARp(end+1,:)=RSHEARp(1,:);
end


figure;plot(ZRCHI',ZZCHI')
hold on;plot(RRCURV,RZCURV,'*')
plot(ZRSUR,ZZSUR,'k')
axis equal

figure;plot(CRp,CZp)
hold on;plot(RRCURV,RZCURV,'*')
plot(ZRSUR,ZZSUR,'k')
axis equal
%%
figure;contour(CRp,CZp,asinh(RSHEARp),100)
hold on;plotos(RRCURV,RZCURV,'*',[1 4]);
plot(ZRSUR,ZZSUR,'k')
axis equal
%%
ij=[RSHEARp<00];
figure;plotos(CRp(ij),CZp(ij),'o',[1 3],1);
hold on;plotos(RRCURV,RZCURV,'*',[1 4]);
plot(ZRSUR,ZZSUR,'k')
axis equal
