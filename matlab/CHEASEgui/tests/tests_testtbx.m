classdef tests_testtbx < matlab.unittest.TestCase
% a simple parametrized test with tags - modify at will
% see documentation of matlab.unittest suite for more details!

% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

  properties(TestParameter)
    shot = {61400}; % list of shots for which to run a test
  end

  methods(Test,TestTags = {'fast'})
    function my_fast_test(testCase,shot)
      a1=gdat(shot,'eqdsk');
      a2=read_eqdsk(a1.eqdsk.fnamefull,a1.eqdsk.cocos,1); % read eqdsk file
      testCase.assertTrue(ismatrix(a2.B));
    end
  end

  methods(Test,TestTags = {'slow'})
    function my_slow_test(testCase,shot)
      a1=gdat(shot,'eqdsk');
      [fname_out,globalsvalues,namelist_struct,namelistfile_eff] = run_chease(2,a1.eqdsk,a1.eqdsk.cocos);
      fname_out
      testCase.assertTrue(any(contains(fname_out,'.cols')) && any(contains(fname_out,'.RZcols')) && any(contains(fname_out,'NUPLO')));
      [hmode_distance,nuplo_data]=nuplo_2d_plots(fname_out,[],'0');
      testCase.assertTrue(isfield(hmode_distance,'Smax_0curv_top_half') &&abs(hmode_distance.Smax_0curv_top_half-0.171362336203151)<1e-4);
    end
  end
end
