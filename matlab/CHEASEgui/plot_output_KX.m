function plot_output_KX(plot_sel);
% -------------------------------------------------------------------
% Function to plot KINX output : displacement of flux surfaces
% -------------------------------------------------------------------
% SYNTAX:
%   plot_output_KX(plot_sel);
%
% INPUT:
%   plot_sel : 1 - normal displacement, 2 - displacement vectors,
%              3 - all
% -------------------------------------------------------------------

% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

% check input
% -------------------------------------------------------------------

if nargin<1,
  plot_sel=3;                                                   % plot normal and tangential displacement
end

co='co.bsp';                                                    % coordinates
fi='fi.bsp';                                                    % displacement projected
st='st.bsp';                                                    % inform. about stability
NLines=25;                                                      % nb. of level lines for contour plot
ScalQuiver=1.5;                                                 % scaling for quiver plot

% read coordinates
% -------------------------------------------------------------------

fid1=fopen(co,'r');
  nc = fscanf(fid1,'%d',[3 1]);                                  % read the header, ie. dimensions of data
  xyz = fscanf(fid1,'%g',[3 nc(3)*nc(2)*nc(1)]);                 % read the coordinates (r,z,?)
fclose(fid1);
fprintf('- KINX mesh: %d x %d\n', nc(2), nc(1)-1);


% read the real parts of physical projections of the displacement
% -------------------------------------------------------------------
%
% Definition of columns = projection of displacement to:
% 1 = grad(psi), perp. on magnetic surfaces
% 2 = B x grad(psi), 'poloidally'
% 3 = B, on magnetic field
% 4 = ?
% 5 = r-vector
% 6 = z-vector

fid2=fopen(fi,'r');
  nn = fscanf(fid2,'%d',[2 1]);                                 % header, ie. length and nb. of columns
  fff = fscanf(fid2,'%g',[nn(2) nn(1)]);                        % read data
fclose(fid2);


% read informations about stability analysis
% -------------------------------------------------------------------

fid3=fopen(st,'r');
  nw = str2num(fgetl(fid3));                                    % toroidal wave nb. (1)
  alnorm = str2num(fgetl(fid3));                                % eigenvalue of mode, norm. to Alfven freq. (w^2/w_A^2) (2)

  % --- negatives and initial guess ---
  snegal0 = fgetl(fid3); negal0 = str2num(snegal0);             % nb. of negatives (3)
  neg = negal0(1);
  if(size(negal0,2)==2)
    al0 = negal0(2);
  elseif(size(negal0,2)==1)                                     % only one instable mode
    al0 = str2num(fgetl(fid3));                                 % initial guess of eigenvalue (4)
  end
  idw =str2num(fgetl(fid3));                                    % index for energy norm (kinetic en.=0 or delta W<0)
  it2 = str2num(fgetl(fid3));                                   % iteration count
fclose(fid3);


% find termination status of KINX
% -------------------------------------------------------------------

if( (neg==0) | ((neg==1)&(al0>=alnorm)) )                       % lowest eigenvalue found
  status = 'lowest eigenvalue';
else
  status = [int2str(neg) ' eigenvalues less than ' num2str(al0) ];
end

% --- kinetic norm index ---
if( idw==0 )
  norm = 'kinetic energy norm';
else
  norm = '\delta W norm';
end

% --- check status of KINX termination ---
if( (alnorm<0) )                                                % w^2/w_A^2<0 = unstable
  stable = ' unstable eq.';
elseif ( (al0<0) & (neg>0) )
  stable = 'unstable eq.';
elseif( (alnorm>0) & ((neg==0) | ((neg==1)&(al0>=alnorm))) )    % pos. omega & lowest eigenvalue (smaller than guess)
  stable = 'stable eq.';
  if(it2(1)==it2(2))
    stable = 'eigenvalue accuracy? change initial guess';
  end
else                                                            % unknown status
  stable = 'stability of eq. unknown';
end
fprintf('- KINX status: %s\n',status);


% build coordinates for grid mesh
% -------------------------------------------------------------------

ij=1;
for j=1:nc(2),
  for i=1:nc(1),
     x(i,j)=xyz(1,ij);                                          % get r-coord.
     y(i,j)=xyz(2,ij);                                          % get z-coord.
     ij=ij+1;
  end;
end;


% plot displacement level lines normal to grad(psi)
% -------------------------------------------------------------------
%
% evg0 format (6 components) assumed

figure;
cmt='';                                                         % init. plot name

if ismember(plot_sel,[1,3]);
  if (plot_sel==3), subplot(121); end
  %tcvview('v');
  for ii=1:1,
    if(ii==1)
      set(gca,'box','on');
      cmt=' (REAL)';
    end;
    if(ii==2)
      set(gca,'box','on');
      cmt=' (IMAG)';
    end;
    colormap('jet');
    ij=1;
    for j=1:nc(2),
      for i=1:nc(1),
        f(i,j)=fff(ii,ij);
        ij=ij+1;
      end;
    end;
    view(2);                                                      % default 2D-view
    hold on;
    axis('image');
    contour(x,y,f,NLines);                                        % plot contour lines of displacement levels
%    colorbar;
    plot(x(:,nc(2)),y(:,nc(2)),'k');                              % plot plasma boundary
    hold off;
  end
  title(['Normal displacement',cmt,' of n=' int2str(nw) ' mode, mesh ' int2str(nc(2)) '\times' int2str(nc(1)-1)]);
  xlabel(['\omega^2/\omega_A^2=' num2str(alnorm) '; ' norm  '; ' stable '; '  status]);
  axis('image');
end


% plot displacement projected on (r,z)
% -------------------------------------------------------------------

if ismember(plot_sel,[2,3]),

  if (plot_sel==3),subplot(122); end
  for jj=5:5;

  % for the plot
    if(jj==1)
  %   subplot(1,2,1);
      set(gca,'box','on');
      title('Displacement REAL');
    end;
    if(jj==2)
  %   subplot(1,2,2);
      set(gca,'box','on');
      title('Normal displacement level lines IMAG');
    end;
    colormap('jet');
    ij=1;
    for j=1:nc(2),
      for i=1:nc(1),
        fx(i,j)=fff(jj,ij);
        ij=ij+1;
      end;
    end;
    ij=1;
    for j=1:nc(2),
      for i=1:nc(1),
        fy(i,j)=fff(jj+1,ij);
        ij=ij+1;
      end;
    end;
    view(2);                                                        % default 2-D view
    hold on;
    quiver(x,y,fx,fy,ScalQuiver);                                   % quiver plot
    plot(x(:,nc(2)),y(:,nc(2)),'k');                                % boundary
    axis('image');
    hold off;
  end;
  title(['Displacement of n=' int2str(nw) ' mode, mesh ' int2str(nc(2)) '\times' int2str(nc(1)-1)]);
  xlabel(['\omega^2/\omega_A^2=' num2str(alnorm) '; ' norm  '; ' stable '; '  status]);
  axis('image');
end

return
