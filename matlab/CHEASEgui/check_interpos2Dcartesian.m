% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

xin=linspace(0,1,10)';
yin=linspace(2,3,12);

fin=xin*yin.^2+0.3.*xin.^3*yin;

xout=linspace(-0.5,1.5,1000)';
yout=linspace(1.5,3.5,100);
xout=linspace(0,1,5)';
yout=linspace(2,3,7);

% to check easily use same out as in
xout=xin;
yout=yin;

xout2d=repmat(xout,1,length(yout));
yout2d=repmat(yout',1,length(xout))';

tension_x = -0.;
tension_y = -0;
tension_x = -0.01;
tension_y = -0.01;
kextrapol = 3;
nbc_x = [2 2];
nbc_y = [2 2];

[farray_out]=interpos2Dcartesian(xin,yin,fin,xout2d,yout2d,tension_x,tension_y);

figure
plot(xin,fin)
hold on
plot(xout,farray_out,'k--')

figure
plot(yin,fin)
hold on
plot(yout,farray_out,'k--')

[farray_out]=interpos2Dcartesian(xin,yin,fin,xout2d,yout2d,tension_x,tension_y,kextrapol,nbc_x,nbc_y);

figure
plot(xin,fin)
hold on
plot(xout,farray_out,'k--')

figure
plot(yin,fin)
hold on
plot(yout,farray_out,'k--')

%%
nbc_x=[1 2];
sigma_in=[1e3;length(xin)-1];
sigma_in=[1e3;1e3;1e3;ones(length(xin)-3,1)];
[farray_out]=interpos2Dcartesian(xin,yin,fin,xout2d,yout2d,tension_x,tension_y,kextrapol,nbc_x,nbc_y,sigma_in);

figure
plot(xin,fin)
hold on
plot(xout,farray_out,'k--')

figure
plot(yin,fin)
hold on
plot(yout,farray_out,'k--')
