function [fname_out,globalsvalues,namelist_struct,namelistfile_eff] = run_chease(namelistfile,inputfile,cocos_in,varargin)
%
% [fname_out,globalsvalues,namelist_struct,namelistfile_eff] = run_chease(namelistfile,inputfile,cocos_in,varargin)
% [fname_out,globalsvalues,namelist_struct,namelistfile_eff] = run_chease(namelistfile,inputfile,cocos_in,{exptnzfile,suffix,chease_exe,nverbose,NIN_file})
%
% [~,~,namelist_struct] = run_chease; % returns the default namelist structure (EXPEQ)
% [~,~,namelist_struct] = run_chease(ii); % returns the default namelist structure for case 1, 2 or 0
%
% namelistfile: full path to CHEASE namelist (1 (default): if want default expeq namelist, 2 for default 'eqdsk' namelist, 0 for an example with boundary and profiles given in namelist only)
% inputfile   : full path to input file (EXPEQ/EQDSK file) or equivalent structure if namelistfile=1 or 2
% cocos_in    : cocos of input eqdsk (2 by default, not needed if EXPEQ file given)
% exptnzfile  : (optional) full path to EXPTNZ file
% suffix      : (optional) suffix to written files
% chease_exe  : (optional) chease executable name (default=chease) which should be in your path, set chease_develop to have develop version, chease_lac5, etc
% nverbose    : (optional) verbosity level
% NIN_file    : (optional) full path to CHEASE 'restart' file
%
% fname_out   : cell containing full path of output files
% globalsvalues: structure containing main global results extracted from output: q0, qedge, q95, betaN, betap, li, kappa, delta, etc
% namelist_structure: variables and values in a matlab structure (from read_namelist)
% namelistfile_eff : namelist filename used for running chease
%
% Examples

% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

fname_out='';
globalsvalues=[];

if ~exist('cocos_in','var') || isempty(cocos_in)
  if exist('namelistfile','var') && isstruct(namelistfile) && isfield(namelistfile,'cocos_in')
    cocos_in = namelistfile.cocos_in;
  else
    cocos_in=2;
  end
end

if ~exist('namelistfile','var') || isempty(namelistfile)
    [nl,namelistfile_eff] = write_namelist_chease([],1,cocos_in);
elseif isnumeric(namelistfile) || isstruct(namelistfile)
  [nl,namelistfile_eff] = write_namelist_chease([],namelistfile,cocos_in);
else
    namelistfile_eff = namelistfile;
    nl = read_namelist_chease(namelistfile_eff);
end
namelist_struct = nl;

if nargin == 0
    return
end
if nargin ==1 && ~isempty(namelistfile) && isnumeric(namelistfile) && namelistfile~=0
    % needs inputfile except for namelistfile=0 option
    return
end

if ~exist('inputfile','var') || isempty(inputfile)
    if exist('namelistfile','var') && isnumeric(namelistfile) && namelistfile==0
        % no need of input files
        inputfile_eff = [];
    else
        % if nverbose >= 1,
        disp('inputfile not provided');
        % end
        return
    end
else
    inputfile_eff = inputfile;
end

% set up tmpdir, create if necessary
hash = ['chease','_',datestr(now,'yyyymmddHHMMSSFFF'),'_',char(97+floor(26*rand(1,5)))];
[s,uname] = unix('whoami < /dev/null');
if s
  disp(uname);
  warning('error getting username');
  return
end
tmpdir = fullfile(tempdir,deblank(uname),hash);
if ~exist(tmpdir,'dir'), mkdir(tmpdir); end

if isstruct(inputfile)
  if (isnumeric(namelistfile) && namelistfile==1) || (isstruct(namelistfile) && namelistfile.neqdsk==0) || (isstruct(inputfile) && isfield(inputfile,'pressure'))
    tmp_name = 'EXPEQ_fromstruct';
    if isfield(inputfile,'fname') && ~isempty(inputfile.fname'), tmp_name = [tmp_name '_' inputfile.fname]; end
    [EXPEQdataout,fclose_out] =  write_expeq(inputfile_eff,fullfile(tmpdir,tmp_name));
    inputfile_eff = EXPEQdataout.fnamefull;
  elseif (isnumeric(namelistfile) && namelistfile==2) || (isstruct(namelistfile) && namelistfile.neqdsk==1) || (isstruct(inputfile) && isfield(inputfile,'F'))
    tmp_name = 'EQDSK_fromstruct';
    if isfield(inputfile,'fname') && ~isempty(inputfile.fname'), tmp_name = [tmp_name '_' inputfile.fname]; end
    [eqdsk_cocosout] = write_eqdsk(fullfile(tmpdir,tmp_name),inputfile_eff);
    inputfile_eff = eqdsk_cocosout.fnamefull;
  end
end

if nargin>=4 && ~isempty(varargin{1});
    exptnzfile=varargin{1};
    usetnz = 1;
    if ~exist(exptnzfile,'file')
      warning([exptnzfile,' does not exist']);
      return
    end
else
    usetnz = 0;
    tnzignored = 0;
end

if nargin>=5  && ~isempty(varargin{2}) % file suffix manually set!
    suffix = varargin{2};
end

chease_exe = 'chease';
if nargin>=6  && ~isempty(varargin{3})
    chease_exe = varargin{3};
end

nverbose = 0;
if nargin>=7 && ~isempty(varargin{4})
    nverbose = varargin{4};
end

NIN_file = [];
if nargin>=8 && ~isempty(varargin{5})
    NIN_file = varargin{5};
end

if ~exist(namelistfile_eff,'file')
  warning([namelistfile_eff,' does not exist']);
  return
end

if ~isempty(inputfile_eff) && ischar(inputfile_eff) && ~exist(inputfile_eff,'file')
  warning([inputfile_eff,' does not exist']);
  return
end
if ~isempty(NIN_file) && ~exist(NIN_file,'file')
  warning([NIN_file,' does not exist']);
  return
end

if ~isempty(inputfile_eff)
    [astat,aresult] = copyfile(inputfile_eff,fullfile(tmpdir,'EXPEQ'),'f'); %copy the expeq of actual equilibrium to EXPEQ(file which chease uses)
    if ~astat; disp(aresult); warning('error copying EXPEQ'); return; end
    [bstat,bresult] = copyfile(namelistfile_eff,fullfile(tmpdir,'chease_namelist'),'f'); %copy the namelist
    if ~bstat; disp(bresult); warning('error copying namelist'); return; end
end
if usetnz
    [cstat,cresult] = copyfile(exptnzfile,fullfile(tmpdir,'EXPTNZ'),'f'); %copy the exptnz file to the same dir
    if ~cstat; disp(cresult); warning('error copying EXPTNZ'); return; end
    % CHECK that the namelist has the right option for EXPTNZ
    [s,w] = unix(['grep NBSEXP ',namelistfile_eff,' < /dev/null']);
    if s, disp(w);warning('error grepping namelist'); return; end
    eval(strrep(w,',',';')); % sets value of NBSEXP. replace commas with ; to silence
    if ~exist('NBSEXPQ','var'),
        % Maybe this error comes from the standard input being fed to unix commands by default (see Tips section in unix command's help).
        % Let's see if this happens again with the addition of a /dev/null feed.
        warning('NBSEXPQ was not set. Something is wrong. Probably grep found no match. But why ? let''s find out ...');
        keyboard;
    elseif NBSEXPQ ~= 1111,
        if nverbose >= 1,
            warning('EXPTNZ file specified but NBSEXPQ is not 1111 in namelist! TNZ data will not be taken into account');
        end
        tnzignored = 1;
    end
end
if ~isempty(NIN_file)
    [astat,aresult] = copyfile(NIN_file,fullfile(tmpdir,'NIN'),'f'); %copy the expeq of actual equilibrium to EXPEQ(file which chease uses)
    if ~astat; disp(aresult); warning('error copying NIN'); return; end
end

% determine output file name (same as expeq but remove expeq)
if ~isempty(inputfile_eff)
    fname_expeq = dir(inputfile_eff);
    out_suffix = fname_expeq.name(strfind(lower(fname_expeq.name),'expeq')+length('expeq'):end);
    if isempty(out_suffix); out_suffix = fname_expeq.name; end % if nonstandard name
else
    out_suffix = 'stdrun';
end
if strcmp(out_suffix(1),'.') || strcmp(out_suffix(1),'_');
    out_suffix = out_suffix(2:end);
end
fname_out = fullfile(tmpdir,['o.chease.',out_suffix]);
ofname_out = fname_out;

% copy namelist
[cstat,cresult] = copyfile(fullfile(tmpdir,'chease_namelist'),fname_out,'f');
if ~cstat; disp(cresult); warning('error copying namelist'); return;  end

% RUN CHEASE
if nverbose >= 3,
    fprintf(['\nRunning ' chease_exe ' ...\n'])
    disp(['  namelist: ',namelistfile_eff])
    disp(['     EXPEQ: ',inputfile_eff])
    if usetnz
        disp(['    EXPTNZ: ',exptnzfile])
    elseif tnzignored
        disp(['    EXPTNZ: ',exptnzfile,' *** IGNORED ***'])
    else
        disp('    EXPTNZ: *** none ***')
    end
    disp(['    Output: ',fname_out]);
end

chease_launchtime = now;

olddir = pwd; cd(tmpdir);
%[dstat,dresult] = unix(['/home/sauter/bin/chease < ',namelistfile_eff,'>> ',fname_out]);
% chease should be in path, if it is not, assume in /home/sauter/bin

use_ld_library_path_current = 0;
use_ld_library_path_previous = 0;
if use_ld_library_path_current
  ld_library_path_current = getenv('LD_LIBRARY_PATH');
end
if use_ld_library_path_previous
  if exist('~/.ld_library_path_previous','file')
    [aaa1,aaa2]=unix('cat ~/.ld_library_path_previous');
    if double(aaa2(end))==10; aaa2 = aaa2(1:end-1); end
    setenv('LD_LIBRARY_PATH',aaa2);
    ld_library_path_previous = getenv('LD_LIBRARY_PATH');
  end
end
try
  [a,~]=unix(['which ' chease_exe]);
  if a==0
    [dstat,dresult] = unix([chease_exe ' >> ',fname_out]);
  else
    warning(['no chease in $PATH, you should add it, now tries some default paths'])

    if exist(['/home/codes/bin/' chease_exe],'file')
      [dstat,dresult] = unix(['/home/codes/bin/' chease_exe ' >> ' fname_out]);
      warning(['using /home/codes/bin/' chease_exe]);
    elseif exist(['/home/sauter/bin/' chease_exe],'file')
      [dstat,dresult] = unix(['/home/sauter/bin/' chease_exe ' >> ' fname_out]);
      warning(['using /home/sauter/bin/' chease_exe]);
    elseif exist(['/home/osauter/bin/' chease_exe],'file')
      [dstat,dresult] = unix(['/home/osauter/bin/chease >> ',fname_out]);
      warning(['using /home/osauter/bin/' chease_exe]);
    elseif exist(['/home/sautero/bin/' chease_exe],'file')
      [dstat,dresult] = unix(['/home/sautero/bin/chease >> ',fname_out]);
      warning(['using /home/sautero/bin/' chease_exe]);
    else
      error('Error no chease executable was found');
    end
  end
catch ME
  rethrow(ME);
  return
end

if ~isempty(dresult)
  fid=fopen(fname_out,'a');
  fprintf(fid,'stderr:\n\n%s\n',dresult);
  fclose(fid);
end

if use_ld_library_path_current
  setenv('LD_LIBRARY_PATH',ld_library_path_current);
end
if use_ld_library_path_previous
  setenv('LD_LIBRARY_PATH',ld_library_path_previous);
end

if exist('./fort.6','file'); unix(['cat fort.6 >> ' fname_out]); end
if exist('./fort.0','file'); unix(['cat fort.0 >> ' fname_out]); end

cd(olddir);
% append result to o.xxx file
% check that all went well, else display error
if dstat
  disp(dresult);
  warning(['error running ' chease_exe]);
  return
end

%transform the output file of chease in something readable by Matlab
if nverbose >= 3,
    fprintf('done....\n Transforming CHEASE output file in columns.... ')
end
[a,~]=unix('which o.chease_to_cols');
if a==0
    [fstat,fresult]=unix(['o.chease_to_cols ''',fname_out,''' ''',fname_out,'.cols''']);
elseif exist('/home/sauter/bin/o.chease_to_cols','file')
    [fstat,fresult]=unix(['/home/sauter/bin/o.chease_to_cols ''',fname_out,''' ''',fname_out,'.cols''']);
elseif exist('/home/osauter/bin/o.chease_to_cols','file')
    [fstat,fresult]=unix(['/home/osauter/bin/o.chease_to_cols ''',fname_out,''' ''',fname_out,'.cols''']);
else
  warning('Error no o.chease_to_cols executable was found');
  return
end
if fstat;
  disp(fresult);
  warning('error converting to cols');
  return
end
if nverbose >= 3,
    fprintf(['..done\n cols file: ' fname_out '.cols\n'])
end

if ~any([~cstat,dstat,fstat])
    % any files created after chease was launched are also outputs
    filestmpdir = dir(tmpdir);
    % save if created after chease launched and if a file (not dir)
    filestmpdir = filestmpdir(~cellfun(@isempty,{filestmpdir(:).datenum}));
    mask =  ([filestmpdir.datenum] > chease_launchtime) & ~[filestmpdir.isdir];
    fname_out = cellfun(@(x) fullfile(tmpdir,x),{filestmpdir(mask).name},'UniformOutput',false).';
else
    if ~cstat; disp(cresult); end
    if  dstat; disp(dresult); end
    if  fstat; disp(fresult); end
    warning(['Error running ' chease_exe ' or converting outputs to cols'])
    return
end

if ~exist('suffix','var') || isempty(suffix)
    if ~isempty(inputfile_eff)
        % extract an 'appropriate' suffix
        suffix = inputfile_eff(strfind(lower(inputfile_eff),'expeq')+length('expeq'):end);
    else
        suffix = '';
    end
end
if ~isempty(fname_out)
    fname_out_copied = cell(size(fname_out));
    for tel=1:length(fname_out)
        % copy output files with suffix
        if isempty(strfind(fname_out{tel},'o.chease'))
            fname_out_copied{tel} = [fname_out{tel},suffix];
            if ~strcmp(fname_out_copied{tel},fname_out{tel})
                copyfile(fname_out{tel},fname_out_copied{tel},'f');
                if nverbose >= 3, disp([fname_out{tel},' -> ',fname_out_copied{tel}]);end
            end
        else
            fname_out_copied{tel} = fname_out{tel};
            % o.chease* contain the suffix already from the start
        end
    end
    fname_out = fname_out_copied; % transpose for nicer visualization
else
    disp('isempty(fname_out)');
    [a,b]=unix(['ls -1 ' fullfile(tmpdir,'o.*[^l][^s]')]);
    unix(['tail -20 ' b]);
    disp(['from:  tail -20 ' b]);;disp(fname_out);
    return
end

% find an EQDSK OUT
eqdsk = [];
for j=1:length(fname_out)
  jj = strfind('EQDSK_COCOS_',fname_out{j});
  if ~isempty(jj)
    jjdot = strfind('.OUT',fname_out{j});
    cocos_out = str2double(fname_out{j}(jj+12:jjdot-1));
    eqdsk = read_eqdsk(fname_out{j},cocos_out,0,[],[],nverbose);
    break
  end
end
if isempty(eqdsk)
  try
    globalsvalues = extractdatachease(ofname_out,[ofname_out '.cols']);
  catch
    globalsvalues=struct([]);
  end
else
  try
    globalsvalues = extractdatachease(ofname_out,[ofname_out '.cols'], ...
          [reshape(eqdsk.rplas,length(eqdsk.rplas),1),reshape(eqdsk.zplas,length(eqdsk.zplas),1)]);
  catch
    globalsvalues=struct([]);
  end
end
