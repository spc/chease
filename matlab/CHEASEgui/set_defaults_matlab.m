% Sets default for some of:
% get(0,'Factory')
%

% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

set(0,'DefaultaxesFontName','a14');
set(0,'DefaulttextFontName','a14');
set(0,'DefaultUicontrolFontName','a14')
set(0,'FixedWidthFontName','a14')

set(0,'DefaultlineLineWidth',2);
set(0,'DefaultaxesFontSize',12);
set(0,'DefaultaxesFontWeight','bold');
set(0,'DefaulttextFontSize',12);
set(0,'DefaulttextFontWeight','bold');
set(0,'DefaultlineMarkerSize',7);
set(0,'DefaultsurfaceMarkerSize',7);
set(0,'DefaultaxesXGrid','on');
set(0,'DefaultaxesYGrid','on');
% set(0,'DefaultsurfaceMarker','auto');
set(0,'DefaultsurfaceMarkerFaceColor','auto');

%
% col_1 = [0.9 0.1 0.3];
col_1 = [0 0 1]; % blue
col_2 = [1. 0. 0.]; % red
col_3 = [0. 0.4 0.]; % dark green
col_4 = [0.7 0.4 0.9]; % purple
col_5 = [0. 0.45 0.9]; % dark cyan: blue green
col_6 = [1.0 0.1 0.7]; % reddish magenta
col_7 = [0.0 0.9 0.6]; % light cyan, light green-blue
col_8= [0.8 0.1 0.95]; % dark magenta
col_9= [1. 0.7 0]; % dark yellow
col_10 = [0. 0. 0.]; % black
col_11 = [0. 0.85 0.]; % light green
col_12= [0.2 0.95 0.7]; % cyan
col_12= [0. 0.8 0.8]; % cyan
col_13= [1 0.85 0.3]; % yellow
% col_13= [0.933 0.68 0.05];
col_14= [0.5 0.5 0.5]; % gray
% using uisetcolor
typemk={'*', 'o', 's', 'd', 'p', '^', 'v'};
lnstyle={'-', '--', '-.', ':'};
colos=[col_1; col_2; col_3; col_4; col_5; col_6; col_7; col_8; col_9; col_10; col_11; col_12; col_13; col_14];

set(0,'DefaultAxesColorOrder',colos);
