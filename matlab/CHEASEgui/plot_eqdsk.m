function [eqdskout,varargout]=plot_eqdsk(varargin)
%
% [eqdskout,eqdsk257]=plot_eqdsk(varargin)
%
% New: if eqdskin is array of structure, then compare plasma boundaries only according to plasma_bnd_compare option
%
% Script to plot an eqdsk.
%
%  By defaults, plots the profiles (p, pprime, T, TTprime and q) and the psi(R,Z) contours
%
%  varargin{1}: eqdskin
%               if it is an eqdsk structure, plots the various values
%               if it is a name, assumes it is an eqdsk filename and executes first:
%                   eqdskout=read_eqdsk(eqdskin,cocos_in);
%               if array of structure, compare eqdskin(i).rplas/.zplas according to plasma_bnd_compare
%
%  varargin{2}: ifig: Starting number for figures (default: -30))
%               if ifig<0, starts from abs(ifig) and overlays plots (hold all before plotting)
%               ifig=0: open new figures
%
%  varargin{3}: 0 (default): do not check Grad-Shfranov
%               1 : Checks Grad-Shafranov and returns interpolated eqdsk on 257x257 mesh (=eqdsk257)
%
%  varargin{4}: 0 (default) plot all figures
%               1 plot only contours of left- and right-hand side of Grad-Shafranov equation
%
%  varargin{5}: resonances to plot as an array of frequencies and n*Zi/Ai, n harmonic,
%               if Zi/Ai is negative multiplies by  -eB/me and if positive multiplies by eB/mp thus EC or IC
%               thus [82.7e9 -2; 50e6 1/2] gives omega=82.7GHz for EC X2 (TCV) and 50MHz 1st harmonic D
%               If varargin{5} provided plots only psi(R,Z) contours and flux surface and resonances
%
%  varargin{6}: plasma_bnd_compare (if eqdskin(i) is an array of structures with .rplas, .zplas for plasma boundaries
%               if 0, default, rescale to match within TCV r_hfs=0.624, r_lfs=1.1376 (TCV target value to fit in vessel)
%               if 1, compare rescaling with min(rgeom)
%               if 2, compare rescaling with max(rgeom) and matching a(max(rgeom)) (changing aspect ratio)
%               related z-shift (so above cases but +0, +10 or +20):
%               if [0-9] same zaxis=0, [10-19] Zmin same (if SND e.g.), [20-29] Zmax same
%
%
% Examples:
%
% eq1=plot_eqdsk('EQDSK_A'); eq2=plot_eqdsk('EQDSK_B'); % figures 30 to 34 will have both eqdsks
%
% eq=plot_eqdsk; % will open window to choose an eqdsk file
%
% [eq1,eq257]=plot_eqdsk('EQDSKfilename',[],1);
%
% see also read_eqdsk, write_eqdsk, eqdsk_transform
%

% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

set_defaults_matlab

if nargin == 0 || isempty(varargin{1});
  eqdskin = read_eqdsk;
  fnamefull = eqdskin.fnamefull;
  if ~exist(fnamefull); return; end
  eqdskin.extralines{end+1}=['From file: ' fnamefull];
elseif ~isstruct(varargin{1})
  % eqdsk filename is known, read eqdsk
  fnamefull = varargin{1};
  if ~exist(fnamefull,'file'); disp('need a valid file name'); return; end
  aa = find_cocos(fnamefull);
  cocos_in = aa(1);
  eqdskin = read_eqdsk(fnamefull,cocos_in);
  eqdskin.extralines{end+1}=['From file: ' fnamefull];
else
  % eqdsk structure is given
  eqdskin = varargin{1};
  cocos_in = eqdskin(1).cocos;
end

eqdskout=eqdskin;

ifiggiven=0;
if nargin >= 2 && ~isempty(varargin{2}); ifiggiven = varargin{2};  end

icall_eqtransform=0;
if nargin >= 3 && ~isempty(varargin{3}); icall_eqtransform = varargin{3};  end

iplot_only_gs = 0;
if nargin >= 4 && ~isempty(varargin{4}); iplot_only_gs = varargin{4};  end

iplot_resonances = [82.7e9 -2; 118e9 -3]; % default 2nd and 3rd harmonic EC from TCV
if abs(eqdskin(1).b0) > 1.6
  iplot_resonances = [140e9 -2; 140e9 -3]; % assume ECRH 2 AUG
  iplot_resonances = [105e9 -2; 105e9 -3; 140e9 -2; 140e9 -3]; % assume ECRH 2 AUG
end
doplot_only_Bres = 0;
if nargin >= 5 && ~isempty(varargin{5});
  iplot_resonances = varargin{5};
  doplot_only_Bres = 1;
end

plasma_bnd_compare = 0;
if nargin >= 6 && ~isempty(varargin{6}); plasma_bnd_compare = varargin{6};  end

if nargin >= 7; error('wrong number of inputs'); return; end

% if array of structure, do comparison and return
if numel(eqdskin) > 1
  for i=1:numel(eqdskin)
    r_geom(i) = (min(eqdskin(i).rplas)+max(eqdskin(i).rplas))/2;
    r_minor(i) = (max(eqdskin(i).rplas)-min(eqdskin(i).rplas))/2;
    rlfs(i) = min(eqdskin(i).rplas);
    rhfs(i) = min(eqdskin(i).rplas);
    raxis(i) = eqdskin(i).raxis;
    zaxis(i) = eqdskin(i).zaxis;
    [zz izz]=min(eqdskin(i).zplas);
    r_zmin(i) = eqdskin(i).rplas(izz);
    [zz izz]=max(eqdskin(i).zplas);
    r_zmax(i) = eqdskin(i).rplas(izz);
  end
  delta_r_gap_hfs = 0.019;
  delta_r_gap_lfs = 0.024;
  rhfs_min_tcv = 0.624 + delta_r_gap_hfs;
  rlfs_max_tcv = 1.1376 - delta_r_gap_lfs;
  a_tcv_max = (rlfs_max_tcv-rhfs_min_tcv)/2;
  r0_tcv = (rhfs_min_tcv+rlfs_max_tcv)/2.;
  [rgeom_min irgeom_min] = min(r_geom);
  [r_minor_min ir_minor_min] = min(r_minor);
  [inv_aspect_ratio_min i_inv_aspect_ratio_min] = min(r_minor./r_geom);
  % aim for zaxis=0 first, shift later if needed
  switch mod(plasma_bnd_compare,10)
    case 0
      % match r0_tcv and fit in TCV vessel
      r_factor = (a_tcv_max./r0_tcv) ./ (r_minor./r_geom) .* (r0_tcv./r_geom);
      r0_target = r0_tcv;
    case 1
      % match min(r0)
      [r0_target,i_r0_target] = min(r_geom);
      a_target = r_minor(i_r0_target);
      r_factor = r0_target ./ r_geom;
    case 2
      % match max(r0) and its r_minor
      [r0_target,i_r0_target] = max(r_geom);
      a_target = r_minor(i_r0_target);
      r_factor = (a_target./r0_target) ./ (r_minor./r_geom) .* (r0_target./r_geom);
    otherwise
      warning(['case plasma_bnd_compare = ' num2str(plasma_bnd_compare) ' not yet implemented']);
      return
  end
  for i=1:numel(eqdskin)
    eqdskout(i).r_factor = r_factor(i);
    eqdskout(i).rplas_new = r0_target + (eqdskin(i).rplas - r_geom(i)).*r_factor(i);
    eqdskout(i).zplas_new = 0.+(eqdskin(i).zplas-eqdskin(i).zaxis).*r_factor(i);
    eqdskout(i).zaxis_new = 0.;
    zmin_new(i) = min(eqdskout(i).zplas_new);
    zmax_new(i) = max(eqdskout(i).zplas_new);
  end
  z_target = 0;
  delta_z = 0.;
  if floor(plasma_bnd_compare/10) == 0
    % keep z_axis = 0
    z_target = zeros(size(eqdskout));
    delta_z = zeros(size(eqdskout));
  elseif floor(plasma_bnd_compare/10) == 1
    z_target = min(zmin_new);
    delta_z = z_target - zmin_new;
  elseif floor(plasma_bnd_compare/10) == 2
    z_target = max(zmax_new);
    delta_z = z_target - zmax_new;
  else
    error('option not impemented yet, ask O. Sauter');
  end
  figure
  for i=1:numel(eqdskin)
    eqdskout(i).zaxis_new = delta_z(i);
    eqdskout(i).zplas_new = delta_z(i) + eqdskout(i).zplas_new;
    plot(eqdskout(i).rplas_new,eqdskout(i).zplas_new);
    hold on
  end
  legend(num2str([1:numel(eqdskin)]'))
  axis equal
  return
end


%%%%%%%%%%%%%%%%%%%%%%%%
% plot pressure and pprime
%%%%%%%%%%%%%%%%%%%%%%%%
if ~iplot_only_gs && ~doplot_only_Bres
  if ifiggiven==0
    ifig_eff=figure;
  else
    ifig_eff=figure(abs(ifiggiven));
  end
  if isnumeric(ifig_eff)
    ifig = ifig_eff;
  else
    ifig = ifig_eff.Number;
  end
  icol=1;
  subplot(2,1,1)
  if ifiggiven<0
    hold all
    icol=length(get(gca,'child'))+1;
  end
  plotos(eqdskin.psimesh,eqdskin.p,'-',[],[],colos(icol,:));
  ylabel('p')
  xlabel('psi')

  subplot(2,1,2)
  plotos(eqdskin.psimesh,eqdskin.pprime,'-',[],[],colos(icol,:));
  xlabel('psi')
  ylabel('pprime')

  %%%%%%%%%%%%%%%%%%%%%%%%
  % plot F and FFprime
  %%%%%%%%%%%%%%%%%%%%%%%%
  ifig=ifig+1;
  figure(ifig)
  if ifiggiven==0; clf; end
  subplot(2,1,1)
  if ifiggiven<0
    hold all
  end
  plotos(eqdskin.psimesh,eqdskin.F,'-',[],[],colos(icol,:));
  ylabel('F')
  xlabel('psi')

  subplot(2,1,2)
  plotos(eqdskin.psimesh,eqdskin.FFprime,'-',[],[],colos(icol,:));
  xlabel('psi')
  ylabel('FFprime')

  %%%%%%%%%%%%%%%%%%%%%%%%
  % plot q
  %%%%%%%%%%%%%%%%%%%%%%%%
  ifig=ifig+1;
  figure(ifig)
  if ifiggiven==0; clf; end
  subplot(2,1,1)
  if ifiggiven<0
    hold all
  end
  plotos(eqdskin.psimesh,eqdskin.q,'-',[],[],colos(icol,:));
  ylabel('q')
  xlabel('psi')

  subplot(2,1,2)
  plotos(eqdskin.rhopsi,eqdskin.q,'-',[],[],colos(icol,:));
  xlabel('rho')
  ylabel('q')

  %%%%%%%%%%%%%%%%%%%%%%%%
  % plot just plasma boundary
  %%%%%%%%%%%%%%%%%%%%%%%%
  ifig=ifig+1;
  figure(ifig)
  if ifiggiven==0; clf; end
  if ifiggiven<0
    hold all
  end
  plotos(eqdskin.rplas,eqdskin.zplas,'-',[],[],colos(icol,:));
  ylabel('Z')
  xlabel('R')
  title('plasma boundary')
  axis equal

  %%%%%%%%%%%%%%%%%%%%%%%%
  % plot psi(R,Z)
  %%%%%%%%%%%%%%%%%%%%%%%%
  ifig=ifig+1;
  figure(ifig)
  if ifiggiven==0; clf; end
  if ifiggiven<0
    hold all
  end
  lnstyle=[{'-'} {'--'} {':'} {'-.'}];
  contour(eqdskin.rmesh,eqdskin.zmesh,eqdskin.psi',100,lnstyle{mod(icol-1,4)+1});
  hold all
  plotos(eqdskin.rplas,eqdskin.zplas,lnstyle{mod(icol-1,4)+1},[2],[],colos(icol,:));
  if eqdskin.nblim>=5; plotos(eqdskin.rlim,eqdskin.zlim,lnstyle{mod(icol-1,4)+1},[],[],colos(icol,:)); end

  ylabel('Z')
  xlabel('R')
  title('psi(R,Z)')
  axis equal

  if ~isfield(eqdskin,'rjphi') || isempty(eqdskin.rjphi)
    % read_eqdsk was called without calculating Grad-Shafranov related fields
    return
  end

end

%%%%%%%%%%%%%%%%%%%%%%%%
% Plot rjphi and Gradshafranov from read_eqdsk (new)
%%%%%%%%%%%%%%%%%%%%%%%%

[zzz iraxis]=min(abs(eqdskin.rmesh-eqdskin.raxis));
[zzz izaxis]=min(abs(eqdskin.zmesh-eqdskin.zaxis));
rjphi_axis=eqdskin.rjphi(iraxis,izaxis);
zzzmax=sign(rjphi_axis)*max([abs(eqdskin.rjphi(iraxis,izaxis)), ...
                    abs(eqdskin.gradshaf(iraxis,izaxis)),abs(-eqdskin.FFprime(1)-4e-7*pi*eqdskin.pprime(1)*eqdskin.raxis.^2)]);
zzzmin=zzzmax;
if zzzmax>0
  zzzmin=0.1*zzzmax;
else
  zzzmax=0.1*zzzmin;
end
% remove values outside plasma since no meaning and slows down contour plot
ijk=find(eqdskin.rjphi==0);
eqdskin.gradshaf(ijk)=-0.1*rjphi_axis;

if ~iplot_only_gs && ~doplot_only_Bres

  figure
  contour(eqdskin.rmesh,eqdskin.zmesh,eqdskin.rjphi',linspace(zzzmin,zzzmax,100));
  colorbar
  hold all
  plotos(eqdskin.rplas,eqdskin.zplas,'k-');
  axis equal
  title(['eqdsk.rjphi on linspace(' num2str(zzzmin) ',' num2str(zzzmax) ',' num2str(100) ')'])

end

if ~doplot_only_Bres
  figure
  contour(eqdskin.rmesh,eqdskin.zmesh,eqdskin.rjphi',linspace(zzzmin,zzzmax,60),'-');
  hold all
  contour(eqdskin.rmesh,eqdskin.zmesh,eqdskin.gradshaf',linspace(zzzmin,zzzmax,60),'--');
  plotos(eqdskin.rplas,eqdskin.zplas,'k-');
  axis equal
  title(['eqdsk.rjphi (solid) ; eqdsk.gradshaf(dashed) on linspace(' num2str(zzzmin) ',' num2str(zzzmax) ',' num2str(100) ')'])

  figure;
  plot(eqdskin.rmesh,eqdskin.rjphi(:,izaxis))
  hold all
  plot(eqdskin.rmesh,eqdskin.gradshaf(:,izaxis),'--')
  legend('r j_\phi','\Delta* \psi')
  xlabel('R [m]')
  ylabel('left/right hand side of GS eq.')
end

if ~iplot_only_gs && ~doplot_only_Bres

  %%%%%%%%%%%%%%%%%%%%%%%%
  % Plot B components
  %%%%%%%%%%%%%%%%%%%%%%%%

  figure
  contour(eqdskin.rmesh,eqdskin.zmesh,eqdskin.BR',100);
  hold all
  plotos(eqdskin.rplas,eqdskin.zplas,'k-');
  axis equal
  title(['eqdsk.BR'])

  figure
  contour(eqdskin.rmesh,eqdskin.zmesh,eqdskin.BZ',100);
  axis equal
  hold all
  plotos(eqdskin.rplas,eqdskin.zplas,'k-');
  title(['eqdsk.BZ'])

  figure
  contour(eqdskin.rmesh,eqdskin.zmesh,eqdskin.Bphi',100);
  axis equal
  hold all
  plotos(eqdskin.rplas,eqdskin.zplas,'k-');
  title(['eqdsk.Bphi'])

  figure
  contour(eqdskin.rmesh,eqdskin.zmesh,eqdskin.Bpol',100);
  hold all
  plotos(eqdskin.rplas,eqdskin.zplas,'k-');
  axis equal
  title(['eqdsk.Bpol'])

end

% B and resonances

figure
e_over_mass_e = 1.602e-19./0.910938356e-30;
e_over_mass_p = 1.602e-19./1.6726219e-27;
titleg = [];
for ires=1:size(iplot_resonances,1)
  if iplot_resonances(ires,2) < 0;
    Bres(ires) = iplot_resonances(ires,1) * 2. * pi ./ abs(iplot_resonances(ires,2)) ./ e_over_mass_e;
  else
    Bres(ires) = iplot_resonances(ires,1) * 2. * pi ./ iplot_resonances(ires,2) ./ e_over_mass_p;
  end
  [hh1 hh2]=contour(eqdskin.rmesh,eqdskin.zmesh,eqdskin.B',[Bres(ires) Bres(ires)],'--','linewidth',2,'color',colos(ires,:));
  if ~isempty(hh1);
    hold all
    titleg{end+1} = [num2str(iplot_resonances(ires,1),'%.2e') ' ' num2str(iplot_resonances(ires,2))];
  else
    delete(hh2);
  end
% $$$   if isobject(hh2)
% $$$     xxx = hh2.XData;
% $$$     yyy = hh2.YData;
% $$$   else
% $$$     xxx=get(get(hh2,'children'),'xdata');
% $$$     yyy=get(get(hh2,'children'),'ydata');
% $$$   end
% $$$   if ~iscell(xxx)
% $$$     if ~isempty(xxx) && ~isempty(yyy)
% $$$       text(xxx(1)-0.01,yyy(1),[num2str(iplot_resonances(ires,1),'%e') ' ' num2str(iplot_resonances(ires,2))])
% $$$     end
% $$$   end

end
lgd=legend(titleg,'location','BestOutside');
%set(lgd,'AutoUpdate','off');
contour(eqdskin.rmesh,eqdskin.zmesh,eqdskin.B',100);
axis equal
if eqdskin.nblim>=5; plotos(eqdskin.rlim,eqdskin.zlim,'-',[],[],'k'); end

% $$$   BX3=118e9*2*pi./1.602e-19.*0.911e-30/3;
% $$$   [hh1 hh2]=contour(eqdskin.rmesh,eqdskin.zmesh,eqdskin.B',[BX3 BX3],'k--','linewidth',2);
% $$$   xxx=get(get(hh2,'children'),'xdata');
% $$$   yyy=get(get(hh2,'children'),'ydata');
% $$$   if ~iscell(xxx)
% $$$     if ~isempty(xxx) && ~isempty(yyy)
% $$$       text(xxx(1)-0.01,yyy(1),'X3')
% $$$     end
% $$$   end

plotos(eqdskin.rplas,eqdskin.zplas,'k-',[2 0]);
ylabel('Z')
xlabel('R')
title(['eqdsk.B'])

if doplot_only_Bres
  figure
  for ires=1:size(iplot_resonances,1)
    [hh1 hh2]=contour(eqdskin.rmesh,eqdskin.zmesh,eqdskin.B',[Bres(ires) Bres(ires)],'--','linewidth',2,'color',colos(ires,:));
    hold all
  end
  lnstyle=[{'-'} {'--'} {':'} {'-.'}];
  icol=1;
  contour(eqdskin.rmesh,eqdskin.zmesh,eqdskin.psi',100,lnstyle{mod(icol-1,4)+1});
  plotos(eqdskin.rplas,eqdskin.zplas,lnstyle{mod(icol-1,4)+1},[2],[],colos(icol,:));
  if eqdskin.nblim>=5; plotos(eqdskin.rlim,eqdskin.zlim,lnstyle{mod(icol-1,4)+1},[],[],colos(icol,:)); end

  ylabel('Z')
  xlabel('R')
  title('psi(R,Z)')
  axis equal
  legend(titleg,'location','BestOutside')
end
%%%%%%%%%%%%%%%%%%%%%%%%
% check Grad-Shafranov and interpolate to 257x257
%%%%%%%%%%%%%%%%%%%%%%%%

if icall_eqtransform
  varargout{1}=eqdsk_transform(eqdskin,257,257,[],1) % makes plots including Grad-shfranov test
end
