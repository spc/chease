function extralines_cellarray = write_expeq_extralines(extralines)
%
% extralines_cellarray = write_expeq_extralines(extralines)
%
% write extralinesiliary information to extralines_cellarray so it can be added at the end of EXPEQ file.
% Formats borrowed from CHEASE outmksa.f90
%

% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

mu0 = 4.E-07*pi;
%
if any(~isfield(extralines,{'R0exp','B0exp'})) return; end % do not continue if not at least R0 and B0 are defined
%
R0 = extralines.R0exp; B0 = extralines.B0exp;
SIGNB0XP = sign(B0);
% $$$
i = 0;
i = i+1;extralines_cellarray{i} = '*************************************';
i = i+1;extralines_cellarray{i} = 'SOME QUANTITIES AND THEIR MKSA VALUES';
i = i+1;extralines_cellarray{i} = '*************************************';
% $$$
% $$$
if isfield(extralines,'psi0');
  psi0_chease = extralines.psi0/(B0*R0^2);
  i = i+1;extralines_cellarray{i}=sprintf('\n%18.8E%s%18.8E', abs(psi0_chease), ' abs(PSI-AXIS) --> [T M**2] ', abs(extralines.psi0));
end
if isfield(extralines,'rmag'); i = i+1;extralines_cellarray{i}=sprintf('\n%18.8E%s%18.8E', extralines.rmag/R0,' R OF MAGAXE --> [M]   ',extralines.rmag); end
if isfield(extralines,'zmag'); i = i+1;extralines_cellarray{i}=sprintf('\n%18.8E%s%18.8E', extralines.zmag/R0,' Z OF MAGAXE --> [M]   ',extralines.zmag); end
if isfield(extralines,'z0'); i = i+1;extralines_cellarray{i}=sprintf('\n%18.8E%s%18.8E', extralines.z0/R0,' Z0 --> [M]   ',extralines.z0); end
i = i+1;extralines_cellarray{i}=sprintf('\n%18.8E%s',R0,' R0 [M] USED FOR CONVERTING TO MKSA');
i = i+1;extralines_cellarray{i}=sprintf('\n%18.8E%s',B0,' B0 [T] USED FOR CONVERTING TO MKSA');
i = i+1;extralines_cellarray{i}=sprintf('\n%18.8E%s',SIGNB0XP,' SIGN OF B0 IN EXPERIMENT (CHEASE ASSUMES 1.0) ');
if isfield(extralines,'Itot')
  SIGNIPXP = sign(extralines.Itot);
  i = i+1;extralines_cellarray{i}=sprintf('\n%18.8E%s%18.8E',extralines.Itot / (R0*B0)*mu0, ' TOTAL CURRENT --> [A] ',extralines.Itot);
  i = i+1;extralines_cellarray{i}=sprintf('\n%18.8E%s',SIGNIPXP,' SIGN OF IP IN EXPERIMENT (CHEASE ASSUMES 1.0) ');
end
if isfield(extralines,'elong'); i = i+1;extralines_cellarray{i}=sprintf('\n%18.8E%s',extralines.elong,' b/a'); end
if isfield(extralines,'q0');
  i = i+1;extralines_cellarray{i}=sprintf('\n%18.8E%s%18.8E',extralines.q0,' Q_ZERO, USING SIGNS OF IP AND B0, WOULD GIVE: ', SIGNB0XP*SIGNIPXP*extralines.q0);
end
if isfield(extralines,'q_edge');
  i = i+1;extralines_cellarray{i}=sprintf('\n%18.8E%s%18.8E',extralines.q_edge,' Q_EDGE, USING SIGNS OF IP AND B0, WOULD GIVE: ', SIGNB0XP*SIGNIPXP*extralines.q_edge);
end
if isfield(extralines,'betap'); i = i+1;extralines_cellarray{i}=sprintf('%18.8E%s',extralines.betap,' POLOIDAL BETA'); end
if isfield(extralines,'betax'); i = i+1;extralines_cellarray{i}=sprintf('%18.8E%s',extralines.betax,' BETA_EXP=<P>*2*MU0/B0**2'); end
if isfield(extralines,'induc'); i = i+1;extralines_cellarray{i}=sprintf('%18.8E%s',extralines.induc,' LI'); end
if isfield(extralines,'p0');
  p0_chease = extralines.p0 *(mu0 / (B0^2));
  p0_1019m3keV = p0_chease * B0^2/mu0/1.602e-16/1e19;
  i = i+1;extralines_cellarray{i}=sprintf('\n%18.8E%s%18.8E%s%18.8E',p0_chease, ' PRESSURE ON AXIS --> [Pa] ', extralines.p0,'  --> [10**19 M**-3 KEV]: ', p0_1019m3keV);
end
if isfield(extralines,'beta'); i = i+1;extralines_cellarray{i}=sprintf('%18.8E%s',extralines.beta,' BETA'); end
if isfield(extralines,'betas'); i = i+1;extralines_cellarray{i}=sprintf('%18.8E%s',extralines.betas,' BETA* (SQRT(<P**2>))'); end
if isfield(extralines,'psi0')
  i = i+1;extralines_cellarray{i}=sprintf('\n%18.8E%s%18.8E',psi0_chease, ' PSI-AXIS --> [T M**2] ', extralines.psi0);
  i = i+1;extralines_cellarray{i}=sprintf('\n%18.8E%s%18.8E',2*pi*psi0_chease,' 2*PI*PSI-AXIS -->     ', 2*pi*extralines.psi0);
  i = i+1;extralines_cellarray{i}=sprintf('\n%18.8E%s%18.8E',SIGNIPXP*psi0_chease, ' IP_SIGN*PSI-AXIS --> [T M**2] ', SIGNIPXP*psi0_chease);
  i = i+1;extralines_cellarray{i}=sprintf('\n%18.8E%s%18.8E',SIGNIPXP*2*pi*psi0_chease,' IP_SIGN*2*PI*PSI-AXIS -->     ', SIGNIPXP*2*pi*psi0_chease);
end
if isfield(extralines,'aratio'); i = i+1;extralines_cellarray{i}=sprintf('\n%18.8E%s%18.8E',extralines.aratio/R0,' ASPECT RATIO ; a/R= ', extralines.aratio); end
if isfield(extralines,'volume'); i = i+1;extralines_cellarray{i}=sprintf('\n%18.8E%s%18.8E',extralines.volume/R0^3, ' VOLUME -> ',extralines.volume); end
if isfield(extralines,'area'); i = i+1;extralines_cellarray{i}=sprintf('\n%18.8E%s%18.8E',extralines.area/R0^2,   ' AREA   -> ',extralines.area); end
if isfield(extralines,'length'); i = i+1;extralines_cellarray{i}=sprintf('\n%18.8E%s%18.8E',extralines.length/R0, ' LENGTH -> ',extralines.length); end
if isfield(extralines,'rmin'); i = i+1;extralines_cellarray{i}=sprintf('\n%18.8E%s%18.8E',extralines.rmin/R0,' RMIN -> RMIN [m] ',extralines.rmin); end
if isfield(extralines,'rmax'); i = i+1;extralines_cellarray{i}=sprintf('\n%18.8E%s%18.8E',extralines.rmax/R0,' RMAX -> RMAX [m] ',extralines.rmax); end
if isfield(extralines,'zmin'); i = i+1;extralines_cellarray{i}=sprintf('\n%18.8E%s%18.8E',extralines.zmin/R0,' ZMIN -> ZMIN [m] ',extralines.zmin); end
if isfield(extralines,'zmax'); i = i+1;extralines_cellarray{i}=sprintf('\n%18.8E%s%18.8E',extralines.zmax/R0,' ZMAX -> ZMAX [m] ',extralines.zmax); end
if isfield(extralines,'rgeom'); i = i+1;extralines_cellarray{i}=sprintf('\n%18.8E%s%18.8E',extralines.rgeom/R0,' RGEOM -> RGEOM [m] ', extralines.rgeom); end
if isfield(extralines,'aratio'); i = i+1;extralines_cellarray{i}=sprintf('\n%18.8E%s%18.8E',extralines.aratio,' MINOR RADIUS -> A [m] ',extralines.aratio*R0); end
% $$$
