function [fname_out_nblopt,globalsvalues_nblopt,namelist_struct_nblopt,expeqout_nblopt,ocols_nblopt,jout_nblopt] = run_nblopt(inputfile,is_eqdsk,add_jedge,main_suffix,varargin);
%
% [fname_out_nblopt,globalsvalues_nblopt,namelist_struct_nblopt,expeqout_nblopt,ocols_nblopt,jout_nblopt] = run_nblopt(inputfile,is_eqdsk,add_jedge,main_suffix,varargin);
%
% inputfile: an EXPEQ or an EQDSK file or structure
% is_eqdsk: 0 for an EXPEQ input and 1 for an eqdsk
% add_jedge: additional edge current (0 by default), to mimic edge bootstrap of CD. If array, loops over values provided
%            j_expeq = (1-add_jedge(i)/5) * j_expeq + add_jedge(i) * j_edge(rho)
%            hence [0, 0.5, 1] will scan initial j_expeq with Ip fixed (NCSCAL=2, except if npropt_fix=5)
%            0 will just test given equilibrium as is
% main_suffix: suffix added to output filenames by run_chease
%
% varargin{1}: ninblopt, max number of ballooning optimization iteration (default = 30)
% varargin{2}: pprime_bal_max (def=6), maximum pprime above which assumed unstable to limit pprime<abs(PPRIME_BAL_MAX) when is 2nd stability region
% varargin{3}: rhodep_j (default = 0.97), location of added jedge Gaussian
% varargin{4}: wdep_j (default = 0.04), full 1/e width of added jedge Gaussian
%

% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

npropt_for_fix_profile = 2;

jout = 0;
if ~exist('is_eqdsk'), fprintf('\n%s\n\n','is_eqdsk should be provided'); return; end
if ~exist('add_jedge') || isempty(add_jedge), add_jedge = 0; end

ninblopt = 30;
if nargin >= 5 && ~isempty(varargin{1}) && isnumeric(varargin{1})
  ninblopt = varargin{1};
end
pprime_bal_max = 6.;
if nargin >= 6 && ~isempty(varargin{2}) && isnumeric(varargin{2})
  pprime_bal_max = varargin{2};
end
rhodep_j = 0.97;
if nargin >= 7 && ~isempty(varargin{3}) && isnumeric(varargin{3})
  rhodep_j = varargin{3};
end
wdep_j = 0.04;
if nargin >= 8 && ~isempty(varargin{4}) && isnumeric(varargin{4})
  wdep_j = varargin{4};
end

% get npropt = npropt_for_fix_profile EXPEQ file to run nblopt = 1 on
if is_eqdsk == 1
  if ~exist('main_suffix') || isempty('main_suffix')
    main_suffix = 'run_nblopt_eqdsk';
  end
  [~,~,namelist_struct_eqdsk] = run_chease(2);
  namelist_struct_eqdsk.npropt = npropt_for_fix_profile;
  jout = jout + 1;
  if ~isstruct(inputfile)
    eqdsk_struct_in = read_eqdsk(inputfile);
  else
    eqdsk_struct_in = inputfile;
  end
  namelist_struct_eqdsk.nverbose = 3; % required to get ocols and globalsout
  [fname_out_nblopt{jout},globalsvalues_nblopt{jout},namelist_struct_nblopt{jout}] = run_chease(namelist_struct_eqdsk,eqdsk_struct_in, ...
          eqdsk_struct_in.cocos,[],main_suffix);
  r0exp = globalsvalues_nblopt{jout}.r0exp;
  b0exp = globalsvalues_nblopt{jout}.b0exp;
  ip_phys = globalsvalues_nblopt{jout}.ip_phys;
  ip_chease = globalsvalues_nblopt{jout}.ipchease;
  ab = {fname_out_nblopt{jout}{contains(fname_out_nblopt{jout},'EXPEQ.OUT')}};
  expeqout_nblopt{jout} = ab{1};
  ab = fname_out_nblopt{jout}{contains(fname_out_nblopt{jout},'.cols')};
  ocols_nblopt{jout} = ab;
  % keep original (R,Z)
  expeq_struct_ref = read_expeq(expeqout_nblopt{jout});
  expeq_struct_ref.RZ_psi = [reshape(eqdsk_struct_in.rplas/r0exp,eqdsk_struct_in.nbbound,1),reshape(eqdsk_struct_in.zplas/r0exp,eqdsk_struct_in.nbbound,1)];
  expeq_struct_ref.n_psi = eqdsk_struct_in.nbbound;
elseif is_eqdsk == 0
  if ~exist('main_suffix') || isempty('main_suffix')
    main_suffix = 'run_nblopt_expeq';
  end
  if ~isstruct(inputfile)
    expeq_struct_ref = read_expeq(inputfile);
  else
    expeq_struct_ref = inputfile;
  end
  if expeq_struct_ref.nsttp ~= npropt_for_fix_profile || expeq_struct_ref.nrhotype ~= 0
    % need to run CHEASE to get npropt expeq.out(rhopol)
    [~,~,namelist_struct_expeq] = run_chease(1);
    namelist_struct_expeq.npropt = npropt_for_fix_profile;
    namelist_struct_expeq.nverbose = 3; % required to get ocols and globalsout
    jout = jout + 1;
    [fname_out_nblopt{jout},globalsvalues_nblopt{jout},namelist_struct_nblopt{jout}] = run_chease(namelist_struct_expeq,inputfile,[], ...
          [],main_suffix);
    r0exp = globalsvalues_nblopt{jout}.r0exp;
    b0exp = globalsvalues_nblopt{jout}.b0exp;
    ip_phys = globalsvalues_nblopt{jout}.ip_phys;
    ip_chease = globalsvalues_nblopt{jout}.ipchease;
    ab = {fname_out_nblopt{jout}{contains(fname_out_nblopt{jout},'EXPEQ.OUT')}};
    expeqout_nblopt{jout} = ab{1};
    ab = fname_out_nblopt{jout}{contains(fname_out_nblopt{jout},'.cols')};
    ocols_nblopt{jout} = ab;
    % keep original (R,Z)
    expeq_struct_ref0 = expeq_struct_ref;
    expeq_struct_ref = read_expeq(expeqout_nblopt{jout});
    expeq_struct_ref.RZ_psi = expeq_struct_ref0.RZ_psi;
    expeq_struct_ref.n_psi = expeq_struct_ref0.n_psi;
  else
    try
      r0_line = expeq_struct_ref.extralines{contains(expeq_struct_ref.extralines,'R0 [M]')};
      b0_line = expeq_struct_ref.extralines{contains(expeq_struct_ref.extralines,'B0 [T]')};
      ip_line = expeq_struct_ref.extralines{contains(expeq_struct_ref.extralines,'TOTAL CURRENT')};
      r0exp = sscanf(r0_line,'%f');
      b0exp = sscanf(b0_line,'%f');
      ip_chease = sscanf(ip_line,'%f');
      ip_phys = sscanf(ip_line(findstr(ip_line,'[A]')+4:end),'%f');
    catch ME
      disp('tried to extract r0exp, b0exp and Ips but failed, provide expeq structure in input')
      throw ME
    end
  end
else
  disp(['is_eqdsk = ' is_eqdsk ' should be 0 or 1'])
  return
end


%% start from typical pprime, avoid center and finite edge value
expeq_struct_ref.Pprime = min(0.,-6.*(expeq_struct_ref.rho-0.2).*(1-expeq_struct_ref.rho)-0.3.*expeq_struct_ref.rho.^2);

ip_phys

amp_j0 = 0.4;
for iloop=1:numel(add_jedge)
  jout = jout + 1;
  expeq_struct_ref_iloop = expeq_struct_ref;
  % add edge current (inspired by eqdsk2chease2D.m)
  amp_j = add_jedge(iloop) * amp_j0;
  frac_johm = max(1 - 0.2 * add_jedge(iloop),0.2); % to try to have same integrated current in input
  %jbsexp=amp_j.*(1+tanh((expeq_struct_ref_iloop.rho-rhodep_j)./wdep_j))+amp_j.*exp(-(expeq_struct_ref_iloop.rho-0.97).^2./0.12^2);
  jbsexp=amp_j.*exp(-4.*(expeq_struct_ref_iloop.rho-rhodep_j).^2./wdep_j^2);
  if any(npropt_for_fix_profile == [2, 3, 4])
    expeq_struct_ref_iloop.Istar=frac_johm.*expeq_struct_ref_iloop.Istar+jbsexp;
  elseif npropt_for_fix_profile == 5
    jbsexp=amp_j.*(1+tanh((expeq_struct_ref_iloop.rho-rhodep_j)./wdep_j))+amp_j.*exp(-(expeq_struct_ref_iloop.rho-0.97).^2./0.12^2);
    expeq_struct_ref_iloop.q = expeq_struct_ref_iloop.q+jbsexp;
  end
  %
  [~,~,namelist_struct_nblopt{jout}] = run_chease(1);
  namelist_struct_nblopt{jout}.r0exp = r0exp;
  namelist_struct_nblopt{jout}.b0exp = b0exp;
  namelist_struct_nblopt{jout}.currt = ip_chease;
  namelist_struct_nblopt{jout}.ncscal = 2;
  namelist_struct_nblopt{jout}.relax = 0.2;
  namelist_struct_nblopt{jout}.ninmap = 60;
  namelist_struct_nblopt{jout}.ninsca = 60;
  if npropt_for_fix_profile==5
    namelist_struct_nblopt{jout}.ncscal = 1;
    namelist_struct_nblopt{jout}.ninsca = 100;
    namelist_struct_nblopt{jout}.ninmap = 100;
  namelist_struct_nblopt{jout}.relax = 0.4;
  end
  namelist_struct_nblopt{jout}.npropt = npropt_for_fix_profile;
  namelist_struct_nblopt{jout}.nblopt = 1;
  namelist_struct_nblopt{jout}.epslon = 1e-6;
  namelist_struct_nblopt{jout}.ninblopt = ninblopt;
  namelist_struct_nblopt{jout}.nppr = 100;
  namelist_struct_nblopt{jout}.cfbal = 100;
  namelist_struct_nblopt{jout}.pprime_bal_max = pprime_bal_max;
  namelist_struct_nblopt{jout}.ndiagop = 0;
  namelist_struct_nblopt{jout}.nverbose = 3;
  chease_suffix = [main_suffix 'nblopt'];
  tic
  [fname_out_nblopt{jout},globalsvalues_nblopt{jout},namelist_struct_nblopt{jout}] = ...
      run_chease(namelist_struct_nblopt{jout},expeq_struct_ref_iloop,[],[],chease_suffix);
  toc
  jout_nblopt(iloop) = jout;
  ab = fname_out_nblopt{jout}{contains(fname_out_nblopt{jout},'NOUT')};
  NOUT_chease_nblopt = ab;
  ab = {fname_out_nblopt{jout}{contains(fname_out_nblopt{jout},['EXPEQ.OUT' chease_suffix])}};
  if numel(ab) >= 1
    expeqout_nblopt{jout} = ab{1};
  else
    fname_out_nblopt
    warning('CHEASE could not run properly');
    expeqout_nblopt{jout} = [];
  end
  % could replace RZ with original shape at this stage for expeqout_nblopt{jout_nblopt(iloop)}
  ab = fname_out_nblopt{jout}{contains(fname_out_nblopt{jout},'.cols')};
  ocols_nblopt{jout} = ab;
  if numel(ab) >= 1
    out_chease_nblopt = ab(1:end-5);
    unix(['grep ''ERROR >'' ' out_chease_nblopt ' | tail -10']);
  end
  if isempty(expeqout_nblopt{jout_nblopt(iloop)})
    jout = jout + 2;
    expeqout_nblopt{jout-1:jout} = [];
    ocols_nblopt{jout-1:jout} = [];
  else
    %%
    plot_expeq(expeq_struct_ref_iloop);
    plot_expeq(gcf,expeqout_nblopt{jout_nblopt(iloop)});
    %% check stability
    jout = jout + 1;
    [~,~,namelist_struct_nblopt{jout}] = run_chease(1);
    namelist_struct_nblopt{jout}.r0exp = r0exp;
    namelist_struct_nblopt{jout}.b0exp = b0exp;
    namelist_struct_nblopt{jout}.currt = ip_chease;
    namelist_struct_nblopt{jout}.ncscal = 4;
    namelist_struct_nblopt{jout}.cpress=0.95;
    namelist_struct_nblopt{jout}.nverbose=3;
    chease_suffix = [main_suffix '_testBAL_095'];
    tic
    [fname_out_nblopt{jout},globalsvalues_nblopt{jout},namelist_struct_nblopt{jout}] = ...
        run_chease(namelist_struct_nblopt{jout},expeqout_nblopt{jout_nblopt(iloop)},[],[],chease_suffix);
    toc
    ab = fname_out_nblopt{jout}{contains(fname_out_nblopt{jout},'.cols')};
    ocols_nblopt{jout} = ab;
    out_chease_nblopt095 = ab(1:end-5);
    ab = {fname_out_nblopt{jout}{contains(fname_out_nblopt{jout},['EXPEQ.OUT' chease_suffix])}};
    if numel(ab)>=1
      expeqout_nblopt{jout} = ab{1};
    else
      expeqout_nblopt{jout} = '';
    end
    ab = fname_out_nblopt{jout}{contains(fname_out_nblopt{jout},'NOUT')};
    NOUT_chease_nblopt095 = ab;
    [globalsvalues_nblopt{jout},ocols_struct] = extractdatachease(out_chease_nblopt095,ocols_nblopt{jout});
    ij=find(ocols_struct.s_mesh.data>0.5);
    nb_unstable = max(ocols_struct.ncbal.data(ij));
    if nb_unstable == 0
      disp(['with 0.95 cpress, ' expeqout_nblopt{jout_nblopt(iloop)} ' is stable']);
    else
      disp(['with 0.95 cpress, ' expeqout_nblopt{jout_nblopt(iloop)} ' is unstable']);
    end
    figure;plot(ocols_struct.s_mesh.data,ocols_struct.ncbal.data)
    hold on

    %%
    jout = jout + 1;
    namelist_struct_nblopt{jout} = namelist_struct_nblopt{jout-1};
    namelist_struct_nblopt{jout}.cpress=1.02;
    namelist_struct_nblopt{jout}.relax=0.2;
    namelist_struct_nblopt{jout}.nopt = -2;
    chease_suffix = [main_suffix '_testBAL_102'];
    tic
    [fname_out_nblopt{jout},globalsvalues_nblopt{jout},namelist_struct_nblopt{jout},namelistfile_eff] = ...
        run_chease(namelist_struct_nblopt{jout},expeqout_nblopt{jout_nblopt(iloop)},[],[],chease_suffix,[],[],NOUT_chease_nblopt095);
    toc
    ab = fname_out_nblopt{jout}{contains(fname_out_nblopt{jout},'.cols')};
    ocols_nblopt{jout} = ab;
    out_chease_nblopt102 = ab(1:end-5);
    ab = {fname_out_nblopt{jout}{contains(fname_out_nblopt{jout},['EXPEQ.OUT' chease_suffix])}};
    if numel(ab)>=1
      expeqout_nblopt{jout} = ab{1};
    else
      expeqout_nblopt{jout} = '';
    end
    [globalsvalues_nblopt{jout},ocols_struct] = extractdatachease(out_chease_nblopt102,[out_chease_nblopt102 '.cols'],[out_chease_nblopt102 '.RZcols']);
    ij=find(ocols_struct.s_mesh.data>0.5);
    nb_unstable = max(ocols_struct.ncbal.data(ij));
    if nb_unstable == 0
      disp(['with 1.02 cpress, ' expeqout_nblopt{jout_nblopt(iloop)} ' is stable']);
    else
      disp(['with 1.02 cpress, ' expeqout_nblopt{jout_nblopt(iloop)} ' is unstable']);
    end
    plot(ocols_struct.s_mesh.data,ocols_struct.ncbal.data)
    legend('0.95','1.02')
    ylabel('NCBAL')
    title('with inputfile as is')
    unix(['grep ''GEXP'' '  out_chease_nblopt095]);
    unix(['grep ''GEXP'' '  out_chease_nblopt102]);
  end
end
