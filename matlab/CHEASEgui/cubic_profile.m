function [x,y] = cubic_profile (x1,x2,y1,y2,y1prime,y2prime);
%this function builds the cubic profile starting from (x1,y1), (x2,y2) points which
%belong to the function, y1prime and y2prime, value of the derivative of
%the funciton in x=x1 and x=x2
%SYNTAX:
%cubic_profile (x1,x2,y1,y2,y1prime,y2prime)

% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

%define coefficients
delta =x2-x1;
d = y1;
a = y2;
c =  delta.*y1prime + 3*d;
b = -delta.*y2prime + 3*a;

%build the profile

x = linspace (x1,x2,33);
tmp1=(x-x1)/delta;
tmp2=(x2-x)/delta;
y=a * tmp1.^3 + b * (tmp1.^2).*tmp2 + c * tmp1 .* (tmp2.^2) + d * tmp2.^3;
%plot (x,y);
