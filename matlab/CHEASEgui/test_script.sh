#!/bin/bash
# Copyright 2024 SPC-EPFL
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# testing script for Continuous Integration
#

## test type
if [[ -z "$1" ]]
then
  echo 'usage:'
  echo ' Run matlab tests: '
  echo '     ./test_script.sh $matlabcmd $test_to_run'
  echo '     will call required matlab and run >>test_matlab($test_to_run)'
  echo '   $matlabcommand is the command used to launch the correct matlab version'
  echo '   works on SPC lac clusters for now'
  exit 1
fi

matlabbin=$1
testargument=$2

matlabcmd="$matlabbin -nodesktop -nosplash -noFigureWindows"
matlab_call="tests_matlab('$testargument')"
full_cmd="$matlabcmd -r $matlab_call";
echo $full_cmd
export CHEASE_ROOT=$PWD/../..
export PATH=$CHEASE_ROOT/scripts_for_bin:$PATH:/home/codes/bin # to add path for o.chease_to_cols and official chease release at SPC

$full_cmd ## execute
CODE=$?
echo "exit with CODE" $CODE
exit $CODE
