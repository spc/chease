function [cocos_out,comments,status]=find_cocos(b0,ip,psi_profile,pprime_profile,q_profile,r0,a_minor,cocos_to_check,sigma_rphiz,ffprime0,f0,phitor_edge);
%
% [cocos_ok,status,cocos_ok_qabs]=find_cocos(b0,ip,psi_profile,pprime_profile,q_profile,r0,a_minor[,cocos_to_check[,sigma_rphiz[,ffprime0[,f0[,phitor_edge]]]]]);
%
%    or
%
% [cocos_ok,status,cocos_ok_qabs]=find_cocos(eqdsk_filename_or_structure[,cocos_to_check[,sigma_rphiz]]);
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% finds the various cocos compatible with the input values and, if provided, check if the given cocos is ok
%
% status: 0 => cocos_to_check is fine
%         >0 => cocos_to_check not provided, so number of cocos ok and otherwise no problems spotted
%         <0 : some problems spotted and listed in comments
%
% cocos_ok: array of cocos compatible with inputs, 1st one is cocos_to_check if provided and ok
%           cocos_ok cocos compatible with q sign included
%           cocos_ok_qabs cocos compatible if one assumes abs(q) might have been set (allows sign error for q and phitor)
%
% sigma_rphiz: if given allows to reduce optional cocos values, since cannot be determined otherwise
%              if not known but effective b0 sign known, then deduce it from b0
%
% if only q_edge or q_95 or q0 is known, provide 1 value in q_profile
%
% if only psi_edge - psi_axis, provide this value in psi_profile
%
% Provide pprime_profile only if psi_profile is provided. If not known, provide pprime(0)
%
% If any of the required inputs is not known, give an empty value []
%
% If an eqdsk structure or feqdsk_file_name is to be checked, provide just as 1st input, 2nd input (optional) is then cocos_to_check
%
% if b0, ip, q, r0 and/or a_minor are empty, assume +1
%
% examples:
%
% [cocos_ok,comments,status]=find_cocos(b0,ip,psi_profile,pprime_profile,q_profile,r0,a_minor,2,[],ffprime0); % checking cocos=2
% [cocos_ok,comments,status]=find_cocos(b0,ip,psi_profile,pprime_profile,q_profile,r0,a_minor,[],-1,ffprime0); % find cocos knowing rphiz
% [cocos_ok,comments,status] = find_cocos('eqdsk_file_name');
% [cocos_ok,comments,status] = find_cocos(eqdsk_structure,1); % checking cocos = 1
%

% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

status = -1;
cocos_out = [];
comments = [];
if nargin==0 || (isnumeric(b0) && nargin < 7) || (~ischar(b0) && (~isstring(b0) || isstring(b0)&&numel(b0)>1) && ~isstruct(b0) && ~isnumeric(b0))
  disp('requires at least 7 inputs:')
  disp(['[cocos_ok,status,cocos_ok_qabs]=find_cocos(b0,ip,psi_profile,pprime_profile,q_profile,r0,a_minor[,f0[,ffprime0[,' ...
        ' cocos_to_check]]])']);
  disp('or 1-2 inputs with');
  disp('[cocos_ok,status,cocos_ok_qabs]=find_cocos(eqdsk_filename_or_structure[,cocos_to_check]);');
  return
end

i_eqdsk_input = 0;
if ischar(b0) || isstring(b0) || isstruct(b0)
  % eqdsk given, fill in inputs
  cocos_for_readeqdsk = 2; % start with a default value so can call find_cocos from read_eqdsk
  if nargin >=2 && ~isempty(ip)
    cocos_to_check = ip;
    cocos_for_readeqdsk = cocos_to_check;
  end
  if nargin >=3 && ~isempty(psi_profile)
    sigma_rphiz = psi_profile;
  end
  if ischar(b0) || isstring(b0)
    eqdsk_filename = b0;
    try
      eqdsk = read_eqdsk(eqdsk_filename,cocos_for_readeqdsk,0,[],[],0);
    catch ME
      rethrow ME
      disp('problem with eqdsk input')
      return
    end
  else
    eqdsk = b0;
  end
  try
    b0 = eqdsk.b0;
    ip = eqdsk.ip;
    psi_profile = eqdsk.psimesh.*(eqdsk.psiedge-eqdsk.psiaxis);
    pprime_profile = eqdsk.pprime;
    q_profile = eqdsk.q;
    r0 = eqdsk.r0;
    if ~isfield(eqdsk,'syscode_formulas'),
      [eqdsk.syscode_formulas] = syscode_formulas(eqdsk.b0,eqdsk.ip,eqdsk.rplas,eqdsk.zplas);
    end
    a_minor = eqdsk.syscode_formulas.a_minor;
    ffprime0 = eqdsk.FFprime(1);
    f0 = eqdsk.F(1);
    i_eqdsk_input = 1;
  catch ME3
    rethrow ME3
    disp('problem with eqdsk input')
    return
  end
end

if isempty(psi_profile);
  disp('psi_profile or or value of psi_edge-psi_axis not provided')
  status = -2;
  return
end
if isempty(b0); b0=1; end
if isempty(ip); ip=1; end
if isempty(q_profile); q_profile=1; end
if isempty(r0); r0=1; end
if isempty(a_minor); a_minor=1; end

i_sigma_bp = 1;
i_sigma_rhothetaphi = 2;
i_exp_bp = 3;
i_sigma_rphiz = 4;
i_sign_q_pos = 5;
i_sign_pprime_pos = 6;
i_cocos = 7;

cocos_table = NaN*ones(18,i_cocos);
for i=1:8
  cocos_opt{i} = cocos(i);
  cocos_table(i,i_sigma_bp) = cocos_opt{i}.sigma_Bp;
  cocos_table(i,i_sigma_rhothetaphi) = cocos_opt{i}.sigma_rhothetaphi;
  cocos_table(i,i_exp_bp) = cocos_opt{i}.exp_Bp;
  cocos_table(i,i_sigma_rphiz) = cocos_opt{i}.sigma_RphiZ;
  cocos_table(i,i_sign_q_pos) = cocos_opt{i}.sign_q_pos;
  cocos_table(i,i_sign_pprime_pos) = cocos_opt{i}.sign_pprime_pos;
  cocos_table(i,i_cocos) = i;
end

for i=11:18
  cocos_opt{i} = cocos(i);
  cocos_table(i,i_sigma_bp) = cocos_opt{i}.sigma_Bp;
  cocos_table(i,i_sigma_rhothetaphi) = cocos_opt{i}.sigma_rhothetaphi;
  cocos_table(i,i_exp_bp) = cocos_opt{i}.exp_Bp;
  cocos_table(i,i_sigma_rphiz) = cocos_opt{i}.sigma_RphiZ;
  cocos_table(i,i_sign_q_pos) = cocos_opt{i}.sign_q_pos;
  cocos_table(i,i_sign_pprime_pos) = cocos_opt{i}.sign_pprime_pos;
  cocos_table(i,i_cocos) = i;
end

sigma_ip = sign(ip);
sigma_b0 = sign(b0);

% determine sign of dpsi
%
if length(psi_profile) > 1
  sigma_dpsi = sign(psi_profile(end)-psi_profile(1));
else
  sigma_dpsi = sign(psi_profile);
end

sigma_bp = sigma_dpsi .* sigma_ip;

% check if pprime has correct sign
if (length(psi_profile) > 1) && (length(psi_profile) == length(pprime_profile))
  aa = sum(pprime_profile.*psi_profile);
  if sign(aa) == 1
    status = -3;
    comments{end+1,1} = 'pprime has wrong sign'
    disp('cannot find cocos, sign(dpsi) not consistent with pprime')
    return
  end
end

% check if f0 has correct sign
if exist('f0') && ~isempty(f0)
  if sigma_b0*sign(f0) < 0
    status = -4;
    comments{end+1,1} = 'f0 has wrong sign, different from B0'
    disp('sign of f0 not consistent with b0')
  end
end

% sign of rhothetaphi
sigma_rhothetaphi = sign(q_profile(end)) * sigma_ip * sigma_b0;
if sign(q_profile(end)) > 0
  % get results if q was in fact -q but here positive because abs(q) was set
  sigma_rhothetaphi_minusq = -1 * sigma_ip * sigma_b0;
else
  % not relevant since q is negative
  sigma_rhothetaphi_minusq = NaN;
end

% check sign of Phitor
if exist('phitor_edge') && ~isempty(phitor_edge)
  if (length(psi_profile) > 1) &&  (length(q_profile) == length(psi_profile))
    phitor = sum(q_profile .* psi_profile * sigma_bp * sigma_rhothetaphi);
  else
    if length(psi_profile) > 1
      phitor = q_profile(end) .* (psi_profile(end) - psi_profile(1)) * sigma_bp * sigma_rhothetaphi;
    else
      phitor = q_profile(end) .* psi_profile * sigma_bp * sigma_rhothetaphi;
    end
  end
  if sign(phitor_edge)*sign(phitor) < 0
    status = -5;
    comments{end+1,1} = 'phitor has wrong sign, different from B0';
    disp('sign of phitor not consistent with b0');
    if sign(q_profile(end)) > 0
      % may be q was set as abs(q)
      comments{end+1,1} = 'May be q was set as abs(a), try with -q_profile';
    end
  end
end


% find exp_bp, approximate raxis=r0, assume psi=(R-Raxis)^2./a^2*(psi_edge-psi_axis) + psi_axis
if length(psi_profile) > 1
  deltastar_axis = 4.*(psi_profile(end)-psi_profile(1))./a_minor.^2;
else
  deltastar_axis = 4.*psi_profile(end)./a_minor.^2;
end
mu0 = 4e-7 *pi;
if exist('ffprime0') && ~isempty(ffprime0)
  rjphi_nopi = - mu0.*pprime_profile(1)*r0^2 - ffprime0;
else
  if abs(pprime_profile(1))>0
    rjphi_nopi = - mu0.*pprime_profile(1)*r0^2 * 3; % usually ffprime dominates
  else
    % cannot estimate rjphi in center
    rjphi_nopi = NaN;
    comments{end+1,1} = 'cannot estimate exp-Bp, needs FFprime central since pprime(1)=0';
  end
end
rjphi_4pi2 = rjphi_nopi .* 4 .* pi^2;

if ~isnan(rjphi_nopi)
  if abs(rjphi_nopi-deltastar_axis) < abs(rjphi_4pi2-deltastar_axis)
    cocos_lower_10 = 1;
    i_cocos_ok_1 = find(cocos_table(:,i_sigma_bp)==sigma_bp & cocos_table(:,i_sigma_rhothetaphi)==sigma_rhothetaphi & cocos_table(:,i_cocos)<10);
    i_cocos_ok_1_minusq = find(cocos_table(:,i_sigma_bp)==sigma_bp & cocos_table(:,i_sigma_rhothetaphi)==-sigma_rhothetaphi & cocos_table(:,i_cocos)<10);
  else
    cocos_lower_10 = 0;
    i_cocos_ok_1 = find(cocos_table(:,i_sigma_bp)==sigma_bp & cocos_table(:,i_sigma_rhothetaphi)==sigma_rhothetaphi & cocos_table(:,i_cocos)>10);
    i_cocos_ok_1_minusq = find(cocos_table(:,i_sigma_bp)==sigma_bp & cocos_table(:,i_sigma_rhothetaphi)==-sigma_rhothetaphi & cocos_table(:,i_cocos)>10);
  end
else
  cocos_lower_10 = -1;
  i_cocos_ok_1 = find(cocos_table(:,i_sigma_bp)==sigma_bp & cocos_table(:,i_sigma_rhothetaphi)==sigma_rhothetaphi & cocos_table(:,i_cocos)>0);
  i_cocos_ok_1_minusq = find(cocos_table(:,i_sigma_bp)==sigma_bp & cocos_table(:,i_sigma_rhothetaphi)==-sigma_rhothetaphi & cocos_table(:,i_cocos)>0);
end

% use sigma_rphiz if given
if exist('sigma_rphiz') && ~isempty(sigma_rphiz)
  ifinal = find(cocos_table(i_cocos_ok_1,i_sigma_rphiz)==sigma_rphiz);
  if ~isempty(ifinal)
    i_cocos_ok_1 = i_cocos_ok_1(ifinal);
  else
    status = -6;
    comments{end+1} = ['sigma_rphiz = ' num2str(sigma_rphiz) ' does not match'];
  end
end

cocos_out_1 = cocos_table(i_cocos_ok_1,i_cocos);
cocos_out = cocos_out_1;

if status == -1
  % no problems spotted so far
  status = length(cocos_out_1);
end
comments{end+1,1} = ['deduced values: sigma_bp = ' num2str(sigma_bp) ', sigma_rhothetaphi = ' num2str(sigma_rhothetaphi)];
comments{end+1,1} = ['deltastar_axis = ' num2str(deltastar_axis) ', rjphi_nopi = ' num2str(rjphi_nopi) ...
                   ', rjphi_4pi2 = ' num2str(rjphi_4pi2) ];

if exist('cocos_to_check') && ~isempty(cocos_to_check)
  ij= find(cocos_out_1==cocos_to_check);
  if ~isempty(ij)
    cocos_out(1) = cocos_out_1(ij);
    if length(cocos_out_1)>1
      cocos_out(2:length(cocos_out_1)) = setdiff([1:length(cocos_out_1)],ij);
    end
    if status>=0; status = 0; end
    comments{end+1,1} = ['cocos = ' num2str(cocos_to_check) ' is consistent with inputs'];
  end
end

if ~isnan(sigma_rhothetaphi_minusq)
  cocos_out_1_minusq = cocos_table(i_cocos_ok_1_minusq,i_cocos);
  comments{end+1,1} = [' assuming abs(q) was set and q is in fact negative, cocos is then ok with: ' num2str(cocos_out_1_minusq')];
end

% comment if ip, b0 are assumed clockwise or counter-clockwise in respective system
disp_clockwise = {'clockwise','anti-clockwise'};
for i=1:length(cocos_out)
  ip_clockwise = sigma_ip * cocos_opt{i}.sigma_RphiZ;
  b0_clockwise = sigma_b0 * cocos_opt{i}.sigma_RphiZ;
  comments{end+1} = ['with cocos=' num2str(cocos_out(i)) ' ip and b0 are ' disp_clockwise{round(1.5+ip_clockwise/2)} ' and ' ...
                      disp_clockwise{round(1.5+b0_clockwise/2)} ' resp. as seen from the top (incl. signs of ip, b0)'];
end

% final test if eqdsk
if i_eqdsk_input
  disp(' ')
  disp('================================================================')
  disp('       The final test for a given cocos and eqdsk file, please do:')
  disp('       >> eq=read_eqdsk(filename,cocos_value,1);')
  disp('       >> plot_eqdsk(eq,[],[],1);')
  disp('       Then left- and right-hand sides of the Grad-Shafranov equation should match')
  disp('================================================================')
end
