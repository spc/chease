function [chease_data] = eqdsk_ocols_to_chease_data(eqdsk_filename,ocols_filename,chease_mat_filename)
%
% [chease_data] = eqdsk_ocols_to_chease_data(eqdsk_filename,ocols_filename[,chease_mat_filename]);
%
% chease_data: structure needed by RAPTOR containing eqdsk data (eqdsk) and various profiles (profiledata)
% chease_mat_filename: (optional) if given and non empty, save chease_data in this file
%

% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

chease_data.eqdsk = [];
chease_data.profiledata = [];

% check inputs
eqdsk_file_stat=exist(eqdsk_filename,'file');
if eqdsk_file_stat==0
  disp(['Warning: file ' eqdsk_filename ' does not exist']);
  return
end

ocols_file_stat=exist(ocols_filename,'file');
if ocols_file_stat==0
  disp(['Warning: file ' ocols_filename ' does not exist']);
  return
end

% get data

% read ocols profile data
chease_data.profiledata = read_ocols(ocols_filename);

% read eqdsk data
cocos_in = 2; % assume eqdsk coming from standard chease cocos_out=2 (change chease namelist if not the case)
chease_data.eqdsk = read_eqdsk(eqdsk_filename,cocos_in);

% save in given filename if given
if exist('chease_mat_filename') && ~isempty(chease_mat_filename)
  eval(['save ' chease_mat_filename ' chease_data;']);

  disp(' ')
  disp(['saved chease_data structure into file: ' chease_mat_filename])
end
