function [equilibrium,wall] = eqdsk2ids(eqdsk_fname,time,cocos_in)
% Read eqdsk file into equilibrium and wall IDS
%
% function [equilibrium,wall] = eqdsk2ids(eqdsk_fname,time[,cocos_in])
%
% if fname is empty, prompts for it
% if fname is a structure, assume basic eqdsk structure
%
% See also: read_eqdsk, plot_eqdsk
%
% Author: A. Merle SPC-EPFL
% Created: 28.jan.2020

% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

assert(~isempty(which('ids_gen')),'No valid IMAS installation found');

equilibrium = ids_gen('equilibrium');

eqdsk = read_eqdsk(eqdsk_fname);

if ~exist('cocos_in','var') || isempty(cocos_in)
  cocos_in = eqdsk.cocos;
end

eqdsk = eqdsk_cocos_transform(eqdsk,[cocos_in, 11]);

% profiles_2d
profiles_2d = equilibrium.time_slice{1}.profiles_2d;
profiles_2d{1}.grid_type.index       = 1;
profiles_2d{1}.grid_type.name        = 'rectangular';
profiles_2d{1}.grid_type.description = 'Cylindrical R,Z ala eqdsk. In this case the position arrays should not be filled since they are redundant with grid/dim1 and dim2.';
profiles_2d{1}.grid.dim1             = eqdsk.rmesh;
profiles_2d{1}.grid.dim2             = eqdsk.zmesh;
profiles_2d{1}.psi                   = eqdsk.psi;
equilibrium.time_slice{1}.profiles_2d = profiles_2d;

% profiles_1d
profiles_1d = equilibrium.time_slice{1}.profiles_1d;
profiles_1d.f              = eqdsk.F;
profiles_1d.pressure       = eqdsk.p;
profiles_1d.f_df_dpsi      = eqdsk.FFprime;
profiles_1d.dpressure_dpsi = eqdsk.pprime;
profiles_1d.q              = eqdsk.q;
profiles_1d.psi            = eqdsk.psiaxis+(eqdsk.psiedge - eqdsk.psiaxis)*eqdsk.psimesh;
equilibrium.time_slice{1}.profiles_1d = profiles_1d;

% boundary
boundary = equilibrium.time_slice{1}.boundary;
boundary.outline.r = eqdsk.rplas;
boundary.outline.z = eqdsk.zplas;
equilibrium.time_slice{1}.boundary = boundary;

% global_quantities
global_quantities = equilibrium.time_slice{1}.global_quantities;
global_quantities.magnetic_axis.r = eqdsk.raxis;
global_quantities.magnetic_axis.z = eqdsk.zaxis;
global_quantities.psi_axis        = eqdsk.psiaxis;
global_quantities.psi_boundary    = eqdsk.psiedge;
global_quantities.ip              = eqdsk.ip;
equilibrium.time_slice{1}.global_quantities = global_quantities;

% vacuum_toroidal_field
equilibrium.vacuum_toroidal_field.r0 = eqdsk.r0;
equilibrium.vacuum_toroidal_field.b0 = eqdsk.b0;

equilibrium.time = time;
equilibrium.ids_properties.homogeneous_time = 1;
equilibrium.ids_properties.source  = sprintf('Generated from EQDSK by eqdsk2ids from file ''%s'' with cocos_in=%2d',eqdsk.fnamefull,cocos_in);
equilibrium.ids_properties.comment = sprintf('EQDSK Title: %s',eqdsk.stitle);
equilibrium.ids_properties.creation_date = datestr(now,'yyyy-mm-dd HH:MM:SS');

% Remove empty structures
equilibrium.grids_ggd = {};
equilibrium.time_slice{1}.boundary.x_point      = {};
equilibrium.time_slice{1}.boundary.strike_point = {};
equilibrium.time_slice{1}.boundary_separatrix.x_point      = {};
equilibrium.time_slice{1}.boundary_separatrix.strike_point = {};
equilibrium.time_slice{1}.constraints.bpol_probe             = {};
equilibrium.time_slice{1}.constraints.faraday_angle          = {};
equilibrium.time_slice{1}.constraints.mse_polarisation_angle = {};
equilibrium.time_slice{1}.constraints.flux_loop              = {};
equilibrium.time_slice{1}.constraints.iron_core_segment      = {};
equilibrium.time_slice{1}.constraints.n_e                    = {};
equilibrium.time_slice{1}.constraints.n_e_line               = {};
equilibrium.time_slice{1}.constraints.pf_current             = {};
equilibrium.time_slice{1}.constraints.pressure               = {};
equilibrium.time_slice{1}.constraints.q                      = {};
equilibrium.time_slice{1}.constraints.x_point                = {};
equilibrium.time_slice{1}.ggd = {};


if nargout > 1

  wall = ids_gen('wall');

  wall.description_2d{1}.limiter.type.index = 0;
  wall.description_2d{1}.limiter.unit{1}.outline.r = eqdsk.rlim;
  wall.description_2d{1}.limiter.unit{1}.outline.z = eqdsk.zlim;

  wall.ids_properties.homogeneous_time = 0;
  wall.ids_properties.source  = sprintf('Generated from EQDSK by eqdsk2ids from file ''%s'' with cocos_in=%2d',eqdsk.fnamefull,cocos_in);
  wall.ids_properties.comment = sprintf('EQDSK Title: %s',eqdsk.stitle);
  wall.ids_properties.creation_date = datestr(now,'yyyy-mm-dd HH:MM:SS');

  wall.description_2d{1}.mobile.unit = {};
  wall.description_2d{1}.vessel.unit = {};
  wall.description_ggd = {};

end

end
