function [h5all,h5allstruct,h5struct,h5struct_attribute,h5_top_attribute] = get_h5(filename_in);
%
% [h5all,h5allstruct,h5struct,h5struct_attribute,h5_top_attribute] = get_h5(filename_in);
%
% For futils h5 files (tested with ogyropsi.h5, hamada.h5, neoart.h5 created by chease_hdf5)
%

% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

aa=h5info(filename_in);

nbGroups_top=numel(aa.Groups);
if nbGroups_top > 0
  for ij=1:nbGroups_top
    nbgroups(ij)=numel(aa.Groups(ij).Groups);
    if nbgroups(ij) >= 1
      for ijk=1:nbgroups(ij)
        if ~isempty(aa.Groups(ij).Groups(ijk).Groups)
          keyboard
        end
        nbdatasets(ijk,ij) = numel(aa.Groups(ij).Groups(ijk).Datasets);
        for i_da=1:nbdatasets(ijk,ij)
          h5struct{ij}{ijk}(i_da).name = aa.Groups(ij).Groups(ijk).Datasets(i_da).Name;
          h5struct{ij}{ijk}(i_da).fullname =[aa.Groups(ij).Groups(ijk).Name '/' h5struct{ij}{ijk}(i_da).name];
          h5struct{ij}{ijk}(i_da).data = h5read(filename_in,h5struct{ij}{ijk}(i_da).fullname);
          h5allstruct.(h5struct{ij}{ijk}(i_da).name).label = h5struct{ij}{ijk}(i_da).name;
          h5allstruct.(h5struct{ij}{ijk}(i_da).name).data = h5struct{ij}{ijk}(i_da).data;
          h5all.(h5struct{ij}{ijk}(i_da).name) = h5struct{ij}{ijk}(i_da).data;
        end
        nb_sub_attributes(ijk,ij) = numel(aa.Groups(ij).Groups(ijk).Attributes);
        for i_subatt=1:nb_sub_attributes(ijk,ij)
          h5struct_attribute{ij}{ijk}(i_subatt).Name = aa.Groups(ij).Groups(ijk).Attributes(i_subatt).Name;
          h5struct_attribute{ij}{ijk}(i_subatt).Location = aa.Groups(ij).Groups(ijk).Name;
          ij_att=find(h5struct_attribute{ij}{ijk}(i_subatt).Name=='/');
          if isempty(ij_att), ij_att(1) = 0; end
          h5struct_attribute{ij}{ijk}(i_subatt).Value = h5readatt(filename_in,h5struct_attribute{ij}{ijk}(i_subatt).Location, ...
          h5struct_attribute{ij}{ijk}(i_subatt).Name(ij_att(end)+1:end));
        end
      end
    else
      h5struct{ij} = [];
      h5struct_attribute{ij} = [];
    end
    nb_topatasets(ij) = numel(aa.Groups(ij).Datasets);
    for i_da=1:nb_topatasets(ij)
      h5struct{ij}(i_da).name = aa.Groups(ij).Datasets(i_da).Name;
      h5struct{ij}(i_da).fullname =[aa.Groups(ij).Name '/' h5struct{ij}(i_da).name];
      h5struct{ij}(i_da).data = h5read(filename_in,h5struct{ij}(i_da).fullname);
      h5allstruct.(h5struct{ij}(i_da).name).label =  h5struct{ij}(i_da).name;
      h5allstruct.(h5struct{ij}(i_da).name).data = h5struct{ij}(i_da).data;
      h5all.(h5struct{ij}(i_da).name) = h5struct{ij}(i_da).data;
    end
    nb_top_attributes(ij) = numel(aa.Groups(ij).Attributes);
    for i_subatt=1:nb_top_attributes(ij)
      h5_top_attribute{ij}(i_subatt).Name = aa.Groups(ij).Attributes(i_subatt).Name;
      h5_top_attribute{ij}(i_subatt).Location = aa.Groups(ij).Name;
      ij_att=find(h5_top_attribute{ij}(i_subatt).Name=='/');
      if isempty(ij_att), ij_att(1) = 0; end
      h5_top_attribute{ij}(i_subatt).Value = h5readatt(filename_in, ...
          h5_top_attribute{ij}(i_subatt).Location,h5_top_attribute{ij}(i_subatt).Name(ij_att(end)+1:end));
    end
  end
end

% with ogyropsi.h5:
% contour(h5allstruct.rmesh.data,h5allstruct.zmesh.data,h5allstruct.psiRZ.data',100); axis equal
% contour(h5allstruct.rmesh.data,h5allstruct.zmesh.data,h5allstruct.chiRZ.data',100); axis equal
