function [fname_out,globalsvalues] = run_chease_expeq(namelistfile,expeqfile,varargin)
%
%function [fname_out,globalsvalues] = run_chease_expeq(namelistfile,expeqfile{,exptnzfile,suffix,nverbose})
%
% namelistfile: full path to CHEASE namelist
% expeqfile   : full path to EXPEQ file
% exptnzfile  : (optional) full path to EXPTNZ file
% suffix      : (optional) suffix to written files
% nverbose    : (optional) verbosity level
%
% fname_out   : cell containing full path of output files
% globalsvalues: structure containing main global results extracted from output: q0, qedge, q95, betaN, betap, li, kappa, delta, etc
%
% This function is now calling run_chease with correct syntax to avoid maintaining multiple 'run_chease' files.

% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

cocos_in = 2;
chease_exe = '';

switch nargin,
    case 5,
        args = [{namelistfile,expeqfile,cocos_in},varargin(1:2),{chease_exe},varargin(3)];
        % We have to make chease_exe a cell to avoid concatenation with nearby elements
    case {3,4}
        args = [{namelistfile,expeqfile,cocos_in},varargin(1:nargin-2)];
    case 2
        args = {namelistfile,expeqfile};
    otherwise
        error('nargin = %d not supported in %s',nargin,mfilename);
end

if isnumeric(namelistfile) || isempty(namelistfile),
    args{1} = 1; % Override setting for expeq file input
end

[fname_out,globalsvalues] = run_chease(args{:});
