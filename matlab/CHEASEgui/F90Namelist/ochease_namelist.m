% from namelist: par
i=0; ;
i=i+1;namelist.varnames{i}='afbs';
namelist.afbs= [0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00] ;
i=i+1;namelist.varnames{i}='afbs2';
namelist.afbs2= [0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00] ;
i=i+1;namelist.varnames{i}='ap';
namelist.ap= [0.000000000000000000E+00,-0.800000000000000044,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00] ;
i=i+1;namelist.varnames{i}='aplace';
namelist.aplace= [0.000000000000000000E+00,0.699999999999999956,1.00000000000000000,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00] ;
i=i+1;namelist.varnames{i}='ap2';
namelist.ap2= [0.100000000000000006,0.500000000000000000,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00] ;
i=i+1;namelist.varnames{i}='aspct';
namelist.aspct= 0.320170000000000010 ;
i=i+1;namelist.varnames{i}='at';
namelist.at= [0.000000000000000000E+00,-3.07615360000000004,0.723183569999999998,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00] ;
i=i+1;namelist.varnames{i}='at2';
namelist.at2= [1.51649999999999996,0.141889999999999988,-5.04169999999999963,36.7590000000000003,-121.109999999999999,200.379999999999995,-162.229999999999990,51.1520000000000010,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00] ;
i=i+1;namelist.varnames{i}='at3';
namelist.at3= [0.525029999999999997,0.927540000000000031,0.218959999999999988,-2.40779999999999994,8.12110000000000021,-13.8699999999999992,11.6530000000000005,-3.79420000000000002,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00] ;
i=i+1;namelist.varnames{i}='at4';
namelist.at4= [29500.0000000000000,-68768.0000000000000,272720.000000000000,-1147400.00000000000,2798300.00000000000,-3873600.00000000000,2842600.00000000000,-852840.000000000000,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00] ;
i=i+1;namelist.varnames{i}='awidth';
namelist.awidth= [0.500000000000000028E-01,0.700000000000000067E-01,0.500000000000000028E-01,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00] ;
i=i+1;namelist.varnames{i}='beans';
namelist.beans= 0.000000000000000000E+00 ;
i=i+1;namelist.varnames{i}='bentaxis';
namelist.bentaxis= 1.01499999999999990 ;
i=i+1;namelist.varnames{i}='bentqprofile';
namelist.bentqprofile= 0.000000000000000000E+00 ;
i=i+1;namelist.varnames{i}='bentradius';
namelist.bentradius= 0.500000000000000000 ;
i=i+1;namelist.varnames{i}='bplace';
namelist.bplace= [0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00] ;
i=i+1;namelist.varnames{i}='bsfrac';
namelist.bsfrac= 0.500000000000000000 ;
i=i+1;namelist.varnames{i}='bwidth';
namelist.bwidth= [0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00] ;
i=i+1;namelist.varnames{i}='b0exp';
namelist.b0exp= 1.08000000000000007 ;
i=i+1;namelist.varnames{i}='ceta';
namelist.ceta= 0.239999999999999991 ;
i=i+1;namelist.varnames{i}='cfbal';
namelist.cfbal= 10.0000000000000000 ;
i=i+1;namelist.varnames{i}='cfnress';
namelist.cfnress= 1.00000000000000000 ;
i=i+1;namelist.varnames{i}='cfnresso';
namelist.cfnresso= 1.00000000000000000 ;
i=i+1;namelist.varnames{i}='cplace';
namelist.cplace= [0.949999999999999956,0.989999999999999991,1.00000000000000000,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00] ;
i=i+1;namelist.varnames{i}='cpress';
namelist.cpress= 1.00000000000000000 ;
i=i+1;namelist.varnames{i}='cpresso';
namelist.cpresso= 1.00000000000000000 ;
i=i+1;namelist.varnames{i}='cq0';
namelist.cq0= 0.750000000000000000 ;
i=i+1;namelist.varnames{i}='csspec';
namelist.csspec= 0.000000000000000000E+00 ;
i=i+1;namelist.varnames{i}='currt';
namelist.currt= 0.520000000000000018 ;
i=i+1;namelist.varnames{i}='cwidth';
namelist.cwidth= [0.100000000000000006,0.200000000000000004E-01,0.500000000000000028E-01,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00] ;
i=i+1;namelist.varnames{i}='delta';
namelist.delta= 0.000000000000000000E+00 ;
i=i+1;namelist.varnames{i}='dplace';
namelist.dplace= [-1.80000000000000004,-1.80000000000000004,4.00000000000000000,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00] ;
i=i+1;namelist.varnames{i}='dwidth';
namelist.dwidth= [0.179999999999999993,0.800000000000000017E-01,0.500000000000000028E-01,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00] ;
i=i+1;namelist.varnames{i}='elong';
namelist.elong= 2.04999999999999982 ;
i=i+1;namelist.varnames{i}='eplace';
namelist.eplace= [-1.69999999999999996,-1.69999999999999996,1.69999999999999996,1.69999999999999996,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00] ;
i=i+1;namelist.varnames{i}='etaei';
namelist.etaei= 0.100000000000000006 ;
i=i+1;namelist.varnames{i}='ewidth';
namelist.ewidth= [0.179999999999999993,0.800000000000000017E-01,0.179999999999999993,0.800000000000000017E-01,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00] ;
i=i+1;namelist.varnames{i}='epslon';
namelist.epslon= 0.100000000000000002E-07 ;
i=i+1;namelist.varnames{i}='gamma';
namelist.gamma= 1.66666666670000008 ;
i=i+1;namelist.varnames{i}='pangle';
namelist.pangle= 0.000000000000000000E+00 ;
i=i+1;namelist.varnames{i}='predge';
namelist.predge= 0.999999999999999955E-06 ;
i=i+1;namelist.varnames{i}='psiscl';
namelist.psiscl= 1.00000000000000000 ;
i=i+1;namelist.varnames{i}='qspec';
namelist.qspec= 0.900000000000000022 ;
i=i+1;namelist.varnames{i}='qplace';
namelist.qplace= [1.00000000000000000,1.00000000000000000,2.00000000000000000,2.00000000000000000,3.00000000000000000,3.00000000000000000,4.00000000000000000,4.00000000000000000,4.41000000000000014,4.41000000000000014] ;
i=i+1;namelist.varnames{i}='qwidth';
namelist.qwidth= [0.130000000000000004,0.400000000000000008E-01,0.899999999999999967E-01,0.400000000000000008E-01,0.700000000000000067E-01,0.200000000000000004E-01,0.400000000000000008E-01,0.100000000000000002E-01,0.100000000000000002E-01,0.100000000000000002E-02] ;
i=i+1;namelist.varnames{i}='qvalneo';
namelist.qvalneo= [0.000000000000000000E+00,1.00000000000000000,1.50000000000000000,2.00000000000000000,3.00000000000000000,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00,0.000000000000000000E+00] ;
i=i+1;namelist.varnames{i}='rboxlen';
namelist.rboxlen= -1.00000000000000000 ;
i=i+1;namelist.varnames{i}='rboxlft';
namelist.rboxlft= -1.00000000000000000 ;
i=i+1;namelist.varnames{i}='rc';
namelist.rc= 1.00000000000000000 ;
i=i+1;namelist.varnames{i}='relax';
namelist.relax= 0.000000000000000000E+00 ;
i=i+1;namelist.varnames{i}='rext';
namelist.rext= 10.0000000000000000 ;
i=i+1;namelist.varnames{i}='rnu';
namelist.rnu= 0.000000000000000000E+00 ;
i=i+1;namelist.varnames{i}='rpeop';
namelist.rpeop= 0.500000000000000000 ;
i=i+1;namelist.varnames{i}='rzion';
namelist.rzion= 1.50000000000000000 ;
i=i+1;namelist.varnames{i}='rz0';
namelist.rz0= 0.724920000000000009E-01 ;
i=i+1;namelist.varnames{i}='rz0w';
namelist.rz0w= 0.000000000000000000E+00 ;
i=i+1;namelist.varnames{i}='r0';
namelist.r0= 1.00000000000000000 ;
i=i+1;namelist.varnames{i}='r0exp';
namelist.r0exp= 2.89999999999999991 ;
i=i+1;namelist.varnames{i}='r0w';
namelist.r0w= 1.00000000000000000 ;
i=i+1;namelist.varnames{i}='scalne';
namelist.scalne= 0.000000000000000000E+00 ;
i=i+1;namelist.varnames{i}='scexp';
namelist.scexp= 1.00000000000000000 ;
i=i+1;namelist.varnames{i}='signb0xp';
namelist.signb0xp= 1.00000000000000000 ;
i=i+1;namelist.varnames{i}='signipxp';
namelist.signipxp= 1.00000000000000000 ;
i=i+1;namelist.varnames{i}='sgma';
namelist.sgma= 0.000000000000000000E+00 ;
i=i+1;namelist.varnames{i}='slimit';
namelist.slimit= 100000.000000000000 ;
i=i+1;namelist.varnames{i}='snumber';
namelist.snumber= 10000000.0000000000 ;
i=i+1;namelist.varnames{i}='solpda';
namelist.solpda= 0.100000000000000006 ;
i=i+1;namelist.varnames{i}='solpdb';
namelist.solpdb= 0.000000000000000000E+00 ;
i=i+1;namelist.varnames{i}='solpdc';
namelist.solpdc= 0.699999999999999956 ;
i=i+1;namelist.varnames{i}='solpdd';
namelist.solpdd= 0.599999999999999978 ;
i=i+1;namelist.varnames{i}='solpde';
namelist.solpde= 0.500000000000000000 ;
i=i+1;namelist.varnames{i}='spitzer';
namelist.spitzer= 0.000000000000000000E+00 ;
i=i+1;namelist.varnames{i}='theta0';
namelist.theta0= 0.000000000000000000E+00 ;
i=i+1;namelist.varnames{i}='treeitm';
namelist.treeitm= ['euitm','euitm'] ;
i=i+1;namelist.varnames{i}='triang';
namelist.triang= 0.699999999999999956 ;
i=i+1;namelist.varnames{i}='triplt';
namelist.triplt= 0.000000000000000000E+00 ;
i=i+1;namelist.varnames{i}='xi';
namelist.xi= 0.000000000000000000E+00 ;
i=i+1;namelist.varnames{i}='zboxlen';
namelist.zboxlen= 1.50000000000000000 ;
i=i+1;namelist.varnames{i}='msmax';
namelist.msmax= 1 ;
i=i+1;namelist.varnames{i}='nanal';
namelist.nanal= 0 ;
i=i+1;namelist.varnames{i}='nbal';
namelist.nbal= 0 ;
i=i+1;namelist.varnames{i}='nblc0';
namelist.nblc0= 16 ;
i=i+1;namelist.varnames{i}='nblopt';
namelist.nblopt= 0 ;
i=i+1;namelist.varnames{i}='nbpsout';
namelist.nbpsout= 300 ;
i=i+1;namelist.varnames{i}='nbsexpq';
namelist.nbsexpq= 0 ;
i=i+1;namelist.varnames{i}='nbsfun';
namelist.nbsfun= 1 ;
i=i+1;namelist.varnames{i}='nbsopt';
namelist.nbsopt= 0 ;
i=i+1;namelist.varnames{i}='nbstrp';
namelist.nbstrp= 1 ;
i=i+1;namelist.varnames{i}='nchi';
namelist.nchi= 80 ;
i=i+1;namelist.varnames{i}='ncscal';
namelist.ncscal= 2 ;
i=i+1;namelist.varnames{i}='ndiagop';
namelist.ndiagop= 1 ;
i=i+1;namelist.varnames{i}='ndifps';
namelist.ndifps= 0 ;
i=i+1;namelist.varnames{i}='ndift';
namelist.ndift= 1 ;
i=i+1;namelist.varnames{i}='negp';
namelist.negp= -1 ;
i=i+1;namelist.varnames{i}='neonbqs';
namelist.neonbqs= 6 ;
i=i+1;namelist.varnames{i}='neqdsk';
namelist.neqdsk= 0 ;
i=i+1;namelist.varnames{i}='ner';
namelist.ner= 1 ;
i=i+1;namelist.varnames{i}='nitmopt';
namelist.nitmopt= 0 ;
i=i+1;namelist.varnames{i}='nitmrun';
namelist.nitmrun= [1,1] ;
i=i+1;namelist.varnames{i}='nitmshot';
namelist.nitmshot= [180,170] ;
i=i+1;namelist.varnames{i}='nfftopt';
namelist.nfftopt= 1 ;
i=i+1;namelist.varnames{i}='nfunc';
namelist.nfunc= 4 ;
i=i+1;namelist.varnames{i}='nfunrho';
namelist.nfunrho= 0 ;
i=i+1;namelist.varnames{i}='nideal';
namelist.nideal= 6 ;
i=i+1;namelist.varnames{i}='ninmap';
namelist.ninmap= 40 ;
i=i+1;namelist.varnames{i}='ninsca';
namelist.ninsca= 40 ;
i=i+1;namelist.varnames{i}='nipr';
namelist.nipr= 1 ;
i=i+1;namelist.varnames{i}='niso';
namelist.niso= 80 ;
i=i+1;namelist.varnames{i}='nmesha';
namelist.nmesha= 0 ;
i=i+1;namelist.varnames{i}='nmeshb';
namelist.nmeshb= 0 ;
i=i+1;namelist.varnames{i}='nmeshc';
namelist.nmeshc= 1 ;
i=i+1;namelist.varnames{i}='nmeshd';
namelist.nmeshd= 1 ;
i=i+1;namelist.varnames{i}='nmeshe';
namelist.nmeshe= 0 ;
i=i+1;namelist.varnames{i}='nmgaus';
namelist.nmgaus= 4 ;
i=i+1;namelist.varnames{i}='nopt';
namelist.nopt= 0 ;
i=i+1;namelist.varnames{i}='nplot';
namelist.nplot= 1 ;
i=i+1;namelist.varnames{i}='npoida';
namelist.npoida= 1 ;
i=i+1;namelist.varnames{i}='npoidb';
namelist.npoidb= 0 ;
i=i+1;namelist.varnames{i}='npoidc';
namelist.npoidc= 2 ;
i=i+1;namelist.varnames{i}='npoidd';
namelist.npoidd= 2 ;
i=i+1;namelist.varnames{i}='npoide';
namelist.npoide= 4 ;
i=i+1;namelist.varnames{i}='npoidq';
namelist.npoidq= 10 ;
i=i+1;namelist.varnames{i}='npp';
namelist.npp= 1 ;
i=i+1;namelist.varnames{i}='nppfun';
namelist.nppfun= 4 ;
i=i+1;namelist.varnames{i}='nppr';
namelist.nppr= 24 ;
i=i+1;namelist.varnames{i}='nprofz';
namelist.nprofz= 0 ;
i=i+1;namelist.varnames{i}='npropt';
namelist.npropt= 1 ;
i=i+1;namelist.varnames{i}='nprpsi';
namelist.nprpsi= 0 ;
i=i+1;namelist.varnames{i}='npsi';
namelist.npsi= 80 ;
i=i+1;namelist.varnames{i}='nrbox';
namelist.nrbox= 33 ;
i=i+1;namelist.varnames{i}='nrfp';
namelist.nrfp= 0 ;
i=i+1;namelist.varnames{i}='nrscal';
namelist.nrscal= 0 ;
i=i+1;namelist.varnames{i}='ns';
namelist.ns= 20 ;
i=i+1;namelist.varnames{i}='nsgaus';
namelist.nsgaus= 4 ;
i=i+1;namelist.varnames{i}='nsmooth';
namelist.nsmooth= 1 ;
i=i+1;namelist.varnames{i}='nsour';
namelist.nsour= 8 ;
i=i+1;namelist.varnames{i}='nsttp';
namelist.nsttp= 1 ;
i=i+1;namelist.varnames{i}='nsurf';
namelist.nsurf= 6 ;
i=i+1;namelist.varnames{i}='nsym';
namelist.nsym= 0 ;
i=i+1;namelist.varnames{i}='nt';
namelist.nt= 20 ;
i=i+1;namelist.varnames{i}='ntcase';
namelist.ntcase= 0 ;
i=i+1;namelist.varnames{i}='ntgaus';
namelist.ntgaus= 4 ;
i=i+1;namelist.varnames{i}='ntest';
namelist.ntest= 0 ;
i=i+1;namelist.varnames{i}='ntmf0';
namelist.ntmf0= 0 ;
i=i+1;namelist.varnames{i}='ntnova';
namelist.ntnova= 12 ;
i=i+1;namelist.varnames{i}='nturn';
namelist.nturn= 0 ;
i=i+1;namelist.varnames{i}='nv';
namelist.nv= 40 ;
i=i+1;namelist.varnames{i}='nvexp';
namelist.nvexp= 1 ;
i=i+1;namelist.varnames{i}='nzbox';
namelist.nzbox= 65 ;
i=i+1;namelist.varnames{i}='neqdxtpo';
namelist.neqdxtpo= 1 ;
i=i+1;namelist.varnames{i}='comments';
namelist.comments= ['***               EXPEQ_50555t63.95','***','***NEGP=-1,NER= 1  TCV case with ECRH','*** NSURF=6,NEQDSK=0,  NEQDXTPO=-1'] ;
% end of namelist
varnames=namelist.varnames;
namelist=rmfield(namelist,'varnames');
varsorted=sort(fields(namelist))';
namelist.varnames=varnames;
namelist.varsorted=varsorted;

