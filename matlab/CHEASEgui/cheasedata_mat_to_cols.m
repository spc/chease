% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

chease_data_filename = 'ITER_hybrid_citrin_equil_cheasedata.mat';

aa=load(chease_data_filename);

fid=fopen([chease_data_filename '2cols'],'w');
fprintf(fid,'%%');
fprintf(fid,' %s',aa.chease_data.labels{:});
fprintf(fid,'\n');
for i=1:size(aa.chease_data.data,1)
  fprintf(fid,'%13.5e ',aa.chease_data.data(i,:)');
  fprintf(fid,'\n');
end
fclose(fid);
