% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

>> mcc -m CHEASEgui.m

Then gets CHEASEgui, readme.txt, run_CHEASEgui.sh

running
./CHEASEgui

gets
CHEASEgui: error while loading shared libraries: libmwlaunchermain.so: cannot open shared object file: No such file or directory
./CHEASEgui: error while loading shared libraries: libmwmclmcrrt.so.8.3: cannot open shared object file: No such file or directory

Found these in: (thus change LD_LIBRARY_PATH (easiest or launch with sh script and give LD_LIB_path)

LD_LIBRARY_PATH=$LD_LIBRARY_PATH':/usr/local/MATLAB/R2014a/bin/glnxa64:/usr/local/MATLAB/R2014a/runtime/glnxa64'
(I was using matlab from /usr/local/MATLAB/R2014a/bin/matlab)

Now compiled CHEASEgui works but external files not known, need to add them in mcc or use deploytool I guess

try now: (in files_...)

mcc -m CHEASEgui.m customize_options.m extractdatachease.m plotdatafile.m read_namelist.m run_chease_expeq.m syscode_formulas.m write_namelist_chease.m read_namelist_chease.m read_ocols.m run_chease.m write_expeq.m -a customize_list_ITM.data -a default_namelist_chease

now works correctly, but need further files.m

So try within CHEASEgui simply:

mcc -m CHEASEgui.m astra2chease_equil.m eqdsk2expeq.m forLiuqe_fromCHEASE.m plotdatafile.m quadratic_profile.m read_ocols.m syscode_formulas.m write_namelist_kinx.m astra2expeq.m eqdsk_cocos_transform.m gethdf5.m plot_eqdsk.m read_eqdsk_addw07.m read_pest3_output.m chease_demo.m eqdsk_maker.m interpos2Dcartesian.m plot_expeq.m read_eqdsk.m run_chease_expeq.m write_eqdsk.m eqdsk_runchease_AUG.m kinetic_CHEASE_TCV.m plot_exptnz.m read_expeq.m run_chease.m write_expeq.m cocos.m eqdsk_to_fluxtheta_map.m linear_profile.m plot_ocols.m read_exptnz.m run_pest3.m write_exptnz.m cubic_profile.m eqdsk_transform.m plasma_boundary_global_params.m plotos.m read_namelist_chease.m set_colos.m write_namelist_caxe.m customize_options.m extractdatachease.m plot_chease.m plot_pest3_output.m read_namelist.m set_defaults_matlab.m write_namelist_chease.m -a chease_expeq_default_namelist -a customize_list_ITM.data -a default_namelist_chease_eqdsk -a default_namelist_chease_nofiles -a default_namelist_chease -a default_namelist_chease_expeq -d CHEASEgui_build

cd CHEASEgui_build/
