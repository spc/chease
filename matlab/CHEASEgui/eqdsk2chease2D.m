function [hmode_distance,hmode_distance_hedge,nuplo_data,eqdsk_out,fname_out_in,globalsvalues_in, ...
          fname_out_hedge,globalsvalues_hedge] = eqdsk2chease2D(eqdsk_in,varargin);
%
%  [hmode_distance,hmode_distance_hedge,nuplo_data,eqdsk_out,fname_out_in,globalsvalues_in, ...
%          fname_out_hedge,globalsvalues_hedge] = eqdsk2chease2D(eqdsk_in);
%
% get eqdsk_in as (array of) eqdsk structure or read from (array of cell) eqdsk_in filenames (using find_cocos(1))
% run chease from: [fname_out_in,globalsvalues_in] = run_chease(2,aa.eqdsk,aa.eqdsk.cocos);
% Get 2D plot from: [hmode_distance,nuplo_data] = nuplo_2d_plots(fname_out_in{contains(fname_out_in, 'NUPLO')},[num2str(shot) '@' num2str(time)]);
%
% varargin{1}: shot(s)
% varargin{2}: time(s)
% varargin{3}: cell array(s) of sufiix to used for labels for eqch eqdsk
%

% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

if nargin==0 || isempty(eqdsk_in)
  error('requires eqdsk_in as eqdsk structure or eqdskfilename')
elseif ischar(eqdsk_in)
  eqdsk_in = {eqdsk_in};
end

suffix = '';
psi_surface = 0.90;
for i=1:numel(eqdsk_in)
  if isstruct(eqdsk_in)
    eqdsk_out(i) = eqdsk_in(i);
  else
    cocos_in = find_cocos(eqdsk_in{i});
    eqdsk_out(i) = read_eqdsk(eqdsk_in{i},cocos_in(1));
  end
  if nargin>3 && ~isempty(varargin{3})
    suffix = varargin{3}{i};
  elseif nargin==3
    if numel(varargin{1})>=i && numel(varargin{2})>=i
      suffix = [num2str(varargin{1}(i)) '@' num2str(varargin{2}(i))];
    end
  elseif nargin==2
    if numel(varargin{1})>=i
      suffix = [num2str(varargin{1}(i))];
    end
  end

  [fname_out_in{i},globalsvalues_in(i)] = run_chease(2,eqdsk_out(i),eqdsk_out(i).cocos);
  [hmode_distance(i),nuplo_data(i)] = nuplo_2d_plots(fname_out_in{i}{contains(fname_out_in{i}, 'NUPLO')},suffix,'35',psi_surface);

  expeq_out0 = regexp(fname_out_in{i},'EXPEQ.OUT$');
  expeq_out0 = fname_out_in{i}{find(cellfun(@(x) ~isempty(x), expeq_out0))};
  exp1 = read_expeq(expeq_out0);
  amp_pp = 1.1;
  rhodep_pp = 0.6;
  wdep_pp = 0.015;
  amp_j = 0.4;
  rhodep_j = 0.75;
  wdep_j = 0.015;
  frac_johm = 0.8;
  [~,~,namelist_struct_expeq] = run_chease(1);
  ppexp=-amp_pp.*exp(-(exp1.rho-rhodep_pp).^2./wdep_pp^2);
  ppexp=-amp_pp.*(1+tanh((exp1.rho-rhodep_pp)./wdep_pp));
  exp2 = exp1;
  exp2.Pprime=exp1.Pprime+ppexp;
  jbsexp=amp_j.*exp(-(exp1.rho-rhodep_j).^2./wdep_j^2);
  jbsexp=amp_j.*(1+tanh((exp1.rho-rhodep_j)./wdep_j));
  exp2.Istar=frac_johm.*exp1.Istar+jbsexp;
  namelist_struct_expeq = namelist_struct_expeq;
  namelist_struct_expeq.ncscal = 2;
  namelist_struct_expeq.currt = globalsvalues_in.ipchease;
  namelist_struct_expeq.r0exp = globalsvalues_in.r0exp;
  namelist_struct_expeq.b0exp = globalsvalues_in.b0exp;
  [fname_out_hedge{i},globalsvalues_hedge(i),namelist_struct_hedge,namelistfile_eff_hedge] = run_chease(namelist_struct_expeq,exp2);
  [hmode_distance_hedge(i),nuplo_data_hedge(i)] = nuplo_2d_plots(fname_out_hedge{i}{find(contains(fname_out_hedge{i},'NUPLO'))}, ...
          [suffix '_hedge'],'35',psi_surface);

end
