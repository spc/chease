function plot_xharmr_KX();
% Script to plot KINX results : resonant harmonics
% -------------------------------------------------------------
% Will read the files:
% 'xharms.bsp'  : harmonics as function of radial coordinate
% 'st.bsp'      : informations about KINX run
% -------------------------------------------------------------
% A. Pitzschke, CRPP, 29.01.2009

% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

user_entry=questdlg(' armonic numbers  yes or no?', 'Number armonics', 'y', 'n', 'y');
figure;
hf=gcf;



%%% read the formatted file

% file names
fi='xharms.bsp';
st='st.bsp';
%
fid2=fopen(fi,'r');
    % dimensions
%    nn = fscanf(fid2,'%d',[3 1]);
    snn = fgetl(fid2);
    nn = str2num(snn);
    if(size(nn,2)==3)
        ns = nn(2);
        nt = 2*nn(1)+1;
        nnh = nn(3);
        n = [nt ns-1];
    elseif(size(nn,2)==2)
        disp('old harmonic file format');
        ns = nn(2);
        nt = 2*nn(1)+1;
        nnh = nt;
        n = [nt ns-1];
    else
        disp('wrong harmonic file format');
        return;
    end
    % function
    fff=zeros(ns,nnh);
    % numbers
    nnn=zeros(nnh,1);
    % radial
    sss=zeros(ns,1);
    sss = fscanf(fid2,'%g',[ns 1]);
    for nh=1:nnh,
        nnn(nh) = fscanf(fid2,'%d',[1 1]);
        fff(:,nh) = fscanf(fid2,'%g',[ns 1]);
    end
    qqq=zeros(ns,1);
    qqq = fscanf(fid2,'%g',[ns 1]);
fclose(fid2);

fid3=fopen(st,'r');
    % toroidal wave number
    nw = fscanf(fid3,'%d',1);
    % eigenvalue
    alnorm = fscanf(fid3,'%g',1);
    % negatives and initial guess
    neg = fscanf(fid3,'%d',1);
    al0 = fscanf(fid3,'%g',1);
    % dw index
    idw = fscanf(fid3,'%d',1);
    % iteration count
    it2 = fscanf(fid3,'%d',2);
fclose(fid3);

% nnhc = 1;
% %%% all harmonics: nnhd = nnh-1;
% nnhd = (nnh-1)/1;
% nnh2 = nnhd+1;
% fff2 = fff(:,nnhc:nnhc+nnhd);
% nnn2 = nnn(nnhc:nnhc+nnhd);
% clear nnn fff;

% resonant poloidal numbers
li=floor(min(qqq)*nw)+1;
la=floor(max(qqq)*nw);
nnh2=0;
clear nnn2;
fff2 = zeros(ns,1);
for nh=1:nnh,
%%%    if(nnn(nh)<=la & nnn(nh)>=li)
    if(nnn(nh)>=li)
        nnh2 = nnh2 + 1;
        fff2(:,nnh2) = fff(:,nh);
        nnn2(nnh2) = nnn(nh);
    end
end
clear nnn fff;

disp(sprintf('%d %d min max resonant poloidal',li,la))
disp(sprintf('resonant and surface %d min max %d %d to plot',nnh2,min(nnn2),max(nnn2)))

%%fff2 = fff2./max(max(abs(fff2)));
%%%plot(sss,fff2)
ccc = get(gca,'ColorOrder');
ncc = size(ccc);
nc1 = ncc(1);
hold on;
for nh=1:nnh2,
    imax=find(abs(fff2(:,nh))==max(abs(fff2(:,nh))));
    nh2 = abs(nnn2(nh));
    ncl = nh2-nc1*floor(nh2/nc1)+1;
    plot(sss,fff2(:,nh), 'Color', ccc(ncl,:));
end

xlabel('sqrt(\psi)');
ylabel('\xi \cdot \nabla \psi');

if( (neg==0) | ((neg==1)&(al0>=alnorm)) )
    status = 'lowest eigenvalue';
else
    status = [int2str(neg) ' eigenvalues less than ' num2str(al0) ];
end
if( idw==0 )
    norm = 'kinetic energy norm';
else
    norm = '\delta W norm';
end
if( (alnorm<0) )
    stable = ' unstable eq.';
elseif ( (al0<0) & (neg>0) )
    stable = 'unstable eq.';
elseif( (alnorm>0) & ((neg==0) | ((neg==1)&(al0>=alnorm))) )
    stable = 'stable eq.';
    if(it2(1)==it2(2))
        stable = 'eigenvalue accuracy? change initial guess';
    end
else
    stable = 'stability of eq. unknown';
end
title(strvcat( ['Normal displacement (SFL harmonics) of n=' int2str(nw) ' mode, mesh=' int2str(n(2)) '\times' int2str(n(1)-1)],' ')...
    ,'VerticalAlignment','bottom');
xlabel(['sqrt(\psi);    \omega^2/\omega_A^2=' num2str(alnorm) '; ' norm  '; ' stable '; '  status]);
axis tight

% plot q
set(gca,'Color','none','PlotBoxAspectRatio',[4 3 3]);
%%%ax1 = gca;
eval(['ax1_' num2str(gcf) ' = gca;']);
%%%ax2 = axes('Position',get(ax1,'Position'),'YAxisLocation','right','XTick',[],'FontSize',14);
eval(['ax2_' num2str(gcf) ...
    ' = axes(''Position'',get(ax1_' num2str(gcf) ...
    ',''Position''), ''PlotBoxAspectRatio'',[4 3 3], ''XAxisLocation'',''top'',''YAxisLocation'',''right'');']);
%%%    ',''Position''), ''PlotBoxAspectRatio'',[4 3 3], ''XAxisLocation'',''top'',''YAxisLocation'',''right'');']);
hold on
eval(['axes(ax2_' num2str(gcf) ')']);
%%%line(sss,qqq,'Color','k','Parent',ax2,'LineWidth',2);
line(sss,qqq,'Color','k','LineWidth',2);
ylabel('q');
%%%ylabel('q');
axis tight

eval(['axes(ax1_' num2str(gcf) ')']);
%%%axes(ax1);

%user_entry = input('harmonic numbers y/n? ','s');

if(user_entry == 'y')
    for nh=1:nnh2,
        imax=find(abs(fff2(:,nh))==max(abs(fff2(:,nh))));
        %%%ncl = (nh-1)-nc1*floor((nh-1)/nc1)+1;
        nh2 = abs(nnn2(nh));
        ncl = nh2-nc1*floor(nh2/nc1)+1;
        text(sss(imax), fff2( imax , nh) ,num2str(nnn2(nh)), 'Color', ccc(ncl,:));
    end
end

figure(gcf);
