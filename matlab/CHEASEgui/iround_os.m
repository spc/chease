function  [is,sigi]=iround_os(sig,val)
%
% Given two arrays sig and val, for each element in val
% returns the index and value of the nearest element in sig.
%
% sig and/or val can be non-monotonic (contrary to TCV iround which requires sig to be monotonic)
%
% Example:
%
% >> [i,sigi]=iround_os(sig,val);
%

% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

is = zeros(size(val));
for j=1:numel(val)
  [~,is(j)]=min(abs(sig-val(j)));
end
sigi=sig(is);
