function [ocols, expeqs, handles_nblopt_plot] = nblopt_plot(ocols_files,expeqout_files,suffix_titles);
%
% [ocols, expeqs, handles_nblopt_plot] = nblopt_plot(ocols_files,expeqout_files,suffix_titles);
%
% Assume 1st index is reference expeq and ocols
% suffix_titles used for titles of figures, optional
%

% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

set_defaults_matlab

if ~exist('expeqout_files') || numel(expeqout_files)<1
  warning('inputs empty?, no expeq cannot do much');
  return
end

if ~exist('suffix_titles') || isempty(suffix_titles)
  [a_path, a_name, a_extension] = fileparts(expeqout_files{1});
  if ~isempty(a_extension) && numel(a_extension)>=5
    suffix_titles = strrep(a_extension(2:end),'OUT','');
  else
    suffix_titles = strrep([a_name a_extension],'EXPEQ.OUT','');
  end
end
suffix_titles = strrep(suffix_titles,'_','\_');

ocols{1} = plotdatafile(ocols_files{1},0); % assumes 1st is fine and is ref
[a1,a2,a3]=fileparts(ocols_files{1});
leg{1} = a2(min(round(numel(a2)/2)-1,10):numel(a2));
for i=2:numel(ocols_files)
  try
    ocols{end+1} = plotdatafile(ocols_files{i},0);
    [a1,a2,a3]=fileparts(ocols_files{i});
    leg{end+1} = a2(min(round(numel(a2)/2)-1,10):end);
  end
end

%%
expeqs{1} = plot_expeq(expeqout_files{1});
for j=2:numel(expeqout_files)
  try
    expeqs{end+1} = plot_expeq(gcf,expeqout_files{j});
  end
end
%%

irhopol = 1;
ipprime = 7;
iq = 8;
ishear = 10;
iistar = 11;
ialpha = 57;
incbal = 88;
% alpha= - 2μ0 dV/dpsi 1/(2 pi)^2 (V / (2 pi^2 R_0))^(1/2)  dp/dpsi
% alphaGroebner = -dp/dpsi * dV/dpsi /2/pi**2 * sqrt(V/2pi**2/Rgeom)

%%
ix_to_plot = [irhopol, irhopol, irhopol, irhopol, irhopol, ialpha];
iy_to_plot = [iistar, iq, ishear, ialpha, ipprime, ishear];
leg = cellstr(num2str([1:numel(ocols)]'));

n_fig = 1;
for j=1:numel(iy_to_plot)
  ix = ix_to_plot(j);
  iy = iy_to_plot(j);
  %%
  handles_nblopt_plot.fig{2} = figure;
  for i=1:numel(ocols)
    handles_nblopt_plot.plot{i,n_fig} = plot(ocols{i}.pdata_in(:,ix),ocols{i}.pdata_in(:,iy));
    hold on
  end
  xlabel(ocols{i}.labels{ix})
  ylabel(ocols{i}.labels{iy})
  handles_nblopt_plot.leg{n_fig} = legend(leg);
  %%
end
handles_nblopt_plot.title{n_fig} = title([suffix_titles]);

%% Add shear vs alpha stable/unstable
ix = ialpha;
iy = ishear;
n_fig = n_fig + 1;
handles_nblopt_plot.fig{n_fig} = figure;
for i=1:numel(ocols)
  iunstable = [ocols{i}.pdata_in(:,incbal) ~= 0];
  aa = ocols{i}.pdata_in(:,ix);
  aa(iunstable) = NaN;
  handles_nblopt_plot.plot{i,n_fig} = plotos(aa,ocols{i}.pdata_in(:,iy),'*-',[2 5],0,colos(i,:));
  hold on
end
handles_nblopt_plot.title{n_fig} = title(['stable points ' suffix_titles]);

% $$$ for i=1:numel(ocols)
% $$$   istable = [ocols{i}.pdata_in(:,incbal) == 0];
% $$$   aa = ocols{i}.pdata_in(:,ix);
% $$$   aa(istable) = NaN;
% $$$   plotos(aa,ocols{i}.pdata_in(:,iy),'s-',[1 6],0,colos(i,:));
% $$$   hold on
% $$$ end
xlabel(ocols{i}.labels{ix})
ylabel(ocols{i}.labels{iy})
handles_nblopt_plot.leg{n_fig} = legend(leg);
%%

%%
ix = irhopol;
n_fig = n_fig + 1;
handles_nblopt_plot.fig{n_fig} = figure;
iy = ipprime;
for i=2:numel(ocols)
  yfit = interpos(ocols{i}.pdata_in(:,ix),ocols{i}.pdata_in(:,iy),ocols{1}.pdata_in(:,ix),-0.01);
  fmarg(:,i) = yfit./ocols{1}.pdata_in(:,iy);
  handles_nblopt_plot.plot{i,n_fig} = plot(ocols{1}.pdata_in(:,ix),fmarg(:,i));
  hold on
end
xlabel(ocols{i}.labels{ix})
ylabel('Fmarg=P''nblopt/ref')
handles_nblopt_plot.leg{n_fig} = legend(leg{2:end});
axis([0 1 0 10])
handles_nblopt_plot.title{n_fig} = title([suffix_titles]);

%%
n_fig = n_fig + 1;
handles_nblopt_plot.fig{n_fig} = figure;
handles_nblopt_plot.subplot{1,n_fig} = subplot(3,1,1);
for i=2:numel(ocols)
  handles_nblopt_plot.subplots{i,1,n_fig} = plot(ocols{1}.pdata_in(:,ix),fmarg(:,i));
  hold on
end
ylabel('Fmarg')
%axis([0 1 0.5 4.5]);
handles_nblopt_plot.subplots{numel(ocols)+1,1,n_fig} = plotos([0 1],[1 1],'k--',[1 0]);
handles_nblopt_plot.leg{n_fig} = legend(leg{2:end},'location','nw');
handles_nblopt_plot.title{n_fig} = title([suffix_titles]);

handles_nblopt_plot.subplot{2,n_fig} = subplot(3,1,2);
iy = ishear;
for i=1:numel(ocols)
  handles_nblopt_plot.subplots{i,2,n_fig} = plot(ocols{i}.pdata_in(:,ix),ocols{i}.pdata_in(:,iy));
  hold on
end
ylabel('shear')
legend(leg,'location','nw')


handles_nblopt_plot.subplot{3,n_fig} = subplot(3,1,3);
iy = ialpha;
for i=1:numel(ocols)
  handles_nblopt_plot.subplots{i,3,n_fig} = plot(ocols{i}.pdata_in(:,ix),ocols{i}.pdata_in(:,iy));
  hold on
end
ylabel('alpha')
%legend(leg)
xlabel('\rho_p')
%[child_handles,hslice] = modsubplot([0.9 1],[],-1.3,{[0.5 4.5] [0 20] [0 3]},[],[],[],[],[0 0.12]);
[handles_nblopt_plot.child_handles{n_fig},hslice] = modsubplot([0. 1],[],-1.3,{[0. 8] [-5 20] [0 10]},[],'x',[],[],[0 0.12]);
%saveas(gcf,'cheaseout.61141t0.9800_fmarg.png','png');

%%
n_fig = n_fig + 1;
handles_nblopt_plot.fig{n_fig} = figure;
handles_nblopt_plot.subplot{1,n_fig} = subplot(3,1,1);
iy = iistar;
for i=1:numel(ocols)
  plot(ocols{i}.pdata_in(:,ix),ocols{i}.pdata_in(:,iy));
  hold on
end
%ylabel('<j_\phi/R>/<1/R>')
handles_nblopt_plot.text{1,n_fig} = text(0.71,0.16,'<j_\phi/R>/<1/R>');
%axis([0 1 0.5 4.5]);
%plotos([0 1],[1 1],'k--',[1 0]);
%legend(leg{1:end},'location','nw')
handles_nblopt_plot.title{n_fig} = title([suffix_titles]);

handles_nblopt_plot.subplot{2,n_fig} = subplot(3,1,2);
iy = ishear;
for i=1:numel(ocols)
  handles_nblopt_plot.subplots{i,2,n_fig} = plot(ocols{i}.pdata_in(:,ix),ocols{i}.pdata_in(:,iy));
  hold on
end
%ylabel('shear')
handles_nblopt_plot.text{2,n_fig} = text(0.71,2.6,'shear');
%legend(leg,'location','nw')


handles_nblopt_plot.subplot{3,n_fig} = subplot(3,1,3);
iy = ipprime;
for i=1:numel(ocols)
  handles_nblopt_plot.subplots{i,3,n_fig} = plot(ocols{i}.pdata_in(:,ix),abs(ocols{i}.pdata_in(:,iy)));
  hold on
end
%ylabel('|dp/d\psi|_{max} stable to n=\infty')
handles_nblopt_plot.text{3,n_fig} = text(0.71,-0.6,'|dp/d\psi|_{max} stable to n=\infty');
%legend(leg)
xlabel('\rho_\psi')
%[child_handles,hslice] = modsubplot([0.9 1],[],-1.3,{[0.5 4.5] [0 20] [0 3]},[],[],[],[],[0 0.12]);
[handles_nblopt_plot.child_handles{n_fig},hslice] = modsubplot([0.7 1],[],-1.32,{},[],'x',[],[],[0 -0.01]);
%[handles_nblopt_plot.child_handles{n_fig},hslice] = modsubplot([0.7 1],[],-1.32,{[0. 1.2] [0 3] [-1.1 0]},[],'x',[],[],[0 -0.01]);
%saveas(gcf,'cheaseout.61141t0.9800_fmarg.png','png');
