function [R0,Z0,a_minor,kappa,delta_struct,w07,ksi,R07,Z07,w07_bottom,w07_top] = plasma_boundary_global_params(R_in,Z_in);
%
% Find main parameters characterizing the shape (R_in,Z_in), including w07 as defined in O. Sauter, FED 2016
%
% Infer the ksi value from the appendix of same paper such that a shape with:
%
% R = R0 + a.*cos[theta + delta.*sin(theta) - ksi.*sin(2.*theta)];
% Z = kappa.*a_minor.*sin[theta + ksi.*sin(2.*theta)];
%
% yields the w07 value
%

% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

R0 = [];
a_minor = [];
kappa = [];
delta_struct.delta_top = [];
delta_struct.delta_bottom = [];
delta_struct.delta = [];
w07 = [];
ksi = [];

if ~exist('R_in') || isempty(R_in) || ~exist('Z_in') || isempty(Z_in)
  disp('R_in and/or Z_in not provided correctly in plasma_boundary_global_params');
  return
end

R=R_in;
Z=Z_in;
[Zmax,iZmax]=max(Z);
R_Zmax=R(iZmax);
[Zmin,iZmin]=min(Z);
R_Zmin=R(iZmin);
Z0=0.5*(Zmin+Zmax);
bup=Zmax-Z0;

% a_minor defined at z=zaxis, thus look at points near Z0
i_near_Z0 = find(abs(Z-Z0)<=0.2*bup);
[Rmax,iRmax]=max(R(i_near_Z0));
[Rmin,iRmin]=min(R(i_near_Z0));
R0 = (Rmax+Rmin)./2;
a_minor = (Rmax-Rmin)./2;

kappa = (Zmax-Zmin)./(Rmax-Rmin);

Z1=(Z-Z0)./bup;
delta_struct.delta_top = (R0 - R_Zmax)./a_minor;
delta_struct.delta_bottom = (R0 - R_Zmin)./a_minor;
delta = 0.5.*(delta_struct.delta_top+delta_struct.delta_bottom);
delta_struct.delta = delta;

ijLFS=find((R-R_Zmax)>=0);
ijHFS=find((R-R_Zmax)<=0);
ij07max45=iround_os(Z1(ijLFS),0.7);
ij07max135=iround_os(Z1(ijHFS),0.7);
bdown=Z0-Zmin;
Z2=(Z0-Z)./bdown;
ij07min45=iround_os(Z2(ijLFS),0.7);
ij07min135=iround_os(Z2(ijHFS),0.7);
R07=[R(ijLFS(ij07max45)) R(ijHFS(ij07max135)) R(ijHFS(ij07min135)) R(ijLFS(ij07min45))];
Z07=[Z0+0.7*bup Z0+0.7*bup Z0-0.7*bdown Z0-0.7*bdown];

w07_top = (R(ijLFS(ij07max45)) - R(ijHFS(ij07max135))) ./ (2.*a_minor.*cos(asin(0.7)));
w07_bottom = (R(ijLFS(ij07min45)) - R(ijHFS(ij07min135))) ./ (2.*a_minor.*cos(asin(0.7)));

w07 = 0.5.*(w07_top + w07_bottom);

ksi_test = linspace(-0.5,0.60,111);
theta_07LFS = asin(0.7) + (1.-sqrt(1+8.*ksi_test.^2))./4./ksi_test;
w07anal = cos(theta_07LFS-ksi_test.*sin(2.*theta_07LFS))./sqrt(0.51).*(1-0.49./2.*delta.^2);
% should be monotonic
ksi = interp1(w07anal,ksi_test,w07);
