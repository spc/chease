% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

% minimalist demo to run chease with matlab scripts
%clear; close all;

namelist_file = 'namelist_default';
EXPEQ_file = 'EXPEQ_default';

EXPEQ_default = write_expeq;
[EXPEQdataout,fclose_out] = write_expeq(EXPEQ_default,EXPEQ_file);

[~,ha] = plot_expeq(EXPEQdataout.fnamefull); % plot input EXPEQ file

nl.nsttp = EXPEQ_default.nsttp; % set correct nsttp in namelist
nl.npropt = nl.nsttp; % so EXPEQ OUT contains same profile as EXPEQ in
nl = write_namelist_chease(namelist_file,nl);

% Set verbosity level to 3 for demo
fname_out = run_chease(namelist_file,EXPEQ_file,2,'','','',3);

% Unneeded now with new revision of plot_expeq
% plot_expeq(EXPEQdataout.fnamefull,'b'); drawnow

%%
% find EXPEQ_OUT file
for ii=1:length(fname_out);
    if ~isempty(strfind(fname_out{ii},'EXPEQ.OUT_'))
        EXPEQ_OUT_file = fname_out{ii}; break
    end
end
% find cols file
for ii=1:length(fname_out);
  if ~isempty(regexpi(fname_out{ii},'o.*\.cols','start'))
    ocolsfile = fname_out{ii};
  end
  if ~isempty(regexpi(fname_out{ii},'o.*\.RZcols','start'))
    oRZcolsfile = fname_out{ii};
  end
end

% plot output EXPEQ file in same plot
plot_expeq(ha.fig,EXPEQ_OUT_file,'r');
% it is normal that j|| is different

%%
cheasedata = read_ocols(ocolsfile,oRZcolsfile);
poRZcols=plotdatafile(oRZcolsfile);
pocols=plotdatafile(ocolsfile);
hpos=get(pocols.main,'position');
set(pocols.main,'position',[hpos(1)+0.15*hpos(3) hpos(2)-0.15*hpos(4) hpos(3) hpos(4)])
hpos=get(pocols.figplot1D,'position');
set(pocols.figplot1D,'position',[hpos(1)+0.15*hpos(3) hpos(2)-0.15*hpos(4) hpos(3) hpos(4)])
