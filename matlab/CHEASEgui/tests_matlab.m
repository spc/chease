function tests_matlab(test_case)
% tests_matlab(test_case)

% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

try
   fprintf('\n Running test file: %s\n',mfilename('fullpath'));
   fprintf('     Time: %s\n',datestr(now));
   [a,uname_out]=unix('uname -a');
   uname_out

   passed = run_cheasegui_tests(test_case); % call to your test script here, with optional test_case input
   exit_code = int32(~passed); % convert to bash shell convention
catch ME
   disp(getReport(ME))
   exit_code = 1;
end
exit(exit_code);
