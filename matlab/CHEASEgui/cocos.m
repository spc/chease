function [cocos_struct]=cocos(cocos_in);
%
% [cocos_struct]=cocos(cocos_in);
%
% Given the COCOS=cocos_in index (See O. Sauter and S. Yu. Medvedev's paper in CHEASE trunk)
% It returns a structure corresponding to the values of (from Table I of paper):
%
% cocos_struct.exp_Bp:             0 or 1 depending if psi is already divided by 2pi or not, respectively
% cocos_struct.sigma_Bp:          +1 or -1, depending if psi is increasing or decreasing with Ip and B0 positive
% cocos_struct.sigma_RphiZ:       +1 or -1 depending if (R, phi, Z) is right-handed or (R, Z, phi), resp.
% cocos_struct.sigma_rhothetaphi: +1 or -1 depending if (rho, theta, phi) is right-handed or (rho, phi, theta), resp.
% cocos_struct.sign_q_pos:        +1 or -1 depending if q is positive or negative with Ip and B0 positive
% cocos_struct.sign_pprime_pos:   +1 or -1 depending if dp/dpsi is positive or negative with Ip and B0 positive
%                                 Note that the expected sign(dpsi) for Ip and B0 positive is -sign_pprime_pos
% derived useful quantities:
% cocos_struct.sign_theta_clockwise = sigma_RphiZ*sigma_rhothetaphi: +1 if theta clockwise and -1 if counter-clockwise
%                                     'standard' mathematical polar direction is counter-clockwise thus sign_theta_clockwise=-1
%
% Note that table III gives the sign transformations when Ip and B0 are not positive
%
% if cocos_in = 0 or empty, returns an empty structure
%

% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

if ~exist('cocos_in') || isempty(cocos_in)
  cocos_in = 0;
end
% default structure:
cocos_struct.cocos = cocos_in;
cocos_struct.exp_Bp = [];
cocos_struct.sigma_Bp = [];
cocos_struct.sigma_RphiZ = [];
cocos_struct.sigma_rhothetaphi = [];
cocos_struct.sign_q_pos = [];
cocos_struct.sign_pprime_pos = [];
cocos_struct.sign_theta_clockwise = [];

if isempty(cocos_in) || cocos_in<1 || cocos_in>18 || (cocos_in>8 & cocos_in<11)
  disp('just returns default empty structure since cocos_in is not in [1,8] or [11,18]')
  return
end

%
cocos_struct.exp_Bp = 0;
if cocos_in >= 11
  cocos_struct.exp_Bp = 1;
end

%
% Other parameters from Table I
%
switch cocos_in
 case {1, 11}
  % ITER, Boozer are COCOS=11
  cocos_struct.sigma_Bp = +1;
  cocos_struct.sigma_RphiZ = +1;
  cocos_struct.sigma_rhothetaphi = +1;
  cocos_struct.sign_q_pos = +1;
  cocos_struct.sign_pprime_pos = -1;
  %
 case {2, 12}
  % CHEASE, ONETWO, Hinton-Hazeltine, LION is COCOS=2
  cocos_struct.sigma_Bp = +1;
  cocos_struct.sigma_RphiZ = -1;
  cocos_struct.sigma_rhothetaphi = +1;
  cocos_struct.sign_q_pos = +1;
  cocos_struct.sign_pprime_pos = -1;
  %
 case {3, 13}
  % Freidberg, CAXE, KINX are COCOS=3
  % EU-ITM up to end of 2011 is COCOS=13
  cocos_struct.sigma_Bp = -1;
  cocos_struct.sigma_RphiZ = +1;
  cocos_struct.sigma_rhothetaphi = -1;
  cocos_struct.sign_q_pos = -1;
  cocos_struct.sign_pprime_pos = +1;
  %
 case {4, 14}
  %
  cocos_struct.sigma_Bp = -1;
  cocos_struct.sigma_RphiZ = -1;
  cocos_struct.sigma_rhothetaphi = -1;
  cocos_struct.sign_q_pos = -1;
  cocos_struct.sign_pprime_pos = +1;
  %
 case {5, 15}
  %
  cocos_struct.sigma_Bp = +1;
  cocos_struct.sigma_RphiZ = +1;
  cocos_struct.sigma_rhothetaphi = -1;
  cocos_struct.sign_q_pos = -1;
  cocos_struct.sign_pprime_pos = -1;
  %
 case {6, 16}
  %
  cocos_struct.sigma_Bp = +1;
  cocos_struct.sigma_RphiZ = -1;
  cocos_struct.sigma_rhothetaphi = -1;
  cocos_struct.sign_q_pos = -1;
  cocos_struct.sign_pprime_pos = -1;
  %
 case {7, 17}
  % TCV psitbx is COCOS=7
  cocos_struct.sigma_Bp = -1;
  cocos_struct.sigma_RphiZ = +1;
  cocos_struct.sigma_rhothetaphi = +1;
  cocos_struct.sign_q_pos = +1;
  cocos_struct.sign_pprime_pos = +1;
  %
 case {8, 18}
  %
  cocos_struct.sigma_Bp = -1;
  cocos_struct.sigma_RphiZ = -1;
  cocos_struct.sigma_rhothetaphi = +1;
  cocos_struct.sign_q_pos = +1;
  cocos_struct.sign_pprime_pos = +1;
  %
 otherwise
  % should not be here since all cases defined
  error([' ERROR IN COCOS: COCOS = ' num2str(cocos_in) ' DOES NOT EXIST']);
end
%
cocos_struct.sign_theta_clockwise = cocos_struct.sigma_RphiZ * cocos_struct.sigma_rhothetaphi;
