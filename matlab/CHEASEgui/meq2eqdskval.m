function eqdskval=meq2eqdskval(L,LY,varargin)
% MEQ2EQDSKVAL - Convert liuqe.m output structures into OS's eqdsk structure.
%
%   The code assumes that the TCV equilibrium follows COCOS=17 conventions.
%
% CALL
%   >> eqdskval=meq2eqdskval(L,LY [,ParameterName,ParameterValue])
%
% INPUT
%   L   STRUCT  LIUQE parameter structure
%   LY  STRUCT  LIUQE result structure; must contain surface integral
%               ('iterq'>1) and only a single time.
%
% PARAMETERS
%   'facB0'  DOUBLE  B-field multiplier (default: 0.996, see
%                    https://gitlab.epfl.ch/spc/tcv/tbx/meq/-/issues/41
%                    for details)
%
% OUTPUT
%   eqdskval  STRUCT  Equilibrium structure used by write_eqkdsk.m, etc.
%
% EXAMPLE
%   >> [L,LX,LY]=liuqe(65402,1,'iterq',50);
%   >> eqdskval=meq2eqdskval(L,LY,'facB0',0.996);
%
% REQUIREMENTS
%   psi2bnd.m (tcvlib)
%   interpos.m (GUIprofs)
%

% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

%% Parameters
% Defaults
p.facb0=0.996;

% Overwrite defaults
for j=1:length(varargin)-1
    if ischar(varargin{j}) && any(strcmpi(fieldnames(p),varargin{j}))
        p.(lower(varargin{j}))=varargin{j+1};
    end
end

%% Checks
assert(LY.PQ(end)==0,'Edge pressure must be zero.')
assert(isfield(LY,'iqQ'),'Surface integral must have been calculated (''iterq''>1).')
assert(numel(LY.t)==1,'LIUQE structure must only contain a single time.')

%% Constants
mu0=4e-7*pi;

%% Conventions
psieq=linspace(0,1,numel(L.rx))'; % All profiles on equidistant-psi grid

eqdskval.cocos=17;   % LIUQE

%% Parse eqdsk file
% 1st line: 48 character, dummy integer, nr, nz
ss=sprintf('%s (MEQ) #%0d t=%0.4f %s',L.P.tokamak,LY.shot,LY.t,date);
eqdskval.stitle=sprintf('%-48s',ss);
eqdskval.ind1=3;
eqdskval.nr=L.nrx;
eqdskval.nz=L.nzx;

% 2nd line: rboxlen, zboxlen, r0, rboxlft, zboxmid
eqdskval.rboxlen=L.rx(end)-L.rx(1);
eqdskval.zboxlen=L.zx(end)-L.zx(1);
eqdskval.r0=L.P.r0;
eqdskval.rboxleft=L.rx(1);
eqdskval.zmid= 0.5*(L.zx(1)+L.zx(end));

% 3rd line: rmag, zmag, psimag, psiedge, B0
eqdskval.raxis=LY.rA;
eqdskval.zaxis=LY.zA;
eqdskval.psiaxis=LY.FA-LY.FB;
eqdskval.psiedge=0.0;
eqdskval.b0=LY.rBt*p.facb0/L.P.r0;

% 4th line: Ip, psiax1 (duplicated), psiax2 (duplicated), raxis1 (duplicated), raxis2 (duplicated)
eqdskval.ip=LY.Ip;

% 5th line: zaxis1, zaxis2, psi_sep, R_xpoint, Z_xpoint
if LY.lX
    eqdskval.rxpt=LY.rB;
    eqdskval.zxpt=LY.zB;
else
    eqdskval.rxpt=0.;
    eqdskval.zxpt=0.;
end

% Calculate profiles on the EQDSK-'Psi'mesh; include B-field correction.
[PpPsi,TTpPsi,PPsi,TPsi] = meqprof(L.fPg,L.fTg,LY.ag,psieq,LY.FA,LY.FB,LY.rBt*p.facb0,L.bfct,L.bfp,L.idsx,L.smalldia);
% 6th entry: T(psi) on nr equidistant psi mesh
eqdskval.F=TPsi;
% 7th entry: p(psi) on nr equidistant psi mesh
eqdskval.p=PPsi;
% 8th entry: TT'(psi) on nr equidistant psi mesh (- sign from LIUQE) (in MKSA)
eqdskval.FFprime=TTpPsi;
% 9th entry: p'(psi) on nr equidistant psi mesh (in MKSA)
eqdskval.pprime=PpPsi;

% 10th entry: psi(i,j)
eqdskval.psirz=reshape(LY.Fx'-LY.FB,L.nrx*L.nzx,1);

% 11th entry: q profile on nr equidistant psi mesh
% do not use 0der in center and some smoothing to get real output from LIUQE even if LIUQE not so good near center
ival=find(LY.iqQ);
eqdskval.q=TPsi.*interpos(-13,L.pQ(ival).^2,1./(LY.TQ(ival).*LY.iqQ(ival)),psieq);

eqdskval.psimesh=psieq;
eqdskval.rhopsi=sqrt(psieq);

% 12th entry: (R,Z) plasma boundary and wall position
bg=psi2bnd(meq2psi(L,LY));
eqdskval.nbbound=numel(bg.x{1});
eqdskval.nblim=L.G.nl;
% Plasma boundary
eqdskval.rplas=bg.x{1}(:);
eqdskval.zplas=bg.x{2}(:);
% Wall position
eqdskval.rlim=L.G.rl(:);
eqdskval.zlim=L.G.zl(:);

%% End of official part of eqdsk. Add same last lines as expeq file for info
eqdskval.extralines{1}=sprintf('%18.8e   psiaxis, psi0/2pi = %18.8e', ...
    LY.FA-LY.FB, (LY.FA-LY.FB)/(2*pi));
eqdskval.extralines{end+1}=sprintf('%18.8e   r-magaxe', LY.rA);
eqdskval.extralines{end+1}=sprintf('%18.8e   z-magaxe', LY.zA);

zrmax = max(bg.x{1});
zrmin = min(bg.x{1});
zzmax = max(bg.x{2});
zzmin = min(bg.x{2});
za      = 0.5*(zrmax-zrmin);
zkappa  = 0.5*(zzmax-zzmin)./za;
zaver   = 0.5*(zzmax+zzmin);
eqdskval.extralines{end+1}=sprintf('%18.8e   Z0 (zaver)', zaver);
eqdskval.extralines{end+1}=sprintf('%18.8e   R0 [m]', L.P.r0);
eqdskval.extralines{end+1}=sprintf('%18.8e   B0 [T]', LY.rBt*p.facb0/L.P.r0);
eqdskval.extralines{end+1}=sprintf('%18.8e   SIGN OF B0 IN EXPERIMENT', sign(LY.rBt));
eqdskval.extralines{end+1}=sprintf('%18.8e   TOTAL CURRENT -> I-p [A]:  %18.8e', ...
    mu0*LY.Ip./(LY.rBt*p.facb0), LY.Ip);
eqdskval.extralines{end+1}=sprintf('%18.8e   SIGN OF IP IN EXPERIMENT', sign(LY.Ip));
eqdskval.extralines{end+1}=sprintf('%18.8e   kappa', zkappa);

eqdskval.extralines{end+1}=sprintf('%18.8e   q_0        zrhopol(1)  %18.8e', ...
    eqdskval.q(1), 0);
eqdskval.extralines{end+1}=sprintf('%18.8e   q_edge', eqdskval.q(end));
eqdskval.extralines{end+1}=sprintf('%18.8e   beta_pol', LY.bp);
eqdskval.extralines{end+1}=sprintf('%18.8e   beta_tor', LY.bt);
eqdskval.extralines{end+1}=sprintf('%18.8e   li', LY.li);      % Is li(3)?

eqdskval.extralines{end+1}=sprintf('%18.8e   number of X points', LY.nX);
for ix=1:LY.nX
    eqdskval.extralines{end+1}=sprintf('%18.8e  R of Xpt nb: %d  -> in [m]: %18.8e', ...
        LY.rX(ix)/L.P.r0, ix, LY.rX(ix));
    eqdskval.extralines{end+1}=sprintf('%18.8e  Z of Xpt nb: %d  -> in [m]: %18.8e', ...
        LY.zX(ix)/L.P.r0, ix, LY.zX(ix));
end

eqdskval.extralines{end+1}=sprintf('%18.8e   time', LY.t);
eqdskval.extralines{end+1}=sprintf('  %5i   shot number', LY.shot);

% Duplicate information
eqdskval.psi=LY.Fx'-LY.FB;
eqdskval.rmesh=L.rx;
eqdskval.zmesh=L.zx;
