function [hmode_distance,nuplo_data,nuplo_figs_handle] = nuplo_2d_plots(nuplo_filename,title_str,varargin)
%
% [hmode_distance,nuplo_data,nuplo_figs_handle] = nuplo_2d_plots(nuplo_filename,title_str,varargin);
%
% Read data from file NUPLO and extract data relevant on the poloidal cross-section in nuplo_data
% Plot (1) chi=cst lines
%      (2) psi=cst surfaces
%      (3) S(R,Z) local magnetic shear and zero curvature as contour asinh
%      (4) as (3) but only markers for negative S
%      (5) local shear S versus theta on psi_surface flux surface, used to determine distance to H-mode
%
% nuplo_filename: NUPLO full filename, output of CHEASE.
%                 if cell array (fname_out from run_chease) find file with NUPLO in name with nuplo_filename{contains(nuplo_filename, 'NUPLO')}
% title_str: title string to add to all plots:
%            not provideed or [] replace by last_folder/name of nuplo_filename (default)
%            -1:  no title set
%
% varargin{1}: ('12345' default) choice of figure to plot ('3' to plot only 3rd figure)
% varargin{2}: psi_surface(normalized) default: 0.90 normalized psi surface to look for max(S) and zero curvature
%
% hmode_distance: angles of zero curvature and max(S) on psi_surface and related quantities
% hmode_distance.comments: descriptions of some fields
% nuplo_data: data extracted from NUPLO and some derived data
% nuplo_data.comments: descriptions of some relevant fields, in particular used for the plots
%

% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

nuplo_data = [];
nuplo_figs_handle = NaN*ones(1,5);
nuplo_data = [];
hmode_distance = [];

if exist('nuplo_filename') && iscell(nuplo_filename)
  nuplo_filename = nuplo_filename{contains(nuplo_filename, 'NUPLO')};
elseif ~exist('nuplo_filename') || isempty(nuplo_filename) || exist(nuplo_filename,'file')~=2
  error('requires the NUPLO full filename');
end

if ~exist('title_str') || isempty(title_str)
  aa = dir(nuplo_filename);
  [a1,a2]=fileparts(aa.folder);
  title_str = fullfile(a2,aa.name);
  title_str = replace(title_str,'_','\_');
end

opt_figs = '12345';
if nargin>=3 && ~isempty(varargin{1})
  if isnumeric(varargin{1})
    opt_figs = num2str(varargin{1});
  else
    opt_figs = varargin{1};
  end
end

psi_surface = 0.90;
if numel(varargin)>=2 && ~isempty(varargin{2})
  psi_surface = varargin{2};
end
hmode_distance.psi_surface = psi_surface;
hmode_distance.comments.psi_surface = 'target normalized psi surface on which to look for max(S(theta)), curvature=0, etc';

doload = 1;
if doload
  assert(exist(nuplo_filename)==2,['filename ' nuplo_filename ' not correct']);
  fid = fopen(nuplo_filename,'r');
  insur = fscanf(fid,'%d',1);
  nchi = fscanf(fid,'%d',1);
  nchi1 = fscanf(fid,'%d',1);
  npsi = fscanf(fid,'%d',1);
  npsi1 = fscanf(fid,'%d',1);
  ns = fscanf(fid,'%d',1);
  ns1 = fscanf(fid,'%d',1);
  nt = fscanf(fid,'%d',1);
  nt1 = fscanf(fid,'%d',1);
  ins = fscanf(fid,'%d',1);
  inr = fscanf(fid,'%d',1);
  inbchi = fscanf(fid,'%d',1);
  intext = fscanf(fid,'%d',1);
  nuplo_data.ncurv = fscanf(fid,'%d',1);
  nmesha = fscanf(fid,'%d',1);
  nmeshb = fscanf(fid,'%d',1);
  nmeshc = fscanf(fid,'%d',1);
  nmeshd = fscanf(fid,'%d',1);
  nmeshe = fscanf(fid,'%d',1);
  npoida = fscanf(fid,'%d',1);
  npoidb = fscanf(fid,'%d',1);
  npoidc = fscanf(fid,'%d',1);
  npoidd = fscanf(fid,'%d',1);
  npoide = fscanf(fid,'%d',1);
  niso = fscanf(fid,'%d',1);
  nmgaus = fscanf(fid,'%d\n',1);
  niso1 = niso + 1;
  for i=1:intext
    nuplo_text{i} = fgetl(fid);
  end
  [nuplo_data.iball,icount] = fscanf(fid,'%f',npsi1);
  [nuplo_data.imerci,icount] = fscanf(fid,'%f',npsi1);
  [nuplo_data.imercr,icount] = fscanf(fid,'%f',npsi1);
  solpda = fscanf(fid,'%f',1);
  solpdb = fscanf(fid,'%f',1);
  solpdc = fscanf(fid,'%f',1);
  solpdd = fscanf(fid,'%f',1);
  solpde = fscanf(fid,'%f',1);
  zrmax = fscanf(fid,'%f',1);
  zrmin = fscanf(fid,'%f',1);
  zzmax = fscanf(fid,'%f',1);
  zzmin = fscanf(fid,'%f',1);
  pangle = fscanf(fid,'%f\n',1);
  [nuplo_data.aplace,icount] = fscanf(fid,'%f',10);
  [nuplo_data.awidth,icount] = fscanf(fid,'%f',10);
  [nuplo_data.bplace,icount] = fscanf(fid,'%f',10);
  [nuplo_data.bwidth,icount] = fscanf(fid,'%f',10);
  [nuplo_data.cplace,icount] = fscanf(fid,'%f',10);
  [nuplo_data.cwidth,icount] = fscanf(fid,'%f',10);
  [nuplo_data.dplace,icount] = fscanf(fid,'%f',10);
  [nuplo_data.dwidth,icount] = fscanf(fid,'%f',10);
  [nuplo_data.eplace,icount] = fscanf(fid,'%f',10);
  [nuplo_data.ewidth,icount] = fscanf(fid,'%f',10);
  [nuplo_data.ztet,icount] = fscanf(fid,'%f',nt1);
  [nuplo_data.csig,icount] = fscanf(fid,'%f',ns1);
  [nuplo_data.cs,icount] = fscanf(fid,'%f',niso1);
  nuplo_data.comments.cs = 's-mesh ~ sqrt(psi_normalized)';
  [nuplo_data.zchi,icount] = fscanf(fid,'%f',nchi1);
  nuplo_data.comments.zchi = 'chi-mesh (note: meaning depends on Jacobian)';
  [nuplo_data.zcsipr,icount] = fscanf(fid,'%f',niso1);
  [nuplo_data.zrtet,icount] = fscanf(fid,'%f',nt);
  [nuplo_data.zztet,icount] = fscanf(fid,'%f',nt);
  [nuplo_data.zrsur,icount] = fscanf(fid,'%f',insur);
  [nuplo_data.ztsur,icount] = fscanf(fid,'%f',insur);
  [nuplo_data.zzsur,icount] = fscanf(fid,'%f',insur);
  nuplo_data.comments.zrsur_zzsur = '(R-Rmag,Z-Zmag) values plasma boundary surface';
  [nuplo_data.zabis,icount] = fscanf(fid,'%f',ins);
  [nuplo_data.zabit,icount] = fscanf(fid,'%f',nt1);
  [nuplo_data.zabic,icount] = fscanf(fid,'%f',nchi1);
  [nuplo_data.zoart,icount] = fscanf(fid,'%f',nt1);
  [nuplo_data.zabipr,icount] = fscanf(fid,'%f',niso+1);
  [nuplo_data.zabisg,icount] = fscanf(fid,'%f',ns1);
  [nuplo_data.zabsm,icount] = fscanf(fid,'%f',ins);
  [nuplo_data.zabr ,icount] = fscanf(fid,'%f',inr);
  [nuplo_data.zoqs ,icount] = fscanf(fid,'%f',ins);
  [nuplo_data.zoqr ,icount] = fscanf(fid,'%f',inr);
  [nuplo_data.zodqs,icount] = fscanf(fid,'%f',ins);
  [nuplo_data.zodqr,icount] = fscanf(fid,'%f',inr);
  [nuplo_data.zoshs(1:npsi1-1),icount] = fscanf(fid,'%f',npsi1-1);
  [nuplo_data.zoshs(npsi1),icount] = fscanf(fid,'%f\n',1);
  aa=fgetl(fid); % bad number
  [nuplo_data.zoshr,icount] = fscanf(fid,'%f',inr);
  [nuplo_data.zojbs,icount] = fscanf(fid,'%f',ins);
  [nuplo_data.zojbr,icount] = fscanf(fid,'%f',inr);
  [nuplo_data.zojbss(1:ins,1),icount] = fscanf(fid,'%f',ins);
  [nuplo_data.zojbss(1:ins,2),icount] = fscanf(fid,'%f',ins);
  [nuplo_data.zojbss(1:ins,3),icount] = fscanf(fid,'%f',ins);
  [nuplo_data.zojbss(1:ins,4),icount] = fscanf(fid,'%f',ins);
  [nuplo_data.zojbsr(1:inr,1),icount] = fscanf(fid,'%f',inr);
  [nuplo_data.zojbsr(1:inr,2),icount] = fscanf(fid,'%f',inr);
  [nuplo_data.zojbsr(1:inr,3),icount] = fscanf(fid,'%f',inr);
  [nuplo_data.zojbsr(1:inr,4),icount] = fscanf(fid,'%f',inr);
  [nuplo_data.zojps,icount] = fscanf(fid,'%f',ins);
  [nuplo_data.zojpr,icount] = fscanf(fid,'%f',inr);
  [nuplo_data.zotrs,icount] = fscanf(fid,'%f',ins);
  [nuplo_data.zotrr,icount] = fscanf(fid,'%f',inr);
  [nuplo_data.zohs,icount] = fscanf(fid,'%f',ins);
  [nuplo_data.zodis,icount] = fscanf(fid,'%f',ins);
  [nuplo_data.zodrs,icount] = fscanf(fid,'%f',ins);
  [nuplo_data.zopps,icount] = fscanf(fid,'%f',ins);
  [nuplo_data.zoppr,icount] = fscanf(fid,'%f',inr);
  [nuplo_data.zops ,icount] = fscanf(fid,'%f',ins);
  [nuplo_data.zopr ,icount] = fscanf(fid,'%f',inr);
  [nuplo_data.zotts,icount] = fscanf(fid,'%f',ins);
  [nuplo_data.zottr,icount] = fscanf(fid,'%f',inr);
  [nuplo_data.zots,icount] = fscanf(fid,'%f',ins);
  [nuplo_data.zotr ,icount] = fscanf(fid,'%f',inr);
  [nuplo_data.zoips,icount] = fscanf(fid,'%f',ins);
  [nuplo_data.zoipr,icount] = fscanf(fid,'%f',inr);
  [nuplo_data.zobetr,icount] = fscanf(fid,'%f',inr);
  [nuplo_data.zobets,icount] = fscanf(fid,'%f',npsi1);
  [nuplo_data.zofr ,icount] = fscanf(fid,'%f',inr);
  [nuplo_data.zoars,icount] = fscanf(fid,'%f',npsi1);
  [nuplo_data.zojr ,icount] = fscanf(fid,'%f',inr);
  [nuplo_data.zabs,icount] = fscanf(fid,'%f',npsi1);

  for j=1:npsi1
    [nuplo_data.rriso(1:nmgaus*nt1,j),icount] = fscanf(fid,'%f',nmgaus*nt1);
    [nuplo_data.rziso(1:nmgaus*nt1,j),icount] = fscanf(fid,'%f',nmgaus*nt1);
  end
  [nuplo_data.rrcurv,icount] = fscanf(fid,'%f',nuplo_data.ncurv);
  [nuplo_data.rzcurv,icount] = fscanf(fid,'%f',nuplo_data.ncurv);
  nuplo_data.comments.rrcurv_rzcurv = '(R-Rmag,Z-Zmag) values of zero curvature line, as from NUPLO';
  [aa,icount] = fscanf(fid,'%f',inbchi*npsi1);
  ij = 0;
  for j=1:npsi1
    nuplo_data.zrchi(1:inbchi,j) = aa(ij+1:ij+inbchi);
    ij = ij + inbchi;
  end
  [aa,icount] = fscanf(fid,'%f',inbchi*npsi1);
  ij = 0;
  for j=1:npsi1
    nuplo_data.zzchi(1:inbchi,j) = aa(ij+1:ij+inbchi);
    ij = ij + inbchi;
  end
  nuplo_data.comments.zrchi_zzchi = '(R-Rmag,Z-Zmag) values of chi=cst lines';
  [aa,icount] = fscanf(fid,'%f',nchi*npsi1);
  ij = 0;
  for j=1:npsi1
    nuplo_data.rshear(1:nchi,j) = aa(ij+1:ij+nchi);
    ij = ij + nchi;
  end
  [aa,icount] = fscanf(fid,'%f',nchi*npsi1);
  ij = 0;
  for j=1:npsi1
    nuplo_data.cr(1:nchi,j) = aa(ij+1:ij+nchi);
    ij = ij + nchi;
  end
  [aa,icount] = fscanf(fid,'%f',nchi*npsi1);
  ij = 0;
  for j=1:npsi1
    nuplo_data.cz(1:nchi,j) = aa(ij+1:ij+nchi);
    ij = ij + nchi;
  end
  try
    [aa,icount] = fscanf(fid,'%f',nchi*(npsi1+1));
  catch
    nuplo_data.eq12 = NaN;
    icount = 0;
  end
  if icount == nchi*(npsi1+1)
    ij = 0;
    for j=1:npsi1+1
      nuplo_data.eq12(1:nchi,j) = aa(ij+1:ij+nchi);
      ij = ij + nchi;
    end
  else
    nuplo_data.eq12 = NaN;
  end
  fclose(fid);

  nuplo_data.crp = nuplo_data.cr;
  nuplo_data.crp(end+1,:) = nuplo_data.crp(1,:);
  nuplo_data.czp = nuplo_data.cz;
  nuplo_data.czp(end+1,:) = nuplo_data.czp(1,:);
  nuplo_data.comments.cr_cz_p = '(R-Rmag,Z-Zmag) values of psi=cst surfaces, ''p'' for including periodic point';
  nuplo_data.rshearp = nuplo_data.rshear;
  nuplo_data.rshearp(end+1,:) = nuplo_data.rshearp(1,:);
  nuplo_data.comments.rshear_p = '(R-Rmag,Z-Zmag) values of local shear S, ''p'' for including periodic point';
end

nuplo_data.r0curv = [nuplo_data.rrcurv(end-1:-2:1); nuplo_data.rrcurv(2:2:end)];
nuplo_data.z0curv = [nuplo_data.rzcurv(end-1:-2:1); nuplo_data.rzcurv(2:2:end)];
nuplo_data.comments.r0curv_z0curv = '(R-Rmag,Z-Zmag) values of zero curvature line ordered from top to bottom';

%%
j_psi_surf=iround_os(nuplo_data.cs,sqrt(psi_surface));
nuplo_data.j_psi_surf = j_psi_surf;
theta_psi_surf=atan2(nuplo_data.cz(:,j_psi_surf),nuplo_data.cr(:,j_psi_surf));
nuplo_data.theta_psi_surf = theta_psi_surf;
nuplo_data.comments.theta_psi_surf = 'theta mesh on psi=psi_surface j_psi_surf of nuplo_data';
[theta_psi_surf_sort,isort]=sort(theta_psi_surf);
nuplo_data.theta_psi_surf_sort = theta_psi_surf_sort;
rshear_psi_surf_sort = nuplo_data.rshear(isort,j_psi_surf);
nuplo_data.rshear_psi_surf_sort = rshear_psi_surf_sort;
theta_fit = linspace(theta_psi_surf_sort(1),theta_psi_surf_sort(end),721);
[a0,a1,a2]=interpos(theta_psi_surf_sort,rshear_psi_surf_sort,theta_fit,-2,[-1 -1],[2*pi,2*pi]);
i0=iround_os(theta_fit,0);
i45=iround_os(theta_fit,45/180.*pi);
i125=iround_os(theta_fit,125/180.*pi);
im45=iround_os(theta_fit,-45/180.*pi);
im125=iround_os(theta_fit,-125/180.*pi);
% find local max first based on 2nd derivative
[~,i2nd_min]=min(a2(i0:end));
iSmaxtop_sort = i0+i2nd_min-1;
if iSmaxtop_sort>i125
  % most probably missed a local minimum
  [~,i2nd_min]=min(a2(i0:i125));
  iSmaxtop_sort = i0+i2nd_min-1;
elseif iSmaxtop_sort < i45
  [~,i2nd_min]=min(a2(i45:i125));
  iSmaxtop_sort = i45+i2nd_min-1;
end
% find nearby local max (on splined fit)
[a0max,ithjj] = max(a0(iSmaxtop_sort-20:iSmaxtop_sort+20));
iSmaxtop_sort = iSmaxtop_sort-20-1+ithjj;
thjj = theta_fit(iSmaxtop_sort);
% move back to theta_psi_surf reference
iSmaxtop_theta_psi_surf_sort = iround_os(theta_psi_surf_sort,thjj);
iSmaxtop_theta_psi_surf = isort(iSmaxtop_theta_psi_surf_sort);

% find Smax in bottom half cross-section
[~,i2nd_min]=min(a2(i0:-1:1));
iSmaxbot_sort = i0-i2nd_min+1;
if iSmaxbot_sort < im125
  % most probably missed a local minimum
  [~,i2nd_min]=min(a2(i0:-1:im125));
  iSmaxbot_sort = i0-i2nd_min+1;
elseif iSmaxtop_theta_psi_surf_sort > im45
  [~,i2nd_min]=min(a2(im45:-1:im125));
  iSmaxbot_sort = im45-i2nd_min+1;
end
[a0max,ithjj] = max(a0(iSmaxbot_sort-20:iSmaxbot_sort+20));
iSmaxbot_sort = iSmaxbot_sort-20-1+ithjj;
thjj = theta_fit(iSmaxbot_sort);

% move back to theta_psi_surf reference
iSmaxbot_theta_psi_surf_sort = iround_os(theta_psi_surf_sort,thjj);
iSmaxbot_theta_psi_surf = isort(iSmaxbot_theta_psi_surf_sort);
theta_top_0curv = atan2(nuplo_data.z0curv(nuplo_data.ncurv/2-j_psi_surf+1),nuplo_data.r0curv(nuplo_data.ncurv/2-j_psi_surf+1));
theta_bot_0curv = atan2(nuplo_data.z0curv(nuplo_data.ncurv/2+j_psi_surf-1),nuplo_data.r0curv(nuplo_data.ncurv/2+j_psi_surf-1));
%%
dtheta_0curv_to_maxS_top = theta_psi_surf(iSmaxtop_theta_psi_surf) - theta_top_0curv;
dtheta_0curv_to_maxS_bot = theta_bot_0curv - theta_psi_surf(iSmaxbot_theta_psi_surf);

hmode_distance.Smax_0curv_top_half = dtheta_0curv_to_maxS_top*180/pi / 90;
hmode_distance.Smax_0curv_bot_half = dtheta_0curv_to_maxS_bot*180/pi / 90;
hmode_distance.comments.Smax_0curv_top_half = sprintf('%s\n%s','positive means favourable to H-mode, negative means stays in L-mode',...
          'distance in angle relative to 90deg of zero curvature to max local shear near top');
hmode_distance.comments.Smax_0curv_bot_half = sprintf('%s\n%s','positive means favourable to H-mode, negative means stays in L-mode',...
          'distance in angle relative to 90deg of zero curvature to max local shear near bottom');
hmode_distance.theta_top_0curv_deg = theta_top_0curv*180/pi;
hmode_distance.theta_bot_0curv_deg = theta_bot_0curv*180/pi;
hmode_distance.theta_top_0curv_Svalue = interp1(theta_psi_surf_sort,rshear_psi_surf_sort,theta_top_0curv);
hmode_distance.theta_bot_0curv_Svalue = interp1(theta_psi_surf_sort,rshear_psi_surf_sort,theta_bot_0curv);
hmode_distance.theta_top_maxS_deg = theta_psi_surf(iSmaxtop_theta_psi_surf)*180/pi;
hmode_distance.theta_top_maxS_value = nuplo_data.rshear(iSmaxtop_theta_psi_surf,j_psi_surf);
hmode_distance.theta_top_maxS_fit_deg = theta_fit(iSmaxtop_sort)*180/pi;
hmode_distance.theta_top_maxS_fit_value = a0(iSmaxtop_sort);
hmode_distance.theta_top_maxS_fit_2ndder_deg = a2(iSmaxtop_sort).*(pi/180)^2; % versus degrees
% location is not necessarily at min of S'', thus take local min
ij=find(theta_fit*180/pi>=hmode_distance.theta_top_maxS_fit_deg-10 & theta_fit*180/pi<=hmode_distance.theta_top_maxS_fit_deg+10);
hmode_distance.theta_top_maxS_fit_2ndder_deg_local_min = min(a2(ij)) .*(pi/180)^2;
nuplo_data.iSmaxtop_theta_psi_surf = iSmaxtop_theta_psi_surf;
hmode_distance.theta_bot_maxS_deg = theta_psi_surf(iSmaxbot_theta_psi_surf)*180/pi;
hmode_distance.theta_bot_maxS_value = nuplo_data.rshear(iSmaxbot_theta_psi_surf,j_psi_surf);
hmode_distance.theta_bot_maxS_fit_deg = theta_fit(iSmaxbot_sort)*180/pi;
hmode_distance.theta_bot_maxS_fit_value = a0(iSmaxbot_sort);
hmode_distance.theta_bot_maxS_fit_2ndder_deg = a2(iSmaxbot_sort).*(pi/180)^2; % versus degrees
ij=find(theta_fit*180/pi>=hmode_distance.theta_bot_maxS_fit_deg-10 & theta_fit*180/pi<=hmode_distance.theta_bot_maxS_fit_deg+10);
hmode_distance.theta_bot_maxS_fit_2ndder_deg_local_min = min(a2(ij)) .*(pi/180)^2;
% plateau just in good curvature region, can have overshoot near X-point so use original datapoints
plateau_range = 25;
ij=find(nuplo_data.theta_psi_surf_sort*180/pi>=hmode_distance.theta_top_0curv_deg & ...
        nuplo_data.theta_psi_surf_sort*180/pi<=hmode_distance.theta_top_0curv_deg+plateau_range);
hmode_distance.S_local_min_top_good_curv = min(nuplo_data.rshear_psi_surf_sort(ij));
ij=find(nuplo_data.theta_psi_surf_sort*180/pi>=hmode_distance.theta_bot_0curv_deg-plateau_range & ...
        nuplo_data.theta_psi_surf_sort*180/pi<=hmode_distance.theta_bot_0curv_deg);
hmode_distance.S_local_min_bot_good_curv = min(nuplo_data.rshear_psi_surf_sort(ij));
nuplo_data.iSmaxbot_theta_psi_surf = iSmaxbot_theta_psi_surf;
hmode_distance.min_local_shear_psi_surf = min(nuplo_data.rshear(:,j_psi_surf));
hmode_distance.max_local_shear_psi_surf = max(nuplo_data.rshear(:,j_psi_surf));
hmode_distance.comments.theta_top_maxS_deg = sprintf('%s%d%s','angle of max(local_shear(',psi_surface*100,'% flux surface)) in bottom half');
hmode_distance.comments.theta_bot_maxS_deg = sprintf('%s%d%s','angle of max(local_shear(',psi_surface*100,'% flux surface)) in bottom half');
hmode_distance.comments.theta_top_0curv_deg = sprintf('%s%d%s','angle of zero curvature at ',psi_surface*100,'% flux surface top half');
hmode_distance.comments.theta_bot_0curv_deg = sprintf('%s%d%s','angle of zero curvature at ',psi_surface*100,'% flux surface bottom half');
hmode_distance.comments.min_local_shear_psi_surf = sprintf('%s%d%s','min value of the local magnetic shear at ',psi_surface*100,'% flux surface');
hmode_distance.comments.max_local_shear_psi_surf = sprintf('%s%d%s','max value of the local magnetic shear at ',psi_surface*100,'% flux surface');
hmode_distance.comments.S_local_min_top_good_curv = sprintf('%s%f%s','local min of S just in good curvature within theta_top_0curv+[0,', ...
          plateau_range,']');
hmode_distance.comments.S_local_min_bot_good_curv = sprintf('%s%f%s','local min of S just in good curvature within theta_bot_0curv+[-', ...
          plateau_range,',0]');

%% location of S=0 inside Smax (i.e. between 0 and theta_Smax)
ij = find(a0(iSmaxtop_sort:-1:i0)<0);
if ~isempty(ij) && a0(iSmaxtop_sort)>0
  thetaS0top_sort = interp1(a0(iSmaxtop_sort-ij(1)+1:iSmaxtop_sort-ij(1)+2),theta_fit(iSmaxtop_sort-ij(1)+1:iSmaxtop_sort-ij(1)+2),0);
  hmode_distance.theta_top_S0_deg = thetaS0top_sort*180/pi;
else
  thetaS0top_sort = NaN;
  hmode_distance.theta_top_S0_deg = NaN;
end
if hmode_distance.theta_top_maxS_fit_2ndder_deg < 0 && hmode_distance.theta_top_maxS_value > 0
  thetaS0top_fromSmax_deg = hmode_distance.theta_top_maxS_deg ...
      - sqrt(-2.*hmode_distance.theta_top_maxS_value/hmode_distance.theta_top_maxS_fit_2ndder_deg);
  ij=iround_os(nuplo_data.theta_psi_surf_sort*180/pi,thetaS0top_fromSmax_deg);
  hmode_distance.S_at_theta_top_S0_approx_from_Smax = interp1(nuplo_data.theta_psi_surf_sort(ij-1:ij+1), ...
          nuplo_data.rshear_psi_surf_sort(ij-1:ij+1),thetaS0top_fromSmax_deg*pi/180);
else
  thetaS0top_fromSmax_deg = NaN;
  hmode_distance.S_at_theta_top_S0_approx_from_Smax = NaN;
end
hmode_distance.theta_top_S0_approx_from_Smax_deg = thetaS0top_fromSmax_deg;

ij = find(a0(iSmaxbot_sort:i0)<0);
if ~isempty(ij) && a0(iSmaxbot_sort)>0
  thetaS0bot_sort = interp1(a0(iSmaxbot_sort+ij(1)-2:iSmaxbot_sort+ij(1)-1),theta_fit(iSmaxbot_sort+ij(1)-2:iSmaxbot_sort+ij(1)-1),0);
else
  thetaS0bot_sort = NaN;
end
hmode_distance.theta_bot_S0_deg = thetaS0bot_sort*180/pi;
if hmode_distance.theta_bot_maxS_fit_2ndder_deg < 0 && hmode_distance.theta_bot_maxS_value > 0
  thetaS0bot_fromSmax_deg = hmode_distance.theta_bot_maxS_deg ...
      + sqrt(-2.*hmode_distance.theta_bot_maxS_value/hmode_distance.theta_bot_maxS_fit_2ndder_deg);
  ij=iround_os(nuplo_data.theta_psi_surf_sort*180/pi,thetaS0bot_fromSmax_deg);
  hmode_distance.S_at_theta_bot_S0_approx_from_Smax = interp1(nuplo_data.theta_psi_surf_sort(ij-1:ij+1), ...
          nuplo_data.rshear_psi_surf_sort(ij-1:ij+1),thetaS0bot_fromSmax_deg*pi/180);
else
  thetaS0bot_fromSmax_deg = NaN;
  hmode_distance.S_at_theta_bot_S0_approx_from_Smax = NaN;
end
hmode_distance.theta_bot_S0_approx_from_Smax_deg = thetaS0bot_fromSmax_deg;

hmode_distance.title_str = title_str;

%% should have ended with computing relevvant data, now just plotting (thus should only use output data)
if ~isempty(findstr(opt_figs,'1'))
  nuplo_figs_handle(1) = figure;
  plot(nuplo_data.zrchi',nuplo_data.zzchi')
  hold on
  plotos(nuplo_data.r0curv,nuplo_data.z0curv,'k-',[3 0],[]);
  plot(nuplo_data.zrsur,nuplo_data.zzsur,'k')
  axis equal
  if ~isnumeric(hmode_distance.title_str) || hmode_distance.title_str~=-1, title(hmode_distance.title_str); end
  ylabel('\chi = cst lines on (R,Z) and curvature=0')
end
%%
if ~isempty(findstr(opt_figs,'2'))
  nuplo_figs_handle(2) = figure;
  plot(nuplo_data.crp,nuplo_data.czp)
  hold on
  plotos(nuplo_data.r0curv,nuplo_data.z0curv,'k-',[3 0]);
  plot(nuplo_data.zrsur,nuplo_data.zzsur,'k')
  axis equal
  if ~isnumeric(hmode_distance.title_str) || hmode_distance.title_str~=-1, title(hmode_distance.title_str); end
  ylabel('\psi = cst lines on (R,Z) and curvature=0')
end
%%
if ~isempty(findstr(opt_figs,'3'))
  nuplo_figs_handle(3) = figure;
  contour(nuplo_data.crp,nuplo_data.czp,asinh(nuplo_data.rshearp),100)
  hold on
  plotos(nuplo_data.r0curv,nuplo_data.z0curv,'k-',[3 0]);
  plot(nuplo_data.zrsur,nuplo_data.zzsur,'k')
  plotos(nuplo_data.cr(nuplo_data.iSmaxtop_theta_psi_surf,nuplo_data.j_psi_surf), ...
         nuplo_data.cz(nuplo_data.iSmaxtop_theta_psi_surf,nuplo_data.j_psi_surf),'ro',[1 9],1);
  plotos(nuplo_data.cr(nuplo_data.iSmaxbot_theta_psi_surf,nuplo_data.j_psi_surf), ...
         nuplo_data.cz(nuplo_data.iSmaxbot_theta_psi_surf,nuplo_data.j_psi_surf),'ro',[1 9],1);
  plotos(nuplo_data.r0curv(nuplo_data.ncurv/2+nuplo_data.j_psi_surf-1), ...
         nuplo_data.z0curv(nuplo_data.ncurv/2+nuplo_data.j_psi_surf-1),'s',[1 10],[1],[0. 0.4 0.]);
  plotos(nuplo_data.r0curv(nuplo_data.ncurv/2-nuplo_data.j_psi_surf+1), ...
         nuplo_data.z0curv(nuplo_data.ncurv/2-nuplo_data.j_psi_surf+1),'s',[1 10],[1],[0. 0.4 0.]);
  if ~isnan(hmode_distance.theta_top_S0_deg)
    itheta_top_S0_deg = iround_os(nuplo_data.theta_psi_surf,hmode_distance.theta_top_S0_deg/180*pi);
    plotos(nuplo_data.cr(itheta_top_S0_deg,nuplo_data.j_psi_surf),nuplo_data.cz(itheta_top_S0_deg,nuplo_data.j_psi_surf),'mv',[1 9],1);
    itheta_bot_S0_deg = iround_os(nuplo_data.theta_psi_surf,hmode_distance.theta_bot_S0_deg/180*pi);
    plotos(nuplo_data.cr(itheta_bot_S0_deg,nuplo_data.j_psi_surf),nuplo_data.cz(itheta_bot_S0_deg,nuplo_data.j_psi_surf),'mv',[1 9],1);
  end
  if ~isnan(hmode_distance.theta_top_S0_approx_from_Smax_deg)
    itheta_top_S0_approx_from_Smax_deg = iround_os(nuplo_data.theta_psi_surf,hmode_distance.theta_top_S0_approx_from_Smax_deg/180*pi);
    plotos(nuplo_data.cr(itheta_top_S0_approx_from_Smax_deg,nuplo_data.j_psi_surf), ...
           nuplo_data.cz(itheta_top_S0_approx_from_Smax_deg,nuplo_data.j_psi_surf),'m^',[1 9],0);
    itheta_bot_S0_approx_from_Smax_deg = iround_os(nuplo_data.theta_psi_surf,hmode_distance.theta_bot_S0_approx_from_Smax_deg/180*pi);
    plotos(nuplo_data.cr(itheta_bot_S0_approx_from_Smax_deg,nuplo_data.j_psi_surf), ...
           nuplo_data.cz(itheta_bot_S0_approx_from_Smax_deg,nuplo_data.j_psi_surf),'m^',[1 9],0);
  end
  axis equal
  hcol=colorbar;
  if ~isnumeric(hmode_distance.title_str) || hmode_distance.title_str~=-1, title(hmode_distance.title_str); end
  ylabel('contour of asinh(local shear) on (R,Z)')
end
%%
if ~isempty(findstr(opt_figs,'4'))
  ij=[nuplo_data.rshearp<00];
  nuplo_figs_handle(4) = figure;
  plotos(nuplo_data.crp(ij),nuplo_data.czp(ij),'o',[1 3],1);
  hold on
  plotos(nuplo_data.r0curv,nuplo_data.z0curv,'k-',[3 0]);
  plot(nuplo_data.zrsur,nuplo_data.zzsur,'k')
  if ~isnan(hmode_distance.theta_top_S0_deg)
    itheta_top_S0_deg = iround_os(nuplo_data.theta_psi_surf,hmode_distance.theta_top_S0_deg/180*pi);
    plotos(nuplo_data.cr(itheta_top_S0_deg,nuplo_data.j_psi_surf),nuplo_data.cz(itheta_top_S0_deg,nuplo_data.j_psi_surf),'mv',[1 9],1);
    itheta_bot_S0_deg = iround_os(nuplo_data.theta_psi_surf,hmode_distance.theta_bot_S0_deg/180*pi);
    plotos(nuplo_data.cr(itheta_bot_S0_deg,nuplo_data.j_psi_surf),nuplo_data.cz(itheta_bot_S0_deg,nuplo_data.j_psi_surf),'mv',[1 9],1);
  end
  if ~isnan(hmode_distance.theta_top_S0_approx_from_Smax_deg)
    itheta_top_S0_approx_from_Smax_deg = iround_os(nuplo_data.theta_psi_surf,hmode_distance.theta_top_S0_approx_from_Smax_deg/180*pi);
    plotos(nuplo_data.cr(itheta_top_S0_approx_from_Smax_deg,nuplo_data.j_psi_surf), ...
           nuplo_data.cz(itheta_top_S0_approx_from_Smax_deg,nuplo_data.j_psi_surf),'m^',[1 9],0);
    itheta_bot_S0_approx_from_Smax_deg = iround_os(nuplo_data.theta_psi_surf,hmode_distance.theta_bot_S0_approx_from_Smax_deg/180*pi);
    plotos(nuplo_data.cr(itheta_bot_S0_approx_from_Smax_deg,nuplo_data.j_psi_surf), ...
           nuplo_data.cz(itheta_bot_S0_approx_from_Smax_deg,nuplo_data.j_psi_surf),'m^',[1 9],0);
  end
  axis equal
  if ~isnumeric(hmode_distance.title_str) || hmode_distance.title_str~=-1, title(hmode_distance.title_str); end
  ylabel('local shear<0 and zero curvature on (R,Z)')
end
if ~isempty(findstr(opt_figs,'5'))
  nuplo_figs_handle(5) = figure;
  plot(180/pi*nuplo_data.theta_psi_surf_sort,nuplo_data.rshear_psi_surf_sort);
  ylabel(['local shear at psi=' num2str(hmode_distance.psi_surface)])
  xlabel('theta [deg]')
  hold on
  plotos(180/pi*nuplo_data.theta_psi_surf(nuplo_data.iSmaxtop_theta_psi_surf), ...
         nuplo_data.rshear(nuplo_data.iSmaxtop_theta_psi_surf,nuplo_data.j_psi_surf),'r*',[1 8]);
  plotos(180/pi*nuplo_data.theta_psi_surf(nuplo_data.iSmaxbot_theta_psi_surf), ...
         nuplo_data.rshear(nuplo_data.iSmaxbot_theta_psi_surf,nuplo_data.j_psi_surf),'r*',[1 8]);
  if ~isnan(hmode_distance.theta_top_S0_deg)
    itheta_top_S0_deg = iround_os(nuplo_data.theta_psi_surf,hmode_distance.theta_top_S0_deg/180*pi);
    plotos(180/pi*nuplo_data.theta_psi_surf(itheta_top_S0_deg),nuplo_data.rshear(itheta_top_S0_deg,nuplo_data.j_psi_surf),'mv',[1 8],1);
    itheta_bot_S0_deg = iround_os(nuplo_data.theta_psi_surf,hmode_distance.theta_bot_S0_deg/180*pi);
    plotos(180/pi*nuplo_data.theta_psi_surf(itheta_bot_S0_deg),nuplo_data.rshear(itheta_bot_S0_deg,nuplo_data.j_psi_surf),'mv',[1 8],1);
  end
  if ~isnan(hmode_distance.theta_top_S0_approx_from_Smax_deg)
    itheta_top_S0_approx_from_Smax_deg = iround_os(nuplo_data.theta_psi_surf,hmode_distance.theta_top_S0_approx_from_Smax_deg/180*pi);
    plotos(180/pi*nuplo_data.theta_psi_surf(itheta_top_S0_approx_from_Smax_deg),0.,'m^',[1 9],0);
    itheta_bot_S0_approx_from_Smax_deg = iround_os(nuplo_data.theta_psi_surf,hmode_distance.theta_bot_S0_approx_from_Smax_deg/180*pi);
    plotos(180/pi*nuplo_data.theta_psi_surf(itheta_bot_S0_approx_from_Smax_deg),0.,'m^',[1 9],0);
  end
  aa=axis;
  plotos([hmode_distance.theta_bot_0curv_deg hmode_distance.theta_bot_0curv_deg],[aa(3) aa(4)],'k--');
  plotos([hmode_distance.theta_top_0curv_deg hmode_distance.theta_top_0curv_deg],[aa(3) aa(4)],'k--');
  if ~isnumeric(hmode_distance.title_str) || hmode_distance.title_str~=-1, title(hmode_distance.title_str); end
end
%%
if ishandle(nuplo_figs_handle(3)), figure(nuplo_figs_handle(3)); end
