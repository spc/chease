function [syscode_formulas_out] = syscode_formulas(B0_in,Ip_in,R_in,Z_in,R0_in,a_minor_in,kappa_in,delta_in,w07_in);
%
% [syscode_formulas_out] = syscode_formulas(Ip_in,[Rin_in,Zin_in][,R0_in,a_minor_in,kappa_in,delta_in,Ip_in,w07_in]);
%
% Compute volume, surfaces, trapped fraction q95 from O. Sauter formulas for system codes paper
%
% Ip_in: if empty, assume q95=3 and use Ip formula, if given [A] use q95 formula
%
% Assume either
% R_in,Z_in plasma boundary is provided (in meters), hence compute R0, kappa, delta, a_minor, w07 to be used in the formulas
% or if R_in or Z_in is empty:
% use R0_in,a_minor_in,kappa_in,delta_in,w07_in. If one is empty, assume default value (1 usually)
%
% defaults (if any is empty and required):
%           B0 = 1 T
%           R0 = 1 m
%           a_minor from R0/a=3
%           kappa = 1.5
%           delta = 0.3
%           w07 = 1
%           q95 = 3
%
% Output:
%
%      syscode_formulas_out.R0, .a_minor, .epsilon, .kappa, .delta, .w07, .volume, .area_pol, .length_pol,
%                          .surface_phi, .q95, .Ip, .trapped_fraction, .Bp, .ksi
%      syscode_formulas_out.boundary.R/.Z :  as given or as determined by input kappa, delta, etc using Intor CHEASE formula
%      syscode_formulas_out.boundary.type : 'RZ given' if R_in,Z_in provided,
%                                           'INTOR CHEASE with ksi=xxx' if from standard formula and ksi from w07
%
%

% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

default_B0 = 1;
default_aspect_ratio = 3;
default_kappa = 1.5;
default_delta = 0.3;
default_w07 = 1.0;
default_q95 = 3;
%
if ~exist('B0_in') || isempty(B0_in)
  B0 = default_B0;
else
  B0 = B0_in;
end
%
compute_ip = 0;
if ~exist('Ip_in') || isempty(Ip_in)
  compute_ip = 1;
end

compute_RZ = 0;
if ~exist('R_in') || ~exist('Z_in') || numel(R_in)<=3 || numel(Z_in)<=3 || length(R_in)~=length(Z_in)
  compute_RZ = 1;
end

if compute_RZ==0
  % R_in, Z_in given, compute other parameters
  [R0,Z0,a_minor,kappa,delta_struct,w07,ksi,R07,Z07,w07_bottom,w07_top] = plasma_boundary_global_params(R_in,Z_in);
  delta = delta_struct.delta;
  syscode_formulas_out.boundary.R = R_in;
  syscode_formulas_out.boundary.Z = Z_in;
  syscode_formulas_out.boundary.type = 'RZ given';
else
  % R_in, Z_in not given, use input params
  if ~exist('R0_in') || isempty(R0_in)
    R0 = 1;
  else
    R0 = R0_in;
  end
  if ~exist('a_minor_in') || isempty(a_minor_in)
    a_minor = R0/default_aspect_ratio;
  else
    a_minor = a_minor_in;
  end
  if ~exist('kappa_in') || isempty(kappa_in)
    kappa = default_kappa;
  else
    kappa = kappa_in;
  end
  if ~exist('delta_in') || isempty(delta_in)
    delta = default_delta;
  else
    delta = delta_in;
  end
  if ~exist('w07_in') || isempty(w07_in)
    w07 = default_w07;
  else
    w07 = w07_in;
  end
  ksi_test = linspace(-0.5,0.6,111);
  theta_07LFS = asin(0.7) + (1.-sqrt(1+8.*ksi_test.^2))./4./ksi_test;
  w07anal = cos(theta_07LFS-ksi_test.*sin(2.*theta_07LFS))./sqrt(0.51).*(1-0.49./2.*delta.^2);
  % should be monotonic
  ksi = interp1(w07anal,ksi_test,w07);
  theta=linspace(0,2*pi,201);
  syscode_formulas_out.boundary.R = R0 + a_minor.*cos(theta+delta.*sin(theta)-ksi.*sin(2.*theta));
  syscode_formulas_out.boundary.Z = 0. + a_minor.*kappa.*sin(theta + ksi.*sin(2.*theta));
  syscode_formulas_out.boundary.type = ['std CHEASE bndry with R0, kappa, delta and ksi=' num2str(ksi)];
end

epsilon = a_minor/R0;

syscode_formulas_out.R0 = R0;
syscode_formulas_out.a_minor = a_minor;
syscode_formulas_out.epsilon = epsilon;
syscode_formulas_out.kappa = kappa;
syscode_formulas_out.delta = delta;
syscode_formulas_out.w07 = w07;
syscode_formulas_out.ksi = ksi;

syscode_formulas_out.length_pol = 2.*pi.*a_minor.*(0.45+0.55.*kappa).*(1+0.08.*delta.^2).*(1+0.2.*(w07-1));
syscode_formulas_out.area_pol = 2.*pi.*R0.*(1-0.32.*delta.*epsilon).*syscode_formulas_out.length_pol;
syscode_formulas_out.surface_phi = pi.*a_minor.^2.*kappa.*(1+0.52.*(w07-1));
syscode_formulas_out.volume = 2.*pi.*R0.*(1-0.25.*delta.*epsilon).*syscode_formulas_out.surface_phi;
if compute_ip == 0
  syscode_formulas_out.Ip = Ip_in;
  syscode_formulas_out.q95 = 4.1.*a_minor.^2.*B0./R0./(Ip_in/1e6).* ...
      (1+1.2.*(kappa-1)+0.56.*(kappa-1).^2).*(1+0.09.*delta+0.16.*delta.^2) ...
      .*(1+0.45.*delta.*epsilon)./(1-0.74.*epsilon).*(1+0.55.*(w07-1));
else
  syscode_formulas_out.q95 = default_q95;
  syscode_formulas_out.Ip = 1e6.*4.1.*a_minor.^2.*B0./R0./(default_q95).* ...
      (1+1.2.*(kappa-1)+0.56.*(kappa-1).^2).*(1+0.09.*delta+0.16.*delta.^2) ...
      .*(1+0.45.*delta.*epsilon)./(1-0.74.*epsilon).*(1+0.55.*(w07-1));
end
eps_eff = 0.67.*(1-1.4.*delta.*abs(delta)).*epsilon;
syscode_formulas_out.trapped_fraction = min(1.,1.-(1.-eps_eff)./(1.+2.*sqrt(eps_eff)).*sqrt((1.-epsilon)./(1.+epsilon)));
syscode_formulas_out.Bp = syscode_formulas_out.Ip/1e6./5./(syscode_formulas_out.length_pol./2./pi);
