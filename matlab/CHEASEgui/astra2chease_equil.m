function [fname_chease_out,chease_data,EXPEQdata] = astra2chease_equil(filename_astra,varargin)
%function [fname_chease_out,chease_data] = astra2chease_equil(filename_astra,{astratime,doplot,ask_for_tension})
% runs CHEASE from ASTRA data
%
% INPUTS
%  filename_astra : path to .mat file containing ASTRA data (as saved by astra_tcv.m)
%
%  astratime      : index of time in astra .mat file to use as equilibrium
%                   (default: last time), see help astra2expeq for details
%                   >0: index thus time = astra.T(astratime);
%                   <0: time to use is abs(astratime) thus time = abs(astratime)
%                   'a': char then plots Ip(t) and users chooses time from plot
%  doplot         : if set to 1, plots comparison of EXPEQ and EXPEQ.OUT,
%                   and q profiles. No plots if set to 0. (default: 1)
%
%  ask_for_tension: 0 (default) do not ask if ok and option to change tension
%                   1: do prompt if ok
%
% OUTPUTS
%  fname_chease_out: cell array of strings containing path of CHEASE output files
%  chease_data     : matlab variable containing o.cols readout
%
% Procedure:
%  *Creates EXPEQ using [astra2expeq.m] (user must confirm spline fits)
%  *Sets CHEASE namelist using [write_namelist_chease.m]
%  *runs CHEASE using [run_chease_expeq.m]
%  *reads o.cols file [using read_ocols.m]
%  *plots EXPEQ files and compares output q profiles (optionally) [using plot_expeq.m]
%

% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------


astratime = []; % default
if nargin >= 2
  if ~isempty(varargin{1})
    astratime=varargin{1};
  end;
end

doplot = 1;
if nargin >=3;
  if ~isempty(varargin{2})
    doplot = varargin{2};
  end;
end

ask_for_tension=0;
if nargin >=4;
  if ~isempty(varargin{3})
    ask_for_tension = varargin{3};
  end;
end

% set up tmpdir, create if necessary
[s,uname] = unix('whoami'); tmpdir = ['/tmp/',deblank(uname),'/'];
if ~exist(tmpdir,'dir'), unix([ 'mkdir ',tmpdir]); end

%filename_astra = 'shots/38364_z0_eITB_RES.mat';

[astrafilepath,astrafname] = fileparts(filename_astra);
nsttp_jb_over_B0=4;
[fnames_out,EXPEQdata] = astra2expeq(filename_astra,astratime,astrafname,astrafilepath,nsttp_jb_over_B0,doplot,ask_for_tension);

TCV_astra = load(filename_astra); % for B0exp, IpkA
s = TCV_astra.out;
fname_expeq = fnames_out{1}; fname_exptnz = fnames_out{2};

% APPEND STUFF TO STANDARD NAMELIST
nl.Title = astrafname;
nl.IpkA = s.IPL(end) * 1000;
nl.r0exp =  s.RTOR(end);
nl.b0exp =  s.BTOR(end);
% TODO: This might not be the same time as used in astra2expeq

nl.nsttp = nsttp_jb_over_B0; % given I//
nl.npropt = -nsttp_jb_over_B0; % selector for reading experimental current profile -> I//
nl.neqdsk = 0; % no eqdsk but expeq is used as input

% %%%%%% COMPUTATION OF RHOPSI (already done in astra2expeq) %%%%%

% To determine poloidal flux at rho=0. I have to interpolate, since ametr(1)~=0, rho(1)~=0 etc...
% must use trick to get interpos to give me the zero derivative at rho=0; see interpos help
xin_eff = [0;s.AMETR(:,end)]; yin_eff = [s.FP(1,end);s.FP(:,end)];
yout_eff = interpos(13,xin_eff,yin_eff,xin_eff,1e-9,[1 0],[0. 0.],[100;ones(size(s.AMETR(:,end)))]);
% modified by Antoine Merle to avoid complex values of rhopsi
yout_eff(1) = min(yout_eff(1:2));
psi_axis = yout_eff(1);
psi_edge = s.FP(end,end);
rhopsi = sqrt((s.FP(:,end)-psi_axis)./(psi_edge-psi_axis));



% Rescale q95
nl.ncscal = 1; % scale to given q
q95 = interp1(rhopsi,1./s.MU(:,end),sqrt(0.95));
%disp(['Fixed q_95=',num2str(q95),' in CHEASE namelist']) ;
nl.qspec = q95; nl.csspec = sqrt(0.95); % fix q95 = q(psi=95%) = q(rho_psi =sqrt(.95));

% cannot rescale q outside rho=0 if nsttp=3 or 4 (should adapt chease) and cannot rescale Ip, so no rescale(4) or q0 (1)
nl.ncscal = 4;
nl.currt = s.IPL(end) * 1e6 *mu0/nl.r0exp/nl.b0exp;
nl.qspec = EXPEQdata.q(1); nl.csspec = 0.;

nl.nbsexpq=1111; % to use EXPTNZ

fname_namelist = [tmpdir,'astra_chease_namelist'];
write_namelist_chease(fname_namelist,nl);

% run chease
try
  [fname_chease_out,globalsvalues] = run_chease_expeq(fname_namelist,fname_expeq,fname_exptnz);
  if isempty(fname_chease_out)
    disp('problems with CHEASE')
    if nargout >=2; chease_data = []; end
    if nargout >=3; EXPEQdata = []; end
    return
  end
catch ME
  getReports(ME)
  disp('problems with CHEASE')
  fname_out = [];
  return
end
EXPEQdata.aux
globalsvalues
aaa=regexpi(fname_chease_out,'o.*\.cols$');
ij=[];
for i=1:length(aaa)
  if ~isempty(aaa{i}); ij=i; end
end
chease_data=read_ocols(fname_chease_out{ij});

% search for EXPEQ.OUT amongst in outputs (to plot)
for ifile = 1:length(fname_chease_out)
  if ~isempty(strfind(fname_chease_out{ifile},'EXPEQ.OUT')) && ...
        isempty(strfind(fname_chease_out{ifile},'TOR')); % do not plot the EXPEQ.TOR but the first EXPEQ
    expeqfile_out = fname_chease_out{ifile}; break;
  end
end

if doplot
  figure
  plot_expeq(fname_expeq,'b'); hold on; plot_expeq(gcf,expeqfile_out,'r'); legend('IN','OUT');
  figure
  plot(rhopsi,1./s.MU(:,end),'b');hold on;
  plot(chease_data.s_mesh.data,chease_data.qprofile.data,'r');
  ylabel('q'); xlabel('rho_\psi');
  legend('astra input','chease output')
end
