function [object] = plotos(x,y, mark, size,iopt, color, varargin);
%
% function [object] = plotos(x,y, mark, size,iopt, color,varargin);
%
% new: can plot on given handle of axes ala plot(hh,x,y,...)
%           plotos(hh,x,y,...); if hh is an axes handles
%
% to plot(x,y) with type of line and marker given by mark
%
% object returns handle to the plot
%
% size,iopt, color are optional:
%
% size <= 0: use default size
% size > 0 : set size of marker
% if size of length 2, then:
%     size_line = size(1)  (use 0 to keep default value)
%     size_marker = size(2)  (use 0 to keep default value)
%
% iopt = 0: open symbol
% iopt = 1: solid symbol
%
% color can be introduced in mark or not, it will superseed the color set in
% variable mark
%
% Typical calls are:
%
% plotos(x,y);
% plotos(x,y,'ro-',8,1);                       with filled marker of size 8
% plotos(x,y,'r-',[2 0]);                      To plot with line size=2
% hh=plotos(x,y,'ro',8,1,'[0.2 0.5 0.8]);    with specific color
%

% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

if nargin == 0;
  return
end
hplot = [];
nargin_eff = nargin;
try
  if numel(x)==1 && any(ishandle(x)) && strcmp(get(x,'type'),'axes')
    nargin_eff = nargin-1;
    hplot = x;
    if nargin >= 2; x = y; end
    if nargin >= 3; y = mark; end
    if nargin >= 4; mark = size; end
    if nargin >= 5; size = iopt; end
    if nargin >= 6; iopt = color; end
    if nargin >= 7; color = varargin{1}; end
  end
catch ME
  throw(ME)
end

if nargin_eff == 1;
  if isempty(hplot)
    object=plot(x);
  else
    object=plot(hplot,x);
  end
elseif nargin_eff == 2;
  if isempty(hplot)
    object=plot(x,y);
  else
    object=plot(hplot,x,y);
  end
else
  if isempty(hplot)
    object=plot(x,y,mark);
  else
    object=plot(hplot,x,y,mark);
  end
end

% set sizes

if nargin_eff <= 3 ;
  return
elseif nargin_eff >= 4;
  size_line = 0;
  size_mark = 0;
  if length(size) == 2;
    size_line = size(1);
    size_mark = size(2);
  else
    if size > 0;
      size_mark = size;
    end
  end
  if size_line > 0;
    set(object,'LineWidth',size_line);
  end
  if size_mark > 0;
    set(object,'MarkerSize',size_mark);
  end
end

% set open/solid and colors

if nargin_eff == 5;
  if iopt == 1;
    obj_col = get(object,'Color');
    set(object,'MarkerFaceColor',obj_col);
  end;
elseif nargin_eff == 6;
  set(object,'Color',color);
  if iopt == 1;
    set(object,'MarkerFaceColor',color);
  end
end
