function chease_output_struct = read_ocols(varargin)
%
% function chease_output_struct = read_ocols(varargin)
% function chease_output_struct = read_ocols(file_dot_cols,file_dot_RZcols)
%
% reads .cols file generated from CHEASE output file by unix script:
%    o.chease_to_cols o.chease_output o.chease_output.cols
%
% New: the .RZcols file is also created and can be given as 3rd argument to read the RZ plasma boundary
%
% Thus you get chease_output_struct.data and chease_output_struct.data_rz as well as chease_output_struct.r_plasma_boundary for example
%
% to plot, use plot_ocols.m

% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

Swarningstate=warning('query');
oncleanupObj = onCleanup(@() warning(Swarningstate));
warning('off','all');

if nargin == 0
  [fname,fpath]=uigetfile('*.cols','chease_output_struct.cols file');
  file = fullfile(fpath,fname);
else
  file = [];
  file_rz = [];
  if nargin>=1 && ~isempty(varargin{1})
    file = varargin{1};
    if ~exist(file,'file'); error([file,' does not exist']); end
  end
  if nargin>=2 && ~isempty(varargin{2})
    file_rz = varargin{2};
    if ~exist(file_rz,'file'); error([file_rz,' does not exist']); end
  end
end

data = [];
labels = [];
if ~isempty(file)
  try
    data=load(file);
  catch
    error('error loading file, are you sure you selected the .cols file?');
  end
  % read comment line to get labels
  fid = fopen(file);
  [aa]=textscan(fid,'%[^\n]',1); % reads first line (assume starts with comment char)
  fclose(fid);
  if aa{1}{1}(1)=='%'
    labels=textscan(aa{1}{1}(2:end),'%s');
    labels = labels{1};
  else
    error('first line does not start with comment character %');
  end
  if isempty(labels); error('error reading labels from .cols file'); end

  chease_output_struct=struct;
  for tel=1:length(labels);
    varvalue = data(:,tel);
    fieldname = labels{tel};
    [fieldname,mesh] = fieldname_adapt_to_var(fieldname);
    newfield =struct('data',varvalue,'mesh',mesh,'label',labels{tel});
    chease_output_struct.(fieldname)=newfield;
  end
end

data_rz = [];
labels_rz = [];
if ~isempty(file_rz)
  try
    data_rz=load(file_rz);
  catch
    error('error loading file_rz (varargin{2}), are you sure you selected the .RZcols file_rz?');
  end
  % read comment line to get labels
  fid = fopen(file_rz);
  [aa]=textscan(fid,'%[^\n]',1); % reads first line (assume starts with comment char)
  fclose(fid);
  if aa{1}{1}(1)=='%'
    labels_rz=textscan(aa{1}{1}(2:end),'%s');
    labels_rz = labels_rz{1};
  else
    error('first line does not start with comment character %');
  end
  if isempty(labels_rz); error('error reading labels_rz from .cols file_rz'); end
  for tel=1:length(labels_rz);
    varvalue = data_rz(:,tel);
    fieldname = labels_rz{tel};
    [fieldname,mesh] = fieldname_adapt_to_var(fieldname);
    newfield =struct('data',varvalue,'mesh',mesh,'label',labels_rz{tel});
    chease_output_struct.(fieldname)=newfield;
  end
end

%%% OS: No need to extract since in data and labels already?

% $$$ fields = fieldnames(chease_output_struct);
% $$$ % extract data and fields from structure
% $$$ for tel=1:length(fields)-2
% $$$   data(:,tel) = getfield(chease_output_struct,fields{tel},'data');
% $$$   labels{tel} = getfield(chease_output_struct,fields{tel},'label');
% $$$ end

chease_output_struct.data=data;
chease_output_struct.labels=labels;
chease_output_struct.data_rz=data_rz;
chease_output_struct.labels_rz=labels_rz;

function [fieldname_new,mesh] = fieldname_adapt_to_var(fieldname_in);
%
% transform label into a fieldname ala: <xxx> becomes av_xxx_av
%

fieldname_new = fieldname_in;
if isempty(fieldname_in)
  return
end
%
if ~isempty(strfind(fieldname_new,'(CSM)'))
  mesh = ('CSM');
  fieldname_new = strrep(fieldname_new,'(CSM)','');
elseif ~isempty(strfind(fieldname_new,'(CS)'))
  mesh = ('CS');
  fieldname_new = strrep(fieldname_new,'(CS)','');
else
  mesh = '';
end
fieldname_new = lower(fieldname_new);
fieldname_new = strrep(fieldname_new,'-','_');
fieldname_new = strrep(fieldname_new,'**','pow');
fieldname_new = strrep(fieldname_new,'*','x');
fieldname_new = strrep(fieldname_new,'//','_par');
fieldname_new = strrep(fieldname_new,'/','_over_');
fieldname_new = strrep(fieldname_new,'|','_');
fieldname_new = strrep(fieldname_new,'(','_');
fieldname_new = strrep(fieldname_new,')','_');
fieldname_new = strrep(fieldname_new,'<','av_');
fieldname_new = strrep(fieldname_new,'>','_av');
fieldname_new = strrep(fieldname_new,'[','_');
fieldname_new = strrep(fieldname_new,']','_');
fieldname_new = strrep(fieldname_new,'=','_eq_');
fieldname_new = strrep(fieldname_new,'...','_');
fieldname_new = strrep(fieldname_new,'.','_dot_');
fieldname_new = strrep(fieldname_new,',','_');
fieldname_new = strrep(fieldname_new,'''','_');
fieldname_new = strrep(fieldname_new,':','_');
fieldname_new = strrep(fieldname_new,'^','pow');
fieldname_new = strrep(fieldname_new,'&','_');
fieldname_new = strrep(fieldname_new,'___','_');
fieldname_new = strrep(fieldname_new,'__','_');
fieldname_new = regexprep(fieldname_new,'^1\+','X1p');
fieldname_new = regexprep(fieldname_new,'^1','X1');
while strcmp(fieldname_new(end),'_'); fieldname_new=fieldname_new(1:end-1); end;
