#!/usr/bin/env python
# Copyright 2024 SPC-EPFL
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
import numpy as np
import h5py
import os
# Sets default backend based on whether DISPLAY is set
import matplotlib_interactive
DEFAULT_INTERACTIVE = matplotlib_interactive.DEFAULT_INTERACTIVE
import matplotlib.pyplot as plt
import argparse

# Define some functions to make working with figures nicer
def interactive_show_or_save(fig, plot_filename, figlist, interactive,
                             close_after_saving=True, **kwargs):
    """
    Either show the figure (non-blocking) (and append to figlist) or save the
    figure, depending on the truthness of interactive
    """
    if interactive:
        plt.show(block=False)
        figlist.append((fig, plot_filename))
    else:
        fig.savefig(plot_filename, **kwargs)
        if close_after_saving:
            plt.close(fig)

def interactive_hold_and_save(figlist, **kwargs):
    """
    Given a list of figures ask if the user wants to save them.
    Exception: if the length of the list is 0, do nothing
    """
    if len(figlist) == 0:
        return
    try:
        savefig = int(input("save (1/0)"))
        if savefig:
            print("Trying to save...")
            for (fig, plot_filename) in figlist:
                fig.savefig(plot_filename)
        else:
            print("Not saving")
    except ValueError as e:
        print("Not saving")

def plot_ring(sval, ax, sgrid, rgrid, zgrid, rstom=1., **plot_kwargs):
    """
    Add a ring at s=sval to the passed (R,Z) axis.
    rstom is an optional scaling factor
    """
    sind = np.argmin(np.abs(sgrid - sval))
    try:
        sint = np.where(sgrid - sval <= 0)[0][-1]
        sfrac = (sval - sgrid[sint])/(sgrid[1] - sgrid[0])
    except IndexError:
        return
    try:
        rgrid_loc = ((1.0 - sfrac) * rgrid[:,sint] + sfrac * rgrid[:,sint+1]) / rstom
        zgrid_loc = ((1.0 - sfrac) * zgrid[:,sint] + sfrac * zgrid[:,sint+1]) / rstom
        ax.plot(rgrid_loc, zgrid_loc, **plot_kwargs)
    except IndexError:
        pass

def plot_chi_line(chival, ax, chigrid, rgrid, zgrid, rstom=1., **plot_kwargs):
    """
    Plot a line of constant chi=theta* to the passed (R,Z) axis.
    rstom is an optional scaling factor
    """
    chiind = np.argmin(np.abs(chigrid - chival))
    try:
        chiint = np.where(chigrid - chival <= 0)[0][-1]
        chifrac = (chival - chigrid[chiint])/(chigrid[1] - chigrid[0])
    except IndexError:
        return
    try:
        rgrid_loc = ((1.0 - chifrac) * rgrid[chiint,:] + chifrac * rgrid[chiint+1,:]) / rstom
        zgrid_loc = ((1.0 - chifrac) * zgrid[chiint,:] + chifrac * zgrid[chiint+1,:]) / rstom
        ax.plot(rgrid_loc, zgrid_loc, **plot_kwargs)
    except IndexError:
        pass

def get_norm_labels(norm="SI"):
    """
    Return the units of R, B given the string norm
    """
    norm_dict = {
        "si".lower(): {"R":"m", "B":"T"},
        "rho_s": {"R":r"$\rho_s$", "B": "$B_0$"}
    }
    return norm_dict.get(norm.lower(), {})

def add_ghost(array, axis=0):
    """
    Extend a 2D array by 1 in a given axis, with the added row/col taking the
    value of the first row/col.
    Used for filling the missing wedges in angular quantities when the range is
    specified for [0,2pi)
    """
    if axis == 0:
        new_array = np.zeros(np.array(array.shape)+np.array([1,0]))
        new_array[:-1,:] = array[:,:]
        new_array[-1,:]  = array[0,:]
    elif axis == 1:
        new_array = np.zeros(np.array(array.shape)+np.array([0,1]))
        new_array[:,:-1] = array[:,:]
        new_array[:,-1]  = array[:,0]
    return new_array

class chease_eq(object):
    """
    A Representation of a chease ogyropsi.h5 file.
    With plotting:
        * (2D, X(RZ) (default X is |B|))
        * q(r)
    """
    def __init__(self, dir=".", filename="ogyropsi.h5"):
        self.dir = dir
        self.filename = filename

    def load_eq(self):
        """
        Read R(s,c), Z(s,c), B(s,c) from hdf5 file
        Also attempt to read:
            q,
            s=sqrt(PSI/PSI[-1),
            rho_tor=rho_tor/rho_tor[-1],
            Jacobian(s,c)
        """
        with h5py.File(os.path.join(self.dir, self.filename), "r") as h5_obj:
            self.R = h5_obj["/data/var2d/R"][:]
            self.Z = h5_obj["/data/var2d/Z"][:]
            self.B = h5_obj["/data/var2d/B"][:]
            try:
                self.q = h5_obj["/data/var1d/q"][:]
                PSI = h5_obj["/data/grid/PSI"][:]
                PSI_n = PSI / PSI[-1]
                self.s = np.sqrt(PSI_n)
                rho_tor_u = h5_obj["/data/var1d/rho_tor"][:]
                self.rho_tor = rho_tor_u/rho_tor_u[-1]
                self.Jacobian = h5_obj["/data/var2d/Jacobian"][:]
            except KeyError as e:
                print("Following error when loading non-mandatory chease quantity:")
                print(e)

    def plot(self, outline=True, label=True, fig=None, ax=None,
             plot_data=None, quantity="B", norm="SI", contours=10, mark_axis=False):
        """
        2D R,Z plot of |B|. Optionally instead pass in tuple plot_data=(X(s,c), 'label')
        """
        if ax is None:
            fig, ax = plt.subplots()
        R = add_ghost(self.R)
        Z = add_ghost(self.Z)

        if plot_data is None:
            plot_label = None
            plot_data = add_ghost(self.B)
        else:
            # Pass in (data, label) as tuple
            plot_label = plot_data[1]
            plot_data = add_ghost(plot_data[0])

        pc = ax.pcolormesh(R, Z, plot_data, shading="auto")
        if outline:
            ax.plot(R[:,-1], Z[:,-1], "k-")
        if contours:
            contour_line = ax.contour(R, Z, plot_data,
                                      levels=np.linspace(*pc.get_clim(), contours+1),
                                      colors="k", linewidths=0.1)

        ax.set_aspect("equal")
        ax.set_xlim(np.array([-0.025, 0.025])+ax.get_xlim())
        ax.set_ylim(np.array([-0.025, 0.025])+ax.get_ylim())
        cbar = plt.colorbar(pc, ax=ax)

        norms = get_norm_labels(norm)
        R_label = norms.get("R", "a.u.")
        if plot_label is None:
            B_label = rf"B [{norms.get('B', 'a.u.')}]"
        else:
            B_label = plot_label

        ax.set_xlabel(r"R [{}]".format(R_label))
        ax.set_ylabel(r"Z [{}]".format(R_label))
        cbar.ax.set_ylabel(B_label)

        if contours:
            [cbar.ax.axhline(i, color="k", linewidth=0.1) for i in contour_line.levels]
        if mark_axis:
            cbar.ax.axhline(plot_data[0,0], color="m", linestyle="--", linewidth=2.)
        if fig is not None:
            fig.tight_layout()
        return fig, ax

    def plot_q(self, fig=None, ax=None, xunit="s", **kwargs):
        """
        Plot q(x). Default 'x' is s=sqrt(Psi_norm), but if xunit='rhot', we get q(rho_t_norm)
        """
        if ax is None:
            fig, ax = plt.subplots()
        xgrid_dict = {"s": (self.s, "s"), "rhot": (self.rho_tor, r"\rho_{t}")}
        xdata, xlabel = xgrid_dict.get(xunit.lower(), (self.s, "s"))
        ax.plot(xdata, self.q, **kwargs)
        ax.set_xlabel(f"${xlabel}$")
        ax.set_ylabel("$q$")
        return fig, ax

    def plot_psi(self, fig=None, ax=None, npsi=10):
        """
        Add npsi flux surfaces (equispaced in s) to the passed (R,Z) axis
        """
        R = add_ghost(self.R)
        Z = add_ghost(self.Z)

        sgrid = np.linspace(0., 1., R.shape[0])

        slist = np.linspace(0, 1, npsi+1)[1:-1]
        slist = [spos for spos in slist if spos >= sgrid[0] and spos <= sgrid[-1]]
        for sval in slist:
            plot_ring(sval, ax, sgrid, R, Z, color="k", linewidth=0.5)

    def plot_chi(self, fig=None, ax=None, nchi=8):
        """
        Add nchi lines of equispaced chi=theta* to the passed (R,Z) axis
        """
        R = np.copy(self.R)
        Z = np.copy(self.Z)

        chigrid = np.linspace(0., 2.*np.pi, R.shape[1], endpoint=False)

        chilist = np.linspace(0., 2.*np.pi, nchi, endpoint=False)
        for chival in chilist:
            plot_chi_line(chival, ax, chigrid, R, Z, color="k", linewidth=0.5)

if __name__ == "__main__":
    all_figs = []
    parser = argparse.ArgumentParser()
    parser.add_argument("-filename", type=str, default="ogyropsi.h5")
    parser.add_argument("-interactivePlots", type=int, default=DEFAULT_INTERACTIVE)
    args = parser.parse_args()
    interactive = args.interactivePlots

    cheq = chease_eq(filename=args.filename)
    cheq.load_eq()

    try:
        print("2a = ", cheq.R.max() - cheq.R.min())
        print("B[0,0] = ", cheq.B[0,0])
        print("R[0,0] = ", cheq.R[0,0])
    except AttributeError as e:
        print(e)

    fig, ax = cheq.plot()
    cheq.plot_psi(ax=ax, npsi=10)
    plot_filename = "chease_B_plot_psi.png"
    interactive_show_or_save(fig, plot_filename, all_figs,
                             interactive)

    fig, ax = cheq.plot()
    cheq.plot_psi(ax=ax, npsi=10)
    cheq.plot_chi(ax=ax, nchi=8)
    plot_filename = "chease_B_plot_psi_chi.png"
    interactive_show_or_save(fig, plot_filename, all_figs,
                             interactive, dpi=300)

    fig, ax = cheq.plot_q(xunit="s", label=r"$q(s)$")
    plot_filename = "chease_q.pdf"
    interactive_show_or_save(fig, plot_filename, all_figs, interactive,
                             close_after_saving=False)

    fig, ax = cheq.plot_q(xunit="s", label=r"$q(\rho_p)$")
    fig, ax = cheq.plot_q(fig=fig, ax=ax, xunit="rhot", label=r"q($\rho_t$)")
    ax.set_xlabel("Radial coordinate")
    ax.legend()
    plot_filename = "chease_q_rhot.pdf"
    interactive_show_or_save(fig, plot_filename, all_figs, interactive)

    interactive_hold_and_save(all_figs)
