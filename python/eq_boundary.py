#!/usr/bin/env python
# Copyright 2024 SPC-EPFL
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
import numpy as np
import matplotlib.pyplot as plt
import argparse

def delta_loc_def(eps=0.3, R0=1., R_max=1.3):
    a = R0 * eps
    return (R0 - R_max) / a

def delta_loc_get(r, z, eps, verbose=False):
    Zmin_ind = np.argmin(z)
    Zmax_ind = np.argmax(z)
    R_zmin = r[Zmin_ind]
    R_zmax = r[Zmax_ind]
    delta_u_def = delta_loc_def(eps=eps, R0=1., R_max=R_zmax)
    delta_l_def = delta_loc_def(eps=eps, R0=1., R_max=R_zmin)
    if verbose:
        print(f"'local' delta_l = {delta_l_def}")
        print(f"'local' delta_u = {delta_u_def}")
    return delta_l_def, delta_u_def

def rz_1(theta, eps=0.3, kappa=1.5, delta=0.6, xi=0.1):
    r_1 = 1. + eps * np.cos(theta+delta * np.sin(theta) - xi * np.sin(2*theta))
    z_1 = 0.0 + kappa * eps * np.sin(theta+xi * np.sin(2 * theta))
    return r_1, z_1

def get_rz(theta, eps=0.3, kappa_l=0.1, kappa_u=0.1, delta_l=0.1, delta_u=0.1, xi_l=0.0, xi_u=0.0):
    r1, z1 = rz_1(theta[theta <  np.pi], eps=eps, delta=delta_u, kappa=kappa_u, xi=xi_u)
    r2, z2 = rz_1(theta[theta >= np.pi], eps=eps, delta=delta_l, kappa=kappa_l, xi=xi_l)
    r = np.append(r1, r2)
    z = np.append(z1, z2)
    return r, z

def plot_params(theta, eps=0.3, kappa_l=1.1, kappa_u=1.0, delta_l=0.0,
                delta_u=0.0, xi_l=0.0, xi_u=0.0, nest=False, verbose=True,
                ax=None):
    if ax is None:
        fig, ax = plt.subplots()

    if not nest:
        eps_list = [eps]
    else:
        eps_list = np.linspace(0.0, eps, 11)[1:]

    for ieps, eps_val in enumerate(eps_list[::-1]):
        r, z = get_rz(theta, eps=eps_val,
                      kappa_l=kappa_l, kappa_u=kappa_u,
                      delta_l=delta_l, delta_u=delta_u,
                      xi_l=xi_l, xi_u=xi_u)
        ax.plot(r, z)
        if ieps == 0:
            if r.min() < ax.get_xlim()[0] or r.max() > ax.get_xlim()[1]:
                ax.set_xlim([r.min()-0.1, r.max()+0.1])
            if z.min() < ax.get_ylim()[0] or z.max() > ax.get_ylim()[1]:
                ax.set_ylim([z.min()-0.1, z.max()+0.1])
            if verbose:
                delta_loc_l, delta_loc_u = delta_loc_get(r, z, eps)
                print(f"'local' delta_l = {delta_loc_l}")
                print(f"'local' delta_u = {delta_loc_u}")
    return ax


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Tool for parameterized descriptions of plasma equilibrium boundary")
    parser.add_argument("-eps", type=float, default=0.1, help="Aspect ratio")
    parser.add_argument("-kappa", type=float, default=1., help="Elongation (if not both of kappa_l or kappa_u are provided")
    parser.add_argument("-kappa_l", type=float, help="Lower elongation (kappa_u must also be provided)")
    parser.add_argument("-kappa_u", type=float, help="Upper elongation (kappa_l must also be provided)")
    parser.add_argument("-delta", type=float, default=0., help="Triangularity (if not both of delta_l or delta_u are provided")
    parser.add_argument("-delta_l", type=float, help="Lower triangularity (delta_u must also be provided)")
    parser.add_argument("-delta_u", type=float, help="Upper triangularity (delta_l must also be provided)")
    parser.add_argument("-xi", type=float, default=0., help="Squareness (if not both of xi_l or xi_u are provided")
    parser.add_argument("-xi_l", type=float, help="Lower squareness (xi_u must also be provided)")
    parser.add_argument("-xi_u", type=float, help="Upper squareness (xi_l must also be provided)")
    parser.add_argument("-nest", type=int, default=1, help="Whether to plot multiple nested surfaces when plotting eq.")
    args = parser.parse_args()

    if args.kappa_l is None or args.kappa_u is None:
        if (args.kappa_l is None) ^ (args.kappa_u is None):
            print(f"One of kappa_l, kappa_u not provided, taking kappa_{{l,u}}={args.kappa}")
        kappa_l = args.kappa
        kappa_u = args.kappa
    else:
        kappa_l = args.kappa_l
        kappa_u = args.kappa_u

    if args.delta_l is None or args.delta_u is None:
        if (args.delta_l is None) ^ (args.delta_u is None):
            print(f"One of delta_l, delta_u not provided, taking delta_{{l,u}}={args.delta}")
        delta_l = args.delta
        delta_u = args.delta
    else:
        delta_l = args.delta_l
        delta_u = args.delta_u

    if args.xi_l is None or args.xi_u is None:
        if (args.xi_l is None) ^ (args.xi_u is None):
            print(f"One of xi_l, xi_u not provided, taking xi_{{l,u}}={args.xi}")
        xi_l = args.xi
        xi_u = args.xi
    else:
        xi_l = args.xi_l
        xi_u = args.xi_u

    eps = args.eps

    fig, ax = plt.subplots()
    theta = np.linspace(0, 2.0*np.pi, 1001)
    _ = plot_params(theta, eps=eps,
                    delta_l=delta_l, delta_u=delta_u,
                    kappa_l=kappa_l, kappa_u=kappa_u,
                    xi_l=xi_l, xi_u=xi_u,
                    nest=args.nest, ax=ax)
    ax.set_aspect(1.0)
    ax.set_xlabel("R")
    ax.set_ylabel("Z")
    plt.show()
