# Copyright 2024 SPC-EPFL
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# Care should be taken to set a non-interactive (Agg) matplotlib backend if #
# $DISPLAY is not set
#
# Similarly, select an interactive backend if $DISPLAY is set, but the default
# is non-interactive (as is the case for matplotlib installed with EasyBuild,
# for example (as done at ITER)).

from os import environ
import matplotlib

if "DISPLAY" not in environ:
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt
    DEFAULT_INTERACTIVE=0
else:
    import matplotlib.pyplot as plt
    DEFAULT_INTERACTIVE=1
    if not matplotlib.is_interactive():
        plt.switch_backend(matplotlib.rcsetup._auto_backend_sentinel)
