# Copyright 2024 SPC-EPFL
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------

# -----------------------------
# PYTHON SCRIPT TO CALL CHEASE
# -----------------------------

# export PYTHONPATH=~/public/PYTHON_ACTORS/chease:$PYTHONPATH

# NEEDED MODULES
import os,imas,yaml
import numpy as np
from chease.wrapper import chease_actor as chease

# INPUT/OUTPUT CONFIGURATION
curdir = os.getenv('PWD')
run_py_dir = os.path.dirname(os.path.realpath(__file__))
input_scenario = run_py_dir + "/../input/scenario.yaml"

file = open(input_scenario, 'r')
config = yaml.load(file,Loader=yaml.CLoader)
file.close()
shot                = config['shot']
run_in              = config['run_in']
input_user_or_path  = config['input_user_or_path']
input_database      = config['input_database']
if config['output_user_or_path'] == 'default':
    output_user_or_path = os.getenv('USER')
else:
    output_user_or_path = config['output_user_or_path']
output_database     = config['output_database']
run_out             = config['run_out']
dt_required         = config['dt_required']
time_slice          = config['time_slice']
ntimes              = config['ntimes']

# TO SUPPRESS THE ACCESS LAYER WARNINGS ABOUT OBSOLETE DATA
import logging
log = logging.getLogger()
log.setLevel(logging.ERROR)

# DISPLAY SIMULATION INFORMATION
print('---------------------------------')
print('shot                = ',shot)
print('run_in              = ',run_in)
print('run_out             = ',run_out)
print('input_user_or_path  = ',input_user_or_path)
print('input_database      = ',input_database)
print('output_user_or_path = ',output_user_or_path)
print('output_database     = ',output_database)
print('time_slice          = ',time_slice)
print('ntimes              = ',ntimes)
print('dt_required         = ',dt_required,' s')
print('---------------------------------')

# FUNCTION TO FIND INDEX OF NEAREST TIME SLICE IN TIME ARRAY
def find_nearest(a, a0):
    "Element in nd array `a` closest to the scalar value `a0`"
    idx = np.abs(a - a0).argmin()
    return a.flat[idx],idx

# READ INPUT IDSS
print('=> Open input datafile')
input = imas.DBEntry(imas.imasdef.MDSPLUS_BACKEND,input_database,shot,run_in,input_user_or_path)
input.open()

# READ FULL TIME VECTOR OF EQUILIBRIUM IDS TO GET THE TIME BASE
print('=> Open input datafile and to read time array from equilibrium IDS')
time_array = input.partial_get(ids_name='equilibrium',data_path='time')
if time_slice == 0:
    ntimes = len(time_array)
    it = 0
    print('-----------------------------------------------------')
    print('Use the whole time array of the input equilibrium IDS')
    print('- t_init = ',time_array[0],'s')
    print('- ntimes = ',ntimes)
    print('-----------------------------------------------------')
elif time_slice < 0:
    it = 0
    time_slice = time_array[0]
    if ntimes > len(time_array):
      time_array = np.linspace(time_array[0],time_array[0]+ntimes*dt_required,ntimes)
      print('-------------------------------------------------------------------------------------')
      print('Use the first time slice of the input equilibrium IDS with ntimes defined by the user')
      print('... However ntimes is greater than the input time array')
      print('--> Artificial time array generated with dt defined by the user:')
      print('    - t_init = ',time_array[it],'s')
      print('    - ntimes = ',ntimes)
      print('    - dt_required = ',dt_required)
      print('-------------------------------------------------------------------------------------')
    else:
        print('--------------------------------------------------------------------------------------')
        print('Use the first time slice of the input equilibrium IDS with ntimes defined by the user:')
        print('- t_init = ',time_array[0],'s')
        print('- ntimes = ',ntimes)
        print('--------------------------------------------------------------------------------------')
else:
  # FIND INDEX OF NEAREST TIME SLICE IN TIME ARRAY
  [tc,it] = find_nearest(time_array,time_slice)
  if ntimes > len(time_array) or it+ntimes>len(time_array):
      time_array = np.linspace(time_array[it],time_array[it]+ntimes*dt_required,ntimes)
      it = 0
      print('----------------------------------------------------------------')
      print('Required ntimes greater than the input time array')
      print('or required t_init too large for the required ntimes')
      print('--> Artificial time array generated with dt defined by the user:')
      print('    - Required t_init = ',time_slice,'s')
      print('    - Actual t_init = ',time_array[it],'s')
      print('    - ntimes = ',ntimes)
      print('    - dt_required = ',dt_required)
      print('----------------------------------------------------------------')
  else:
      print('--------------------------------------------------------------------')
      print('Use the specificed first time slice with ntimes defined by the user:')
      print('- Required t_init = ',time_slice,'s')
      print('- Actual t_init = ',time_array[it],'s')
      print('- ntimes = ',ntimes)
      print('--------------------------------------------------------------------')

# IF LOCAL DATABASE DOES NOT EXIST: CREATE IT
local_database = os.getenv("HOME") + "/public/imasdb/" + output_database + "/3/0"
if os.path.isdir(local_database) == False:
    print("-- Create local database " + local_database)
    os.makedirs(local_database)

# CREATE OUTPUT DATAFILE
print('=> Create output datafile')
output = imas.DBEntry(imas.imasdef.MDSPLUS_BACKEND,output_database,shot,run_out,output_user_or_path)
output.create()

# START TIME LOOP
for itime in range(ntimes):

    # TIME PASSING BY
    print('Time = %5.2f' % time_array[itime],'s, itime = ',itime,'/',ntimes)

    # READ INPUT IDSS
    input_equilibrium = input.get_slice('equilibrium',time_array[itime],1)

    # EXECUTE CHEASE
    output_equilibrium = chease(input_equilibrium,run_py_dir + '/../input/chease_input_choices.xml')

    # WRITE RESULT TO LOCAL DATABASE
    output.put_slice(output_equilibrium)

    print('-------------------------------------')
    print('Output time = ',output_equilibrium.time[0])
    print('-------------------------------------')

input.close()
output.close()
print('Done.')
