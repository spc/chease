# Copyright 2024 SPC-EPFL
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------

# -----------------------------
# PYTHON SCRIPT TO CALL CHEASE
# -----------------------------

# export PYTHONPATH=~/public/PYTHON_ACTORS/chease:$PYTHONPATH

# NEEDED MODULES
import os,imas
import numpy as np
from yaml import load as yamlload
from idstools.utils.clihelper import get_backend_id
try:
    from yaml import CLoader as yamlLoader
except ImportError:
    from yaml import Loader as yamlLoader

path_file = os.path.dirname(os.path.abspath(__file__))
current_file=os.path.join(path_file,'../input/scenario.yaml')
print('code_parameters.parameters_path                = ',path_file)

# INPUT/OUTPUT CONFIGURATION
file = open(current_file, 'r')
cc = yamlload(file,Loader=yamlLoader)
file.close()
if cc['output_user_or_path'] == 'default':
    output_user_or_path = os.getenv('USER')
else:
    output_user_or_path = cc['output_user_or_path']

if cc['output_tmp_folder'] == 'default':
    output_tmp_folder = '/tmp/'+os.getenv('USER')
    os.system('mkdir -p '+output_tmp_folder)
else:
    output_tmp_folder = cc['output_tmp_folder']

current_folder = os.getcwd()
os.chdir(output_tmp_folder)

# CHEASE INITIALISATION
from chease.actor import chease as chease_actor
chease = chease_actor()
code_parameters = chease.get_code_parameters()
code_parameters.parameters_path = os.path.join(path_file,'../input/chease_input_choices.xml')
print('code_parameters.parameters_path                = ',path_file)
runtime_settings = chease.get_runtime_settings()
if cc['debug'] == 1:
    from chease.common.runtime_settings import DebugMode
    runtime_settings.debug_mode = DebugMode.STANDALONE
if cc['sandbox_manual'] == 1:
    from chease.common.runtime_settings import SandboxLifeTime, SandboxMode
    runtime_settings.sandbox.life_time = SandboxLifeTime.PERSISTENT
    runtime_settings.sandbox.mode = SandboxMode.MANUAL
    runtime_settings.sandbox.path = os.getcwd()
chease.initialize(code_parameters=code_parameters,runtime_settings=runtime_settings)

# TO SUPPRESS THE ACCESS LAYER WARNINGS ABOUT OBSOLETE DATA
import logging
log = logging.getLogger()
log.setLevel(logging.ERROR)

# DISPLAY SIMULATION INFORMATION
print('---------------------------------')
print('shot                = ',cc['shot'])
print('run_in              = ',cc['run_in'])
print('run_out             = ',cc['run_out'])
print('input_user_or_path  = ',cc['input_user_or_path'])
print('input_database      = ',cc['input_database'])
print('output_user_or_path = ',output_user_or_path)
print('output_database     = ',cc['output_database'])
print('time_slice          = ',cc['time_slice'])
print('ntimes              = ',cc['ntimes'])
print('dt_required         = ',cc['dt_required'],' s')
print('---------------------------------')

# FUNCTION TO FIND INDEX OF NEAREST TIME SLICE IN TIME ARRAY
def find_nearest(a, a0):
    "Element in nd array `a` closest to the scalar value `a0`"
    idx = np.abs(a - a0).argmin()
    return a.flat[idx],idx

# READ INPUT IDSS
print('=> Open input datafile')
input = imas.DBEntry(get_backend_id(cc['input_backend']),cc['input_database'],cc['shot'],cc['run_in'],cc['input_user_or_path'],data_version='3')
input.open()

# READ FULL TIME VECTOR OF EQUILIBRIUM IDS TO GET THE TIME BASE
print('=> Open input datafile and to read time array from equilibrium IDS')
time_array = input.partial_get(ids_name='equilibrium',data_path='time')
if cc['time_slice'] == 0:
    cc['ntimes'] = len(time_array)
    it = 0
    print('-----------------------------------------------------')
    print('Use the whole time array of the input equilibrium IDS')
    print('- t_init = ',time_array[0],'s')
    print('- ntimes = ',cc['ntimes'])
    print('-----------------------------------------------------')
elif cc['time_slice'] < 0:
    it = 0
    cc['time_slice'] = time_array[0]
    if cc['ntimes'] > len(time_array):
      time_array = np.linspace(time_array[0],time_array[0]+cc['ntimes']*cc['dt_required'],cc['ntimes'])
      print('-------------------------------------------------------------------------------------')
      print('Use the first time slice of the input equilibrium IDS with ntimes defined by the user')
      print('... However ntimes is greater than the input time array')
      print('--> Artificial time array generated with dt defined by the user:')
      print('    - t_init = ',time_array[it],'s')
      print('    - ntimes = ',cc['ntimes'])
      print('    - dt_required = ',cc['dt_required'])
      print('-------------------------------------------------------------------------------------')
    else:
        print('--------------------------------------------------------------------------------------')
        print('Use the first time slice of the input equilibrium IDS with ntimes defined by the user:')
        print('- t_init = ',time_array[0],'s')
        print('- ntimes = ',cc['ntimes'])
        print('--------------------------------------------------------------------------------------')
else:
  # FIND INDEX OF NEAREST TIME SLICE IN TIME ARRAY
  [tc,it] = find_nearest(time_array,cc['time_slice'])
  if cc['ntimes'] > len(time_array) or it+cc['ntimes']>len(time_array):
      time_array = np.linspace(time_array[it],time_array[it]+cc['ntimes']*cc['dt_required'],cc['ntimes'])
      it = 0
      print('----------------------------------------------------------------')
      print('Required ntimes greater than the input time array')
      print('or required t_init too large for the required ntimes')
      print('--> Artificial time array generated with dt defined by the user:')
      print('    - Required t_init = ',cc['time_slice'],'s')
      print('    - Actual t_init = ',time_array[it],'s')
      print('    - ntimes = ',cc['ntimes'])
      print('    - dt_required = ',cc['dt_required'])
      print('----------------------------------------------------------------')
  else:
      print('--------------------------------------------------------------------')
      print('Use the specificed first time slice with ntimes defined by the user:')
      print('- Required t_init = ',cc['time_slice'],'s')
      print('- Actual t_init = ',time_array[it],'s')
      print('- ntimes = ',cc['ntimes'])
      print('--------------------------------------------------------------------')

# IF LOCAL DATABASE DOES NOT EXIST: CREATE IT
local_database = os.getenv("HOME") + "/public/imasdb/" + cc['output_database'] + "/3/0"
if os.path.isdir(local_database) == False:
    print("-- Create local database " + local_database)
    os.makedirs(local_database)

# CREATE OUTPUT DATAFILE
print("=> Create output datafile",get_backend_id(cc['output_backend']),cc['output_database'],cc['shot'],cc['run_out'],output_user_or_path)
output = imas.DBEntry(get_backend_id(cc['output_backend']),cc['output_database'],cc['shot'],cc['run_out'],output_user_or_path)
output.create()

# START TIME LOOP
for itime in range(cc['ntimes']):

    # TIME PASSING BY
    print('Time = %5.2f' % time_array[itime],'s, itime = ',itime,'/',cc['ntimes'])

    # READ INPUT IDSS
    input_equilibrium = input.get_slice('equilibrium',time_array[itime],1)

    # EXECUTE CHEASE
    output_equilibrium = chease(input_equilibrium)

    # WRITE RESULT TO LOCAL DATABASE
    output.put_slice(output_equilibrium)

    print('-------------------------------------')
    print('Output time = ',output_equilibrium.time[0])
    print('-------------------------------------')

chease.finalize()
input.close()
output.close()
print('Done.')

os.chdir(current_folder)
