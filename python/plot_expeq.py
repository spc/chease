#!/usr/bin/env python
# Copyright 2024 SPC-EPFL
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
import numpy as np
import matplotlib_interactive
DEFAULT_INTERACTIVE = matplotlib_interactive.DEFAULT_INTERACTIVE
import matplotlib.pyplot as plt
import argparse
import os.path
try:
    from scipy.optimize import curve_fit, minimize
    HAVE_SCIPY = True
except ImportError:
    HAVE_SCIPY = False

from chease_hdf5 import interactive_show_or_save, interactive_hold_and_save
import eq_boundary

# Dicts for the NPPFUN and NSTTP labels
NPPFUN_LABEL_DICT = {
    4: 'pprime',
    8: 'pressure',
}
NSTTP_LABEL_DICT = {
    1: "TT'",
    2: "$I^*$",
    3: "$I_{||}$",
    4: "$J_{||}$",
    5: 'q',
}
NRHOTYPE_LABEL_DICT = {
    0: r"\rho_{\psi}",
    1: r"\rho_{tor}",
}

def read_field(fileobj, nlines):
    tmp = []
    for _ in range(nlines):
        tmp.append(fileobj.readline())
    ncols = len(tmp[0].split())
    if ncols > 1:
        array = np.array([[float(i) for i in line.split()] for line in tmp])
    else:
        array = np.array([float(i) for line in tmp for i in line.split()])
    return array

def read_scalar(fileobj, scalar_type=float):
    line = fileobj.readline()
    return scalar_type(line.split()[0])

def read_expeq(filename="EXPEQ"):
    """
    Read an EXPEQ file and return a struct.
    N.B. some of the field labels are either placeholders, or are incorrect for some options.
    """
    expeq_str = {}
    with open(filename, "r") as expeq:
        expeq_str["aspect_ratio"] = read_scalar(expeq, float)
        expeq_str["rz0c"] = read_scalar(expeq, float)
        expeq_str["pedge"] = read_scalar(expeq, float)
        expeq_str["nbps"] = read_scalar(expeq, int)
        expeq_str["boundary"] = read_field(expeq, expeq_str["nbps"])
        expeq_str["npsi"], expeq_str["nppfun"] = [int(x) for x in expeq.readline().split()]
        expeq_str["nsttp"], expeq_str["nrhotype"] = [int(x) for x in expeq.readline().split()]
        expeq_str["psi"] = read_field(expeq, expeq_str["npsi"])
        expeq_str["pressure"] = read_field(expeq, expeq_str["npsi"])
        expeq_str["q"] = read_field(expeq, expeq_str["npsi"])
        footer = []
        while True:
            line = expeq.readline()
            if line == "":
                break
            footer.append(line)
        expeq_str["footer"] = footer
    return expeq_str

def write_expeq(expeq_str, filename="EXPEQ_out"):
    """
    Write an EXPEQ file given a struct.
    N.B. some of the field labels are either placeholders, or are incorrect for some options.
    """
    with open(filename, "w") as expeq:
        expeq.write(str(expeq_str["aspect_ratio"]) + "\n")
        expeq.write(str(expeq_str["rz0c"]) + "\n")
        expeq.write(str(expeq_str["pedge"]) + "\n")
        expeq.write(str(expeq_str["nbps"]) + "\n")
        for x, y in expeq_str["boundary"]:
            expeq.write(f"{x} {y}\n")
        expeq.write(f"{expeq_str['npsi']} {expeq_str['nppfun']}\n")
        expeq.write(f"{expeq_str['nsttp']} {expeq_str['nrhotype']}\n")
        for psival in expeq_str["psi"]:
            expeq.write(f"{psival}\n")
        for pressureval in expeq_str["pressure"]:
            expeq.write(f"{pressureval}\n")
        for qval in expeq_str["q"]:
            expeq.write(f"{qval}\n")
        for line in expeq_str["footer"]:
            expeq.write(line)

def test_plot(expeq, show=True):
    fig, ax = plt.subplots()
    ax.plot(expeq["boundary"][:,0], expeq["boundary"][:,1])
    ax.set_aspect(1.)
    fig, ax = plt.subplots()
    ax.plot(np.sqrt(expeq["psi"]), expeq["q"])
    fig, ax = plt.subplots()
    ax.plot(np.sqrt(expeq["psi"]), expeq["pressure"])
    print(expeq["pressure"][0])
    if show:
        plt.show(block=True)

def fit(boundary, eps, z0=0., guess=None, plot=False, do_rz_simul=True, do_rz_stagger=False):
    """
    Given a prescribed boundary, fit the parameters of our analytical function to the boundary.
    """
    if not HAVE_SCIPY:
        raise NotImplementedError
    if not (do_rz_simul + do_rz_stagger):
        print("At least one of do_rz_simul or do_rz_stagger must be true")
        raise ValueError

    theta = np.linspace(0.0, 2.0*np.pi, boundary.shape[0])
    r_orig = boundary[:,0]
    z_orig = boundary[:,1] - z0

    if guess is None:
        popt = [1., 1., 0.0, 0.0, 0.0, 0.0]
    else:
        popt = guess

    if plot:
        fig, ax = plt.subplots()
        ax.plot(r_orig, z_orig + z0, 'k-')

    def target_func(theta, *x):
        kl, ku, dl, du, xl, xu = x
        r_fit, z_fit = eq_boundary.get_rz(
            theta, eps=eps,
            kappa_l=kl, kappa_u=ku,
            delta_l=dl, delta_u=du,
            xi_l=xl, xi_u=xu,
        )
        return r_fit, z_fit

    if do_rz_stagger:
        def target_func_r(theta, kl, ku, dl, du, xl, xu):
            return eq_boundary.get_rz(theta, eps=eps,
                        kappa_l=kl, kappa_u=ku,
                        delta_l=dl, delta_u=du,
                        xi_l=xl, xi_u=xu,
                        )[0]
        def target_func_z(theta, kl, ku, dl, du, xl, xu):
            return eq_boundary.get_rz(theta, eps=eps,
                        kappa_l=kl, kappa_u=ku,
                        delta_l=dl, delta_u=du,
                        xi_l=xl, xi_u=xu,
                        )[1]

        # Here we iterate between optimizing for R and optimizing for Z
        for _ in range(100):
            p0 = popt
            popt, pcov = curve_fit(target_func_r, theta, r_orig, p0)
            p0 = popt
            popt, pcov = curve_fit(target_func_z, theta, z_orig, p0)
        print(popt)
        if plot:
            r_fit_2, z_fit_2 = target_func(theta, *popt)
            ax.plot(r_fit_2, z_fit_2 + z0, 'b-.')

    if do_rz_simul:
        # Here we solve R and Z simultaneously
        # Start from the previous answer if we did the previous minimization

        def cost_function(params, theta, data):
            rdata, zdata = data
            r_fit, z_fit = target_func(theta, *params)
            return np.sum((r_fit - r_orig)**2 + (z_fit - z_orig)**2)

        data = [r_orig, z_orig]
        a = minimize(cost_function, popt, args=(theta, data))
        r_fit, z_fit = target_func(theta, *a.x)
        print(a.x)
        if plot:
            ax.plot(r_fit, z_fit + z0, 'r--')
        popt = a.x

    if plot:
        ax.plot([1.0], [z0], 'k+')
        plt.show()
    return popt

def test():
    expeq = read_expeq()
    fit(expeq["boundary"], expeq["aspect_ratio"], plot=True, z0=expeq["rz0c"], do_rz_stagger=True)
    test_plot(expeq)
    write_expeq(expeq, filename="EXPEQ_test")

def replace(args):
    """
    Replace the boundary in an EXPEQ file.
    EQ Values are taken from the command line arguments.
    Default EQ parameters are taken by fitting the boundary in the existing EXPEQ file.

    The directory, as well as the input and output filenames can be controlled via command line options.
    """
    if args.kappa_l is None and args.kappa_u is None:
        kappa_l = args.kappa
        kappa_u = args.kappa
    else:
        kappa_l = args.kappa_l
        kappa_u = args.kappa_u

    if args.delta_l is None and args.delta_u is None:
        delta_l = args.delta
        delta_u = args.delta
    else:
        delta_l = args.delta_l
        delta_u = args.delta_u

    if args.xi_l is None and args.xi_u is None:
        xi_l = args.xi
        xi_u = args.xi
    else:
        xi_l = args.xi_l
        xi_u = args.xi_u

    expeq = read_expeq(filename=os.path.join(args.directory, args.filename))

    if args.eps is None:
        eps = expeq["aspect_ratio"]

    if kappa_l is None or kappa_u is None or delta_l is None or delta_u is None or xi_l is None or xi_u is None:
        if args.plot_boundary:
            plt.plot(expeq["boundary"][:,0], expeq["boundary"][:,1], 'k-')
        fit_params = fit(
            expeq["boundary"],
            expeq["aspect_ratio"],
            z0=expeq["rz0c"],
            plot=args.plot_boundary
        )
        if kappa_l is None:
            kappa_l = fit_params[0]
            print(f"{kappa_l=}")
        if kappa_u is None:
            kappa_u = fit_params[1]
            print(f"{kappa_u=}")
        if delta_l is None:
            delta_l = fit_params[2]
            print(f"{delta_l=}")
        if delta_u is None:
            delta_u = fit_params[3]
            print(f"{delta_u=}")
        if xi_l is None:
            xi_l = fit_params[4]
            print(f"{xi_l=}")
        if xi_u is None:
            xi_u = fit_params[5]
            print(f"{xi_u=}")

    theta = np.linspace(0.0, 2.0*np.pi, expeq["nbps"], endpoint=True)
    r, z = eq_boundary.get_rz(
        theta, eps=eps,
        kappa_l=kappa_l, kappa_u=kappa_u,
        delta_l=delta_l, delta_u=delta_u,
        xi_l=xi_l, xi_u=xi_u
    )
    # Re-add the Z0 offset
    z = z[:] + expeq["rz0c"]
    if args.plot_boundary:
        plt.plot(r, z)
        plt.plot([1.0], [expeq["rz0c"]], '+')
        plt.gca().set_aspect(1.)
        plt.show()
    expeq["boundary"][:,0] = r[:]
    expeq["boundary"][:,1] = z[:]

    if args.filename_out is None:
        filename = "EXPEQ_OUT"
    else:
        filename = args.filename_out
    write_expeq(expeq, filename=os.path.join(args.directory, filename))


def plot_expeq(expeq=None, filename='EXPEQ', directory='.', interactivePlots=False):
    """
    Given a filename or expeq dict, plot the contents of the EXPEQ file (boundary + two profiles).
    """
    figlist = []

    if expeq is None:
        expeq = read_expeq(os.path.join(directory, filename))

    fig, ax = plt.subplots()
    ax.plot(expeq["boundary"][:,0], expeq["boundary"][:,1])
    ax.set_aspect(1.)
    plot_filename = "expeq_boundary.pdf"
    interactive_show_or_save(fig, plot_filename, figlist, interactivePlots)


    fig, ax = plt.subplots()
    c = next(ax._get_lines.prop_cycler)['color']
    label = NPPFUN_LABEL_DICT.get(expeq["nppfun"])
    ax.plot(expeq["psi"], expeq["pressure"], color=c, label=label)
    c = next(ax._get_lines.prop_cycler)['color']

    ax2 = ax.twinx()
    label = NSTTP_LABEL_DICT.get(expeq["nsttp"])
    ax.plot([], [], label=label, color=c)
    ax2.plot(expeq["psi"], expeq["q"], color=c)
    ax.legend()

    # Set xlabel depending on nrhotype
    ax.set_xlabel(f"${NRHOTYPE_LABEL_DICT.get(expeq['nrhotype'])}$")
    plt.show()

    plot_filename = "expeq_profs.pdf"
    interactive_show_or_save(fig, plot_filename, figlist, interactivePlots)

    interactive_hold_and_save(figlist)


def main():
    parser = argparse.ArgumentParser(description="Tool for plotting and manipulating EXPEQ equilibrium files")
    parser.add_argument("-directory", type=str, default=".", help="Directory of the input/output EXPEQ file(s)")
    parser.add_argument("-filename", type=str, default="EXPEQ", help="Filename of the input EXPEQ file")
    parser.add_argument("-filename_out", type=str, help="Filename of the output EXPEQ file")
    parser.add_argument("-eps", type=float, default=0.1, help="Aspect ratio")
    parser.add_argument("-kappa", type=float, default=1., help="Elongation (if neither of kappa_l or kappa_u are provided")
    parser.add_argument("-kappa_l", type=float, help="Lower elongation")
    parser.add_argument("-kappa_u", type=float, help="Upper elongation")
    parser.add_argument("-delta", type=float, default=0., help="Triangularity (if neither of delta_l or delta_u are provided")
    parser.add_argument("-delta_l", type=float, help="Lower triangularity")
    parser.add_argument("-delta_u", type=float, help="Upper triangularity")
    parser.add_argument("-xi", type=float, default=0., help="Squareness (if neither of xi_l or xi_u are provided")
    parser.add_argument("-xi_l", type=float, help="Lower squareness")
    parser.add_argument("-xi_u", type=float, help="Upper squareness")
    parser.add_argument("-plot_boundary", type=int, default=0, help="Whether to plot the boundary when doing a boundary replacement")
    parser.add_argument("-test", type=int, default=0, help="Test the code. Will read a file EXPEQ and write a file EXPEQ_test")
    parser.add_argument("-fit", type=int, default=0, help="Whether to fit an analytical approximation to the boundary (experimental)")
    parser.add_argument("-replace", type=int, default=0, help="Whether to replace the EXPEQ boundary with parameters/fitted values)")
    parser.add_argument("-plot", type=int, default=0, help="Whether to plot the EXPEQ file")
    parser.add_argument("-interactivePlots", type=int, default=0, help="If plots should be interactive or saved")
    args = parser.parse_args()

    if args.test:
        test()
        return

    if args.replace:
        replace(args)

    if args.plot:
        plot_expeq(filename=args.filename, directory=args.directory, interactivePlots=args.interactivePlots)


if __name__ == "__main__":
    main()
