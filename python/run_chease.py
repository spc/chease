# Copyright 2024 SPC-EPFL
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------

# --------------------------------------------
# PYTHON WRAPPER TO CALL CHEASE
# --------------------------------------------

# NEEDED MODULES
import os,imas,sys,pdb
from pyal import ALEnv
from random import randint

# INPUT AND OUTPUT CONFIGURATION
shot                = 100015
run_in              = 1
run_out             = 3
input_user_or_path  = 'public'
input_database      = 'iter'
output_user_or_path = os.getenv('USER')
output_database     = 'iter'

# IMPORT MODULE(S) FOR SPECIFIC PHYSICS CODE(S)
actor_path = os.path.join(os.getenv('KEPLER'), 'imas/src/org/iter/imas/python')
list_of_actors = ['chease']
for name in list_of_actors:
  sys.path[:0] = [os.path.join(actor_path,name)]
  globals()[name] = getattr(__import__(name), name)

# IMAS DATA VERSION
version = os.getenv('IMAS_VERSION')[0]

# OPEN INPUT DATAFILE TO GET DATA FROM INPUT DATABASE
# AND READ FULL TIME VECTOR OF EQUILIBRIUM IDS TO GET THE TIME BASE
print('=> Open input datafile and read total equilibrium IDS')
input = imas.ids(shot,run_in,0,0)
input.open_env(input_user_or_path,input_database,version)
time_array = input.equilibrium.partialGet('time')
ntime = len(time_array)
idx_in = input.equilibrium.getPulseCtx()

# CREATE OUTPUT DATAFILE
print('=> Create output datafile')
output = imas.ids(shot,run_out,0,0)
output.create_env(output_user_or_path,output_database,version)
idx_out = output.equilibrium.getPulseCtx()

# FOR THE TEMPORARY FILE, ONLY THE DEFAULT USER_OR_PATH BASED ON USERNAME IS USED
# CLEVERLY CHOOSE 'TMP' FOR DATABASE NAME TO NEVER MIX TEMPORARY FILES WITH OTHERS
tmp_user_or_path = os.getenv('USER') # (PUTTING IT TO SOMETHING ELSE DOES NOT MAKE A DIFFERENCE)
tmp_database = 'tmp'
tmp_folder = os.getenv('HOME')+'/public/imasdb/'+tmp_database+'/3/0'
if os.path.isdir(tmp_folder) == False:
  print('-- Create local database for tmp file '+tmp_folder)
  os.makedirs(tmp_folder)

# DEFINE THE SHOT/RUN NUMBERS AND LOCATION OF THE TEMPORARY FILE
exist = 'yes'
shot_tmp = 9987
while exist == 'yes':
  run_tmp  = randint(0,9999)
  tmp = imas.ids(shot_tmp,run_tmp,0,0)
  [err,n]=tmp.open_env(tmp_user_or_path,tmp_database,version,silent=True)
  if err != 0:
    exist = 'no'
tmp_db = ALEnv(shot=shot_tmp, run_temp=run_tmp, machine_temp=tmp_database).ids_tmp

first_time_slice = 1
for itime in range(ntime):
#for itime in [80]:

    # EXECUTE PHYSICS CODE
    print('Time = %5.2f' % time_array[itime],'s, itime = ',itime,'/',ntime)

    input.equilibrium.setPulseCtx(idx_in)
    input.equilibrium.getSlice(time_array[itime],1)

    output.equilibrium = chease(input.equilibrium,'input/chease_input_choices.xml')

    output.equilibrium.setPulseCtx(idx_out)
    if first_time_slice == 1:
      output.equilibrium.put()
    else:
      output.equilibrium.putSlice()

    print('-------------------------------------')
    print('Output time = ',output.equilibrium.time[0])
    print('-------------------------------------')

    first_time_slice = 0

input.close()
output.close()
print('Done.')
