#!/bin/tcsh -fe
# Copyright 2024 SPC-EPFL
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#----------------------------------------------------------------------

if ($ITM_ENVIRONMENT_LOADED == 'yes') then
  # for ITM, expects
  #source $ITMSCRIPTDIR/ITMv1 kepler test 4.10b.10_R2.2.0
      #source $ITMSCRIPTDIR/ITMv1 kepler test 4.10b_rc
      #module switch fc2k/rc
      #module switch scripts/R2.2
  cd src-f90
  make clean
  make libchease_kepler
  cd ..

  # can test with: fc2k -docfile doc/chease.txt fc2k/chease.xml
  echo " "
  echo "Can execute: fc2k -docfile doc/chease.txt fc2k/chease.xml"

  # then can test with workflow in ./workflow_test/chease_test_workflow_nocpoin.xml which creates shot 12345, run=1 from           no CPO (dummy shot=12345, run 4321 used)
  # then can test with workflow in ./workflow_test/chease_test_workflow.xml using shot=12345, run=1 to create run=11

else
  echo "error ITM environment not available. Cant use makefile option 'ITM_ENVIRONMENT_LOADED=yes'"
endif
