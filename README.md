# CHEASE

The CHEASE code is an open source fixed-boundary equilibrium code described in the following publications:

1. [H. Lütjens, A. Bondeson and O. Sauter, _The CHEASE code for toroidal MHD equilibria_, Comput. Phys. Commun. *97* (1996) 219](https://crppwww.epfl.ch/~sauter/CHEASE/Lutjens_CHEASE_CPC96.pdf "CPC 1996") <details><summary>BibTeX</summary>
@article{Lutjens1996,
abstract = {The CHEASE code (Cubic Hermite Element Axisymmetric Static Equilibrium) solves the Grad-Shafranov equation for toroidal MHD equilibria using a Hermite bicubic finite element discretization with pressure, current profiles and plasma boundaries specified by analytical forms or sets of experimental data points. Moreover, CHEASE allows the automatic generation of pressure profiles marginally stable to ballooning modes or with a prescribed fraction of bootstrap current. The code provides equilibrium quantities for several stability and global wave propagation codes.},
author = {Lutjens, H and Bondeson, A and Sauter, O},
issn = {0010-4655},
journal = {Computer Physics Communications},
number = {3},
pages = {219--260},
title = {{The CHEASE code for toroidal MHD equilibria}},
volume = {97},
year = {1996}
}</details>
2. [H. Lütjens, A. Bondeson and A. Roy, _Axisymmetric MHD equilibrium solver with bicubic Hermite elements_, Comput. Phys. Commun. *69* (1992) 287](https://crppwww.epfl.ch/~sauter/CHEASE/Lutjens_CPC1992_chease_a.pdf "CPC 1992")<details><summary>BibTeX</summary>@article{Lutjens1992,
abstract = {A numerical code using Hermite bicubic finite elements has been developed for the computation of axisymmetric magnetohydrodynamic (MHD) equilibria. The code provides a mapping to flux coordinates for MHD stability calculations. Several test cases are studied to show the convergence rate for the equilibrium. Convergence tests are also presented for the eigenvalues of the stability calculations when the equilibrium mesh is varied.},
author = {L{\"{u}}tjens, H. and Bondeson, a. and Roy, a.},
file = {:Users/ffelici/Library/Application Support/Mendeley Desktop/Downloaded/L{\"{u}}tjens, Bondeson, Roy - 1992 - Axisymmetric MHD equilibrium solver with bicubic Hermite elements.pdf:pdf},
journal = {Computer Physics Communications},
number = {2-3},
pages = {287--298},
title = {{Axisymmetric MHD equilibrium solver with bicubic Hermite elements}},
volume = {69},
year = {1992}
}</details>

## License
The code is open source with Apache 2.0 license, see `License`

## Contributing
You are very welcome to contribute to the code, see `CONTRIBUTING.md`

## COordinate COnventionS
CHEASE uses `COCOS=2` internally, but has COCOS_in and COCOS_out namelist variable to match to any `COCOS` index (see below)

## Repository
CHEASE is presently maintained at https://gitlab.epfl.ch/spc/chease
To get the latest version do: `git clone https://gitlab.epfl.ch/spc/chease.git chease`
A list of tags and releases is maintained [`README_tags`](https://gitlab.epfl.ch/spc/chease/-/blob/master/README_tags)

Prior to November 2021, CHEASE was maintained on https://spcsvn.epfl.ch. The switch to SVN was made at SVN commit number 1368 corresponding to tag [v12.18](https://gitlab.epfl.ch/spc/chease/-/tags/4.10b.10_IMAS_3_CHEASEv12_18)


## Installation
A new file explaining how to install and run the make commands has been added: [Make_README](`src-f90/Make_README`)

## GUI
There is a matlab GUI within the above release (in `matlab` directory) which is useful to get used to running CHEASE in `matlab/CHEASEgui.m` (see demos below)
<details><summary>Usage instruction details</summary>
To use the `CHEASEgui` you should have the executable `chease` in your path as well as the files `o.chease_to_cols`, `o.chease_rz_plasma_boundary_to_cols.pl` and `o.chease_to_cols_v10.pl` (which are in the main `CHEASE/trunk/scripts_for_bin` directory).

Typically you would have:
```
PATH=$PATH:$HOME/bin  # in your .profile, .cshrc or .bashrc file (or equivalent)
cd $HOME/bin
$ ln –s .../chease/src-f90/chease .
$ ln –s .../chease/scripts_for_bin/o.chease_to_cols .
$ ln –s .../chease/scripts_for_bin/o.chease_rz_plasma_boundary_to_cols.pl .
$ ln –s .../chease/scripts_for_bin/o.chease_to_cols_v10.pl .
$ matlab -nodesktop # open matlab
>> addpath .../chease/matlab/CHEASEgui
>> CHEASEgui
```
Then click on "plot and save" for the plasma boundary, the pprime profile and the TTprime or current profile, then choose suffix (default `run_name`) then click "prepare CHEASE input", then in menu "Run CHEASE")

You can create a file containing all the output 1D profiles with (from the chease directory):
```
chease > o.cheaseoutput
o.chease_to_cols   o.cheaseoutput   o.cheaseoutput.cols
matlab
>> addpath matlab/CHEASEgui
>> plotdatafile(' o.cheaseoutput.cols') % which will open a GUI to plot profiles
```
</details>

## Instructions for using CHEASE with q profile input
New option for CHEASE with q profile in input: `NSTTP = 5`
You can test this option using:
```
  make test_chease_q
```
Then looking at the files in `/tmp/$USER/nsttp_tests`

## Dependencies
`CHEASEgui` as well as `read_eqdsk` use extensively the cubic spline with smoothing package: `interpos` (http://spc.epfl.ch/interpos) which you might be interested in using as well (matlab, fortran, C, etc). You will need to sign a similar agreement.

## References
* [CHEASE website](https://spc.epfl.ch/chease)
* [Notes on the CHEASE normalizations](https://crppwww.epfl.ch/~sauter/CHEASE/chease_normalization.pdf)
* **COCOS paper** defining uniquely the multiple options for Coordinate COnventionS (by O. Sauter et S. Y Medvedev) ([author version](https://crppwww.epfl.ch/~sauter/cocos/Sauter_COCOS_Tokamak_Coordinate_Conventions.pdf), [journal version](https://doi.org/10.1016/j.cpc.2012.09.010))
* CPO structure. CHEASE now uses the equilibrium structure originally proposed by the EU-ITM Task Force: [CPO paper](https://crppwww.epfl.ch/~sauter/CHEASE/DataStructurePaper_CPC2010_181_reprint.pdf)

## Demos
* Using CamStudio, the following demo are available (.avi large files)
 * [Demo 1](https://crppwww.epfl.ch/~sauter/CHEASE/CHEASEgui_demo1.avi) (42MB) for getting started with `CHEASEgui`
 * [Demo 2](https://crppwww.epfl.ch/~sauter/CHEASE/CHEASEgui_demo2.avi) (20MB) to easily specify a reverse `q` profile

## Contact person
Olivier Sauter, SPC-EPFL: olivier.sauter@epfl.ch
