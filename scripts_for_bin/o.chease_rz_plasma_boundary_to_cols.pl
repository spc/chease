#!/usr/bin/perl
# Copyright 2024 SPC-EPFL
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# to use:
# o.chease_rz_plasma_boundary_to_cols o_file > o_file.RZcols
#
$matlab_out=0;
$table_out=1;
$table_head=1;
@tags=(
    "R-PLASMA-BOUNDARY",
    "Z-PLASMA-BOUNDARY",
    );



$varnum=-1;
while(<>){
    if(/^\s*\n/ && $get==1){
	$get=0;
	$matlab_out &&	print "];\n";
    }
    if($get){
	@d=split;
	foreach $datum(@d){
	    $data[$varnum][$subscript]=$datum;
	    $subscript++;
	    $matlab_out && print "$datum\n";
	}
    }
    foreach $tag(@tags){
	$match=quotemeta($tag);
	if(/$match/x){
	    $varnum++;
	    $subscript=0;
	    $get=1;
	    $matlab_out && print "v_$varnum=[...\n";
	}
    }
}
if($table_out){
    $minwidth=15;
    if($table_head){
	print "%";
	for $j ( 0 .. $#tags ) {
	    $width=max(length($tags[$j])+2,$minwidth);
	    $tmp=$tags[$j];$tmp=~s/\s//g;
	    printf("%".$width."s",$tmp);
	}
	print "\n";
    }
    for $i ( 0 .. $#{$data[0]} ) {
	print " ";
	for $j ( 0 .. $#tags ) {
	    $width=max(length($tags[$j])+2,$minwidth);
	    printf("%".$width.".4e",$data[$j][$i]);
	}
	print "\n";
    }
}

sub max{
    my $a=shift;
    my $b=shift;
    return $a<$b?$b:$a;
}
