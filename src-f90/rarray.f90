! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*DECK U30
!*CALL PROCESS
SUBROUTINE RARRAY(KNAME,PA,KDIM)
  !        ---------- ------
  !
  ! U.30   PRINT NAME AND VALUES OF REAL ARRAY
  !
  !---*----*----*----*----*----*----*----*----*----*----*----*----*----*
  !
  !
  USE globals
  IMPLICIT NONE
  INTEGER, INTENT(IN)       :: KDIM
  REAL(RKIND), INTENT(IN)   :: PA(KDIM)
  CHARACTER*(*), INTENT(IN) :: KNAME
  INTEGER                   :: J
!!$  DIMENSION &
!!$       &   PA(KDIM)
  !
  CALL BLINES(1)
  WRITE (6,9900) KNAME
  WRITE (6,9901) (PA(J),J=1,KDIM)
  !
  RETURN
9900 FORMAT(1X,A)
9901 FORMAT((1X,8(1PE13.4)))
END SUBROUTINE RARRAY
