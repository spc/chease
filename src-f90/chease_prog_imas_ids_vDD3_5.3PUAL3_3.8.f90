! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
program chease_prog
  !------------------------------------------------------------------
  ! Wrapper program for chease routine to be called with equil_in, equil_out and codename for parameters
  ! It should allow to run chease independently from the ITM structure, gateway or with libraries
  ! allowing read/write from ITM structure.
  ! If chease is called from Kepler. Then this program is not run and chease routine directly called with NITMOPT=22
  !
  ! Thus the namelist should be read here and then if needed the equil_in.
  !
  ! Compatible with only ids structures, calling chease(equil_in_ids,equil_out_ids;param_ids_input)
  !
  !------------------------------------------------------------------
  !
  use globals    ! needed here since keep option read(..,eqdata) namelist for backward compatibility
  use ids_schemas                       ! module containing the equilibrium type definitions
  use euitm_schemas ! needed for codeparam used with assign_parameters
  use assign_chease_codeparameters_reflist    ! changed to _choices if chosen in Makefile

  IMPLICIT NONE

  INCLUDE 'COMDAT.inc'  ! include namelist eqdata

  !
  interface
     subroutine chease(equil_in_ids,equil_out_ids,param_input_ids,flag_status,output_message)
       use globals
       use euITM_schemas                       ! module containing the equilibrium type definitions
       use ids_schemas                       ! module containing the equilibrium type definitions
       use itm_ids_utilities ! to give extra_data for ids
       use assign_chease_codeparameters_reflist    ! changed to _choices if chosen in Makefile
       IMPLICIT NONE
       type(ids_equilibrium), intent(in) :: equil_in_ids
       type(ids_equilibrium), intent(out) :: equil_out_ids
       type(ids_Parameters_Input), intent(inout) :: param_input_ids
       integer :: flag_status
       character(len=:), pointer :: output_message
       !
     end subroutine chease

     subroutine load_imas(equil_in_ids,kitmopt,kitmshot,kitmrun,kitmocc,citmtree,kflag)
       use globals
       use ids_schemas                       ! module containing the equilibrium type definitions
       !avoid for no ITM compatibility       use euITM_routines
       IMPLICIT NONE
       type(ids_equilibrium)      :: equil_in_ids
       character*120  :: citmtree
       integer        :: kitmopt, kitmshot, kitmrun, kitmocc, kflag
     end subroutine load_imas

     subroutine write_imas(equil_out_ids,kitmopt,kitmshot,kitmrun,kitmocc,citmtree)
       use globals
       use ids_schemas                       ! module containing the equilibrium type definitions
       !       use euITM_routines
       IMPLICIT NONE
       type(ids_equilibrium)      :: equil_out_ids
       character*120  :: citmtree
       integer        :: kitmopt, kitmshot, kitmrun, kitmocc
     end subroutine write_imas
     !
  end interface
  !
  type(ids_equilibrium)  :: equil_in_ids, equil_out_ids
  type (type_param) :: codeparam_itm
  ! type (type_param) :: codeparam_param
  type (ids_Parameters_Input) :: param_input_ids
  integer :: itmopt, i, nflag, flag_status
  character(len=:), pointer :: output_message => null()

!!$  character(len = 132), target :: codename(1) = 'CHEASE'
!!$  character(len = 132), target :: codeversion(1) = 'version 10'

  character(len = 132), allocatable :: parameters(:)
  target :: parameters
  character(len = 132) :: xml_line
  integer(ikind) :: n_lines, ios, ios2, INXML, istatus, INAMELIST, iwrite_imas, ihas0

  !*********************************************************************
  !
  !
  ! Initialize NITMOPT depending on local installation:
  ! NITMOPT = -1: if no itm stuff/libraries installed (default)
  ! NITMOPT =  0: if linked with ITM/mds modules (so can define ITM shot in namelist)

  NITMOPT = 0

  !
  ! Set up the case
  !
  CALL PRESET
  ! if itmopt<0 allows to force negative value independent of namelist value later
  itmopt = nitmopt

  !*********************************************************************
  ! Read namelist in xml format in file chease_input.xml
  ! or can read namelist as former namelist BUT in file: chease_namelist
  !

  !-- read chease_input.xml
  INXML = 12
  OPEN (unit = INXML, file = "chease_input.xml", status = 'old', &
       action = 'read', iostat = ios)

  IF (ios /= 0) THEN
     IF (NVERBOSE .GE. 3) write(0,*) ' WARNING:  ''chease_input.xml'' file missing,'
     IF (NVERBOSE .GE. 3) write(0,*) '            now will see if namelist given in file: ''chease_namelist'''
     INAMELIST = 13
     OPEN (unit = INAMELIST, file = "chease_namelist", status = 'old', &
          action = 'read', iostat = ios2)
     IF (ios2 /= 0) THEN
        IF (NVERBOSE .GE. 0) THEN
          write(0,*) ' ERROR: ''chease_namelist'' does not exist either'
          write(0,*) ' Note: CHEASE has been modified to allow for xml type inputs,'
          write(0,*) '       therefore input data should be given through files.'
          write(0,*) ' CHEASE accepts xml type input in file ''chease_input.xml'' or '
          write(0,*) '        namelist type as before but in file named ''chease_namelist'' '
          write(0,*) ' Thus copy your input datafile to the file ''chease_namelist'''
          write(0,*) '      and then run CHEASE with just the command: chease'
        END IF
        stop
     END IF
     rewind(INAMELIST)
     rewind(INAMELIST)
     rewind(INAMELIST)
     READ (INAMELIST,1000) LABEL1
     comments(1) = LABEL1
     READ (INAMELIST,1000) LABEL2
     comments(2) = LABEL2
     READ (INAMELIST,1000) LABEL3
     comments(3) = LABEL3
     READ (INAMELIST,1000) LABEL4
     comments(4) = LABEL4
     rewind(INAMELIST)
1000 FORMAT(A)

     READ (INAMELIST,EQDATA)
     close(INAMELIST)
     IF (NVERBOSE .GE. 1) THEN
       CALL BLINES(10)
       CALL MESAGE(LABEL1)
       CALL BLINES(1)
       CALL MESAGE(LABEL2)
       CALL BLINES(1)
       CALL MESAGE(LABEL3)
       CALL BLINES(1)
       CALL MESAGE(LABEL4)
     END IF
     if (associated(codeparam_itm%parameters)) deallocate(codeparam_itm%parameters)
     ! write(*,EQDATA)
     IF (NVERBOSE .GE. 3) PRINT *,' namelist read from file chease_namelist'
     IF (NVERBOSE .GE. 3) PRINT *,'nitmshot= ',nitmshot
  ELSE
     REWIND(INXML)
     IF (NVERBOSE .GE. 3) PRINT *,' namelist will be taken from file chease_input.xml'
     n_lines = 0
     DO
        READ (INXML, '(a)', iostat = ios) xml_line
        if (ios == 0) then
           n_lines = n_lines + 1
        else
           exit
        end if
     END DO
     rewind INXML
     allocate(parameters(n_lines))
     do i = 1, n_lines
        read (INXML, '(a)', iostat = ios) parameters(i)
     end do
     close(INXML)
     !  PRINT *,'parameters'
     !  PRINT *,parameters(:)

     !-- assign code parameters to internal variables defined in globals
     allocate(codeparam_itm%parameters(n_lines))
     codeparam_itm%parameters = parameters
     allocate(codeparam_itm%default_param(n_lines))
     codeparam_itm%default_param = parameters
     !
     ! need also schemas in codeparam_itm%schema now
     !
     OPEN (unit = INXML, file = "chease_schema_reflist.xsd", status = 'old', &
          action = 'read', iostat = ios)
     if (ios .ne. 0) then
       print *,'ERROR: cannot open chease_schema_reflist.xsd'    ! changed to _choices if chosen in Makefile
       stop 'no chease_schema_reflist.xsd file'
     end if
     n_lines = 0
     DO
        READ (INXML, '(a)', iostat = ios) xml_line
        if (ios == 0) then
           n_lines = n_lines + 1
        else
           exit
        end if
     END DO
     rewind INXML
     deallocate(parameters)
     allocate(parameters(n_lines))
     do i = 1, n_lines
        read (INXML, '(a)', iostat = ios) parameters(i)
     end do
     close(INXML)
     ! PRINT *,'parameters'
     ! PRINT *,parameters(:)
     allocate(codeparam_itm%schema(n_lines))
     codeparam_itm%schema = parameters
     IF (NVERBOSE .GE. 3) print *,codeparam_itm%parameters
     call assign_chease_codepar_reflist(codeparam_itm,istatus)    ! changed to _choices if chosen in Makefile

     IF (NVERBOSE .GE. 3) print *,'ntcase= ',ntcase
     if (istatus /= 0) then
       IF (NVERBOSE .GE. 0) write(*, *) 'ERROR: Could not assign some code parameters.'
        stop 'assign_code_parameters'
     end if

     ! Copy input xml to param_input_ids
     allocate(param_input_ids%parameters_value(size(codeparam_itm%parameters)))
     param_input_ids%parameters_value = codeparam_itm%parameters
     allocate(param_input_ids%parameters_default(size(codeparam_itm%parameters)))
     param_input_ids%parameters_default = codeparam_itm%parameters
     allocate(param_input_ids%schema(size(codeparam_itm%schema)))
     param_input_ids%schema = codeparam_itm%schema
     !
  end if
  !
  if (itmopt .lt. 0) then
     ! chease not compiled with itm libraries, so keep itmopt=-1 or 0
     NITMOPT = itmopt
  end if
  if (tree_user(1) == 'xxx') then
    call getenv('USER',tree_user(1))
    tree_user(2) = tree_user(1)
  end if
  !
  !   fetch equilibrium from ITM database if required
  !
  IF (NVERBOSE .GE. 3) print *,'NITMOPT= ',NITMOPT
  IF (NVERBOSE .GE. 3) print *,'NITMSHOT= ',NITMSHOT
  IF (NITMOPT.GT.0 .AND. MOD(NITMOPT,10) .EQ. 1) THEN
    IF (NVERBOSE .GE. 3) PRINT *,'load with treeitm(1), nitmshot(1), nitmrun(1), nitmocc(1)= ', &
         & trim(treeitm(1)), nitmshot(1), nitmrun(1), nitmocc(1)
    CALL LOAD_IMAS(equil_in_ids,nitmopt,nitmshot(1),nitmrun(1),nitmocc(1),treeitm(1),nflag)
    if (nflag .eq. -999) then
      ! problem with loading equil_in_ids, no allocation
      write(0,*) 'in prog_imas: problems with chease, return early'
      stop
    end if
    IF (NVERBOSE .GE. 3) write(*,*) 'equil_in_ids%time(1:4) = ',equil_in_ids%time(1:min(4,size(equil_in_ids%time)))
    !
  else
    ! since did not load it, make sure if associated can work
    IF (NVERBOSE .GE. 3) PRINT *,'equilibrium not taken from ITM structure, thus equil_in_ids not loaded'
  end if
  !
  allocate(character(132) :: output_message)
  call chease(equil_in_ids,equil_out_ids,param_input_ids,flag_status,output_message)
  print *,'flag_status = ',flag_status
  if (associated(output_message)) then
    print *,'output_message = x',output_message,'x'
  end if
  deallocate(output_message)
  ! for testing memory leaks run twice:
!!$  write(6,*) 'in between'
!!$  call flush(6)
!!$  call chease(equil_in_ids,equil_out_ids,param_input_ids)
  !
  ! write data to ITM structure if needed
  !
  iwrite_imas = 0
  if (associated(equil_out_ids%code%output_flag)) then
    ihas0 = minval(abs(equil_out_ids%code%output_flag))
    ! should have at least one correct equilibrium or save anyhow? Now does require this
    if (ihas0 .eq. 0) then
      IF (NITMOPT .GE. 10) THEN
        IF (NVERBOSE .GE. 3) PRINT *,'write with treeitm(2), nitmshot(2), nitmrun(2), nitmocc(2)= ', &
          & trim(treeitm(2)), nitmshot(2), nitmrun(2), nitmocc(2)
        CALL WRITE_IMAS(equil_out_ids,nitmopt,nitmshot(2),nitmrun(2), nitmocc(2),treeitm(2))
        iwrite_imas = 1
      end IF
    end if
  end if
  if (iwrite_imas .eq. 0) then
    write(0,*) '***********************************************************************'
    if (associated(equil_out_ids%code%output_flag)) then
      if (equil_out_ids%code%output_flag(1) .ne. 0) then
        write(0,*) 'in prog_imas: problems with chease: code%output_flag(1) = ',equil_out_ids%code%output_flag(1)
      else
        if (nverbose .ge. 3)  write(0,*) 'correct end of CHEASE'
      end if
    else
      write(0,*) 'in prog_imas: problems with chease: code%output_flag not associated'
    end if
    write(0,*) 'see messages in output and may need to rerun with NVERBOSE>=3 to have all info'
    call flush(0)
    !OS: no equivalent to eqchease_out(1)%codeparam%output_diag(:) in IDS at this stage.
    !OS: If it exists in later version, add to copy_itm_to_imas and then here below
    !OS    write(0,*) 'eqchease_out(1)%codeparam%output_diag(:):'
    !OS    write(0,'(A)') eqchease_out(1)%codeparam%output_diag(:)
  end if
  !
end program chease_prog
