! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
SUBROUTINE EQCHEASE_MKSA
  !
  ! transforms all eqchease... inputs into mksa units using
  !
  ! R0EXP
  ! B0EXP
  ! B0XPSGN
  ! IPXPSGN
  !
  ! In this way, eqchease arrays can be used within chease and actually have replaced former chease arrays
  !
  ! Should be called near end of chease run
  !
  USE globals
  USE cocos_module
  IMPLICIT NONE
  INTEGER :: iexp_Bp_out, isigma_Bp_out, isigma_RphiZ_out, isigma_rhothetaphi_out, isign_q_pos_out, isign_pprime_pos_out, itheta_sign_clockwise_out
  INTEGER :: iCOCOS_in, iIpsign_out, iB0sign_out
  REAL(RKIND) :: sigma_Ip_eff, sigma_B0_eff, sigma_Bp_eff, sigma_rhothetaphi_eff, sigma_RphiZ_eff, exp_Bp_eff, &
    & fact_psi, fact_q, fact_dpsi, fact_dtheta
  REAL(RKIND) :: ZMU0, zIp_in, zB0_in
  !
  ZMU0 = 4.E-07_RKIND * CPI
  ! use cocos_module to ala transform from cocos_in=2 (chease output) to cocos_out
  call COCOS(COCOS_out,iexp_Bp_out,isigma_Bp_out,isigma_RphiZ_out,isigma_rhothetaphi_out,isign_q_pos_out,isign_pprime_pos_out,itheta_sign_clockwise_out)
  iCOCOS_in = 2
  zIp_in = 1._rkind ! CHEASE ip sign
  zB0_in = 1._rkind !
  iIpsign_out = SIGNIPXP
  iB0sign_out = SIGNB0XP
  call COCOS_values_coefficients(iCOCOS_in, COCOS_out, zIp_in, zB0_in, &
    & sigma_Ip_eff, sigma_B0_eff, sigma_Bp_eff, sigma_rhothetaphi_eff, sigma_RphiZ_eff, exp_Bp_eff, &
    & fact_psi, fact_q, fact_dpsi, fact_dtheta, iIpsign_out, iB0sign_out) !  do give desired output signs, since can be provided in namelist
  !
  ! 1. eqchease_out_add_1d
  !
  eqchease_out_add_1d(:,iirgeo) = eqchease_out_add_1d(:,iirgeo) * R0EXP
  eqchease_out_add_1d(:,iiamin) = eqchease_out_add_1d(:,iiamin) * R0EXP
  eqchease_out_add_1d(:,iidqdpsi) =  fact_q * fact_dpsi * eqchease_out_add_1d(:,iidqdpsi) / R0EXP**2 / B0EXP
  eqchease_out_add_1d(:,iid2qdpsi2) = fact_q * fact_dpsi**2 * eqchease_out_add_1d(:,iid2qdpsi2)/(B0EXP*R0EXP**2)**2
  eqchease_out_add_1d(:,iidsheardpsi) = fact_dpsi * eqchease_out_add_1d(:,iidsheardpsi) / R0EXP**2 / B0EXP
!!$  eqchease_out_add_1d(:,iidVdpsi) = fact_dpsi * eqchease_out_add_1d(:,iidVdpsi) * R0EXP / B0EXP
!!$  eqchease_out_add_1d(:,iidpsidrhotor) = fact_psi * eqchease_out_add_1d(:,iidpsidrhotor) * B0EXP * R0EXP
   ! <| grd PSI |>
  eqchease_out_add_1d(:,iigradpsi_av) = abs(fact_psi) * eqchease_out_add_1d(:,iigradpsi_av) * R0EXP * B0EXP
  ! < r >
  eqchease_out_add_1d(:,iia_av) = eqchease_out_add_1d(:,iia_av) * R0EXP
  ! < R >
  eqchease_out_add_1d(:,iiR_av) = eqchease_out_add_1d(:,iiR_av) * R0EXP
  eqchease_out_add_1d(:,iiBmax) = SIGNB0XP * eqchease_out_add_1d(:,iiBmax) * B0EXP
  eqchease_out_add_1d(:,iiBmin) = SIGNB0XP * eqchease_out_add_1d(:,iiBmin) * B0EXP
  eqchease_out_add_1d(:,iiIplas) = SIGNIPXP * eqchease_out_add_1d(:,iiIplas) * R0EXP * B0EXP / ZMU0
  ! Te's, ne's already in eV or m^-3 since from EXPTNZ, but d/dpsi not normalized yet
  eqchease_out_add_1d(:,iidTedpsi) = fact_dpsi * eqchease_out_add_1d(:,iidTedpsi) / B0EXP / R0EXP**2
  eqchease_out_add_1d(:,iidnedpsi) = fact_dpsi * eqchease_out_add_1d(:,iidnedpsi) / B0EXP / R0EXP**2
  eqchease_out_add_1d(:,iidTidpsi) = fact_dpsi * eqchease_out_add_1d(:,iidTidpsi) / B0EXP / R0EXP**2
  eqchease_out_add_1d(:,iidnidpsi) = fact_dpsi * eqchease_out_add_1d(:,iidnidpsi) / B0EXP / R0EXP**2
  ! signeo has already SI units?
  ! [j] is B0/R0/mu0, [B]=B0 and sign(jbs) is sign(Ip)
  eqchease_out_add_1d(:,iijbsBav) = SIGNIPXP * eqchease_out_add_1d(:,iijbsBav) * B0EXP**2 / ZMU0 / R0EXP
  !
  ! 2. eqchease_out_add_2d
  !
  eqchease_out_add_2d(:,:,iiB) = SIGNB0XP * eqchease_out_add_2d(:,:,iiB) * B0EXP
  eqchease_out_add_2d(:,:,iidBdpsi) = SIGNB0XP * fact_dpsi * eqchease_out_add_2d(:,:,iidBdpsi) / R0EXP**2
  ! direction of dchi depends on sigma_RphiZ_eff * sigma_rhothetaphi_eff
  eqchease_out_add_2d(:,:,iidBdchi) = SIGNB0XP * fact_dtheta * eqchease_out_add_2d(:,:,iidBdchi) * B0EXP
  eqchease_out_add_2d(:,:,iidpsidR) = fact_psi * eqchease_out_add_2d(:,:,iidpsidR) * R0EXP*B0EXP
  eqchease_out_add_2d(:,:,iidpsidZ) = fact_psi * eqchease_out_add_2d(:,:,iidpsidZ) * R0EXP*B0EXP
  eqchease_out_add_2d(:,:,iidchidR) = fact_dtheta * eqchease_out_add_2d(:,:,iidchidR) /R0EXP
  eqchease_out_add_2d(:,:,iidchidZ) = fact_dtheta * eqchease_out_add_2d(:,:,iidchidZ) /R0EXP
  !
  eqchease_out_add_2d(:,:,iidRdpsi) = fact_dpsi * eqchease_out_add_2d(:,:,iidRdpsi) / R0EXP / B0EXP
  eqchease_out_add_2d(:,:,iidRdchi) = fact_dtheta * eqchease_out_add_2d(:,:,iidRdchi) * R0EXP
  eqchease_out_add_2d(:,:,iidZdpsi) = fact_dpsi * eqchease_out_add_2d(:,:,iidZdpsi) / R0EXP / B0EXP
  eqchease_out_add_2d(:,:,iidZdchi) = fact_dtheta * eqchease_out_add_2d(:,:,iidZdchi) * R0EXP

  eqchease_out_add_2d(:,:,iiAh) = eqchease_out_add_2d(:,:,iiAh) / R0EXP**2
  eqchease_out_add_2d(:,:,iidAhdpsi) = fact_dpsi * eqchease_out_add_2d(:,:,iidAhdpsi) / R0EXP**4 / B0EXP

  return
end SUBROUTINE EQCHEASE_MKSA
