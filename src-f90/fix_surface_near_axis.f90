! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
subroutine fix_surface_near_axis(psmesh,ksmesh)
  !        ####################################
  !
  !                                        AUTHORS:
  !                                        O. SAUTER,  SPC-EPFL
  !
  ! TYPICALLY PSMESH=SMISO AND KSMESH=NISO1EFF
  !
  ! assumes 1st point is not on-axis (psmesh(1)>0.)
  !
  !**********************************************************************
  !
  ! Calculations of various quantities are not precise near the plasma axis, fix them in this routine
  ! This routine should be optional, thus should not create new information, only replacing existing arrays
  ! uses similar interpos fix as used in gloqua to get vprime from rivol
  ! Add a value at s=0, with large error bar and interpolate with standard spline BC (2nd der at 0=0)
  ! using the abscissa such that the quantity is linear near axis
  !
  !**********************************************************************
  !
  USE globals
  use interpos_module
  IMPLICIT NONE
  !
  REAL(RKIND) :: PSMESH(KSMESH)
  INTEGER :: KSMESH
  !
  REAL(RKIND) :: SIGMA_psi(ksmesh+1), SIGMA_rho(ksmesh+1), ZRHO(ksmesh+1), zpsiin(ksmesh), zpsinorm(ksmesh+1), zynew(ksmesh+1)
  REAL(RKIND) :: TENS_DEF, delta_psi
  INTEGER ::  INBRHO, ipsi01 ! , I
  !
  !----*----*----*---*----*----*----*----*----*----*----*----*----*----*-
  !
  if (psmesh(1) .le. 0._rkind) then
    print *,'fix_surface_near_axis:  assume 1st point is not at 0, ask o. sauter if need to do it still in this case'
    return
  end if
  inbrho = ksmesh + 1
  !
  tens_def = -0.1_rkind
!!$  allocate(zrho(inbrho))
  zrho(1:ksmesh+1) = (/0._rkind, psmesh(1:ksmesh) /)
  zpsinorm=zrho*zrho
  zpsiin = psmesh*psmesh
!!$  allocate(sigma(inbrho))
  delta_psi = 0.003_rkind
  sigma_psi=1._rkind + 1000._rkind * exp(-(zpsinorm/delta_psi)**2)
  sigma_rho=1._rkind + 1000._rkind * exp(-(zpsinorm/delta_psi))
  !
  ! Should treat the various array calculated in surface.f90
  !
  CALL INTERPOS(zpsinorm,(/RARE(1), RARE(1:KSMESH)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=RARE(1:KSMESH), &
       & SIGMA=sigma_psi,nbc=(/0, 2/), ybc=(/RC0P, RARE(ksmesh)/))
  CALL INTERPOS(zpsinorm,(/RIP(1), RIP(1:KSMESH)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=RIP(1:KSMESH), &
       & SIGMA=sigma_psi,nbc=(/0, 2/), ybc=(/RC0P, RIP(ksmesh)/))
  CALL INTERPOS(zpsinorm,(/RIP2(1), RIP2(1:KSMESH)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=RIP2(1:KSMESH), &
       & SIGMA=sigma_psi,nbc=(/0, 2/), ybc=(/RC0P, RIP2(ksmesh)/))
  CALL INTERPOS(zpsinorm,(/RIB(1), RIB(1:KSMESH)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=RIB(1:KSMESH), &
       & SIGMA=sigma_psi,nbc=(/0, 2/), ybc=(/RC0P, RIB(ksmesh)/))
  CALL INTERPOS(zpsinorm,(/RIB2(1), RIB2(1:KSMESH)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=RIB2(1:KSMESH), &
       & SIGMA=sigma_psi,nbc=(/0, 2/), ybc=(/RC0P, RIB2(ksmesh)/))
  CALL INTERPOS(zpsinorm,(/RIBI2(1), RIBI2(1:KSMESH)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=RIBI2(1:KSMESH), &
    & SIGMA=sigma_psi,nbc=(/0, 2/), ybc=(/RC0P, RIBI2(ksmesh)/))
  CALL INTERPOS(zpsinorm,(/RIR2(1), RIR2(1:KSMESH)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=RIR2(1:KSMESH), &
       & SIGMA=sigma_psi,nbc=(/0, 2/), ybc=(/RC0P, RIR2(ksmesh)/))
  CALL INTERPOS(zpsinorm,(/RIVOL(1), RIVOL(1:KSMESH)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=RIVOL(1:KSMESH), &
    & SIGMA=sigma_psi,nbc=(/0, 2/), ybc=(/RC0P, RIVOL(ksmesh)/))
  CALL INTERPOS(zpsinorm,(/RIIE(1), RIIE(1:KSMESH)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=RIIE(1:KSMESH), &
       & SIGMA=sigma_psi,nbc=(/0, 2/), ybc=(/RC0P, RIIE(ksmesh)/))
  ! assume RIIR(axis) should be 0
  CALL INTERPOS(zpsinorm,(/0._rkind, RIIR(1:KSMESH)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=RIIR(1:KSMESH), &
       & SIGMA=sigma_psi,nbc=(/0, 2/), ybc=(/RC0P, RIIR(ksmesh)/))
  ! rleng is linear with rho, not psi, thus use rho
  CALL INTERPOS(zrho,(/RLENG(1), RLENG(1:KSMESH)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=psmesh(1:KSMESH),YOUT=RLENG(1:KSMESH), &
    & SIGMA=sigma_rho,nbc=(/0, 2/), ybc=(/RC0P, RLENG(ksmesh)/))
  CALL INTERPOS(zrho,(/RLENG1(1), RLENG1(1:KSMESH)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=psmesh(1:KSMESH),YOUT=RLENG1(1:KSMESH), &
    & SIGMA=sigma_rho,nbc=(/0, 2/), ybc=(/RC0P, RLENG1(ksmesh)/))
  ! nothing for surface, RJ1, RJ2, RJ3
  CALL INTERPOS(zpsinorm,(/RJ4(1), RJ4(1:KSMESH)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=RJ4(1:KSMESH), &
       & SIGMA=sigma_psi,nbc=(/0, 2/), ybc=(/RC0P, RJ4(ksmesh)/))
  CALL INTERPOS(zpsinorm,(/RJ5(1), RJ5(1:KSMESH)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=RJ5(1:KSMESH), &
       & SIGMA=sigma_psi,nbc=(/0, 2/), ybc=(/RC0P, RJ5(ksmesh)/))
  ! RJ5P seems quite strange near center for psi<0.01, thus treat special
  ipsi01 = minloc(abs(zpsinorm(1:inbrho)-0.01_rkind),1)
  zynew(1:KSMESH+1) = (/RJ5P(1), RJ5P(1:KSMESH)/)
  zynew(1:ipsi01) = zynew(ipsi01)
  CALL INTERPOS(zpsinorm,zynew,nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=RJ5P(1:KSMESH), &
       & SIGMA=sigma_psi,nbc=(/0, 2/), ybc=(/RC0P, RJ5P(ksmesh)/))
  ! assume RJ6(axis) should be 0
  CALL INTERPOS(zpsinorm,(/0._rkind, RJ6(1:KSMESH)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=RJ6(1:KSMESH), &
       & SIGMA=sigma_psi,nbc=(/0, 2/), ybc=(/RC0P, RJ6(ksmesh)/))
  ! assume RJ7s(axis,1) should be 0
  CALL INTERPOS(zpsinorm,(/RC0P, RJ7s(1:KSMESH,1)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=RJ7s(1:KSMESH,1), &
       & SIGMA=sigma_psi,nbc=(/0, 2/), ybc=(/RC0P, RJ7s(ksmesh,1)/))
  ! assume RJ7s(:,2) is linear with rho, not psi, thus use rho, assume (axis)=0
  CALL INTERPOS(zrho,(/RC0P, RJ7s(1:KSMESH,2)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=psmesh(1:KSMESH),YOUT=RJ7s(1:KSMESH,2), &
    & SIGMA=sigma_rho,nbc=(/0, 2/), ybc=(/RC0P, RJ7s(ksmesh,2)/))
  CALL INTERPOS(zpsinorm,(/RJ7s(1,3), RJ7s(1:KSMESH,3)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=RJ7s(1:KSMESH,3), &
       & SIGMA=sigma_psi,nbc=(/0, 2/), ybc=(/RC0P, RJ7s(ksmesh,3)/))
  CALL INTERPOS(zpsinorm,(/RJ7s(1,4), RJ7s(1:KSMESH,4)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=RJ7s(1:KSMESH,4), &
       & SIGMA=sigma_psi,nbc=(/0, 2/), ybc=(/RC0P, RJ7s(ksmesh,4)/))
  CALL INTERPOS(zpsinorm,(/RJ7s(1,5), RJ7s(1:KSMESH,5)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=RJ7s(1:KSMESH,5), &
       & SIGMA=sigma_psi,nbc=(/0, 2/), ybc=(/RC0P, RJ7s(ksmesh,5)/))
  ! assume RJ7s(axis,6) should be 0
  CALL INTERPOS(zpsinorm,(/RC0P, RJ7s(1:KSMESH,6)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=RJ7s(1:KSMESH,6), &
       & SIGMA=sigma_psi,nbc=(/0, 2/), ybc=(/RC0P, RJ7s(ksmesh,6)/))
  ! nothing for RJ7s(:,7) (would on 1/ if needed)
  ! assume RJ7s(:,8) is linear with rho, not psi, thus use rho, assume (axis)=0
  CALL INTERPOS(zrho,(/RC0P, RJ7s(1:KSMESH,8)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=psmesh(1:KSMESH),YOUT=RJ7s(1:KSMESH,8), &
    & SIGMA=sigma_rho,nbc=(/0, 2/), ybc=(/RC0P, RJ7s(ksmesh,8)/))
  ! assume RJ7s(:,9) is linear with rho, not psi, thus use rho, assume (axis)=0
  CALL INTERPOS(zrho,(/RC0P, RJ7s(1:KSMESH,9)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=psmesh(1:KSMESH),YOUT=RJ7s(1:KSMESH,9), &
    & SIGMA=sigma_rho,nbc=(/0, 2/), ybc=(/RC0P, RJ7s(ksmesh,9)/))
  ! nothing for RJ7s(:,10) (would on 1/ if needed)

  ! BCHIN, BCHIO quite oscillating at some rho, could do a std interpos on 1/ with -0.1 for all theta except 1st index which has just 0s

  CALL INTERPOS(zpsinorm,(/qpsi(1), qpsi(1:KSMESH)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=qpsi(1:KSMESH), &
       & SIGMA=sigma_psi,nbc=(/0, 2/), ybc=(/RC0P, qpsi(ksmesh)/))
  ! cdq seems quite strange near center for psi<0.01, thus treat special
  ipsi01 = minloc(abs(zpsinorm(1:inbrho)-0.01_rkind),1)
  zynew(1:KSMESH+1) = (/cdq(1), cdq(1:KSMESH)/)
  zynew(1:ipsi01) = zynew(ipsi01)
  CALL INTERPOS(zpsinorm,zynew,nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=cdq(1:KSMESH), &
       & SIGMA=sigma_psi,nbc=(/0, 2/), ybc=(/RC0P, cdq(ksmesh)/))
  ! cp is linear with rho, not psi, thus use rho, assume (axis)=0
  CALL INTERPOS(zrho,(/RC0P, CP(1:KSMESH)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=psmesh(1:KSMESH),YOUT=CP(1:KSMESH), &
    & SIGMA=(/1._rkind, sigma_rho(2:inbrho)/),nbc=(/0, 2/), ybc=(/RC0P, CP(ksmesh)/))
  ! nothing for CPDP (diverges)
  CALL INTERPOS(zpsinorm,(/ripr(1), ripr(1:KSMESH)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=ripr(1:KSMESH), &
       & SIGMA=sigma_psi,nbc=(/0, 2/), ybc=(/RC0P, ripr(ksmesh)/))
  CALL INTERPOS(zpsinorm,(/rjdotb(1), rjdotb(1:KSMESH)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=rjdotb(1:KSMESH), &
       & SIGMA=sigma_psi,nbc=(/0, 2/), ybc=(/RC0P, rjdotb(ksmesh)/))
  CALL INTERPOS(zpsinorm,(/rb2av(1), rb2av(1:KSMESH)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=rb2av(1:KSMESH), &
       & SIGMA=sigma_psi,nbc=(/0, 2/), ybc=(/RC0P, rb2av(ksmesh)/))
  CALL INTERPOS(zpsinorm,(/rjpar(1), rjpar(1:KSMESH)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=rjpar(1:KSMESH), &
       & SIGMA=sigma_psi,nbc=(/0, 2/), ybc=(/RC0P, rjpar(ksmesh)/))
  CALL INTERPOS(zpsinorm,(/rell(1), rell(1:KSMESH)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=rell(1:KSMESH), &
       & SIGMA=sigma_psi,nbc=(/0, 2/), ybc=(/RC0P, rell(ksmesh)/))
  ! nothing for dimensional: elongation, r_in/outboard, triang, amin, rgeo, bmin, bmax, aratio: they seem ok
  ! RFCIRC impose 1 at 0
  CALL INTERPOS(zpsinorm,(/1._rkind, rfcirc(1:KSMESH)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=rfcirc(1:KSMESH), &
       & SIGMA=(/1._rkind, sigma_psi(2:inbrho)/),nbc=(/2, 2/), ybc=(/1._rkind, rfcirc(ksmesh)/))

!!$  CALL INTERPOS(zpsinorm,(/(1), (1:KSMESH)/),nin=inbrho,nout=ksmesh,TENSION=TENS_DEF,xout=zpsiin(1:KSMESH),YOUT=(1:KSMESH), &
!!$       & SIGMA=sigma_psi,nbc=(/0, 2/), ybc=(/RC0P, (ksmesh)/))

!!$  write(502,*) '% psmesh RARE RIP RIP2 RIB RIB2 RIBI2 RIR2 RIVOL RIIE RIIR RLENG RLENG1 '
!!$  write(502,'(1p13e15.7)') (psmesh(i),RARE(i),RIP(i),RIP2(i),RIB(i),RIB2(i),RIBI2(i),RIR2(i),RIVOL(i),RIIE(i),RIIR(i),RLENG(i),RLENG1(i),i=1,ksmesh)
!!$  write(503,*) '% eqchease_out(index_out)%profiles_1d%surface(K+1) RJ1 RJ2 RJ3 RJ4 RJ5 RJ5P RJ6 RJ7s(:,1:10) '
!!$  write(503,'(1p9e15.7)') (psmesh(i),eqchease_out(index_out)%profiles_1d%surface(i+1), RJ1(i), RJ2(i), RJ3(i), RJ4(i), RJ5(i), RJ5P(i), RJ6(i), i=1,ksmesh)
!!$  write(5031,*) '% RJ7s(:,1:10) RJ7s(:,1:10) RJ7s(:,1:10) RJ7s(:,1:10) RJ7s(:,1:10) RJ7s(:,1:10) RJ7s(:,1:10) RJ7s(:,1:10) RJ7s(:,1:10) RJ7s(:,1:10) '
!!$  write(5031,'(1p11e15.7)') (psmesh(i),RJ7s(i,1), RJ7s(i,2), RJ7s(i,3), RJ7s(i,4), RJ7s(i,5), RJ7s(i,6), RJ7s(i,7), RJ7s(i,8), RJ7s(i,9), RJ7s(i,10), i=1,ksmesh)
!!$  write(504,*) '% rho BCHIN(15,:) BCHIO(25,:) QPSI CDQ CP CPDP RIPR RJDOTB RB2AV RJPAR RELL '
!!$  write(504,'(1p12e15.7)') (psmesh(i),BCHIN(15,i), BCHIO(25,i), QPSI(i), CDQ(i), CP(i), CPDP(i), RIPR(i), RJDOTB(i), RB2AV(i), RJPAR(i), RELL(i), i=1,ksmesh)
!!$  write(505,*) '% eqchease_out(index_out)%profiles_1d%elongation eqchease_out(index_out)%profiles_1d%r_inboard eqchease_out(index_out)%profiles_1d%r_outboard amin  rgeo aratio tria_lower'
!!$  write(505,'(1p8e15.7)') (psmesh(i),eqchease_out(index_out)%profiles_1d%elongation(i+1), eqchease_out(index_out)%profiles_1d%r_inboard(i+1), eqchease_out(index_out)%profiles_1d%r_outboard(i+1), eqchease_out_add_1d(i+1,iiamin), eqchease_out_add_1d(i+1,iirgeo), ARATIO(i), eqchease_out(index_out)%profiles_1d%tria_lower(i+1), i=1,ksmesh)
!!$  write(506,*) '%eqchease_out(index_out)%profiles_1d%tria_upper eqchease_out_add_1d(:+1,iiBmin) eqchease_out_add_1d(:+1,iiBmax) eqchease_out(index_out)%profiles_1d%b_min(:+1) b_max  rfcirc'
!!$  write(506,'(1p7e15.7)') (psmesh(i),eqchease_out(index_out)%profiles_1d%tria_upper(i+1), eqchease_out_add_1d(i+1,iiBmin), eqchease_out_add_1d(i+1,iiBmax), eqchease_out(index_out)%profiles_1d%b_min(i+1), eqchease_out(index_out)%profiles_1d%b_max(i+1), RFCIRC(i), i=1,ksmesh)
  !
  !
  return
end subroutine FIX_SURFACE_NEAR_AXIS
