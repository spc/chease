<!-- Copyright 2024 SPC-EPFL -->
<!--  -->
<!-- Licensed under the Apache License, Version 2.0 (the "License"); -->
<!-- you may not use this file except in compliance with the License. -->
<!-- You may obtain a copy of the License at -->
<!--  -->
<!--     http://www.apache.org/licenses/LICENSE-2.0 -->
<!--  -->
<!-- Unless required by applicable law or agreed to in writing, software -->
<!-- distributed under the License is distributed on an "AS IS" BASIS, -->
<!-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. -->
<!-- See the License for the specific language governing permissions and -->
<!-- limitations under the License. -->
<!-- ********************************************************************** -->
<?xml version="1.0" encoding="UTF-8"?>
<?jaxfront version=2.66;time=2011-12-05 17:17:03.817?>
<parameters xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <nitmopt> 22 </nitmopt> <!-- 22 within kepler, 1 to read from ITM database, 11 to read and write, 10 to write, 0 to be independent from ITM database -->
    <ndiagop> -1  </ndiagop> <!-- set to -1 to remove outputs of array at the end -->
    <nverbose> 0 </nverbose> <!-- set to 1 for more messages and to 2 for full output -->
    <cocos_in> 13 </cocos_in> <!-- 13 for original ITM,11 for ITER, 2 for eqdsk/chease standard -->
    <cocos_out> 13 </cocos_out> <!-- 13 for original ITM, 11 for ITER, 2 for eqdsk/chease standard -->

    <tensbnd> -0.1 </tensbnd> <!-- -1. is standard smoothing, 0 to avoid problems in loops, -10 if boundary hectic or experimental -->
    <tensprof> -0.1 </tensprof> <!-- 0 for no smoothing of input profiles, -1 or -10 for stronger and stronger smoothing -->
    <nbpsout> 300 </nbpsout>
    <signipxp> -9 </signipxp> <!-- to let the code follow the cocos_in -> _out transformation for sign of Ip-->
    <signb0xp> -9 </signb0xp> <!-- to let the code follow the cocos_in -> _out transformation for sign of Ip-->

    <epslon> 1.0e-8 </epslon>
    <relax> 0.2  </relax>

    <nchi> 100  </nchi> <!-- can put 160 for nchi, npiso, npsi with ns=nt=30 or 40 for better resolution -->
    <niso> 100  </niso>
    <npsi> 100  </npsi>
    <ns> 30  </ns>
    <nt> 30  </nt>
    <nturn> 20  </nturn>
    <nrbox> 101 </nrbox>
    <nzbox> 101 </nzbox>

    <cpress> 1.0 </cpress>
    <ncscal> 4  </ncscal> <!-- 4 for no scaling, 1 for q(csspec) definition (2, for Ip) -->
    <csspec> 0.0  </csspec>
    <qspec> 0.9  </qspec> <!-- set qsppec=-q0 and ncscal=1 to force a given q(csspec) -->

    <nbsexpq> 0000 </nbsexpq> <!-- 0000 if exptnz not given, 1111 otherwise -->
    <nsurf> 6  </nsurf>
    <nppfun> 8 </nppfun> <!-- nppfun=4 for pprime and 8 for p as input (needs nsttp.ge.2 for nppfun=8) -->
    <nfunc> 4  </nfunc>
    <nfunrho> 1 </nfunrho>
    <nrhomesh> 1 </nrhomesh>
    <nsttp> 4 </nsttp>  <!-- set nsttp=1 for ffprime, 2 for Istar=jphi, 3 for Ipar and 4 for jpar=<j.B>/B0 -->
    <nideal> 6  </nideal> <!-- standard mapping -->

    <ner> 1 </ner> <!-- straight field line, NER=2, NEGP=0, since Jacobian=C(psi) R^NER grad(psi)^NEGP -->
    <negp> -1 </negp> <!-- straight field line, NER=2, NEGP=0, since Jacobian=C(psi) R^NER grad(psi)^NEGP -->
    <nmesha> 1  </nmesha>
    <nmeshc> 1  </nmeshc>
    <nmeshd> 1  </nmeshd>
    <nmeshe> 0  </nmeshe>
    <nmeshpol> 1  </nmeshpol>
    <npoida> 2  </npoida>
    <npoidc> 2  </npoidc>
    <npoidd> 2  </npoidd>
    <npoide> 4  </npoide>
    <npoidq> 10  </npoidq>
    <solpda> 0.70 </solpda>
    <solpdc> 0.70 </solpdc>
    <solpdd> 0.70 </solpdd>
    <solpde> 0.50 </solpde>
    <solpdpol> 0.25 </solpdpol>
    <aplace> .96 1.0 </aplace>
    <awidth> .07 .02 </awidth>
    <cplace> .95 .99 1.0 </cplace>
    <cwidth> .10 .02 .05 </cwidth>
    <dplace> -1.80 -1.80 4.0 </dplace>
    <dwidth> .18 .08 .05 </dwidth>
    <eplace> -1.70 -1.70  1.70  1.70 </eplace>
    <ewidth> .18 .08 .18 .08 </ewidth>
    <qplace> 1.00 1.00 2.00 2.00 3.00 3.00 4.00 4.00 4.41 4.41 </qplace>
    <qwidth> 0.13 0.04 0.09 0.04 0.07 0.02 0.04 0.01 0.01 0.001 </qwidth>

    <nprof2d> 11 </nprof2d> <!-- 1: compute simple profiles_2d(2), 2: complete, 11: as 1 and _2d(3) on rhotor_norm mesh, 12: complete _2d(3) -->

</parameters>
