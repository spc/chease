! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*DECK C2SA05
!*CALL PROCESS
SUBROUTINE QPLACS
  !        #################
  !
  !                                        AUTHORS:
  !                                        H. LUTJENS,  CRPP-EPFL
  !                                        A. BONDESON, CRPP-EPFL
  !**********************************************************************
  !                                                                     *
  ! C2SA05 : DENSIFY STABILITY MESH AT A PREDEFINED SET OF Q-VALUES     *
  !          GIVEN IN QPLACS (SEE SECTION 6.4.5 IN PUBLICATION)         *
  !                                                                     *
  !**********************************************************************
  !
  USE globals
  USE interpol
  IMPLICIT NONE
  !
  INTEGER          ::     J
  INTEGER          ::     J12
  INTEGER          ::     J13
  INTEGER          ::     JPLACE
  INTEGER          ::     J11
  INTEGER          ::     J10
  INTEGER          ::     J9
  INTEGER          ::     J7
  INTEGER          ::     J5
  REAL(RKIND)      ::     ZCSCAL
  INTEGER          ::     J4
  INTEGER          ::     J3
  REAL(RKIND)      ::     ZCSHFT
  REAL(RKIND)      ::     TICS
  REAL(RKIND)      ::     QICS
  INTEGER          ::     ISRCHFGE
  INTEGER          ::     ICS
  REAL(RKIND)      ::     ZQ
  REAL(RKIND)      ::     ZS
  INTEGER          ::     J2
!!$  INTEGER          ::     J1
  DIMENSION &
       &   ZQ(NPPSI1),   ZS(NPPSI1)
  !
  !----*----*----*---*----*----*----*----*----*----*----*----*----*----*-
  !
!!$  DO J1=1,NPSI
!!$     CSM(J1) = .5_RKIND * (CS(J1+1) + CS(J1))
!!$  END DO
!!$  CSM(NPSI1) = 1._RKIND
  !
  CALL PREMAP(3)
  !
  ! smiso is mesh of psiiso from cs(2:npsi+1) without axis
  zs(1) = rc0p
  zs(2:npsi1) = smiso(1:npsi)
  !
  IF (NRFP .EQ. 0) THEN
     !
     !**********************************************************************
     !                                                                     *
     ! TOKAMAK EQUILIBRIUM                                                 *
     !                                                                     *
     !**********************************************************************
     !
     IF (NCSCAL .EQ. 1) THEN
       !
        DO J2=2,NPSI1
           !
           ZQ(J2) = .5_RKIND * TMF(J2-1) * CIDQ(J2-1) / CPI
           !
        END DO
        !
        ICS = ISRCHFGE(NPSI1,ZS,1,CSSPEC) - 1
        !
        ! need mesh from 0 to 1 to find q places, but uses only sutrface values from psiiso
        ! since axis values in any case obtained with fcccc0 => ics=3 min to use zq(2:...)
        IF (ICS .LT. 3)         ICS = 3
        IF (ICS .GE. NPSI1 - 1) ICS = NPSI1 - 2
        !
        QICS = FCCCC0(ZQ(ICS-1),ZQ(ICS),ZQ(ICS+1),ZQ(ICS+2), &
          &           ZS(ICS-1),ZS(ICS),ZS(ICS+1),ZS(ICS+2),CSSPEC)
        TICS = FCCCC0(TMF(ICS-2),TMF(ICS-1),TMF(ICS),TMF(ICS+1), &
          &           ZS(ICS-1),ZS(ICS), ZS(ICS+1),ZS(ICS+2),CSSPEC)
        !
        ZCSHFT = TICS**2 * ((QSPEC / QICS)**2 - 1._RKIND)
        !
        DO J3=2,NPSI1
          ZQ(J3) = ZQ(J3) * SQRT(1._RKIND + ZCSHFT / TMF(J3-1)**2)
        END DO
        zq(1)    = fcccc0(zq(2),zq(3),zq(4),zq(5), &
          &               zs(2),zs(3),zs(4),zs(5),rc0p)
        !
     ELSE IF (NCSCAL .EQ. 2) THEN
        !
        T0 = FCCCC0(TMF(1),TMF(2),TMF(3),TMF(4), &
             &      zs(2),zs(3),zs(4),zs(5),rc0p)
        !
        DO J4=2,NPSI1
          ZQ(J4) = .5_RKIND * TMF(J4-1) * CIDQ(J4-1) / CPI
        END DO
        !
        IF (NSURF .EQ. 1) THEN
           ZCSCAL = 1._RKIND
        ELSE
           ZCSCAL = CURRT / CUROLD
        ENDIF
        !
        IF (NTMF0 .EQ. 0) THEN
          ZCSHFT = 1._RKIND - (ZCSCAL * TMF(NPSI1-1))**2
        ELSE IF (NTMF0 .EQ. 1) THEN
          ZCSHFT = 1._RKIND - (ZCSCAL * T0)**2
        ENDIF
        !
        DO J5=2,NPSI1
          ZQ(J5) = ZQ(J5) * SQRT(1._RKIND + ZCSHFT / (ZCSCAL*TMF(J5-1))**2)
        END DO
        zq(1)    = fcccc0(zq(2),zq(3),zq(4),zq(5), &
          &               zs(2),zs(3),zs(4),zs(5),rc0p)
        !
     ELSE IF (NCSCAL .EQ. 3) THEN
        !
        DO J7=2,NPSI1
           ZQ(J7) = .5_RKIND * TMF(J7-1) * CIDQ(J7-1) / CPI
        END DO
        !
        ICS = ISRCHFGE(NPSI1,ZS,1,CSSPEC) - 1
        !
        IF (ICS .LT. 3)      ICS = 3
        IF (ICS .GE. NPSI1 - 1) ICS = NPSI1 - 2
        !
        QICS = FCCCC0(ZQ(ICS-1),ZQ(ICS),ZQ(ICS+1),ZQ(ICS+2), &
             &        ZS(ICS-1),ZS(ICS),ZS(ICS+1),ZS(ICS+2),CSSPEC)
        TICS = FCCCC0(TMF(ICS-2),TMF(ICS-1),TMF(ICS),TMF(ICS+1), &
             &        ZS(ICS-1),ZS(ICS), ZS(ICS+1),ZS(ICS+2),CSSPEC)
        !
        ZCSHFT = TICS**2 * ((QSPEC / QICS)**2 - 1._RKIND)
        !
        DO J9=2,NPSI1
           ZQ(J9) = ZQ(J9) * SQRT(1._RKIND + ZCSHFT / TMF(J9-1)**2)
        END DO
        zq(1)    = fcccc0(zq(2),zq(3),zq(4),zq(5), &
          &               zs(2),zs(3),zs(4),zs(5),rc0p)
        !
     ELSE IF (NCSCAL .EQ. 4) THEN
        !
        DO J10=2,NPSI1
           ZQ(J10) = .5_RKIND * TMF(J10-1) * CIDQ(J10-1) / CPI
        END DO
        zq(1)    = fcccc0(zq(2),zq(3),zq(4),zq(5), &
          &               zs(2),zs(3),zs(4),zs(5),rc0p)
        !
     ENDIF
     !
     !**********************************************************************
     !                                                                     *
     ! REVERSED FIELD PINCH EQUILIBRIUM                                    *
     !                                                                     *
     !**********************************************************************
     !
  ELSE IF (NRFP .EQ. 1) THEN
     !
     DO J11=2,NPSI1
        ZQ(J11) = .5_RKIND * TMF(J11-1) * CIDQ(J11-1) / CPI
     END DO
     zq(1)    = fcccc0(zq(2),zq(3),zq(4),zq(5), &
       &               zs(2),zs(3),zs(4),zs(5),rc0p)
     !
  ENDIF
  !
  JPLACE = 0
  !
  DO J13=1,NPOIDQ
     !
     DO J12=1,NPSI
        !
        IF ((QPLACE(J13)-ZQ(J12))*(QPLACE(J13)-ZQ(J12+1)) .LE. 0._RKIND) THEN
           !
           JPLACE = JPLACE + 1
           !
           APLACE(JPLACE) = ZS(J12) + (ZS(J12+1) - ZS(J12)) * &
                &                       (QPLACE(J13)-ZQ(J12)) / (ZQ(J12+1)-ZQ(J12))
           AWIDTH(JPLACE) = QWIDTH(J13)
           !
        ENDIF
        !
     END DO
  END DO
  !
  NPOIDA = JPLACE
  !
  WRITE(6,'(/,A20,/,A20,I2,A,/,(1P10E12.4))') ' AFTER Q PACKING:' &
       &     ,'APLACE(1:',NPOIDA,')',(APLACE(J),J=1,NPOIDA)
  WRITE(6,'(A20,I2,A,/,(1P10E12.4))') &
       &     'AWIDTH(1:',NPOIDA,')',(AWIDTH(J),J=1,NPOIDA)
  !
  RETURN
END SUBROUTINE QPLACS
