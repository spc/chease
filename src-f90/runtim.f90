! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
         SUBROUTINE RUNTIM
!        -----------------
!
!  UPDATE CPU TIME (SECS) AND PRINT IT
!
         use prec_const
         use globals
         implicit none
!
         real(rkind) :: cptime
!
         CPTIME = 0._rkind
!
         call cpu_time(cptime)
!
         CPTIME = CPTIME - STIME
!
         if (nverbose .ge. 1) WRITE (6,9900) CPTIME
!
         RETURN
!
 9900    FORMAT(/,1X,'CPU TIME USED SO FAR =',1PE14.6,' SECS')
!
         END
