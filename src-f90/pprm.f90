! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*DECK C2SP10
!*CALL PROCESS
SUBROUTINE PPRM(KPPR1,KFIN)
  !        #####################
  !                                        AUTHORS:
  !                                        H. LUTJENS,  CRPP-EPFL
  !                                        A. BONDESON, CRPP-EPFL
  !**********************************************************************
  !                                                                     *
  !  C2SP10  MODIFY P' DURING BALLOONING OPTIMIZATION ACCORDING TO THE  *
  !          ALGORITHM DESCRIBED IN SECTION 5.5 OF THE PUBLICATION      *
  !                                                                     *
  !**********************************************************************
  !
  USE globals
  IMPLICIT NONE
  !
  INTEGER          ::     KPPR1, KFIN
  !
  REAL(RKIND)      ::     ZDIFF(KPPR1), ZWORK(KPPR1)
  INTEGER          ::     ISTAB, ISUM, IP01, J960, J910
  INTEGER          ::     J, J11, J12, J13, J14, J15, J16, J17, J18, iunit_output, iunit_cols
  INTEGER, SAVE    ::     ICOUNT=0
  REAL(RKIND)      ::     ZEPS, ZERRMAX, ZERRTOT
  PARAMETER(ZEPS = 1.E-2_RKIND)
  !
  IF (KPPR1 .NE. NPPR+1) THEN
    write(0,*) 'PROBLEM IN PPRM, KPPR1=',KPPR1,' .NE. NPPR+1=',NPPR+1
    write(eqchease_out(index_out)%codeparam%output_diag(1),*) 'in pprm: PROBLEM IN PPRM, KPPR1=',KPPR1,' .NE. NPPR+1=',NPPR+1
    eqchease_out(index_out)%codeparam%output_flag = -201
    return
  END IF
  !
  ICOUNT = ICOUNT + 1
  ISTAB = 10
  ! Unit 95 also used (historical)
  ! Unit 55 for all nblop outputs that used to be in stdout except the list of profiles/lambda which are now in 56/57
  iunit_output = 55
  iunit_cols = 56 ! and iunit_cols+1=57 used for columns with different layout
  WRITE(iunit_output,*) (NP4(J),NP3(J),NP2(J),NP1(J),NP0(J),J=1,NPPR+1)
  WRITE(iunit_output,100) 'UNSTABLES VALUES : '
  !
  DO J11=1,NPPR+1
     !
     XP3(J11) = XP2(J11)
     XP2(J11) = XP1(J11)
     XP1(J11) = XP0(J11)
     XP0(J11) = RPRM(J11)
     !
     NP3(J11) = NP2(J11)
     NP2(J11) = NP1(J11)
     NP1(J11) = NP0(J11)
     !
     ! do not test for Mercier but limit to s>0.2 and q>1 in bltest
     IF (NCBAL(J11) .EQ. 0) THEN
        ! STABLE
        IF (NP0(J11) .NE. 0) NP0(J11) = 1
        !
        IF (XP0(J11).NE.0.0_RKIND) THEN
           !
           IF (ISTAB.EQ.10) ISTAB=1
           IF (ISTAB.EQ.2)  ISTAB=3
           !
        ENDIF
        !
     ELSE IF ( NCBAL(J11) .GE. 1) THEN
        ! UNSTABLE
        WRITE(iunit_output,200) J11,XP0(J11)
        !
        IF (NP0(J11) .NE. 0) NP0(J11) = - 1
        !
        XP4(J11) = XP0(J11)
        NP4(J11) = NP0(J11)
        !
        IF (XP0(J11) .NE. 0._RKIND) THEN
           !
           IF (ISTAB .EQ. 10) ISTAB=2
           IF (ISTAB .EQ. 1)  ISTAB=3
           !
        ENDIF
     ENDIF
     !
   END DO
  !
  WRITE(95,400) (PCSM(J),XP0(J),J=1,NPPR+1)
  WRITE(95,410) (PCSM(J),NP0(J),J=1,NPPR+1)
  !
  !***********************************************************************
  !                                                                      *
  ! ISTAB=1 IF THE PROFILE IS STABLE   EVERYWHERE                        *
  !      =2 IF THE PROFILE IS UNSTABLE EVERYWHERE                        *
  !      =3 OTHERWISE                                                    *
  !                                                                      *
  !***********************************************************************
  !
  IF (ISTAB .EQ. 1) THEN
     !
     IF (NSRCH .EQ. 2) THEN
        !
        DO J12=1,NPPR+1
           !
           XPPRMN(J12) = XP0(J12)
           XLAMB(J12)  = 0._RKIND
           XP0(J12)    = XP4(J12)
           NP0(J12)    = NP4(J12)
           !
        END DO
        !
        WRITE(iunit_output,*) 'MINIMUM FOUND'
        WRITE(iunit_output,'(F15.8)') (XPPRMN(J),J=1,NPPR+1)
        !
        NSRCH = 2
        !
     ENDIF
     !
  ENDIF
  !
  IF (ISTAB .EQ. 2) THEN
     !
     IF (NSRCH .EQ. 1) THEN
        !
        DO J13=1,NPPR+1
           !
           XPPRMN(J13) = 0._RKIND
           XPPRMX(J13) = XP0(J13)
           XPPRDF(J13) = XPPRMX(J13) - XPPRMN(J13)
           XLAMB(J13)  = 1._RKIND
           NP1(J13)    = 0
           !
        END DO
        !
        WRITE(iunit_output,*)
        WRITE(iunit_output,*) 'MAXIMUM FOUND'
        !
        NSRCH = 3
        !
     ENDIF
     !
  ENDIF
  !
  !***********************************************************************
  !                                                                      *
  ! KFIN = -1 : FIRST ITERATION                                          *
  !         0 : ERROR LARGER THAN ZEPS                                   *
  !         1 : ERROR SMALLER THAN ZEPS BUT NOT STABLE EVERYWHERE        *
  !         2 : ERROR SMALLER THAN ZEPS AND STABLE EVERYWHERE  OR        *
  !             ERROR SMALLER THAN ZEPS / 10.0                           *
  !         3 : CONVERGED                                                *
  !                                                                      *
  !***********************************************************************
  !
  write(iunit_output,*) 'KFIN = ',KFIN,'    ISTAB = ',ISTAB,'   NSRCH = ',NSRCH
  IF (KFIN .EQ. -1) THEN
     !
     DO J14=1,NPPR+1
        !
        IF (PCSM(J14) .LT. .1_RKIND) THEN
           !
           XP0(J14) = 0._RKIND
           NP0(J14) = 0
           !
        ENDIF
        !
        XP4(J14) = XP0(J14)
        NP4(J14) = NP0(J14)
        !
        XPPRMN(J14) = 0._RKIND
        XPPRMX(J14) = XP0(J14)
        XPPRDF(J14) = XP0(J14)
        !
     END DO
     !
     KFIN = 0
     !
  ENDIF
  !
  !***********************************************************************
  !                                                                      *
  ! IF NSRCH=1, SEARCH A COMPLETELY UNSTABLE PROFILE                     *
  !          2,                     STABLE   PROFILE                     *
  !          3, SEARCH OPTIMIZED PROFILE (1st time)                      *
  !          4,        OPTIMIZED PROFILE                                 *
  !                                                                      *
  !***********************************************************************
  !
  IF (NSRCH .EQ. 1) THEN
     !
     DO J15=1,NPPR+1
        !
        IF (NP0(J15) .EQ. 1 .AND. XP0(J15) .NE. 0._RKIND) THEN
           !
          if (icount .LE. 10) then
            XLAMB(J15) = 1.4_RKIND
          else
            XLAMB(J15) = 1.15_RKIND
          end if
           !
           IF (XP0(J15) .NE. 0._RKIND) XP0(J15) = XP0(J15) - 0.02_RKIND
           !
        ELSE
           !
           XLAMB(J15)=1._RKIND
           !
        ENDIF
        !
        RPRM(J15) = XLAMB(J15) * XP0(J15)
        !
        IF (RPRM(J15) .GT. 0._RKIND) RPRM(J15) = 0._RKIND
        !
     END DO
     !
     GOTO 9000
     !
  ELSE IF (NSRCH .EQ. 2) THEN
     !
     DO J16=1,NPPR+1
        !
        IF (NP0(J16) .EQ. -1 .AND. XP0(J16) .NE. 0._RKIND) THEN
           !
           XLAMB(J16) = .5_RKIND
           !
        ELSE
           !
           XLAMB(J16) = 1._RKIND
           !
        ENDIF
        !
        RPRM(J16) = XLAMB(J16)*XP0(J16)
        !
        IF (RPRM(J16) .GT. 0._RKIND) RPRM(J16) = 0._RKIND
        !
     END DO
     !
     GOTO 9000
     !
  ELSE IF (NSRCH .EQ. 3) THEN
     !
     DO J17=1,NPPR+1
       !
       if (icount .LE. 20) then
         X2SRCH(J17) = 0.3_RKIND
       else
         X2SRCH(J17) = 0.1_RKIND
       end if
       XLAMB(J17)  = 1._RKIND
        !
        IF (NP0(J17) .EQ. -1) THEN
           !
           IF (XP0(J17) .GT. -.005_RKIND .AND. XP0(J17-1) .EQ. 0._RKIND) THEN
              !
              WRITE(iunit_output,*) J17,XP0(J17),' SET TO ZERO'
              !
              XP0(J17)    = 0._RKIND
              XPPRMN(J17) = 0._RKIND
              XPPRDF(J17) = 0._RKIND
              !
           ENDIF
           !
           XLAMB(J17) = XLAMB(J17)  - X2SRCH(J17)
           !
        ELSE
           !
           XLAMB(J17) = XLAMB(J17)  + X2SRCH(J17)
           !
        ENDIF

        !
        RPRM(J17)  = XPPRMN(J17) + XLAMB(J17) * XPPRDF(J17)
        !
        IF (RPRM(J17) .GT. 0._RKIND) RPRM(J17) = 0._RKIND
        !
     END DO
     !
     NSRCH = 4
     !
     GOTO 9500
     !
  ELSE IF (NSRCH .EQ. 4) THEN
     !
     DO J18=1,NPPR+1
        !
        IF (XPPRDF(J18) .EQ. 0._RKIND) GOTO 18
        !
        IP01 = NP0(J18) * NP1(J18)
        ISUM = ABS(NP1(J18) + NP2(J18) + NP3(J18))
        !
        IF (IP01 .LT. 0) THEN
           !
           X2SRCH(J18) = .5_RKIND * X2SRCH(J18)
           !
        ELSE IF (ISUM .EQ. 3 .AND. KFIN .EQ. 0) THEN
           !
           X2SRCH(J18) = 1.2_RKIND * X2SRCH(J18)
           !
        ENDIF
        !
        IF (NP0(J18) .EQ. -1) THEN
           !
           IF (XP0(J18) .GT. -.005_RKIND .AND. XP0(J18-1) .EQ. 0._RKIND) THEN
              !
              WRITE(iunit_output,*) J18,XP0(J18),' SET TO ZERO'
              !
              XP0(J18)    = 0._RKIND
              XPPRMN(J18) = 0._RKIND
              XPPRDF(J18) = 0._RKIND
              !
           ENDIF
           !
           XLAMB(J18) = XLAMB(J18) - X2SRCH(J18)
           RPRM(J18)  = XPPRMN(J18) + XLAMB(J18) * XPPRDF(J18)
           !
           IF (RPRM(J18) .GT. 0._RKIND) RPRM(J18) = 0._RKIND
           !
        ELSE IF (KFIN .EQ. 0 .AND. NP0(J18) .EQ. 1) THEN
           !
           XLAMB(J18) = XLAMB(J18)  + X2SRCH(J18)
           RPRM(J18)  = XPPRMN(J18) + XLAMB(J18) * XPPRDF(J18)
           !
           IF (RPRM(J18) .GT. 0._RKIND) RPRM(J18) = 0._RKIND
           !
        ENDIF
        !
18      CONTINUE
     END DO
     !
     GOTO 9500
     !
  ENDIF
  !
  !***********************************************************************
  !                                                                      *
  !                            OUTPUT                                    *
  !                                                                      *
  !***********************************************************************
  !
9000 CONTINUE
  !
  ZDIFF(1:NPPR+1) = ABS(RPRM(1:NPPR+1) - XP0(1:NPPR+1))
  ZERRTOT = SUM(ZDIFF(1:NPPR+1)) / REAL(NPPR+1,RKIND)
  !
  if (icount .eq. 1) WRITE(iunit_cols+1,'(A)') '% ICOUNT  I       CSM       P1P  IP1       P0P  IP0      LAMBDA          PP'
  DO J910=1,NPPR+1
     WRITE(iunit_cols+1,250) ICOUNT,J910,PCSM(J910),XP1(J910),NP1(J910), &
          &                   XP0(J910),NP0(J910), &
          &                   XLAMB(J910),RPRM(J910)
  END DO
  WRITE(iunit_output,*) 'ICOUNT = ',ICOUNT,':     ERROR  : ',ZERRTOT,ZEPS,'          ISTAB,NSRCH : ',ISTAB,NSRCH
  WRITE(6,*) 'PPRM: ICOUNT = ',ICOUNT,':     ERROR  : ',ZERRTOT,ZEPS,'          ISTAB,NSRCH : ',ISTAB,NSRCH
  !
  CALL SPLINE(NPPR+1,PCSM,RPRM,D2RPRM,ZWORK)
  !
  RETURN
  !
9500 CONTINUE
  !
  ZDIFF(1:NPPR+1) = ABS(RPRM(1:NPPR+1) - XP0(1:NPPR+1))
  ZERRMAX = MAXVAL(ZDIFF(1:NPPR+1))
  ZERRTOT = SUM(ZDIFF(1:NPPR+1)) / REAL(NPPR+1,RKIND)
  !
  if (icount .eq. 1) WRITE(iunit_cols,'(A,A)') '% ICOUNT   I          CSM               PP0      IP0         MIN               MAX              LAMBDA', &
    &             '         DIFF                PP'
  DO J960=1,NPPR+1
     WRITE(iunit_cols,260) ICOUNT,J960,PCSM(J960),XP0(J960),NP0(J960), &
          &                   XPPRMN(J960),XPPRMX(J960),XLAMB(J960), &
          &                   ZDIFF(J960),RPRM(J960)
  END DO
  !
  IF (ZERRTOT .LE. ZEPS) THEN
     !
     IF (ZERRTOT .EQ. 0._RKIND .AND. ISTAB .NE. 1) THEN
       write(0,*) '(ZERRTOT .EQ. 0._RKIND .AND. ISTAB .NE. 1)'
       write(eqchease_out(index_out)%codeparam%output_diag(1),*) 'in pprm: (ZERRTOT .EQ. 0._RKIND .AND. ISTAB .NE. 1)'
       eqchease_out(index_out)%codeparam%output_flag = -202
       return
     END IF
     !
     WRITE(iunit_output,'(A,I3,A,1p3e11.2,A,2I3)') 'ICOUNT =',ICOUNT,' : EPSILON > ERROR > EPSILON / 5._RKIND: ',ZEPS,ZERRTOT,ZEPS/5._RKIND, &
       & '          ISTAB, NSRCH-3: ',ISTAB,NSRCH-3
     WRITE(6,'(A,I3,A,1p3e11.2,A,2I3)') 'PPRM converged: ICOUNT =',ICOUNT,' : EPSILON > ERROR > EPSILON / 5._RKIND: ',ZEPS,ZERRTOT,ZEPS/5._RKIND, &
       & '          ISTAB, NSRCH-3: ',ISTAB,NSRCH-3
     !
     KFIN = 3
     !
  ELSE
     !
    WRITE(iunit_output,'(A,I3,A,1p3e11.2,A,2I3)') 'ICOUNT =',ICOUNT,' : ERROR > EPSILON, ERROR MAX: ',ZERRTOT,ZEPS,ZERRMAX, &
       & '          ISTAB, NSRCH-3: ',ISTAB,NSRCH-3
    WRITE(6,'(A,I3,A,1p3e11.2,A,2I3)') 'PPRM: ICOUNT =',ICOUNT,' : ERROR > EPSILON, ERROR MAX: ',ZERRTOT,ZEPS,ZERRMAX, &
       & '          ISTAB, NSRCH-3: ',ISTAB,NSRCH-3
     !
  ENDIF
  !
  CALL SPLINE(NPPR+1,PCSM,RPRM,D2RPRM,ZWORK)
  !
  RETURN
  !
100 FORMAT (A)
110 FORMAT (A,/)
  !
200 FORMAT (I8,2X,F15.8,/)
250 FORMAT (2I5,2X,F15.8,2(2X,F15.8,I5),2(2X,F15.8))
260 FORMAT (2I5,2(2X,F15.8),I5,5(2X,F15.8))
  !
400 FORMAT (E15.8,2X,E15.8)
410 FORMAT (E15.8,2X,I5)
  !
END SUBROUTINE PPRM
