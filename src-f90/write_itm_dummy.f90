! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
subroutine write_itm(equil_out,kitmopt,kitmshot,kitmrun,kitmocc,citmtree)
  !
  use globals
  use euITM_schemas                       ! module containing the equilibrium type definitions
  IMPLICIT NONE
  type(type_equilibrium),pointer      :: equil_out(:)
  character*120  :: citmtree
  integer        :: kitmopt, kitmshot, kitmrun, kitmocc
  !
  write(0,*) 'This routine should be linked to write_itm.f90 when CHEASE is compiled without libraries for ITM structure'
  write(0,*) 'It is not supposed to be called.'
  write(0,*) 'NITMOPT should always be -1 in such cases and routines load_itm and write_itm are not called'
  eqchease_out(1)%codeparam%output_diag(1) = &
    & ' in write_itm_dummy: This routine should be linked to write_itm.f90 when CHEASE is compiled without libraries for ITM structure'
  eqchease_out(1)%codeparam%output_diag(2) = 'It is not supposed to be called.'
  eqchease_out(1)%codeparam%output_diag(3) = 'NITMOPT should always be -1 in such cases and routines load_itm and write_itm are not called'
  eqchease_out(1)%codeparam%output_flag = -322
  return

end subroutine write_itm
