! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------

  module ids_schemas

  use ids_schemas_amns_data
  use ids_schemas_barometry
  use ids_schemas_b_field_non_axisymmetric
  use ids_schemas_bolometer
  use ids_schemas_bremsstrahlung_visible
  use ids_schemas_calorimetry
  use ids_schemas_camera_ir
  use ids_schemas_camera_visible
  use ids_schemas_camera_x_rays
  use ids_schemas_charge_exchange
  use ids_schemas_coils_non_axisymmetric
  use ids_schemas_controllers
  use ids_schemas_core_instant_changes
  use ids_schemas_core_profiles
  use ids_schemas_core_sources
  use ids_schemas_core_transport
  use ids_schemas_cryostat
  use ids_schemas_dataset_description
  use ids_schemas_dataset_fair
  use ids_schemas_disruption
  use ids_schemas_distribution_sources
  use ids_schemas_distributions
  use ids_schemas_divertors
  use ids_schemas_ec_launchers
  use ids_schemas_ece
  use ids_schemas_edge_profiles
  use ids_schemas_edge_sources
  use ids_schemas_edge_transport
  use ids_schemas_em_coupling
  use ids_schemas_equilibrium
  use ids_schemas_focs
  use ids_schemas_gas_injection
  use ids_schemas_gas_pumping
  use ids_schemas_gyrokinetics
  use ids_schemas_hard_x_rays
  use ids_schemas_ic_antennas
  use ids_schemas_interferometer
  use ids_schemas_iron_core
  use ids_schemas_langmuir_probes
  use ids_schemas_lh_antennas
  use ids_schemas_magnetics
  use ids_schemas_mhd
  use ids_schemas_mhd_linear
  use ids_schemas_mse
  use ids_schemas_nbi
  use ids_schemas_neutron_diagnostic
  use ids_schemas_ntms
  use ids_schemas_pellets
  use ids_schemas_pf_active
  use ids_schemas_pf_passive
  use ids_schemas_plasma_initiation
  use ids_schemas_polarimeter
  use ids_schemas_pulse_schedule
  use ids_schemas_radiation
  use ids_schemas_real_time_data
  use ids_schemas_reflectometer_profile
  use ids_schemas_reflectometer_fluctuation
  use ids_schemas_refractometer
  use ids_schemas_runaway_electrons
  use ids_schemas_sawteeth
  use ids_schemas_soft_x_rays
  use ids_schemas_spectrometer_mass
  use ids_schemas_spectrometer_uv
  use ids_schemas_spectrometer_visible
  use ids_schemas_spectrometer_x_ray_crystal
  use ids_schemas_summary
  use ids_schemas_temporary
  use ids_schemas_thomson_scattering
  use ids_schemas_tf
  use ids_schemas_transport_solver_numerics
  use ids_schemas_turbulence
  use ids_schemas_wall
  use ids_schemas_waves
  use ids_schemas_workflow
  end module
