! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*DECK CRAY05
!*CALL PROCESS
FUNCTION ISRCHFGE(N,PV,NX,TARGET)
  !        ---------------------------------
  !
  !  FIND FIRST ELEMENT IN REAL ARRAY PV WHICH IS GREATER OR EQUAL
  !  THAN TARGET. PV IS INCREMENTED BY NX
  !
  USE globals
  IMPLICIT NONE
  REAL(RKIND)      ::     TARGET
  REAL(RKIND)      ::     PV
  INTEGER          ::     J
  INTEGER          ::     I
  INTEGER          ::     ISRCHFGE
  INTEGER          ::     NX
  INTEGER          ::     N
  DIMENSION &
       &   PV(N*NX)
  !
  I = 1
  IF (NX .LT. 0) STOP
  !
  DO J=1,N
     IF (PV(I) .GE. TARGET) GOTO 2
     I = I + NX
  END DO
2 ISRCHFGE = I
  !
  RETURN
END FUNCTION ISRCHFGE
FUNCTION ISRCHFGT(N,PV,NX,TARGET)
  !        ---------------------------------
  !
  !  FIND FIRST ELEMENT IN REAL ARRAY PV WHICH IS GREATER OR EQUAL
  !  THAN TARGET. PV IS INCREMENTED BY NX
  !
  USE globals
  IMPLICIT NONE
  REAL(RKIND)      ::     TARGET
  REAL(RKIND)      ::     PV
  INTEGER          ::     J
  INTEGER          ::     I
  INTEGER          ::     ISRCHFGT
  INTEGER          ::     NX
  INTEGER          ::     N
  DIMENSION &
       &   PV(N*NX)
  !
  I = 1
  IF (NX .LT. 0) STOP
  !
  DO J=1,N
     IF (PV(I) .GT. TARGET) GOTO 2
     I = I + NX
  END DO
2 ISRCHFGT = I
  !
  RETURN
END FUNCTION ISRCHFGT
