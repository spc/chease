! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*DECK MRD02
!*CALL PROCESS
SUBROUTINE TRIDAGM(A,B,R,DIAG,N,MD,M,EPS,INFO)
  !        #########################################
  !                                        AUTHORS:
  !                                        H. LUTJENS,  CRPP-EPFL
  !                                        A. BONDESON, CRPP-EPFL
  !**********************************************************************
  !                                                                     *
  ! MRD02 PERFORM L*U DECOMPOSITION AND BACK-SUBSTITUTION OF M          *
  !       TRIDIAGONAL SYSTEMS                                           *
  !                                                                     *
  !**********************************************************************
  !
  !         USE globals
  USE prec_const
  IMPLICIT NONE
  INTEGER          ::     INFO, J6
  INTEGER          ::     J10
  INTEGER          ::     J5
  REAL(RKIND)      ::     R
  REAL(RKIND)      ::     B
  REAL(RKIND)      ::     TOP
  INTEGER          ::     J3
  REAL(RKIND)      ::     EPS
  INTEGER          ::     ISAMIN
  INTEGER          ::     IMN
  REAL(RKIND)      ::     A
  REAL(RKIND)      ::     DIAG
  INTEGER          ::     M
  INTEGER          ::     J2
  INTEGER          ::     ITOP
  INTEGER          ::     J4
  INTEGER          ::     N
  INTEGER          ::     MD
  DIMENSION A(MD,N),B(MD,N),R(MD,N),DIAG(MD)
  !
  ! DECOMPOSE AND FORWARD SUBSTITUTION
  !
  INFO = 0
  DO J4=2,N
     !
     ITOP = J4 - 1
     !
     DO J2=1,M
        !
        DIAG(J2) = A(J2,ITOP)
        !
     END DO
     !
     IMN = ISAMIN(M,DIAG,1)
     !
     IF (DIAG(IMN) .LT. EPS) THEN
       WRITE(0,*) ' ZERO PIVOT I = ',J4,', M = ',IMN
       INFO = -291
       return
     ENDIF
     !
     DO J3=1,M
        !
        TOP  = B(J3,ITOP)
        !
        B(J3,ITOP) = B(J3,ITOP) / DIAG(J3)
        R(J3,ITOP) = R(J3,ITOP) / DIAG(J3)
        !
        A(J3,J4) = A(J3,J4) - TOP * B(J3,ITOP)
        R(J3,J4) = R(J3,J4) - TOP * R(J3,ITOP)
        !
     END DO
  END DO
  !
  ! CHECK LAST PIVOT
  !
  IMN = ISAMIN(M,A(1,N),1)
  !
  IF (A(IMN,N) .LT. EPS) THEN
    WRITE(0,*) ' ZERO PIVOT I = ',N,', M = ',IMN
    INFO = -292
    return
  ENDIF
  !
  ! BACKSUBSTITUTION
  !
  DO J5=1,M
     !
     R(J5,N) = R(J5,N) / A(J5,N)
     !
  END DO
  !
  DO J10=N-1,1,-1
     DO J6=1,M
        !
        R(J6,J10) = R(J6,J10) - B(J6,J10) * R(J6,J10+1)
        !
     END DO
  END DO
  !
  RETURN
END SUBROUTINE TRIDAGM
