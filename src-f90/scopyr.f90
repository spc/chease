! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*DECK U49
!*CALL PROCESS
SUBROUTINE SCOPYR(RF, N, X, NX, Y, NY)
  !        --------------------------------------
  !                                        AUTHORS:
  !                                        H. LUTJENS,  CRPP-EPFL
  !
  ! Y = Y + RF * (X - Y)
  ! X IS INCREMENTED BY NX
  ! Y IS INCREMENTED BY NY
  !
  !
  USE prec_const
  IMPLICIT NONE
  INTEGER          ::     J
  INTEGER          ::     NM1
  REAL(RKIND)      ::     X
  REAL(RKIND)      ::     RF
  REAL(RKIND)      ::     Y
  INTEGER          ::     NY
  INTEGER          ::     NX
  INTEGER          ::     N
  DIMENSION &
       &   X(N*NX), Y(N*NY)
  !
  IF (N .LE. 0) RETURN
  !
  Y(1) = Y(1) + RF*(X(1)-Y(1))
  !
  IF (N .EQ. 1) RETURN
  !
  IF (NX.LT.0 .OR. NY.LT.0) THEN
     PRINT*,'NEGATIVE INCREMENTS FORBIDDEN'
     STOP
  ENDIF
  !
  NM1 = N - 1
  !
  DO J=1,NM1
     Y(J*NY+1) = Y(J*NY+1) + RF*(X(J*NX+1)-Y(J*NY+1))
  END DO
  !
  RETURN
END SUBROUTINE SCOPYR
