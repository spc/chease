! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!
SUBROUTINE NTRIDG(A,KDIMA,ND1,ND2,N)
  !        #########################
  !                                        AUTHORS:
  !                                        H. LUTJENS,  CRPP-EPFL
  !                                        A. BONDESON, CRPP-EPFL
  !**************************************************************
  ! PERFORM L*U DECOMPOSITION FOR ND2 - ND1 TRIDIAGONAL SYMETRIC
  ! REAL MATRIXES IN PARALLEL.
  !***************************************************************
  !
  USE globals, except_A => A
  IMPLICIT NONE
  !
  INTEGER          ::     KDIMA, N, ND1, ND2
  REAL(RKIND)      ::     A(KDIMA,2,N)
  !
  INTEGER          ::     ITOP, J2, J3, J4
  REAL(RKIND)      ::     TOP
  REAL(RKIND)      ::     DIAG(ND2)
  !
  DO J4=2,N
    ITOP = J4 - 1
    !
    DO J2=ND1,ND2
      DIAG(J2) = A(J2,1,ITOP)
    END DO
    !
    DO J3=ND1,ND2
      TOP    = A(J3,2,ITOP)
      A(J3,2,ITOP) = A(J3,2,ITOP) / DIAG(J3)
      A(J3,1,J4)   = A(J3,1,J4) - TOP * A(J3,2,ITOP)
    END DO
  END DO
  !
  RETURN
END SUBROUTINE NTRIDG
