! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*DECK NUMREC01
!*CALL PROCESS
SUBROUTINE SORT3(N,RA,RB,RC,WKSP,IWKSP)
  !       ---------------------------------------
  !
  !       NUMERICAL RECIPES SORTING ROUTINE.
  !
  !         USE globals
  USE prec_const
  IMPLICIT NONE
  REAL(RKIND)      ::     RC
  REAL(RKIND)      ::     RB
  REAL(RKIND)      ::     WKSP
  INTEGER          ::     J
  INTEGER          ::     IWKSP
  REAL(RKIND)      ::     RA
  INTEGER          ::     N
  DIMENSION RA(N),RB(N),RC(N),WKSP(N),IWKSP(N)
  CALL INDEXX(N,RA,IWKSP)
  DO J=1,N
     WKSP(J)=RA(J)
  END DO
  DO J=1,N
     RA(J)=WKSP(IWKSP(J))
  END DO
  DO J=1,N
     WKSP(J)=RB(J)
  END DO
  DO J=1,N
     RB(J)=WKSP(IWKSP(J))
  END DO
  DO J=1,N
     WKSP(J)=RC(J)
  END DO
  DO J=1,N
     RC(J)=WKSP(IWKSP(J))
  END DO
  RETURN
END SUBROUTINE SORT3
