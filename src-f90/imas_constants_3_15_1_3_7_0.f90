! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------

!> Module implementing the IMAS physics constants
!>
!> Source:
!>  based on SOLPS b2mod_constants.F
!>    09/12/2007 xpb : source CODATA 2006 (http://www.nist.gov/)
!>    08/19/2011 xpb : source CODATA 2010 (http://www.nist.gov/)
!>  pulled from ets r100
!>
!> \author David Coster
!>

module imas_imas_constants

  use iso_c_binding

  implicit none
  private
  real(kind=c_double), parameter :: IMAS_PI = 3.141592653589793238462643383280_c_double
  real(kind=c_double), parameter :: IMAS_C = 2.99792458e8_c_double  !< speed of light [m/s]
  real(kind=c_double), parameter :: IMAS_AMU = 1.660538921e-27_c_double  !< atomic mass unit [kg]
  real(kind=c_double), parameter :: IMAS_EV = 1.602176565e-19_c_double  !< electron volt (eV)
  real(kind=c_double), parameter :: IMAS_MU0 = 4.0e-7_c_double * IMAS_PI   !< vacuum permeability

  type :: type_imas_constants
    real(c_double) :: IMAS_PI
    real(c_double) :: IMAS_C
    real(c_double) :: IMAS_ME
    real(c_double) :: IMAS_MP
    real(c_double) :: IMAS_MN
    real(c_double) :: IMAS_MD
    real(c_double) :: IMAS_MT
    real(c_double) :: IMAS_MA
    real(c_double) :: IMAS_AMU
    real(c_double) :: IMAS_EV
    real(c_double) :: IMAS_QE
    real(c_double) :: IMAS_MU0
    real(c_double) :: IMAS_EPS0
    real(c_double) :: IMAS_AVOGR
    real(c_double) :: IMAS_KBOLT
    real(c_double) :: IMAS_MASS_H_1
    real(c_double) :: IMAS_MASS_H_2
    real(c_double) :: IMAS_MASS_H_3
    real(c_double) :: IMAS_MASS_H_4
    real(c_double) :: IMAS_MASS_H_5
    real(c_double) :: IMAS_MASS_H_6
    real(c_double) :: IMAS_MASS_H_7
    real(c_double) :: IMAS_MASS_He_3
    real(c_double) :: IMAS_MASS_He_4
    real(c_double) :: IMAS_MASS_He_5
    real(c_double) :: IMAS_MASS_He_6
    real(c_double) :: IMAS_MASS_He_7
    real(c_double) :: IMAS_MASS_He_8
    real(c_double) :: IMAS_MASS_He_9
    real(c_double) :: IMAS_MASS_He_10
    real(c_double) :: IMAS_MASS_Li_3
    real(c_double) :: IMAS_MASS_Li_4
    real(c_double) :: IMAS_MASS_Li_5
    real(c_double) :: IMAS_MASS_Li_6
    real(c_double) :: IMAS_MASS_Li_7
    real(c_double) :: IMAS_MASS_Li_8
    real(c_double) :: IMAS_MASS_Li_9
    real(c_double) :: IMAS_MASS_Li_10
    real(c_double) :: IMAS_MASS_Li_11
    real(c_double) :: IMAS_MASS_Li_12
    real(c_double) :: IMAS_MASS_Be_5
    real(c_double) :: IMAS_MASS_Be_6
    real(c_double) :: IMAS_MASS_Be_7
    real(c_double) :: IMAS_MASS_Be_8
    real(c_double) :: IMAS_MASS_Be_9
    real(c_double) :: IMAS_MASS_Be_10
    real(c_double) :: IMAS_MASS_Be_11
    real(c_double) :: IMAS_MASS_Be_12
    real(c_double) :: IMAS_MASS_Be_13
    real(c_double) :: IMAS_MASS_Be_14
    real(c_double) :: IMAS_MASS_Be_15
    real(c_double) :: IMAS_MASS_Be_16
    real(c_double) :: IMAS_MASS_B_6
    real(c_double) :: IMAS_MASS_B_7
    real(c_double) :: IMAS_MASS_B_8
    real(c_double) :: IMAS_MASS_B_9
    real(c_double) :: IMAS_MASS_B_10
    real(c_double) :: IMAS_MASS_B_11
    real(c_double) :: IMAS_MASS_B_12
    real(c_double) :: IMAS_MASS_B_13
    real(c_double) :: IMAS_MASS_B_14
    real(c_double) :: IMAS_MASS_B_15
    real(c_double) :: IMAS_MASS_B_16
    real(c_double) :: IMAS_MASS_B_17
    real(c_double) :: IMAS_MASS_B_18
    real(c_double) :: IMAS_MASS_B_19
    real(c_double) :: IMAS_MASS_C_8
    real(c_double) :: IMAS_MASS_C_9
    real(c_double) :: IMAS_MASS_C_10
    real(c_double) :: IMAS_MASS_C_11
    real(c_double) :: IMAS_MASS_C_12
    real(c_double) :: IMAS_MASS_C_13
    real(c_double) :: IMAS_MASS_C_14
    real(c_double) :: IMAS_MASS_C_15
    real(c_double) :: IMAS_MASS_C_16
    real(c_double) :: IMAS_MASS_C_17
    real(c_double) :: IMAS_MASS_C_18
    real(c_double) :: IMAS_MASS_C_19
    real(c_double) :: IMAS_MASS_C_20
    real(c_double) :: IMAS_MASS_C_21
    real(c_double) :: IMAS_MASS_C_22
    real(c_double) :: IMAS_MASS_N_10
    real(c_double) :: IMAS_MASS_N_11
    real(c_double) :: IMAS_MASS_N_12
    real(c_double) :: IMAS_MASS_N_13
    real(c_double) :: IMAS_MASS_N_14
    real(c_double) :: IMAS_MASS_N_15
    real(c_double) :: IMAS_MASS_N_16
    real(c_double) :: IMAS_MASS_N_17
    real(c_double) :: IMAS_MASS_N_18
    real(c_double) :: IMAS_MASS_N_19
    real(c_double) :: IMAS_MASS_N_20
    real(c_double) :: IMAS_MASS_N_21
    real(c_double) :: IMAS_MASS_N_22
    real(c_double) :: IMAS_MASS_N_23
    real(c_double) :: IMAS_MASS_N_24
    real(c_double) :: IMAS_MASS_N_25
    real(c_double) :: IMAS_MASS_O_12
    real(c_double) :: IMAS_MASS_O_13
    real(c_double) :: IMAS_MASS_O_14
    real(c_double) :: IMAS_MASS_O_15
    real(c_double) :: IMAS_MASS_O_16
    real(c_double) :: IMAS_MASS_O_17
    real(c_double) :: IMAS_MASS_O_18
    real(c_double) :: IMAS_MASS_O_19
    real(c_double) :: IMAS_MASS_O_20
    real(c_double) :: IMAS_MASS_O_21
    real(c_double) :: IMAS_MASS_O_22
    real(c_double) :: IMAS_MASS_O_23
    real(c_double) :: IMAS_MASS_O_24
    real(c_double) :: IMAS_MASS_O_25
    real(c_double) :: IMAS_MASS_O_26
    real(c_double) :: IMAS_MASS_O_27
    real(c_double) :: IMAS_MASS_O_28
    real(c_double) :: IMAS_MASS_F_14
    real(c_double) :: IMAS_MASS_F_15
    real(c_double) :: IMAS_MASS_F_16
    real(c_double) :: IMAS_MASS_F_17
    real(c_double) :: IMAS_MASS_F_18
    real(c_double) :: IMAS_MASS_F_19
    real(c_double) :: IMAS_MASS_F_20
    real(c_double) :: IMAS_MASS_F_21
    real(c_double) :: IMAS_MASS_F_22
    real(c_double) :: IMAS_MASS_F_23
    real(c_double) :: IMAS_MASS_F_24
    real(c_double) :: IMAS_MASS_F_25
    real(c_double) :: IMAS_MASS_F_26
    real(c_double) :: IMAS_MASS_F_27
    real(c_double) :: IMAS_MASS_F_28
    real(c_double) :: IMAS_MASS_F_29
    real(c_double) :: IMAS_MASS_F_30
    real(c_double) :: IMAS_MASS_F_31
    real(c_double) :: IMAS_MASS_Ne_16
    real(c_double) :: IMAS_MASS_Ne_17
    real(c_double) :: IMAS_MASS_Ne_18
    real(c_double) :: IMAS_MASS_Ne_19
    real(c_double) :: IMAS_MASS_Ne_20
    real(c_double) :: IMAS_MASS_Ne_21
    real(c_double) :: IMAS_MASS_Ne_22
    real(c_double) :: IMAS_MASS_Ne_23
    real(c_double) :: IMAS_MASS_Ne_24
    real(c_double) :: IMAS_MASS_Ne_25
    real(c_double) :: IMAS_MASS_Ne_26
    real(c_double) :: IMAS_MASS_Ne_27
    real(c_double) :: IMAS_MASS_Ne_28
    real(c_double) :: IMAS_MASS_Ne_29
    real(c_double) :: IMAS_MASS_Ne_30
    real(c_double) :: IMAS_MASS_Ne_31
    real(c_double) :: IMAS_MASS_Ne_32
    real(c_double) :: IMAS_MASS_Ne_33
    real(c_double) :: IMAS_MASS_Ne_34
    real(c_double) :: IMAS_MASS_Na_18
    real(c_double) :: IMAS_MASS_Na_19
    real(c_double) :: IMAS_MASS_Na_20
    real(c_double) :: IMAS_MASS_Na_21
    real(c_double) :: IMAS_MASS_Na_22
    real(c_double) :: IMAS_MASS_Na_23
    real(c_double) :: IMAS_MASS_Na_24
    real(c_double) :: IMAS_MASS_Na_25
    real(c_double) :: IMAS_MASS_Na_26
    real(c_double) :: IMAS_MASS_Na_27
    real(c_double) :: IMAS_MASS_Na_28
    real(c_double) :: IMAS_MASS_Na_29
    real(c_double) :: IMAS_MASS_Na_30
    real(c_double) :: IMAS_MASS_Na_31
    real(c_double) :: IMAS_MASS_Na_32
    real(c_double) :: IMAS_MASS_Na_33
    real(c_double) :: IMAS_MASS_Na_34
    real(c_double) :: IMAS_MASS_Na_35
    real(c_double) :: IMAS_MASS_Na_36
    real(c_double) :: IMAS_MASS_Na_37
    real(c_double) :: IMAS_MASS_Mg_19
    real(c_double) :: IMAS_MASS_Mg_20
    real(c_double) :: IMAS_MASS_Mg_21
    real(c_double) :: IMAS_MASS_Mg_22
    real(c_double) :: IMAS_MASS_Mg_23
    real(c_double) :: IMAS_MASS_Mg_24
    real(c_double) :: IMAS_MASS_Mg_25
    real(c_double) :: IMAS_MASS_Mg_26
    real(c_double) :: IMAS_MASS_Mg_27
    real(c_double) :: IMAS_MASS_Mg_28
    real(c_double) :: IMAS_MASS_Mg_29
    real(c_double) :: IMAS_MASS_Mg_30
    real(c_double) :: IMAS_MASS_Mg_31
    real(c_double) :: IMAS_MASS_Mg_32
    real(c_double) :: IMAS_MASS_Mg_33
    real(c_double) :: IMAS_MASS_Mg_34
    real(c_double) :: IMAS_MASS_Mg_35
    real(c_double) :: IMAS_MASS_Mg_36
    real(c_double) :: IMAS_MASS_Mg_37
    real(c_double) :: IMAS_MASS_Mg_38
    real(c_double) :: IMAS_MASS_Mg_39
    real(c_double) :: IMAS_MASS_Mg_40
    real(c_double) :: IMAS_MASS_Al_21
    real(c_double) :: IMAS_MASS_Al_22
    real(c_double) :: IMAS_MASS_Al_23
    real(c_double) :: IMAS_MASS_Al_24
    real(c_double) :: IMAS_MASS_Al_25
    real(c_double) :: IMAS_MASS_Al_26
    real(c_double) :: IMAS_MASS_Al_27
    real(c_double) :: IMAS_MASS_Al_28
    real(c_double) :: IMAS_MASS_Al_29
    real(c_double) :: IMAS_MASS_Al_30
    real(c_double) :: IMAS_MASS_Al_31
    real(c_double) :: IMAS_MASS_Al_32
    real(c_double) :: IMAS_MASS_Al_33
    real(c_double) :: IMAS_MASS_Al_34
    real(c_double) :: IMAS_MASS_Al_35
    real(c_double) :: IMAS_MASS_Al_36
    real(c_double) :: IMAS_MASS_Al_37
    real(c_double) :: IMAS_MASS_Al_38
    real(c_double) :: IMAS_MASS_Al_39
    real(c_double) :: IMAS_MASS_Al_40
    real(c_double) :: IMAS_MASS_Al_41
    real(c_double) :: IMAS_MASS_Al_42
    real(c_double) :: IMAS_MASS_Si_22
    real(c_double) :: IMAS_MASS_Si_23
    real(c_double) :: IMAS_MASS_Si_24
    real(c_double) :: IMAS_MASS_Si_25
    real(c_double) :: IMAS_MASS_Si_26
    real(c_double) :: IMAS_MASS_Si_27
    real(c_double) :: IMAS_MASS_Si_28
    real(c_double) :: IMAS_MASS_Si_29
    real(c_double) :: IMAS_MASS_Si_30
    real(c_double) :: IMAS_MASS_Si_31
    real(c_double) :: IMAS_MASS_Si_32
    real(c_double) :: IMAS_MASS_Si_33
    real(c_double) :: IMAS_MASS_Si_34
    real(c_double) :: IMAS_MASS_Si_35
    real(c_double) :: IMAS_MASS_Si_36
    real(c_double) :: IMAS_MASS_Si_37
    real(c_double) :: IMAS_MASS_Si_38
    real(c_double) :: IMAS_MASS_Si_39
    real(c_double) :: IMAS_MASS_Si_40
    real(c_double) :: IMAS_MASS_Si_41
    real(c_double) :: IMAS_MASS_Si_42
    real(c_double) :: IMAS_MASS_Si_43
    real(c_double) :: IMAS_MASS_Si_44
    real(c_double) :: IMAS_MASS_P_24
    real(c_double) :: IMAS_MASS_P_25
    real(c_double) :: IMAS_MASS_P_26
    real(c_double) :: IMAS_MASS_P_27
    real(c_double) :: IMAS_MASS_P_28
    real(c_double) :: IMAS_MASS_P_29
    real(c_double) :: IMAS_MASS_P_30
    real(c_double) :: IMAS_MASS_P_31
    real(c_double) :: IMAS_MASS_P_32
    real(c_double) :: IMAS_MASS_P_33
    real(c_double) :: IMAS_MASS_P_34
    real(c_double) :: IMAS_MASS_P_35
    real(c_double) :: IMAS_MASS_P_36
    real(c_double) :: IMAS_MASS_P_37
    real(c_double) :: IMAS_MASS_P_38
    real(c_double) :: IMAS_MASS_P_39
    real(c_double) :: IMAS_MASS_P_40
    real(c_double) :: IMAS_MASS_P_41
    real(c_double) :: IMAS_MASS_P_42
    real(c_double) :: IMAS_MASS_P_43
    real(c_double) :: IMAS_MASS_P_44
    real(c_double) :: IMAS_MASS_P_45
    real(c_double) :: IMAS_MASS_P_46
    real(c_double) :: IMAS_MASS_S_26
    real(c_double) :: IMAS_MASS_S_27
    real(c_double) :: IMAS_MASS_S_28
    real(c_double) :: IMAS_MASS_S_29
    real(c_double) :: IMAS_MASS_S_30
    real(c_double) :: IMAS_MASS_S_31
    real(c_double) :: IMAS_MASS_S_32
    real(c_double) :: IMAS_MASS_S_33
    real(c_double) :: IMAS_MASS_S_34
    real(c_double) :: IMAS_MASS_S_35
    real(c_double) :: IMAS_MASS_S_36
    real(c_double) :: IMAS_MASS_S_37
    real(c_double) :: IMAS_MASS_S_38
    real(c_double) :: IMAS_MASS_S_39
    real(c_double) :: IMAS_MASS_S_40
    real(c_double) :: IMAS_MASS_S_41
    real(c_double) :: IMAS_MASS_S_42
    real(c_double) :: IMAS_MASS_S_43
    real(c_double) :: IMAS_MASS_S_44
    real(c_double) :: IMAS_MASS_S_45
    real(c_double) :: IMAS_MASS_S_46
    real(c_double) :: IMAS_MASS_S_47
    real(c_double) :: IMAS_MASS_S_48
    real(c_double) :: IMAS_MASS_S_49
    real(c_double) :: IMAS_MASS_Cl_28
    real(c_double) :: IMAS_MASS_Cl_29
    real(c_double) :: IMAS_MASS_Cl_30
    real(c_double) :: IMAS_MASS_Cl_31
    real(c_double) :: IMAS_MASS_Cl_32
    real(c_double) :: IMAS_MASS_Cl_33
    real(c_double) :: IMAS_MASS_Cl_34
    real(c_double) :: IMAS_MASS_Cl_35
    real(c_double) :: IMAS_MASS_Cl_36
    real(c_double) :: IMAS_MASS_Cl_37
    real(c_double) :: IMAS_MASS_Cl_38
    real(c_double) :: IMAS_MASS_Cl_39
    real(c_double) :: IMAS_MASS_Cl_40
    real(c_double) :: IMAS_MASS_Cl_41
    real(c_double) :: IMAS_MASS_Cl_42
    real(c_double) :: IMAS_MASS_Cl_43
    real(c_double) :: IMAS_MASS_Cl_44
    real(c_double) :: IMAS_MASS_Cl_45
    real(c_double) :: IMAS_MASS_Cl_46
    real(c_double) :: IMAS_MASS_Cl_47
    real(c_double) :: IMAS_MASS_Cl_48
    real(c_double) :: IMAS_MASS_Cl_49
    real(c_double) :: IMAS_MASS_Cl_50
    real(c_double) :: IMAS_MASS_Cl_51
    real(c_double) :: IMAS_MASS_Ar_30
    real(c_double) :: IMAS_MASS_Ar_31
    real(c_double) :: IMAS_MASS_Ar_32
    real(c_double) :: IMAS_MASS_Ar_33
    real(c_double) :: IMAS_MASS_Ar_34
    real(c_double) :: IMAS_MASS_Ar_35
    real(c_double) :: IMAS_MASS_Ar_36
    real(c_double) :: IMAS_MASS_Ar_37
    real(c_double) :: IMAS_MASS_Ar_38
    real(c_double) :: IMAS_MASS_Ar_39
    real(c_double) :: IMAS_MASS_Ar_40
    real(c_double) :: IMAS_MASS_Ar_41
    real(c_double) :: IMAS_MASS_Ar_42
    real(c_double) :: IMAS_MASS_Ar_43
    real(c_double) :: IMAS_MASS_Ar_44
    real(c_double) :: IMAS_MASS_Ar_45
    real(c_double) :: IMAS_MASS_Ar_46
    real(c_double) :: IMAS_MASS_Ar_47
    real(c_double) :: IMAS_MASS_Ar_48
    real(c_double) :: IMAS_MASS_Ar_49
    real(c_double) :: IMAS_MASS_Ar_50
    real(c_double) :: IMAS_MASS_Ar_51
    real(c_double) :: IMAS_MASS_Ar_52
    real(c_double) :: IMAS_MASS_Ar_53
    real(c_double) :: IMAS_MASS_K_32
    real(c_double) :: IMAS_MASS_K_33
    real(c_double) :: IMAS_MASS_K_34
    real(c_double) :: IMAS_MASS_K_35
    real(c_double) :: IMAS_MASS_K_36
    real(c_double) :: IMAS_MASS_K_37
    real(c_double) :: IMAS_MASS_K_38
    real(c_double) :: IMAS_MASS_K_39
    real(c_double) :: IMAS_MASS_K_40
    real(c_double) :: IMAS_MASS_K_41
    real(c_double) :: IMAS_MASS_K_42
    real(c_double) :: IMAS_MASS_K_43
    real(c_double) :: IMAS_MASS_K_44
    real(c_double) :: IMAS_MASS_K_45
    real(c_double) :: IMAS_MASS_K_46
    real(c_double) :: IMAS_MASS_K_47
    real(c_double) :: IMAS_MASS_K_48
    real(c_double) :: IMAS_MASS_K_49
    real(c_double) :: IMAS_MASS_K_50
    real(c_double) :: IMAS_MASS_K_51
    real(c_double) :: IMAS_MASS_K_52
    real(c_double) :: IMAS_MASS_K_53
    real(c_double) :: IMAS_MASS_K_54
    real(c_double) :: IMAS_MASS_K_55
    real(c_double) :: IMAS_MASS_Ca_34
    real(c_double) :: IMAS_MASS_Ca_35
    real(c_double) :: IMAS_MASS_Ca_36
    real(c_double) :: IMAS_MASS_Ca_37
    real(c_double) :: IMAS_MASS_Ca_38
    real(c_double) :: IMAS_MASS_Ca_39
    real(c_double) :: IMAS_MASS_Ca_40
    real(c_double) :: IMAS_MASS_Ca_41
    real(c_double) :: IMAS_MASS_Ca_42
    real(c_double) :: IMAS_MASS_Ca_43
    real(c_double) :: IMAS_MASS_Ca_44
    real(c_double) :: IMAS_MASS_Ca_45
    real(c_double) :: IMAS_MASS_Ca_46
    real(c_double) :: IMAS_MASS_Ca_47
    real(c_double) :: IMAS_MASS_Ca_48
    real(c_double) :: IMAS_MASS_Ca_49
    real(c_double) :: IMAS_MASS_Ca_50
    real(c_double) :: IMAS_MASS_Ca_51
    real(c_double) :: IMAS_MASS_Ca_52
    real(c_double) :: IMAS_MASS_Ca_53
    real(c_double) :: IMAS_MASS_Ca_54
    real(c_double) :: IMAS_MASS_Ca_55
    real(c_double) :: IMAS_MASS_Ca_56
    real(c_double) :: IMAS_MASS_Ca_57
    real(c_double) :: IMAS_MASS_Sc_36
    real(c_double) :: IMAS_MASS_Sc_37
    real(c_double) :: IMAS_MASS_Sc_38
    real(c_double) :: IMAS_MASS_Sc_39
    real(c_double) :: IMAS_MASS_Sc_40
    real(c_double) :: IMAS_MASS_Sc_41
    real(c_double) :: IMAS_MASS_Sc_42
    real(c_double) :: IMAS_MASS_Sc_43
    real(c_double) :: IMAS_MASS_Sc_44
    real(c_double) :: IMAS_MASS_Sc_45
    real(c_double) :: IMAS_MASS_Sc_46
    real(c_double) :: IMAS_MASS_Sc_47
    real(c_double) :: IMAS_MASS_Sc_48
    real(c_double) :: IMAS_MASS_Sc_49
    real(c_double) :: IMAS_MASS_Sc_50
    real(c_double) :: IMAS_MASS_Sc_51
    real(c_double) :: IMAS_MASS_Sc_52
    real(c_double) :: IMAS_MASS_Sc_53
    real(c_double) :: IMAS_MASS_Sc_54
    real(c_double) :: IMAS_MASS_Sc_55
    real(c_double) :: IMAS_MASS_Sc_56
    real(c_double) :: IMAS_MASS_Sc_57
    real(c_double) :: IMAS_MASS_Sc_58
    real(c_double) :: IMAS_MASS_Sc_59
    real(c_double) :: IMAS_MASS_Sc_60
    real(c_double) :: IMAS_MASS_Ti_38
    real(c_double) :: IMAS_MASS_Ti_39
    real(c_double) :: IMAS_MASS_Ti_40
    real(c_double) :: IMAS_MASS_Ti_41
    real(c_double) :: IMAS_MASS_Ti_42
    real(c_double) :: IMAS_MASS_Ti_43
    real(c_double) :: IMAS_MASS_Ti_44
    real(c_double) :: IMAS_MASS_Ti_45
    real(c_double) :: IMAS_MASS_Ti_46
    real(c_double) :: IMAS_MASS_Ti_47
    real(c_double) :: IMAS_MASS_Ti_48
    real(c_double) :: IMAS_MASS_Ti_49
    real(c_double) :: IMAS_MASS_Ti_50
    real(c_double) :: IMAS_MASS_Ti_51
    real(c_double) :: IMAS_MASS_Ti_52
    real(c_double) :: IMAS_MASS_Ti_53
    real(c_double) :: IMAS_MASS_Ti_54
    real(c_double) :: IMAS_MASS_Ti_55
    real(c_double) :: IMAS_MASS_Ti_56
    real(c_double) :: IMAS_MASS_Ti_57
    real(c_double) :: IMAS_MASS_Ti_58
    real(c_double) :: IMAS_MASS_Ti_59
    real(c_double) :: IMAS_MASS_Ti_60
    real(c_double) :: IMAS_MASS_Ti_61
    real(c_double) :: IMAS_MASS_Ti_62
    real(c_double) :: IMAS_MASS_Ti_63
    real(c_double) :: IMAS_MASS_V_40
    real(c_double) :: IMAS_MASS_V_41
    real(c_double) :: IMAS_MASS_V_42
    real(c_double) :: IMAS_MASS_V_43
    real(c_double) :: IMAS_MASS_V_44
    real(c_double) :: IMAS_MASS_V_45
    real(c_double) :: IMAS_MASS_V_46
    real(c_double) :: IMAS_MASS_V_47
    real(c_double) :: IMAS_MASS_V_48
    real(c_double) :: IMAS_MASS_V_49
    real(c_double) :: IMAS_MASS_V_50
    real(c_double) :: IMAS_MASS_V_51
    real(c_double) :: IMAS_MASS_V_52
    real(c_double) :: IMAS_MASS_V_53
    real(c_double) :: IMAS_MASS_V_54
    real(c_double) :: IMAS_MASS_V_55
    real(c_double) :: IMAS_MASS_V_56
    real(c_double) :: IMAS_MASS_V_57
    real(c_double) :: IMAS_MASS_V_58
    real(c_double) :: IMAS_MASS_V_59
    real(c_double) :: IMAS_MASS_V_60
    real(c_double) :: IMAS_MASS_V_61
    real(c_double) :: IMAS_MASS_V_62
    real(c_double) :: IMAS_MASS_V_63
    real(c_double) :: IMAS_MASS_V_64
    real(c_double) :: IMAS_MASS_V_65
    real(c_double) :: IMAS_MASS_Cr_42
    real(c_double) :: IMAS_MASS_Cr_43
    real(c_double) :: IMAS_MASS_Cr_44
    real(c_double) :: IMAS_MASS_Cr_45
    real(c_double) :: IMAS_MASS_Cr_46
    real(c_double) :: IMAS_MASS_Cr_47
    real(c_double) :: IMAS_MASS_Cr_48
    real(c_double) :: IMAS_MASS_Cr_49
    real(c_double) :: IMAS_MASS_Cr_50
    real(c_double) :: IMAS_MASS_Cr_51
    real(c_double) :: IMAS_MASS_Cr_52
    real(c_double) :: IMAS_MASS_Cr_53
    real(c_double) :: IMAS_MASS_Cr_54
    real(c_double) :: IMAS_MASS_Cr_55
    real(c_double) :: IMAS_MASS_Cr_56
    real(c_double) :: IMAS_MASS_Cr_57
    real(c_double) :: IMAS_MASS_Cr_58
    real(c_double) :: IMAS_MASS_Cr_59
    real(c_double) :: IMAS_MASS_Cr_60
    real(c_double) :: IMAS_MASS_Cr_61
    real(c_double) :: IMAS_MASS_Cr_62
    real(c_double) :: IMAS_MASS_Cr_63
    real(c_double) :: IMAS_MASS_Cr_64
    real(c_double) :: IMAS_MASS_Cr_65
    real(c_double) :: IMAS_MASS_Cr_66
    real(c_double) :: IMAS_MASS_Cr_67
    real(c_double) :: IMAS_MASS_Mn_44
    real(c_double) :: IMAS_MASS_Mn_45
    real(c_double) :: IMAS_MASS_Mn_46
    real(c_double) :: IMAS_MASS_Mn_47
    real(c_double) :: IMAS_MASS_Mn_48
    real(c_double) :: IMAS_MASS_Mn_49
    real(c_double) :: IMAS_MASS_Mn_50
    real(c_double) :: IMAS_MASS_Mn_51
    real(c_double) :: IMAS_MASS_Mn_52
    real(c_double) :: IMAS_MASS_Mn_53
    real(c_double) :: IMAS_MASS_Mn_54
    real(c_double) :: IMAS_MASS_Mn_55
    real(c_double) :: IMAS_MASS_Mn_56
    real(c_double) :: IMAS_MASS_Mn_57
    real(c_double) :: IMAS_MASS_Mn_58
    real(c_double) :: IMAS_MASS_Mn_59
    real(c_double) :: IMAS_MASS_Mn_60
    real(c_double) :: IMAS_MASS_Mn_61
    real(c_double) :: IMAS_MASS_Mn_62
    real(c_double) :: IMAS_MASS_Mn_63
    real(c_double) :: IMAS_MASS_Mn_64
    real(c_double) :: IMAS_MASS_Mn_65
    real(c_double) :: IMAS_MASS_Mn_66
    real(c_double) :: IMAS_MASS_Mn_67
    real(c_double) :: IMAS_MASS_Mn_68
    real(c_double) :: IMAS_MASS_Mn_69
    real(c_double) :: IMAS_MASS_Fe_45
    real(c_double) :: IMAS_MASS_Fe_46
    real(c_double) :: IMAS_MASS_Fe_47
    real(c_double) :: IMAS_MASS_Fe_48
    real(c_double) :: IMAS_MASS_Fe_49
    real(c_double) :: IMAS_MASS_Fe_50
    real(c_double) :: IMAS_MASS_Fe_51
    real(c_double) :: IMAS_MASS_Fe_52
    real(c_double) :: IMAS_MASS_Fe_53
    real(c_double) :: IMAS_MASS_Fe_54
    real(c_double) :: IMAS_MASS_Fe_55
    real(c_double) :: IMAS_MASS_Fe_56
    real(c_double) :: IMAS_MASS_Fe_57
    real(c_double) :: IMAS_MASS_Fe_58
    real(c_double) :: IMAS_MASS_Fe_59
    real(c_double) :: IMAS_MASS_Fe_60
    real(c_double) :: IMAS_MASS_Fe_61
    real(c_double) :: IMAS_MASS_Fe_62
    real(c_double) :: IMAS_MASS_Fe_63
    real(c_double) :: IMAS_MASS_Fe_64
    real(c_double) :: IMAS_MASS_Fe_65
    real(c_double) :: IMAS_MASS_Fe_66
    real(c_double) :: IMAS_MASS_Fe_67
    real(c_double) :: IMAS_MASS_Fe_68
    real(c_double) :: IMAS_MASS_Fe_69
    real(c_double) :: IMAS_MASS_Fe_70
    real(c_double) :: IMAS_MASS_Fe_71
    real(c_double) :: IMAS_MASS_Fe_72
    real(c_double) :: IMAS_MASS_Co_47
    real(c_double) :: IMAS_MASS_Co_48
    real(c_double) :: IMAS_MASS_Co_49
    real(c_double) :: IMAS_MASS_Co_50
    real(c_double) :: IMAS_MASS_Co_51
    real(c_double) :: IMAS_MASS_Co_52
    real(c_double) :: IMAS_MASS_Co_53
    real(c_double) :: IMAS_MASS_Co_54
    real(c_double) :: IMAS_MASS_Co_55
    real(c_double) :: IMAS_MASS_Co_56
    real(c_double) :: IMAS_MASS_Co_57
    real(c_double) :: IMAS_MASS_Co_58
    real(c_double) :: IMAS_MASS_Co_59
    real(c_double) :: IMAS_MASS_Co_60
    real(c_double) :: IMAS_MASS_Co_61
    real(c_double) :: IMAS_MASS_Co_62
    real(c_double) :: IMAS_MASS_Co_63
    real(c_double) :: IMAS_MASS_Co_64
    real(c_double) :: IMAS_MASS_Co_65
    real(c_double) :: IMAS_MASS_Co_66
    real(c_double) :: IMAS_MASS_Co_67
    real(c_double) :: IMAS_MASS_Co_68
    real(c_double) :: IMAS_MASS_Co_69
    real(c_double) :: IMAS_MASS_Co_70
    real(c_double) :: IMAS_MASS_Co_71
    real(c_double) :: IMAS_MASS_Co_72
    real(c_double) :: IMAS_MASS_Co_73
    real(c_double) :: IMAS_MASS_Co_74
    real(c_double) :: IMAS_MASS_Co_75
    real(c_double) :: IMAS_MASS_Ni_48
    real(c_double) :: IMAS_MASS_Ni_49
    real(c_double) :: IMAS_MASS_Ni_50
    real(c_double) :: IMAS_MASS_Ni_51
    real(c_double) :: IMAS_MASS_Ni_52
    real(c_double) :: IMAS_MASS_Ni_53
    real(c_double) :: IMAS_MASS_Ni_54
    real(c_double) :: IMAS_MASS_Ni_55
    real(c_double) :: IMAS_MASS_Ni_56
    real(c_double) :: IMAS_MASS_Ni_57
    real(c_double) :: IMAS_MASS_Ni_58
    real(c_double) :: IMAS_MASS_Ni_59
    real(c_double) :: IMAS_MASS_Ni_60
    real(c_double) :: IMAS_MASS_Ni_61
    real(c_double) :: IMAS_MASS_Ni_62
    real(c_double) :: IMAS_MASS_Ni_63
    real(c_double) :: IMAS_MASS_Ni_64
    real(c_double) :: IMAS_MASS_Ni_65
    real(c_double) :: IMAS_MASS_Ni_66
    real(c_double) :: IMAS_MASS_Ni_67
    real(c_double) :: IMAS_MASS_Ni_68
    real(c_double) :: IMAS_MASS_Ni_69
    real(c_double) :: IMAS_MASS_Ni_70
    real(c_double) :: IMAS_MASS_Ni_71
    real(c_double) :: IMAS_MASS_Ni_72
    real(c_double) :: IMAS_MASS_Ni_73
    real(c_double) :: IMAS_MASS_Ni_74
    real(c_double) :: IMAS_MASS_Ni_75
    real(c_double) :: IMAS_MASS_Ni_76
    real(c_double) :: IMAS_MASS_Ni_77
    real(c_double) :: IMAS_MASS_Ni_78
    real(c_double) :: IMAS_MASS_Cu_52
    real(c_double) :: IMAS_MASS_Cu_53
    real(c_double) :: IMAS_MASS_Cu_54
    real(c_double) :: IMAS_MASS_Cu_55
    real(c_double) :: IMAS_MASS_Cu_56
    real(c_double) :: IMAS_MASS_Cu_57
    real(c_double) :: IMAS_MASS_Cu_58
    real(c_double) :: IMAS_MASS_Cu_59
    real(c_double) :: IMAS_MASS_Cu_60
    real(c_double) :: IMAS_MASS_Cu_61
    real(c_double) :: IMAS_MASS_Cu_62
    real(c_double) :: IMAS_MASS_Cu_63
    real(c_double) :: IMAS_MASS_Cu_64
    real(c_double) :: IMAS_MASS_Cu_65
    real(c_double) :: IMAS_MASS_Cu_66
    real(c_double) :: IMAS_MASS_Cu_67
    real(c_double) :: IMAS_MASS_Cu_68
    real(c_double) :: IMAS_MASS_Cu_69
    real(c_double) :: IMAS_MASS_Cu_70
    real(c_double) :: IMAS_MASS_Cu_71
    real(c_double) :: IMAS_MASS_Cu_72
    real(c_double) :: IMAS_MASS_Cu_73
    real(c_double) :: IMAS_MASS_Cu_74
    real(c_double) :: IMAS_MASS_Cu_75
    real(c_double) :: IMAS_MASS_Cu_76
    real(c_double) :: IMAS_MASS_Cu_77
    real(c_double) :: IMAS_MASS_Cu_78
    real(c_double) :: IMAS_MASS_Cu_79
    real(c_double) :: IMAS_MASS_Cu_80
    real(c_double) :: IMAS_MASS_Zn_54
    real(c_double) :: IMAS_MASS_Zn_55
    real(c_double) :: IMAS_MASS_Zn_56
    real(c_double) :: IMAS_MASS_Zn_57
    real(c_double) :: IMAS_MASS_Zn_58
    real(c_double) :: IMAS_MASS_Zn_59
    real(c_double) :: IMAS_MASS_Zn_60
    real(c_double) :: IMAS_MASS_Zn_61
    real(c_double) :: IMAS_MASS_Zn_62
    real(c_double) :: IMAS_MASS_Zn_63
    real(c_double) :: IMAS_MASS_Zn_64
    real(c_double) :: IMAS_MASS_Zn_65
    real(c_double) :: IMAS_MASS_Zn_66
    real(c_double) :: IMAS_MASS_Zn_67
    real(c_double) :: IMAS_MASS_Zn_68
    real(c_double) :: IMAS_MASS_Zn_69
    real(c_double) :: IMAS_MASS_Zn_70
    real(c_double) :: IMAS_MASS_Zn_71
    real(c_double) :: IMAS_MASS_Zn_72
    real(c_double) :: IMAS_MASS_Zn_73
    real(c_double) :: IMAS_MASS_Zn_74
    real(c_double) :: IMAS_MASS_Zn_75
    real(c_double) :: IMAS_MASS_Zn_76
    real(c_double) :: IMAS_MASS_Zn_77
    real(c_double) :: IMAS_MASS_Zn_78
    real(c_double) :: IMAS_MASS_Zn_79
    real(c_double) :: IMAS_MASS_Zn_80
    real(c_double) :: IMAS_MASS_Zn_81
    real(c_double) :: IMAS_MASS_Zn_82
    real(c_double) :: IMAS_MASS_Zn_83
    real(c_double) :: IMAS_MASS_Ga_56
    real(c_double) :: IMAS_MASS_Ga_57
    real(c_double) :: IMAS_MASS_Ga_58
    real(c_double) :: IMAS_MASS_Ga_59
    real(c_double) :: IMAS_MASS_Ga_60
    real(c_double) :: IMAS_MASS_Ga_61
    real(c_double) :: IMAS_MASS_Ga_62
    real(c_double) :: IMAS_MASS_Ga_63
    real(c_double) :: IMAS_MASS_Ga_64
    real(c_double) :: IMAS_MASS_Ga_65
    real(c_double) :: IMAS_MASS_Ga_66
    real(c_double) :: IMAS_MASS_Ga_67
    real(c_double) :: IMAS_MASS_Ga_68
    real(c_double) :: IMAS_MASS_Ga_69
    real(c_double) :: IMAS_MASS_Ga_70
    real(c_double) :: IMAS_MASS_Ga_71
    real(c_double) :: IMAS_MASS_Ga_72
    real(c_double) :: IMAS_MASS_Ga_73
    real(c_double) :: IMAS_MASS_Ga_74
    real(c_double) :: IMAS_MASS_Ga_75
    real(c_double) :: IMAS_MASS_Ga_76
    real(c_double) :: IMAS_MASS_Ga_77
    real(c_double) :: IMAS_MASS_Ga_78
    real(c_double) :: IMAS_MASS_Ga_79
    real(c_double) :: IMAS_MASS_Ga_80
    real(c_double) :: IMAS_MASS_Ga_81
    real(c_double) :: IMAS_MASS_Ga_82
    real(c_double) :: IMAS_MASS_Ga_83
    real(c_double) :: IMAS_MASS_Ga_84
    real(c_double) :: IMAS_MASS_Ga_85
    real(c_double) :: IMAS_MASS_Ga_86
    real(c_double) :: IMAS_MASS_Ge_58
    real(c_double) :: IMAS_MASS_Ge_59
    real(c_double) :: IMAS_MASS_Ge_60
    real(c_double) :: IMAS_MASS_Ge_61
    real(c_double) :: IMAS_MASS_Ge_62
    real(c_double) :: IMAS_MASS_Ge_63
    real(c_double) :: IMAS_MASS_Ge_64
    real(c_double) :: IMAS_MASS_Ge_65
    real(c_double) :: IMAS_MASS_Ge_66
    real(c_double) :: IMAS_MASS_Ge_67
    real(c_double) :: IMAS_MASS_Ge_68
    real(c_double) :: IMAS_MASS_Ge_69
    real(c_double) :: IMAS_MASS_Ge_70
    real(c_double) :: IMAS_MASS_Ge_71
    real(c_double) :: IMAS_MASS_Ge_72
    real(c_double) :: IMAS_MASS_Ge_73
    real(c_double) :: IMAS_MASS_Ge_74
    real(c_double) :: IMAS_MASS_Ge_75
    real(c_double) :: IMAS_MASS_Ge_76
    real(c_double) :: IMAS_MASS_Ge_77
    real(c_double) :: IMAS_MASS_Ge_78
    real(c_double) :: IMAS_MASS_Ge_79
    real(c_double) :: IMAS_MASS_Ge_80
    real(c_double) :: IMAS_MASS_Ge_81
    real(c_double) :: IMAS_MASS_Ge_82
    real(c_double) :: IMAS_MASS_Ge_83
    real(c_double) :: IMAS_MASS_Ge_84
    real(c_double) :: IMAS_MASS_Ge_85
    real(c_double) :: IMAS_MASS_Ge_86
    real(c_double) :: IMAS_MASS_Ge_87
    real(c_double) :: IMAS_MASS_Ge_88
    real(c_double) :: IMAS_MASS_Ge_89
    real(c_double) :: IMAS_MASS_As_60
    real(c_double) :: IMAS_MASS_As_61
    real(c_double) :: IMAS_MASS_As_62
    real(c_double) :: IMAS_MASS_As_63
    real(c_double) :: IMAS_MASS_As_64
    real(c_double) :: IMAS_MASS_As_65
    real(c_double) :: IMAS_MASS_As_66
    real(c_double) :: IMAS_MASS_As_67
    real(c_double) :: IMAS_MASS_As_68
    real(c_double) :: IMAS_MASS_As_69
    real(c_double) :: IMAS_MASS_As_70
    real(c_double) :: IMAS_MASS_As_71
    real(c_double) :: IMAS_MASS_As_72
    real(c_double) :: IMAS_MASS_As_73
    real(c_double) :: IMAS_MASS_As_74
    real(c_double) :: IMAS_MASS_As_75
    real(c_double) :: IMAS_MASS_As_76
    real(c_double) :: IMAS_MASS_As_77
    real(c_double) :: IMAS_MASS_As_78
    real(c_double) :: IMAS_MASS_As_79
    real(c_double) :: IMAS_MASS_As_80
    real(c_double) :: IMAS_MASS_As_81
    real(c_double) :: IMAS_MASS_As_82
    real(c_double) :: IMAS_MASS_As_83
    real(c_double) :: IMAS_MASS_As_84
    real(c_double) :: IMAS_MASS_As_85
    real(c_double) :: IMAS_MASS_As_86
    real(c_double) :: IMAS_MASS_As_87
    real(c_double) :: IMAS_MASS_As_88
    real(c_double) :: IMAS_MASS_As_89
    real(c_double) :: IMAS_MASS_As_90
    real(c_double) :: IMAS_MASS_As_91
    real(c_double) :: IMAS_MASS_As_92
    real(c_double) :: IMAS_MASS_Se_65
    real(c_double) :: IMAS_MASS_Se_66
    real(c_double) :: IMAS_MASS_Se_67
    real(c_double) :: IMAS_MASS_Se_68
    real(c_double) :: IMAS_MASS_Se_69
    real(c_double) :: IMAS_MASS_Se_70
    real(c_double) :: IMAS_MASS_Se_71
    real(c_double) :: IMAS_MASS_Se_72
    real(c_double) :: IMAS_MASS_Se_73
    real(c_double) :: IMAS_MASS_Se_74
    real(c_double) :: IMAS_MASS_Se_75
    real(c_double) :: IMAS_MASS_Se_76
    real(c_double) :: IMAS_MASS_Se_77
    real(c_double) :: IMAS_MASS_Se_78
    real(c_double) :: IMAS_MASS_Se_79
    real(c_double) :: IMAS_MASS_Se_80
    real(c_double) :: IMAS_MASS_Se_81
    real(c_double) :: IMAS_MASS_Se_82
    real(c_double) :: IMAS_MASS_Se_83
    real(c_double) :: IMAS_MASS_Se_84
    real(c_double) :: IMAS_MASS_Se_85
    real(c_double) :: IMAS_MASS_Se_86
    real(c_double) :: IMAS_MASS_Se_87
    real(c_double) :: IMAS_MASS_Se_88
    real(c_double) :: IMAS_MASS_Se_89
    real(c_double) :: IMAS_MASS_Se_90
    real(c_double) :: IMAS_MASS_Se_91
    real(c_double) :: IMAS_MASS_Se_92
    real(c_double) :: IMAS_MASS_Se_93
    real(c_double) :: IMAS_MASS_Se_94
    real(c_double) :: IMAS_MASS_Br_67
    real(c_double) :: IMAS_MASS_Br_68
    real(c_double) :: IMAS_MASS_Br_69
    real(c_double) :: IMAS_MASS_Br_70
    real(c_double) :: IMAS_MASS_Br_71
    real(c_double) :: IMAS_MASS_Br_72
    real(c_double) :: IMAS_MASS_Br_73
    real(c_double) :: IMAS_MASS_Br_74
    real(c_double) :: IMAS_MASS_Br_75
    real(c_double) :: IMAS_MASS_Br_76
    real(c_double) :: IMAS_MASS_Br_77
    real(c_double) :: IMAS_MASS_Br_78
    real(c_double) :: IMAS_MASS_Br_79
    real(c_double) :: IMAS_MASS_Br_80
    real(c_double) :: IMAS_MASS_Br_81
    real(c_double) :: IMAS_MASS_Br_82
    real(c_double) :: IMAS_MASS_Br_83
    real(c_double) :: IMAS_MASS_Br_84
    real(c_double) :: IMAS_MASS_Br_85
    real(c_double) :: IMAS_MASS_Br_86
    real(c_double) :: IMAS_MASS_Br_87
    real(c_double) :: IMAS_MASS_Br_88
    real(c_double) :: IMAS_MASS_Br_89
    real(c_double) :: IMAS_MASS_Br_90
    real(c_double) :: IMAS_MASS_Br_91
    real(c_double) :: IMAS_MASS_Br_92
    real(c_double) :: IMAS_MASS_Br_93
    real(c_double) :: IMAS_MASS_Br_94
    real(c_double) :: IMAS_MASS_Br_95
    real(c_double) :: IMAS_MASS_Br_96
    real(c_double) :: IMAS_MASS_Br_97
    real(c_double) :: IMAS_MASS_Kr_69
    real(c_double) :: IMAS_MASS_Kr_70
    real(c_double) :: IMAS_MASS_Kr_71
    real(c_double) :: IMAS_MASS_Kr_72
    real(c_double) :: IMAS_MASS_Kr_73
    real(c_double) :: IMAS_MASS_Kr_74
    real(c_double) :: IMAS_MASS_Kr_75
    real(c_double) :: IMAS_MASS_Kr_76
    real(c_double) :: IMAS_MASS_Kr_77
    real(c_double) :: IMAS_MASS_Kr_78
    real(c_double) :: IMAS_MASS_Kr_79
    real(c_double) :: IMAS_MASS_Kr_80
    real(c_double) :: IMAS_MASS_Kr_81
    real(c_double) :: IMAS_MASS_Kr_82
    real(c_double) :: IMAS_MASS_Kr_83
    real(c_double) :: IMAS_MASS_Kr_84
    real(c_double) :: IMAS_MASS_Kr_85
    real(c_double) :: IMAS_MASS_Kr_86
    real(c_double) :: IMAS_MASS_Kr_87
    real(c_double) :: IMAS_MASS_Kr_88
    real(c_double) :: IMAS_MASS_Kr_89
    real(c_double) :: IMAS_MASS_Kr_90
    real(c_double) :: IMAS_MASS_Kr_91
    real(c_double) :: IMAS_MASS_Kr_92
    real(c_double) :: IMAS_MASS_Kr_93
    real(c_double) :: IMAS_MASS_Kr_94
    real(c_double) :: IMAS_MASS_Kr_95
    real(c_double) :: IMAS_MASS_Kr_96
    real(c_double) :: IMAS_MASS_Kr_97
    real(c_double) :: IMAS_MASS_Kr_98
    real(c_double) :: IMAS_MASS_Kr_99
    real(c_double) :: IMAS_MASS_Kr_100
    real(c_double) :: IMAS_MASS_Rb_71
    real(c_double) :: IMAS_MASS_Rb_72
    real(c_double) :: IMAS_MASS_Rb_73
    real(c_double) :: IMAS_MASS_Rb_74
    real(c_double) :: IMAS_MASS_Rb_75
    real(c_double) :: IMAS_MASS_Rb_76
    real(c_double) :: IMAS_MASS_Rb_77
    real(c_double) :: IMAS_MASS_Rb_78
    real(c_double) :: IMAS_MASS_Rb_79
    real(c_double) :: IMAS_MASS_Rb_80
    real(c_double) :: IMAS_MASS_Rb_81
    real(c_double) :: IMAS_MASS_Rb_82
    real(c_double) :: IMAS_MASS_Rb_83
    real(c_double) :: IMAS_MASS_Rb_84
    real(c_double) :: IMAS_MASS_Rb_85
    real(c_double) :: IMAS_MASS_Rb_86
    real(c_double) :: IMAS_MASS_Rb_87
    real(c_double) :: IMAS_MASS_Rb_88
    real(c_double) :: IMAS_MASS_Rb_89
    real(c_double) :: IMAS_MASS_Rb_90
    real(c_double) :: IMAS_MASS_Rb_91
    real(c_double) :: IMAS_MASS_Rb_92
    real(c_double) :: IMAS_MASS_Rb_93
    real(c_double) :: IMAS_MASS_Rb_94
    real(c_double) :: IMAS_MASS_Rb_95
    real(c_double) :: IMAS_MASS_Rb_96
    real(c_double) :: IMAS_MASS_Rb_97
    real(c_double) :: IMAS_MASS_Rb_98
    real(c_double) :: IMAS_MASS_Rb_99
    real(c_double) :: IMAS_MASS_Rb_100
    real(c_double) :: IMAS_MASS_Rb_101
    real(c_double) :: IMAS_MASS_Rb_102
    real(c_double) :: IMAS_MASS_Sr_73
    real(c_double) :: IMAS_MASS_Sr_74
    real(c_double) :: IMAS_MASS_Sr_75
    real(c_double) :: IMAS_MASS_Sr_76
    real(c_double) :: IMAS_MASS_Sr_77
    real(c_double) :: IMAS_MASS_Sr_78
    real(c_double) :: IMAS_MASS_Sr_79
    real(c_double) :: IMAS_MASS_Sr_80
    real(c_double) :: IMAS_MASS_Sr_81
    real(c_double) :: IMAS_MASS_Sr_82
    real(c_double) :: IMAS_MASS_Sr_83
    real(c_double) :: IMAS_MASS_Sr_84
    real(c_double) :: IMAS_MASS_Sr_85
    real(c_double) :: IMAS_MASS_Sr_86
    real(c_double) :: IMAS_MASS_Sr_87
    real(c_double) :: IMAS_MASS_Sr_88
    real(c_double) :: IMAS_MASS_Sr_89
    real(c_double) :: IMAS_MASS_Sr_90
    real(c_double) :: IMAS_MASS_Sr_91
    real(c_double) :: IMAS_MASS_Sr_92
    real(c_double) :: IMAS_MASS_Sr_93
    real(c_double) :: IMAS_MASS_Sr_94
    real(c_double) :: IMAS_MASS_Sr_95
    real(c_double) :: IMAS_MASS_Sr_96
    real(c_double) :: IMAS_MASS_Sr_97
    real(c_double) :: IMAS_MASS_Sr_98
    real(c_double) :: IMAS_MASS_Sr_99
    real(c_double) :: IMAS_MASS_Sr_100
    real(c_double) :: IMAS_MASS_Sr_101
    real(c_double) :: IMAS_MASS_Sr_102
    real(c_double) :: IMAS_MASS_Sr_103
    real(c_double) :: IMAS_MASS_Sr_104
    real(c_double) :: IMAS_MASS_Sr_105
    real(c_double) :: IMAS_MASS_Y_76
    real(c_double) :: IMAS_MASS_Y_77
    real(c_double) :: IMAS_MASS_Y_78
    real(c_double) :: IMAS_MASS_Y_79
    real(c_double) :: IMAS_MASS_Y_80
    real(c_double) :: IMAS_MASS_Y_81
    real(c_double) :: IMAS_MASS_Y_82
    real(c_double) :: IMAS_MASS_Y_83
    real(c_double) :: IMAS_MASS_Y_84
    real(c_double) :: IMAS_MASS_Y_85
    real(c_double) :: IMAS_MASS_Y_86
    real(c_double) :: IMAS_MASS_Y_87
    real(c_double) :: IMAS_MASS_Y_88
    real(c_double) :: IMAS_MASS_Y_89
    real(c_double) :: IMAS_MASS_Y_90
    real(c_double) :: IMAS_MASS_Y_91
    real(c_double) :: IMAS_MASS_Y_92
    real(c_double) :: IMAS_MASS_Y_93
    real(c_double) :: IMAS_MASS_Y_94
    real(c_double) :: IMAS_MASS_Y_95
    real(c_double) :: IMAS_MASS_Y_96
    real(c_double) :: IMAS_MASS_Y_97
    real(c_double) :: IMAS_MASS_Y_98
    real(c_double) :: IMAS_MASS_Y_99
    real(c_double) :: IMAS_MASS_Y_100
    real(c_double) :: IMAS_MASS_Y_101
    real(c_double) :: IMAS_MASS_Y_102
    real(c_double) :: IMAS_MASS_Y_103
    real(c_double) :: IMAS_MASS_Y_104
    real(c_double) :: IMAS_MASS_Y_105
    real(c_double) :: IMAS_MASS_Y_106
    real(c_double) :: IMAS_MASS_Y_107
    real(c_double) :: IMAS_MASS_Y_108
    real(c_double) :: IMAS_MASS_Zr_78
    real(c_double) :: IMAS_MASS_Zr_79
    real(c_double) :: IMAS_MASS_Zr_80
    real(c_double) :: IMAS_MASS_Zr_81
    real(c_double) :: IMAS_MASS_Zr_82
    real(c_double) :: IMAS_MASS_Zr_83
    real(c_double) :: IMAS_MASS_Zr_84
    real(c_double) :: IMAS_MASS_Zr_85
    real(c_double) :: IMAS_MASS_Zr_86
    real(c_double) :: IMAS_MASS_Zr_87
    real(c_double) :: IMAS_MASS_Zr_88
    real(c_double) :: IMAS_MASS_Zr_89
    real(c_double) :: IMAS_MASS_Zr_90
    real(c_double) :: IMAS_MASS_Zr_91
    real(c_double) :: IMAS_MASS_Zr_92
    real(c_double) :: IMAS_MASS_Zr_93
    real(c_double) :: IMAS_MASS_Zr_94
    real(c_double) :: IMAS_MASS_Zr_95
    real(c_double) :: IMAS_MASS_Zr_96
    real(c_double) :: IMAS_MASS_Zr_97
    real(c_double) :: IMAS_MASS_Zr_98
    real(c_double) :: IMAS_MASS_Zr_99
    real(c_double) :: IMAS_MASS_Zr_100
    real(c_double) :: IMAS_MASS_Zr_101
    real(c_double) :: IMAS_MASS_Zr_102
    real(c_double) :: IMAS_MASS_Zr_103
    real(c_double) :: IMAS_MASS_Zr_104
    real(c_double) :: IMAS_MASS_Zr_105
    real(c_double) :: IMAS_MASS_Zr_106
    real(c_double) :: IMAS_MASS_Zr_107
    real(c_double) :: IMAS_MASS_Zr_108
    real(c_double) :: IMAS_MASS_Zr_109
    real(c_double) :: IMAS_MASS_Zr_110
    real(c_double) :: IMAS_MASS_Nb_81
    real(c_double) :: IMAS_MASS_Nb_82
    real(c_double) :: IMAS_MASS_Nb_83
    real(c_double) :: IMAS_MASS_Nb_84
    real(c_double) :: IMAS_MASS_Nb_85
    real(c_double) :: IMAS_MASS_Nb_86
    real(c_double) :: IMAS_MASS_Nb_87
    real(c_double) :: IMAS_MASS_Nb_88
    real(c_double) :: IMAS_MASS_Nb_89
    real(c_double) :: IMAS_MASS_Nb_90
    real(c_double) :: IMAS_MASS_Nb_91
    real(c_double) :: IMAS_MASS_Nb_92
    real(c_double) :: IMAS_MASS_Nb_93
    real(c_double) :: IMAS_MASS_Nb_94
    real(c_double) :: IMAS_MASS_Nb_95
    real(c_double) :: IMAS_MASS_Nb_96
    real(c_double) :: IMAS_MASS_Nb_97
    real(c_double) :: IMAS_MASS_Nb_98
    real(c_double) :: IMAS_MASS_Nb_99
    real(c_double) :: IMAS_MASS_Nb_100
    real(c_double) :: IMAS_MASS_Nb_101
    real(c_double) :: IMAS_MASS_Nb_102
    real(c_double) :: IMAS_MASS_Nb_103
    real(c_double) :: IMAS_MASS_Nb_104
    real(c_double) :: IMAS_MASS_Nb_105
    real(c_double) :: IMAS_MASS_Nb_106
    real(c_double) :: IMAS_MASS_Nb_107
    real(c_double) :: IMAS_MASS_Nb_108
    real(c_double) :: IMAS_MASS_Nb_109
    real(c_double) :: IMAS_MASS_Nb_110
    real(c_double) :: IMAS_MASS_Nb_111
    real(c_double) :: IMAS_MASS_Nb_112
    real(c_double) :: IMAS_MASS_Nb_113
    real(c_double) :: IMAS_MASS_Mo_83
    real(c_double) :: IMAS_MASS_Mo_84
    real(c_double) :: IMAS_MASS_Mo_85
    real(c_double) :: IMAS_MASS_Mo_86
    real(c_double) :: IMAS_MASS_Mo_87
    real(c_double) :: IMAS_MASS_Mo_88
    real(c_double) :: IMAS_MASS_Mo_89
    real(c_double) :: IMAS_MASS_Mo_90
    real(c_double) :: IMAS_MASS_Mo_91
    real(c_double) :: IMAS_MASS_Mo_92
    real(c_double) :: IMAS_MASS_Mo_93
    real(c_double) :: IMAS_MASS_Mo_94
    real(c_double) :: IMAS_MASS_Mo_95
    real(c_double) :: IMAS_MASS_Mo_96
    real(c_double) :: IMAS_MASS_Mo_97
    real(c_double) :: IMAS_MASS_Mo_98
    real(c_double) :: IMAS_MASS_Mo_99
    real(c_double) :: IMAS_MASS_Mo_100
    real(c_double) :: IMAS_MASS_Mo_101
    real(c_double) :: IMAS_MASS_Mo_102
    real(c_double) :: IMAS_MASS_Mo_103
    real(c_double) :: IMAS_MASS_Mo_104
    real(c_double) :: IMAS_MASS_Mo_105
    real(c_double) :: IMAS_MASS_Mo_106
    real(c_double) :: IMAS_MASS_Mo_107
    real(c_double) :: IMAS_MASS_Mo_108
    real(c_double) :: IMAS_MASS_Mo_109
    real(c_double) :: IMAS_MASS_Mo_110
    real(c_double) :: IMAS_MASS_Mo_111
    real(c_double) :: IMAS_MASS_Mo_112
    real(c_double) :: IMAS_MASS_Mo_113
    real(c_double) :: IMAS_MASS_Mo_114
    real(c_double) :: IMAS_MASS_Mo_115
    real(c_double) :: IMAS_MASS_Tc_85
    real(c_double) :: IMAS_MASS_Tc_86
    real(c_double) :: IMAS_MASS_Tc_87
    real(c_double) :: IMAS_MASS_Tc_88
    real(c_double) :: IMAS_MASS_Tc_89
    real(c_double) :: IMAS_MASS_Tc_90
    real(c_double) :: IMAS_MASS_Tc_91
    real(c_double) :: IMAS_MASS_Tc_92
    real(c_double) :: IMAS_MASS_Tc_93
    real(c_double) :: IMAS_MASS_Tc_94
    real(c_double) :: IMAS_MASS_Tc_95
    real(c_double) :: IMAS_MASS_Tc_96
    real(c_double) :: IMAS_MASS_Tc_97
    real(c_double) :: IMAS_MASS_Tc_98
    real(c_double) :: IMAS_MASS_Tc_99
    real(c_double) :: IMAS_MASS_Tc_100
    real(c_double) :: IMAS_MASS_Tc_101
    real(c_double) :: IMAS_MASS_Tc_102
    real(c_double) :: IMAS_MASS_Tc_103
    real(c_double) :: IMAS_MASS_Tc_104
    real(c_double) :: IMAS_MASS_Tc_105
    real(c_double) :: IMAS_MASS_Tc_106
    real(c_double) :: IMAS_MASS_Tc_107
    real(c_double) :: IMAS_MASS_Tc_108
    real(c_double) :: IMAS_MASS_Tc_109
    real(c_double) :: IMAS_MASS_Tc_110
    real(c_double) :: IMAS_MASS_Tc_111
    real(c_double) :: IMAS_MASS_Tc_112
    real(c_double) :: IMAS_MASS_Tc_113
    real(c_double) :: IMAS_MASS_Tc_114
    real(c_double) :: IMAS_MASS_Tc_115
    real(c_double) :: IMAS_MASS_Tc_116
    real(c_double) :: IMAS_MASS_Tc_117
    real(c_double) :: IMAS_MASS_Tc_118
    real(c_double) :: IMAS_MASS_Ru_87
    real(c_double) :: IMAS_MASS_Ru_88
    real(c_double) :: IMAS_MASS_Ru_89
    real(c_double) :: IMAS_MASS_Ru_90
    real(c_double) :: IMAS_MASS_Ru_91
    real(c_double) :: IMAS_MASS_Ru_92
    real(c_double) :: IMAS_MASS_Ru_93
    real(c_double) :: IMAS_MASS_Ru_94
    real(c_double) :: IMAS_MASS_Ru_95
    real(c_double) :: IMAS_MASS_Ru_96
    real(c_double) :: IMAS_MASS_Ru_97
    real(c_double) :: IMAS_MASS_Ru_98
    real(c_double) :: IMAS_MASS_Ru_99
    real(c_double) :: IMAS_MASS_Ru_100
    real(c_double) :: IMAS_MASS_Ru_101
    real(c_double) :: IMAS_MASS_Ru_102
    real(c_double) :: IMAS_MASS_Ru_103
    real(c_double) :: IMAS_MASS_Ru_104
    real(c_double) :: IMAS_MASS_Ru_105
    real(c_double) :: IMAS_MASS_Ru_106
    real(c_double) :: IMAS_MASS_Ru_107
    real(c_double) :: IMAS_MASS_Ru_108
    real(c_double) :: IMAS_MASS_Ru_109
    real(c_double) :: IMAS_MASS_Ru_110
    real(c_double) :: IMAS_MASS_Ru_111
    real(c_double) :: IMAS_MASS_Ru_112
    real(c_double) :: IMAS_MASS_Ru_113
    real(c_double) :: IMAS_MASS_Ru_114
    real(c_double) :: IMAS_MASS_Ru_115
    real(c_double) :: IMAS_MASS_Ru_116
    real(c_double) :: IMAS_MASS_Ru_117
    real(c_double) :: IMAS_MASS_Ru_118
    real(c_double) :: IMAS_MASS_Ru_119
    real(c_double) :: IMAS_MASS_Ru_120
    real(c_double) :: IMAS_MASS_Rh_89
    real(c_double) :: IMAS_MASS_Rh_90
    real(c_double) :: IMAS_MASS_Rh_91
    real(c_double) :: IMAS_MASS_Rh_92
    real(c_double) :: IMAS_MASS_Rh_93
    real(c_double) :: IMAS_MASS_Rh_94
    real(c_double) :: IMAS_MASS_Rh_95
    real(c_double) :: IMAS_MASS_Rh_96
    real(c_double) :: IMAS_MASS_Rh_97
    real(c_double) :: IMAS_MASS_Rh_98
    real(c_double) :: IMAS_MASS_Rh_99
    real(c_double) :: IMAS_MASS_Rh_100
    real(c_double) :: IMAS_MASS_Rh_101
    real(c_double) :: IMAS_MASS_Rh_102
    real(c_double) :: IMAS_MASS_Rh_103
    real(c_double) :: IMAS_MASS_Rh_104
    real(c_double) :: IMAS_MASS_Rh_105
    real(c_double) :: IMAS_MASS_Rh_106
    real(c_double) :: IMAS_MASS_Rh_107
    real(c_double) :: IMAS_MASS_Rh_108
    real(c_double) :: IMAS_MASS_Rh_109
    real(c_double) :: IMAS_MASS_Rh_110
    real(c_double) :: IMAS_MASS_Rh_111
    real(c_double) :: IMAS_MASS_Rh_112
    real(c_double) :: IMAS_MASS_Rh_113
    real(c_double) :: IMAS_MASS_Rh_114
    real(c_double) :: IMAS_MASS_Rh_115
    real(c_double) :: IMAS_MASS_Rh_116
    real(c_double) :: IMAS_MASS_Rh_117
    real(c_double) :: IMAS_MASS_Rh_118
    real(c_double) :: IMAS_MASS_Rh_119
    real(c_double) :: IMAS_MASS_Rh_120
    real(c_double) :: IMAS_MASS_Rh_121
    real(c_double) :: IMAS_MASS_Rh_122
    real(c_double) :: IMAS_MASS_Pd_91
    real(c_double) :: IMAS_MASS_Pd_92
    real(c_double) :: IMAS_MASS_Pd_93
    real(c_double) :: IMAS_MASS_Pd_94
    real(c_double) :: IMAS_MASS_Pd_95
    real(c_double) :: IMAS_MASS_Pd_96
    real(c_double) :: IMAS_MASS_Pd_97
    real(c_double) :: IMAS_MASS_Pd_98
    real(c_double) :: IMAS_MASS_Pd_99
    real(c_double) :: IMAS_MASS_Pd_100
    real(c_double) :: IMAS_MASS_Pd_101
    real(c_double) :: IMAS_MASS_Pd_102
    real(c_double) :: IMAS_MASS_Pd_103
    real(c_double) :: IMAS_MASS_Pd_104
    real(c_double) :: IMAS_MASS_Pd_105
    real(c_double) :: IMAS_MASS_Pd_106
    real(c_double) :: IMAS_MASS_Pd_107
    real(c_double) :: IMAS_MASS_Pd_108
    real(c_double) :: IMAS_MASS_Pd_109
    real(c_double) :: IMAS_MASS_Pd_110
    real(c_double) :: IMAS_MASS_Pd_111
    real(c_double) :: IMAS_MASS_Pd_112
    real(c_double) :: IMAS_MASS_Pd_113
    real(c_double) :: IMAS_MASS_Pd_114
    real(c_double) :: IMAS_MASS_Pd_115
    real(c_double) :: IMAS_MASS_Pd_116
    real(c_double) :: IMAS_MASS_Pd_117
    real(c_double) :: IMAS_MASS_Pd_118
    real(c_double) :: IMAS_MASS_Pd_119
    real(c_double) :: IMAS_MASS_Pd_120
    real(c_double) :: IMAS_MASS_Pd_121
    real(c_double) :: IMAS_MASS_Pd_122
    real(c_double) :: IMAS_MASS_Pd_123
    real(c_double) :: IMAS_MASS_Pd_124
    real(c_double) :: IMAS_MASS_Ag_93
    real(c_double) :: IMAS_MASS_Ag_94
    real(c_double) :: IMAS_MASS_Ag_95
    real(c_double) :: IMAS_MASS_Ag_96
    real(c_double) :: IMAS_MASS_Ag_97
    real(c_double) :: IMAS_MASS_Ag_98
    real(c_double) :: IMAS_MASS_Ag_99
    real(c_double) :: IMAS_MASS_Ag_100
    real(c_double) :: IMAS_MASS_Ag_101
    real(c_double) :: IMAS_MASS_Ag_102
    real(c_double) :: IMAS_MASS_Ag_103
    real(c_double) :: IMAS_MASS_Ag_104
    real(c_double) :: IMAS_MASS_Ag_105
    real(c_double) :: IMAS_MASS_Ag_106
    real(c_double) :: IMAS_MASS_Ag_107
    real(c_double) :: IMAS_MASS_Ag_108
    real(c_double) :: IMAS_MASS_Ag_109
    real(c_double) :: IMAS_MASS_Ag_110
    real(c_double) :: IMAS_MASS_Ag_111
    real(c_double) :: IMAS_MASS_Ag_112
    real(c_double) :: IMAS_MASS_Ag_113
    real(c_double) :: IMAS_MASS_Ag_114
    real(c_double) :: IMAS_MASS_Ag_115
    real(c_double) :: IMAS_MASS_Ag_116
    real(c_double) :: IMAS_MASS_Ag_117
    real(c_double) :: IMAS_MASS_Ag_118
    real(c_double) :: IMAS_MASS_Ag_119
    real(c_double) :: IMAS_MASS_Ag_120
    real(c_double) :: IMAS_MASS_Ag_121
    real(c_double) :: IMAS_MASS_Ag_122
    real(c_double) :: IMAS_MASS_Ag_123
    real(c_double) :: IMAS_MASS_Ag_124
    real(c_double) :: IMAS_MASS_Ag_125
    real(c_double) :: IMAS_MASS_Ag_126
    real(c_double) :: IMAS_MASS_Ag_127
    real(c_double) :: IMAS_MASS_Ag_128
    real(c_double) :: IMAS_MASS_Ag_129
    real(c_double) :: IMAS_MASS_Ag_130
    real(c_double) :: IMAS_MASS_Cd_95
    real(c_double) :: IMAS_MASS_Cd_96
    real(c_double) :: IMAS_MASS_Cd_97
    real(c_double) :: IMAS_MASS_Cd_98
    real(c_double) :: IMAS_MASS_Cd_99
    real(c_double) :: IMAS_MASS_Cd_100
    real(c_double) :: IMAS_MASS_Cd_101
    real(c_double) :: IMAS_MASS_Cd_102
    real(c_double) :: IMAS_MASS_Cd_103
    real(c_double) :: IMAS_MASS_Cd_104
    real(c_double) :: IMAS_MASS_Cd_105
    real(c_double) :: IMAS_MASS_Cd_106
    real(c_double) :: IMAS_MASS_Cd_107
    real(c_double) :: IMAS_MASS_Cd_108
    real(c_double) :: IMAS_MASS_Cd_109
    real(c_double) :: IMAS_MASS_Cd_110
    real(c_double) :: IMAS_MASS_Cd_111
    real(c_double) :: IMAS_MASS_Cd_112
    real(c_double) :: IMAS_MASS_Cd_113
    real(c_double) :: IMAS_MASS_Cd_114
    real(c_double) :: IMAS_MASS_Cd_115
    real(c_double) :: IMAS_MASS_Cd_116
    real(c_double) :: IMAS_MASS_Cd_117
    real(c_double) :: IMAS_MASS_Cd_118
    real(c_double) :: IMAS_MASS_Cd_119
    real(c_double) :: IMAS_MASS_Cd_120
    real(c_double) :: IMAS_MASS_Cd_121
    real(c_double) :: IMAS_MASS_Cd_122
    real(c_double) :: IMAS_MASS_Cd_123
    real(c_double) :: IMAS_MASS_Cd_124
    real(c_double) :: IMAS_MASS_Cd_125
    real(c_double) :: IMAS_MASS_Cd_126
    real(c_double) :: IMAS_MASS_Cd_127
    real(c_double) :: IMAS_MASS_Cd_128
    real(c_double) :: IMAS_MASS_Cd_129
    real(c_double) :: IMAS_MASS_Cd_130
    real(c_double) :: IMAS_MASS_Cd_131
    real(c_double) :: IMAS_MASS_Cd_132
    real(c_double) :: IMAS_MASS_In_97
    real(c_double) :: IMAS_MASS_In_98
    real(c_double) :: IMAS_MASS_In_99
    real(c_double) :: IMAS_MASS_In_100
    real(c_double) :: IMAS_MASS_In_101
    real(c_double) :: IMAS_MASS_In_102
    real(c_double) :: IMAS_MASS_In_103
    real(c_double) :: IMAS_MASS_In_104
    real(c_double) :: IMAS_MASS_In_105
    real(c_double) :: IMAS_MASS_In_106
    real(c_double) :: IMAS_MASS_In_107
    real(c_double) :: IMAS_MASS_In_108
    real(c_double) :: IMAS_MASS_In_109
    real(c_double) :: IMAS_MASS_In_110
    real(c_double) :: IMAS_MASS_In_111
    real(c_double) :: IMAS_MASS_In_112
    real(c_double) :: IMAS_MASS_In_113
    real(c_double) :: IMAS_MASS_In_114
    real(c_double) :: IMAS_MASS_In_115
    real(c_double) :: IMAS_MASS_In_116
    real(c_double) :: IMAS_MASS_In_117
    real(c_double) :: IMAS_MASS_In_118
    real(c_double) :: IMAS_MASS_In_119
    real(c_double) :: IMAS_MASS_In_120
    real(c_double) :: IMAS_MASS_In_121
    real(c_double) :: IMAS_MASS_In_122
    real(c_double) :: IMAS_MASS_In_123
    real(c_double) :: IMAS_MASS_In_124
    real(c_double) :: IMAS_MASS_In_125
    real(c_double) :: IMAS_MASS_In_126
    real(c_double) :: IMAS_MASS_In_127
    real(c_double) :: IMAS_MASS_In_128
    real(c_double) :: IMAS_MASS_In_129
    real(c_double) :: IMAS_MASS_In_130
    real(c_double) :: IMAS_MASS_In_131
    real(c_double) :: IMAS_MASS_In_132
    real(c_double) :: IMAS_MASS_In_133
    real(c_double) :: IMAS_MASS_In_134
    real(c_double) :: IMAS_MASS_In_135
    real(c_double) :: IMAS_MASS_Sn_99
    real(c_double) :: IMAS_MASS_Sn_100
    real(c_double) :: IMAS_MASS_Sn_101
    real(c_double) :: IMAS_MASS_Sn_102
    real(c_double) :: IMAS_MASS_Sn_103
    real(c_double) :: IMAS_MASS_Sn_104
    real(c_double) :: IMAS_MASS_Sn_105
    real(c_double) :: IMAS_MASS_Sn_106
    real(c_double) :: IMAS_MASS_Sn_107
    real(c_double) :: IMAS_MASS_Sn_108
    real(c_double) :: IMAS_MASS_Sn_109
    real(c_double) :: IMAS_MASS_Sn_110
    real(c_double) :: IMAS_MASS_Sn_111
    real(c_double) :: IMAS_MASS_Sn_112
    real(c_double) :: IMAS_MASS_Sn_113
    real(c_double) :: IMAS_MASS_Sn_114
    real(c_double) :: IMAS_MASS_Sn_115
    real(c_double) :: IMAS_MASS_Sn_116
    real(c_double) :: IMAS_MASS_Sn_117
    real(c_double) :: IMAS_MASS_Sn_118
    real(c_double) :: IMAS_MASS_Sn_119
    real(c_double) :: IMAS_MASS_Sn_120
    real(c_double) :: IMAS_MASS_Sn_121
    real(c_double) :: IMAS_MASS_Sn_122
    real(c_double) :: IMAS_MASS_Sn_123
    real(c_double) :: IMAS_MASS_Sn_124
    real(c_double) :: IMAS_MASS_Sn_125
    real(c_double) :: IMAS_MASS_Sn_126
    real(c_double) :: IMAS_MASS_Sn_127
    real(c_double) :: IMAS_MASS_Sn_128
    real(c_double) :: IMAS_MASS_Sn_129
    real(c_double) :: IMAS_MASS_Sn_130
    real(c_double) :: IMAS_MASS_Sn_131
    real(c_double) :: IMAS_MASS_Sn_132
    real(c_double) :: IMAS_MASS_Sn_133
    real(c_double) :: IMAS_MASS_Sn_134
    real(c_double) :: IMAS_MASS_Sn_135
    real(c_double) :: IMAS_MASS_Sn_136
    real(c_double) :: IMAS_MASS_Sn_137
    real(c_double) :: IMAS_MASS_Sb_103
    real(c_double) :: IMAS_MASS_Sb_104
    real(c_double) :: IMAS_MASS_Sb_105
    real(c_double) :: IMAS_MASS_Sb_106
    real(c_double) :: IMAS_MASS_Sb_107
    real(c_double) :: IMAS_MASS_Sb_108
    real(c_double) :: IMAS_MASS_Sb_109
    real(c_double) :: IMAS_MASS_Sb_110
    real(c_double) :: IMAS_MASS_Sb_111
    real(c_double) :: IMAS_MASS_Sb_112
    real(c_double) :: IMAS_MASS_Sb_113
    real(c_double) :: IMAS_MASS_Sb_114
    real(c_double) :: IMAS_MASS_Sb_115
    real(c_double) :: IMAS_MASS_Sb_116
    real(c_double) :: IMAS_MASS_Sb_117
    real(c_double) :: IMAS_MASS_Sb_118
    real(c_double) :: IMAS_MASS_Sb_119
    real(c_double) :: IMAS_MASS_Sb_120
    real(c_double) :: IMAS_MASS_Sb_121
    real(c_double) :: IMAS_MASS_Sb_122
    real(c_double) :: IMAS_MASS_Sb_123
    real(c_double) :: IMAS_MASS_Sb_124
    real(c_double) :: IMAS_MASS_Sb_125
    real(c_double) :: IMAS_MASS_Sb_126
    real(c_double) :: IMAS_MASS_Sb_127
    real(c_double) :: IMAS_MASS_Sb_128
    real(c_double) :: IMAS_MASS_Sb_129
    real(c_double) :: IMAS_MASS_Sb_130
    real(c_double) :: IMAS_MASS_Sb_131
    real(c_double) :: IMAS_MASS_Sb_132
    real(c_double) :: IMAS_MASS_Sb_133
    real(c_double) :: IMAS_MASS_Sb_134
    real(c_double) :: IMAS_MASS_Sb_135
    real(c_double) :: IMAS_MASS_Sb_136
    real(c_double) :: IMAS_MASS_Sb_137
    real(c_double) :: IMAS_MASS_Sb_138
    real(c_double) :: IMAS_MASS_Sb_139
    real(c_double) :: IMAS_MASS_Te_105
    real(c_double) :: IMAS_MASS_Te_106
    real(c_double) :: IMAS_MASS_Te_107
    real(c_double) :: IMAS_MASS_Te_108
    real(c_double) :: IMAS_MASS_Te_109
    real(c_double) :: IMAS_MASS_Te_110
    real(c_double) :: IMAS_MASS_Te_111
    real(c_double) :: IMAS_MASS_Te_112
    real(c_double) :: IMAS_MASS_Te_113
    real(c_double) :: IMAS_MASS_Te_114
    real(c_double) :: IMAS_MASS_Te_115
    real(c_double) :: IMAS_MASS_Te_116
    real(c_double) :: IMAS_MASS_Te_117
    real(c_double) :: IMAS_MASS_Te_118
    real(c_double) :: IMAS_MASS_Te_119
    real(c_double) :: IMAS_MASS_Te_120
    real(c_double) :: IMAS_MASS_Te_121
    real(c_double) :: IMAS_MASS_Te_122
    real(c_double) :: IMAS_MASS_Te_123
    real(c_double) :: IMAS_MASS_Te_124
    real(c_double) :: IMAS_MASS_Te_125
    real(c_double) :: IMAS_MASS_Te_126
    real(c_double) :: IMAS_MASS_Te_127
    real(c_double) :: IMAS_MASS_Te_128
    real(c_double) :: IMAS_MASS_Te_129
    real(c_double) :: IMAS_MASS_Te_130
    real(c_double) :: IMAS_MASS_Te_131
    real(c_double) :: IMAS_MASS_Te_132
    real(c_double) :: IMAS_MASS_Te_133
    real(c_double) :: IMAS_MASS_Te_134
    real(c_double) :: IMAS_MASS_Te_135
    real(c_double) :: IMAS_MASS_Te_136
    real(c_double) :: IMAS_MASS_Te_137
    real(c_double) :: IMAS_MASS_Te_138
    real(c_double) :: IMAS_MASS_Te_139
    real(c_double) :: IMAS_MASS_Te_140
    real(c_double) :: IMAS_MASS_Te_141
    real(c_double) :: IMAS_MASS_Te_142
    real(c_double) :: IMAS_MASS_I_108
    real(c_double) :: IMAS_MASS_I_109
    real(c_double) :: IMAS_MASS_I_110
    real(c_double) :: IMAS_MASS_I_111
    real(c_double) :: IMAS_MASS_I_112
    real(c_double) :: IMAS_MASS_I_113
    real(c_double) :: IMAS_MASS_I_114
    real(c_double) :: IMAS_MASS_I_115
    real(c_double) :: IMAS_MASS_I_116
    real(c_double) :: IMAS_MASS_I_117
    real(c_double) :: IMAS_MASS_I_118
    real(c_double) :: IMAS_MASS_I_119
    real(c_double) :: IMAS_MASS_I_120
    real(c_double) :: IMAS_MASS_I_121
    real(c_double) :: IMAS_MASS_I_122
    real(c_double) :: IMAS_MASS_I_123
    real(c_double) :: IMAS_MASS_I_124
    real(c_double) :: IMAS_MASS_I_125
    real(c_double) :: IMAS_MASS_I_126
    real(c_double) :: IMAS_MASS_I_127
    real(c_double) :: IMAS_MASS_I_128
    real(c_double) :: IMAS_MASS_I_129
    real(c_double) :: IMAS_MASS_I_130
    real(c_double) :: IMAS_MASS_I_131
    real(c_double) :: IMAS_MASS_I_132
    real(c_double) :: IMAS_MASS_I_133
    real(c_double) :: IMAS_MASS_I_134
    real(c_double) :: IMAS_MASS_I_135
    real(c_double) :: IMAS_MASS_I_136
    real(c_double) :: IMAS_MASS_I_137
    real(c_double) :: IMAS_MASS_I_138
    real(c_double) :: IMAS_MASS_I_139
    real(c_double) :: IMAS_MASS_I_140
    real(c_double) :: IMAS_MASS_I_141
    real(c_double) :: IMAS_MASS_I_142
    real(c_double) :: IMAS_MASS_I_143
    real(c_double) :: IMAS_MASS_I_144
    real(c_double) :: IMAS_MASS_Xe_110
    real(c_double) :: IMAS_MASS_Xe_111
    real(c_double) :: IMAS_MASS_Xe_112
    real(c_double) :: IMAS_MASS_Xe_113
    real(c_double) :: IMAS_MASS_Xe_114
    real(c_double) :: IMAS_MASS_Xe_115
    real(c_double) :: IMAS_MASS_Xe_116
    real(c_double) :: IMAS_MASS_Xe_117
    real(c_double) :: IMAS_MASS_Xe_118
    real(c_double) :: IMAS_MASS_Xe_119
    real(c_double) :: IMAS_MASS_Xe_120
    real(c_double) :: IMAS_MASS_Xe_121
    real(c_double) :: IMAS_MASS_Xe_122
    real(c_double) :: IMAS_MASS_Xe_123
    real(c_double) :: IMAS_MASS_Xe_124
    real(c_double) :: IMAS_MASS_Xe_125
    real(c_double) :: IMAS_MASS_Xe_126
    real(c_double) :: IMAS_MASS_Xe_127
    real(c_double) :: IMAS_MASS_Xe_128
    real(c_double) :: IMAS_MASS_Xe_129
    real(c_double) :: IMAS_MASS_Xe_130
    real(c_double) :: IMAS_MASS_Xe_131
    real(c_double) :: IMAS_MASS_Xe_132
    real(c_double) :: IMAS_MASS_Xe_133
    real(c_double) :: IMAS_MASS_Xe_134
    real(c_double) :: IMAS_MASS_Xe_135
    real(c_double) :: IMAS_MASS_Xe_136
    real(c_double) :: IMAS_MASS_Xe_137
    real(c_double) :: IMAS_MASS_Xe_138
    real(c_double) :: IMAS_MASS_Xe_139
    real(c_double) :: IMAS_MASS_Xe_140
    real(c_double) :: IMAS_MASS_Xe_141
    real(c_double) :: IMAS_MASS_Xe_142
    real(c_double) :: IMAS_MASS_Xe_143
    real(c_double) :: IMAS_MASS_Xe_144
    real(c_double) :: IMAS_MASS_Xe_145
    real(c_double) :: IMAS_MASS_Xe_146
    real(c_double) :: IMAS_MASS_Xe_147
    real(c_double) :: IMAS_MASS_Cs_112
    real(c_double) :: IMAS_MASS_Cs_113
    real(c_double) :: IMAS_MASS_Cs_114
    real(c_double) :: IMAS_MASS_Cs_115
    real(c_double) :: IMAS_MASS_Cs_116
    real(c_double) :: IMAS_MASS_Cs_117
    real(c_double) :: IMAS_MASS_Cs_118
    real(c_double) :: IMAS_MASS_Cs_119
    real(c_double) :: IMAS_MASS_Cs_120
    real(c_double) :: IMAS_MASS_Cs_121
    real(c_double) :: IMAS_MASS_Cs_122
    real(c_double) :: IMAS_MASS_Cs_123
    real(c_double) :: IMAS_MASS_Cs_124
    real(c_double) :: IMAS_MASS_Cs_125
    real(c_double) :: IMAS_MASS_Cs_126
    real(c_double) :: IMAS_MASS_Cs_127
    real(c_double) :: IMAS_MASS_Cs_128
    real(c_double) :: IMAS_MASS_Cs_129
    real(c_double) :: IMAS_MASS_Cs_130
    real(c_double) :: IMAS_MASS_Cs_131
    real(c_double) :: IMAS_MASS_Cs_132
    real(c_double) :: IMAS_MASS_Cs_133
    real(c_double) :: IMAS_MASS_Cs_134
    real(c_double) :: IMAS_MASS_Cs_135
    real(c_double) :: IMAS_MASS_Cs_136
    real(c_double) :: IMAS_MASS_Cs_137
    real(c_double) :: IMAS_MASS_Cs_138
    real(c_double) :: IMAS_MASS_Cs_139
    real(c_double) :: IMAS_MASS_Cs_140
    real(c_double) :: IMAS_MASS_Cs_141
    real(c_double) :: IMAS_MASS_Cs_142
    real(c_double) :: IMAS_MASS_Cs_143
    real(c_double) :: IMAS_MASS_Cs_144
    real(c_double) :: IMAS_MASS_Cs_145
    real(c_double) :: IMAS_MASS_Cs_146
    real(c_double) :: IMAS_MASS_Cs_147
    real(c_double) :: IMAS_MASS_Cs_148
    real(c_double) :: IMAS_MASS_Cs_149
    real(c_double) :: IMAS_MASS_Cs_150
    real(c_double) :: IMAS_MASS_Cs_151
    real(c_double) :: IMAS_MASS_Ba_114
    real(c_double) :: IMAS_MASS_Ba_115
    real(c_double) :: IMAS_MASS_Ba_116
    real(c_double) :: IMAS_MASS_Ba_117
    real(c_double) :: IMAS_MASS_Ba_118
    real(c_double) :: IMAS_MASS_Ba_119
    real(c_double) :: IMAS_MASS_Ba_120
    real(c_double) :: IMAS_MASS_Ba_121
    real(c_double) :: IMAS_MASS_Ba_122
    real(c_double) :: IMAS_MASS_Ba_123
    real(c_double) :: IMAS_MASS_Ba_124
    real(c_double) :: IMAS_MASS_Ba_125
    real(c_double) :: IMAS_MASS_Ba_126
    real(c_double) :: IMAS_MASS_Ba_127
    real(c_double) :: IMAS_MASS_Ba_128
    real(c_double) :: IMAS_MASS_Ba_129
    real(c_double) :: IMAS_MASS_Ba_130
    real(c_double) :: IMAS_MASS_Ba_131
    real(c_double) :: IMAS_MASS_Ba_132
    real(c_double) :: IMAS_MASS_Ba_133
    real(c_double) :: IMAS_MASS_Ba_134
    real(c_double) :: IMAS_MASS_Ba_135
    real(c_double) :: IMAS_MASS_Ba_136
    real(c_double) :: IMAS_MASS_Ba_137
    real(c_double) :: IMAS_MASS_Ba_138
    real(c_double) :: IMAS_MASS_Ba_139
    real(c_double) :: IMAS_MASS_Ba_140
    real(c_double) :: IMAS_MASS_Ba_141
    real(c_double) :: IMAS_MASS_Ba_142
    real(c_double) :: IMAS_MASS_Ba_143
    real(c_double) :: IMAS_MASS_Ba_144
    real(c_double) :: IMAS_MASS_Ba_145
    real(c_double) :: IMAS_MASS_Ba_146
    real(c_double) :: IMAS_MASS_Ba_147
    real(c_double) :: IMAS_MASS_Ba_148
    real(c_double) :: IMAS_MASS_Ba_149
    real(c_double) :: IMAS_MASS_Ba_150
    real(c_double) :: IMAS_MASS_Ba_151
    real(c_double) :: IMAS_MASS_Ba_152
    real(c_double) :: IMAS_MASS_Ba_153
    real(c_double) :: IMAS_MASS_La_117
    real(c_double) :: IMAS_MASS_La_118
    real(c_double) :: IMAS_MASS_La_119
    real(c_double) :: IMAS_MASS_La_120
    real(c_double) :: IMAS_MASS_La_121
    real(c_double) :: IMAS_MASS_La_122
    real(c_double) :: IMAS_MASS_La_123
    real(c_double) :: IMAS_MASS_La_124
    real(c_double) :: IMAS_MASS_La_125
    real(c_double) :: IMAS_MASS_La_126
    real(c_double) :: IMAS_MASS_La_127
    real(c_double) :: IMAS_MASS_La_128
    real(c_double) :: IMAS_MASS_La_129
    real(c_double) :: IMAS_MASS_La_130
    real(c_double) :: IMAS_MASS_La_131
    real(c_double) :: IMAS_MASS_La_132
    real(c_double) :: IMAS_MASS_La_133
    real(c_double) :: IMAS_MASS_La_134
    real(c_double) :: IMAS_MASS_La_135
    real(c_double) :: IMAS_MASS_La_136
    real(c_double) :: IMAS_MASS_La_137
    real(c_double) :: IMAS_MASS_La_138
    real(c_double) :: IMAS_MASS_La_139
    real(c_double) :: IMAS_MASS_La_140
    real(c_double) :: IMAS_MASS_La_141
    real(c_double) :: IMAS_MASS_La_142
    real(c_double) :: IMAS_MASS_La_143
    real(c_double) :: IMAS_MASS_La_144
    real(c_double) :: IMAS_MASS_La_145
    real(c_double) :: IMAS_MASS_La_146
    real(c_double) :: IMAS_MASS_La_147
    real(c_double) :: IMAS_MASS_La_148
    real(c_double) :: IMAS_MASS_La_149
    real(c_double) :: IMAS_MASS_La_150
    real(c_double) :: IMAS_MASS_La_151
    real(c_double) :: IMAS_MASS_La_152
    real(c_double) :: IMAS_MASS_La_153
    real(c_double) :: IMAS_MASS_La_154
    real(c_double) :: IMAS_MASS_La_155
    real(c_double) :: IMAS_MASS_Ce_119
    real(c_double) :: IMAS_MASS_Ce_120
    real(c_double) :: IMAS_MASS_Ce_121
    real(c_double) :: IMAS_MASS_Ce_122
    real(c_double) :: IMAS_MASS_Ce_123
    real(c_double) :: IMAS_MASS_Ce_124
    real(c_double) :: IMAS_MASS_Ce_125
    real(c_double) :: IMAS_MASS_Ce_126
    real(c_double) :: IMAS_MASS_Ce_127
    real(c_double) :: IMAS_MASS_Ce_128
    real(c_double) :: IMAS_MASS_Ce_129
    real(c_double) :: IMAS_MASS_Ce_130
    real(c_double) :: IMAS_MASS_Ce_131
    real(c_double) :: IMAS_MASS_Ce_132
    real(c_double) :: IMAS_MASS_Ce_133
    real(c_double) :: IMAS_MASS_Ce_134
    real(c_double) :: IMAS_MASS_Ce_135
    real(c_double) :: IMAS_MASS_Ce_136
    real(c_double) :: IMAS_MASS_Ce_137
    real(c_double) :: IMAS_MASS_Ce_138
    real(c_double) :: IMAS_MASS_Ce_139
    real(c_double) :: IMAS_MASS_Ce_140
    real(c_double) :: IMAS_MASS_Ce_141
    real(c_double) :: IMAS_MASS_Ce_142
    real(c_double) :: IMAS_MASS_Ce_143
    real(c_double) :: IMAS_MASS_Ce_144
    real(c_double) :: IMAS_MASS_Ce_145
    real(c_double) :: IMAS_MASS_Ce_146
    real(c_double) :: IMAS_MASS_Ce_147
    real(c_double) :: IMAS_MASS_Ce_148
    real(c_double) :: IMAS_MASS_Ce_149
    real(c_double) :: IMAS_MASS_Ce_150
    real(c_double) :: IMAS_MASS_Ce_151
    real(c_double) :: IMAS_MASS_Ce_152
    real(c_double) :: IMAS_MASS_Ce_153
    real(c_double) :: IMAS_MASS_Ce_154
    real(c_double) :: IMAS_MASS_Ce_155
    real(c_double) :: IMAS_MASS_Ce_156
    real(c_double) :: IMAS_MASS_Ce_157
    real(c_double) :: IMAS_MASS_Pr_121
    real(c_double) :: IMAS_MASS_Pr_122
    real(c_double) :: IMAS_MASS_Pr_123
    real(c_double) :: IMAS_MASS_Pr_124
    real(c_double) :: IMAS_MASS_Pr_125
    real(c_double) :: IMAS_MASS_Pr_126
    real(c_double) :: IMAS_MASS_Pr_127
    real(c_double) :: IMAS_MASS_Pr_128
    real(c_double) :: IMAS_MASS_Pr_129
    real(c_double) :: IMAS_MASS_Pr_130
    real(c_double) :: IMAS_MASS_Pr_131
    real(c_double) :: IMAS_MASS_Pr_132
    real(c_double) :: IMAS_MASS_Pr_133
    real(c_double) :: IMAS_MASS_Pr_134
    real(c_double) :: IMAS_MASS_Pr_135
    real(c_double) :: IMAS_MASS_Pr_136
    real(c_double) :: IMAS_MASS_Pr_137
    real(c_double) :: IMAS_MASS_Pr_138
    real(c_double) :: IMAS_MASS_Pr_139
    real(c_double) :: IMAS_MASS_Pr_140
    real(c_double) :: IMAS_MASS_Pr_141
    real(c_double) :: IMAS_MASS_Pr_142
    real(c_double) :: IMAS_MASS_Pr_143
    real(c_double) :: IMAS_MASS_Pr_144
    real(c_double) :: IMAS_MASS_Pr_145
    real(c_double) :: IMAS_MASS_Pr_146
    real(c_double) :: IMAS_MASS_Pr_147
    real(c_double) :: IMAS_MASS_Pr_148
    real(c_double) :: IMAS_MASS_Pr_149
    real(c_double) :: IMAS_MASS_Pr_150
    real(c_double) :: IMAS_MASS_Pr_151
    real(c_double) :: IMAS_MASS_Pr_152
    real(c_double) :: IMAS_MASS_Pr_153
    real(c_double) :: IMAS_MASS_Pr_154
    real(c_double) :: IMAS_MASS_Pr_155
    real(c_double) :: IMAS_MASS_Pr_156
    real(c_double) :: IMAS_MASS_Pr_157
    real(c_double) :: IMAS_MASS_Pr_158
    real(c_double) :: IMAS_MASS_Pr_159
    real(c_double) :: IMAS_MASS_Nd_124
    real(c_double) :: IMAS_MASS_Nd_125
    real(c_double) :: IMAS_MASS_Nd_126
    real(c_double) :: IMAS_MASS_Nd_127
    real(c_double) :: IMAS_MASS_Nd_128
    real(c_double) :: IMAS_MASS_Nd_129
    real(c_double) :: IMAS_MASS_Nd_130
    real(c_double) :: IMAS_MASS_Nd_131
    real(c_double) :: IMAS_MASS_Nd_132
    real(c_double) :: IMAS_MASS_Nd_133
    real(c_double) :: IMAS_MASS_Nd_134
    real(c_double) :: IMAS_MASS_Nd_135
    real(c_double) :: IMAS_MASS_Nd_136
    real(c_double) :: IMAS_MASS_Nd_137
    real(c_double) :: IMAS_MASS_Nd_138
    real(c_double) :: IMAS_MASS_Nd_139
    real(c_double) :: IMAS_MASS_Nd_140
    real(c_double) :: IMAS_MASS_Nd_141
    real(c_double) :: IMAS_MASS_Nd_142
    real(c_double) :: IMAS_MASS_Nd_143
    real(c_double) :: IMAS_MASS_Nd_144
    real(c_double) :: IMAS_MASS_Nd_145
    real(c_double) :: IMAS_MASS_Nd_146
    real(c_double) :: IMAS_MASS_Nd_147
    real(c_double) :: IMAS_MASS_Nd_148
    real(c_double) :: IMAS_MASS_Nd_149
    real(c_double) :: IMAS_MASS_Nd_150
    real(c_double) :: IMAS_MASS_Nd_151
    real(c_double) :: IMAS_MASS_Nd_152
    real(c_double) :: IMAS_MASS_Nd_153
    real(c_double) :: IMAS_MASS_Nd_154
    real(c_double) :: IMAS_MASS_Nd_155
    real(c_double) :: IMAS_MASS_Nd_156
    real(c_double) :: IMAS_MASS_Nd_157
    real(c_double) :: IMAS_MASS_Nd_158
    real(c_double) :: IMAS_MASS_Nd_159
    real(c_double) :: IMAS_MASS_Nd_160
    real(c_double) :: IMAS_MASS_Nd_161
    real(c_double) :: IMAS_MASS_Pm_126
    real(c_double) :: IMAS_MASS_Pm_127
    real(c_double) :: IMAS_MASS_Pm_128
    real(c_double) :: IMAS_MASS_Pm_129
    real(c_double) :: IMAS_MASS_Pm_130
    real(c_double) :: IMAS_MASS_Pm_131
    real(c_double) :: IMAS_MASS_Pm_132
    real(c_double) :: IMAS_MASS_Pm_133
    real(c_double) :: IMAS_MASS_Pm_134
    real(c_double) :: IMAS_MASS_Pm_135
    real(c_double) :: IMAS_MASS_Pm_136
    real(c_double) :: IMAS_MASS_Pm_137
    real(c_double) :: IMAS_MASS_Pm_138
    real(c_double) :: IMAS_MASS_Pm_139
    real(c_double) :: IMAS_MASS_Pm_140
    real(c_double) :: IMAS_MASS_Pm_141
    real(c_double) :: IMAS_MASS_Pm_142
    real(c_double) :: IMAS_MASS_Pm_143
    real(c_double) :: IMAS_MASS_Pm_144
    real(c_double) :: IMAS_MASS_Pm_145
    real(c_double) :: IMAS_MASS_Pm_146
    real(c_double) :: IMAS_MASS_Pm_147
    real(c_double) :: IMAS_MASS_Pm_148
    real(c_double) :: IMAS_MASS_Pm_149
    real(c_double) :: IMAS_MASS_Pm_150
    real(c_double) :: IMAS_MASS_Pm_151
    real(c_double) :: IMAS_MASS_Pm_152
    real(c_double) :: IMAS_MASS_Pm_153
    real(c_double) :: IMAS_MASS_Pm_154
    real(c_double) :: IMAS_MASS_Pm_155
    real(c_double) :: IMAS_MASS_Pm_156
    real(c_double) :: IMAS_MASS_Pm_157
    real(c_double) :: IMAS_MASS_Pm_158
    real(c_double) :: IMAS_MASS_Pm_159
    real(c_double) :: IMAS_MASS_Pm_160
    real(c_double) :: IMAS_MASS_Pm_161
    real(c_double) :: IMAS_MASS_Pm_162
    real(c_double) :: IMAS_MASS_Pm_163
    real(c_double) :: IMAS_MASS_Sm_128
    real(c_double) :: IMAS_MASS_Sm_129
    real(c_double) :: IMAS_MASS_Sm_130
    real(c_double) :: IMAS_MASS_Sm_131
    real(c_double) :: IMAS_MASS_Sm_132
    real(c_double) :: IMAS_MASS_Sm_133
    real(c_double) :: IMAS_MASS_Sm_134
    real(c_double) :: IMAS_MASS_Sm_135
    real(c_double) :: IMAS_MASS_Sm_136
    real(c_double) :: IMAS_MASS_Sm_137
    real(c_double) :: IMAS_MASS_Sm_138
    real(c_double) :: IMAS_MASS_Sm_139
    real(c_double) :: IMAS_MASS_Sm_140
    real(c_double) :: IMAS_MASS_Sm_141
    real(c_double) :: IMAS_MASS_Sm_142
    real(c_double) :: IMAS_MASS_Sm_143
    real(c_double) :: IMAS_MASS_Sm_144
    real(c_double) :: IMAS_MASS_Sm_145
    real(c_double) :: IMAS_MASS_Sm_146
    real(c_double) :: IMAS_MASS_Sm_147
    real(c_double) :: IMAS_MASS_Sm_148
    real(c_double) :: IMAS_MASS_Sm_149
    real(c_double) :: IMAS_MASS_Sm_150
    real(c_double) :: IMAS_MASS_Sm_151
    real(c_double) :: IMAS_MASS_Sm_152
    real(c_double) :: IMAS_MASS_Sm_153
    real(c_double) :: IMAS_MASS_Sm_154
    real(c_double) :: IMAS_MASS_Sm_155
    real(c_double) :: IMAS_MASS_Sm_156
    real(c_double) :: IMAS_MASS_Sm_157
    real(c_double) :: IMAS_MASS_Sm_158
    real(c_double) :: IMAS_MASS_Sm_159
    real(c_double) :: IMAS_MASS_Sm_160
    real(c_double) :: IMAS_MASS_Sm_161
    real(c_double) :: IMAS_MASS_Sm_162
    real(c_double) :: IMAS_MASS_Sm_163
    real(c_double) :: IMAS_MASS_Sm_164
    real(c_double) :: IMAS_MASS_Sm_165
    real(c_double) :: IMAS_MASS_Eu_130
    real(c_double) :: IMAS_MASS_Eu_131
    real(c_double) :: IMAS_MASS_Eu_132
    real(c_double) :: IMAS_MASS_Eu_133
    real(c_double) :: IMAS_MASS_Eu_134
    real(c_double) :: IMAS_MASS_Eu_135
    real(c_double) :: IMAS_MASS_Eu_136
    real(c_double) :: IMAS_MASS_Eu_137
    real(c_double) :: IMAS_MASS_Eu_138
    real(c_double) :: IMAS_MASS_Eu_139
    real(c_double) :: IMAS_MASS_Eu_140
    real(c_double) :: IMAS_MASS_Eu_141
    real(c_double) :: IMAS_MASS_Eu_142
    real(c_double) :: IMAS_MASS_Eu_143
    real(c_double) :: IMAS_MASS_Eu_144
    real(c_double) :: IMAS_MASS_Eu_145
    real(c_double) :: IMAS_MASS_Eu_146
    real(c_double) :: IMAS_MASS_Eu_147
    real(c_double) :: IMAS_MASS_Eu_148
    real(c_double) :: IMAS_MASS_Eu_149
    real(c_double) :: IMAS_MASS_Eu_150
    real(c_double) :: IMAS_MASS_Eu_151
    real(c_double) :: IMAS_MASS_Eu_152
    real(c_double) :: IMAS_MASS_Eu_153
    real(c_double) :: IMAS_MASS_Eu_154
    real(c_double) :: IMAS_MASS_Eu_155
    real(c_double) :: IMAS_MASS_Eu_156
    real(c_double) :: IMAS_MASS_Eu_157
    real(c_double) :: IMAS_MASS_Eu_158
    real(c_double) :: IMAS_MASS_Eu_159
    real(c_double) :: IMAS_MASS_Eu_160
    real(c_double) :: IMAS_MASS_Eu_161
    real(c_double) :: IMAS_MASS_Eu_162
    real(c_double) :: IMAS_MASS_Eu_163
    real(c_double) :: IMAS_MASS_Eu_164
    real(c_double) :: IMAS_MASS_Eu_165
    real(c_double) :: IMAS_MASS_Eu_166
    real(c_double) :: IMAS_MASS_Eu_167
    real(c_double) :: IMAS_MASS_Gd_134
    real(c_double) :: IMAS_MASS_Gd_135
    real(c_double) :: IMAS_MASS_Gd_136
    real(c_double) :: IMAS_MASS_Gd_137
    real(c_double) :: IMAS_MASS_Gd_138
    real(c_double) :: IMAS_MASS_Gd_139
    real(c_double) :: IMAS_MASS_Gd_140
    real(c_double) :: IMAS_MASS_Gd_141
    real(c_double) :: IMAS_MASS_Gd_142
    real(c_double) :: IMAS_MASS_Gd_143
    real(c_double) :: IMAS_MASS_Gd_144
    real(c_double) :: IMAS_MASS_Gd_145
    real(c_double) :: IMAS_MASS_Gd_146
    real(c_double) :: IMAS_MASS_Gd_147
    real(c_double) :: IMAS_MASS_Gd_148
    real(c_double) :: IMAS_MASS_Gd_149
    real(c_double) :: IMAS_MASS_Gd_150
    real(c_double) :: IMAS_MASS_Gd_151
    real(c_double) :: IMAS_MASS_Gd_152
    real(c_double) :: IMAS_MASS_Gd_153
    real(c_double) :: IMAS_MASS_Gd_154
    real(c_double) :: IMAS_MASS_Gd_155
    real(c_double) :: IMAS_MASS_Gd_156
    real(c_double) :: IMAS_MASS_Gd_157
    real(c_double) :: IMAS_MASS_Gd_158
    real(c_double) :: IMAS_MASS_Gd_159
    real(c_double) :: IMAS_MASS_Gd_160
    real(c_double) :: IMAS_MASS_Gd_161
    real(c_double) :: IMAS_MASS_Gd_162
    real(c_double) :: IMAS_MASS_Gd_163
    real(c_double) :: IMAS_MASS_Gd_164
    real(c_double) :: IMAS_MASS_Gd_165
    real(c_double) :: IMAS_MASS_Gd_166
    real(c_double) :: IMAS_MASS_Gd_167
    real(c_double) :: IMAS_MASS_Gd_168
    real(c_double) :: IMAS_MASS_Gd_169
    real(c_double) :: IMAS_MASS_Tb_136
    real(c_double) :: IMAS_MASS_Tb_137
    real(c_double) :: IMAS_MASS_Tb_138
    real(c_double) :: IMAS_MASS_Tb_139
    real(c_double) :: IMAS_MASS_Tb_140
    real(c_double) :: IMAS_MASS_Tb_141
    real(c_double) :: IMAS_MASS_Tb_142
    real(c_double) :: IMAS_MASS_Tb_143
    real(c_double) :: IMAS_MASS_Tb_144
    real(c_double) :: IMAS_MASS_Tb_145
    real(c_double) :: IMAS_MASS_Tb_146
    real(c_double) :: IMAS_MASS_Tb_147
    real(c_double) :: IMAS_MASS_Tb_148
    real(c_double) :: IMAS_MASS_Tb_149
    real(c_double) :: IMAS_MASS_Tb_150
    real(c_double) :: IMAS_MASS_Tb_151
    real(c_double) :: IMAS_MASS_Tb_152
    real(c_double) :: IMAS_MASS_Tb_153
    real(c_double) :: IMAS_MASS_Tb_154
    real(c_double) :: IMAS_MASS_Tb_155
    real(c_double) :: IMAS_MASS_Tb_156
    real(c_double) :: IMAS_MASS_Tb_157
    real(c_double) :: IMAS_MASS_Tb_158
    real(c_double) :: IMAS_MASS_Tb_159
    real(c_double) :: IMAS_MASS_Tb_160
    real(c_double) :: IMAS_MASS_Tb_161
    real(c_double) :: IMAS_MASS_Tb_162
    real(c_double) :: IMAS_MASS_Tb_163
    real(c_double) :: IMAS_MASS_Tb_164
    real(c_double) :: IMAS_MASS_Tb_165
    real(c_double) :: IMAS_MASS_Tb_166
    real(c_double) :: IMAS_MASS_Tb_167
    real(c_double) :: IMAS_MASS_Tb_168
    real(c_double) :: IMAS_MASS_Tb_169
    real(c_double) :: IMAS_MASS_Tb_170
    real(c_double) :: IMAS_MASS_Tb_171
    real(c_double) :: IMAS_MASS_Dy_138
    real(c_double) :: IMAS_MASS_Dy_139
    real(c_double) :: IMAS_MASS_Dy_140
    real(c_double) :: IMAS_MASS_Dy_141
    real(c_double) :: IMAS_MASS_Dy_142
    real(c_double) :: IMAS_MASS_Dy_143
    real(c_double) :: IMAS_MASS_Dy_144
    real(c_double) :: IMAS_MASS_Dy_145
    real(c_double) :: IMAS_MASS_Dy_146
    real(c_double) :: IMAS_MASS_Dy_147
    real(c_double) :: IMAS_MASS_Dy_148
    real(c_double) :: IMAS_MASS_Dy_149
    real(c_double) :: IMAS_MASS_Dy_150
    real(c_double) :: IMAS_MASS_Dy_151
    real(c_double) :: IMAS_MASS_Dy_152
    real(c_double) :: IMAS_MASS_Dy_153
    real(c_double) :: IMAS_MASS_Dy_154
    real(c_double) :: IMAS_MASS_Dy_155
    real(c_double) :: IMAS_MASS_Dy_156
    real(c_double) :: IMAS_MASS_Dy_157
    real(c_double) :: IMAS_MASS_Dy_158
    real(c_double) :: IMAS_MASS_Dy_159
    real(c_double) :: IMAS_MASS_Dy_160
    real(c_double) :: IMAS_MASS_Dy_161
    real(c_double) :: IMAS_MASS_Dy_162
    real(c_double) :: IMAS_MASS_Dy_163
    real(c_double) :: IMAS_MASS_Dy_164
    real(c_double) :: IMAS_MASS_Dy_165
    real(c_double) :: IMAS_MASS_Dy_166
    real(c_double) :: IMAS_MASS_Dy_167
    real(c_double) :: IMAS_MASS_Dy_168
    real(c_double) :: IMAS_MASS_Dy_169
    real(c_double) :: IMAS_MASS_Dy_170
    real(c_double) :: IMAS_MASS_Dy_171
    real(c_double) :: IMAS_MASS_Dy_172
    real(c_double) :: IMAS_MASS_Dy_173
    real(c_double) :: IMAS_MASS_Ho_140
    real(c_double) :: IMAS_MASS_Ho_141
    real(c_double) :: IMAS_MASS_Ho_142
    real(c_double) :: IMAS_MASS_Ho_143
    real(c_double) :: IMAS_MASS_Ho_144
    real(c_double) :: IMAS_MASS_Ho_145
    real(c_double) :: IMAS_MASS_Ho_146
    real(c_double) :: IMAS_MASS_Ho_147
    real(c_double) :: IMAS_MASS_Ho_148
    real(c_double) :: IMAS_MASS_Ho_149
    real(c_double) :: IMAS_MASS_Ho_150
    real(c_double) :: IMAS_MASS_Ho_151
    real(c_double) :: IMAS_MASS_Ho_152
    real(c_double) :: IMAS_MASS_Ho_153
    real(c_double) :: IMAS_MASS_Ho_154
    real(c_double) :: IMAS_MASS_Ho_155
    real(c_double) :: IMAS_MASS_Ho_156
    real(c_double) :: IMAS_MASS_Ho_157
    real(c_double) :: IMAS_MASS_Ho_158
    real(c_double) :: IMAS_MASS_Ho_159
    real(c_double) :: IMAS_MASS_Ho_160
    real(c_double) :: IMAS_MASS_Ho_161
    real(c_double) :: IMAS_MASS_Ho_162
    real(c_double) :: IMAS_MASS_Ho_163
    real(c_double) :: IMAS_MASS_Ho_164
    real(c_double) :: IMAS_MASS_Ho_165
    real(c_double) :: IMAS_MASS_Ho_166
    real(c_double) :: IMAS_MASS_Ho_167
    real(c_double) :: IMAS_MASS_Ho_168
    real(c_double) :: IMAS_MASS_Ho_169
    real(c_double) :: IMAS_MASS_Ho_170
    real(c_double) :: IMAS_MASS_Ho_171
    real(c_double) :: IMAS_MASS_Ho_172
    real(c_double) :: IMAS_MASS_Ho_173
    real(c_double) :: IMAS_MASS_Ho_174
    real(c_double) :: IMAS_MASS_Ho_175
    real(c_double) :: IMAS_MASS_Er_143
    real(c_double) :: IMAS_MASS_Er_144
    real(c_double) :: IMAS_MASS_Er_145
    real(c_double) :: IMAS_MASS_Er_146
    real(c_double) :: IMAS_MASS_Er_147
    real(c_double) :: IMAS_MASS_Er_148
    real(c_double) :: IMAS_MASS_Er_149
    real(c_double) :: IMAS_MASS_Er_150
    real(c_double) :: IMAS_MASS_Er_151
    real(c_double) :: IMAS_MASS_Er_152
    real(c_double) :: IMAS_MASS_Er_153
    real(c_double) :: IMAS_MASS_Er_154
    real(c_double) :: IMAS_MASS_Er_155
    real(c_double) :: IMAS_MASS_Er_156
    real(c_double) :: IMAS_MASS_Er_157
    real(c_double) :: IMAS_MASS_Er_158
    real(c_double) :: IMAS_MASS_Er_159
    real(c_double) :: IMAS_MASS_Er_160
    real(c_double) :: IMAS_MASS_Er_161
    real(c_double) :: IMAS_MASS_Er_162
    real(c_double) :: IMAS_MASS_Er_163
    real(c_double) :: IMAS_MASS_Er_164
    real(c_double) :: IMAS_MASS_Er_165
    real(c_double) :: IMAS_MASS_Er_166
    real(c_double) :: IMAS_MASS_Er_167
    real(c_double) :: IMAS_MASS_Er_168
    real(c_double) :: IMAS_MASS_Er_169
    real(c_double) :: IMAS_MASS_Er_170
    real(c_double) :: IMAS_MASS_Er_171
    real(c_double) :: IMAS_MASS_Er_172
    real(c_double) :: IMAS_MASS_Er_173
    real(c_double) :: IMAS_MASS_Er_174
    real(c_double) :: IMAS_MASS_Er_175
    real(c_double) :: IMAS_MASS_Er_176
    real(c_double) :: IMAS_MASS_Er_177
    real(c_double) :: IMAS_MASS_Tm_145
    real(c_double) :: IMAS_MASS_Tm_146
    real(c_double) :: IMAS_MASS_Tm_147
    real(c_double) :: IMAS_MASS_Tm_148
    real(c_double) :: IMAS_MASS_Tm_149
    real(c_double) :: IMAS_MASS_Tm_150
    real(c_double) :: IMAS_MASS_Tm_151
    real(c_double) :: IMAS_MASS_Tm_152
    real(c_double) :: IMAS_MASS_Tm_153
    real(c_double) :: IMAS_MASS_Tm_154
    real(c_double) :: IMAS_MASS_Tm_155
    real(c_double) :: IMAS_MASS_Tm_156
    real(c_double) :: IMAS_MASS_Tm_157
    real(c_double) :: IMAS_MASS_Tm_158
    real(c_double) :: IMAS_MASS_Tm_159
    real(c_double) :: IMAS_MASS_Tm_160
    real(c_double) :: IMAS_MASS_Tm_161
    real(c_double) :: IMAS_MASS_Tm_162
    real(c_double) :: IMAS_MASS_Tm_163
    real(c_double) :: IMAS_MASS_Tm_164
    real(c_double) :: IMAS_MASS_Tm_165
    real(c_double) :: IMAS_MASS_Tm_166
    real(c_double) :: IMAS_MASS_Tm_167
    real(c_double) :: IMAS_MASS_Tm_168
    real(c_double) :: IMAS_MASS_Tm_169
    real(c_double) :: IMAS_MASS_Tm_170
    real(c_double) :: IMAS_MASS_Tm_171
    real(c_double) :: IMAS_MASS_Tm_172
    real(c_double) :: IMAS_MASS_Tm_173
    real(c_double) :: IMAS_MASS_Tm_174
    real(c_double) :: IMAS_MASS_Tm_175
    real(c_double) :: IMAS_MASS_Tm_176
    real(c_double) :: IMAS_MASS_Tm_177
    real(c_double) :: IMAS_MASS_Tm_178
    real(c_double) :: IMAS_MASS_Tm_179
    real(c_double) :: IMAS_MASS_Yb_148
    real(c_double) :: IMAS_MASS_Yb_149
    real(c_double) :: IMAS_MASS_Yb_150
    real(c_double) :: IMAS_MASS_Yb_151
    real(c_double) :: IMAS_MASS_Yb_152
    real(c_double) :: IMAS_MASS_Yb_153
    real(c_double) :: IMAS_MASS_Yb_154
    real(c_double) :: IMAS_MASS_Yb_155
    real(c_double) :: IMAS_MASS_Yb_156
    real(c_double) :: IMAS_MASS_Yb_157
    real(c_double) :: IMAS_MASS_Yb_158
    real(c_double) :: IMAS_MASS_Yb_159
    real(c_double) :: IMAS_MASS_Yb_160
    real(c_double) :: IMAS_MASS_Yb_161
    real(c_double) :: IMAS_MASS_Yb_162
    real(c_double) :: IMAS_MASS_Yb_163
    real(c_double) :: IMAS_MASS_Yb_164
    real(c_double) :: IMAS_MASS_Yb_165
    real(c_double) :: IMAS_MASS_Yb_166
    real(c_double) :: IMAS_MASS_Yb_167
    real(c_double) :: IMAS_MASS_Yb_168
    real(c_double) :: IMAS_MASS_Yb_169
    real(c_double) :: IMAS_MASS_Yb_170
    real(c_double) :: IMAS_MASS_Yb_171
    real(c_double) :: IMAS_MASS_Yb_172
    real(c_double) :: IMAS_MASS_Yb_173
    real(c_double) :: IMAS_MASS_Yb_174
    real(c_double) :: IMAS_MASS_Yb_175
    real(c_double) :: IMAS_MASS_Yb_176
    real(c_double) :: IMAS_MASS_Yb_177
    real(c_double) :: IMAS_MASS_Yb_178
    real(c_double) :: IMAS_MASS_Yb_179
    real(c_double) :: IMAS_MASS_Yb_180
    real(c_double) :: IMAS_MASS_Yb_181
    real(c_double) :: IMAS_MASS_Lu_150
    real(c_double) :: IMAS_MASS_Lu_151
    real(c_double) :: IMAS_MASS_Lu_152
    real(c_double) :: IMAS_MASS_Lu_153
    real(c_double) :: IMAS_MASS_Lu_154
    real(c_double) :: IMAS_MASS_Lu_155
    real(c_double) :: IMAS_MASS_Lu_156
    real(c_double) :: IMAS_MASS_Lu_157
    real(c_double) :: IMAS_MASS_Lu_158
    real(c_double) :: IMAS_MASS_Lu_159
    real(c_double) :: IMAS_MASS_Lu_160
    real(c_double) :: IMAS_MASS_Lu_161
    real(c_double) :: IMAS_MASS_Lu_162
    real(c_double) :: IMAS_MASS_Lu_163
    real(c_double) :: IMAS_MASS_Lu_164
    real(c_double) :: IMAS_MASS_Lu_165
    real(c_double) :: IMAS_MASS_Lu_166
    real(c_double) :: IMAS_MASS_Lu_167
    real(c_double) :: IMAS_MASS_Lu_168
    real(c_double) :: IMAS_MASS_Lu_169
    real(c_double) :: IMAS_MASS_Lu_170
    real(c_double) :: IMAS_MASS_Lu_171
    real(c_double) :: IMAS_MASS_Lu_172
    real(c_double) :: IMAS_MASS_Lu_173
    real(c_double) :: IMAS_MASS_Lu_174
    real(c_double) :: IMAS_MASS_Lu_175
    real(c_double) :: IMAS_MASS_Lu_176
    real(c_double) :: IMAS_MASS_Lu_177
    real(c_double) :: IMAS_MASS_Lu_178
    real(c_double) :: IMAS_MASS_Lu_179
    real(c_double) :: IMAS_MASS_Lu_180
    real(c_double) :: IMAS_MASS_Lu_181
    real(c_double) :: IMAS_MASS_Lu_182
    real(c_double) :: IMAS_MASS_Lu_183
    real(c_double) :: IMAS_MASS_Lu_184
    real(c_double) :: IMAS_MASS_Hf_153
    real(c_double) :: IMAS_MASS_Hf_154
    real(c_double) :: IMAS_MASS_Hf_155
    real(c_double) :: IMAS_MASS_Hf_156
    real(c_double) :: IMAS_MASS_Hf_157
    real(c_double) :: IMAS_MASS_Hf_158
    real(c_double) :: IMAS_MASS_Hf_159
    real(c_double) :: IMAS_MASS_Hf_160
    real(c_double) :: IMAS_MASS_Hf_161
    real(c_double) :: IMAS_MASS_Hf_162
    real(c_double) :: IMAS_MASS_Hf_163
    real(c_double) :: IMAS_MASS_Hf_164
    real(c_double) :: IMAS_MASS_Hf_165
    real(c_double) :: IMAS_MASS_Hf_166
    real(c_double) :: IMAS_MASS_Hf_167
    real(c_double) :: IMAS_MASS_Hf_168
    real(c_double) :: IMAS_MASS_Hf_169
    real(c_double) :: IMAS_MASS_Hf_170
    real(c_double) :: IMAS_MASS_Hf_171
    real(c_double) :: IMAS_MASS_Hf_172
    real(c_double) :: IMAS_MASS_Hf_173
    real(c_double) :: IMAS_MASS_Hf_174
    real(c_double) :: IMAS_MASS_Hf_175
    real(c_double) :: IMAS_MASS_Hf_176
    real(c_double) :: IMAS_MASS_Hf_177
    real(c_double) :: IMAS_MASS_Hf_178
    real(c_double) :: IMAS_MASS_Hf_179
    real(c_double) :: IMAS_MASS_Hf_180
    real(c_double) :: IMAS_MASS_Hf_181
    real(c_double) :: IMAS_MASS_Hf_182
    real(c_double) :: IMAS_MASS_Hf_183
    real(c_double) :: IMAS_MASS_Hf_184
    real(c_double) :: IMAS_MASS_Hf_185
    real(c_double) :: IMAS_MASS_Hf_186
    real(c_double) :: IMAS_MASS_Hf_187
    real(c_double) :: IMAS_MASS_Hf_188
    real(c_double) :: IMAS_MASS_Ta_155
    real(c_double) :: IMAS_MASS_Ta_156
    real(c_double) :: IMAS_MASS_Ta_157
    real(c_double) :: IMAS_MASS_Ta_158
    real(c_double) :: IMAS_MASS_Ta_159
    real(c_double) :: IMAS_MASS_Ta_160
    real(c_double) :: IMAS_MASS_Ta_161
    real(c_double) :: IMAS_MASS_Ta_162
    real(c_double) :: IMAS_MASS_Ta_163
    real(c_double) :: IMAS_MASS_Ta_164
    real(c_double) :: IMAS_MASS_Ta_165
    real(c_double) :: IMAS_MASS_Ta_166
    real(c_double) :: IMAS_MASS_Ta_167
    real(c_double) :: IMAS_MASS_Ta_168
    real(c_double) :: IMAS_MASS_Ta_169
    real(c_double) :: IMAS_MASS_Ta_170
    real(c_double) :: IMAS_MASS_Ta_171
    real(c_double) :: IMAS_MASS_Ta_172
    real(c_double) :: IMAS_MASS_Ta_173
    real(c_double) :: IMAS_MASS_Ta_174
    real(c_double) :: IMAS_MASS_Ta_175
    real(c_double) :: IMAS_MASS_Ta_176
    real(c_double) :: IMAS_MASS_Ta_177
    real(c_double) :: IMAS_MASS_Ta_178
    real(c_double) :: IMAS_MASS_Ta_179
    real(c_double) :: IMAS_MASS_Ta_180
    real(c_double) :: IMAS_MASS_Ta_181
    real(c_double) :: IMAS_MASS_Ta_182
    real(c_double) :: IMAS_MASS_Ta_183
    real(c_double) :: IMAS_MASS_Ta_184
    real(c_double) :: IMAS_MASS_Ta_185
    real(c_double) :: IMAS_MASS_Ta_186
    real(c_double) :: IMAS_MASS_Ta_187
    real(c_double) :: IMAS_MASS_Ta_188
    real(c_double) :: IMAS_MASS_Ta_189
    real(c_double) :: IMAS_MASS_Ta_190
    real(c_double) :: IMAS_MASS_W_158
    real(c_double) :: IMAS_MASS_W_159
    real(c_double) :: IMAS_MASS_W_160
    real(c_double) :: IMAS_MASS_W_161
    real(c_double) :: IMAS_MASS_W_162
    real(c_double) :: IMAS_MASS_W_163
    real(c_double) :: IMAS_MASS_W_164
    real(c_double) :: IMAS_MASS_W_165
    real(c_double) :: IMAS_MASS_W_166
    real(c_double) :: IMAS_MASS_W_167
    real(c_double) :: IMAS_MASS_W_168
    real(c_double) :: IMAS_MASS_W_169
    real(c_double) :: IMAS_MASS_W_170
    real(c_double) :: IMAS_MASS_W_171
    real(c_double) :: IMAS_MASS_W_172
    real(c_double) :: IMAS_MASS_W_173
    real(c_double) :: IMAS_MASS_W_174
    real(c_double) :: IMAS_MASS_W_175
    real(c_double) :: IMAS_MASS_W_176
    real(c_double) :: IMAS_MASS_W_177
    real(c_double) :: IMAS_MASS_W_178
    real(c_double) :: IMAS_MASS_W_179
    real(c_double) :: IMAS_MASS_W_180
    real(c_double) :: IMAS_MASS_W_181
    real(c_double) :: IMAS_MASS_W_182
    real(c_double) :: IMAS_MASS_W_183
    real(c_double) :: IMAS_MASS_W_184
    real(c_double) :: IMAS_MASS_W_185
    real(c_double) :: IMAS_MASS_W_186
    real(c_double) :: IMAS_MASS_W_187
    real(c_double) :: IMAS_MASS_W_188
    real(c_double) :: IMAS_MASS_W_189
    real(c_double) :: IMAS_MASS_W_190
    real(c_double) :: IMAS_MASS_W_191
    real(c_double) :: IMAS_MASS_W_192
    real(c_double) :: IMAS_MASS_Re_160
    real(c_double) :: IMAS_MASS_Re_161
    real(c_double) :: IMAS_MASS_Re_162
    real(c_double) :: IMAS_MASS_Re_163
    real(c_double) :: IMAS_MASS_Re_164
    real(c_double) :: IMAS_MASS_Re_165
    real(c_double) :: IMAS_MASS_Re_166
    real(c_double) :: IMAS_MASS_Re_167
    real(c_double) :: IMAS_MASS_Re_168
    real(c_double) :: IMAS_MASS_Re_169
    real(c_double) :: IMAS_MASS_Re_170
    real(c_double) :: IMAS_MASS_Re_171
    real(c_double) :: IMAS_MASS_Re_172
    real(c_double) :: IMAS_MASS_Re_173
    real(c_double) :: IMAS_MASS_Re_174
    real(c_double) :: IMAS_MASS_Re_175
    real(c_double) :: IMAS_MASS_Re_176
    real(c_double) :: IMAS_MASS_Re_177
    real(c_double) :: IMAS_MASS_Re_178
    real(c_double) :: IMAS_MASS_Re_179
    real(c_double) :: IMAS_MASS_Re_180
    real(c_double) :: IMAS_MASS_Re_181
    real(c_double) :: IMAS_MASS_Re_182
    real(c_double) :: IMAS_MASS_Re_183
    real(c_double) :: IMAS_MASS_Re_184
    real(c_double) :: IMAS_MASS_Re_185
    real(c_double) :: IMAS_MASS_Re_186
    real(c_double) :: IMAS_MASS_Re_187
    real(c_double) :: IMAS_MASS_Re_188
    real(c_double) :: IMAS_MASS_Re_189
    real(c_double) :: IMAS_MASS_Re_190
    real(c_double) :: IMAS_MASS_Re_191
    real(c_double) :: IMAS_MASS_Re_192
    real(c_double) :: IMAS_MASS_Re_193
    real(c_double) :: IMAS_MASS_Re_194
    real(c_double) :: IMAS_MASS_Os_162
    real(c_double) :: IMAS_MASS_Os_163
    real(c_double) :: IMAS_MASS_Os_164
    real(c_double) :: IMAS_MASS_Os_165
    real(c_double) :: IMAS_MASS_Os_166
    real(c_double) :: IMAS_MASS_Os_167
    real(c_double) :: IMAS_MASS_Os_168
    real(c_double) :: IMAS_MASS_Os_169
    real(c_double) :: IMAS_MASS_Os_170
    real(c_double) :: IMAS_MASS_Os_171
    real(c_double) :: IMAS_MASS_Os_172
    real(c_double) :: IMAS_MASS_Os_173
    real(c_double) :: IMAS_MASS_Os_174
    real(c_double) :: IMAS_MASS_Os_175
    real(c_double) :: IMAS_MASS_Os_176
    real(c_double) :: IMAS_MASS_Os_177
    real(c_double) :: IMAS_MASS_Os_178
    real(c_double) :: IMAS_MASS_Os_179
    real(c_double) :: IMAS_MASS_Os_180
    real(c_double) :: IMAS_MASS_Os_181
    real(c_double) :: IMAS_MASS_Os_182
    real(c_double) :: IMAS_MASS_Os_183
    real(c_double) :: IMAS_MASS_Os_184
    real(c_double) :: IMAS_MASS_Os_185
    real(c_double) :: IMAS_MASS_Os_186
    real(c_double) :: IMAS_MASS_Os_187
    real(c_double) :: IMAS_MASS_Os_188
    real(c_double) :: IMAS_MASS_Os_189
    real(c_double) :: IMAS_MASS_Os_190
    real(c_double) :: IMAS_MASS_Os_191
    real(c_double) :: IMAS_MASS_Os_192
    real(c_double) :: IMAS_MASS_Os_193
    real(c_double) :: IMAS_MASS_Os_194
    real(c_double) :: IMAS_MASS_Os_195
    real(c_double) :: IMAS_MASS_Os_196
    real(c_double) :: IMAS_MASS_Ir_164
    real(c_double) :: IMAS_MASS_Ir_165
    real(c_double) :: IMAS_MASS_Ir_166
    real(c_double) :: IMAS_MASS_Ir_167
    real(c_double) :: IMAS_MASS_Ir_168
    real(c_double) :: IMAS_MASS_Ir_169
    real(c_double) :: IMAS_MASS_Ir_170
    real(c_double) :: IMAS_MASS_Ir_171
    real(c_double) :: IMAS_MASS_Ir_172
    real(c_double) :: IMAS_MASS_Ir_173
    real(c_double) :: IMAS_MASS_Ir_174
    real(c_double) :: IMAS_MASS_Ir_175
    real(c_double) :: IMAS_MASS_Ir_176
    real(c_double) :: IMAS_MASS_Ir_177
    real(c_double) :: IMAS_MASS_Ir_178
    real(c_double) :: IMAS_MASS_Ir_179
    real(c_double) :: IMAS_MASS_Ir_180
    real(c_double) :: IMAS_MASS_Ir_181
    real(c_double) :: IMAS_MASS_Ir_182
    real(c_double) :: IMAS_MASS_Ir_183
    real(c_double) :: IMAS_MASS_Ir_184
    real(c_double) :: IMAS_MASS_Ir_185
    real(c_double) :: IMAS_MASS_Ir_186
    real(c_double) :: IMAS_MASS_Ir_187
    real(c_double) :: IMAS_MASS_Ir_188
    real(c_double) :: IMAS_MASS_Ir_189
    real(c_double) :: IMAS_MASS_Ir_190
    real(c_double) :: IMAS_MASS_Ir_191
    real(c_double) :: IMAS_MASS_Ir_192
    real(c_double) :: IMAS_MASS_Ir_193
    real(c_double) :: IMAS_MASS_Ir_194
    real(c_double) :: IMAS_MASS_Ir_195
    real(c_double) :: IMAS_MASS_Ir_196
    real(c_double) :: IMAS_MASS_Ir_197
    real(c_double) :: IMAS_MASS_Ir_198
    real(c_double) :: IMAS_MASS_Ir_199
    real(c_double) :: IMAS_MASS_Pt_166
    real(c_double) :: IMAS_MASS_Pt_167
    real(c_double) :: IMAS_MASS_Pt_168
    real(c_double) :: IMAS_MASS_Pt_169
    real(c_double) :: IMAS_MASS_Pt_170
    real(c_double) :: IMAS_MASS_Pt_171
    real(c_double) :: IMAS_MASS_Pt_172
    real(c_double) :: IMAS_MASS_Pt_173
    real(c_double) :: IMAS_MASS_Pt_174
    real(c_double) :: IMAS_MASS_Pt_175
    real(c_double) :: IMAS_MASS_Pt_176
    real(c_double) :: IMAS_MASS_Pt_177
    real(c_double) :: IMAS_MASS_Pt_178
    real(c_double) :: IMAS_MASS_Pt_179
    real(c_double) :: IMAS_MASS_Pt_180
    real(c_double) :: IMAS_MASS_Pt_181
    real(c_double) :: IMAS_MASS_Pt_182
    real(c_double) :: IMAS_MASS_Pt_183
    real(c_double) :: IMAS_MASS_Pt_184
    real(c_double) :: IMAS_MASS_Pt_185
    real(c_double) :: IMAS_MASS_Pt_186
    real(c_double) :: IMAS_MASS_Pt_187
    real(c_double) :: IMAS_MASS_Pt_188
    real(c_double) :: IMAS_MASS_Pt_189
    real(c_double) :: IMAS_MASS_Pt_190
    real(c_double) :: IMAS_MASS_Pt_191
    real(c_double) :: IMAS_MASS_Pt_192
    real(c_double) :: IMAS_MASS_Pt_193
    real(c_double) :: IMAS_MASS_Pt_194
    real(c_double) :: IMAS_MASS_Pt_195
    real(c_double) :: IMAS_MASS_Pt_196
    real(c_double) :: IMAS_MASS_Pt_197
    real(c_double) :: IMAS_MASS_Pt_198
    real(c_double) :: IMAS_MASS_Pt_199
    real(c_double) :: IMAS_MASS_Pt_200
    real(c_double) :: IMAS_MASS_Pt_201
    real(c_double) :: IMAS_MASS_Pt_202
    real(c_double) :: IMAS_MASS_Au_169
    real(c_double) :: IMAS_MASS_Au_170
    real(c_double) :: IMAS_MASS_Au_171
    real(c_double) :: IMAS_MASS_Au_172
    real(c_double) :: IMAS_MASS_Au_173
    real(c_double) :: IMAS_MASS_Au_174
    real(c_double) :: IMAS_MASS_Au_175
    real(c_double) :: IMAS_MASS_Au_176
    real(c_double) :: IMAS_MASS_Au_177
    real(c_double) :: IMAS_MASS_Au_178
    real(c_double) :: IMAS_MASS_Au_179
    real(c_double) :: IMAS_MASS_Au_180
    real(c_double) :: IMAS_MASS_Au_181
    real(c_double) :: IMAS_MASS_Au_182
    real(c_double) :: IMAS_MASS_Au_183
    real(c_double) :: IMAS_MASS_Au_184
    real(c_double) :: IMAS_MASS_Au_185
    real(c_double) :: IMAS_MASS_Au_186
    real(c_double) :: IMAS_MASS_Au_187
    real(c_double) :: IMAS_MASS_Au_188
    real(c_double) :: IMAS_MASS_Au_189
    real(c_double) :: IMAS_MASS_Au_190
    real(c_double) :: IMAS_MASS_Au_191
    real(c_double) :: IMAS_MASS_Au_192
    real(c_double) :: IMAS_MASS_Au_193
    real(c_double) :: IMAS_MASS_Au_194
    real(c_double) :: IMAS_MASS_Au_195
    real(c_double) :: IMAS_MASS_Au_196
    real(c_double) :: IMAS_MASS_Au_197
    real(c_double) :: IMAS_MASS_Au_198
    real(c_double) :: IMAS_MASS_Au_199
    real(c_double) :: IMAS_MASS_Au_200
    real(c_double) :: IMAS_MASS_Au_201
    real(c_double) :: IMAS_MASS_Au_202
    real(c_double) :: IMAS_MASS_Au_203
    real(c_double) :: IMAS_MASS_Au_204
    real(c_double) :: IMAS_MASS_Au_205
    real(c_double) :: IMAS_MASS_Hg_171
    real(c_double) :: IMAS_MASS_Hg_172
    real(c_double) :: IMAS_MASS_Hg_173
    real(c_double) :: IMAS_MASS_Hg_174
    real(c_double) :: IMAS_MASS_Hg_175
    real(c_double) :: IMAS_MASS_Hg_176
    real(c_double) :: IMAS_MASS_Hg_177
    real(c_double) :: IMAS_MASS_Hg_178
    real(c_double) :: IMAS_MASS_Hg_179
    real(c_double) :: IMAS_MASS_Hg_180
    real(c_double) :: IMAS_MASS_Hg_181
    real(c_double) :: IMAS_MASS_Hg_182
    real(c_double) :: IMAS_MASS_Hg_183
    real(c_double) :: IMAS_MASS_Hg_184
    real(c_double) :: IMAS_MASS_Hg_185
    real(c_double) :: IMAS_MASS_Hg_186
    real(c_double) :: IMAS_MASS_Hg_187
    real(c_double) :: IMAS_MASS_Hg_188
    real(c_double) :: IMAS_MASS_Hg_189
    real(c_double) :: IMAS_MASS_Hg_190
    real(c_double) :: IMAS_MASS_Hg_191
    real(c_double) :: IMAS_MASS_Hg_192
    real(c_double) :: IMAS_MASS_Hg_193
    real(c_double) :: IMAS_MASS_Hg_194
    real(c_double) :: IMAS_MASS_Hg_195
    real(c_double) :: IMAS_MASS_Hg_196
    real(c_double) :: IMAS_MASS_Hg_197
    real(c_double) :: IMAS_MASS_Hg_198
    real(c_double) :: IMAS_MASS_Hg_199
    real(c_double) :: IMAS_MASS_Hg_200
    real(c_double) :: IMAS_MASS_Hg_201
    real(c_double) :: IMAS_MASS_Hg_202
    real(c_double) :: IMAS_MASS_Hg_203
    real(c_double) :: IMAS_MASS_Hg_204
    real(c_double) :: IMAS_MASS_Hg_205
    real(c_double) :: IMAS_MASS_Hg_206
    real(c_double) :: IMAS_MASS_Hg_207
    real(c_double) :: IMAS_MASS_Hg_208
    real(c_double) :: IMAS_MASS_Hg_209
    real(c_double) :: IMAS_MASS_Hg_210
    real(c_double) :: IMAS_MASS_Tl_176
    real(c_double) :: IMAS_MASS_Tl_177
    real(c_double) :: IMAS_MASS_Tl_178
    real(c_double) :: IMAS_MASS_Tl_179
    real(c_double) :: IMAS_MASS_Tl_180
    real(c_double) :: IMAS_MASS_Tl_181
    real(c_double) :: IMAS_MASS_Tl_182
    real(c_double) :: IMAS_MASS_Tl_183
    real(c_double) :: IMAS_MASS_Tl_184
    real(c_double) :: IMAS_MASS_Tl_185
    real(c_double) :: IMAS_MASS_Tl_186
    real(c_double) :: IMAS_MASS_Tl_187
    real(c_double) :: IMAS_MASS_Tl_188
    real(c_double) :: IMAS_MASS_Tl_189
    real(c_double) :: IMAS_MASS_Tl_190
    real(c_double) :: IMAS_MASS_Tl_191
    real(c_double) :: IMAS_MASS_Tl_192
    real(c_double) :: IMAS_MASS_Tl_193
    real(c_double) :: IMAS_MASS_Tl_194
    real(c_double) :: IMAS_MASS_Tl_195
    real(c_double) :: IMAS_MASS_Tl_196
    real(c_double) :: IMAS_MASS_Tl_197
    real(c_double) :: IMAS_MASS_Tl_198
    real(c_double) :: IMAS_MASS_Tl_199
    real(c_double) :: IMAS_MASS_Tl_200
    real(c_double) :: IMAS_MASS_Tl_201
    real(c_double) :: IMAS_MASS_Tl_202
    real(c_double) :: IMAS_MASS_Tl_203
    real(c_double) :: IMAS_MASS_Tl_204
    real(c_double) :: IMAS_MASS_Tl_205
    real(c_double) :: IMAS_MASS_Tl_206
    real(c_double) :: IMAS_MASS_Tl_207
    real(c_double) :: IMAS_MASS_Tl_208
    real(c_double) :: IMAS_MASS_Tl_209
    real(c_double) :: IMAS_MASS_Tl_210
    real(c_double) :: IMAS_MASS_Tl_211
    real(c_double) :: IMAS_MASS_Tl_212
    real(c_double) :: IMAS_MASS_Pb_178
    real(c_double) :: IMAS_MASS_Pb_179
    real(c_double) :: IMAS_MASS_Pb_180
    real(c_double) :: IMAS_MASS_Pb_181
    real(c_double) :: IMAS_MASS_Pb_182
    real(c_double) :: IMAS_MASS_Pb_183
    real(c_double) :: IMAS_MASS_Pb_184
    real(c_double) :: IMAS_MASS_Pb_185
    real(c_double) :: IMAS_MASS_Pb_186
    real(c_double) :: IMAS_MASS_Pb_187
    real(c_double) :: IMAS_MASS_Pb_188
    real(c_double) :: IMAS_MASS_Pb_189
    real(c_double) :: IMAS_MASS_Pb_190
    real(c_double) :: IMAS_MASS_Pb_191
    real(c_double) :: IMAS_MASS_Pb_192
    real(c_double) :: IMAS_MASS_Pb_193
    real(c_double) :: IMAS_MASS_Pb_194
    real(c_double) :: IMAS_MASS_Pb_195
    real(c_double) :: IMAS_MASS_Pb_196
    real(c_double) :: IMAS_MASS_Pb_197
    real(c_double) :: IMAS_MASS_Pb_198
    real(c_double) :: IMAS_MASS_Pb_199
    real(c_double) :: IMAS_MASS_Pb_200
    real(c_double) :: IMAS_MASS_Pb_201
    real(c_double) :: IMAS_MASS_Pb_202
    real(c_double) :: IMAS_MASS_Pb_203
    real(c_double) :: IMAS_MASS_Pb_204
    real(c_double) :: IMAS_MASS_Pb_205
    real(c_double) :: IMAS_MASS_Pb_206
    real(c_double) :: IMAS_MASS_Pb_207
    real(c_double) :: IMAS_MASS_Pb_208
    real(c_double) :: IMAS_MASS_Pb_209
    real(c_double) :: IMAS_MASS_Pb_210
    real(c_double) :: IMAS_MASS_Pb_211
    real(c_double) :: IMAS_MASS_Pb_212
    real(c_double) :: IMAS_MASS_Pb_213
    real(c_double) :: IMAS_MASS_Pb_214
    real(c_double) :: IMAS_MASS_Pb_215
    real(c_double) :: IMAS_MASS_Bi_184
    real(c_double) :: IMAS_MASS_Bi_185
    real(c_double) :: IMAS_MASS_Bi_186
    real(c_double) :: IMAS_MASS_Bi_187
    real(c_double) :: IMAS_MASS_Bi_188
    real(c_double) :: IMAS_MASS_Bi_189
    real(c_double) :: IMAS_MASS_Bi_190
    real(c_double) :: IMAS_MASS_Bi_191
    real(c_double) :: IMAS_MASS_Bi_192
    real(c_double) :: IMAS_MASS_Bi_193
    real(c_double) :: IMAS_MASS_Bi_194
    real(c_double) :: IMAS_MASS_Bi_195
    real(c_double) :: IMAS_MASS_Bi_196
    real(c_double) :: IMAS_MASS_Bi_197
    real(c_double) :: IMAS_MASS_Bi_198
    real(c_double) :: IMAS_MASS_Bi_199
    real(c_double) :: IMAS_MASS_Bi_200
    real(c_double) :: IMAS_MASS_Bi_201
    real(c_double) :: IMAS_MASS_Bi_202
    real(c_double) :: IMAS_MASS_Bi_203
    real(c_double) :: IMAS_MASS_Bi_204
    real(c_double) :: IMAS_MASS_Bi_205
    real(c_double) :: IMAS_MASS_Bi_206
    real(c_double) :: IMAS_MASS_Bi_207
    real(c_double) :: IMAS_MASS_Bi_208
    real(c_double) :: IMAS_MASS_Bi_209
    real(c_double) :: IMAS_MASS_Bi_210
    real(c_double) :: IMAS_MASS_Bi_211
    real(c_double) :: IMAS_MASS_Bi_212
    real(c_double) :: IMAS_MASS_Bi_213
    real(c_double) :: IMAS_MASS_Bi_214
    real(c_double) :: IMAS_MASS_Bi_215
    real(c_double) :: IMAS_MASS_Bi_216
    real(c_double) :: IMAS_MASS_Bi_217
    real(c_double) :: IMAS_MASS_Bi_218
    real(c_double) :: IMAS_MASS_Po_188
    real(c_double) :: IMAS_MASS_Po_189
    real(c_double) :: IMAS_MASS_Po_190
    real(c_double) :: IMAS_MASS_Po_191
    real(c_double) :: IMAS_MASS_Po_192
    real(c_double) :: IMAS_MASS_Po_193
    real(c_double) :: IMAS_MASS_Po_194
    real(c_double) :: IMAS_MASS_Po_195
    real(c_double) :: IMAS_MASS_Po_196
    real(c_double) :: IMAS_MASS_Po_197
    real(c_double) :: IMAS_MASS_Po_198
    real(c_double) :: IMAS_MASS_Po_199
    real(c_double) :: IMAS_MASS_Po_200
    real(c_double) :: IMAS_MASS_Po_201
    real(c_double) :: IMAS_MASS_Po_202
    real(c_double) :: IMAS_MASS_Po_203
    real(c_double) :: IMAS_MASS_Po_204
    real(c_double) :: IMAS_MASS_Po_205
    real(c_double) :: IMAS_MASS_Po_206
    real(c_double) :: IMAS_MASS_Po_207
    real(c_double) :: IMAS_MASS_Po_208
    real(c_double) :: IMAS_MASS_Po_209
    real(c_double) :: IMAS_MASS_Po_210
    real(c_double) :: IMAS_MASS_Po_211
    real(c_double) :: IMAS_MASS_Po_212
    real(c_double) :: IMAS_MASS_Po_213
    real(c_double) :: IMAS_MASS_Po_214
    real(c_double) :: IMAS_MASS_Po_215
    real(c_double) :: IMAS_MASS_Po_216
    real(c_double) :: IMAS_MASS_Po_217
    real(c_double) :: IMAS_MASS_Po_218
    real(c_double) :: IMAS_MASS_Po_219
    real(c_double) :: IMAS_MASS_Po_220
    real(c_double) :: IMAS_MASS_At_193
    real(c_double) :: IMAS_MASS_At_194
    real(c_double) :: IMAS_MASS_At_195
    real(c_double) :: IMAS_MASS_At_196
    real(c_double) :: IMAS_MASS_At_197
    real(c_double) :: IMAS_MASS_At_198
    real(c_double) :: IMAS_MASS_At_199
    real(c_double) :: IMAS_MASS_At_200
    real(c_double) :: IMAS_MASS_At_201
    real(c_double) :: IMAS_MASS_At_202
    real(c_double) :: IMAS_MASS_At_203
    real(c_double) :: IMAS_MASS_At_204
    real(c_double) :: IMAS_MASS_At_205
    real(c_double) :: IMAS_MASS_At_206
    real(c_double) :: IMAS_MASS_At_207
    real(c_double) :: IMAS_MASS_At_208
    real(c_double) :: IMAS_MASS_At_209
    real(c_double) :: IMAS_MASS_At_210
    real(c_double) :: IMAS_MASS_At_211
    real(c_double) :: IMAS_MASS_At_212
    real(c_double) :: IMAS_MASS_At_213
    real(c_double) :: IMAS_MASS_At_214
    real(c_double) :: IMAS_MASS_At_215
    real(c_double) :: IMAS_MASS_At_216
    real(c_double) :: IMAS_MASS_At_217
    real(c_double) :: IMAS_MASS_At_218
    real(c_double) :: IMAS_MASS_At_219
    real(c_double) :: IMAS_MASS_At_220
    real(c_double) :: IMAS_MASS_At_221
    real(c_double) :: IMAS_MASS_At_222
    real(c_double) :: IMAS_MASS_At_223
    real(c_double) :: IMAS_MASS_Rn_195
    real(c_double) :: IMAS_MASS_Rn_196
    real(c_double) :: IMAS_MASS_Rn_197
    real(c_double) :: IMAS_MASS_Rn_198
    real(c_double) :: IMAS_MASS_Rn_199
    real(c_double) :: IMAS_MASS_Rn_200
    real(c_double) :: IMAS_MASS_Rn_201
    real(c_double) :: IMAS_MASS_Rn_202
    real(c_double) :: IMAS_MASS_Rn_203
    real(c_double) :: IMAS_MASS_Rn_204
    real(c_double) :: IMAS_MASS_Rn_205
    real(c_double) :: IMAS_MASS_Rn_206
    real(c_double) :: IMAS_MASS_Rn_207
    real(c_double) :: IMAS_MASS_Rn_208
    real(c_double) :: IMAS_MASS_Rn_209
    real(c_double) :: IMAS_MASS_Rn_210
    real(c_double) :: IMAS_MASS_Rn_211
    real(c_double) :: IMAS_MASS_Rn_212
    real(c_double) :: IMAS_MASS_Rn_213
    real(c_double) :: IMAS_MASS_Rn_214
    real(c_double) :: IMAS_MASS_Rn_215
    real(c_double) :: IMAS_MASS_Rn_216
    real(c_double) :: IMAS_MASS_Rn_217
    real(c_double) :: IMAS_MASS_Rn_218
    real(c_double) :: IMAS_MASS_Rn_219
    real(c_double) :: IMAS_MASS_Rn_220
    real(c_double) :: IMAS_MASS_Rn_221
    real(c_double) :: IMAS_MASS_Rn_222
    real(c_double) :: IMAS_MASS_Rn_223
    real(c_double) :: IMAS_MASS_Rn_224
    real(c_double) :: IMAS_MASS_Rn_225
    real(c_double) :: IMAS_MASS_Rn_226
    real(c_double) :: IMAS_MASS_Rn_227
    real(c_double) :: IMAS_MASS_Rn_228
    real(c_double) :: IMAS_MASS_Fr_199
    real(c_double) :: IMAS_MASS_Fr_200
    real(c_double) :: IMAS_MASS_Fr_201
    real(c_double) :: IMAS_MASS_Fr_202
    real(c_double) :: IMAS_MASS_Fr_203
    real(c_double) :: IMAS_MASS_Fr_204
    real(c_double) :: IMAS_MASS_Fr_205
    real(c_double) :: IMAS_MASS_Fr_206
    real(c_double) :: IMAS_MASS_Fr_207
    real(c_double) :: IMAS_MASS_Fr_208
    real(c_double) :: IMAS_MASS_Fr_209
    real(c_double) :: IMAS_MASS_Fr_210
    real(c_double) :: IMAS_MASS_Fr_211
    real(c_double) :: IMAS_MASS_Fr_212
    real(c_double) :: IMAS_MASS_Fr_213
    real(c_double) :: IMAS_MASS_Fr_214
    real(c_double) :: IMAS_MASS_Fr_215
    real(c_double) :: IMAS_MASS_Fr_216
    real(c_double) :: IMAS_MASS_Fr_217
    real(c_double) :: IMAS_MASS_Fr_218
    real(c_double) :: IMAS_MASS_Fr_219
    real(c_double) :: IMAS_MASS_Fr_220
    real(c_double) :: IMAS_MASS_Fr_221
    real(c_double) :: IMAS_MASS_Fr_222
    real(c_double) :: IMAS_MASS_Fr_223
    real(c_double) :: IMAS_MASS_Fr_224
    real(c_double) :: IMAS_MASS_Fr_225
    real(c_double) :: IMAS_MASS_Fr_226
    real(c_double) :: IMAS_MASS_Fr_227
    real(c_double) :: IMAS_MASS_Fr_228
    real(c_double) :: IMAS_MASS_Fr_229
    real(c_double) :: IMAS_MASS_Fr_230
    real(c_double) :: IMAS_MASS_Fr_231
    real(c_double) :: IMAS_MASS_Fr_232
    real(c_double) :: IMAS_MASS_Ra_202
    real(c_double) :: IMAS_MASS_Ra_203
    real(c_double) :: IMAS_MASS_Ra_204
    real(c_double) :: IMAS_MASS_Ra_205
    real(c_double) :: IMAS_MASS_Ra_206
    real(c_double) :: IMAS_MASS_Ra_207
    real(c_double) :: IMAS_MASS_Ra_208
    real(c_double) :: IMAS_MASS_Ra_209
    real(c_double) :: IMAS_MASS_Ra_210
    real(c_double) :: IMAS_MASS_Ra_211
    real(c_double) :: IMAS_MASS_Ra_212
    real(c_double) :: IMAS_MASS_Ra_213
    real(c_double) :: IMAS_MASS_Ra_214
    real(c_double) :: IMAS_MASS_Ra_215
    real(c_double) :: IMAS_MASS_Ra_216
    real(c_double) :: IMAS_MASS_Ra_217
    real(c_double) :: IMAS_MASS_Ra_218
    real(c_double) :: IMAS_MASS_Ra_219
    real(c_double) :: IMAS_MASS_Ra_220
    real(c_double) :: IMAS_MASS_Ra_221
    real(c_double) :: IMAS_MASS_Ra_222
    real(c_double) :: IMAS_MASS_Ra_223
    real(c_double) :: IMAS_MASS_Ra_224
    real(c_double) :: IMAS_MASS_Ra_225
    real(c_double) :: IMAS_MASS_Ra_226
    real(c_double) :: IMAS_MASS_Ra_227
    real(c_double) :: IMAS_MASS_Ra_228
    real(c_double) :: IMAS_MASS_Ra_229
    real(c_double) :: IMAS_MASS_Ra_230
    real(c_double) :: IMAS_MASS_Ra_231
    real(c_double) :: IMAS_MASS_Ra_232
    real(c_double) :: IMAS_MASS_Ra_233
    real(c_double) :: IMAS_MASS_Ra_234
    real(c_double) :: IMAS_MASS_Ac_206
    real(c_double) :: IMAS_MASS_Ac_207
    real(c_double) :: IMAS_MASS_Ac_208
    real(c_double) :: IMAS_MASS_Ac_209
    real(c_double) :: IMAS_MASS_Ac_210
    real(c_double) :: IMAS_MASS_Ac_211
    real(c_double) :: IMAS_MASS_Ac_212
    real(c_double) :: IMAS_MASS_Ac_213
    real(c_double) :: IMAS_MASS_Ac_214
    real(c_double) :: IMAS_MASS_Ac_215
    real(c_double) :: IMAS_MASS_Ac_216
    real(c_double) :: IMAS_MASS_Ac_217
    real(c_double) :: IMAS_MASS_Ac_218
    real(c_double) :: IMAS_MASS_Ac_219
    real(c_double) :: IMAS_MASS_Ac_220
    real(c_double) :: IMAS_MASS_Ac_221
    real(c_double) :: IMAS_MASS_Ac_222
    real(c_double) :: IMAS_MASS_Ac_223
    real(c_double) :: IMAS_MASS_Ac_224
    real(c_double) :: IMAS_MASS_Ac_225
    real(c_double) :: IMAS_MASS_Ac_226
    real(c_double) :: IMAS_MASS_Ac_227
    real(c_double) :: IMAS_MASS_Ac_228
    real(c_double) :: IMAS_MASS_Ac_229
    real(c_double) :: IMAS_MASS_Ac_230
    real(c_double) :: IMAS_MASS_Ac_231
    real(c_double) :: IMAS_MASS_Ac_232
    real(c_double) :: IMAS_MASS_Ac_233
    real(c_double) :: IMAS_MASS_Ac_234
    real(c_double) :: IMAS_MASS_Ac_235
    real(c_double) :: IMAS_MASS_Ac_236
    real(c_double) :: IMAS_MASS_Th_209
    real(c_double) :: IMAS_MASS_Th_210
    real(c_double) :: IMAS_MASS_Th_211
    real(c_double) :: IMAS_MASS_Th_212
    real(c_double) :: IMAS_MASS_Th_213
    real(c_double) :: IMAS_MASS_Th_214
    real(c_double) :: IMAS_MASS_Th_215
    real(c_double) :: IMAS_MASS_Th_216
    real(c_double) :: IMAS_MASS_Th_217
    real(c_double) :: IMAS_MASS_Th_218
    real(c_double) :: IMAS_MASS_Th_219
    real(c_double) :: IMAS_MASS_Th_220
    real(c_double) :: IMAS_MASS_Th_221
    real(c_double) :: IMAS_MASS_Th_222
    real(c_double) :: IMAS_MASS_Th_223
    real(c_double) :: IMAS_MASS_Th_224
    real(c_double) :: IMAS_MASS_Th_225
    real(c_double) :: IMAS_MASS_Th_226
    real(c_double) :: IMAS_MASS_Th_227
    real(c_double) :: IMAS_MASS_Th_228
    real(c_double) :: IMAS_MASS_Th_229
    real(c_double) :: IMAS_MASS_Th_230
    real(c_double) :: IMAS_MASS_Th_231
    real(c_double) :: IMAS_MASS_Th_232
    real(c_double) :: IMAS_MASS_Th_233
    real(c_double) :: IMAS_MASS_Th_234
    real(c_double) :: IMAS_MASS_Th_235
    real(c_double) :: IMAS_MASS_Th_236
    real(c_double) :: IMAS_MASS_Th_237
    real(c_double) :: IMAS_MASS_Th_238
    real(c_double) :: IMAS_MASS_Pa_212
    real(c_double) :: IMAS_MASS_Pa_213
    real(c_double) :: IMAS_MASS_Pa_214
    real(c_double) :: IMAS_MASS_Pa_215
    real(c_double) :: IMAS_MASS_Pa_216
    real(c_double) :: IMAS_MASS_Pa_217
    real(c_double) :: IMAS_MASS_Pa_218
    real(c_double) :: IMAS_MASS_Pa_219
    real(c_double) :: IMAS_MASS_Pa_220
    real(c_double) :: IMAS_MASS_Pa_221
    real(c_double) :: IMAS_MASS_Pa_222
    real(c_double) :: IMAS_MASS_Pa_223
    real(c_double) :: IMAS_MASS_Pa_224
    real(c_double) :: IMAS_MASS_Pa_225
    real(c_double) :: IMAS_MASS_Pa_226
    real(c_double) :: IMAS_MASS_Pa_227
    real(c_double) :: IMAS_MASS_Pa_228
    real(c_double) :: IMAS_MASS_Pa_229
    real(c_double) :: IMAS_MASS_Pa_230
    real(c_double) :: IMAS_MASS_Pa_231
    real(c_double) :: IMAS_MASS_Pa_232
    real(c_double) :: IMAS_MASS_Pa_233
    real(c_double) :: IMAS_MASS_Pa_234
    real(c_double) :: IMAS_MASS_Pa_235
    real(c_double) :: IMAS_MASS_Pa_236
    real(c_double) :: IMAS_MASS_Pa_237
    real(c_double) :: IMAS_MASS_Pa_238
    real(c_double) :: IMAS_MASS_Pa_239
    real(c_double) :: IMAS_MASS_Pa_240
    real(c_double) :: IMAS_MASS_U_217
    real(c_double) :: IMAS_MASS_U_218
    real(c_double) :: IMAS_MASS_U_219
    real(c_double) :: IMAS_MASS_U_220
    real(c_double) :: IMAS_MASS_U_221
    real(c_double) :: IMAS_MASS_U_222
    real(c_double) :: IMAS_MASS_U_223
    real(c_double) :: IMAS_MASS_U_224
    real(c_double) :: IMAS_MASS_U_225
    real(c_double) :: IMAS_MASS_U_226
    real(c_double) :: IMAS_MASS_U_227
    real(c_double) :: IMAS_MASS_U_228
    real(c_double) :: IMAS_MASS_U_229
    real(c_double) :: IMAS_MASS_U_230
    real(c_double) :: IMAS_MASS_U_231
    real(c_double) :: IMAS_MASS_U_232
    real(c_double) :: IMAS_MASS_U_233
    real(c_double) :: IMAS_MASS_U_234
    real(c_double) :: IMAS_MASS_U_235
    real(c_double) :: IMAS_MASS_U_236
    real(c_double) :: IMAS_MASS_U_237
    real(c_double) :: IMAS_MASS_U_238
    real(c_double) :: IMAS_MASS_U_239
    real(c_double) :: IMAS_MASS_U_240
    real(c_double) :: IMAS_MASS_U_241
    real(c_double) :: IMAS_MASS_U_242
    real(c_double) :: IMAS_MASS_Np_225
    real(c_double) :: IMAS_MASS_Np_226
    real(c_double) :: IMAS_MASS_Np_227
    real(c_double) :: IMAS_MASS_Np_228
    real(c_double) :: IMAS_MASS_Np_229
    real(c_double) :: IMAS_MASS_Np_230
    real(c_double) :: IMAS_MASS_Np_231
    real(c_double) :: IMAS_MASS_Np_232
    real(c_double) :: IMAS_MASS_Np_233
    real(c_double) :: IMAS_MASS_Np_234
    real(c_double) :: IMAS_MASS_Np_235
    real(c_double) :: IMAS_MASS_Np_236
    real(c_double) :: IMAS_MASS_Np_237
    real(c_double) :: IMAS_MASS_Np_238
    real(c_double) :: IMAS_MASS_Np_239
    real(c_double) :: IMAS_MASS_Np_240
    real(c_double) :: IMAS_MASS_Np_241
    real(c_double) :: IMAS_MASS_Np_242
    real(c_double) :: IMAS_MASS_Np_243
    real(c_double) :: IMAS_MASS_Np_244
    real(c_double) :: IMAS_MASS_Pu_228
    real(c_double) :: IMAS_MASS_Pu_229
    real(c_double) :: IMAS_MASS_Pu_230
    real(c_double) :: IMAS_MASS_Pu_231
    real(c_double) :: IMAS_MASS_Pu_232
    real(c_double) :: IMAS_MASS_Pu_233
    real(c_double) :: IMAS_MASS_Pu_234
    real(c_double) :: IMAS_MASS_Pu_235
    real(c_double) :: IMAS_MASS_Pu_236
    real(c_double) :: IMAS_MASS_Pu_237
    real(c_double) :: IMAS_MASS_Pu_238
    real(c_double) :: IMAS_MASS_Pu_239
    real(c_double) :: IMAS_MASS_Pu_240
    real(c_double) :: IMAS_MASS_Pu_241
    real(c_double) :: IMAS_MASS_Pu_242
    real(c_double) :: IMAS_MASS_Pu_243
    real(c_double) :: IMAS_MASS_Pu_244
    real(c_double) :: IMAS_MASS_Pu_245
    real(c_double) :: IMAS_MASS_Pu_246
    real(c_double) :: IMAS_MASS_Pu_247
    real(c_double) :: IMAS_MASS_Am_231
    real(c_double) :: IMAS_MASS_Am_232
    real(c_double) :: IMAS_MASS_Am_233
    real(c_double) :: IMAS_MASS_Am_234
    real(c_double) :: IMAS_MASS_Am_235
    real(c_double) :: IMAS_MASS_Am_236
    real(c_double) :: IMAS_MASS_Am_237
    real(c_double) :: IMAS_MASS_Am_238
    real(c_double) :: IMAS_MASS_Am_239
    real(c_double) :: IMAS_MASS_Am_240
    real(c_double) :: IMAS_MASS_Am_241
    real(c_double) :: IMAS_MASS_Am_242
    real(c_double) :: IMAS_MASS_Am_243
    real(c_double) :: IMAS_MASS_Am_244
    real(c_double) :: IMAS_MASS_Am_245
    real(c_double) :: IMAS_MASS_Am_246
    real(c_double) :: IMAS_MASS_Am_247
    real(c_double) :: IMAS_MASS_Am_248
    real(c_double) :: IMAS_MASS_Am_249
    real(c_double) :: IMAS_MASS_Cm_233
    real(c_double) :: IMAS_MASS_Cm_234
    real(c_double) :: IMAS_MASS_Cm_235
    real(c_double) :: IMAS_MASS_Cm_236
    real(c_double) :: IMAS_MASS_Cm_237
    real(c_double) :: IMAS_MASS_Cm_238
    real(c_double) :: IMAS_MASS_Cm_239
    real(c_double) :: IMAS_MASS_Cm_240
    real(c_double) :: IMAS_MASS_Cm_241
    real(c_double) :: IMAS_MASS_Cm_242
    real(c_double) :: IMAS_MASS_Cm_243
    real(c_double) :: IMAS_MASS_Cm_244
    real(c_double) :: IMAS_MASS_Cm_245
    real(c_double) :: IMAS_MASS_Cm_246
    real(c_double) :: IMAS_MASS_Cm_247
    real(c_double) :: IMAS_MASS_Cm_248
    real(c_double) :: IMAS_MASS_Cm_249
    real(c_double) :: IMAS_MASS_Cm_250
    real(c_double) :: IMAS_MASS_Cm_251
    real(c_double) :: IMAS_MASS_Cm_252
    real(c_double) :: IMAS_MASS_Bk_235
    real(c_double) :: IMAS_MASS_Bk_236
    real(c_double) :: IMAS_MASS_Bk_237
    real(c_double) :: IMAS_MASS_Bk_238
    real(c_double) :: IMAS_MASS_Bk_239
    real(c_double) :: IMAS_MASS_Bk_240
    real(c_double) :: IMAS_MASS_Bk_241
    real(c_double) :: IMAS_MASS_Bk_242
    real(c_double) :: IMAS_MASS_Bk_243
    real(c_double) :: IMAS_MASS_Bk_244
    real(c_double) :: IMAS_MASS_Bk_245
    real(c_double) :: IMAS_MASS_Bk_246
    real(c_double) :: IMAS_MASS_Bk_247
    real(c_double) :: IMAS_MASS_Bk_248
    real(c_double) :: IMAS_MASS_Bk_249
    real(c_double) :: IMAS_MASS_Bk_250
    real(c_double) :: IMAS_MASS_Bk_251
    real(c_double) :: IMAS_MASS_Bk_252
    real(c_double) :: IMAS_MASS_Bk_253
    real(c_double) :: IMAS_MASS_Bk_254
    real(c_double) :: IMAS_MASS_Cf_237
    real(c_double) :: IMAS_MASS_Cf_238
    real(c_double) :: IMAS_MASS_Cf_239
    real(c_double) :: IMAS_MASS_Cf_240
    real(c_double) :: IMAS_MASS_Cf_241
    real(c_double) :: IMAS_MASS_Cf_242
    real(c_double) :: IMAS_MASS_Cf_243
    real(c_double) :: IMAS_MASS_Cf_244
    real(c_double) :: IMAS_MASS_Cf_245
    real(c_double) :: IMAS_MASS_Cf_246
    real(c_double) :: IMAS_MASS_Cf_247
    real(c_double) :: IMAS_MASS_Cf_248
    real(c_double) :: IMAS_MASS_Cf_249
    real(c_double) :: IMAS_MASS_Cf_250
    real(c_double) :: IMAS_MASS_Cf_251
    real(c_double) :: IMAS_MASS_Cf_252
    real(c_double) :: IMAS_MASS_Cf_253
    real(c_double) :: IMAS_MASS_Cf_254
    real(c_double) :: IMAS_MASS_Cf_255
    real(c_double) :: IMAS_MASS_Cf_256
    real(c_double) :: IMAS_MASS_Es_240
    real(c_double) :: IMAS_MASS_Es_241
    real(c_double) :: IMAS_MASS_Es_242
    real(c_double) :: IMAS_MASS_Es_243
    real(c_double) :: IMAS_MASS_Es_244
    real(c_double) :: IMAS_MASS_Es_245
    real(c_double) :: IMAS_MASS_Es_246
    real(c_double) :: IMAS_MASS_Es_247
    real(c_double) :: IMAS_MASS_Es_248
    real(c_double) :: IMAS_MASS_Es_249
    real(c_double) :: IMAS_MASS_Es_250
    real(c_double) :: IMAS_MASS_Es_251
    real(c_double) :: IMAS_MASS_Es_252
    real(c_double) :: IMAS_MASS_Es_253
    real(c_double) :: IMAS_MASS_Es_254
    real(c_double) :: IMAS_MASS_Es_255
    real(c_double) :: IMAS_MASS_Es_256
    real(c_double) :: IMAS_MASS_Es_257
    real(c_double) :: IMAS_MASS_Es_258
    real(c_double) :: IMAS_MASS_Fm_242
    real(c_double) :: IMAS_MASS_Fm_243
    real(c_double) :: IMAS_MASS_Fm_244
    real(c_double) :: IMAS_MASS_Fm_245
    real(c_double) :: IMAS_MASS_Fm_246
    real(c_double) :: IMAS_MASS_Fm_247
    real(c_double) :: IMAS_MASS_Fm_248
    real(c_double) :: IMAS_MASS_Fm_249
    real(c_double) :: IMAS_MASS_Fm_250
    real(c_double) :: IMAS_MASS_Fm_251
    real(c_double) :: IMAS_MASS_Fm_252
    real(c_double) :: IMAS_MASS_Fm_253
    real(c_double) :: IMAS_MASS_Fm_254
    real(c_double) :: IMAS_MASS_Fm_255
    real(c_double) :: IMAS_MASS_Fm_256
    real(c_double) :: IMAS_MASS_Fm_257
    real(c_double) :: IMAS_MASS_Fm_258
    real(c_double) :: IMAS_MASS_Fm_259
    real(c_double) :: IMAS_MASS_Fm_260
    real(c_double) :: IMAS_MASS_Md_245
    real(c_double) :: IMAS_MASS_Md_246
    real(c_double) :: IMAS_MASS_Md_247
    real(c_double) :: IMAS_MASS_Md_248
    real(c_double) :: IMAS_MASS_Md_249
    real(c_double) :: IMAS_MASS_Md_250
    real(c_double) :: IMAS_MASS_Md_251
    real(c_double) :: IMAS_MASS_Md_252
    real(c_double) :: IMAS_MASS_Md_253
    real(c_double) :: IMAS_MASS_Md_254
    real(c_double) :: IMAS_MASS_Md_255
    real(c_double) :: IMAS_MASS_Md_256
    real(c_double) :: IMAS_MASS_Md_257
    real(c_double) :: IMAS_MASS_Md_258
    real(c_double) :: IMAS_MASS_Md_259
    real(c_double) :: IMAS_MASS_Md_260
    real(c_double) :: IMAS_MASS_Md_261
    real(c_double) :: IMAS_MASS_Md_262
    real(c_double) :: IMAS_MASS_No_248
    real(c_double) :: IMAS_MASS_No_249
    real(c_double) :: IMAS_MASS_No_250
    real(c_double) :: IMAS_MASS_No_251
    real(c_double) :: IMAS_MASS_No_252
    real(c_double) :: IMAS_MASS_No_253
    real(c_double) :: IMAS_MASS_No_254
    real(c_double) :: IMAS_MASS_No_255
    real(c_double) :: IMAS_MASS_No_256
    real(c_double) :: IMAS_MASS_No_257
    real(c_double) :: IMAS_MASS_No_258
    real(c_double) :: IMAS_MASS_No_259
    real(c_double) :: IMAS_MASS_No_260
    real(c_double) :: IMAS_MASS_No_261
    real(c_double) :: IMAS_MASS_No_262
    real(c_double) :: IMAS_MASS_No_263
    real(c_double) :: IMAS_MASS_No_264
    real(c_double) :: IMAS_MASS_Lr_251
    real(c_double) :: IMAS_MASS_Lr_252
    real(c_double) :: IMAS_MASS_Lr_253
    real(c_double) :: IMAS_MASS_Lr_254
    real(c_double) :: IMAS_MASS_Lr_255
    real(c_double) :: IMAS_MASS_Lr_256
    real(c_double) :: IMAS_MASS_Lr_257
    real(c_double) :: IMAS_MASS_Lr_258
    real(c_double) :: IMAS_MASS_Lr_259
    real(c_double) :: IMAS_MASS_Lr_260
    real(c_double) :: IMAS_MASS_Lr_261
    real(c_double) :: IMAS_MASS_Lr_262
    real(c_double) :: IMAS_MASS_Lr_263
    real(c_double) :: IMAS_MASS_Lr_264
    real(c_double) :: IMAS_MASS_Lr_265
    real(c_double) :: IMAS_MASS_Lr_266
    real(c_double) :: IMAS_MASS_Rf_253
    real(c_double) :: IMAS_MASS_Rf_254
    real(c_double) :: IMAS_MASS_Rf_255
    real(c_double) :: IMAS_MASS_Rf_256
    real(c_double) :: IMAS_MASS_Rf_257
    real(c_double) :: IMAS_MASS_Rf_258
    real(c_double) :: IMAS_MASS_Rf_259
    real(c_double) :: IMAS_MASS_Rf_260
    real(c_double) :: IMAS_MASS_Rf_261
    real(c_double) :: IMAS_MASS_Rf_262
    real(c_double) :: IMAS_MASS_Rf_263
    real(c_double) :: IMAS_MASS_Rf_264
    real(c_double) :: IMAS_MASS_Rf_265
    real(c_double) :: IMAS_MASS_Rf_266
    real(c_double) :: IMAS_MASS_Rf_267
    real(c_double) :: IMAS_MASS_Rf_268
    real(c_double) :: IMAS_MASS_Db_255
    real(c_double) :: IMAS_MASS_Db_256
    real(c_double) :: IMAS_MASS_Db_257
    real(c_double) :: IMAS_MASS_Db_258
    real(c_double) :: IMAS_MASS_Db_259
    real(c_double) :: IMAS_MASS_Db_260
    real(c_double) :: IMAS_MASS_Db_261
    real(c_double) :: IMAS_MASS_Db_262
    real(c_double) :: IMAS_MASS_Db_263
    real(c_double) :: IMAS_MASS_Db_264
    real(c_double) :: IMAS_MASS_Db_265
    real(c_double) :: IMAS_MASS_Db_266
    real(c_double) :: IMAS_MASS_Db_267
    real(c_double) :: IMAS_MASS_Db_268
    real(c_double) :: IMAS_MASS_Db_269
    real(c_double) :: IMAS_MASS_Db_270
    real(c_double) :: IMAS_MASS_Sg_258
    real(c_double) :: IMAS_MASS_Sg_259
    real(c_double) :: IMAS_MASS_Sg_260
    real(c_double) :: IMAS_MASS_Sg_261
    real(c_double) :: IMAS_MASS_Sg_262
    real(c_double) :: IMAS_MASS_Sg_263
    real(c_double) :: IMAS_MASS_Sg_264
    real(c_double) :: IMAS_MASS_Sg_265
    real(c_double) :: IMAS_MASS_Sg_266
    real(c_double) :: IMAS_MASS_Sg_267
    real(c_double) :: IMAS_MASS_Sg_268
    real(c_double) :: IMAS_MASS_Sg_269
    real(c_double) :: IMAS_MASS_Sg_270
    real(c_double) :: IMAS_MASS_Sg_271
    real(c_double) :: IMAS_MASS_Sg_272
    real(c_double) :: IMAS_MASS_Sg_273
    real(c_double) :: IMAS_MASS_Bh_260
    real(c_double) :: IMAS_MASS_Bh_261
    real(c_double) :: IMAS_MASS_Bh_262
    real(c_double) :: IMAS_MASS_Bh_263
    real(c_double) :: IMAS_MASS_Bh_264
    real(c_double) :: IMAS_MASS_Bh_265
    real(c_double) :: IMAS_MASS_Bh_266
    real(c_double) :: IMAS_MASS_Bh_267
    real(c_double) :: IMAS_MASS_Bh_268
    real(c_double) :: IMAS_MASS_Bh_269
    real(c_double) :: IMAS_MASS_Bh_270
    real(c_double) :: IMAS_MASS_Bh_271
    real(c_double) :: IMAS_MASS_Bh_272
    real(c_double) :: IMAS_MASS_Bh_273
    real(c_double) :: IMAS_MASS_Bh_274
    real(c_double) :: IMAS_MASS_Bh_275
    real(c_double) :: IMAS_MASS_Hs_263
    real(c_double) :: IMAS_MASS_Hs_264
    real(c_double) :: IMAS_MASS_Hs_265
    real(c_double) :: IMAS_MASS_Hs_266
    real(c_double) :: IMAS_MASS_Hs_267
    real(c_double) :: IMAS_MASS_Hs_268
    real(c_double) :: IMAS_MASS_Hs_269
    real(c_double) :: IMAS_MASS_Hs_270
    real(c_double) :: IMAS_MASS_Hs_271
    real(c_double) :: IMAS_MASS_Hs_272
    real(c_double) :: IMAS_MASS_Hs_273
    real(c_double) :: IMAS_MASS_Hs_274
    real(c_double) :: IMAS_MASS_Hs_275
    real(c_double) :: IMAS_MASS_Hs_276
    real(c_double) :: IMAS_MASS_Hs_277
    real(c_double) :: IMAS_MASS_Mt_265
    real(c_double) :: IMAS_MASS_Mt_266
    real(c_double) :: IMAS_MASS_Mt_267
    real(c_double) :: IMAS_MASS_Mt_268
    real(c_double) :: IMAS_MASS_Mt_269
    real(c_double) :: IMAS_MASS_Mt_270
    real(c_double) :: IMAS_MASS_Mt_271
    real(c_double) :: IMAS_MASS_Mt_272
    real(c_double) :: IMAS_MASS_Mt_273
    real(c_double) :: IMAS_MASS_Mt_274
    real(c_double) :: IMAS_MASS_Mt_275
    real(c_double) :: IMAS_MASS_Mt_276
    real(c_double) :: IMAS_MASS_Mt_277
    real(c_double) :: IMAS_MASS_Mt_278
    real(c_double) :: IMAS_MASS_Mt_279
    real(c_double) :: IMAS_MASS_Ds_267
    real(c_double) :: IMAS_MASS_Ds_268
    real(c_double) :: IMAS_MASS_Ds_269
    real(c_double) :: IMAS_MASS_Ds_270
    real(c_double) :: IMAS_MASS_Ds_271
    real(c_double) :: IMAS_MASS_Ds_272
    real(c_double) :: IMAS_MASS_Ds_273
    real(c_double) :: IMAS_MASS_Ds_274
    real(c_double) :: IMAS_MASS_Ds_275
    real(c_double) :: IMAS_MASS_Ds_276
    real(c_double) :: IMAS_MASS_Ds_277
    real(c_double) :: IMAS_MASS_Ds_278
    real(c_double) :: IMAS_MASS_Ds_279
    real(c_double) :: IMAS_MASS_Ds_280
    real(c_double) :: IMAS_MASS_Ds_281
    real(c_double) :: IMAS_MASS_Rg_272
    real(c_double) :: IMAS_MASS_Rg_273
    real(c_double) :: IMAS_MASS_Rg_274
    real(c_double) :: IMAS_MASS_Rg_275
    real(c_double) :: IMAS_MASS_Rg_276
    real(c_double) :: IMAS_MASS_Rg_277
    real(c_double) :: IMAS_MASS_Rg_278
    real(c_double) :: IMAS_MASS_Rg_279
    real(c_double) :: IMAS_MASS_Rg_280
    real(c_double) :: IMAS_MASS_Rg_281
    real(c_double) :: IMAS_MASS_Rg_282
    real(c_double) :: IMAS_MASS_Rg_283
    real(c_double) :: IMAS_MASS_Cn_277
    real(c_double) :: IMAS_MASS_Cn_278
    real(c_double) :: IMAS_MASS_Cn_279
    real(c_double) :: IMAS_MASS_Cn_280
    real(c_double) :: IMAS_MASS_Cn_281
    real(c_double) :: IMAS_MASS_Cn_282
    real(c_double) :: IMAS_MASS_Cn_283
    real(c_double) :: IMAS_MASS_Cn_284
    real(c_double) :: IMAS_MASS_Cn_285
    real(c_double) :: IMAS_MASS_Uut_283
    real(c_double) :: IMAS_MASS_Uut_284
    real(c_double) :: IMAS_MASS_Uut_285
    real(c_double) :: IMAS_MASS_Uut_286
    real(c_double) :: IMAS_MASS_Uut_287
    real(c_double) :: IMAS_MASS_Uuq_285
    real(c_double) :: IMAS_MASS_Uuq_286
    real(c_double) :: IMAS_MASS_Uuq_287
    real(c_double) :: IMAS_MASS_Uuq_288
    real(c_double) :: IMAS_MASS_Uuq_289
    real(c_double) :: IMAS_MASS_Uup_287
    real(c_double) :: IMAS_MASS_Uup_288
    real(c_double) :: IMAS_MASS_Uup_289
    real(c_double) :: IMAS_MASS_Uup_290
    real(c_double) :: IMAS_MASS_Uup_291
    real(c_double) :: IMAS_MASS_Uuh_289
    real(c_double) :: IMAS_MASS_Uuh_290
    real(c_double) :: IMAS_MASS_Uuh_291
    real(c_double) :: IMAS_MASS_Uuh_292
    real(c_double) :: IMAS_MASS_Uus_291
    real(c_double) :: IMAS_MASS_Uus_292
    real(c_double) :: IMAS_MASS_Uuo_293
    integer :: version
  end type type_imas_constants

  type(type_imas_constants), public,parameter :: imas_constants = type_imas_constants( &
    IMAS_PI = 3.141592653589793238462643383280, &
    IMAS_C = 2.99792458e8, &
    IMAS_ME = 9.10938291e-31, &
    IMAS_MP = 1.672621777e-27, &
    IMAS_MN = 1.674927351e-27, &
    IMAS_MD = 3.34358348e-27, &
    IMAS_MT = 5.00735630e-27, &
    IMAS_MA = 6.64465675e-27, &
    IMAS_AMU = 1.660538921e-27, &
    IMAS_EV = 1.602176565e-19, &
    IMAS_QE = IMAS_EV, &
    IMAS_MU0 = 4.0e-7 * IMAS_PI, &
    IMAS_EPS0 = 1.0 / (IMAS_MU0 * IMAS_C * IMAS_C), &
    IMAS_AVOGR = 6.02214129e23, &
    IMAS_KBOLT = 1.3806488e-23, &
    IMAS_MASS_H_1 = 1.00782503207 * IMAS_AMU, &
    IMAS_MASS_H_2 = 2.0141017778 * IMAS_AMU, &
    IMAS_MASS_H_3 = 3.0160492777 * IMAS_AMU, &
    IMAS_MASS_H_4 = 4.02781 * IMAS_AMU, &
    IMAS_MASS_H_5 = 5.03531 * IMAS_AMU, &
    IMAS_MASS_H_6 = 6.04494 * IMAS_AMU, &
    IMAS_MASS_H_7 = 7.05275 * IMAS_AMU, &
    IMAS_MASS_He_3 = 3.0160293191 * IMAS_AMU, &
    IMAS_MASS_He_4 = 4.00260325415 * IMAS_AMU, &
    IMAS_MASS_He_5 = 5.012220 * IMAS_AMU, &
    IMAS_MASS_He_6 = 6.0188891 * IMAS_AMU, &
    IMAS_MASS_He_7 = 7.028021 * IMAS_AMU, &
    IMAS_MASS_He_8 = 8.033922 * IMAS_AMU, &
    IMAS_MASS_He_9 = 9.043950 * IMAS_AMU, &
    IMAS_MASS_He_10 = 10.052400 * IMAS_AMU, &
    IMAS_MASS_Li_3 = 3.03078 * IMAS_AMU, &
    IMAS_MASS_Li_4 = 4.02719 * IMAS_AMU, &
    IMAS_MASS_Li_5 = 5.012540 * IMAS_AMU, &
    IMAS_MASS_Li_6 = 6.015122795 * IMAS_AMU, &
    IMAS_MASS_Li_7 = 7.01600455 * IMAS_AMU, &
    IMAS_MASS_Li_8 = 8.02248736 * IMAS_AMU, &
    IMAS_MASS_Li_9 = 9.0267895 * IMAS_AMU, &
    IMAS_MASS_Li_10 = 10.035481 * IMAS_AMU, &
    IMAS_MASS_Li_11 = 11.043798 * IMAS_AMU, &
    IMAS_MASS_Li_12 = 12.05378 * IMAS_AMU, &
    IMAS_MASS_Be_5 = 5.04079 * IMAS_AMU, &
    IMAS_MASS_Be_6 = 6.019726 * IMAS_AMU, &
    IMAS_MASS_Be_7 = 7.01692983 * IMAS_AMU, &
    IMAS_MASS_Be_8 = 8.00530510 * IMAS_AMU, &
    IMAS_MASS_Be_9 = 9.0121822 * IMAS_AMU, &
    IMAS_MASS_Be_10 = 10.0135338 * IMAS_AMU, &
    IMAS_MASS_Be_11 = 11.021658 * IMAS_AMU, &
    IMAS_MASS_Be_12 = 12.026921 * IMAS_AMU, &
    IMAS_MASS_Be_13 = 13.035690 * IMAS_AMU, &
    IMAS_MASS_Be_14 = 14.04289 * IMAS_AMU, &
    IMAS_MASS_Be_15 = 15.05346 * IMAS_AMU, &
    IMAS_MASS_Be_16 = 16.06192 * IMAS_AMU, &
    IMAS_MASS_B_6 = 6.04681 * IMAS_AMU, &
    IMAS_MASS_B_7 = 7.029920 * IMAS_AMU, &
    IMAS_MASS_B_8 = 8.0246072 * IMAS_AMU, &
    IMAS_MASS_B_9 = 9.0133288 * IMAS_AMU, &
    IMAS_MASS_B_10 = 10.0129370 * IMAS_AMU, &
    IMAS_MASS_B_11 = 11.0093054 * IMAS_AMU, &
    IMAS_MASS_B_12 = 12.0143521 * IMAS_AMU, &
    IMAS_MASS_B_13 = 13.0177802 * IMAS_AMU, &
    IMAS_MASS_B_14 = 14.025404 * IMAS_AMU, &
    IMAS_MASS_B_15 = 15.031103 * IMAS_AMU, &
    IMAS_MASS_B_16 = 16.039810 * IMAS_AMU, &
    IMAS_MASS_B_17 = 17.04699 * IMAS_AMU, &
    IMAS_MASS_B_18 = 18.05617 * IMAS_AMU, &
    IMAS_MASS_B_19 = 19.06373 * IMAS_AMU, &
    IMAS_MASS_C_8 = 8.037675 * IMAS_AMU, &
    IMAS_MASS_C_9 = 9.0310367 * IMAS_AMU, &
    IMAS_MASS_C_10 = 10.0168532 * IMAS_AMU, &
    IMAS_MASS_C_11 = 11.0114336 * IMAS_AMU, &
    IMAS_MASS_C_12 = 12.0000000 * IMAS_AMU, &
    IMAS_MASS_C_13 = 13.0033548378 * IMAS_AMU, &
    IMAS_MASS_C_14 = 14.003241989 * IMAS_AMU, &
    IMAS_MASS_C_15 = 15.0105993 * IMAS_AMU, &
    IMAS_MASS_C_16 = 16.014701 * IMAS_AMU, &
    IMAS_MASS_C_17 = 17.022586 * IMAS_AMU, &
    IMAS_MASS_C_18 = 18.026760 * IMAS_AMU, &
    IMAS_MASS_C_19 = 19.03481 * IMAS_AMU, &
    IMAS_MASS_C_20 = 20.04032 * IMAS_AMU, &
    IMAS_MASS_C_21 = 21.04934 * IMAS_AMU, &
    IMAS_MASS_C_22 = 22.05720 * IMAS_AMU, &
    IMAS_MASS_N_10 = 10.04165 * IMAS_AMU, &
    IMAS_MASS_N_11 = 11.026090 * IMAS_AMU, &
    IMAS_MASS_N_12 = 12.0186132 * IMAS_AMU, &
    IMAS_MASS_N_13 = 13.00573861 * IMAS_AMU, &
    IMAS_MASS_N_14 = 14.0030740048 * IMAS_AMU, &
    IMAS_MASS_N_15 = 15.0001088982 * IMAS_AMU, &
    IMAS_MASS_N_16 = 16.0061017 * IMAS_AMU, &
    IMAS_MASS_N_17 = 17.008450 * IMAS_AMU, &
    IMAS_MASS_N_18 = 18.014079 * IMAS_AMU, &
    IMAS_MASS_N_19 = 19.017029 * IMAS_AMU, &
    IMAS_MASS_N_20 = 20.023370 * IMAS_AMU, &
    IMAS_MASS_N_21 = 21.02711 * IMAS_AMU, &
    IMAS_MASS_N_22 = 22.03439 * IMAS_AMU, &
    IMAS_MASS_N_23 = 23.04122 * IMAS_AMU, &
    IMAS_MASS_N_24 = 24.05104 * IMAS_AMU, &
    IMAS_MASS_N_25 = 25.06066 * IMAS_AMU, &
    IMAS_MASS_O_12 = 12.034405 * IMAS_AMU, &
    IMAS_MASS_O_13 = 13.024812 * IMAS_AMU, &
    IMAS_MASS_O_14 = 14.00859625 * IMAS_AMU, &
    IMAS_MASS_O_15 = 15.0030656 * IMAS_AMU, &
    IMAS_MASS_O_16 = 15.99491461956 * IMAS_AMU, &
    IMAS_MASS_O_17 = 16.99913170 * IMAS_AMU, &
    IMAS_MASS_O_18 = 17.9991610 * IMAS_AMU, &
    IMAS_MASS_O_19 = 19.003580 * IMAS_AMU, &
    IMAS_MASS_O_20 = 20.0040767 * IMAS_AMU, &
    IMAS_MASS_O_21 = 21.008656 * IMAS_AMU, &
    IMAS_MASS_O_22 = 22.009970 * IMAS_AMU, &
    IMAS_MASS_O_23 = 23.01569 * IMAS_AMU, &
    IMAS_MASS_O_24 = 24.02047 * IMAS_AMU, &
    IMAS_MASS_O_25 = 25.02946 * IMAS_AMU, &
    IMAS_MASS_O_26 = 26.03834 * IMAS_AMU, &
    IMAS_MASS_O_27 = 27.04826 * IMAS_AMU, &
    IMAS_MASS_O_28 = 28.05781 * IMAS_AMU, &
    IMAS_MASS_F_14 = 14.03506 * IMAS_AMU, &
    IMAS_MASS_F_15 = 15.01801 * IMAS_AMU, &
    IMAS_MASS_F_16 = 16.011466 * IMAS_AMU, &
    IMAS_MASS_F_17 = 17.00209524 * IMAS_AMU, &
    IMAS_MASS_F_18 = 18.0009380 * IMAS_AMU, &
    IMAS_MASS_F_19 = 18.99840322 * IMAS_AMU, &
    IMAS_MASS_F_20 = 19.99998132 * IMAS_AMU, &
    IMAS_MASS_F_21 = 20.9999490 * IMAS_AMU, &
    IMAS_MASS_F_22 = 22.002999 * IMAS_AMU, &
    IMAS_MASS_F_23 = 23.003570 * IMAS_AMU, &
    IMAS_MASS_F_24 = 24.008120 * IMAS_AMU, &
    IMAS_MASS_F_25 = 25.01210 * IMAS_AMU, &
    IMAS_MASS_F_26 = 26.01962 * IMAS_AMU, &
    IMAS_MASS_F_27 = 27.02676 * IMAS_AMU, &
    IMAS_MASS_F_28 = 28.03567 * IMAS_AMU, &
    IMAS_MASS_F_29 = 29.04326 * IMAS_AMU, &
    IMAS_MASS_F_30 = 30.05250 * IMAS_AMU, &
    IMAS_MASS_F_31 = 31.06043 * IMAS_AMU, &
    IMAS_MASS_Ne_16 = 16.025761 * IMAS_AMU, &
    IMAS_MASS_Ne_17 = 17.017672 * IMAS_AMU, &
    IMAS_MASS_Ne_18 = 18.0057082 * IMAS_AMU, &
    IMAS_MASS_Ne_19 = 19.0018802 * IMAS_AMU, &
    IMAS_MASS_Ne_20 = 19.9924401754 * IMAS_AMU, &
    IMAS_MASS_Ne_21 = 20.99384668 * IMAS_AMU, &
    IMAS_MASS_Ne_22 = 21.991385114 * IMAS_AMU, &
    IMAS_MASS_Ne_23 = 22.99446690 * IMAS_AMU, &
    IMAS_MASS_Ne_24 = 23.9936108 * IMAS_AMU, &
    IMAS_MASS_Ne_25 = 24.997737 * IMAS_AMU, &
    IMAS_MASS_Ne_26 = 26.000461 * IMAS_AMU, &
    IMAS_MASS_Ne_27 = 27.00759 * IMAS_AMU, &
    IMAS_MASS_Ne_28 = 28.01207 * IMAS_AMU, &
    IMAS_MASS_Ne_29 = 29.01939 * IMAS_AMU, &
    IMAS_MASS_Ne_30 = 30.02480 * IMAS_AMU, &
    IMAS_MASS_Ne_31 = 31.03311 * IMAS_AMU, &
    IMAS_MASS_Ne_32 = 32.04002 * IMAS_AMU, &
    IMAS_MASS_Ne_33 = 33.04938 * IMAS_AMU, &
    IMAS_MASS_Ne_34 = 34.05703 * IMAS_AMU, &
    IMAS_MASS_Na_18 = 18.025970 * IMAS_AMU, &
    IMAS_MASS_Na_19 = 19.013877 * IMAS_AMU, &
    IMAS_MASS_Na_20 = 20.007351 * IMAS_AMU, &
    IMAS_MASS_Na_21 = 20.9976552 * IMAS_AMU, &
    IMAS_MASS_Na_22 = 21.9944364 * IMAS_AMU, &
    IMAS_MASS_Na_23 = 22.9897692809 * IMAS_AMU, &
    IMAS_MASS_Na_24 = 23.99096278 * IMAS_AMU, &
    IMAS_MASS_Na_25 = 24.9899540 * IMAS_AMU, &
    IMAS_MASS_Na_26 = 25.992633 * IMAS_AMU, &
    IMAS_MASS_Na_27 = 26.994077 * IMAS_AMU, &
    IMAS_MASS_Na_28 = 27.998938 * IMAS_AMU, &
    IMAS_MASS_Na_29 = 29.002861 * IMAS_AMU, &
    IMAS_MASS_Na_30 = 30.008976 * IMAS_AMU, &
    IMAS_MASS_Na_31 = 31.01359 * IMAS_AMU, &
    IMAS_MASS_Na_32 = 32.02047 * IMAS_AMU, &
    IMAS_MASS_Na_33 = 33.02672 * IMAS_AMU, &
    IMAS_MASS_Na_34 = 34.03517 * IMAS_AMU, &
    IMAS_MASS_Na_35 = 35.04249 * IMAS_AMU, &
    IMAS_MASS_Na_36 = 36.05148 * IMAS_AMU, &
    IMAS_MASS_Na_37 = 37.05934 * IMAS_AMU, &
    IMAS_MASS_Mg_19 = 19.03547 * IMAS_AMU, &
    IMAS_MASS_Mg_20 = 20.018863 * IMAS_AMU, &
    IMAS_MASS_Mg_21 = 21.011713 * IMAS_AMU, &
    IMAS_MASS_Mg_22 = 21.9995738 * IMAS_AMU, &
    IMAS_MASS_Mg_23 = 22.9941237 * IMAS_AMU, &
    IMAS_MASS_Mg_24 = 23.985041700 * IMAS_AMU, &
    IMAS_MASS_Mg_25 = 24.98583692 * IMAS_AMU, &
    IMAS_MASS_Mg_26 = 25.982592929 * IMAS_AMU, &
    IMAS_MASS_Mg_27 = 26.98434059 * IMAS_AMU, &
    IMAS_MASS_Mg_28 = 27.9838768 * IMAS_AMU, &
    IMAS_MASS_Mg_29 = 28.988600 * IMAS_AMU, &
    IMAS_MASS_Mg_30 = 29.990434 * IMAS_AMU, &
    IMAS_MASS_Mg_31 = 30.996546 * IMAS_AMU, &
    IMAS_MASS_Mg_32 = 31.998975 * IMAS_AMU, &
    IMAS_MASS_Mg_33 = 33.005254 * IMAS_AMU, &
    IMAS_MASS_Mg_34 = 34.00946 * IMAS_AMU, &
    IMAS_MASS_Mg_35 = 35.01734 * IMAS_AMU, &
    IMAS_MASS_Mg_36 = 36.02300 * IMAS_AMU, &
    IMAS_MASS_Mg_37 = 37.03140 * IMAS_AMU, &
    IMAS_MASS_Mg_38 = 38.03757 * IMAS_AMU, &
    IMAS_MASS_Mg_39 = 39.04677 * IMAS_AMU, &
    IMAS_MASS_Mg_40 = 40.05393 * IMAS_AMU, &
    IMAS_MASS_Al_21 = 21.02804 * IMAS_AMU, &
    IMAS_MASS_Al_22 = 22.01952 * IMAS_AMU, &
    IMAS_MASS_Al_23 = 23.007267 * IMAS_AMU, &
    IMAS_MASS_Al_24 = 23.9999389 * IMAS_AMU, &
    IMAS_MASS_Al_25 = 24.9904281 * IMAS_AMU, &
    IMAS_MASS_Al_26 = 25.98689169 * IMAS_AMU, &
    IMAS_MASS_Al_27 = 26.98153863 * IMAS_AMU, &
    IMAS_MASS_Al_28 = 27.98191031 * IMAS_AMU, &
    IMAS_MASS_Al_29 = 28.9804450 * IMAS_AMU, &
    IMAS_MASS_Al_30 = 29.982960 * IMAS_AMU, &
    IMAS_MASS_Al_31 = 30.983947 * IMAS_AMU, &
    IMAS_MASS_Al_32 = 31.988120 * IMAS_AMU, &
    IMAS_MASS_Al_33 = 32.990840 * IMAS_AMU, &
    IMAS_MASS_Al_34 = 33.99685 * IMAS_AMU, &
    IMAS_MASS_Al_35 = 34.99986 * IMAS_AMU, &
    IMAS_MASS_Al_36 = 36.00621 * IMAS_AMU, &
    IMAS_MASS_Al_37 = 37.01068 * IMAS_AMU, &
    IMAS_MASS_Al_38 = 38.01723 * IMAS_AMU, &
    IMAS_MASS_Al_39 = 39.02297 * IMAS_AMU, &
    IMAS_MASS_Al_40 = 40.03145 * IMAS_AMU, &
    IMAS_MASS_Al_41 = 41.03833 * IMAS_AMU, &
    IMAS_MASS_Al_42 = 42.04689 * IMAS_AMU, &
    IMAS_MASS_Si_22 = 22.03453 * IMAS_AMU, &
    IMAS_MASS_Si_23 = 23.02552 * IMAS_AMU, &
    IMAS_MASS_Si_24 = 24.011546 * IMAS_AMU, &
    IMAS_MASS_Si_25 = 25.004106 * IMAS_AMU, &
    IMAS_MASS_Si_26 = 25.992330 * IMAS_AMU, &
    IMAS_MASS_Si_27 = 26.98670491 * IMAS_AMU, &
    IMAS_MASS_Si_28 = 27.9769265325 * IMAS_AMU, &
    IMAS_MASS_Si_29 = 28.976494700 * IMAS_AMU, &
    IMAS_MASS_Si_30 = 29.97377017 * IMAS_AMU, &
    IMAS_MASS_Si_31 = 30.97536323 * IMAS_AMU, &
    IMAS_MASS_Si_32 = 31.97414808 * IMAS_AMU, &
    IMAS_MASS_Si_33 = 32.978000 * IMAS_AMU, &
    IMAS_MASS_Si_34 = 33.978576 * IMAS_AMU, &
    IMAS_MASS_Si_35 = 34.984580 * IMAS_AMU, &
    IMAS_MASS_Si_36 = 35.98660 * IMAS_AMU, &
    IMAS_MASS_Si_37 = 36.99294 * IMAS_AMU, &
    IMAS_MASS_Si_38 = 37.99563 * IMAS_AMU, &
    IMAS_MASS_Si_39 = 39.00207 * IMAS_AMU, &
    IMAS_MASS_Si_40 = 40.00587 * IMAS_AMU, &
    IMAS_MASS_Si_41 = 41.01456 * IMAS_AMU, &
    IMAS_MASS_Si_42 = 42.01979 * IMAS_AMU, &
    IMAS_MASS_Si_43 = 43.02866 * IMAS_AMU, &
    IMAS_MASS_Si_44 = 44.03526 * IMAS_AMU, &
    IMAS_MASS_P_24 = 24.03435 * IMAS_AMU, &
    IMAS_MASS_P_25 = 25.02026 * IMAS_AMU, &
    IMAS_MASS_P_26 = 26.01178 * IMAS_AMU, &
    IMAS_MASS_P_27 = 26.999230 * IMAS_AMU, &
    IMAS_MASS_P_28 = 27.992315 * IMAS_AMU, &
    IMAS_MASS_P_29 = 28.9818006 * IMAS_AMU, &
    IMAS_MASS_P_30 = 29.9783138 * IMAS_AMU, &
    IMAS_MASS_P_31 = 30.97376163 * IMAS_AMU, &
    IMAS_MASS_P_32 = 31.97390727 * IMAS_AMU, &
    IMAS_MASS_P_33 = 32.9717255 * IMAS_AMU, &
    IMAS_MASS_P_34 = 33.973636 * IMAS_AMU, &
    IMAS_MASS_P_35 = 34.9733141 * IMAS_AMU, &
    IMAS_MASS_P_36 = 35.978260 * IMAS_AMU, &
    IMAS_MASS_P_37 = 36.979610 * IMAS_AMU, &
    IMAS_MASS_P_38 = 37.98416 * IMAS_AMU, &
    IMAS_MASS_P_39 = 38.98618 * IMAS_AMU, &
    IMAS_MASS_P_40 = 39.99130 * IMAS_AMU, &
    IMAS_MASS_P_41 = 40.99434 * IMAS_AMU, &
    IMAS_MASS_P_42 = 42.00101 * IMAS_AMU, &
    IMAS_MASS_P_43 = 43.00619 * IMAS_AMU, &
    IMAS_MASS_P_44 = 44.01299 * IMAS_AMU, &
    IMAS_MASS_P_45 = 45.01922 * IMAS_AMU, &
    IMAS_MASS_P_46 = 46.02738 * IMAS_AMU, &
    IMAS_MASS_S_26 = 26.02788 * IMAS_AMU, &
    IMAS_MASS_S_27 = 27.01883 * IMAS_AMU, &
    IMAS_MASS_S_28 = 28.00437 * IMAS_AMU, &
    IMAS_MASS_S_29 = 28.996610 * IMAS_AMU, &
    IMAS_MASS_S_30 = 29.984903 * IMAS_AMU, &
    IMAS_MASS_S_31 = 30.9795547 * IMAS_AMU, &
    IMAS_MASS_S_32 = 31.97207100 * IMAS_AMU, &
    IMAS_MASS_S_33 = 32.97145876 * IMAS_AMU, &
    IMAS_MASS_S_34 = 33.96786690 * IMAS_AMU, &
    IMAS_MASS_S_35 = 34.96903216 * IMAS_AMU, &
    IMAS_MASS_S_36 = 35.96708076 * IMAS_AMU, &
    IMAS_MASS_S_37 = 36.97112557 * IMAS_AMU, &
    IMAS_MASS_S_38 = 37.971163 * IMAS_AMU, &
    IMAS_MASS_S_39 = 38.975130 * IMAS_AMU, &
    IMAS_MASS_S_40 = 39.97545 * IMAS_AMU, &
    IMAS_MASS_S_41 = 40.97958 * IMAS_AMU, &
    IMAS_MASS_S_42 = 41.98102 * IMAS_AMU, &
    IMAS_MASS_S_43 = 42.98715 * IMAS_AMU, &
    IMAS_MASS_S_44 = 43.99021 * IMAS_AMU, &
    IMAS_MASS_S_45 = 44.99651 * IMAS_AMU, &
    IMAS_MASS_S_46 = 46.00075 * IMAS_AMU, &
    IMAS_MASS_S_47 = 47.00859 * IMAS_AMU, &
    IMAS_MASS_S_48 = 48.01417 * IMAS_AMU, &
    IMAS_MASS_S_49 = 49.02362 * IMAS_AMU, &
    IMAS_MASS_Cl_28 = 28.02851 * IMAS_AMU, &
    IMAS_MASS_Cl_29 = 29.01411 * IMAS_AMU, &
    IMAS_MASS_Cl_30 = 30.00477 * IMAS_AMU, &
    IMAS_MASS_Cl_31 = 30.992410 * IMAS_AMU, &
    IMAS_MASS_Cl_32 = 31.985690 * IMAS_AMU, &
    IMAS_MASS_Cl_33 = 32.9774519 * IMAS_AMU, &
    IMAS_MASS_Cl_34 = 33.97376282 * IMAS_AMU, &
    IMAS_MASS_Cl_35 = 34.96885268 * IMAS_AMU, &
    IMAS_MASS_Cl_36 = 35.96830698 * IMAS_AMU, &
    IMAS_MASS_Cl_37 = 36.96590259 * IMAS_AMU, &
    IMAS_MASS_Cl_38 = 37.96801043 * IMAS_AMU, &
    IMAS_MASS_Cl_39 = 38.9680082 * IMAS_AMU, &
    IMAS_MASS_Cl_40 = 39.970420 * IMAS_AMU, &
    IMAS_MASS_Cl_41 = 40.970680 * IMAS_AMU, &
    IMAS_MASS_Cl_42 = 41.97325 * IMAS_AMU, &
    IMAS_MASS_Cl_43 = 42.97405 * IMAS_AMU, &
    IMAS_MASS_Cl_44 = 43.97828 * IMAS_AMU, &
    IMAS_MASS_Cl_45 = 44.98029 * IMAS_AMU, &
    IMAS_MASS_Cl_46 = 45.98421 * IMAS_AMU, &
    IMAS_MASS_Cl_47 = 46.98871 * IMAS_AMU, &
    IMAS_MASS_Cl_48 = 47.99495 * IMAS_AMU, &
    IMAS_MASS_Cl_49 = 49.00032 * IMAS_AMU, &
    IMAS_MASS_Cl_50 = 50.00784 * IMAS_AMU, &
    IMAS_MASS_Cl_51 = 51.01449 * IMAS_AMU, &
    IMAS_MASS_Ar_30 = 30.02156 * IMAS_AMU, &
    IMAS_MASS_Ar_31 = 31.01212 * IMAS_AMU, &
    IMAS_MASS_Ar_32 = 31.9976380 * IMAS_AMU, &
    IMAS_MASS_Ar_33 = 32.9899257 * IMAS_AMU, &
    IMAS_MASS_Ar_34 = 33.9802712 * IMAS_AMU, &
    IMAS_MASS_Ar_35 = 34.9752576 * IMAS_AMU, &
    IMAS_MASS_Ar_36 = 35.967545106 * IMAS_AMU, &
    IMAS_MASS_Ar_37 = 36.96677632 * IMAS_AMU, &
    IMAS_MASS_Ar_38 = 37.9627324 * IMAS_AMU, &
    IMAS_MASS_Ar_39 = 38.964313 * IMAS_AMU, &
    IMAS_MASS_Ar_40 = 39.9623831225 * IMAS_AMU, &
    IMAS_MASS_Ar_41 = 40.9645006 * IMAS_AMU, &
    IMAS_MASS_Ar_42 = 41.963046 * IMAS_AMU, &
    IMAS_MASS_Ar_43 = 42.965636 * IMAS_AMU, &
    IMAS_MASS_Ar_44 = 43.9649240 * IMAS_AMU, &
    IMAS_MASS_Ar_45 = 44.9680400 * IMAS_AMU, &
    IMAS_MASS_Ar_46 = 45.968090 * IMAS_AMU, &
    IMAS_MASS_Ar_47 = 46.97219 * IMAS_AMU, &
    IMAS_MASS_Ar_48 = 47.97454 * IMAS_AMU, &
    IMAS_MASS_Ar_49 = 48.98052 * IMAS_AMU, &
    IMAS_MASS_Ar_50 = 49.98443 * IMAS_AMU, &
    IMAS_MASS_Ar_51 = 50.99163 * IMAS_AMU, &
    IMAS_MASS_Ar_52 = 51.99678 * IMAS_AMU, &
    IMAS_MASS_Ar_53 = 53.00494 * IMAS_AMU, &
    IMAS_MASS_K_32 = 32.02192 * IMAS_AMU, &
    IMAS_MASS_K_33 = 33.00726 * IMAS_AMU, &
    IMAS_MASS_K_34 = 33.99841 * IMAS_AMU, &
    IMAS_MASS_K_35 = 34.988010 * IMAS_AMU, &
    IMAS_MASS_K_36 = 35.981292 * IMAS_AMU, &
    IMAS_MASS_K_37 = 36.97337589 * IMAS_AMU, &
    IMAS_MASS_K_38 = 37.9690812 * IMAS_AMU, &
    IMAS_MASS_K_39 = 38.96370668 * IMAS_AMU, &
    IMAS_MASS_K_40 = 39.96399848 * IMAS_AMU, &
    IMAS_MASS_K_41 = 40.96182576 * IMAS_AMU, &
    IMAS_MASS_K_42 = 41.96240281 * IMAS_AMU, &
    IMAS_MASS_K_43 = 42.960716 * IMAS_AMU, &
    IMAS_MASS_K_44 = 43.961560 * IMAS_AMU, &
    IMAS_MASS_K_45 = 44.960699 * IMAS_AMU, &
    IMAS_MASS_K_46 = 45.961977 * IMAS_AMU, &
    IMAS_MASS_K_47 = 46.961678 * IMAS_AMU, &
    IMAS_MASS_K_48 = 47.965514 * IMAS_AMU, &
    IMAS_MASS_K_49 = 48.967450 * IMAS_AMU, &
    IMAS_MASS_K_50 = 49.97278 * IMAS_AMU, &
    IMAS_MASS_K_51 = 50.97638 * IMAS_AMU, &
    IMAS_MASS_K_52 = 51.98261 * IMAS_AMU, &
    IMAS_MASS_K_53 = 52.98712 * IMAS_AMU, &
    IMAS_MASS_K_54 = 53.99420 * IMAS_AMU, &
    IMAS_MASS_K_55 = 54.99971 * IMAS_AMU, &
    IMAS_MASS_Ca_34 = 34.01412 * IMAS_AMU, &
    IMAS_MASS_Ca_35 = 35.00494 * IMAS_AMU, &
    IMAS_MASS_Ca_36 = 35.993090 * IMAS_AMU, &
    IMAS_MASS_Ca_37 = 36.985870 * IMAS_AMU, &
    IMAS_MASS_Ca_38 = 37.976318 * IMAS_AMU, &
    IMAS_MASS_Ca_39 = 38.9707197 * IMAS_AMU, &
    IMAS_MASS_Ca_40 = 39.96259098 * IMAS_AMU, &
    IMAS_MASS_Ca_41 = 40.96227806 * IMAS_AMU, &
    IMAS_MASS_Ca_42 = 41.95861801 * IMAS_AMU, &
    IMAS_MASS_Ca_43 = 42.9587666 * IMAS_AMU, &
    IMAS_MASS_Ca_44 = 43.9554818 * IMAS_AMU, &
    IMAS_MASS_Ca_45 = 44.9561866 * IMAS_AMU, &
    IMAS_MASS_Ca_46 = 45.9536926 * IMAS_AMU, &
    IMAS_MASS_Ca_47 = 46.9545460 * IMAS_AMU, &
    IMAS_MASS_Ca_48 = 47.952534 * IMAS_AMU, &
    IMAS_MASS_Ca_49 = 48.955674 * IMAS_AMU, &
    IMAS_MASS_Ca_50 = 49.957519 * IMAS_AMU, &
    IMAS_MASS_Ca_51 = 50.96150 * IMAS_AMU, &
    IMAS_MASS_Ca_52 = 51.96510 * IMAS_AMU, &
    IMAS_MASS_Ca_53 = 52.97005 * IMAS_AMU, &
    IMAS_MASS_Ca_54 = 53.97435 * IMAS_AMU, &
    IMAS_MASS_Ca_55 = 54.98055 * IMAS_AMU, &
    IMAS_MASS_Ca_56 = 55.98557 * IMAS_AMU, &
    IMAS_MASS_Ca_57 = 56.99236 * IMAS_AMU, &
    IMAS_MASS_Sc_36 = 36.01492 * IMAS_AMU, &
    IMAS_MASS_Sc_37 = 37.00305 * IMAS_AMU, &
    IMAS_MASS_Sc_38 = 37.99470 * IMAS_AMU, &
    IMAS_MASS_Sc_39 = 38.984790 * IMAS_AMU, &
    IMAS_MASS_Sc_40 = 39.977967 * IMAS_AMU, &
    IMAS_MASS_Sc_41 = 40.96925113 * IMAS_AMU, &
    IMAS_MASS_Sc_42 = 41.96551643 * IMAS_AMU, &
    IMAS_MASS_Sc_43 = 42.9611507 * IMAS_AMU, &
    IMAS_MASS_Sc_44 = 43.9594028 * IMAS_AMU, &
    IMAS_MASS_Sc_45 = 44.9559119 * IMAS_AMU, &
    IMAS_MASS_Sc_46 = 45.9551719 * IMAS_AMU, &
    IMAS_MASS_Sc_47 = 46.9524075 * IMAS_AMU, &
    IMAS_MASS_Sc_48 = 47.952231 * IMAS_AMU, &
    IMAS_MASS_Sc_49 = 48.950024 * IMAS_AMU, &
    IMAS_MASS_Sc_50 = 49.952188 * IMAS_AMU, &
    IMAS_MASS_Sc_51 = 50.953603 * IMAS_AMU, &
    IMAS_MASS_Sc_52 = 51.95668 * IMAS_AMU, &
    IMAS_MASS_Sc_53 = 52.95961 * IMAS_AMU, &
    IMAS_MASS_Sc_54 = 53.96326 * IMAS_AMU, &
    IMAS_MASS_Sc_55 = 54.96824 * IMAS_AMU, &
    IMAS_MASS_Sc_56 = 55.97287 * IMAS_AMU, &
    IMAS_MASS_Sc_57 = 56.97779 * IMAS_AMU, &
    IMAS_MASS_Sc_58 = 57.98371 * IMAS_AMU, &
    IMAS_MASS_Sc_59 = 58.98922 * IMAS_AMU, &
    IMAS_MASS_Sc_60 = 59.99571 * IMAS_AMU, &
    IMAS_MASS_Ti_38 = 38.00977 * IMAS_AMU, &
    IMAS_MASS_Ti_39 = 39.00161 * IMAS_AMU, &
    IMAS_MASS_Ti_40 = 39.99050 * IMAS_AMU, &
    IMAS_MASS_Ti_41 = 40.98315 * IMAS_AMU, &
    IMAS_MASS_Ti_42 = 41.973031 * IMAS_AMU, &
    IMAS_MASS_Ti_43 = 42.968522 * IMAS_AMU, &
    IMAS_MASS_Ti_44 = 43.9596901 * IMAS_AMU, &
    IMAS_MASS_Ti_45 = 44.9581256 * IMAS_AMU, &
    IMAS_MASS_Ti_46 = 45.9526316 * IMAS_AMU, &
    IMAS_MASS_Ti_47 = 46.9517631 * IMAS_AMU, &
    IMAS_MASS_Ti_48 = 47.9479463 * IMAS_AMU, &
    IMAS_MASS_Ti_49 = 48.9478700 * IMAS_AMU, &
    IMAS_MASS_Ti_50 = 49.9447912 * IMAS_AMU, &
    IMAS_MASS_Ti_51 = 50.9466150 * IMAS_AMU, &
    IMAS_MASS_Ti_52 = 51.946897 * IMAS_AMU, &
    IMAS_MASS_Ti_53 = 52.94973 * IMAS_AMU, &
    IMAS_MASS_Ti_54 = 53.95105 * IMAS_AMU, &
    IMAS_MASS_Ti_55 = 54.95527 * IMAS_AMU, &
    IMAS_MASS_Ti_56 = 55.95820 * IMAS_AMU, &
    IMAS_MASS_Ti_57 = 56.96399 * IMAS_AMU, &
    IMAS_MASS_Ti_58 = 57.96697 * IMAS_AMU, &
    IMAS_MASS_Ti_59 = 58.97293 * IMAS_AMU, &
    IMAS_MASS_Ti_60 = 59.97676 * IMAS_AMU, &
    IMAS_MASS_Ti_61 = 60.98320 * IMAS_AMU, &
    IMAS_MASS_Ti_62 = 61.98749 * IMAS_AMU, &
    IMAS_MASS_Ti_63 = 62.99442 * IMAS_AMU, &
    IMAS_MASS_V_40 = 40.01109 * IMAS_AMU, &
    IMAS_MASS_V_41 = 40.99978 * IMAS_AMU, &
    IMAS_MASS_V_42 = 41.99123 * IMAS_AMU, &
    IMAS_MASS_V_43 = 42.98065 * IMAS_AMU, &
    IMAS_MASS_V_44 = 43.97411 * IMAS_AMU, &
    IMAS_MASS_V_45 = 44.965776 * IMAS_AMU, &
    IMAS_MASS_V_46 = 45.9602005 * IMAS_AMU, &
    IMAS_MASS_V_47 = 46.9549089 * IMAS_AMU, &
    IMAS_MASS_V_48 = 47.9522537 * IMAS_AMU, &
    IMAS_MASS_V_49 = 48.9485161 * IMAS_AMU, &
    IMAS_MASS_V_50 = 49.9471585 * IMAS_AMU, &
    IMAS_MASS_V_51 = 50.9439595 * IMAS_AMU, &
    IMAS_MASS_V_52 = 51.9447755 * IMAS_AMU, &
    IMAS_MASS_V_53 = 52.944338 * IMAS_AMU, &
    IMAS_MASS_V_54 = 53.946440 * IMAS_AMU, &
    IMAS_MASS_V_55 = 54.94723 * IMAS_AMU, &
    IMAS_MASS_V_56 = 55.95053 * IMAS_AMU, &
    IMAS_MASS_V_57 = 56.95256 * IMAS_AMU, &
    IMAS_MASS_V_58 = 57.95683 * IMAS_AMU, &
    IMAS_MASS_V_59 = 58.96021 * IMAS_AMU, &
    IMAS_MASS_V_60 = 59.96503 * IMAS_AMU, &
    IMAS_MASS_V_61 = 60.96848 * IMAS_AMU, &
    IMAS_MASS_V_62 = 61.97378 * IMAS_AMU, &
    IMAS_MASS_V_63 = 62.97755 * IMAS_AMU, &
    IMAS_MASS_V_64 = 63.98347 * IMAS_AMU, &
    IMAS_MASS_V_65 = 64.98792 * IMAS_AMU, &
    IMAS_MASS_Cr_42 = 42.00643 * IMAS_AMU, &
    IMAS_MASS_Cr_43 = 42.99771 * IMAS_AMU, &
    IMAS_MASS_Cr_44 = 43.985550 * IMAS_AMU, &
    IMAS_MASS_Cr_45 = 44.97964 * IMAS_AMU, &
    IMAS_MASS_Cr_46 = 45.968359 * IMAS_AMU, &
    IMAS_MASS_Cr_47 = 46.962900 * IMAS_AMU, &
    IMAS_MASS_Cr_48 = 47.954032 * IMAS_AMU, &
    IMAS_MASS_Cr_49 = 48.9513357 * IMAS_AMU, &
    IMAS_MASS_Cr_50 = 49.9460442 * IMAS_AMU, &
    IMAS_MASS_Cr_51 = 50.9447674 * IMAS_AMU, &
    IMAS_MASS_Cr_52 = 51.9405075 * IMAS_AMU, &
    IMAS_MASS_Cr_53 = 52.9406494 * IMAS_AMU, &
    IMAS_MASS_Cr_54 = 53.9388804 * IMAS_AMU, &
    IMAS_MASS_Cr_55 = 54.9408397 * IMAS_AMU, &
    IMAS_MASS_Cr_56 = 55.9406531 * IMAS_AMU, &
    IMAS_MASS_Cr_57 = 56.9436130 * IMAS_AMU, &
    IMAS_MASS_Cr_58 = 57.94435 * IMAS_AMU, &
    IMAS_MASS_Cr_59 = 58.94859 * IMAS_AMU, &
    IMAS_MASS_Cr_60 = 59.95008 * IMAS_AMU, &
    IMAS_MASS_Cr_61 = 60.95472 * IMAS_AMU, &
    IMAS_MASS_Cr_62 = 61.95661 * IMAS_AMU, &
    IMAS_MASS_Cr_63 = 62.96186 * IMAS_AMU, &
    IMAS_MASS_Cr_64 = 63.96441 * IMAS_AMU, &
    IMAS_MASS_Cr_65 = 64.97016 * IMAS_AMU, &
    IMAS_MASS_Cr_66 = 65.97338 * IMAS_AMU, &
    IMAS_MASS_Cr_67 = 66.97955 * IMAS_AMU, &
    IMAS_MASS_Mn_44 = 44.00687 * IMAS_AMU, &
    IMAS_MASS_Mn_45 = 44.99451 * IMAS_AMU, &
    IMAS_MASS_Mn_46 = 45.98672 * IMAS_AMU, &
    IMAS_MASS_Mn_47 = 46.97610 * IMAS_AMU, &
    IMAS_MASS_Mn_48 = 47.96852 * IMAS_AMU, &
    IMAS_MASS_Mn_49 = 48.959618 * IMAS_AMU, &
    IMAS_MASS_Mn_50 = 49.9542382 * IMAS_AMU, &
    IMAS_MASS_Mn_51 = 50.9482108 * IMAS_AMU, &
    IMAS_MASS_Mn_52 = 51.9455655 * IMAS_AMU, &
    IMAS_MASS_Mn_53 = 52.9412901 * IMAS_AMU, &
    IMAS_MASS_Mn_54 = 53.9403589 * IMAS_AMU, &
    IMAS_MASS_Mn_55 = 54.9380451 * IMAS_AMU, &
    IMAS_MASS_Mn_56 = 55.9389049 * IMAS_AMU, &
    IMAS_MASS_Mn_57 = 56.9382854 * IMAS_AMU, &
    IMAS_MASS_Mn_58 = 57.939980 * IMAS_AMU, &
    IMAS_MASS_Mn_59 = 58.940440 * IMAS_AMU, &
    IMAS_MASS_Mn_60 = 59.942910 * IMAS_AMU, &
    IMAS_MASS_Mn_61 = 60.94465 * IMAS_AMU, &
    IMAS_MASS_Mn_62 = 61.94843 * IMAS_AMU, &
    IMAS_MASS_Mn_63 = 62.95024 * IMAS_AMU, &
    IMAS_MASS_Mn_64 = 63.95425 * IMAS_AMU, &
    IMAS_MASS_Mn_65 = 64.95634 * IMAS_AMU, &
    IMAS_MASS_Mn_66 = 65.96108 * IMAS_AMU, &
    IMAS_MASS_Mn_67 = 66.96414 * IMAS_AMU, &
    IMAS_MASS_Mn_68 = 67.96930 * IMAS_AMU, &
    IMAS_MASS_Mn_69 = 68.97284 * IMAS_AMU, &
    IMAS_MASS_Fe_45 = 45.01458 * IMAS_AMU, &
    IMAS_MASS_Fe_46 = 46.00081 * IMAS_AMU, &
    IMAS_MASS_Fe_47 = 46.99289 * IMAS_AMU, &
    IMAS_MASS_Fe_48 = 47.980500 * IMAS_AMU, &
    IMAS_MASS_Fe_49 = 48.97361 * IMAS_AMU, &
    IMAS_MASS_Fe_50 = 49.962990 * IMAS_AMU, &
    IMAS_MASS_Fe_51 = 50.956820 * IMAS_AMU, &
    IMAS_MASS_Fe_52 = 51.948114 * IMAS_AMU, &
    IMAS_MASS_Fe_53 = 52.9453079 * IMAS_AMU, &
    IMAS_MASS_Fe_54 = 53.9396105 * IMAS_AMU, &
    IMAS_MASS_Fe_55 = 54.9382934 * IMAS_AMU, &
    IMAS_MASS_Fe_56 = 55.9349375 * IMAS_AMU, &
    IMAS_MASS_Fe_57 = 56.9353940 * IMAS_AMU, &
    IMAS_MASS_Fe_58 = 57.9332756 * IMAS_AMU, &
    IMAS_MASS_Fe_59 = 58.9348755 * IMAS_AMU, &
    IMAS_MASS_Fe_60 = 59.934072 * IMAS_AMU, &
    IMAS_MASS_Fe_61 = 60.936745 * IMAS_AMU, &
    IMAS_MASS_Fe_62 = 61.936767 * IMAS_AMU, &
    IMAS_MASS_Fe_63 = 62.94037 * IMAS_AMU, &
    IMAS_MASS_Fe_64 = 63.94120 * IMAS_AMU, &
    IMAS_MASS_Fe_65 = 64.94538 * IMAS_AMU, &
    IMAS_MASS_Fe_66 = 65.94678 * IMAS_AMU, &
    IMAS_MASS_Fe_67 = 66.95095 * IMAS_AMU, &
    IMAS_MASS_Fe_68 = 67.95370 * IMAS_AMU, &
    IMAS_MASS_Fe_69 = 68.95878 * IMAS_AMU, &
    IMAS_MASS_Fe_70 = 69.96146 * IMAS_AMU, &
    IMAS_MASS_Fe_71 = 70.96672 * IMAS_AMU, &
    IMAS_MASS_Fe_72 = 71.96962 * IMAS_AMU, &
    IMAS_MASS_Co_47 = 47.01149 * IMAS_AMU, &
    IMAS_MASS_Co_48 = 48.00176 * IMAS_AMU, &
    IMAS_MASS_Co_49 = 48.98972 * IMAS_AMU, &
    IMAS_MASS_Co_50 = 49.98154 * IMAS_AMU, &
    IMAS_MASS_Co_51 = 50.97072 * IMAS_AMU, &
    IMAS_MASS_Co_52 = 51.963590 * IMAS_AMU, &
    IMAS_MASS_Co_53 = 52.954219 * IMAS_AMU, &
    IMAS_MASS_Co_54 = 53.9484596 * IMAS_AMU, &
    IMAS_MASS_Co_55 = 54.9419990 * IMAS_AMU, &
    IMAS_MASS_Co_56 = 55.9398393 * IMAS_AMU, &
    IMAS_MASS_Co_57 = 56.9362914 * IMAS_AMU, &
    IMAS_MASS_Co_58 = 57.9357528 * IMAS_AMU, &
    IMAS_MASS_Co_59 = 58.9331950 * IMAS_AMU, &
    IMAS_MASS_Co_60 = 59.9338171 * IMAS_AMU, &
    IMAS_MASS_Co_61 = 60.9324758 * IMAS_AMU, &
    IMAS_MASS_Co_62 = 61.934051 * IMAS_AMU, &
    IMAS_MASS_Co_63 = 62.933612 * IMAS_AMU, &
    IMAS_MASS_Co_64 = 63.935810 * IMAS_AMU, &
    IMAS_MASS_Co_65 = 64.936478 * IMAS_AMU, &
    IMAS_MASS_Co_66 = 65.93976 * IMAS_AMU, &
    IMAS_MASS_Co_67 = 66.94089 * IMAS_AMU, &
    IMAS_MASS_Co_68 = 67.94487 * IMAS_AMU, &
    IMAS_MASS_Co_69 = 68.94632 * IMAS_AMU, &
    IMAS_MASS_Co_70 = 69.95100 * IMAS_AMU, &
    IMAS_MASS_Co_71 = 70.95290 * IMAS_AMU, &
    IMAS_MASS_Co_72 = 71.95781 * IMAS_AMU, &
    IMAS_MASS_Co_73 = 72.96024 * IMAS_AMU, &
    IMAS_MASS_Co_74 = 73.96538 * IMAS_AMU, &
    IMAS_MASS_Co_75 = 74.96833 * IMAS_AMU, &
    IMAS_MASS_Ni_48 = 48.01975 * IMAS_AMU, &
    IMAS_MASS_Ni_49 = 49.00966 * IMAS_AMU, &
    IMAS_MASS_Ni_50 = 49.99593 * IMAS_AMU, &
    IMAS_MASS_Ni_51 = 50.98772 * IMAS_AMU, &
    IMAS_MASS_Ni_52 = 51.975680 * IMAS_AMU, &
    IMAS_MASS_Ni_53 = 52.96847 * IMAS_AMU, &
    IMAS_MASS_Ni_54 = 53.957910 * IMAS_AMU, &
    IMAS_MASS_Ni_55 = 54.951330 * IMAS_AMU, &
    IMAS_MASS_Ni_56 = 55.942132 * IMAS_AMU, &
    IMAS_MASS_Ni_57 = 56.9397935 * IMAS_AMU, &
    IMAS_MASS_Ni_58 = 57.9353429 * IMAS_AMU, &
    IMAS_MASS_Ni_59 = 58.9343467 * IMAS_AMU, &
    IMAS_MASS_Ni_60 = 59.9307864 * IMAS_AMU, &
    IMAS_MASS_Ni_61 = 60.9310560 * IMAS_AMU, &
    IMAS_MASS_Ni_62 = 61.9283451 * IMAS_AMU, &
    IMAS_MASS_Ni_63 = 62.9296694 * IMAS_AMU, &
    IMAS_MASS_Ni_64 = 63.9279660 * IMAS_AMU, &
    IMAS_MASS_Ni_65 = 64.9300843 * IMAS_AMU, &
    IMAS_MASS_Ni_66 = 65.9291393 * IMAS_AMU, &
    IMAS_MASS_Ni_67 = 66.931569 * IMAS_AMU, &
    IMAS_MASS_Ni_68 = 67.931869 * IMAS_AMU, &
    IMAS_MASS_Ni_69 = 68.935610 * IMAS_AMU, &
    IMAS_MASS_Ni_70 = 69.93650 * IMAS_AMU, &
    IMAS_MASS_Ni_71 = 70.94074 * IMAS_AMU, &
    IMAS_MASS_Ni_72 = 71.94209 * IMAS_AMU, &
    IMAS_MASS_Ni_73 = 72.94647 * IMAS_AMU, &
    IMAS_MASS_Ni_74 = 73.94807 * IMAS_AMU, &
    IMAS_MASS_Ni_75 = 74.95287 * IMAS_AMU, &
    IMAS_MASS_Ni_76 = 75.95533 * IMAS_AMU, &
    IMAS_MASS_Ni_77 = 76.96055 * IMAS_AMU, &
    IMAS_MASS_Ni_78 = 77.96318 * IMAS_AMU, &
    IMAS_MASS_Cu_52 = 51.99718 * IMAS_AMU, &
    IMAS_MASS_Cu_53 = 52.98555 * IMAS_AMU, &
    IMAS_MASS_Cu_54 = 53.97671 * IMAS_AMU, &
    IMAS_MASS_Cu_55 = 54.96605 * IMAS_AMU, &
    IMAS_MASS_Cu_56 = 55.95856 * IMAS_AMU, &
    IMAS_MASS_Cu_57 = 56.949211 * IMAS_AMU, &
    IMAS_MASS_Cu_58 = 57.9445385 * IMAS_AMU, &
    IMAS_MASS_Cu_59 = 58.9394980 * IMAS_AMU, &
    IMAS_MASS_Cu_60 = 59.9373650 * IMAS_AMU, &
    IMAS_MASS_Cu_61 = 60.9334578 * IMAS_AMU, &
    IMAS_MASS_Cu_62 = 61.932584 * IMAS_AMU, &
    IMAS_MASS_Cu_63 = 62.9295975 * IMAS_AMU, &
    IMAS_MASS_Cu_64 = 63.9297642 * IMAS_AMU, &
    IMAS_MASS_Cu_65 = 64.9277895 * IMAS_AMU, &
    IMAS_MASS_Cu_66 = 65.9288688 * IMAS_AMU, &
    IMAS_MASS_Cu_67 = 66.9277303 * IMAS_AMU, &
    IMAS_MASS_Cu_68 = 67.9296109 * IMAS_AMU, &
    IMAS_MASS_Cu_69 = 68.9294293 * IMAS_AMU, &
    IMAS_MASS_Cu_70 = 69.9323923 * IMAS_AMU, &
    IMAS_MASS_Cu_71 = 70.9326768 * IMAS_AMU, &
    IMAS_MASS_Cu_72 = 71.9358203 * IMAS_AMU, &
    IMAS_MASS_Cu_73 = 72.936675 * IMAS_AMU, &
    IMAS_MASS_Cu_74 = 73.939875 * IMAS_AMU, &
    IMAS_MASS_Cu_75 = 74.94190 * IMAS_AMU, &
    IMAS_MASS_Cu_76 = 75.945275 * IMAS_AMU, &
    IMAS_MASS_Cu_77 = 76.94785 * IMAS_AMU, &
    IMAS_MASS_Cu_78 = 77.95196 * IMAS_AMU, &
    IMAS_MASS_Cu_79 = 78.95456 * IMAS_AMU, &
    IMAS_MASS_Cu_80 = 79.96087 * IMAS_AMU, &
    IMAS_MASS_Zn_54 = 53.99295 * IMAS_AMU, &
    IMAS_MASS_Zn_55 = 54.98398 * IMAS_AMU, &
    IMAS_MASS_Zn_56 = 55.97238 * IMAS_AMU, &
    IMAS_MASS_Zn_57 = 56.96479 * IMAS_AMU, &
    IMAS_MASS_Zn_58 = 57.954590 * IMAS_AMU, &
    IMAS_MASS_Zn_59 = 58.949260 * IMAS_AMU, &
    IMAS_MASS_Zn_60 = 59.941827 * IMAS_AMU, &
    IMAS_MASS_Zn_61 = 60.939511 * IMAS_AMU, &
    IMAS_MASS_Zn_62 = 61.934330 * IMAS_AMU, &
    IMAS_MASS_Zn_63 = 62.9332116 * IMAS_AMU, &
    IMAS_MASS_Zn_64 = 63.9291422 * IMAS_AMU, &
    IMAS_MASS_Zn_65 = 64.9292410 * IMAS_AMU, &
    IMAS_MASS_Zn_66 = 65.9260334 * IMAS_AMU, &
    IMAS_MASS_Zn_67 = 66.9271273 * IMAS_AMU, &
    IMAS_MASS_Zn_68 = 67.9248442 * IMAS_AMU, &
    IMAS_MASS_Zn_69 = 68.9265503 * IMAS_AMU, &
    IMAS_MASS_Zn_70 = 69.9253193 * IMAS_AMU, &
    IMAS_MASS_Zn_71 = 70.927722 * IMAS_AMU, &
    IMAS_MASS_Zn_72 = 71.926858 * IMAS_AMU, &
    IMAS_MASS_Zn_73 = 72.929780 * IMAS_AMU, &
    IMAS_MASS_Zn_74 = 73.929460 * IMAS_AMU, &
    IMAS_MASS_Zn_75 = 74.932940 * IMAS_AMU, &
    IMAS_MASS_Zn_76 = 75.933290 * IMAS_AMU, &
    IMAS_MASS_Zn_77 = 76.93696 * IMAS_AMU, &
    IMAS_MASS_Zn_78 = 77.93844 * IMAS_AMU, &
    IMAS_MASS_Zn_79 = 78.94265 * IMAS_AMU, &
    IMAS_MASS_Zn_80 = 79.94434 * IMAS_AMU, &
    IMAS_MASS_Zn_81 = 80.95048 * IMAS_AMU, &
    IMAS_MASS_Zn_82 = 81.95442 * IMAS_AMU, &
    IMAS_MASS_Zn_83 = 82.96103 * IMAS_AMU, &
    IMAS_MASS_Ga_56 = 55.99491 * IMAS_AMU, &
    IMAS_MASS_Ga_57 = 56.98293 * IMAS_AMU, &
    IMAS_MASS_Ga_58 = 57.97425 * IMAS_AMU, &
    IMAS_MASS_Ga_59 = 58.96337 * IMAS_AMU, &
    IMAS_MASS_Ga_60 = 59.95706 * IMAS_AMU, &
    IMAS_MASS_Ga_61 = 60.949450 * IMAS_AMU, &
    IMAS_MASS_Ga_62 = 61.944175 * IMAS_AMU, &
    IMAS_MASS_Ga_63 = 62.9392942 * IMAS_AMU, &
    IMAS_MASS_Ga_64 = 63.9368387 * IMAS_AMU, &
    IMAS_MASS_Ga_65 = 64.9327348 * IMAS_AMU, &
    IMAS_MASS_Ga_66 = 65.931589 * IMAS_AMU, &
    IMAS_MASS_Ga_67 = 66.9282017 * IMAS_AMU, &
    IMAS_MASS_Ga_68 = 67.9279801 * IMAS_AMU, &
    IMAS_MASS_Ga_69 = 68.9255736 * IMAS_AMU, &
    IMAS_MASS_Ga_70 = 69.9260220 * IMAS_AMU, &
    IMAS_MASS_Ga_71 = 70.9247013 * IMAS_AMU, &
    IMAS_MASS_Ga_72 = 71.9263663 * IMAS_AMU, &
    IMAS_MASS_Ga_73 = 72.9251747 * IMAS_AMU, &
    IMAS_MASS_Ga_74 = 73.926946 * IMAS_AMU, &
    IMAS_MASS_Ga_75 = 74.9265002 * IMAS_AMU, &
    IMAS_MASS_Ga_76 = 75.9288276 * IMAS_AMU, &
    IMAS_MASS_Ga_77 = 76.9291543 * IMAS_AMU, &
    IMAS_MASS_Ga_78 = 77.9316082 * IMAS_AMU, &
    IMAS_MASS_Ga_79 = 78.93289 * IMAS_AMU, &
    IMAS_MASS_Ga_80 = 79.93652 * IMAS_AMU, &
    IMAS_MASS_Ga_81 = 80.93775 * IMAS_AMU, &
    IMAS_MASS_Ga_82 = 81.94299 * IMAS_AMU, &
    IMAS_MASS_Ga_83 = 82.94698 * IMAS_AMU, &
    IMAS_MASS_Ga_84 = 83.95265 * IMAS_AMU, &
    IMAS_MASS_Ga_85 = 84.95700 * IMAS_AMU, &
    IMAS_MASS_Ga_86 = 85.96312 * IMAS_AMU, &
    IMAS_MASS_Ge_58 = 57.99101 * IMAS_AMU, &
    IMAS_MASS_Ge_59 = 58.98175 * IMAS_AMU, &
    IMAS_MASS_Ge_60 = 59.97019 * IMAS_AMU, &
    IMAS_MASS_Ge_61 = 60.96379 * IMAS_AMU, &
    IMAS_MASS_Ge_62 = 61.95465 * IMAS_AMU, &
    IMAS_MASS_Ge_63 = 62.94964 * IMAS_AMU, &
    IMAS_MASS_Ge_64 = 63.941650 * IMAS_AMU, &
    IMAS_MASS_Ge_65 = 64.93944 * IMAS_AMU, &
    IMAS_MASS_Ge_66 = 65.933840 * IMAS_AMU, &
    IMAS_MASS_Ge_67 = 66.932734 * IMAS_AMU, &
    IMAS_MASS_Ge_68 = 67.928094 * IMAS_AMU, &
    IMAS_MASS_Ge_69 = 68.9279645 * IMAS_AMU, &
    IMAS_MASS_Ge_70 = 69.9242474 * IMAS_AMU, &
    IMAS_MASS_Ge_71 = 70.9249510 * IMAS_AMU, &
    IMAS_MASS_Ge_72 = 71.9220758 * IMAS_AMU, &
    IMAS_MASS_Ge_73 = 72.9234589 * IMAS_AMU, &
    IMAS_MASS_Ge_74 = 73.9211778 * IMAS_AMU, &
    IMAS_MASS_Ge_75 = 74.9228589 * IMAS_AMU, &
    IMAS_MASS_Ge_76 = 75.9214026 * IMAS_AMU, &
    IMAS_MASS_Ge_77 = 76.9235486 * IMAS_AMU, &
    IMAS_MASS_Ge_78 = 77.922853 * IMAS_AMU, &
    IMAS_MASS_Ge_79 = 78.92540 * IMAS_AMU, &
    IMAS_MASS_Ge_80 = 79.925370 * IMAS_AMU, &
    IMAS_MASS_Ge_81 = 80.92882 * IMAS_AMU, &
    IMAS_MASS_Ge_82 = 81.92955 * IMAS_AMU, &
    IMAS_MASS_Ge_83 = 82.93462 * IMAS_AMU, &
    IMAS_MASS_Ge_84 = 83.93747 * IMAS_AMU, &
    IMAS_MASS_Ge_85 = 84.94303 * IMAS_AMU, &
    IMAS_MASS_Ge_86 = 85.94649 * IMAS_AMU, &
    IMAS_MASS_Ge_87 = 86.95251 * IMAS_AMU, &
    IMAS_MASS_Ge_88 = 87.95691 * IMAS_AMU, &
    IMAS_MASS_Ge_89 = 88.96383 * IMAS_AMU, &
    IMAS_MASS_As_60 = 59.99313 * IMAS_AMU, &
    IMAS_MASS_As_61 = 60.98062 * IMAS_AMU, &
    IMAS_MASS_As_62 = 61.97320 * IMAS_AMU, &
    IMAS_MASS_As_63 = 62.96369 * IMAS_AMU, &
    IMAS_MASS_As_64 = 63.95757 * IMAS_AMU, &
    IMAS_MASS_As_65 = 64.94956 * IMAS_AMU, &
    IMAS_MASS_As_66 = 65.94471 * IMAS_AMU, &
    IMAS_MASS_As_67 = 66.93919 * IMAS_AMU, &
    IMAS_MASS_As_68 = 67.936770 * IMAS_AMU, &
    IMAS_MASS_As_69 = 68.932270 * IMAS_AMU, &
    IMAS_MASS_As_70 = 69.930920 * IMAS_AMU, &
    IMAS_MASS_As_71 = 70.927112 * IMAS_AMU, &
    IMAS_MASS_As_72 = 71.926752 * IMAS_AMU, &
    IMAS_MASS_As_73 = 72.923825 * IMAS_AMU, &
    IMAS_MASS_As_74 = 73.9239287 * IMAS_AMU, &
    IMAS_MASS_As_75 = 74.9215965 * IMAS_AMU, &
    IMAS_MASS_As_76 = 75.9223940 * IMAS_AMU, &
    IMAS_MASS_As_77 = 76.9206473 * IMAS_AMU, &
    IMAS_MASS_As_78 = 77.921827 * IMAS_AMU, &
    IMAS_MASS_As_79 = 78.920948 * IMAS_AMU, &
    IMAS_MASS_As_80 = 79.922534 * IMAS_AMU, &
    IMAS_MASS_As_81 = 80.922132 * IMAS_AMU, &
    IMAS_MASS_As_82 = 81.92450 * IMAS_AMU, &
    IMAS_MASS_As_83 = 82.92498 * IMAS_AMU, &
    IMAS_MASS_As_84 = 83.92906 * IMAS_AMU, &
    IMAS_MASS_As_85 = 84.93202 * IMAS_AMU, &
    IMAS_MASS_As_86 = 85.93650 * IMAS_AMU, &
    IMAS_MASS_As_87 = 86.93990 * IMAS_AMU, &
    IMAS_MASS_As_88 = 87.94494 * IMAS_AMU, &
    IMAS_MASS_As_89 = 88.94939 * IMAS_AMU, &
    IMAS_MASS_As_90 = 89.95550 * IMAS_AMU, &
    IMAS_MASS_As_91 = 90.96043 * IMAS_AMU, &
    IMAS_MASS_As_92 = 91.96680 * IMAS_AMU, &
    IMAS_MASS_Se_65 = 64.96466 * IMAS_AMU, &
    IMAS_MASS_Se_66 = 65.95521 * IMAS_AMU, &
    IMAS_MASS_Se_67 = 66.95009 * IMAS_AMU, &
    IMAS_MASS_Se_68 = 67.941800 * IMAS_AMU, &
    IMAS_MASS_Se_69 = 68.939560 * IMAS_AMU, &
    IMAS_MASS_Se_70 = 69.933390 * IMAS_AMU, &
    IMAS_MASS_Se_71 = 70.932240 * IMAS_AMU, &
    IMAS_MASS_Se_72 = 71.927112 * IMAS_AMU, &
    IMAS_MASS_Se_73 = 72.926765 * IMAS_AMU, &
    IMAS_MASS_Se_74 = 73.9224764 * IMAS_AMU, &
    IMAS_MASS_Se_75 = 74.9225234 * IMAS_AMU, &
    IMAS_MASS_Se_76 = 75.9192136 * IMAS_AMU, &
    IMAS_MASS_Se_77 = 76.9199140 * IMAS_AMU, &
    IMAS_MASS_Se_78 = 77.9173091 * IMAS_AMU, &
    IMAS_MASS_Se_79 = 78.9184991 * IMAS_AMU, &
    IMAS_MASS_Se_80 = 79.9165213 * IMAS_AMU, &
    IMAS_MASS_Se_81 = 80.9179925 * IMAS_AMU, &
    IMAS_MASS_Se_82 = 81.9166994 * IMAS_AMU, &
    IMAS_MASS_Se_83 = 82.919118 * IMAS_AMU, &
    IMAS_MASS_Se_84 = 83.918462 * IMAS_AMU, &
    IMAS_MASS_Se_85 = 84.922250 * IMAS_AMU, &
    IMAS_MASS_Se_86 = 85.924272 * IMAS_AMU, &
    IMAS_MASS_Se_87 = 86.928520 * IMAS_AMU, &
    IMAS_MASS_Se_88 = 87.931420 * IMAS_AMU, &
    IMAS_MASS_Se_89 = 88.93645 * IMAS_AMU, &
    IMAS_MASS_Se_90 = 89.93996 * IMAS_AMU, &
    IMAS_MASS_Se_91 = 90.94596 * IMAS_AMU, &
    IMAS_MASS_Se_92 = 91.94992 * IMAS_AMU, &
    IMAS_MASS_Se_93 = 92.95629 * IMAS_AMU, &
    IMAS_MASS_Se_94 = 93.96049 * IMAS_AMU, &
    IMAS_MASS_Br_67 = 66.96479 * IMAS_AMU, &
    IMAS_MASS_Br_68 = 67.95852 * IMAS_AMU, &
    IMAS_MASS_Br_69 = 68.95011 * IMAS_AMU, &
    IMAS_MASS_Br_70 = 69.94479 * IMAS_AMU, &
    IMAS_MASS_Br_71 = 70.93874 * IMAS_AMU, &
    IMAS_MASS_Br_72 = 71.936640 * IMAS_AMU, &
    IMAS_MASS_Br_73 = 72.931690 * IMAS_AMU, &
    IMAS_MASS_Br_74 = 73.929891 * IMAS_AMU, &
    IMAS_MASS_Br_75 = 74.925776 * IMAS_AMU, &
    IMAS_MASS_Br_76 = 75.924541 * IMAS_AMU, &
    IMAS_MASS_Br_77 = 76.921379 * IMAS_AMU, &
    IMAS_MASS_Br_78 = 77.921146 * IMAS_AMU, &
    IMAS_MASS_Br_79 = 78.9183371 * IMAS_AMU, &
    IMAS_MASS_Br_80 = 79.9185293 * IMAS_AMU, &
    IMAS_MASS_Br_81 = 80.9162906 * IMAS_AMU, &
    IMAS_MASS_Br_82 = 81.9168041 * IMAS_AMU, &
    IMAS_MASS_Br_83 = 82.915180 * IMAS_AMU, &
    IMAS_MASS_Br_84 = 83.916479 * IMAS_AMU, &
    IMAS_MASS_Br_85 = 84.915608 * IMAS_AMU, &
    IMAS_MASS_Br_86 = 85.918798 * IMAS_AMU, &
    IMAS_MASS_Br_87 = 86.920711 * IMAS_AMU, &
    IMAS_MASS_Br_88 = 87.924070 * IMAS_AMU, &
    IMAS_MASS_Br_89 = 88.926390 * IMAS_AMU, &
    IMAS_MASS_Br_90 = 89.930630 * IMAS_AMU, &
    IMAS_MASS_Br_91 = 90.933970 * IMAS_AMU, &
    IMAS_MASS_Br_92 = 91.939260 * IMAS_AMU, &
    IMAS_MASS_Br_93 = 92.94305 * IMAS_AMU, &
    IMAS_MASS_Br_94 = 93.94868 * IMAS_AMU, &
    IMAS_MASS_Br_95 = 94.95287 * IMAS_AMU, &
    IMAS_MASS_Br_96 = 95.95853 * IMAS_AMU, &
    IMAS_MASS_Br_97 = 96.96280 * IMAS_AMU, &
    IMAS_MASS_Kr_69 = 68.96518 * IMAS_AMU, &
    IMAS_MASS_Kr_70 = 69.95526 * IMAS_AMU, &
    IMAS_MASS_Kr_71 = 70.94963 * IMAS_AMU, &
    IMAS_MASS_Kr_72 = 71.942092 * IMAS_AMU, &
    IMAS_MASS_Kr_73 = 72.939289 * IMAS_AMU, &
    IMAS_MASS_Kr_74 = 73.9330844 * IMAS_AMU, &
    IMAS_MASS_Kr_75 = 74.930946 * IMAS_AMU, &
    IMAS_MASS_Kr_76 = 75.925910 * IMAS_AMU, &
    IMAS_MASS_Kr_77 = 76.9246700 * IMAS_AMU, &
    IMAS_MASS_Kr_78 = 77.9203648 * IMAS_AMU, &
    IMAS_MASS_Kr_79 = 78.920082 * IMAS_AMU, &
    IMAS_MASS_Kr_80 = 79.9163790 * IMAS_AMU, &
    IMAS_MASS_Kr_81 = 80.9165920 * IMAS_AMU, &
    IMAS_MASS_Kr_82 = 81.9134836 * IMAS_AMU, &
    IMAS_MASS_Kr_83 = 82.914136 * IMAS_AMU, &
    IMAS_MASS_Kr_84 = 83.911507 * IMAS_AMU, &
    IMAS_MASS_Kr_85 = 84.9125273 * IMAS_AMU, &
    IMAS_MASS_Kr_86 = 85.91061073 * IMAS_AMU, &
    IMAS_MASS_Kr_87 = 86.91335486 * IMAS_AMU, &
    IMAS_MASS_Kr_88 = 87.914447 * IMAS_AMU, &
    IMAS_MASS_Kr_89 = 88.917630 * IMAS_AMU, &
    IMAS_MASS_Kr_90 = 89.919517 * IMAS_AMU, &
    IMAS_MASS_Kr_91 = 90.923450 * IMAS_AMU, &
    IMAS_MASS_Kr_92 = 91.926156 * IMAS_AMU, &
    IMAS_MASS_Kr_93 = 92.93127 * IMAS_AMU, &
    IMAS_MASS_Kr_94 = 93.93436 * IMAS_AMU, &
    IMAS_MASS_Kr_95 = 94.93984 * IMAS_AMU, &
    IMAS_MASS_Kr_96 = 95.94307 * IMAS_AMU, &
    IMAS_MASS_Kr_97 = 96.94856 * IMAS_AMU, &
    IMAS_MASS_Kr_98 = 97.95191 * IMAS_AMU, &
    IMAS_MASS_Kr_99 = 98.95760 * IMAS_AMU, &
    IMAS_MASS_Kr_100 = 99.96114 * IMAS_AMU, &
    IMAS_MASS_Rb_71 = 70.96532 * IMAS_AMU, &
    IMAS_MASS_Rb_72 = 71.95908 * IMAS_AMU, &
    IMAS_MASS_Rb_73 = 72.95056 * IMAS_AMU, &
    IMAS_MASS_Rb_74 = 73.944265 * IMAS_AMU, &
    IMAS_MASS_Rb_75 = 74.938570 * IMAS_AMU, &
    IMAS_MASS_Rb_76 = 75.9350722 * IMAS_AMU, &
    IMAS_MASS_Rb_77 = 76.930408 * IMAS_AMU, &
    IMAS_MASS_Rb_78 = 77.928141 * IMAS_AMU, &
    IMAS_MASS_Rb_79 = 78.923989 * IMAS_AMU, &
    IMAS_MASS_Rb_80 = 79.922519 * IMAS_AMU, &
    IMAS_MASS_Rb_81 = 80.918996 * IMAS_AMU, &
    IMAS_MASS_Rb_82 = 81.9182086 * IMAS_AMU, &
    IMAS_MASS_Rb_83 = 82.915110 * IMAS_AMU, &
    IMAS_MASS_Rb_84 = 83.914385 * IMAS_AMU, &
    IMAS_MASS_Rb_85 = 84.911789738 * IMAS_AMU, &
    IMAS_MASS_Rb_86 = 85.91116742 * IMAS_AMU, &
    IMAS_MASS_Rb_87 = 86.909180527 * IMAS_AMU, &
    IMAS_MASS_Rb_88 = 87.91131559 * IMAS_AMU, &
    IMAS_MASS_Rb_89 = 88.912278 * IMAS_AMU, &
    IMAS_MASS_Rb_90 = 89.914802 * IMAS_AMU, &
    IMAS_MASS_Rb_91 = 90.916537 * IMAS_AMU, &
    IMAS_MASS_Rb_92 = 91.919729 * IMAS_AMU, &
    IMAS_MASS_Rb_93 = 92.922042 * IMAS_AMU, &
    IMAS_MASS_Rb_94 = 93.926405 * IMAS_AMU, &
    IMAS_MASS_Rb_95 = 94.929303 * IMAS_AMU, &
    IMAS_MASS_Rb_96 = 95.934270 * IMAS_AMU, &
    IMAS_MASS_Rb_97 = 96.937350 * IMAS_AMU, &
    IMAS_MASS_Rb_98 = 97.941790 * IMAS_AMU, &
    IMAS_MASS_Rb_99 = 98.94538 * IMAS_AMU, &
    IMAS_MASS_Rb_100 = 99.94987 * IMAS_AMU, &
    IMAS_MASS_Rb_101 = 100.95320 * IMAS_AMU, &
    IMAS_MASS_Rb_102 = 101.95887 * IMAS_AMU, &
    IMAS_MASS_Sr_73 = 72.96597 * IMAS_AMU, &
    IMAS_MASS_Sr_74 = 73.95631 * IMAS_AMU, &
    IMAS_MASS_Sr_75 = 74.94995 * IMAS_AMU, &
    IMAS_MASS_Sr_76 = 75.941770 * IMAS_AMU, &
    IMAS_MASS_Sr_77 = 76.937945 * IMAS_AMU, &
    IMAS_MASS_Sr_78 = 77.932180 * IMAS_AMU, &
    IMAS_MASS_Sr_79 = 78.929708 * IMAS_AMU, &
    IMAS_MASS_Sr_80 = 79.924521 * IMAS_AMU, &
    IMAS_MASS_Sr_81 = 80.923212 * IMAS_AMU, &
    IMAS_MASS_Sr_82 = 81.918402 * IMAS_AMU, &
    IMAS_MASS_Sr_83 = 82.917557 * IMAS_AMU, &
    IMAS_MASS_Sr_84 = 83.913425 * IMAS_AMU, &
    IMAS_MASS_Sr_85 = 84.912933 * IMAS_AMU, &
    IMAS_MASS_Sr_86 = 85.9092602 * IMAS_AMU, &
    IMAS_MASS_Sr_87 = 86.9088771 * IMAS_AMU, &
    IMAS_MASS_Sr_88 = 87.9056121 * IMAS_AMU, &
    IMAS_MASS_Sr_89 = 88.9074507 * IMAS_AMU, &
    IMAS_MASS_Sr_90 = 89.907738 * IMAS_AMU, &
    IMAS_MASS_Sr_91 = 90.910203 * IMAS_AMU, &
    IMAS_MASS_Sr_92 = 91.911038 * IMAS_AMU, &
    IMAS_MASS_Sr_93 = 92.914026 * IMAS_AMU, &
    IMAS_MASS_Sr_94 = 93.915361 * IMAS_AMU, &
    IMAS_MASS_Sr_95 = 94.919359 * IMAS_AMU, &
    IMAS_MASS_Sr_96 = 95.921697 * IMAS_AMU, &
    IMAS_MASS_Sr_97 = 96.926153 * IMAS_AMU, &
    IMAS_MASS_Sr_98 = 97.928453 * IMAS_AMU, &
    IMAS_MASS_Sr_99 = 98.933240 * IMAS_AMU, &
    IMAS_MASS_Sr_100 = 99.93535 * IMAS_AMU, &
    IMAS_MASS_Sr_101 = 100.94052 * IMAS_AMU, &
    IMAS_MASS_Sr_102 = 101.94302 * IMAS_AMU, &
    IMAS_MASS_Sr_103 = 102.94895 * IMAS_AMU, &
    IMAS_MASS_Sr_104 = 103.95233 * IMAS_AMU, &
    IMAS_MASS_Sr_105 = 104.95858 * IMAS_AMU, &
    IMAS_MASS_Y_76 = 75.95845 * IMAS_AMU, &
    IMAS_MASS_Y_77 = 76.949650 * IMAS_AMU, &
    IMAS_MASS_Y_78 = 77.94361 * IMAS_AMU, &
    IMAS_MASS_Y_79 = 78.93735 * IMAS_AMU, &
    IMAS_MASS_Y_80 = 79.93428 * IMAS_AMU, &
    IMAS_MASS_Y_81 = 80.929130 * IMAS_AMU, &
    IMAS_MASS_Y_82 = 81.92679 * IMAS_AMU, &
    IMAS_MASS_Y_83 = 82.922350 * IMAS_AMU, &
    IMAS_MASS_Y_84 = 83.92039 * IMAS_AMU, &
    IMAS_MASS_Y_85 = 84.916433 * IMAS_AMU, &
    IMAS_MASS_Y_86 = 85.914886 * IMAS_AMU, &
    IMAS_MASS_Y_87 = 86.9108757 * IMAS_AMU, &
    IMAS_MASS_Y_88 = 87.9095011 * IMAS_AMU, &
    IMAS_MASS_Y_89 = 88.9058483 * IMAS_AMU, &
    IMAS_MASS_Y_90 = 89.9071519 * IMAS_AMU, &
    IMAS_MASS_Y_91 = 90.907305 * IMAS_AMU, &
    IMAS_MASS_Y_92 = 91.908949 * IMAS_AMU, &
    IMAS_MASS_Y_93 = 92.909583 * IMAS_AMU, &
    IMAS_MASS_Y_94 = 93.911595 * IMAS_AMU, &
    IMAS_MASS_Y_95 = 94.912821 * IMAS_AMU, &
    IMAS_MASS_Y_96 = 95.915891 * IMAS_AMU, &
    IMAS_MASS_Y_97 = 96.918134 * IMAS_AMU, &
    IMAS_MASS_Y_98 = 97.922203 * IMAS_AMU, &
    IMAS_MASS_Y_99 = 98.924636 * IMAS_AMU, &
    IMAS_MASS_Y_100 = 99.927760 * IMAS_AMU, &
    IMAS_MASS_Y_101 = 100.93031 * IMAS_AMU, &
    IMAS_MASS_Y_102 = 101.933560 * IMAS_AMU, &
    IMAS_MASS_Y_103 = 102.93673 * IMAS_AMU, &
    IMAS_MASS_Y_104 = 103.94105 * IMAS_AMU, &
    IMAS_MASS_Y_105 = 104.94487 * IMAS_AMU, &
    IMAS_MASS_Y_106 = 105.94979 * IMAS_AMU, &
    IMAS_MASS_Y_107 = 106.95414 * IMAS_AMU, &
    IMAS_MASS_Y_108 = 107.95948 * IMAS_AMU, &
    IMAS_MASS_Zr_78 = 77.95523 * IMAS_AMU, &
    IMAS_MASS_Zr_79 = 78.94916 * IMAS_AMU, &
    IMAS_MASS_Zr_80 = 79.94040 * IMAS_AMU, &
    IMAS_MASS_Zr_81 = 80.93721 * IMAS_AMU, &
    IMAS_MASS_Zr_82 = 81.93109 * IMAS_AMU, &
    IMAS_MASS_Zr_83 = 82.92865 * IMAS_AMU, &
    IMAS_MASS_Zr_84 = 83.92325 * IMAS_AMU, &
    IMAS_MASS_Zr_85 = 84.92147 * IMAS_AMU, &
    IMAS_MASS_Zr_86 = 85.916470 * IMAS_AMU, &
    IMAS_MASS_Zr_87 = 86.914816 * IMAS_AMU, &
    IMAS_MASS_Zr_88 = 87.910227 * IMAS_AMU, &
    IMAS_MASS_Zr_89 = 88.908890 * IMAS_AMU, &
    IMAS_MASS_Zr_90 = 89.9047044 * IMAS_AMU, &
    IMAS_MASS_Zr_91 = 90.9056458 * IMAS_AMU, &
    IMAS_MASS_Zr_92 = 91.9050408 * IMAS_AMU, &
    IMAS_MASS_Zr_93 = 92.9064760 * IMAS_AMU, &
    IMAS_MASS_Zr_94 = 93.9063152 * IMAS_AMU, &
    IMAS_MASS_Zr_95 = 94.9080426 * IMAS_AMU, &
    IMAS_MASS_Zr_96 = 95.9082734 * IMAS_AMU, &
    IMAS_MASS_Zr_97 = 96.9109531 * IMAS_AMU, &
    IMAS_MASS_Zr_98 = 97.912735 * IMAS_AMU, &
    IMAS_MASS_Zr_99 = 98.916512 * IMAS_AMU, &
    IMAS_MASS_Zr_100 = 99.917760 * IMAS_AMU, &
    IMAS_MASS_Zr_101 = 100.921140 * IMAS_AMU, &
    IMAS_MASS_Zr_102 = 101.922980 * IMAS_AMU, &
    IMAS_MASS_Zr_103 = 102.92660 * IMAS_AMU, &
    IMAS_MASS_Zr_104 = 103.92878 * IMAS_AMU, &
    IMAS_MASS_Zr_105 = 104.93305 * IMAS_AMU, &
    IMAS_MASS_Zr_106 = 105.93591 * IMAS_AMU, &
    IMAS_MASS_Zr_107 = 106.94075 * IMAS_AMU, &
    IMAS_MASS_Zr_108 = 107.94396 * IMAS_AMU, &
    IMAS_MASS_Zr_109 = 108.94924 * IMAS_AMU, &
    IMAS_MASS_Zr_110 = 109.95287 * IMAS_AMU, &
    IMAS_MASS_Nb_81 = 80.94903 * IMAS_AMU, &
    IMAS_MASS_Nb_82 = 81.94313 * IMAS_AMU, &
    IMAS_MASS_Nb_83 = 82.93671 * IMAS_AMU, &
    IMAS_MASS_Nb_84 = 83.93357 * IMAS_AMU, &
    IMAS_MASS_Nb_85 = 84.92791 * IMAS_AMU, &
    IMAS_MASS_Nb_86 = 85.925040 * IMAS_AMU, &
    IMAS_MASS_Nb_87 = 86.920360 * IMAS_AMU, &
    IMAS_MASS_Nb_88 = 87.91833 * IMAS_AMU, &
    IMAS_MASS_Nb_89 = 88.913418 * IMAS_AMU, &
    IMAS_MASS_Nb_90 = 89.911265 * IMAS_AMU, &
    IMAS_MASS_Nb_91 = 90.906996 * IMAS_AMU, &
    IMAS_MASS_Nb_92 = 91.907194 * IMAS_AMU, &
    IMAS_MASS_Nb_93 = 92.9063781 * IMAS_AMU, &
    IMAS_MASS_Nb_94 = 93.9072839 * IMAS_AMU, &
    IMAS_MASS_Nb_95 = 94.9068358 * IMAS_AMU, &
    IMAS_MASS_Nb_96 = 95.908101 * IMAS_AMU, &
    IMAS_MASS_Nb_97 = 96.9080986 * IMAS_AMU, &
    IMAS_MASS_Nb_98 = 97.910328 * IMAS_AMU, &
    IMAS_MASS_Nb_99 = 98.911618 * IMAS_AMU, &
    IMAS_MASS_Nb_100 = 99.914182 * IMAS_AMU, &
    IMAS_MASS_Nb_101 = 100.915252 * IMAS_AMU, &
    IMAS_MASS_Nb_102 = 101.918040 * IMAS_AMU, &
    IMAS_MASS_Nb_103 = 102.919140 * IMAS_AMU, &
    IMAS_MASS_Nb_104 = 103.92246 * IMAS_AMU, &
    IMAS_MASS_Nb_105 = 104.92394 * IMAS_AMU, &
    IMAS_MASS_Nb_106 = 105.92797 * IMAS_AMU, &
    IMAS_MASS_Nb_107 = 106.93031 * IMAS_AMU, &
    IMAS_MASS_Nb_108 = 107.93484 * IMAS_AMU, &
    IMAS_MASS_Nb_109 = 108.93763 * IMAS_AMU, &
    IMAS_MASS_Nb_110 = 109.94244 * IMAS_AMU, &
    IMAS_MASS_Nb_111 = 110.94565 * IMAS_AMU, &
    IMAS_MASS_Nb_112 = 111.95083 * IMAS_AMU, &
    IMAS_MASS_Nb_113 = 112.95470 * IMAS_AMU, &
    IMAS_MASS_Mo_83 = 82.94874 * IMAS_AMU, &
    IMAS_MASS_Mo_84 = 83.94009 * IMAS_AMU, &
    IMAS_MASS_Mo_85 = 84.93655 * IMAS_AMU, &
    IMAS_MASS_Mo_86 = 85.93070 * IMAS_AMU, &
    IMAS_MASS_Mo_87 = 86.92733 * IMAS_AMU, &
    IMAS_MASS_Mo_88 = 87.921953 * IMAS_AMU, &
    IMAS_MASS_Mo_89 = 88.919480 * IMAS_AMU, &
    IMAS_MASS_Mo_90 = 89.913937 * IMAS_AMU, &
    IMAS_MASS_Mo_91 = 90.911750 * IMAS_AMU, &
    IMAS_MASS_Mo_92 = 91.906811 * IMAS_AMU, &
    IMAS_MASS_Mo_93 = 92.906813 * IMAS_AMU, &
    IMAS_MASS_Mo_94 = 93.9050883 * IMAS_AMU, &
    IMAS_MASS_Mo_95 = 94.9058421 * IMAS_AMU, &
    IMAS_MASS_Mo_96 = 95.9046795 * IMAS_AMU, &
    IMAS_MASS_Mo_97 = 96.9060215 * IMAS_AMU, &
    IMAS_MASS_Mo_98 = 97.9054082 * IMAS_AMU, &
    IMAS_MASS_Mo_99 = 98.9077119 * IMAS_AMU, &
    IMAS_MASS_Mo_100 = 99.907477 * IMAS_AMU, &
    IMAS_MASS_Mo_101 = 100.910347 * IMAS_AMU, &
    IMAS_MASS_Mo_102 = 101.910297 * IMAS_AMU, &
    IMAS_MASS_Mo_103 = 102.913210 * IMAS_AMU, &
    IMAS_MASS_Mo_104 = 103.913760 * IMAS_AMU, &
    IMAS_MASS_Mo_105 = 104.916970 * IMAS_AMU, &
    IMAS_MASS_Mo_106 = 105.918137 * IMAS_AMU, &
    IMAS_MASS_Mo_107 = 106.92169 * IMAS_AMU, &
    IMAS_MASS_Mo_108 = 107.92345 * IMAS_AMU, &
    IMAS_MASS_Mo_109 = 108.92781 * IMAS_AMU, &
    IMAS_MASS_Mo_110 = 109.92973 * IMAS_AMU, &
    IMAS_MASS_Mo_111 = 110.93441 * IMAS_AMU, &
    IMAS_MASS_Mo_112 = 111.93684 * IMAS_AMU, &
    IMAS_MASS_Mo_113 = 112.94188 * IMAS_AMU, &
    IMAS_MASS_Mo_114 = 113.94492 * IMAS_AMU, &
    IMAS_MASS_Mo_115 = 114.95029 * IMAS_AMU, &
    IMAS_MASS_Tc_85 = 84.94883 * IMAS_AMU, &
    IMAS_MASS_Tc_86 = 85.94288 * IMAS_AMU, &
    IMAS_MASS_Tc_87 = 86.93653 * IMAS_AMU, &
    IMAS_MASS_Tc_88 = 87.93268 * IMAS_AMU, &
    IMAS_MASS_Tc_89 = 88.92717 * IMAS_AMU, &
    IMAS_MASS_Tc_90 = 89.92356 * IMAS_AMU, &
    IMAS_MASS_Tc_91 = 90.91843 * IMAS_AMU, &
    IMAS_MASS_Tc_92 = 91.915260 * IMAS_AMU, &
    IMAS_MASS_Tc_93 = 92.910249 * IMAS_AMU, &
    IMAS_MASS_Tc_94 = 93.909657 * IMAS_AMU, &
    IMAS_MASS_Tc_95 = 94.907657 * IMAS_AMU, &
    IMAS_MASS_Tc_96 = 95.907871 * IMAS_AMU, &
    IMAS_MASS_Tc_97 = 96.906365 * IMAS_AMU, &
    IMAS_MASS_Tc_98 = 97.907216 * IMAS_AMU, &
    IMAS_MASS_Tc_99 = 98.9062547 * IMAS_AMU, &
    IMAS_MASS_Tc_100 = 99.9076578 * IMAS_AMU, &
    IMAS_MASS_Tc_101 = 100.907315 * IMAS_AMU, &
    IMAS_MASS_Tc_102 = 101.909215 * IMAS_AMU, &
    IMAS_MASS_Tc_103 = 102.909181 * IMAS_AMU, &
    IMAS_MASS_Tc_104 = 103.911450 * IMAS_AMU, &
    IMAS_MASS_Tc_105 = 104.911660 * IMAS_AMU, &
    IMAS_MASS_Tc_106 = 105.914358 * IMAS_AMU, &
    IMAS_MASS_Tc_107 = 106.91508 * IMAS_AMU, &
    IMAS_MASS_Tc_108 = 107.91846 * IMAS_AMU, &
    IMAS_MASS_Tc_109 = 108.91998 * IMAS_AMU, &
    IMAS_MASS_Tc_110 = 109.923820 * IMAS_AMU, &
    IMAS_MASS_Tc_111 = 110.92569 * IMAS_AMU, &
    IMAS_MASS_Tc_112 = 111.92915 * IMAS_AMU, &
    IMAS_MASS_Tc_113 = 112.93159 * IMAS_AMU, &
    IMAS_MASS_Tc_114 = 113.93588 * IMAS_AMU, &
    IMAS_MASS_Tc_115 = 114.93869 * IMAS_AMU, &
    IMAS_MASS_Tc_116 = 115.94337 * IMAS_AMU, &
    IMAS_MASS_Tc_117 = 116.94648 * IMAS_AMU, &
    IMAS_MASS_Tc_118 = 117.95148 * IMAS_AMU, &
    IMAS_MASS_Ru_87 = 86.94918 * IMAS_AMU, &
    IMAS_MASS_Ru_88 = 87.94026 * IMAS_AMU, &
    IMAS_MASS_Ru_89 = 88.93611 * IMAS_AMU, &
    IMAS_MASS_Ru_90 = 89.92989 * IMAS_AMU, &
    IMAS_MASS_Ru_91 = 90.92629 * IMAS_AMU, &
    IMAS_MASS_Ru_92 = 91.92012 * IMAS_AMU, &
    IMAS_MASS_Ru_93 = 92.917050 * IMAS_AMU, &
    IMAS_MASS_Ru_94 = 93.911360 * IMAS_AMU, &
    IMAS_MASS_Ru_95 = 94.910413 * IMAS_AMU, &
    IMAS_MASS_Ru_96 = 95.907598 * IMAS_AMU, &
    IMAS_MASS_Ru_97 = 96.907555 * IMAS_AMU, &
    IMAS_MASS_Ru_98 = 97.905287 * IMAS_AMU, &
    IMAS_MASS_Ru_99 = 98.9059393 * IMAS_AMU, &
    IMAS_MASS_Ru_100 = 99.9042195 * IMAS_AMU, &
    IMAS_MASS_Ru_101 = 100.9055821 * IMAS_AMU, &
    IMAS_MASS_Ru_102 = 101.9043493 * IMAS_AMU, &
    IMAS_MASS_Ru_103 = 102.9063238 * IMAS_AMU, &
    IMAS_MASS_Ru_104 = 103.905433 * IMAS_AMU, &
    IMAS_MASS_Ru_105 = 104.907753 * IMAS_AMU, &
    IMAS_MASS_Ru_106 = 105.907329 * IMAS_AMU, &
    IMAS_MASS_Ru_107 = 106.90991 * IMAS_AMU, &
    IMAS_MASS_Ru_108 = 107.91017 * IMAS_AMU, &
    IMAS_MASS_Ru_109 = 108.913200 * IMAS_AMU, &
    IMAS_MASS_Ru_110 = 109.914140 * IMAS_AMU, &
    IMAS_MASS_Ru_111 = 110.917700 * IMAS_AMU, &
    IMAS_MASS_Ru_112 = 111.918970 * IMAS_AMU, &
    IMAS_MASS_Ru_113 = 112.922490 * IMAS_AMU, &
    IMAS_MASS_Ru_114 = 113.92428 * IMAS_AMU, &
    IMAS_MASS_Ru_115 = 114.92869 * IMAS_AMU, &
    IMAS_MASS_Ru_116 = 115.93081 * IMAS_AMU, &
    IMAS_MASS_Ru_117 = 116.93558 * IMAS_AMU, &
    IMAS_MASS_Ru_118 = 117.93782 * IMAS_AMU, &
    IMAS_MASS_Ru_119 = 118.94284 * IMAS_AMU, &
    IMAS_MASS_Ru_120 = 119.94531 * IMAS_AMU, &
    IMAS_MASS_Rh_89 = 88.94884 * IMAS_AMU, &
    IMAS_MASS_Rh_90 = 89.94287 * IMAS_AMU, &
    IMAS_MASS_Rh_91 = 90.93655 * IMAS_AMU, &
    IMAS_MASS_Rh_92 = 91.93198 * IMAS_AMU, &
    IMAS_MASS_Rh_93 = 92.92574 * IMAS_AMU, &
    IMAS_MASS_Rh_94 = 93.92170 * IMAS_AMU, &
    IMAS_MASS_Rh_95 = 94.91590 * IMAS_AMU, &
    IMAS_MASS_Rh_96 = 95.914461 * IMAS_AMU, &
    IMAS_MASS_Rh_97 = 96.911340 * IMAS_AMU, &
    IMAS_MASS_Rh_98 = 97.910708 * IMAS_AMU, &
    IMAS_MASS_Rh_99 = 98.908132 * IMAS_AMU, &
    IMAS_MASS_Rh_100 = 99.908122 * IMAS_AMU, &
    IMAS_MASS_Rh_101 = 100.906164 * IMAS_AMU, &
    IMAS_MASS_Rh_102 = 101.906843 * IMAS_AMU, &
    IMAS_MASS_Rh_103 = 102.905504 * IMAS_AMU, &
    IMAS_MASS_Rh_104 = 103.906656 * IMAS_AMU, &
    IMAS_MASS_Rh_105 = 104.905694 * IMAS_AMU, &
    IMAS_MASS_Rh_106 = 105.907287 * IMAS_AMU, &
    IMAS_MASS_Rh_107 = 106.906748 * IMAS_AMU, &
    IMAS_MASS_Rh_108 = 107.90873 * IMAS_AMU, &
    IMAS_MASS_Rh_109 = 108.908737 * IMAS_AMU, &
    IMAS_MASS_Rh_110 = 109.911140 * IMAS_AMU, &
    IMAS_MASS_Rh_111 = 110.911590 * IMAS_AMU, &
    IMAS_MASS_Rh_112 = 111.914390 * IMAS_AMU, &
    IMAS_MASS_Rh_113 = 112.915530 * IMAS_AMU, &
    IMAS_MASS_Rh_114 = 113.91881 * IMAS_AMU, &
    IMAS_MASS_Rh_115 = 114.920330 * IMAS_AMU, &
    IMAS_MASS_Rh_116 = 115.92406 * IMAS_AMU, &
    IMAS_MASS_Rh_117 = 116.92598 * IMAS_AMU, &
    IMAS_MASS_Rh_118 = 117.93007 * IMAS_AMU, &
    IMAS_MASS_Rh_119 = 118.93211 * IMAS_AMU, &
    IMAS_MASS_Rh_120 = 119.93641 * IMAS_AMU, &
    IMAS_MASS_Rh_121 = 120.93872 * IMAS_AMU, &
    IMAS_MASS_Rh_122 = 121.94321 * IMAS_AMU, &
    IMAS_MASS_Pd_91 = 90.94911 * IMAS_AMU, &
    IMAS_MASS_Pd_92 = 91.94042 * IMAS_AMU, &
    IMAS_MASS_Pd_93 = 92.93591 * IMAS_AMU, &
    IMAS_MASS_Pd_94 = 93.92877 * IMAS_AMU, &
    IMAS_MASS_Pd_95 = 94.92469 * IMAS_AMU, &
    IMAS_MASS_Pd_96 = 95.91816 * IMAS_AMU, &
    IMAS_MASS_Pd_97 = 96.91648 * IMAS_AMU, &
    IMAS_MASS_Pd_98 = 97.912721 * IMAS_AMU, &
    IMAS_MASS_Pd_99 = 98.911768 * IMAS_AMU, &
    IMAS_MASS_Pd_100 = 99.908506 * IMAS_AMU, &
    IMAS_MASS_Pd_101 = 100.908289 * IMAS_AMU, &
    IMAS_MASS_Pd_102 = 101.905609 * IMAS_AMU, &
    IMAS_MASS_Pd_103 = 102.906087 * IMAS_AMU, &
    IMAS_MASS_Pd_104 = 103.904036 * IMAS_AMU, &
    IMAS_MASS_Pd_105 = 104.905085 * IMAS_AMU, &
    IMAS_MASS_Pd_106 = 105.903486 * IMAS_AMU, &
    IMAS_MASS_Pd_107 = 106.905133 * IMAS_AMU, &
    IMAS_MASS_Pd_108 = 107.903892 * IMAS_AMU, &
    IMAS_MASS_Pd_109 = 108.905950 * IMAS_AMU, &
    IMAS_MASS_Pd_110 = 109.905153 * IMAS_AMU, &
    IMAS_MASS_Pd_111 = 110.907671 * IMAS_AMU, &
    IMAS_MASS_Pd_112 = 111.907314 * IMAS_AMU, &
    IMAS_MASS_Pd_113 = 112.910150 * IMAS_AMU, &
    IMAS_MASS_Pd_114 = 113.910363 * IMAS_AMU, &
    IMAS_MASS_Pd_115 = 114.913680 * IMAS_AMU, &
    IMAS_MASS_Pd_116 = 115.914160 * IMAS_AMU, &
    IMAS_MASS_Pd_117 = 116.917840 * IMAS_AMU, &
    IMAS_MASS_Pd_118 = 117.91898 * IMAS_AMU, &
    IMAS_MASS_Pd_119 = 118.92311 * IMAS_AMU, &
    IMAS_MASS_Pd_120 = 119.92469 * IMAS_AMU, &
    IMAS_MASS_Pd_121 = 120.92887 * IMAS_AMU, &
    IMAS_MASS_Pd_122 = 121.93055 * IMAS_AMU, &
    IMAS_MASS_Pd_123 = 122.93493 * IMAS_AMU, &
    IMAS_MASS_Pd_124 = 123.93688 * IMAS_AMU, &
    IMAS_MASS_Ag_93 = 92.94978 * IMAS_AMU, &
    IMAS_MASS_Ag_94 = 93.94278 * IMAS_AMU, &
    IMAS_MASS_Ag_95 = 94.93548 * IMAS_AMU, &
    IMAS_MASS_Ag_96 = 95.93068 * IMAS_AMU, &
    IMAS_MASS_Ag_97 = 96.92397 * IMAS_AMU, &
    IMAS_MASS_Ag_98 = 97.921570 * IMAS_AMU, &
    IMAS_MASS_Ag_99 = 98.91760 * IMAS_AMU, &
    IMAS_MASS_Ag_100 = 99.916100 * IMAS_AMU, &
    IMAS_MASS_Ag_101 = 100.91280 * IMAS_AMU, &
    IMAS_MASS_Ag_102 = 101.911690 * IMAS_AMU, &
    IMAS_MASS_Ag_103 = 102.908973 * IMAS_AMU, &
    IMAS_MASS_Ag_104 = 103.908629 * IMAS_AMU, &
    IMAS_MASS_Ag_105 = 104.906529 * IMAS_AMU, &
    IMAS_MASS_Ag_106 = 105.906669 * IMAS_AMU, &
    IMAS_MASS_Ag_107 = 106.905097 * IMAS_AMU, &
    IMAS_MASS_Ag_108 = 107.905956 * IMAS_AMU, &
    IMAS_MASS_Ag_109 = 108.904752 * IMAS_AMU, &
    IMAS_MASS_Ag_110 = 109.906107 * IMAS_AMU, &
    IMAS_MASS_Ag_111 = 110.905291 * IMAS_AMU, &
    IMAS_MASS_Ag_112 = 111.907005 * IMAS_AMU, &
    IMAS_MASS_Ag_113 = 112.906567 * IMAS_AMU, &
    IMAS_MASS_Ag_114 = 113.908804 * IMAS_AMU, &
    IMAS_MASS_Ag_115 = 114.908760 * IMAS_AMU, &
    IMAS_MASS_Ag_116 = 115.911360 * IMAS_AMU, &
    IMAS_MASS_Ag_117 = 116.911680 * IMAS_AMU, &
    IMAS_MASS_Ag_118 = 117.914580 * IMAS_AMU, &
    IMAS_MASS_Ag_119 = 118.91567 * IMAS_AMU, &
    IMAS_MASS_Ag_120 = 119.918790 * IMAS_AMU, &
    IMAS_MASS_Ag_121 = 120.91985 * IMAS_AMU, &
    IMAS_MASS_Ag_122 = 121.92353 * IMAS_AMU, &
    IMAS_MASS_Ag_123 = 122.92490 * IMAS_AMU, &
    IMAS_MASS_Ag_124 = 123.92864 * IMAS_AMU, &
    IMAS_MASS_Ag_125 = 124.93043 * IMAS_AMU, &
    IMAS_MASS_Ag_126 = 125.93450 * IMAS_AMU, &
    IMAS_MASS_Ag_127 = 126.93677 * IMAS_AMU, &
    IMAS_MASS_Ag_128 = 127.94117 * IMAS_AMU, &
    IMAS_MASS_Ag_129 = 128.94369 * IMAS_AMU, &
    IMAS_MASS_Ag_130 = 129.95045 * IMAS_AMU, &
    IMAS_MASS_Cd_95 = 94.94987 * IMAS_AMU, &
    IMAS_MASS_Cd_96 = 95.93977 * IMAS_AMU, &
    IMAS_MASS_Cd_97 = 96.93494 * IMAS_AMU, &
    IMAS_MASS_Cd_98 = 97.927400 * IMAS_AMU, &
    IMAS_MASS_Cd_99 = 98.92501 * IMAS_AMU, &
    IMAS_MASS_Cd_100 = 99.92029 * IMAS_AMU, &
    IMAS_MASS_Cd_101 = 100.91868 * IMAS_AMU, &
    IMAS_MASS_Cd_102 = 101.914460 * IMAS_AMU, &
    IMAS_MASS_Cd_103 = 102.913419 * IMAS_AMU, &
    IMAS_MASS_Cd_104 = 103.909849 * IMAS_AMU, &
    IMAS_MASS_Cd_105 = 104.909468 * IMAS_AMU, &
    IMAS_MASS_Cd_106 = 105.906459 * IMAS_AMU, &
    IMAS_MASS_Cd_107 = 106.906618 * IMAS_AMU, &
    IMAS_MASS_Cd_108 = 107.904184 * IMAS_AMU, &
    IMAS_MASS_Cd_109 = 108.904982 * IMAS_AMU, &
    IMAS_MASS_Cd_110 = 109.9030021 * IMAS_AMU, &
    IMAS_MASS_Cd_111 = 110.9041781 * IMAS_AMU, &
    IMAS_MASS_Cd_112 = 111.9027578 * IMAS_AMU, &
    IMAS_MASS_Cd_113 = 112.9044017 * IMAS_AMU, &
    IMAS_MASS_Cd_114 = 113.9033585 * IMAS_AMU, &
    IMAS_MASS_Cd_115 = 114.9054310 * IMAS_AMU, &
    IMAS_MASS_Cd_116 = 115.904756 * IMAS_AMU, &
    IMAS_MASS_Cd_117 = 116.907219 * IMAS_AMU, &
    IMAS_MASS_Cd_118 = 117.906915 * IMAS_AMU, &
    IMAS_MASS_Cd_119 = 118.909920 * IMAS_AMU, &
    IMAS_MASS_Cd_120 = 119.909850 * IMAS_AMU, &
    IMAS_MASS_Cd_121 = 120.912980 * IMAS_AMU, &
    IMAS_MASS_Cd_122 = 121.913330 * IMAS_AMU, &
    IMAS_MASS_Cd_123 = 122.917000 * IMAS_AMU, &
    IMAS_MASS_Cd_124 = 123.917650 * IMAS_AMU, &
    IMAS_MASS_Cd_125 = 124.921250 * IMAS_AMU, &
    IMAS_MASS_Cd_126 = 125.922350 * IMAS_AMU, &
    IMAS_MASS_Cd_127 = 126.926440 * IMAS_AMU, &
    IMAS_MASS_Cd_128 = 127.92776 * IMAS_AMU, &
    IMAS_MASS_Cd_129 = 128.93215 * IMAS_AMU, &
    IMAS_MASS_Cd_130 = 129.93390 * IMAS_AMU, &
    IMAS_MASS_Cd_131 = 130.94067 * IMAS_AMU, &
    IMAS_MASS_Cd_132 = 131.94555 * IMAS_AMU, &
    IMAS_MASS_In_97 = 96.94954 * IMAS_AMU, &
    IMAS_MASS_In_98 = 97.94214 * IMAS_AMU, &
    IMAS_MASS_In_99 = 98.93422 * IMAS_AMU, &
    IMAS_MASS_In_100 = 99.93111 * IMAS_AMU, &
    IMAS_MASS_In_101 = 100.92634 * IMAS_AMU, &
    IMAS_MASS_In_102 = 101.92409 * IMAS_AMU, &
    IMAS_MASS_In_103 = 102.919914 * IMAS_AMU, &
    IMAS_MASS_In_104 = 103.918300 * IMAS_AMU, &
    IMAS_MASS_In_105 = 104.914674 * IMAS_AMU, &
    IMAS_MASS_In_106 = 105.913465 * IMAS_AMU, &
    IMAS_MASS_In_107 = 106.910295 * IMAS_AMU, &
    IMAS_MASS_In_108 = 107.909698 * IMAS_AMU, &
    IMAS_MASS_In_109 = 108.907151 * IMAS_AMU, &
    IMAS_MASS_In_110 = 109.907165 * IMAS_AMU, &
    IMAS_MASS_In_111 = 110.905103 * IMAS_AMU, &
    IMAS_MASS_In_112 = 111.905532 * IMAS_AMU, &
    IMAS_MASS_In_113 = 112.904058 * IMAS_AMU, &
    IMAS_MASS_In_114 = 113.904914 * IMAS_AMU, &
    IMAS_MASS_In_115 = 114.903878 * IMAS_AMU, &
    IMAS_MASS_In_116 = 115.905260 * IMAS_AMU, &
    IMAS_MASS_In_117 = 116.904514 * IMAS_AMU, &
    IMAS_MASS_In_118 = 117.906354 * IMAS_AMU, &
    IMAS_MASS_In_119 = 118.905845 * IMAS_AMU, &
    IMAS_MASS_In_120 = 119.907960 * IMAS_AMU, &
    IMAS_MASS_In_121 = 120.907846 * IMAS_AMU, &
    IMAS_MASS_In_122 = 121.910280 * IMAS_AMU, &
    IMAS_MASS_In_123 = 122.910438 * IMAS_AMU, &
    IMAS_MASS_In_124 = 123.913180 * IMAS_AMU, &
    IMAS_MASS_In_125 = 124.913600 * IMAS_AMU, &
    IMAS_MASS_In_126 = 125.916460 * IMAS_AMU, &
    IMAS_MASS_In_127 = 126.917350 * IMAS_AMU, &
    IMAS_MASS_In_128 = 127.920170 * IMAS_AMU, &
    IMAS_MASS_In_129 = 128.921700 * IMAS_AMU, &
    IMAS_MASS_In_130 = 129.924970 * IMAS_AMU, &
    IMAS_MASS_In_131 = 130.926850 * IMAS_AMU, &
    IMAS_MASS_In_132 = 131.932990 * IMAS_AMU, &
    IMAS_MASS_In_133 = 132.93781 * IMAS_AMU, &
    IMAS_MASS_In_134 = 133.94415 * IMAS_AMU, &
    IMAS_MASS_In_135 = 134.94933 * IMAS_AMU, &
    IMAS_MASS_Sn_99 = 98.94933 * IMAS_AMU, &
    IMAS_MASS_Sn_100 = 99.93904 * IMAS_AMU, &
    IMAS_MASS_Sn_101 = 100.93606 * IMAS_AMU, &
    IMAS_MASS_Sn_102 = 101.93030 * IMAS_AMU, &
    IMAS_MASS_Sn_103 = 102.92810 * IMAS_AMU, &
    IMAS_MASS_Sn_104 = 103.92314 * IMAS_AMU, &
    IMAS_MASS_Sn_105 = 104.921350 * IMAS_AMU, &
    IMAS_MASS_Sn_106 = 105.916880 * IMAS_AMU, &
    IMAS_MASS_Sn_107 = 106.915640 * IMAS_AMU, &
    IMAS_MASS_Sn_108 = 107.911925 * IMAS_AMU, &
    IMAS_MASS_Sn_109 = 108.911283 * IMAS_AMU, &
    IMAS_MASS_Sn_110 = 109.907843 * IMAS_AMU, &
    IMAS_MASS_Sn_111 = 110.907734 * IMAS_AMU, &
    IMAS_MASS_Sn_112 = 111.904818 * IMAS_AMU, &
    IMAS_MASS_Sn_113 = 112.905171 * IMAS_AMU, &
    IMAS_MASS_Sn_114 = 113.902779 * IMAS_AMU, &
    IMAS_MASS_Sn_115 = 114.903342 * IMAS_AMU, &
    IMAS_MASS_Sn_116 = 115.901741 * IMAS_AMU, &
    IMAS_MASS_Sn_117 = 116.902952 * IMAS_AMU, &
    IMAS_MASS_Sn_118 = 117.901603 * IMAS_AMU, &
    IMAS_MASS_Sn_119 = 118.903308 * IMAS_AMU, &
    IMAS_MASS_Sn_120 = 119.9021947 * IMAS_AMU, &
    IMAS_MASS_Sn_121 = 120.9042355 * IMAS_AMU, &
    IMAS_MASS_Sn_122 = 121.9034390 * IMAS_AMU, &
    IMAS_MASS_Sn_123 = 122.9057208 * IMAS_AMU, &
    IMAS_MASS_Sn_124 = 123.9052739 * IMAS_AMU, &
    IMAS_MASS_Sn_125 = 124.9077841 * IMAS_AMU, &
    IMAS_MASS_Sn_126 = 125.907653 * IMAS_AMU, &
    IMAS_MASS_Sn_127 = 126.910360 * IMAS_AMU, &
    IMAS_MASS_Sn_128 = 127.910537 * IMAS_AMU, &
    IMAS_MASS_Sn_129 = 128.913480 * IMAS_AMU, &
    IMAS_MASS_Sn_130 = 129.913967 * IMAS_AMU, &
    IMAS_MASS_Sn_131 = 130.917000 * IMAS_AMU, &
    IMAS_MASS_Sn_132 = 131.917816 * IMAS_AMU, &
    IMAS_MASS_Sn_133 = 132.923830 * IMAS_AMU, &
    IMAS_MASS_Sn_134 = 133.92829 * IMAS_AMU, &
    IMAS_MASS_Sn_135 = 134.93473 * IMAS_AMU, &
    IMAS_MASS_Sn_136 = 135.93934 * IMAS_AMU, &
    IMAS_MASS_Sn_137 = 136.94599 * IMAS_AMU, &
    IMAS_MASS_Sb_103 = 102.93969 * IMAS_AMU, &
    IMAS_MASS_Sb_104 = 103.93647 * IMAS_AMU, &
    IMAS_MASS_Sb_105 = 104.93149 * IMAS_AMU, &
    IMAS_MASS_Sb_106 = 105.92879 * IMAS_AMU, &
    IMAS_MASS_Sb_107 = 106.92415 * IMAS_AMU, &
    IMAS_MASS_Sb_108 = 107.92216 * IMAS_AMU, &
    IMAS_MASS_Sb_109 = 108.918132 * IMAS_AMU, &
    IMAS_MASS_Sb_110 = 109.91675 * IMAS_AMU, &
    IMAS_MASS_Sb_111 = 110.913160 * IMAS_AMU, &
    IMAS_MASS_Sb_112 = 111.912398 * IMAS_AMU, &
    IMAS_MASS_Sb_113 = 112.909372 * IMAS_AMU, &
    IMAS_MASS_Sb_114 = 113.909270 * IMAS_AMU, &
    IMAS_MASS_Sb_115 = 114.906598 * IMAS_AMU, &
    IMAS_MASS_Sb_116 = 115.906794 * IMAS_AMU, &
    IMAS_MASS_Sb_117 = 116.904836 * IMAS_AMU, &
    IMAS_MASS_Sb_118 = 117.905529 * IMAS_AMU, &
    IMAS_MASS_Sb_119 = 118.903942 * IMAS_AMU, &
    IMAS_MASS_Sb_120 = 119.905072 * IMAS_AMU, &
    IMAS_MASS_Sb_121 = 120.9038157 * IMAS_AMU, &
    IMAS_MASS_Sb_122 = 121.9051737 * IMAS_AMU, &
    IMAS_MASS_Sb_123 = 122.9042140 * IMAS_AMU, &
    IMAS_MASS_Sb_124 = 123.9059357 * IMAS_AMU, &
    IMAS_MASS_Sb_125 = 124.9052538 * IMAS_AMU, &
    IMAS_MASS_Sb_126 = 125.907250 * IMAS_AMU, &
    IMAS_MASS_Sb_127 = 126.906924 * IMAS_AMU, &
    IMAS_MASS_Sb_128 = 127.909169 * IMAS_AMU, &
    IMAS_MASS_Sb_129 = 128.909148 * IMAS_AMU, &
    IMAS_MASS_Sb_130 = 129.911656 * IMAS_AMU, &
    IMAS_MASS_Sb_131 = 130.911982 * IMAS_AMU, &
    IMAS_MASS_Sb_132 = 131.914467 * IMAS_AMU, &
    IMAS_MASS_Sb_133 = 132.915252 * IMAS_AMU, &
    IMAS_MASS_Sb_134 = 133.920380 * IMAS_AMU, &
    IMAS_MASS_Sb_135 = 134.92517 * IMAS_AMU, &
    IMAS_MASS_Sb_136 = 135.93035 * IMAS_AMU, &
    IMAS_MASS_Sb_137 = 136.93531 * IMAS_AMU, &
    IMAS_MASS_Sb_138 = 137.94079 * IMAS_AMU, &
    IMAS_MASS_Sb_139 = 138.94598 * IMAS_AMU, &
    IMAS_MASS_Te_105 = 104.94364 * IMAS_AMU, &
    IMAS_MASS_Te_106 = 105.93750 * IMAS_AMU, &
    IMAS_MASS_Te_107 = 106.93501 * IMAS_AMU, &
    IMAS_MASS_Te_108 = 107.92944 * IMAS_AMU, &
    IMAS_MASS_Te_109 = 108.927420 * IMAS_AMU, &
    IMAS_MASS_Te_110 = 109.922410 * IMAS_AMU, &
    IMAS_MASS_Te_111 = 110.921110 * IMAS_AMU, &
    IMAS_MASS_Te_112 = 111.91701 * IMAS_AMU, &
    IMAS_MASS_Te_113 = 112.915890 * IMAS_AMU, &
    IMAS_MASS_Te_114 = 113.912090 * IMAS_AMU, &
    IMAS_MASS_Te_115 = 114.911900 * IMAS_AMU, &
    IMAS_MASS_Te_116 = 115.908460 * IMAS_AMU, &
    IMAS_MASS_Te_117 = 116.908645 * IMAS_AMU, &
    IMAS_MASS_Te_118 = 117.905828 * IMAS_AMU, &
    IMAS_MASS_Te_119 = 118.906404 * IMAS_AMU, &
    IMAS_MASS_Te_120 = 119.904020 * IMAS_AMU, &
    IMAS_MASS_Te_121 = 120.904936 * IMAS_AMU, &
    IMAS_MASS_Te_122 = 121.9030439 * IMAS_AMU, &
    IMAS_MASS_Te_123 = 122.9042700 * IMAS_AMU, &
    IMAS_MASS_Te_124 = 123.9028179 * IMAS_AMU, &
    IMAS_MASS_Te_125 = 124.9044307 * IMAS_AMU, &
    IMAS_MASS_Te_126 = 125.9033117 * IMAS_AMU, &
    IMAS_MASS_Te_127 = 126.9052263 * IMAS_AMU, &
    IMAS_MASS_Te_128 = 127.9044631 * IMAS_AMU, &
    IMAS_MASS_Te_129 = 128.9065982 * IMAS_AMU, &
    IMAS_MASS_Te_130 = 129.9062244 * IMAS_AMU, &
    IMAS_MASS_Te_131 = 130.9085239 * IMAS_AMU, &
    IMAS_MASS_Te_132 = 131.908553 * IMAS_AMU, &
    IMAS_MASS_Te_133 = 132.910955 * IMAS_AMU, &
    IMAS_MASS_Te_134 = 133.911369 * IMAS_AMU, &
    IMAS_MASS_Te_135 = 134.91645 * IMAS_AMU, &
    IMAS_MASS_Te_136 = 135.920100 * IMAS_AMU, &
    IMAS_MASS_Te_137 = 136.92532 * IMAS_AMU, &
    IMAS_MASS_Te_138 = 137.92922 * IMAS_AMU, &
    IMAS_MASS_Te_139 = 138.93473 * IMAS_AMU, &
    IMAS_MASS_Te_140 = 139.93885 * IMAS_AMU, &
    IMAS_MASS_Te_141 = 140.94465 * IMAS_AMU, &
    IMAS_MASS_Te_142 = 141.94908 * IMAS_AMU, &
    IMAS_MASS_I_108 = 107.94348 * IMAS_AMU, &
    IMAS_MASS_I_109 = 108.93815 * IMAS_AMU, &
    IMAS_MASS_I_110 = 109.93524 * IMAS_AMU, &
    IMAS_MASS_I_111 = 110.93028 * IMAS_AMU, &
    IMAS_MASS_I_112 = 111.92797 * IMAS_AMU, &
    IMAS_MASS_I_113 = 112.923640 * IMAS_AMU, &
    IMAS_MASS_I_114 = 113.92185 * IMAS_AMU, &
    IMAS_MASS_I_115 = 114.918050 * IMAS_AMU, &
    IMAS_MASS_I_116 = 115.91681 * IMAS_AMU, &
    IMAS_MASS_I_117 = 116.913650 * IMAS_AMU, &
    IMAS_MASS_I_118 = 117.913074 * IMAS_AMU, &
    IMAS_MASS_I_119 = 118.910070 * IMAS_AMU, &
    IMAS_MASS_I_120 = 119.910048 * IMAS_AMU, &
    IMAS_MASS_I_121 = 120.907367 * IMAS_AMU, &
    IMAS_MASS_I_122 = 121.907589 * IMAS_AMU, &
    IMAS_MASS_I_123 = 122.905589 * IMAS_AMU, &
    IMAS_MASS_I_124 = 123.9062099 * IMAS_AMU, &
    IMAS_MASS_I_125 = 124.9046302 * IMAS_AMU, &
    IMAS_MASS_I_126 = 125.905624 * IMAS_AMU, &
    IMAS_MASS_I_127 = 126.904473 * IMAS_AMU, &
    IMAS_MASS_I_128 = 127.905809 * IMAS_AMU, &
    IMAS_MASS_I_129 = 128.904988 * IMAS_AMU, &
    IMAS_MASS_I_130 = 129.906674 * IMAS_AMU, &
    IMAS_MASS_I_131 = 130.9061246 * IMAS_AMU, &
    IMAS_MASS_I_132 = 131.907997 * IMAS_AMU, &
    IMAS_MASS_I_133 = 132.907797 * IMAS_AMU, &
    IMAS_MASS_I_134 = 133.909744 * IMAS_AMU, &
    IMAS_MASS_I_135 = 134.910048 * IMAS_AMU, &
    IMAS_MASS_I_136 = 135.914650 * IMAS_AMU, &
    IMAS_MASS_I_137 = 136.917871 * IMAS_AMU, &
    IMAS_MASS_I_138 = 137.922350 * IMAS_AMU, &
    IMAS_MASS_I_139 = 138.926100 * IMAS_AMU, &
    IMAS_MASS_I_140 = 139.93100 * IMAS_AMU, &
    IMAS_MASS_I_141 = 140.93503 * IMAS_AMU, &
    IMAS_MASS_I_142 = 141.94018 * IMAS_AMU, &
    IMAS_MASS_I_143 = 142.94456 * IMAS_AMU, &
    IMAS_MASS_I_144 = 143.94999 * IMAS_AMU, &
    IMAS_MASS_Xe_110 = 109.94428 * IMAS_AMU, &
    IMAS_MASS_Xe_111 = 110.94160 * IMAS_AMU, &
    IMAS_MASS_Xe_112 = 111.93562 * IMAS_AMU, &
    IMAS_MASS_Xe_113 = 112.933340 * IMAS_AMU, &
    IMAS_MASS_Xe_114 = 113.927980 * IMAS_AMU, &
    IMAS_MASS_Xe_115 = 114.926294 * IMAS_AMU, &
    IMAS_MASS_Xe_116 = 115.921581 * IMAS_AMU, &
    IMAS_MASS_Xe_117 = 116.920359 * IMAS_AMU, &
    IMAS_MASS_Xe_118 = 117.916179 * IMAS_AMU, &
    IMAS_MASS_Xe_119 = 118.915411 * IMAS_AMU, &
    IMAS_MASS_Xe_120 = 119.911784 * IMAS_AMU, &
    IMAS_MASS_Xe_121 = 120.911462 * IMAS_AMU, &
    IMAS_MASS_Xe_122 = 121.908368 * IMAS_AMU, &
    IMAS_MASS_Xe_123 = 122.908482 * IMAS_AMU, &
    IMAS_MASS_Xe_124 = 123.9058930 * IMAS_AMU, &
    IMAS_MASS_Xe_125 = 124.9063955 * IMAS_AMU, &
    IMAS_MASS_Xe_126 = 125.904274 * IMAS_AMU, &
    IMAS_MASS_Xe_127 = 126.905184 * IMAS_AMU, &
    IMAS_MASS_Xe_128 = 127.9035313 * IMAS_AMU, &
    IMAS_MASS_Xe_129 = 128.9047794 * IMAS_AMU, &
    IMAS_MASS_Xe_130 = 129.9035080 * IMAS_AMU, &
    IMAS_MASS_Xe_131 = 130.9050824 * IMAS_AMU, &
    IMAS_MASS_Xe_132 = 131.9041535 * IMAS_AMU, &
    IMAS_MASS_Xe_133 = 132.9059107 * IMAS_AMU, &
    IMAS_MASS_Xe_134 = 133.9053945 * IMAS_AMU, &
    IMAS_MASS_Xe_135 = 134.907227 * IMAS_AMU, &
    IMAS_MASS_Xe_136 = 135.907219 * IMAS_AMU, &
    IMAS_MASS_Xe_137 = 136.911562 * IMAS_AMU, &
    IMAS_MASS_Xe_138 = 137.913950 * IMAS_AMU, &
    IMAS_MASS_Xe_139 = 138.918793 * IMAS_AMU, &
    IMAS_MASS_Xe_140 = 139.921640 * IMAS_AMU, &
    IMAS_MASS_Xe_141 = 140.92665 * IMAS_AMU, &
    IMAS_MASS_Xe_142 = 141.92971 * IMAS_AMU, &
    IMAS_MASS_Xe_143 = 142.93511 * IMAS_AMU, &
    IMAS_MASS_Xe_144 = 143.93851 * IMAS_AMU, &
    IMAS_MASS_Xe_145 = 144.94407 * IMAS_AMU, &
    IMAS_MASS_Xe_146 = 145.94775 * IMAS_AMU, &
    IMAS_MASS_Xe_147 = 146.95356 * IMAS_AMU, &
    IMAS_MASS_Cs_112 = 111.95030 * IMAS_AMU, &
    IMAS_MASS_Cs_113 = 112.94449 * IMAS_AMU, &
    IMAS_MASS_Cs_114 = 113.94145 * IMAS_AMU, &
    IMAS_MASS_Cs_115 = 114.93591 * IMAS_AMU, &
    IMAS_MASS_Cs_116 = 115.93337 * IMAS_AMU, &
    IMAS_MASS_Cs_117 = 116.928670 * IMAS_AMU, &
    IMAS_MASS_Cs_118 = 117.926559 * IMAS_AMU, &
    IMAS_MASS_Cs_119 = 118.922377 * IMAS_AMU, &
    IMAS_MASS_Cs_120 = 119.920677 * IMAS_AMU, &
    IMAS_MASS_Cs_121 = 120.917229 * IMAS_AMU, &
    IMAS_MASS_Cs_122 = 121.916110 * IMAS_AMU, &
    IMAS_MASS_Cs_123 = 122.912996 * IMAS_AMU, &
    IMAS_MASS_Cs_124 = 123.912258 * IMAS_AMU, &
    IMAS_MASS_Cs_125 = 124.909728 * IMAS_AMU, &
    IMAS_MASS_Cs_126 = 125.909452 * IMAS_AMU, &
    IMAS_MASS_Cs_127 = 126.907418 * IMAS_AMU, &
    IMAS_MASS_Cs_128 = 127.907749 * IMAS_AMU, &
    IMAS_MASS_Cs_129 = 128.906064 * IMAS_AMU, &
    IMAS_MASS_Cs_130 = 129.906709 * IMAS_AMU, &
    IMAS_MASS_Cs_131 = 130.905464 * IMAS_AMU, &
    IMAS_MASS_Cs_132 = 131.9064343 * IMAS_AMU, &
    IMAS_MASS_Cs_133 = 132.905451933 * IMAS_AMU, &
    IMAS_MASS_Cs_134 = 133.906718475 * IMAS_AMU, &
    IMAS_MASS_Cs_135 = 134.9059770 * IMAS_AMU, &
    IMAS_MASS_Cs_136 = 135.9073116 * IMAS_AMU, &
    IMAS_MASS_Cs_137 = 136.9070895 * IMAS_AMU, &
    IMAS_MASS_Cs_138 = 137.911017 * IMAS_AMU, &
    IMAS_MASS_Cs_139 = 138.913364 * IMAS_AMU, &
    IMAS_MASS_Cs_140 = 139.917282 * IMAS_AMU, &
    IMAS_MASS_Cs_141 = 140.920046 * IMAS_AMU, &
    IMAS_MASS_Cs_142 = 141.924299 * IMAS_AMU, &
    IMAS_MASS_Cs_143 = 142.927352 * IMAS_AMU, &
    IMAS_MASS_Cs_144 = 143.932077 * IMAS_AMU, &
    IMAS_MASS_Cs_145 = 144.935526 * IMAS_AMU, &
    IMAS_MASS_Cs_146 = 145.940290 * IMAS_AMU, &
    IMAS_MASS_Cs_147 = 146.944160 * IMAS_AMU, &
    IMAS_MASS_Cs_148 = 147.94922 * IMAS_AMU, &
    IMAS_MASS_Cs_149 = 148.95293 * IMAS_AMU, &
    IMAS_MASS_Cs_150 = 149.95817 * IMAS_AMU, &
    IMAS_MASS_Cs_151 = 150.96219 * IMAS_AMU, &
    IMAS_MASS_Ba_114 = 113.95068 * IMAS_AMU, &
    IMAS_MASS_Ba_115 = 114.94737 * IMAS_AMU, &
    IMAS_MASS_Ba_116 = 115.94138 * IMAS_AMU, &
    IMAS_MASS_Ba_117 = 116.93850 * IMAS_AMU, &
    IMAS_MASS_Ba_118 = 117.93304 * IMAS_AMU, &
    IMAS_MASS_Ba_119 = 118.93066 * IMAS_AMU, &
    IMAS_MASS_Ba_120 = 119.92604 * IMAS_AMU, &
    IMAS_MASS_Ba_121 = 120.92405 * IMAS_AMU, &
    IMAS_MASS_Ba_122 = 121.919900 * IMAS_AMU, &
    IMAS_MASS_Ba_123 = 122.918781 * IMAS_AMU, &
    IMAS_MASS_Ba_124 = 123.915094 * IMAS_AMU, &
    IMAS_MASS_Ba_125 = 124.914473 * IMAS_AMU, &
    IMAS_MASS_Ba_126 = 125.911250 * IMAS_AMU, &
    IMAS_MASS_Ba_127 = 126.911094 * IMAS_AMU, &
    IMAS_MASS_Ba_128 = 127.908318 * IMAS_AMU, &
    IMAS_MASS_Ba_129 = 128.908679 * IMAS_AMU, &
    IMAS_MASS_Ba_130 = 129.9063208 * IMAS_AMU, &
    IMAS_MASS_Ba_131 = 130.906941 * IMAS_AMU, &
    IMAS_MASS_Ba_132 = 131.9050613 * IMAS_AMU, &
    IMAS_MASS_Ba_133 = 132.9060075 * IMAS_AMU, &
    IMAS_MASS_Ba_134 = 133.9045084 * IMAS_AMU, &
    IMAS_MASS_Ba_135 = 134.9056886 * IMAS_AMU, &
    IMAS_MASS_Ba_136 = 135.9045759 * IMAS_AMU, &
    IMAS_MASS_Ba_137 = 136.9058274 * IMAS_AMU, &
    IMAS_MASS_Ba_138 = 137.9052472 * IMAS_AMU, &
    IMAS_MASS_Ba_139 = 138.9088413 * IMAS_AMU, &
    IMAS_MASS_Ba_140 = 139.910605 * IMAS_AMU, &
    IMAS_MASS_Ba_141 = 140.914411 * IMAS_AMU, &
    IMAS_MASS_Ba_142 = 141.916453 * IMAS_AMU, &
    IMAS_MASS_Ba_143 = 142.920627 * IMAS_AMU, &
    IMAS_MASS_Ba_144 = 143.922953 * IMAS_AMU, &
    IMAS_MASS_Ba_145 = 144.927630 * IMAS_AMU, &
    IMAS_MASS_Ba_146 = 145.930220 * IMAS_AMU, &
    IMAS_MASS_Ba_147 = 146.93495 * IMAS_AMU, &
    IMAS_MASS_Ba_148 = 147.937720 * IMAS_AMU, &
    IMAS_MASS_Ba_149 = 148.94258 * IMAS_AMU, &
    IMAS_MASS_Ba_150 = 149.94568 * IMAS_AMU, &
    IMAS_MASS_Ba_151 = 150.95081 * IMAS_AMU, &
    IMAS_MASS_Ba_152 = 151.95427 * IMAS_AMU, &
    IMAS_MASS_Ba_153 = 152.95961 * IMAS_AMU, &
    IMAS_MASS_La_117 = 116.95007 * IMAS_AMU, &
    IMAS_MASS_La_118 = 117.94673 * IMAS_AMU, &
    IMAS_MASS_La_119 = 118.94099 * IMAS_AMU, &
    IMAS_MASS_La_120 = 119.93807 * IMAS_AMU, &
    IMAS_MASS_La_121 = 120.93301 * IMAS_AMU, &
    IMAS_MASS_La_122 = 121.93071 * IMAS_AMU, &
    IMAS_MASS_La_123 = 122.92624 * IMAS_AMU, &
    IMAS_MASS_La_124 = 123.924570 * IMAS_AMU, &
    IMAS_MASS_La_125 = 124.920816 * IMAS_AMU, &
    IMAS_MASS_La_126 = 125.91951 * IMAS_AMU, &
    IMAS_MASS_La_127 = 126.916375 * IMAS_AMU, &
    IMAS_MASS_La_128 = 127.915590 * IMAS_AMU, &
    IMAS_MASS_La_129 = 128.912693 * IMAS_AMU, &
    IMAS_MASS_La_130 = 129.912369 * IMAS_AMU, &
    IMAS_MASS_La_131 = 130.910070 * IMAS_AMU, &
    IMAS_MASS_La_132 = 131.910100 * IMAS_AMU, &
    IMAS_MASS_La_133 = 132.908220 * IMAS_AMU, &
    IMAS_MASS_La_134 = 133.908514 * IMAS_AMU, &
    IMAS_MASS_La_135 = 134.906977 * IMAS_AMU, &
    IMAS_MASS_La_136 = 135.907640 * IMAS_AMU, &
    IMAS_MASS_La_137 = 136.906494 * IMAS_AMU, &
    IMAS_MASS_La_138 = 137.907112 * IMAS_AMU, &
    IMAS_MASS_La_139 = 138.9063533 * IMAS_AMU, &
    IMAS_MASS_La_140 = 139.9094776 * IMAS_AMU, &
    IMAS_MASS_La_141 = 140.910962 * IMAS_AMU, &
    IMAS_MASS_La_142 = 141.914079 * IMAS_AMU, &
    IMAS_MASS_La_143 = 142.916063 * IMAS_AMU, &
    IMAS_MASS_La_144 = 143.919600 * IMAS_AMU, &
    IMAS_MASS_La_145 = 144.92165 * IMAS_AMU, &
    IMAS_MASS_La_146 = 145.925790 * IMAS_AMU, &
    IMAS_MASS_La_147 = 146.928240 * IMAS_AMU, &
    IMAS_MASS_La_148 = 147.932230 * IMAS_AMU, &
    IMAS_MASS_La_149 = 148.93473 * IMAS_AMU, &
    IMAS_MASS_La_150 = 149.93877 * IMAS_AMU, &
    IMAS_MASS_La_151 = 150.94172 * IMAS_AMU, &
    IMAS_MASS_La_152 = 151.94625 * IMAS_AMU, &
    IMAS_MASS_La_153 = 152.94962 * IMAS_AMU, &
    IMAS_MASS_La_154 = 153.95450 * IMAS_AMU, &
    IMAS_MASS_La_155 = 154.95835 * IMAS_AMU, &
    IMAS_MASS_Ce_119 = 118.95276 * IMAS_AMU, &
    IMAS_MASS_Ce_120 = 119.94664 * IMAS_AMU, &
    IMAS_MASS_Ce_121 = 120.94342 * IMAS_AMU, &
    IMAS_MASS_Ce_122 = 121.93791 * IMAS_AMU, &
    IMAS_MASS_Ce_123 = 122.93540 * IMAS_AMU, &
    IMAS_MASS_Ce_124 = 123.93041 * IMAS_AMU, &
    IMAS_MASS_Ce_125 = 124.92844 * IMAS_AMU, &
    IMAS_MASS_Ce_126 = 125.923970 * IMAS_AMU, &
    IMAS_MASS_Ce_127 = 126.922730 * IMAS_AMU, &
    IMAS_MASS_Ce_128 = 127.918910 * IMAS_AMU, &
    IMAS_MASS_Ce_129 = 128.918100 * IMAS_AMU, &
    IMAS_MASS_Ce_130 = 129.914740 * IMAS_AMU, &
    IMAS_MASS_Ce_131 = 130.914420 * IMAS_AMU, &
    IMAS_MASS_Ce_132 = 131.911460 * IMAS_AMU, &
    IMAS_MASS_Ce_133 = 132.911515 * IMAS_AMU, &
    IMAS_MASS_Ce_134 = 133.908925 * IMAS_AMU, &
    IMAS_MASS_Ce_135 = 134.909151 * IMAS_AMU, &
    IMAS_MASS_Ce_136 = 135.907172 * IMAS_AMU, &
    IMAS_MASS_Ce_137 = 136.907806 * IMAS_AMU, &
    IMAS_MASS_Ce_138 = 137.905991 * IMAS_AMU, &
    IMAS_MASS_Ce_139 = 138.906653 * IMAS_AMU, &
    IMAS_MASS_Ce_140 = 139.9054387 * IMAS_AMU, &
    IMAS_MASS_Ce_141 = 140.9082763 * IMAS_AMU, &
    IMAS_MASS_Ce_142 = 141.909244 * IMAS_AMU, &
    IMAS_MASS_Ce_143 = 142.912386 * IMAS_AMU, &
    IMAS_MASS_Ce_144 = 143.913647 * IMAS_AMU, &
    IMAS_MASS_Ce_145 = 144.917230 * IMAS_AMU, &
    IMAS_MASS_Ce_146 = 145.918760 * IMAS_AMU, &
    IMAS_MASS_Ce_147 = 146.922670 * IMAS_AMU, &
    IMAS_MASS_Ce_148 = 147.924430 * IMAS_AMU, &
    IMAS_MASS_Ce_149 = 148.92840 * IMAS_AMU, &
    IMAS_MASS_Ce_150 = 149.930410 * IMAS_AMU, &
    IMAS_MASS_Ce_151 = 150.93398 * IMAS_AMU, &
    IMAS_MASS_Ce_152 = 151.93654 * IMAS_AMU, &
    IMAS_MASS_Ce_153 = 152.94058 * IMAS_AMU, &
    IMAS_MASS_Ce_154 = 153.94342 * IMAS_AMU, &
    IMAS_MASS_Ce_155 = 154.94804 * IMAS_AMU, &
    IMAS_MASS_Ce_156 = 155.95126 * IMAS_AMU, &
    IMAS_MASS_Ce_157 = 156.95634 * IMAS_AMU, &
    IMAS_MASS_Pr_121 = 120.95536 * IMAS_AMU, &
    IMAS_MASS_Pr_122 = 121.95181 * IMAS_AMU, &
    IMAS_MASS_Pr_123 = 122.94596 * IMAS_AMU, &
    IMAS_MASS_Pr_124 = 123.94296 * IMAS_AMU, &
    IMAS_MASS_Pr_125 = 124.93783 * IMAS_AMU, &
    IMAS_MASS_Pr_126 = 125.93531 * IMAS_AMU, &
    IMAS_MASS_Pr_127 = 126.93083 * IMAS_AMU, &
    IMAS_MASS_Pr_128 = 127.928790 * IMAS_AMU, &
    IMAS_MASS_Pr_129 = 128.925100 * IMAS_AMU, &
    IMAS_MASS_Pr_130 = 129.923590 * IMAS_AMU, &
    IMAS_MASS_Pr_131 = 130.920260 * IMAS_AMU, &
    IMAS_MASS_Pr_132 = 131.919260 * IMAS_AMU, &
    IMAS_MASS_Pr_133 = 132.916331 * IMAS_AMU, &
    IMAS_MASS_Pr_134 = 133.915710 * IMAS_AMU, &
    IMAS_MASS_Pr_135 = 134.913112 * IMAS_AMU, &
    IMAS_MASS_Pr_136 = 135.912692 * IMAS_AMU, &
    IMAS_MASS_Pr_137 = 136.910705 * IMAS_AMU, &
    IMAS_MASS_Pr_138 = 137.910755 * IMAS_AMU, &
    IMAS_MASS_Pr_139 = 138.908938 * IMAS_AMU, &
    IMAS_MASS_Pr_140 = 139.909076 * IMAS_AMU, &
    IMAS_MASS_Pr_141 = 140.9076528 * IMAS_AMU, &
    IMAS_MASS_Pr_142 = 141.9100448 * IMAS_AMU, &
    IMAS_MASS_Pr_143 = 142.9108169 * IMAS_AMU, &
    IMAS_MASS_Pr_144 = 143.913305 * IMAS_AMU, &
    IMAS_MASS_Pr_145 = 144.914512 * IMAS_AMU, &
    IMAS_MASS_Pr_146 = 145.917640 * IMAS_AMU, &
    IMAS_MASS_Pr_147 = 146.918996 * IMAS_AMU, &
    IMAS_MASS_Pr_148 = 147.922135 * IMAS_AMU, &
    IMAS_MASS_Pr_149 = 148.923720 * IMAS_AMU, &
    IMAS_MASS_Pr_150 = 149.926673 * IMAS_AMU, &
    IMAS_MASS_Pr_151 = 150.928319 * IMAS_AMU, &
    IMAS_MASS_Pr_152 = 151.93150 * IMAS_AMU, &
    IMAS_MASS_Pr_153 = 152.93384 * IMAS_AMU, &
    IMAS_MASS_Pr_154 = 153.93752 * IMAS_AMU, &
    IMAS_MASS_Pr_155 = 154.94012 * IMAS_AMU, &
    IMAS_MASS_Pr_156 = 155.94427 * IMAS_AMU, &
    IMAS_MASS_Pr_157 = 156.94743 * IMAS_AMU, &
    IMAS_MASS_Pr_158 = 157.95198 * IMAS_AMU, &
    IMAS_MASS_Pr_159 = 158.95550 * IMAS_AMU, &
    IMAS_MASS_Nd_124 = 123.95223 * IMAS_AMU, &
    IMAS_MASS_Nd_125 = 124.94888 * IMAS_AMU, &
    IMAS_MASS_Nd_126 = 125.94322 * IMAS_AMU, &
    IMAS_MASS_Nd_127 = 126.94050 * IMAS_AMU, &
    IMAS_MASS_Nd_128 = 127.93539 * IMAS_AMU, &
    IMAS_MASS_Nd_129 = 128.93319 * IMAS_AMU, &
    IMAS_MASS_Nd_130 = 129.928510 * IMAS_AMU, &
    IMAS_MASS_Nd_131 = 130.927250 * IMAS_AMU, &
    IMAS_MASS_Nd_132 = 131.923321 * IMAS_AMU, &
    IMAS_MASS_Nd_133 = 132.922350 * IMAS_AMU, &
    IMAS_MASS_Nd_134 = 133.918790 * IMAS_AMU, &
    IMAS_MASS_Nd_135 = 134.918181 * IMAS_AMU, &
    IMAS_MASS_Nd_136 = 135.914976 * IMAS_AMU, &
    IMAS_MASS_Nd_137 = 136.914567 * IMAS_AMU, &
    IMAS_MASS_Nd_138 = 137.911950 * IMAS_AMU, &
    IMAS_MASS_Nd_139 = 138.911978 * IMAS_AMU, &
    IMAS_MASS_Nd_140 = 139.909550 * IMAS_AMU, &
    IMAS_MASS_Nd_141 = 140.909610 * IMAS_AMU, &
    IMAS_MASS_Nd_142 = 141.9077233 * IMAS_AMU, &
    IMAS_MASS_Nd_143 = 142.9098143 * IMAS_AMU, &
    IMAS_MASS_Nd_144 = 143.9100873 * IMAS_AMU, &
    IMAS_MASS_Nd_145 = 144.9125736 * IMAS_AMU, &
    IMAS_MASS_Nd_146 = 145.9131169 * IMAS_AMU, &
    IMAS_MASS_Nd_147 = 146.9161004 * IMAS_AMU, &
    IMAS_MASS_Nd_148 = 147.916893 * IMAS_AMU, &
    IMAS_MASS_Nd_149 = 148.920149 * IMAS_AMU, &
    IMAS_MASS_Nd_150 = 149.920891 * IMAS_AMU, &
    IMAS_MASS_Nd_151 = 150.923829 * IMAS_AMU, &
    IMAS_MASS_Nd_152 = 151.924682 * IMAS_AMU, &
    IMAS_MASS_Nd_153 = 152.927698 * IMAS_AMU, &
    IMAS_MASS_Nd_154 = 153.92948 * IMAS_AMU, &
    IMAS_MASS_Nd_155 = 154.93293 * IMAS_AMU, &
    IMAS_MASS_Nd_156 = 155.93502 * IMAS_AMU, &
    IMAS_MASS_Nd_157 = 156.93903 * IMAS_AMU, &
    IMAS_MASS_Nd_158 = 157.94160 * IMAS_AMU, &
    IMAS_MASS_Nd_159 = 158.94609 * IMAS_AMU, &
    IMAS_MASS_Nd_160 = 159.94909 * IMAS_AMU, &
    IMAS_MASS_Nd_161 = 160.95388 * IMAS_AMU, &
    IMAS_MASS_Pm_126 = 125.95752 * IMAS_AMU, &
    IMAS_MASS_Pm_127 = 126.95163 * IMAS_AMU, &
    IMAS_MASS_Pm_128 = 127.94842 * IMAS_AMU, &
    IMAS_MASS_Pm_129 = 128.94316 * IMAS_AMU, &
    IMAS_MASS_Pm_130 = 129.94045 * IMAS_AMU, &
    IMAS_MASS_Pm_131 = 130.93587 * IMAS_AMU, &
    IMAS_MASS_Pm_132 = 131.93375 * IMAS_AMU, &
    IMAS_MASS_Pm_133 = 132.929780 * IMAS_AMU, &
    IMAS_MASS_Pm_134 = 133.928350 * IMAS_AMU, &
    IMAS_MASS_Pm_135 = 134.924880 * IMAS_AMU, &
    IMAS_MASS_Pm_136 = 135.923570 * IMAS_AMU, &
    IMAS_MASS_Pm_137 = 136.920479 * IMAS_AMU, &
    IMAS_MASS_Pm_138 = 137.919548 * IMAS_AMU, &
    IMAS_MASS_Pm_139 = 138.916804 * IMAS_AMU, &
    IMAS_MASS_Pm_140 = 139.916040 * IMAS_AMU, &
    IMAS_MASS_Pm_141 = 140.913555 * IMAS_AMU, &
    IMAS_MASS_Pm_142 = 141.912874 * IMAS_AMU, &
    IMAS_MASS_Pm_143 = 142.910933 * IMAS_AMU, &
    IMAS_MASS_Pm_144 = 143.912591 * IMAS_AMU, &
    IMAS_MASS_Pm_145 = 144.912749 * IMAS_AMU, &
    IMAS_MASS_Pm_146 = 145.914696 * IMAS_AMU, &
    IMAS_MASS_Pm_147 = 146.9151385 * IMAS_AMU, &
    IMAS_MASS_Pm_148 = 147.917475 * IMAS_AMU, &
    IMAS_MASS_Pm_149 = 148.918334 * IMAS_AMU, &
    IMAS_MASS_Pm_150 = 149.920984 * IMAS_AMU, &
    IMAS_MASS_Pm_151 = 150.921207 * IMAS_AMU, &
    IMAS_MASS_Pm_152 = 151.923497 * IMAS_AMU, &
    IMAS_MASS_Pm_153 = 152.924117 * IMAS_AMU, &
    IMAS_MASS_Pm_154 = 153.926460 * IMAS_AMU, &
    IMAS_MASS_Pm_155 = 154.928100 * IMAS_AMU, &
    IMAS_MASS_Pm_156 = 155.931060 * IMAS_AMU, &
    IMAS_MASS_Pm_157 = 156.93304 * IMAS_AMU, &
    IMAS_MASS_Pm_158 = 157.93656 * IMAS_AMU, &
    IMAS_MASS_Pm_159 = 158.93897 * IMAS_AMU, &
    IMAS_MASS_Pm_160 = 159.94299 * IMAS_AMU, &
    IMAS_MASS_Pm_161 = 160.94586 * IMAS_AMU, &
    IMAS_MASS_Pm_162 = 161.95029 * IMAS_AMU, &
    IMAS_MASS_Pm_163 = 162.95368 * IMAS_AMU, &
    IMAS_MASS_Sm_128 = 127.95808 * IMAS_AMU, &
    IMAS_MASS_Sm_129 = 128.95464 * IMAS_AMU, &
    IMAS_MASS_Sm_130 = 129.94892 * IMAS_AMU, &
    IMAS_MASS_Sm_131 = 130.94611 * IMAS_AMU, &
    IMAS_MASS_Sm_132 = 131.94069 * IMAS_AMU, &
    IMAS_MASS_Sm_133 = 132.93867 * IMAS_AMU, &
    IMAS_MASS_Sm_134 = 133.93397 * IMAS_AMU, &
    IMAS_MASS_Sm_135 = 134.93252 * IMAS_AMU, &
    IMAS_MASS_Sm_136 = 135.928276 * IMAS_AMU, &
    IMAS_MASS_Sm_137 = 136.926970 * IMAS_AMU, &
    IMAS_MASS_Sm_138 = 137.923244 * IMAS_AMU, &
    IMAS_MASS_Sm_139 = 138.922297 * IMAS_AMU, &
    IMAS_MASS_Sm_140 = 139.918995 * IMAS_AMU, &
    IMAS_MASS_Sm_141 = 140.918476 * IMAS_AMU, &
    IMAS_MASS_Sm_142 = 141.915198 * IMAS_AMU, &
    IMAS_MASS_Sm_143 = 142.914628 * IMAS_AMU, &
    IMAS_MASS_Sm_144 = 143.911999 * IMAS_AMU, &
    IMAS_MASS_Sm_145 = 144.913410 * IMAS_AMU, &
    IMAS_MASS_Sm_146 = 145.913041 * IMAS_AMU, &
    IMAS_MASS_Sm_147 = 146.9148979 * IMAS_AMU, &
    IMAS_MASS_Sm_148 = 147.9148227 * IMAS_AMU, &
    IMAS_MASS_Sm_149 = 148.9171847 * IMAS_AMU, &
    IMAS_MASS_Sm_150 = 149.9172755 * IMAS_AMU, &
    IMAS_MASS_Sm_151 = 150.9199324 * IMAS_AMU, &
    IMAS_MASS_Sm_152 = 151.9197324 * IMAS_AMU, &
    IMAS_MASS_Sm_153 = 152.9220974 * IMAS_AMU, &
    IMAS_MASS_Sm_154 = 153.9222093 * IMAS_AMU, &
    IMAS_MASS_Sm_155 = 154.9246402 * IMAS_AMU, &
    IMAS_MASS_Sm_156 = 155.925528 * IMAS_AMU, &
    IMAS_MASS_Sm_157 = 156.928360 * IMAS_AMU, &
    IMAS_MASS_Sm_158 = 157.929990 * IMAS_AMU, &
    IMAS_MASS_Sm_159 = 158.93321 * IMAS_AMU, &
    IMAS_MASS_Sm_160 = 159.93514 * IMAS_AMU, &
    IMAS_MASS_Sm_161 = 160.93883 * IMAS_AMU, &
    IMAS_MASS_Sm_162 = 161.94122 * IMAS_AMU, &
    IMAS_MASS_Sm_163 = 162.94536 * IMAS_AMU, &
    IMAS_MASS_Sm_164 = 163.94828 * IMAS_AMU, &
    IMAS_MASS_Sm_165 = 164.95298 * IMAS_AMU, &
    IMAS_MASS_Eu_130 = 129.96357 * IMAS_AMU, &
    IMAS_MASS_Eu_131 = 130.95775 * IMAS_AMU, &
    IMAS_MASS_Eu_132 = 131.95437 * IMAS_AMU, &
    IMAS_MASS_Eu_133 = 132.94924 * IMAS_AMU, &
    IMAS_MASS_Eu_134 = 133.94651 * IMAS_AMU, &
    IMAS_MASS_Eu_135 = 134.94182 * IMAS_AMU, &
    IMAS_MASS_Eu_136 = 135.93960 * IMAS_AMU, &
    IMAS_MASS_Eu_137 = 136.93557 * IMAS_AMU, &
    IMAS_MASS_Eu_138 = 137.933710 * IMAS_AMU, &
    IMAS_MASS_Eu_139 = 138.929792 * IMAS_AMU, &
    IMAS_MASS_Eu_140 = 139.928090 * IMAS_AMU, &
    IMAS_MASS_Eu_141 = 140.924931 * IMAS_AMU, &
    IMAS_MASS_Eu_142 = 141.923430 * IMAS_AMU, &
    IMAS_MASS_Eu_143 = 142.920298 * IMAS_AMU, &
    IMAS_MASS_Eu_144 = 143.918817 * IMAS_AMU, &
    IMAS_MASS_Eu_145 = 144.916265 * IMAS_AMU, &
    IMAS_MASS_Eu_146 = 145.917206 * IMAS_AMU, &
    IMAS_MASS_Eu_147 = 146.916746 * IMAS_AMU, &
    IMAS_MASS_Eu_148 = 147.918086 * IMAS_AMU, &
    IMAS_MASS_Eu_149 = 148.917931 * IMAS_AMU, &
    IMAS_MASS_Eu_150 = 149.919702 * IMAS_AMU, &
    IMAS_MASS_Eu_151 = 150.9198502 * IMAS_AMU, &
    IMAS_MASS_Eu_152 = 151.9217445 * IMAS_AMU, &
    IMAS_MASS_Eu_153 = 152.9212303 * IMAS_AMU, &
    IMAS_MASS_Eu_154 = 153.9229792 * IMAS_AMU, &
    IMAS_MASS_Eu_155 = 154.9228933 * IMAS_AMU, &
    IMAS_MASS_Eu_156 = 155.924752 * IMAS_AMU, &
    IMAS_MASS_Eu_157 = 156.925424 * IMAS_AMU, &
    IMAS_MASS_Eu_158 = 157.927850 * IMAS_AMU, &
    IMAS_MASS_Eu_159 = 158.929089 * IMAS_AMU, &
    IMAS_MASS_Eu_160 = 159.93197 * IMAS_AMU, &
    IMAS_MASS_Eu_161 = 160.93368 * IMAS_AMU, &
    IMAS_MASS_Eu_162 = 161.93704 * IMAS_AMU, &
    IMAS_MASS_Eu_163 = 162.93921 * IMAS_AMU, &
    IMAS_MASS_Eu_164 = 163.94299 * IMAS_AMU, &
    IMAS_MASS_Eu_165 = 164.94572 * IMAS_AMU, &
    IMAS_MASS_Eu_166 = 165.94997 * IMAS_AMU, &
    IMAS_MASS_Eu_167 = 166.95321 * IMAS_AMU, &
    IMAS_MASS_Gd_134 = 133.95537 * IMAS_AMU, &
    IMAS_MASS_Gd_135 = 134.95257 * IMAS_AMU, &
    IMAS_MASS_Gd_136 = 135.94734 * IMAS_AMU, &
    IMAS_MASS_Gd_137 = 136.94502 * IMAS_AMU, &
    IMAS_MASS_Gd_138 = 137.94012 * IMAS_AMU, &
    IMAS_MASS_Gd_139 = 138.93824 * IMAS_AMU, &
    IMAS_MASS_Gd_140 = 139.933670 * IMAS_AMU, &
    IMAS_MASS_Gd_141 = 140.932126 * IMAS_AMU, &
    IMAS_MASS_Gd_142 = 141.928120 * IMAS_AMU, &
    IMAS_MASS_Gd_143 = 142.92675 * IMAS_AMU, &
    IMAS_MASS_Gd_144 = 143.922960 * IMAS_AMU, &
    IMAS_MASS_Gd_145 = 144.921709 * IMAS_AMU, &
    IMAS_MASS_Gd_146 = 145.918311 * IMAS_AMU, &
    IMAS_MASS_Gd_147 = 146.919094 * IMAS_AMU, &
    IMAS_MASS_Gd_148 = 147.918115 * IMAS_AMU, &
    IMAS_MASS_Gd_149 = 148.919341 * IMAS_AMU, &
    IMAS_MASS_Gd_150 = 149.918659 * IMAS_AMU, &
    IMAS_MASS_Gd_151 = 150.920348 * IMAS_AMU, &
    IMAS_MASS_Gd_152 = 151.9197910 * IMAS_AMU, &
    IMAS_MASS_Gd_153 = 152.9217495 * IMAS_AMU, &
    IMAS_MASS_Gd_154 = 153.9208656 * IMAS_AMU, &
    IMAS_MASS_Gd_155 = 154.9226220 * IMAS_AMU, &
    IMAS_MASS_Gd_156 = 155.9221227 * IMAS_AMU, &
    IMAS_MASS_Gd_157 = 156.9239601 * IMAS_AMU, &
    IMAS_MASS_Gd_158 = 157.9241039 * IMAS_AMU, &
    IMAS_MASS_Gd_159 = 158.9263887 * IMAS_AMU, &
    IMAS_MASS_Gd_160 = 159.9270541 * IMAS_AMU, &
    IMAS_MASS_Gd_161 = 160.9296692 * IMAS_AMU, &
    IMAS_MASS_Gd_162 = 161.930985 * IMAS_AMU, &
    IMAS_MASS_Gd_163 = 162.93399 * IMAS_AMU, &
    IMAS_MASS_Gd_164 = 163.93586 * IMAS_AMU, &
    IMAS_MASS_Gd_165 = 164.93938 * IMAS_AMU, &
    IMAS_MASS_Gd_166 = 165.94160 * IMAS_AMU, &
    IMAS_MASS_Gd_167 = 166.94557 * IMAS_AMU, &
    IMAS_MASS_Gd_168 = 167.94836 * IMAS_AMU, &
    IMAS_MASS_Gd_169 = 168.95287 * IMAS_AMU, &
    IMAS_MASS_Tb_136 = 135.96138 * IMAS_AMU, &
    IMAS_MASS_Tb_137 = 136.95598 * IMAS_AMU, &
    IMAS_MASS_Tb_138 = 137.95316 * IMAS_AMU, &
    IMAS_MASS_Tb_139 = 138.94829 * IMAS_AMU, &
    IMAS_MASS_Tb_140 = 139.94581 * IMAS_AMU, &
    IMAS_MASS_Tb_141 = 140.94145 * IMAS_AMU, &
    IMAS_MASS_Tb_142 = 141.93874 * IMAS_AMU, &
    IMAS_MASS_Tb_143 = 142.935120 * IMAS_AMU, &
    IMAS_MASS_Tb_144 = 143.933050 * IMAS_AMU, &
    IMAS_MASS_Tb_145 = 144.929270 * IMAS_AMU, &
    IMAS_MASS_Tb_146 = 145.927250 * IMAS_AMU, &
    IMAS_MASS_Tb_147 = 146.924045 * IMAS_AMU, &
    IMAS_MASS_Tb_148 = 147.924272 * IMAS_AMU, &
    IMAS_MASS_Tb_149 = 148.923246 * IMAS_AMU, &
    IMAS_MASS_Tb_150 = 149.923660 * IMAS_AMU, &
    IMAS_MASS_Tb_151 = 150.923103 * IMAS_AMU, &
    IMAS_MASS_Tb_152 = 151.924070 * IMAS_AMU, &
    IMAS_MASS_Tb_153 = 152.923435 * IMAS_AMU, &
    IMAS_MASS_Tb_154 = 153.924680 * IMAS_AMU, &
    IMAS_MASS_Tb_155 = 154.923505 * IMAS_AMU, &
    IMAS_MASS_Tb_156 = 155.924747 * IMAS_AMU, &
    IMAS_MASS_Tb_157 = 156.9240246 * IMAS_AMU, &
    IMAS_MASS_Tb_158 = 157.9254131 * IMAS_AMU, &
    IMAS_MASS_Tb_159 = 158.9253468 * IMAS_AMU, &
    IMAS_MASS_Tb_160 = 159.9271676 * IMAS_AMU, &
    IMAS_MASS_Tb_161 = 160.9275699 * IMAS_AMU, &
    IMAS_MASS_Tb_162 = 161.929490 * IMAS_AMU, &
    IMAS_MASS_Tb_163 = 162.930648 * IMAS_AMU, &
    IMAS_MASS_Tb_164 = 163.93335 * IMAS_AMU, &
    IMAS_MASS_Tb_165 = 164.93488 * IMAS_AMU, &
    IMAS_MASS_Tb_166 = 165.93799 * IMAS_AMU, &
    IMAS_MASS_Tb_167 = 166.94005 * IMAS_AMU, &
    IMAS_MASS_Tb_168 = 167.94364 * IMAS_AMU, &
    IMAS_MASS_Tb_169 = 168.94622 * IMAS_AMU, &
    IMAS_MASS_Tb_170 = 169.95025 * IMAS_AMU, &
    IMAS_MASS_Tb_171 = 170.95330 * IMAS_AMU, &
    IMAS_MASS_Dy_138 = 137.96249 * IMAS_AMU, &
    IMAS_MASS_Dy_139 = 138.95954 * IMAS_AMU, &
    IMAS_MASS_Dy_140 = 139.95401 * IMAS_AMU, &
    IMAS_MASS_Dy_141 = 140.95135 * IMAS_AMU, &
    IMAS_MASS_Dy_142 = 141.94637 * IMAS_AMU, &
    IMAS_MASS_Dy_143 = 142.94383 * IMAS_AMU, &
    IMAS_MASS_Dy_144 = 143.939250 * IMAS_AMU, &
    IMAS_MASS_Dy_145 = 144.937430 * IMAS_AMU, &
    IMAS_MASS_Dy_146 = 145.932845 * IMAS_AMU, &
    IMAS_MASS_Dy_147 = 146.931092 * IMAS_AMU, &
    IMAS_MASS_Dy_148 = 147.927150 * IMAS_AMU, &
    IMAS_MASS_Dy_149 = 148.927305 * IMAS_AMU, &
    IMAS_MASS_Dy_150 = 149.925585 * IMAS_AMU, &
    IMAS_MASS_Dy_151 = 150.926185 * IMAS_AMU, &
    IMAS_MASS_Dy_152 = 151.924718 * IMAS_AMU, &
    IMAS_MASS_Dy_153 = 152.925765 * IMAS_AMU, &
    IMAS_MASS_Dy_154 = 153.924424 * IMAS_AMU, &
    IMAS_MASS_Dy_155 = 154.925754 * IMAS_AMU, &
    IMAS_MASS_Dy_156 = 155.924283 * IMAS_AMU, &
    IMAS_MASS_Dy_157 = 156.925466 * IMAS_AMU, &
    IMAS_MASS_Dy_158 = 157.924409 * IMAS_AMU, &
    IMAS_MASS_Dy_159 = 158.9257392 * IMAS_AMU, &
    IMAS_MASS_Dy_160 = 159.9251975 * IMAS_AMU, &
    IMAS_MASS_Dy_161 = 160.9269334 * IMAS_AMU, &
    IMAS_MASS_Dy_162 = 161.9267984 * IMAS_AMU, &
    IMAS_MASS_Dy_163 = 162.9287312 * IMAS_AMU, &
    IMAS_MASS_Dy_164 = 163.9291748 * IMAS_AMU, &
    IMAS_MASS_Dy_165 = 164.9317033 * IMAS_AMU, &
    IMAS_MASS_Dy_166 = 165.9328067 * IMAS_AMU, &
    IMAS_MASS_Dy_167 = 166.935660 * IMAS_AMU, &
    IMAS_MASS_Dy_168 = 167.93713 * IMAS_AMU, &
    IMAS_MASS_Dy_169 = 168.94031 * IMAS_AMU, &
    IMAS_MASS_Dy_170 = 169.94239 * IMAS_AMU, &
    IMAS_MASS_Dy_171 = 170.94620 * IMAS_AMU, &
    IMAS_MASS_Dy_172 = 171.94876 * IMAS_AMU, &
    IMAS_MASS_Dy_173 = 172.95300 * IMAS_AMU, &
    IMAS_MASS_Ho_140 = 139.96854 * IMAS_AMU, &
    IMAS_MASS_Ho_141 = 140.96310 * IMAS_AMU, &
    IMAS_MASS_Ho_142 = 141.95977 * IMAS_AMU, &
    IMAS_MASS_Ho_143 = 142.95461 * IMAS_AMU, &
    IMAS_MASS_Ho_144 = 143.95148 * IMAS_AMU, &
    IMAS_MASS_Ho_145 = 144.94720 * IMAS_AMU, &
    IMAS_MASS_Ho_146 = 145.94464 * IMAS_AMU, &
    IMAS_MASS_Ho_147 = 146.940060 * IMAS_AMU, &
    IMAS_MASS_Ho_148 = 147.93772 * IMAS_AMU, &
    IMAS_MASS_Ho_149 = 148.933775 * IMAS_AMU, &
    IMAS_MASS_Ho_150 = 149.933496 * IMAS_AMU, &
    IMAS_MASS_Ho_151 = 150.931688 * IMAS_AMU, &
    IMAS_MASS_Ho_152 = 151.931714 * IMAS_AMU, &
    IMAS_MASS_Ho_153 = 152.930199 * IMAS_AMU, &
    IMAS_MASS_Ho_154 = 153.930602 * IMAS_AMU, &
    IMAS_MASS_Ho_155 = 154.929103 * IMAS_AMU, &
    IMAS_MASS_Ho_156 = 155.929840 * IMAS_AMU, &
    IMAS_MASS_Ho_157 = 156.928256 * IMAS_AMU, &
    IMAS_MASS_Ho_158 = 157.928941 * IMAS_AMU, &
    IMAS_MASS_Ho_159 = 158.927712 * IMAS_AMU, &
    IMAS_MASS_Ho_160 = 159.928729 * IMAS_AMU, &
    IMAS_MASS_Ho_161 = 160.927855 * IMAS_AMU, &
    IMAS_MASS_Ho_162 = 161.929096 * IMAS_AMU, &
    IMAS_MASS_Ho_163 = 162.9287339 * IMAS_AMU, &
    IMAS_MASS_Ho_164 = 163.9302335 * IMAS_AMU, &
    IMAS_MASS_Ho_165 = 164.9303221 * IMAS_AMU, &
    IMAS_MASS_Ho_166 = 165.9322842 * IMAS_AMU, &
    IMAS_MASS_Ho_167 = 166.933133 * IMAS_AMU, &
    IMAS_MASS_Ho_168 = 167.935520 * IMAS_AMU, &
    IMAS_MASS_Ho_169 = 168.936872 * IMAS_AMU, &
    IMAS_MASS_Ho_170 = 169.939620 * IMAS_AMU, &
    IMAS_MASS_Ho_171 = 170.94147 * IMAS_AMU, &
    IMAS_MASS_Ho_172 = 171.94482 * IMAS_AMU, &
    IMAS_MASS_Ho_173 = 172.94729 * IMAS_AMU, &
    IMAS_MASS_Ho_174 = 173.95115 * IMAS_AMU, &
    IMAS_MASS_Ho_175 = 174.95405 * IMAS_AMU, &
    IMAS_MASS_Er_143 = 142.96634 * IMAS_AMU, &
    IMAS_MASS_Er_144 = 143.96038 * IMAS_AMU, &
    IMAS_MASS_Er_145 = 144.95739 * IMAS_AMU, &
    IMAS_MASS_Er_146 = 145.95200 * IMAS_AMU, &
    IMAS_MASS_Er_147 = 146.94949 * IMAS_AMU, &
    IMAS_MASS_Er_148 = 147.94455 * IMAS_AMU, &
    IMAS_MASS_Er_149 = 148.942310 * IMAS_AMU, &
    IMAS_MASS_Er_150 = 149.937914 * IMAS_AMU, &
    IMAS_MASS_Er_151 = 150.937449 * IMAS_AMU, &
    IMAS_MASS_Er_152 = 151.935050 * IMAS_AMU, &
    IMAS_MASS_Er_153 = 152.935063 * IMAS_AMU, &
    IMAS_MASS_Er_154 = 153.932783 * IMAS_AMU, &
    IMAS_MASS_Er_155 = 154.933209 * IMAS_AMU, &
    IMAS_MASS_Er_156 = 155.931065 * IMAS_AMU, &
    IMAS_MASS_Er_157 = 156.931920 * IMAS_AMU, &
    IMAS_MASS_Er_158 = 157.929893 * IMAS_AMU, &
    IMAS_MASS_Er_159 = 158.930684 * IMAS_AMU, &
    IMAS_MASS_Er_160 = 159.929083 * IMAS_AMU, &
    IMAS_MASS_Er_161 = 160.929995 * IMAS_AMU, &
    IMAS_MASS_Er_162 = 161.928778 * IMAS_AMU, &
    IMAS_MASS_Er_163 = 162.930033 * IMAS_AMU, &
    IMAS_MASS_Er_164 = 163.929200 * IMAS_AMU, &
    IMAS_MASS_Er_165 = 164.930726 * IMAS_AMU, &
    IMAS_MASS_Er_166 = 165.9302931 * IMAS_AMU, &
    IMAS_MASS_Er_167 = 166.9320482 * IMAS_AMU, &
    IMAS_MASS_Er_168 = 167.9323702 * IMAS_AMU, &
    IMAS_MASS_Er_169 = 168.9345904 * IMAS_AMU, &
    IMAS_MASS_Er_170 = 169.9354643 * IMAS_AMU, &
    IMAS_MASS_Er_171 = 170.9380298 * IMAS_AMU, &
    IMAS_MASS_Er_172 = 171.939356 * IMAS_AMU, &
    IMAS_MASS_Er_173 = 172.94240 * IMAS_AMU, &
    IMAS_MASS_Er_174 = 173.94423 * IMAS_AMU, &
    IMAS_MASS_Er_175 = 174.94777 * IMAS_AMU, &
    IMAS_MASS_Er_176 = 175.95008 * IMAS_AMU, &
    IMAS_MASS_Er_177 = 176.95405 * IMAS_AMU, &
    IMAS_MASS_Tm_145 = 144.97007 * IMAS_AMU, &
    IMAS_MASS_Tm_146 = 145.96643 * IMAS_AMU, &
    IMAS_MASS_Tm_147 = 146.96096 * IMAS_AMU, &
    IMAS_MASS_Tm_148 = 147.95784 * IMAS_AMU, &
    IMAS_MASS_Tm_149 = 148.95272 * IMAS_AMU, &
    IMAS_MASS_Tm_150 = 149.94996 * IMAS_AMU, &
    IMAS_MASS_Tm_151 = 150.945483 * IMAS_AMU, &
    IMAS_MASS_Tm_152 = 151.944420 * IMAS_AMU, &
    IMAS_MASS_Tm_153 = 152.942012 * IMAS_AMU, &
    IMAS_MASS_Tm_154 = 153.941568 * IMAS_AMU, &
    IMAS_MASS_Tm_155 = 154.939199 * IMAS_AMU, &
    IMAS_MASS_Tm_156 = 155.938980 * IMAS_AMU, &
    IMAS_MASS_Tm_157 = 156.936970 * IMAS_AMU, &
    IMAS_MASS_Tm_158 = 157.936980 * IMAS_AMU, &
    IMAS_MASS_Tm_159 = 158.934980 * IMAS_AMU, &
    IMAS_MASS_Tm_160 = 159.935260 * IMAS_AMU, &
    IMAS_MASS_Tm_161 = 160.933550 * IMAS_AMU, &
    IMAS_MASS_Tm_162 = 161.933995 * IMAS_AMU, &
    IMAS_MASS_Tm_163 = 162.932651 * IMAS_AMU, &
    IMAS_MASS_Tm_164 = 163.933560 * IMAS_AMU, &
    IMAS_MASS_Tm_165 = 164.932435 * IMAS_AMU, &
    IMAS_MASS_Tm_166 = 165.933554 * IMAS_AMU, &
    IMAS_MASS_Tm_167 = 166.9328516 * IMAS_AMU, &
    IMAS_MASS_Tm_168 = 167.934173 * IMAS_AMU, &
    IMAS_MASS_Tm_169 = 168.9342133 * IMAS_AMU, &
    IMAS_MASS_Tm_170 = 169.9358014 * IMAS_AMU, &
    IMAS_MASS_Tm_171 = 170.9364294 * IMAS_AMU, &
    IMAS_MASS_Tm_172 = 171.938400 * IMAS_AMU, &
    IMAS_MASS_Tm_173 = 172.939604 * IMAS_AMU, &
    IMAS_MASS_Tm_174 = 173.942170 * IMAS_AMU, &
    IMAS_MASS_Tm_175 = 174.943840 * IMAS_AMU, &
    IMAS_MASS_Tm_176 = 175.94699 * IMAS_AMU, &
    IMAS_MASS_Tm_177 = 176.94904 * IMAS_AMU, &
    IMAS_MASS_Tm_178 = 177.95264 * IMAS_AMU, &
    IMAS_MASS_Tm_179 = 178.95534 * IMAS_AMU, &
    IMAS_MASS_Yb_148 = 147.96742 * IMAS_AMU, &
    IMAS_MASS_Yb_149 = 148.96404 * IMAS_AMU, &
    IMAS_MASS_Yb_150 = 149.95842 * IMAS_AMU, &
    IMAS_MASS_Yb_151 = 150.95540 * IMAS_AMU, &
    IMAS_MASS_Yb_152 = 151.95029 * IMAS_AMU, &
    IMAS_MASS_Yb_153 = 152.94948 * IMAS_AMU, &
    IMAS_MASS_Yb_154 = 153.946394 * IMAS_AMU, &
    IMAS_MASS_Yb_155 = 154.945782 * IMAS_AMU, &
    IMAS_MASS_Yb_156 = 155.942818 * IMAS_AMU, &
    IMAS_MASS_Yb_157 = 156.942628 * IMAS_AMU, &
    IMAS_MASS_Yb_158 = 157.939866 * IMAS_AMU, &
    IMAS_MASS_Yb_159 = 158.940050 * IMAS_AMU, &
    IMAS_MASS_Yb_160 = 159.937552 * IMAS_AMU, &
    IMAS_MASS_Yb_161 = 160.937902 * IMAS_AMU, &
    IMAS_MASS_Yb_162 = 161.935768 * IMAS_AMU, &
    IMAS_MASS_Yb_163 = 162.936334 * IMAS_AMU, &
    IMAS_MASS_Yb_164 = 163.934489 * IMAS_AMU, &
    IMAS_MASS_Yb_165 = 164.935280 * IMAS_AMU, &
    IMAS_MASS_Yb_166 = 165.933882 * IMAS_AMU, &
    IMAS_MASS_Yb_167 = 166.934950 * IMAS_AMU, &
    IMAS_MASS_Yb_168 = 167.933897 * IMAS_AMU, &
    IMAS_MASS_Yb_169 = 168.935190 * IMAS_AMU, &
    IMAS_MASS_Yb_170 = 169.9347618 * IMAS_AMU, &
    IMAS_MASS_Yb_171 = 170.9363258 * IMAS_AMU, &
    IMAS_MASS_Yb_172 = 171.9363815 * IMAS_AMU, &
    IMAS_MASS_Yb_173 = 172.9382108 * IMAS_AMU, &
    IMAS_MASS_Yb_174 = 173.9388621 * IMAS_AMU, &
    IMAS_MASS_Yb_175 = 174.9412765 * IMAS_AMU, &
    IMAS_MASS_Yb_176 = 175.9425717 * IMAS_AMU, &
    IMAS_MASS_Yb_177 = 176.9452608 * IMAS_AMU, &
    IMAS_MASS_Yb_178 = 177.946647 * IMAS_AMU, &
    IMAS_MASS_Yb_179 = 178.95017 * IMAS_AMU, &
    IMAS_MASS_Yb_180 = 179.95233 * IMAS_AMU, &
    IMAS_MASS_Yb_181 = 180.95615 * IMAS_AMU, &
    IMAS_MASS_Lu_150 = 149.97323 * IMAS_AMU, &
    IMAS_MASS_Lu_151 = 150.96758 * IMAS_AMU, &
    IMAS_MASS_Lu_152 = 151.96412 * IMAS_AMU, &
    IMAS_MASS_Lu_153 = 152.95877 * IMAS_AMU, &
    IMAS_MASS_Lu_154 = 153.95752 * IMAS_AMU, &
    IMAS_MASS_Lu_155 = 154.954316 * IMAS_AMU, &
    IMAS_MASS_Lu_156 = 155.953030 * IMAS_AMU, &
    IMAS_MASS_Lu_157 = 156.950098 * IMAS_AMU, &
    IMAS_MASS_Lu_158 = 157.949313 * IMAS_AMU, &
    IMAS_MASS_Lu_159 = 158.946630 * IMAS_AMU, &
    IMAS_MASS_Lu_160 = 159.946030 * IMAS_AMU, &
    IMAS_MASS_Lu_161 = 160.943570 * IMAS_AMU, &
    IMAS_MASS_Lu_162 = 161.943280 * IMAS_AMU, &
    IMAS_MASS_Lu_163 = 162.941180 * IMAS_AMU, &
    IMAS_MASS_Lu_164 = 163.941340 * IMAS_AMU, &
    IMAS_MASS_Lu_165 = 164.939407 * IMAS_AMU, &
    IMAS_MASS_Lu_166 = 165.939860 * IMAS_AMU, &
    IMAS_MASS_Lu_167 = 166.938270 * IMAS_AMU, &
    IMAS_MASS_Lu_168 = 167.938740 * IMAS_AMU, &
    IMAS_MASS_Lu_169 = 168.937651 * IMAS_AMU, &
    IMAS_MASS_Lu_170 = 169.938475 * IMAS_AMU, &
    IMAS_MASS_Lu_171 = 170.9379131 * IMAS_AMU, &
    IMAS_MASS_Lu_172 = 171.939086 * IMAS_AMU, &
    IMAS_MASS_Lu_173 = 172.9389306 * IMAS_AMU, &
    IMAS_MASS_Lu_174 = 173.9403375 * IMAS_AMU, &
    IMAS_MASS_Lu_175 = 174.9407718 * IMAS_AMU, &
    IMAS_MASS_Lu_176 = 175.9426863 * IMAS_AMU, &
    IMAS_MASS_Lu_177 = 176.9437581 * IMAS_AMU, &
    IMAS_MASS_Lu_178 = 177.945955 * IMAS_AMU, &
    IMAS_MASS_Lu_179 = 178.947327 * IMAS_AMU, &
    IMAS_MASS_Lu_180 = 179.949880 * IMAS_AMU, &
    IMAS_MASS_Lu_181 = 180.95197 * IMAS_AMU, &
    IMAS_MASS_Lu_182 = 181.95504 * IMAS_AMU, &
    IMAS_MASS_Lu_183 = 182.95757 * IMAS_AMU, &
    IMAS_MASS_Lu_184 = 183.96091 * IMAS_AMU, &
    IMAS_MASS_Hf_153 = 152.97069 * IMAS_AMU, &
    IMAS_MASS_Hf_154 = 153.96486 * IMAS_AMU, &
    IMAS_MASS_Hf_155 = 154.96339 * IMAS_AMU, &
    IMAS_MASS_Hf_156 = 155.95936 * IMAS_AMU, &
    IMAS_MASS_Hf_157 = 156.95840 * IMAS_AMU, &
    IMAS_MASS_Hf_158 = 157.954799 * IMAS_AMU, &
    IMAS_MASS_Hf_159 = 158.953995 * IMAS_AMU, &
    IMAS_MASS_Hf_160 = 159.950684 * IMAS_AMU, &
    IMAS_MASS_Hf_161 = 160.950275 * IMAS_AMU, &
    IMAS_MASS_Hf_162 = 161.947210 * IMAS_AMU, &
    IMAS_MASS_Hf_163 = 162.947090 * IMAS_AMU, &
    IMAS_MASS_Hf_164 = 163.944367 * IMAS_AMU, &
    IMAS_MASS_Hf_165 = 164.944570 * IMAS_AMU, &
    IMAS_MASS_Hf_166 = 165.942180 * IMAS_AMU, &
    IMAS_MASS_Hf_167 = 166.942600 * IMAS_AMU, &
    IMAS_MASS_Hf_168 = 167.940570 * IMAS_AMU, &
    IMAS_MASS_Hf_169 = 168.941260 * IMAS_AMU, &
    IMAS_MASS_Hf_170 = 169.939610 * IMAS_AMU, &
    IMAS_MASS_Hf_171 = 170.940490 * IMAS_AMU, &
    IMAS_MASS_Hf_172 = 171.939448 * IMAS_AMU, &
    IMAS_MASS_Hf_173 = 172.940510 * IMAS_AMU, &
    IMAS_MASS_Hf_174 = 173.940046 * IMAS_AMU, &
    IMAS_MASS_Hf_175 = 174.941509 * IMAS_AMU, &
    IMAS_MASS_Hf_176 = 175.9414086 * IMAS_AMU, &
    IMAS_MASS_Hf_177 = 176.9432207 * IMAS_AMU, &
    IMAS_MASS_Hf_178 = 177.9436988 * IMAS_AMU, &
    IMAS_MASS_Hf_179 = 178.9458161 * IMAS_AMU, &
    IMAS_MASS_Hf_180 = 179.9465500 * IMAS_AMU, &
    IMAS_MASS_Hf_181 = 180.9491012 * IMAS_AMU, &
    IMAS_MASS_Hf_182 = 181.950554 * IMAS_AMU, &
    IMAS_MASS_Hf_183 = 182.953530 * IMAS_AMU, &
    IMAS_MASS_Hf_184 = 183.955450 * IMAS_AMU, &
    IMAS_MASS_Hf_185 = 184.95882 * IMAS_AMU, &
    IMAS_MASS_Hf_186 = 185.96089 * IMAS_AMU, &
    IMAS_MASS_Hf_187 = 186.96459 * IMAS_AMU, &
    IMAS_MASS_Hf_188 = 187.96685 * IMAS_AMU, &
    IMAS_MASS_Ta_155 = 154.97459 * IMAS_AMU, &
    IMAS_MASS_Ta_156 = 155.97230 * IMAS_AMU, &
    IMAS_MASS_Ta_157 = 156.96819 * IMAS_AMU, &
    IMAS_MASS_Ta_158 = 157.96670 * IMAS_AMU, &
    IMAS_MASS_Ta_159 = 158.963018 * IMAS_AMU, &
    IMAS_MASS_Ta_160 = 159.96149 * IMAS_AMU, &
    IMAS_MASS_Ta_161 = 160.958420 * IMAS_AMU, &
    IMAS_MASS_Ta_162 = 161.957290 * IMAS_AMU, &
    IMAS_MASS_Ta_163 = 162.954330 * IMAS_AMU, &
    IMAS_MASS_Ta_164 = 163.953530 * IMAS_AMU, &
    IMAS_MASS_Ta_165 = 164.950773 * IMAS_AMU, &
    IMAS_MASS_Ta_166 = 165.950510 * IMAS_AMU, &
    IMAS_MASS_Ta_167 = 166.948090 * IMAS_AMU, &
    IMAS_MASS_Ta_168 = 167.948050 * IMAS_AMU, &
    IMAS_MASS_Ta_169 = 168.946010 * IMAS_AMU, &
    IMAS_MASS_Ta_170 = 169.946180 * IMAS_AMU, &
    IMAS_MASS_Ta_171 = 170.944480 * IMAS_AMU, &
    IMAS_MASS_Ta_172 = 171.944900 * IMAS_AMU, &
    IMAS_MASS_Ta_173 = 172.943750 * IMAS_AMU, &
    IMAS_MASS_Ta_174 = 173.944450 * IMAS_AMU, &
    IMAS_MASS_Ta_175 = 174.943740 * IMAS_AMU, &
    IMAS_MASS_Ta_176 = 175.944860 * IMAS_AMU, &
    IMAS_MASS_Ta_177 = 176.944472 * IMAS_AMU, &
    IMAS_MASS_Ta_178 = 177.945778 * IMAS_AMU, &
    IMAS_MASS_Ta_179 = 178.9459295 * IMAS_AMU, &
    IMAS_MASS_Ta_180 = 179.9474648 * IMAS_AMU, &
    IMAS_MASS_Ta_181 = 180.9479958 * IMAS_AMU, &
    IMAS_MASS_Ta_182 = 181.9501518 * IMAS_AMU, &
    IMAS_MASS_Ta_183 = 182.9513726 * IMAS_AMU, &
    IMAS_MASS_Ta_184 = 183.954008 * IMAS_AMU, &
    IMAS_MASS_Ta_185 = 184.955559 * IMAS_AMU, &
    IMAS_MASS_Ta_186 = 185.958550 * IMAS_AMU, &
    IMAS_MASS_Ta_187 = 186.96053 * IMAS_AMU, &
    IMAS_MASS_Ta_188 = 187.96370 * IMAS_AMU, &
    IMAS_MASS_Ta_189 = 188.96583 * IMAS_AMU, &
    IMAS_MASS_Ta_190 = 189.96923 * IMAS_AMU, &
    IMAS_MASS_W_158 = 157.97456 * IMAS_AMU, &
    IMAS_MASS_W_159 = 158.97292 * IMAS_AMU, &
    IMAS_MASS_W_160 = 159.96848 * IMAS_AMU, &
    IMAS_MASS_W_161 = 160.96736 * IMAS_AMU, &
    IMAS_MASS_W_162 = 161.963497 * IMAS_AMU, &
    IMAS_MASS_W_163 = 162.962520 * IMAS_AMU, &
    IMAS_MASS_W_164 = 163.958954 * IMAS_AMU, &
    IMAS_MASS_W_165 = 164.958280 * IMAS_AMU, &
    IMAS_MASS_W_166 = 165.955027 * IMAS_AMU, &
    IMAS_MASS_W_167 = 166.954816 * IMAS_AMU, &
    IMAS_MASS_W_168 = 167.951808 * IMAS_AMU, &
    IMAS_MASS_W_169 = 168.951779 * IMAS_AMU, &
    IMAS_MASS_W_170 = 169.949228 * IMAS_AMU, &
    IMAS_MASS_W_171 = 170.949450 * IMAS_AMU, &
    IMAS_MASS_W_172 = 171.947290 * IMAS_AMU, &
    IMAS_MASS_W_173 = 172.947690 * IMAS_AMU, &
    IMAS_MASS_W_174 = 173.946080 * IMAS_AMU, &
    IMAS_MASS_W_175 = 174.946720 * IMAS_AMU, &
    IMAS_MASS_W_176 = 175.945630 * IMAS_AMU, &
    IMAS_MASS_W_177 = 176.946640 * IMAS_AMU, &
    IMAS_MASS_W_178 = 177.945876 * IMAS_AMU, &
    IMAS_MASS_W_179 = 178.947070 * IMAS_AMU, &
    IMAS_MASS_W_180 = 179.946704 * IMAS_AMU, &
    IMAS_MASS_W_181 = 180.948197 * IMAS_AMU, &
    IMAS_MASS_W_182 = 181.9482042 * IMAS_AMU, &
    IMAS_MASS_W_183 = 182.9502230 * IMAS_AMU, &
    IMAS_MASS_W_184 = 183.9509312 * IMAS_AMU, &
    IMAS_MASS_W_185 = 184.9534193 * IMAS_AMU, &
    IMAS_MASS_W_186 = 185.9543641 * IMAS_AMU, &
    IMAS_MASS_W_187 = 186.9571605 * IMAS_AMU, &
    IMAS_MASS_W_188 = 187.958489 * IMAS_AMU, &
    IMAS_MASS_W_189 = 188.96191 * IMAS_AMU, &
    IMAS_MASS_W_190 = 189.96318 * IMAS_AMU, &
    IMAS_MASS_W_191 = 190.96660 * IMAS_AMU, &
    IMAS_MASS_W_192 = 191.96817 * IMAS_AMU, &
    IMAS_MASS_Re_160 = 159.98212 * IMAS_AMU, &
    IMAS_MASS_Re_161 = 160.97759 * IMAS_AMU, &
    IMAS_MASS_Re_162 = 161.97600 * IMAS_AMU, &
    IMAS_MASS_Re_163 = 162.972081 * IMAS_AMU, &
    IMAS_MASS_Re_164 = 163.97032 * IMAS_AMU, &
    IMAS_MASS_Re_165 = 164.967089 * IMAS_AMU, &
    IMAS_MASS_Re_166 = 165.965810 * IMAS_AMU, &
    IMAS_MASS_Re_167 = 166.962600 * IMAS_AMU, &
    IMAS_MASS_Re_168 = 167.961570 * IMAS_AMU, &
    IMAS_MASS_Re_169 = 168.958790 * IMAS_AMU, &
    IMAS_MASS_Re_170 = 169.958220 * IMAS_AMU, &
    IMAS_MASS_Re_171 = 170.955720 * IMAS_AMU, &
    IMAS_MASS_Re_172 = 171.955420 * IMAS_AMU, &
    IMAS_MASS_Re_173 = 172.953240 * IMAS_AMU, &
    IMAS_MASS_Re_174 = 173.953120 * IMAS_AMU, &
    IMAS_MASS_Re_175 = 174.951380 * IMAS_AMU, &
    IMAS_MASS_Re_176 = 175.951620 * IMAS_AMU, &
    IMAS_MASS_Re_177 = 176.950330 * IMAS_AMU, &
    IMAS_MASS_Re_178 = 177.950990 * IMAS_AMU, &
    IMAS_MASS_Re_179 = 178.949988 * IMAS_AMU, &
    IMAS_MASS_Re_180 = 179.950789 * IMAS_AMU, &
    IMAS_MASS_Re_181 = 180.950068 * IMAS_AMU, &
    IMAS_MASS_Re_182 = 181.95121 * IMAS_AMU, &
    IMAS_MASS_Re_183 = 182.950820 * IMAS_AMU, &
    IMAS_MASS_Re_184 = 183.952521 * IMAS_AMU, &
    IMAS_MASS_Re_185 = 184.9529550 * IMAS_AMU, &
    IMAS_MASS_Re_186 = 185.9549861 * IMAS_AMU, &
    IMAS_MASS_Re_187 = 186.9557531 * IMAS_AMU, &
    IMAS_MASS_Re_188 = 187.9581144 * IMAS_AMU, &
    IMAS_MASS_Re_189 = 188.959229 * IMAS_AMU, &
    IMAS_MASS_Re_190 = 189.96182 * IMAS_AMU, &
    IMAS_MASS_Re_191 = 190.963125 * IMAS_AMU, &
    IMAS_MASS_Re_192 = 191.96596 * IMAS_AMU, &
    IMAS_MASS_Re_193 = 192.96747 * IMAS_AMU, &
    IMAS_MASS_Re_194 = 193.97042 * IMAS_AMU, &
    IMAS_MASS_Os_162 = 161.98443 * IMAS_AMU, &
    IMAS_MASS_Os_163 = 162.98269 * IMAS_AMU, &
    IMAS_MASS_Os_164 = 163.97804 * IMAS_AMU, &
    IMAS_MASS_Os_165 = 164.97676 * IMAS_AMU, &
    IMAS_MASS_Os_166 = 165.972691 * IMAS_AMU, &
    IMAS_MASS_Os_167 = 166.971550 * IMAS_AMU, &
    IMAS_MASS_Os_168 = 167.967804 * IMAS_AMU, &
    IMAS_MASS_Os_169 = 168.967019 * IMAS_AMU, &
    IMAS_MASS_Os_170 = 169.963577 * IMAS_AMU, &
    IMAS_MASS_Os_171 = 170.963185 * IMAS_AMU, &
    IMAS_MASS_Os_172 = 171.960023 * IMAS_AMU, &
    IMAS_MASS_Os_173 = 172.959808 * IMAS_AMU, &
    IMAS_MASS_Os_174 = 173.957062 * IMAS_AMU, &
    IMAS_MASS_Os_175 = 174.956946 * IMAS_AMU, &
    IMAS_MASS_Os_176 = 175.954810 * IMAS_AMU, &
    IMAS_MASS_Os_177 = 176.954965 * IMAS_AMU, &
    IMAS_MASS_Os_178 = 177.953251 * IMAS_AMU, &
    IMAS_MASS_Os_179 = 178.953816 * IMAS_AMU, &
    IMAS_MASS_Os_180 = 179.952379 * IMAS_AMU, &
    IMAS_MASS_Os_181 = 180.953240 * IMAS_AMU, &
    IMAS_MASS_Os_182 = 181.952110 * IMAS_AMU, &
    IMAS_MASS_Os_183 = 182.953130 * IMAS_AMU, &
    IMAS_MASS_Os_184 = 183.9524891 * IMAS_AMU, &
    IMAS_MASS_Os_185 = 184.9540423 * IMAS_AMU, &
    IMAS_MASS_Os_186 = 185.9538382 * IMAS_AMU, &
    IMAS_MASS_Os_187 = 186.9557505 * IMAS_AMU, &
    IMAS_MASS_Os_188 = 187.9558382 * IMAS_AMU, &
    IMAS_MASS_Os_189 = 188.9581475 * IMAS_AMU, &
    IMAS_MASS_Os_190 = 189.9584470 * IMAS_AMU, &
    IMAS_MASS_Os_191 = 190.9609297 * IMAS_AMU, &
    IMAS_MASS_Os_192 = 191.9614807 * IMAS_AMU, &
    IMAS_MASS_Os_193 = 192.9641516 * IMAS_AMU, &
    IMAS_MASS_Os_194 = 193.9651821 * IMAS_AMU, &
    IMAS_MASS_Os_195 = 194.96813 * IMAS_AMU, &
    IMAS_MASS_Os_196 = 195.969640 * IMAS_AMU, &
    IMAS_MASS_Ir_164 = 163.99220 * IMAS_AMU, &
    IMAS_MASS_Ir_165 = 164.98752 * IMAS_AMU, &
    IMAS_MASS_Ir_166 = 165.98582 * IMAS_AMU, &
    IMAS_MASS_Ir_167 = 166.981665 * IMAS_AMU, &
    IMAS_MASS_Ir_168 = 167.97988 * IMAS_AMU, &
    IMAS_MASS_Ir_169 = 168.976295 * IMAS_AMU, &
    IMAS_MASS_Ir_170 = 169.97497 * IMAS_AMU, &
    IMAS_MASS_Ir_171 = 170.971630 * IMAS_AMU, &
    IMAS_MASS_Ir_172 = 171.97046 * IMAS_AMU, &
    IMAS_MASS_Ir_173 = 172.967502 * IMAS_AMU, &
    IMAS_MASS_Ir_174 = 173.966861 * IMAS_AMU, &
    IMAS_MASS_Ir_175 = 174.964113 * IMAS_AMU, &
    IMAS_MASS_Ir_176 = 175.963649 * IMAS_AMU, &
    IMAS_MASS_Ir_177 = 176.961302 * IMAS_AMU, &
    IMAS_MASS_Ir_178 = 177.961082 * IMAS_AMU, &
    IMAS_MASS_Ir_179 = 178.959122 * IMAS_AMU, &
    IMAS_MASS_Ir_180 = 179.959229 * IMAS_AMU, &
    IMAS_MASS_Ir_181 = 180.957625 * IMAS_AMU, &
    IMAS_MASS_Ir_182 = 181.958076 * IMAS_AMU, &
    IMAS_MASS_Ir_183 = 182.956846 * IMAS_AMU, &
    IMAS_MASS_Ir_184 = 183.957480 * IMAS_AMU, &
    IMAS_MASS_Ir_185 = 184.956700 * IMAS_AMU, &
    IMAS_MASS_Ir_186 = 185.957946 * IMAS_AMU, &
    IMAS_MASS_Ir_187 = 186.957363 * IMAS_AMU, &
    IMAS_MASS_Ir_188 = 187.958853 * IMAS_AMU, &
    IMAS_MASS_Ir_189 = 188.958719 * IMAS_AMU, &
    IMAS_MASS_Ir_190 = 189.9605460 * IMAS_AMU, &
    IMAS_MASS_Ir_191 = 190.9605940 * IMAS_AMU, &
    IMAS_MASS_Ir_192 = 191.9626050 * IMAS_AMU, &
    IMAS_MASS_Ir_193 = 192.9629264 * IMAS_AMU, &
    IMAS_MASS_Ir_194 = 193.9650784 * IMAS_AMU, &
    IMAS_MASS_Ir_195 = 194.9659796 * IMAS_AMU, &
    IMAS_MASS_Ir_196 = 195.968400 * IMAS_AMU, &
    IMAS_MASS_Ir_197 = 196.969653 * IMAS_AMU, &
    IMAS_MASS_Ir_198 = 197.97228 * IMAS_AMU, &
    IMAS_MASS_Ir_199 = 198.973800 * IMAS_AMU, &
    IMAS_MASS_Pt_166 = 165.99486 * IMAS_AMU, &
    IMAS_MASS_Pt_167 = 166.99298 * IMAS_AMU, &
    IMAS_MASS_Pt_168 = 167.98815 * IMAS_AMU, &
    IMAS_MASS_Pt_169 = 168.98672 * IMAS_AMU, &
    IMAS_MASS_Pt_170 = 169.982495 * IMAS_AMU, &
    IMAS_MASS_Pt_171 = 170.981240 * IMAS_AMU, &
    IMAS_MASS_Pt_172 = 171.977347 * IMAS_AMU, &
    IMAS_MASS_Pt_173 = 172.976440 * IMAS_AMU, &
    IMAS_MASS_Pt_174 = 173.972819 * IMAS_AMU, &
    IMAS_MASS_Pt_175 = 174.972421 * IMAS_AMU, &
    IMAS_MASS_Pt_176 = 175.968945 * IMAS_AMU, &
    IMAS_MASS_Pt_177 = 176.968469 * IMAS_AMU, &
    IMAS_MASS_Pt_178 = 177.965649 * IMAS_AMU, &
    IMAS_MASS_Pt_179 = 178.965363 * IMAS_AMU, &
    IMAS_MASS_Pt_180 = 179.963031 * IMAS_AMU, &
    IMAS_MASS_Pt_181 = 180.963097 * IMAS_AMU, &
    IMAS_MASS_Pt_182 = 181.961171 * IMAS_AMU, &
    IMAS_MASS_Pt_183 = 182.961597 * IMAS_AMU, &
    IMAS_MASS_Pt_184 = 183.959922 * IMAS_AMU, &
    IMAS_MASS_Pt_185 = 184.960620 * IMAS_AMU, &
    IMAS_MASS_Pt_186 = 185.959351 * IMAS_AMU, &
    IMAS_MASS_Pt_187 = 186.960590 * IMAS_AMU, &
    IMAS_MASS_Pt_188 = 187.959395 * IMAS_AMU, &
    IMAS_MASS_Pt_189 = 188.960834 * IMAS_AMU, &
    IMAS_MASS_Pt_190 = 189.959932 * IMAS_AMU, &
    IMAS_MASS_Pt_191 = 190.961677 * IMAS_AMU, &
    IMAS_MASS_Pt_192 = 191.9610380 * IMAS_AMU, &
    IMAS_MASS_Pt_193 = 192.9629874 * IMAS_AMU, &
    IMAS_MASS_Pt_194 = 193.9626803 * IMAS_AMU, &
    IMAS_MASS_Pt_195 = 194.9647911 * IMAS_AMU, &
    IMAS_MASS_Pt_196 = 195.9649515 * IMAS_AMU, &
    IMAS_MASS_Pt_197 = 196.9673402 * IMAS_AMU, &
    IMAS_MASS_Pt_198 = 197.967893 * IMAS_AMU, &
    IMAS_MASS_Pt_199 = 198.970593 * IMAS_AMU, &
    IMAS_MASS_Pt_200 = 199.971441 * IMAS_AMU, &
    IMAS_MASS_Pt_201 = 200.974510 * IMAS_AMU, &
    IMAS_MASS_Pt_202 = 201.97574 * IMAS_AMU, &
    IMAS_MASS_Au_169 = 168.99808 * IMAS_AMU, &
    IMAS_MASS_Au_170 = 169.99612 * IMAS_AMU, &
    IMAS_MASS_Au_171 = 170.991879 * IMAS_AMU, &
    IMAS_MASS_Au_172 = 171.99004 * IMAS_AMU, &
    IMAS_MASS_Au_173 = 172.986237 * IMAS_AMU, &
    IMAS_MASS_Au_174 = 173.98476 * IMAS_AMU, &
    IMAS_MASS_Au_175 = 174.981270 * IMAS_AMU, &
    IMAS_MASS_Au_176 = 175.98010 * IMAS_AMU, &
    IMAS_MASS_Au_177 = 176.976865 * IMAS_AMU, &
    IMAS_MASS_Au_178 = 177.976030 * IMAS_AMU, &
    IMAS_MASS_Au_179 = 178.973213 * IMAS_AMU, &
    IMAS_MASS_Au_180 = 179.972521 * IMAS_AMU, &
    IMAS_MASS_Au_181 = 180.970079 * IMAS_AMU, &
    IMAS_MASS_Au_182 = 181.969618 * IMAS_AMU, &
    IMAS_MASS_Au_183 = 182.967593 * IMAS_AMU, &
    IMAS_MASS_Au_184 = 183.967452 * IMAS_AMU, &
    IMAS_MASS_Au_185 = 184.965789 * IMAS_AMU, &
    IMAS_MASS_Au_186 = 185.965953 * IMAS_AMU, &
    IMAS_MASS_Au_187 = 186.964568 * IMAS_AMU, &
    IMAS_MASS_Au_188 = 187.965324 * IMAS_AMU, &
    IMAS_MASS_Au_189 = 188.963948 * IMAS_AMU, &
    IMAS_MASS_Au_190 = 189.964700 * IMAS_AMU, &
    IMAS_MASS_Au_191 = 190.963700 * IMAS_AMU, &
    IMAS_MASS_Au_192 = 191.964813 * IMAS_AMU, &
    IMAS_MASS_Au_193 = 192.964150 * IMAS_AMU, &
    IMAS_MASS_Au_194 = 193.965365 * IMAS_AMU, &
    IMAS_MASS_Au_195 = 194.9650346 * IMAS_AMU, &
    IMAS_MASS_Au_196 = 195.966570 * IMAS_AMU, &
    IMAS_MASS_Au_197 = 196.9665687 * IMAS_AMU, &
    IMAS_MASS_Au_198 = 197.9682423 * IMAS_AMU, &
    IMAS_MASS_Au_199 = 198.9687652 * IMAS_AMU, &
    IMAS_MASS_Au_200 = 199.970730 * IMAS_AMU, &
    IMAS_MASS_Au_201 = 200.971657 * IMAS_AMU, &
    IMAS_MASS_Au_202 = 201.97381 * IMAS_AMU, &
    IMAS_MASS_Au_203 = 202.975155 * IMAS_AMU, &
    IMAS_MASS_Au_204 = 203.97772 * IMAS_AMU, &
    IMAS_MASS_Au_205 = 204.97987 * IMAS_AMU, &
    IMAS_MASS_Hg_171 = 171.00376 * IMAS_AMU, &
    IMAS_MASS_Hg_172 = 171.99883 * IMAS_AMU, &
    IMAS_MASS_Hg_173 = 172.99724 * IMAS_AMU, &
    IMAS_MASS_Hg_174 = 173.992864 * IMAS_AMU, &
    IMAS_MASS_Hg_175 = 174.99142 * IMAS_AMU, &
    IMAS_MASS_Hg_176 = 175.987355 * IMAS_AMU, &
    IMAS_MASS_Hg_177 = 176.986280 * IMAS_AMU, &
    IMAS_MASS_Hg_178 = 177.982483 * IMAS_AMU, &
    IMAS_MASS_Hg_179 = 178.981834 * IMAS_AMU, &
    IMAS_MASS_Hg_180 = 179.978266 * IMAS_AMU, &
    IMAS_MASS_Hg_181 = 180.977819 * IMAS_AMU, &
    IMAS_MASS_Hg_182 = 181.974690 * IMAS_AMU, &
    IMAS_MASS_Hg_183 = 182.974450 * IMAS_AMU, &
    IMAS_MASS_Hg_184 = 183.971713 * IMAS_AMU, &
    IMAS_MASS_Hg_185 = 184.971899 * IMAS_AMU, &
    IMAS_MASS_Hg_186 = 185.969362 * IMAS_AMU, &
    IMAS_MASS_Hg_187 = 186.969814 * IMAS_AMU, &
    IMAS_MASS_Hg_188 = 187.967577 * IMAS_AMU, &
    IMAS_MASS_Hg_189 = 188.968190 * IMAS_AMU, &
    IMAS_MASS_Hg_190 = 189.966322 * IMAS_AMU, &
    IMAS_MASS_Hg_191 = 190.967157 * IMAS_AMU, &
    IMAS_MASS_Hg_192 = 191.965634 * IMAS_AMU, &
    IMAS_MASS_Hg_193 = 192.966665 * IMAS_AMU, &
    IMAS_MASS_Hg_194 = 193.965439 * IMAS_AMU, &
    IMAS_MASS_Hg_195 = 194.966720 * IMAS_AMU, &
    IMAS_MASS_Hg_196 = 195.965833 * IMAS_AMU, &
    IMAS_MASS_Hg_197 = 196.967213 * IMAS_AMU, &
    IMAS_MASS_Hg_198 = 197.9667690 * IMAS_AMU, &
    IMAS_MASS_Hg_199 = 198.9682799 * IMAS_AMU, &
    IMAS_MASS_Hg_200 = 199.9683260 * IMAS_AMU, &
    IMAS_MASS_Hg_201 = 200.9703023 * IMAS_AMU, &
    IMAS_MASS_Hg_202 = 201.9706430 * IMAS_AMU, &
    IMAS_MASS_Hg_203 = 202.9728725 * IMAS_AMU, &
    IMAS_MASS_Hg_204 = 203.9734939 * IMAS_AMU, &
    IMAS_MASS_Hg_205 = 204.976073 * IMAS_AMU, &
    IMAS_MASS_Hg_206 = 205.977514 * IMAS_AMU, &
    IMAS_MASS_Hg_207 = 206.98259 * IMAS_AMU, &
    IMAS_MASS_Hg_208 = 207.98594 * IMAS_AMU, &
    IMAS_MASS_Hg_209 = 208.99104 * IMAS_AMU, &
    IMAS_MASS_Hg_210 = 209.99451 * IMAS_AMU, &
    IMAS_MASS_Tl_176 = 176.00059 * IMAS_AMU, &
    IMAS_MASS_Tl_177 = 176.996427 * IMAS_AMU, &
    IMAS_MASS_Tl_178 = 177.99490 * IMAS_AMU, &
    IMAS_MASS_Tl_179 = 178.991090 * IMAS_AMU, &
    IMAS_MASS_Tl_180 = 179.98991 * IMAS_AMU, &
    IMAS_MASS_Tl_181 = 180.986257 * IMAS_AMU, &
    IMAS_MASS_Tl_182 = 181.985670 * IMAS_AMU, &
    IMAS_MASS_Tl_183 = 182.982193 * IMAS_AMU, &
    IMAS_MASS_Tl_184 = 183.981870 * IMAS_AMU, &
    IMAS_MASS_Tl_185 = 184.978790 * IMAS_AMU, &
    IMAS_MASS_Tl_186 = 185.97833 * IMAS_AMU, &
    IMAS_MASS_Tl_187 = 186.975906 * IMAS_AMU, &
    IMAS_MASS_Tl_188 = 187.976010 * IMAS_AMU, &
    IMAS_MASS_Tl_189 = 188.973588 * IMAS_AMU, &
    IMAS_MASS_Tl_190 = 189.973880 * IMAS_AMU, &
    IMAS_MASS_Tl_191 = 190.971786 * IMAS_AMU, &
    IMAS_MASS_Tl_192 = 191.972230 * IMAS_AMU, &
    IMAS_MASS_Tl_193 = 192.97067 * IMAS_AMU, &
    IMAS_MASS_Tl_194 = 193.97120 * IMAS_AMU, &
    IMAS_MASS_Tl_195 = 194.969774 * IMAS_AMU, &
    IMAS_MASS_Tl_196 = 195.970481 * IMAS_AMU, &
    IMAS_MASS_Tl_197 = 196.969575 * IMAS_AMU, &
    IMAS_MASS_Tl_198 = 197.970480 * IMAS_AMU, &
    IMAS_MASS_Tl_199 = 198.969880 * IMAS_AMU, &
    IMAS_MASS_Tl_200 = 199.970963 * IMAS_AMU, &
    IMAS_MASS_Tl_201 = 200.970819 * IMAS_AMU, &
    IMAS_MASS_Tl_202 = 201.972106 * IMAS_AMU, &
    IMAS_MASS_Tl_203 = 202.9723442 * IMAS_AMU, &
    IMAS_MASS_Tl_204 = 203.9738635 * IMAS_AMU, &
    IMAS_MASS_Tl_205 = 204.9744275 * IMAS_AMU, &
    IMAS_MASS_Tl_206 = 205.9761103 * IMAS_AMU, &
    IMAS_MASS_Tl_207 = 206.977419 * IMAS_AMU, &
    IMAS_MASS_Tl_208 = 207.9820187 * IMAS_AMU, &
    IMAS_MASS_Tl_209 = 208.985359 * IMAS_AMU, &
    IMAS_MASS_Tl_210 = 209.990074 * IMAS_AMU, &
    IMAS_MASS_Tl_211 = 210.99348 * IMAS_AMU, &
    IMAS_MASS_Tl_212 = 211.99823 * IMAS_AMU, &
    IMAS_MASS_Pb_178 = 178.003830 * IMAS_AMU, &
    IMAS_MASS_Pb_179 = 179.00215 * IMAS_AMU, &
    IMAS_MASS_Pb_180 = 179.997918 * IMAS_AMU, &
    IMAS_MASS_Pb_181 = 180.99662 * IMAS_AMU, &
    IMAS_MASS_Pb_182 = 181.992672 * IMAS_AMU, &
    IMAS_MASS_Pb_183 = 182.991870 * IMAS_AMU, &
    IMAS_MASS_Pb_184 = 183.988142 * IMAS_AMU, &
    IMAS_MASS_Pb_185 = 184.987610 * IMAS_AMU, &
    IMAS_MASS_Pb_186 = 185.984239 * IMAS_AMU, &
    IMAS_MASS_Pb_187 = 186.983918 * IMAS_AMU, &
    IMAS_MASS_Pb_188 = 187.980874 * IMAS_AMU, &
    IMAS_MASS_Pb_189 = 188.980810 * IMAS_AMU, &
    IMAS_MASS_Pb_190 = 189.978082 * IMAS_AMU, &
    IMAS_MASS_Pb_191 = 190.978270 * IMAS_AMU, &
    IMAS_MASS_Pb_192 = 191.975785 * IMAS_AMU, &
    IMAS_MASS_Pb_193 = 192.976170 * IMAS_AMU, &
    IMAS_MASS_Pb_194 = 193.974012 * IMAS_AMU, &
    IMAS_MASS_Pb_195 = 194.974542 * IMAS_AMU, &
    IMAS_MASS_Pb_196 = 195.972774 * IMAS_AMU, &
    IMAS_MASS_Pb_197 = 196.973431 * IMAS_AMU, &
    IMAS_MASS_Pb_198 = 197.972034 * IMAS_AMU, &
    IMAS_MASS_Pb_199 = 198.972917 * IMAS_AMU, &
    IMAS_MASS_Pb_200 = 199.971827 * IMAS_AMU, &
    IMAS_MASS_Pb_201 = 200.972885 * IMAS_AMU, &
    IMAS_MASS_Pb_202 = 201.972159 * IMAS_AMU, &
    IMAS_MASS_Pb_203 = 202.973391 * IMAS_AMU, &
    IMAS_MASS_Pb_204 = 203.9730436 * IMAS_AMU, &
    IMAS_MASS_Pb_205 = 204.9744818 * IMAS_AMU, &
    IMAS_MASS_Pb_206 = 205.9744653 * IMAS_AMU, &
    IMAS_MASS_Pb_207 = 206.9758969 * IMAS_AMU, &
    IMAS_MASS_Pb_208 = 207.9766521 * IMAS_AMU, &
    IMAS_MASS_Pb_209 = 208.9810901 * IMAS_AMU, &
    IMAS_MASS_Pb_210 = 209.9841885 * IMAS_AMU, &
    IMAS_MASS_Pb_211 = 210.9887370 * IMAS_AMU, &
    IMAS_MASS_Pb_212 = 211.9918975 * IMAS_AMU, &
    IMAS_MASS_Pb_213 = 212.996581 * IMAS_AMU, &
    IMAS_MASS_Pb_214 = 213.9998054 * IMAS_AMU, &
    IMAS_MASS_Pb_215 = 215.00481 * IMAS_AMU, &
    IMAS_MASS_Bi_184 = 184.00112 * IMAS_AMU, &
    IMAS_MASS_Bi_185 = 184.997630 * IMAS_AMU, &
    IMAS_MASS_Bi_186 = 185.996600 * IMAS_AMU, &
    IMAS_MASS_Bi_187 = 186.993158 * IMAS_AMU, &
    IMAS_MASS_Bi_188 = 187.992270 * IMAS_AMU, &
    IMAS_MASS_Bi_189 = 188.989200 * IMAS_AMU, &
    IMAS_MASS_Bi_190 = 189.98830 * IMAS_AMU, &
    IMAS_MASS_Bi_191 = 190.985786 * IMAS_AMU, &
    IMAS_MASS_Bi_192 = 191.985460 * IMAS_AMU, &
    IMAS_MASS_Bi_193 = 192.982960 * IMAS_AMU, &
    IMAS_MASS_Bi_194 = 193.982830 * IMAS_AMU, &
    IMAS_MASS_Bi_195 = 194.980651 * IMAS_AMU, &
    IMAS_MASS_Bi_196 = 195.980667 * IMAS_AMU, &
    IMAS_MASS_Bi_197 = 196.978864 * IMAS_AMU, &
    IMAS_MASS_Bi_198 = 197.979210 * IMAS_AMU, &
    IMAS_MASS_Bi_199 = 198.977672 * IMAS_AMU, &
    IMAS_MASS_Bi_200 = 199.978132 * IMAS_AMU, &
    IMAS_MASS_Bi_201 = 200.977009 * IMAS_AMU, &
    IMAS_MASS_Bi_202 = 201.977742 * IMAS_AMU, &
    IMAS_MASS_Bi_203 = 202.976876 * IMAS_AMU, &
    IMAS_MASS_Bi_204 = 203.977813 * IMAS_AMU, &
    IMAS_MASS_Bi_205 = 204.977389 * IMAS_AMU, &
    IMAS_MASS_Bi_206 = 205.978499 * IMAS_AMU, &
    IMAS_MASS_Bi_207 = 206.9784707 * IMAS_AMU, &
    IMAS_MASS_Bi_208 = 207.9797422 * IMAS_AMU, &
    IMAS_MASS_Bi_209 = 208.9803987 * IMAS_AMU, &
    IMAS_MASS_Bi_210 = 209.9841204 * IMAS_AMU, &
    IMAS_MASS_Bi_211 = 210.987269 * IMAS_AMU, &
    IMAS_MASS_Bi_212 = 211.9912857 * IMAS_AMU, &
    IMAS_MASS_Bi_213 = 212.994385 * IMAS_AMU, &
    IMAS_MASS_Bi_214 = 213.998712 * IMAS_AMU, &
    IMAS_MASS_Bi_215 = 215.001770 * IMAS_AMU, &
    IMAS_MASS_Bi_216 = 216.006306 * IMAS_AMU, &
    IMAS_MASS_Bi_217 = 217.00947 * IMAS_AMU, &
    IMAS_MASS_Bi_218 = 218.01432 * IMAS_AMU, &
    IMAS_MASS_Po_188 = 187.999422 * IMAS_AMU, &
    IMAS_MASS_Po_189 = 188.998481 * IMAS_AMU, &
    IMAS_MASS_Po_190 = 189.995101 * IMAS_AMU, &
    IMAS_MASS_Po_191 = 190.994574 * IMAS_AMU, &
    IMAS_MASS_Po_192 = 191.991335 * IMAS_AMU, &
    IMAS_MASS_Po_193 = 192.991030 * IMAS_AMU, &
    IMAS_MASS_Po_194 = 193.988186 * IMAS_AMU, &
    IMAS_MASS_Po_195 = 194.988110 * IMAS_AMU, &
    IMAS_MASS_Po_196 = 195.985535 * IMAS_AMU, &
    IMAS_MASS_Po_197 = 196.985660 * IMAS_AMU, &
    IMAS_MASS_Po_198 = 197.983389 * IMAS_AMU, &
    IMAS_MASS_Po_199 = 198.983666 * IMAS_AMU, &
    IMAS_MASS_Po_200 = 199.981799 * IMAS_AMU, &
    IMAS_MASS_Po_201 = 200.982260 * IMAS_AMU, &
    IMAS_MASS_Po_202 = 201.980758 * IMAS_AMU, &
    IMAS_MASS_Po_203 = 202.981420 * IMAS_AMU, &
    IMAS_MASS_Po_204 = 203.980318 * IMAS_AMU, &
    IMAS_MASS_Po_205 = 204.981203 * IMAS_AMU, &
    IMAS_MASS_Po_206 = 205.980481 * IMAS_AMU, &
    IMAS_MASS_Po_207 = 206.981593 * IMAS_AMU, &
    IMAS_MASS_Po_208 = 207.9812457 * IMAS_AMU, &
    IMAS_MASS_Po_209 = 208.9824304 * IMAS_AMU, &
    IMAS_MASS_Po_210 = 209.9828737 * IMAS_AMU, &
    IMAS_MASS_Po_211 = 210.9866532 * IMAS_AMU, &
    IMAS_MASS_Po_212 = 211.9888680 * IMAS_AMU, &
    IMAS_MASS_Po_213 = 212.992857 * IMAS_AMU, &
    IMAS_MASS_Po_214 = 213.9952014 * IMAS_AMU, &
    IMAS_MASS_Po_215 = 214.9994200 * IMAS_AMU, &
    IMAS_MASS_Po_216 = 216.0019150 * IMAS_AMU, &
    IMAS_MASS_Po_217 = 217.006335 * IMAS_AMU, &
    IMAS_MASS_Po_218 = 218.0089730 * IMAS_AMU, &
    IMAS_MASS_Po_219 = 219.01374 * IMAS_AMU, &
    IMAS_MASS_Po_220 = 220.01660 * IMAS_AMU, &
    IMAS_MASS_At_193 = 192.999840 * IMAS_AMU, &
    IMAS_MASS_At_194 = 193.99873 * IMAS_AMU, &
    IMAS_MASS_At_195 = 194.996268 * IMAS_AMU, &
    IMAS_MASS_At_196 = 195.995790 * IMAS_AMU, &
    IMAS_MASS_At_197 = 196.993190 * IMAS_AMU, &
    IMAS_MASS_At_198 = 197.992840 * IMAS_AMU, &
    IMAS_MASS_At_199 = 198.990530 * IMAS_AMU, &
    IMAS_MASS_At_200 = 199.990351 * IMAS_AMU, &
    IMAS_MASS_At_201 = 200.988417 * IMAS_AMU, &
    IMAS_MASS_At_202 = 201.988630 * IMAS_AMU, &
    IMAS_MASS_At_203 = 202.986942 * IMAS_AMU, &
    IMAS_MASS_At_204 = 203.987251 * IMAS_AMU, &
    IMAS_MASS_At_205 = 204.986074 * IMAS_AMU, &
    IMAS_MASS_At_206 = 205.986667 * IMAS_AMU, &
    IMAS_MASS_At_207 = 206.985784 * IMAS_AMU, &
    IMAS_MASS_At_208 = 207.986590 * IMAS_AMU, &
    IMAS_MASS_At_209 = 208.986173 * IMAS_AMU, &
    IMAS_MASS_At_210 = 209.987148 * IMAS_AMU, &
    IMAS_MASS_At_211 = 210.9874963 * IMAS_AMU, &
    IMAS_MASS_At_212 = 211.990745 * IMAS_AMU, &
    IMAS_MASS_At_213 = 212.992937 * IMAS_AMU, &
    IMAS_MASS_At_214 = 213.996372 * IMAS_AMU, &
    IMAS_MASS_At_215 = 214.998653 * IMAS_AMU, &
    IMAS_MASS_At_216 = 216.002423 * IMAS_AMU, &
    IMAS_MASS_At_217 = 217.004719 * IMAS_AMU, &
    IMAS_MASS_At_218 = 218.008694 * IMAS_AMU, &
    IMAS_MASS_At_219 = 219.011162 * IMAS_AMU, &
    IMAS_MASS_At_220 = 220.015410 * IMAS_AMU, &
    IMAS_MASS_At_221 = 221.01805 * IMAS_AMU, &
    IMAS_MASS_At_222 = 222.02233 * IMAS_AMU, &
    IMAS_MASS_At_223 = 223.02519 * IMAS_AMU, &
    IMAS_MASS_Rn_195 = 195.005440 * IMAS_AMU, &
    IMAS_MASS_Rn_196 = 196.002115 * IMAS_AMU, &
    IMAS_MASS_Rn_197 = 197.001580 * IMAS_AMU, &
    IMAS_MASS_Rn_198 = 197.998679 * IMAS_AMU, &
    IMAS_MASS_Rn_199 = 198.998370 * IMAS_AMU, &
    IMAS_MASS_Rn_200 = 199.995699 * IMAS_AMU, &
    IMAS_MASS_Rn_201 = 200.995630 * IMAS_AMU, &
    IMAS_MASS_Rn_202 = 201.993263 * IMAS_AMU, &
    IMAS_MASS_Rn_203 = 202.993387 * IMAS_AMU, &
    IMAS_MASS_Rn_204 = 203.991429 * IMAS_AMU, &
    IMAS_MASS_Rn_205 = 204.991720 * IMAS_AMU, &
    IMAS_MASS_Rn_206 = 205.990214 * IMAS_AMU, &
    IMAS_MASS_Rn_207 = 206.990734 * IMAS_AMU, &
    IMAS_MASS_Rn_208 = 207.989642 * IMAS_AMU, &
    IMAS_MASS_Rn_209 = 208.990415 * IMAS_AMU, &
    IMAS_MASS_Rn_210 = 209.989696 * IMAS_AMU, &
    IMAS_MASS_Rn_211 = 210.990601 * IMAS_AMU, &
    IMAS_MASS_Rn_212 = 211.990704 * IMAS_AMU, &
    IMAS_MASS_Rn_213 = 212.993883 * IMAS_AMU, &
    IMAS_MASS_Rn_214 = 213.995363 * IMAS_AMU, &
    IMAS_MASS_Rn_215 = 214.998745 * IMAS_AMU, &
    IMAS_MASS_Rn_216 = 216.000274 * IMAS_AMU, &
    IMAS_MASS_Rn_217 = 217.003928 * IMAS_AMU, &
    IMAS_MASS_Rn_218 = 218.0056013 * IMAS_AMU, &
    IMAS_MASS_Rn_219 = 219.0094802 * IMAS_AMU, &
    IMAS_MASS_Rn_220 = 220.0113940 * IMAS_AMU, &
    IMAS_MASS_Rn_221 = 221.015537 * IMAS_AMU, &
    IMAS_MASS_Rn_222 = 222.0175777 * IMAS_AMU, &
    IMAS_MASS_Rn_223 = 223.02179 * IMAS_AMU, &
    IMAS_MASS_Rn_224 = 224.02409 * IMAS_AMU, &
    IMAS_MASS_Rn_225 = 225.02844 * IMAS_AMU, &
    IMAS_MASS_Rn_226 = 226.03089 * IMAS_AMU, &
    IMAS_MASS_Rn_227 = 227.03541 * IMAS_AMU, &
    IMAS_MASS_Rn_228 = 228.03799 * IMAS_AMU, &
    IMAS_MASS_Fr_199 = 199.007260 * IMAS_AMU, &
    IMAS_MASS_Fr_200 = 200.006570 * IMAS_AMU, &
    IMAS_MASS_Fr_201 = 201.003860 * IMAS_AMU, &
    IMAS_MASS_Fr_202 = 202.003370 * IMAS_AMU, &
    IMAS_MASS_Fr_203 = 203.000925 * IMAS_AMU, &
    IMAS_MASS_Fr_204 = 204.000653 * IMAS_AMU, &
    IMAS_MASS_Fr_205 = 204.998594 * IMAS_AMU, &
    IMAS_MASS_Fr_206 = 205.998670 * IMAS_AMU, &
    IMAS_MASS_Fr_207 = 206.996950 * IMAS_AMU, &
    IMAS_MASS_Fr_208 = 207.997140 * IMAS_AMU, &
    IMAS_MASS_Fr_209 = 208.995954 * IMAS_AMU, &
    IMAS_MASS_Fr_210 = 209.996408 * IMAS_AMU, &
    IMAS_MASS_Fr_211 = 210.995537 * IMAS_AMU, &
    IMAS_MASS_Fr_212 = 211.996202 * IMAS_AMU, &
    IMAS_MASS_Fr_213 = 212.996189 * IMAS_AMU, &
    IMAS_MASS_Fr_214 = 213.998971 * IMAS_AMU, &
    IMAS_MASS_Fr_215 = 215.000341 * IMAS_AMU, &
    IMAS_MASS_Fr_216 = 216.003198 * IMAS_AMU, &
    IMAS_MASS_Fr_217 = 217.004632 * IMAS_AMU, &
    IMAS_MASS_Fr_218 = 218.007578 * IMAS_AMU, &
    IMAS_MASS_Fr_219 = 219.009252 * IMAS_AMU, &
    IMAS_MASS_Fr_220 = 220.012327 * IMAS_AMU, &
    IMAS_MASS_Fr_221 = 221.014255 * IMAS_AMU, &
    IMAS_MASS_Fr_222 = 222.017552 * IMAS_AMU, &
    IMAS_MASS_Fr_223 = 223.0197359 * IMAS_AMU, &
    IMAS_MASS_Fr_224 = 224.023250 * IMAS_AMU, &
    IMAS_MASS_Fr_225 = 225.025570 * IMAS_AMU, &
    IMAS_MASS_Fr_226 = 226.02939 * IMAS_AMU, &
    IMAS_MASS_Fr_227 = 227.03184 * IMAS_AMU, &
    IMAS_MASS_Fr_228 = 228.03573 * IMAS_AMU, &
    IMAS_MASS_Fr_229 = 229.038450 * IMAS_AMU, &
    IMAS_MASS_Fr_230 = 230.04251 * IMAS_AMU, &
    IMAS_MASS_Fr_231 = 231.04544 * IMAS_AMU, &
    IMAS_MASS_Fr_232 = 232.04977 * IMAS_AMU, &
    IMAS_MASS_Ra_202 = 202.009890 * IMAS_AMU, &
    IMAS_MASS_Ra_203 = 203.009270 * IMAS_AMU, &
    IMAS_MASS_Ra_204 = 204.006500 * IMAS_AMU, &
    IMAS_MASS_Ra_205 = 205.006270 * IMAS_AMU, &
    IMAS_MASS_Ra_206 = 206.003827 * IMAS_AMU, &
    IMAS_MASS_Ra_207 = 207.003800 * IMAS_AMU, &
    IMAS_MASS_Ra_208 = 208.001840 * IMAS_AMU, &
    IMAS_MASS_Ra_209 = 209.001990 * IMAS_AMU, &
    IMAS_MASS_Ra_210 = 210.000495 * IMAS_AMU, &
    IMAS_MASS_Ra_211 = 211.000898 * IMAS_AMU, &
    IMAS_MASS_Ra_212 = 211.999794 * IMAS_AMU, &
    IMAS_MASS_Ra_213 = 213.000384 * IMAS_AMU, &
    IMAS_MASS_Ra_214 = 214.000108 * IMAS_AMU, &
    IMAS_MASS_Ra_215 = 215.002720 * IMAS_AMU, &
    IMAS_MASS_Ra_216 = 216.003533 * IMAS_AMU, &
    IMAS_MASS_Ra_217 = 217.006320 * IMAS_AMU, &
    IMAS_MASS_Ra_218 = 218.007140 * IMAS_AMU, &
    IMAS_MASS_Ra_219 = 219.010085 * IMAS_AMU, &
    IMAS_MASS_Ra_220 = 220.011028 * IMAS_AMU, &
    IMAS_MASS_Ra_221 = 221.013917 * IMAS_AMU, &
    IMAS_MASS_Ra_222 = 222.015375 * IMAS_AMU, &
    IMAS_MASS_Ra_223 = 223.0185022 * IMAS_AMU, &
    IMAS_MASS_Ra_224 = 224.0202118 * IMAS_AMU, &
    IMAS_MASS_Ra_225 = 225.023612 * IMAS_AMU, &
    IMAS_MASS_Ra_226 = 226.0254098 * IMAS_AMU, &
    IMAS_MASS_Ra_227 = 227.0291778 * IMAS_AMU, &
    IMAS_MASS_Ra_228 = 228.0310703 * IMAS_AMU, &
    IMAS_MASS_Ra_229 = 229.034958 * IMAS_AMU, &
    IMAS_MASS_Ra_230 = 230.037056 * IMAS_AMU, &
    IMAS_MASS_Ra_231 = 231.04122 * IMAS_AMU, &
    IMAS_MASS_Ra_232 = 232.04364 * IMAS_AMU, &
    IMAS_MASS_Ra_233 = 233.04806 * IMAS_AMU, &
    IMAS_MASS_Ra_234 = 234.05070 * IMAS_AMU, &
    IMAS_MASS_Ac_206 = 206.014500 * IMAS_AMU, &
    IMAS_MASS_Ac_207 = 207.011950 * IMAS_AMU, &
    IMAS_MASS_Ac_208 = 208.011550 * IMAS_AMU, &
    IMAS_MASS_Ac_209 = 209.009490 * IMAS_AMU, &
    IMAS_MASS_Ac_210 = 210.009440 * IMAS_AMU, &
    IMAS_MASS_Ac_211 = 211.007730 * IMAS_AMU, &
    IMAS_MASS_Ac_212 = 212.007810 * IMAS_AMU, &
    IMAS_MASS_Ac_213 = 213.006610 * IMAS_AMU, &
    IMAS_MASS_Ac_214 = 214.006902 * IMAS_AMU, &
    IMAS_MASS_Ac_215 = 215.006454 * IMAS_AMU, &
    IMAS_MASS_Ac_216 = 216.008720 * IMAS_AMU, &
    IMAS_MASS_Ac_217 = 217.009347 * IMAS_AMU, &
    IMAS_MASS_Ac_218 = 218.011640 * IMAS_AMU, &
    IMAS_MASS_Ac_219 = 219.012420 * IMAS_AMU, &
    IMAS_MASS_Ac_220 = 220.014763 * IMAS_AMU, &
    IMAS_MASS_Ac_221 = 221.015590 * IMAS_AMU, &
    IMAS_MASS_Ac_222 = 222.017844 * IMAS_AMU, &
    IMAS_MASS_Ac_223 = 223.019137 * IMAS_AMU, &
    IMAS_MASS_Ac_224 = 224.021723 * IMAS_AMU, &
    IMAS_MASS_Ac_225 = 225.023230 * IMAS_AMU, &
    IMAS_MASS_Ac_226 = 226.026098 * IMAS_AMU, &
    IMAS_MASS_Ac_227 = 227.0277521 * IMAS_AMU, &
    IMAS_MASS_Ac_228 = 228.0310211 * IMAS_AMU, &
    IMAS_MASS_Ac_229 = 229.033020 * IMAS_AMU, &
    IMAS_MASS_Ac_230 = 230.03629 * IMAS_AMU, &
    IMAS_MASS_Ac_231 = 231.03856 * IMAS_AMU, &
    IMAS_MASS_Ac_232 = 232.04203 * IMAS_AMU, &
    IMAS_MASS_Ac_233 = 233.04455 * IMAS_AMU, &
    IMAS_MASS_Ac_234 = 234.04842 * IMAS_AMU, &
    IMAS_MASS_Ac_235 = 235.05123 * IMAS_AMU, &
    IMAS_MASS_Ac_236 = 236.05530 * IMAS_AMU, &
    IMAS_MASS_Th_209 = 209.01772 * IMAS_AMU, &
    IMAS_MASS_Th_210 = 210.015075 * IMAS_AMU, &
    IMAS_MASS_Th_211 = 211.014930 * IMAS_AMU, &
    IMAS_MASS_Th_212 = 212.012980 * IMAS_AMU, &
    IMAS_MASS_Th_213 = 213.013010 * IMAS_AMU, &
    IMAS_MASS_Th_214 = 214.011500 * IMAS_AMU, &
    IMAS_MASS_Th_215 = 215.011730 * IMAS_AMU, &
    IMAS_MASS_Th_216 = 216.011062 * IMAS_AMU, &
    IMAS_MASS_Th_217 = 217.013114 * IMAS_AMU, &
    IMAS_MASS_Th_218 = 218.013284 * IMAS_AMU, &
    IMAS_MASS_Th_219 = 219.015540 * IMAS_AMU, &
    IMAS_MASS_Th_220 = 220.015748 * IMAS_AMU, &
    IMAS_MASS_Th_221 = 221.018184 * IMAS_AMU, &
    IMAS_MASS_Th_222 = 222.018468 * IMAS_AMU, &
    IMAS_MASS_Th_223 = 223.020811 * IMAS_AMU, &
    IMAS_MASS_Th_224 = 224.021467 * IMAS_AMU, &
    IMAS_MASS_Th_225 = 225.023951 * IMAS_AMU, &
    IMAS_MASS_Th_226 = 226.024903 * IMAS_AMU, &
    IMAS_MASS_Th_227 = 227.0277041 * IMAS_AMU, &
    IMAS_MASS_Th_228 = 228.0287411 * IMAS_AMU, &
    IMAS_MASS_Th_229 = 229.031762 * IMAS_AMU, &
    IMAS_MASS_Th_230 = 230.0331338 * IMAS_AMU, &
    IMAS_MASS_Th_231 = 231.0363043 * IMAS_AMU, &
    IMAS_MASS_Th_232 = 232.0380553 * IMAS_AMU, &
    IMAS_MASS_Th_233 = 233.0415818 * IMAS_AMU, &
    IMAS_MASS_Th_234 = 234.043601 * IMAS_AMU, &
    IMAS_MASS_Th_235 = 235.047510 * IMAS_AMU, &
    IMAS_MASS_Th_236 = 236.04987 * IMAS_AMU, &
    IMAS_MASS_Th_237 = 237.05389 * IMAS_AMU, &
    IMAS_MASS_Th_238 = 238.05650 * IMAS_AMU, &
    IMAS_MASS_Pa_212 = 212.023200 * IMAS_AMU, &
    IMAS_MASS_Pa_213 = 213.021110 * IMAS_AMU, &
    IMAS_MASS_Pa_214 = 214.020920 * IMAS_AMU, &
    IMAS_MASS_Pa_215 = 215.019190 * IMAS_AMU, &
    IMAS_MASS_Pa_216 = 216.019110 * IMAS_AMU, &
    IMAS_MASS_Pa_217 = 217.018320 * IMAS_AMU, &
    IMAS_MASS_Pa_218 = 218.020042 * IMAS_AMU, &
    IMAS_MASS_Pa_219 = 219.019880 * IMAS_AMU, &
    IMAS_MASS_Pa_220 = 220.021880 * IMAS_AMU, &
    IMAS_MASS_Pa_221 = 221.021880 * IMAS_AMU, &
    IMAS_MASS_Pa_222 = 222.023740 * IMAS_AMU, &
    IMAS_MASS_Pa_223 = 223.023960 * IMAS_AMU, &
    IMAS_MASS_Pa_224 = 224.025626 * IMAS_AMU, &
    IMAS_MASS_Pa_225 = 225.026130 * IMAS_AMU, &
    IMAS_MASS_Pa_226 = 226.027948 * IMAS_AMU, &
    IMAS_MASS_Pa_227 = 227.028805 * IMAS_AMU, &
    IMAS_MASS_Pa_228 = 228.031051 * IMAS_AMU, &
    IMAS_MASS_Pa_229 = 229.0320968 * IMAS_AMU, &
    IMAS_MASS_Pa_230 = 230.034541 * IMAS_AMU, &
    IMAS_MASS_Pa_231 = 231.0358840 * IMAS_AMU, &
    IMAS_MASS_Pa_232 = 232.038592 * IMAS_AMU, &
    IMAS_MASS_Pa_233 = 233.0402473 * IMAS_AMU, &
    IMAS_MASS_Pa_234 = 234.043308 * IMAS_AMU, &
    IMAS_MASS_Pa_235 = 235.045440 * IMAS_AMU, &
    IMAS_MASS_Pa_236 = 236.04868 * IMAS_AMU, &
    IMAS_MASS_Pa_237 = 237.05115 * IMAS_AMU, &
    IMAS_MASS_Pa_238 = 238.054500 * IMAS_AMU, &
    IMAS_MASS_Pa_239 = 239.05726 * IMAS_AMU, &
    IMAS_MASS_Pa_240 = 240.06098 * IMAS_AMU, &
    IMAS_MASS_U_217 = 217.024370 * IMAS_AMU, &
    IMAS_MASS_U_218 = 218.023540 * IMAS_AMU, &
    IMAS_MASS_U_219 = 219.024920 * IMAS_AMU, &
    IMAS_MASS_U_220 = 220.02472 * IMAS_AMU, &
    IMAS_MASS_U_221 = 221.02640 * IMAS_AMU, &
    IMAS_MASS_U_222 = 222.02609 * IMAS_AMU, &
    IMAS_MASS_U_223 = 223.027740 * IMAS_AMU, &
    IMAS_MASS_U_224 = 224.027605 * IMAS_AMU, &
    IMAS_MASS_U_225 = 225.029391 * IMAS_AMU, &
    IMAS_MASS_U_226 = 226.029339 * IMAS_AMU, &
    IMAS_MASS_U_227 = 227.031156 * IMAS_AMU, &
    IMAS_MASS_U_228 = 228.031374 * IMAS_AMU, &
    IMAS_MASS_U_229 = 229.033506 * IMAS_AMU, &
    IMAS_MASS_U_230 = 230.033940 * IMAS_AMU, &
    IMAS_MASS_U_231 = 231.036294 * IMAS_AMU, &
    IMAS_MASS_U_232 = 232.0371562 * IMAS_AMU, &
    IMAS_MASS_U_233 = 233.0396352 * IMAS_AMU, &
    IMAS_MASS_U_234 = 234.0409521 * IMAS_AMU, &
    IMAS_MASS_U_235 = 235.0439299 * IMAS_AMU, &
    IMAS_MASS_U_236 = 236.0455680 * IMAS_AMU, &
    IMAS_MASS_U_237 = 237.0487302 * IMAS_AMU, &
    IMAS_MASS_U_238 = 238.0507882 * IMAS_AMU, &
    IMAS_MASS_U_239 = 239.0542933 * IMAS_AMU, &
    IMAS_MASS_U_240 = 240.056592 * IMAS_AMU, &
    IMAS_MASS_U_241 = 241.06033 * IMAS_AMU, &
    IMAS_MASS_U_242 = 242.06293 * IMAS_AMU, &
    IMAS_MASS_Np_225 = 225.033910 * IMAS_AMU, &
    IMAS_MASS_Np_226 = 226.03515 * IMAS_AMU, &
    IMAS_MASS_Np_227 = 227.034960 * IMAS_AMU, &
    IMAS_MASS_Np_228 = 228.03618 * IMAS_AMU, &
    IMAS_MASS_Np_229 = 229.036260 * IMAS_AMU, &
    IMAS_MASS_Np_230 = 230.037830 * IMAS_AMU, &
    IMAS_MASS_Np_231 = 231.038250 * IMAS_AMU, &
    IMAS_MASS_Np_232 = 232.04011 * IMAS_AMU, &
    IMAS_MASS_Np_233 = 233.040740 * IMAS_AMU, &
    IMAS_MASS_Np_234 = 234.042895 * IMAS_AMU, &
    IMAS_MASS_Np_235 = 235.0440633 * IMAS_AMU, &
    IMAS_MASS_Np_236 = 236.046570 * IMAS_AMU, &
    IMAS_MASS_Np_237 = 237.0481734 * IMAS_AMU, &
    IMAS_MASS_Np_238 = 238.0509464 * IMAS_AMU, &
    IMAS_MASS_Np_239 = 239.0529390 * IMAS_AMU, &
    IMAS_MASS_Np_240 = 240.056162 * IMAS_AMU, &
    IMAS_MASS_Np_241 = 241.058250 * IMAS_AMU, &
    IMAS_MASS_Np_242 = 242.06164 * IMAS_AMU, &
    IMAS_MASS_Np_243 = 243.064280 * IMAS_AMU, &
    IMAS_MASS_Np_244 = 244.06785 * IMAS_AMU, &
    IMAS_MASS_Pu_228 = 228.038740 * IMAS_AMU, &
    IMAS_MASS_Pu_229 = 229.040150 * IMAS_AMU, &
    IMAS_MASS_Pu_230 = 230.039650 * IMAS_AMU, &
    IMAS_MASS_Pu_231 = 231.041101 * IMAS_AMU, &
    IMAS_MASS_Pu_232 = 232.041187 * IMAS_AMU, &
    IMAS_MASS_Pu_233 = 233.043000 * IMAS_AMU, &
    IMAS_MASS_Pu_234 = 234.043317 * IMAS_AMU, &
    IMAS_MASS_Pu_235 = 235.045286 * IMAS_AMU, &
    IMAS_MASS_Pu_236 = 236.0460580 * IMAS_AMU, &
    IMAS_MASS_Pu_237 = 237.0484097 * IMAS_AMU, &
    IMAS_MASS_Pu_238 = 238.0495599 * IMAS_AMU, &
    IMAS_MASS_Pu_239 = 239.0521634 * IMAS_AMU, &
    IMAS_MASS_Pu_240 = 240.0538135 * IMAS_AMU, &
    IMAS_MASS_Pu_241 = 241.0568515 * IMAS_AMU, &
    IMAS_MASS_Pu_242 = 242.0587426 * IMAS_AMU, &
    IMAS_MASS_Pu_243 = 243.062003 * IMAS_AMU, &
    IMAS_MASS_Pu_244 = 244.064204 * IMAS_AMU, &
    IMAS_MASS_Pu_245 = 245.067747 * IMAS_AMU, &
    IMAS_MASS_Pu_246 = 246.070205 * IMAS_AMU, &
    IMAS_MASS_Pu_247 = 247.07407 * IMAS_AMU, &
    IMAS_MASS_Am_231 = 231.04556 * IMAS_AMU, &
    IMAS_MASS_Am_232 = 232.04659 * IMAS_AMU, &
    IMAS_MASS_Am_233 = 233.04635 * IMAS_AMU, &
    IMAS_MASS_Am_234 = 234.04781 * IMAS_AMU, &
    IMAS_MASS_Am_235 = 235.04795 * IMAS_AMU, &
    IMAS_MASS_Am_236 = 236.04958 * IMAS_AMU, &
    IMAS_MASS_Am_237 = 237.050000 * IMAS_AMU, &
    IMAS_MASS_Am_238 = 238.051980 * IMAS_AMU, &
    IMAS_MASS_Am_239 = 239.0530245 * IMAS_AMU, &
    IMAS_MASS_Am_240 = 240.055300 * IMAS_AMU, &
    IMAS_MASS_Am_241 = 241.0568291 * IMAS_AMU, &
    IMAS_MASS_Am_242 = 242.0595492 * IMAS_AMU, &
    IMAS_MASS_Am_243 = 243.0613811 * IMAS_AMU, &
    IMAS_MASS_Am_244 = 244.0642848 * IMAS_AMU, &
    IMAS_MASS_Am_245 = 245.066452 * IMAS_AMU, &
    IMAS_MASS_Am_246 = 246.069775 * IMAS_AMU, &
    IMAS_MASS_Am_247 = 247.07209 * IMAS_AMU, &
    IMAS_MASS_Am_248 = 248.07575 * IMAS_AMU, &
    IMAS_MASS_Am_249 = 249.07848 * IMAS_AMU, &
    IMAS_MASS_Cm_233 = 233.050770 * IMAS_AMU, &
    IMAS_MASS_Cm_234 = 234.050160 * IMAS_AMU, &
    IMAS_MASS_Cm_235 = 235.05143 * IMAS_AMU, &
    IMAS_MASS_Cm_236 = 236.05141 * IMAS_AMU, &
    IMAS_MASS_Cm_237 = 237.05290 * IMAS_AMU, &
    IMAS_MASS_Cm_238 = 238.053030 * IMAS_AMU, &
    IMAS_MASS_Cm_239 = 239.05496 * IMAS_AMU, &
    IMAS_MASS_Cm_240 = 240.0555295 * IMAS_AMU, &
    IMAS_MASS_Cm_241 = 241.0576530 * IMAS_AMU, &
    IMAS_MASS_Cm_242 = 242.0588358 * IMAS_AMU, &
    IMAS_MASS_Cm_243 = 243.0613891 * IMAS_AMU, &
    IMAS_MASS_Cm_244 = 244.0627526 * IMAS_AMU, &
    IMAS_MASS_Cm_245 = 245.0654912 * IMAS_AMU, &
    IMAS_MASS_Cm_246 = 246.0672237 * IMAS_AMU, &
    IMAS_MASS_Cm_247 = 247.070354 * IMAS_AMU, &
    IMAS_MASS_Cm_248 = 248.072349 * IMAS_AMU, &
    IMAS_MASS_Cm_249 = 249.075953 * IMAS_AMU, &
    IMAS_MASS_Cm_250 = 250.078357 * IMAS_AMU, &
    IMAS_MASS_Cm_251 = 251.082285 * IMAS_AMU, &
    IMAS_MASS_Cm_252 = 252.08487 * IMAS_AMU, &
    IMAS_MASS_Bk_235 = 235.05658 * IMAS_AMU, &
    IMAS_MASS_Bk_236 = 236.05733 * IMAS_AMU, &
    IMAS_MASS_Bk_237 = 237.05700 * IMAS_AMU, &
    IMAS_MASS_Bk_238 = 238.05828 * IMAS_AMU, &
    IMAS_MASS_Bk_239 = 239.05828 * IMAS_AMU, &
    IMAS_MASS_Bk_240 = 240.05976 * IMAS_AMU, &
    IMAS_MASS_Bk_241 = 241.06023 * IMAS_AMU, &
    IMAS_MASS_Bk_242 = 242.06198 * IMAS_AMU, &
    IMAS_MASS_Bk_243 = 243.063008 * IMAS_AMU, &
    IMAS_MASS_Bk_244 = 244.065181 * IMAS_AMU, &
    IMAS_MASS_Bk_245 = 245.0663616 * IMAS_AMU, &
    IMAS_MASS_Bk_246 = 246.068670 * IMAS_AMU, &
    IMAS_MASS_Bk_247 = 247.070307 * IMAS_AMU, &
    IMAS_MASS_Bk_248 = 248.073090 * IMAS_AMU, &
    IMAS_MASS_Bk_249 = 249.0749867 * IMAS_AMU, &
    IMAS_MASS_Bk_250 = 250.078317 * IMAS_AMU, &
    IMAS_MASS_Bk_251 = 251.080760 * IMAS_AMU, &
    IMAS_MASS_Bk_252 = 252.08431 * IMAS_AMU, &
    IMAS_MASS_Bk_253 = 253.08688 * IMAS_AMU, &
    IMAS_MASS_Bk_254 = 254.09060 * IMAS_AMU, &
    IMAS_MASS_Cf_237 = 237.06207 * IMAS_AMU, &
    IMAS_MASS_Cf_238 = 238.06141 * IMAS_AMU, &
    IMAS_MASS_Cf_239 = 239.06242 * IMAS_AMU, &
    IMAS_MASS_Cf_240 = 240.06230 * IMAS_AMU, &
    IMAS_MASS_Cf_241 = 241.06373 * IMAS_AMU, &
    IMAS_MASS_Cf_242 = 242.063700 * IMAS_AMU, &
    IMAS_MASS_Cf_243 = 243.06543 * IMAS_AMU, &
    IMAS_MASS_Cf_244 = 244.066001 * IMAS_AMU, &
    IMAS_MASS_Cf_245 = 245.068049 * IMAS_AMU, &
    IMAS_MASS_Cf_246 = 246.0688053 * IMAS_AMU, &
    IMAS_MASS_Cf_247 = 247.071001 * IMAS_AMU, &
    IMAS_MASS_Cf_248 = 248.072185 * IMAS_AMU, &
    IMAS_MASS_Cf_249 = 249.0748535 * IMAS_AMU, &
    IMAS_MASS_Cf_250 = 250.0764061 * IMAS_AMU, &
    IMAS_MASS_Cf_251 = 251.079587 * IMAS_AMU, &
    IMAS_MASS_Cf_252 = 252.081626 * IMAS_AMU, &
    IMAS_MASS_Cf_253 = 253.085133 * IMAS_AMU, &
    IMAS_MASS_Cf_254 = 254.087323 * IMAS_AMU, &
    IMAS_MASS_Cf_255 = 255.09105 * IMAS_AMU, &
    IMAS_MASS_Cf_256 = 256.09344 * IMAS_AMU, &
    IMAS_MASS_Es_240 = 240.06892 * IMAS_AMU, &
    IMAS_MASS_Es_241 = 241.06854 * IMAS_AMU, &
    IMAS_MASS_Es_242 = 242.06975 * IMAS_AMU, &
    IMAS_MASS_Es_243 = 243.06955 * IMAS_AMU, &
    IMAS_MASS_Es_244 = 244.07088 * IMAS_AMU, &
    IMAS_MASS_Es_245 = 245.07132 * IMAS_AMU, &
    IMAS_MASS_Es_246 = 246.07290 * IMAS_AMU, &
    IMAS_MASS_Es_247 = 247.073660 * IMAS_AMU, &
    IMAS_MASS_Es_248 = 248.075470 * IMAS_AMU, &
    IMAS_MASS_Es_249 = 249.076410 * IMAS_AMU, &
    IMAS_MASS_Es_250 = 250.07861 * IMAS_AMU, &
    IMAS_MASS_Es_251 = 251.079992 * IMAS_AMU, &
    IMAS_MASS_Es_252 = 252.082980 * IMAS_AMU, &
    IMAS_MASS_Es_253 = 253.0848247 * IMAS_AMU, &
    IMAS_MASS_Es_254 = 254.088022 * IMAS_AMU, &
    IMAS_MASS_Es_255 = 255.090273 * IMAS_AMU, &
    IMAS_MASS_Es_256 = 256.09360 * IMAS_AMU, &
    IMAS_MASS_Es_257 = 257.09598 * IMAS_AMU, &
    IMAS_MASS_Es_258 = 258.09952 * IMAS_AMU, &
    IMAS_MASS_Fm_242 = 242.07343 * IMAS_AMU, &
    IMAS_MASS_Fm_243 = 243.07435 * IMAS_AMU, &
    IMAS_MASS_Fm_244 = 244.07408 * IMAS_AMU, &
    IMAS_MASS_Fm_245 = 245.07539 * IMAS_AMU, &
    IMAS_MASS_Fm_246 = 246.075300 * IMAS_AMU, &
    IMAS_MASS_Fm_247 = 247.07685 * IMAS_AMU, &
    IMAS_MASS_Fm_248 = 248.077195 * IMAS_AMU, &
    IMAS_MASS_Fm_249 = 249.07903 * IMAS_AMU, &
    IMAS_MASS_Fm_250 = 250.079521 * IMAS_AMU, &
    IMAS_MASS_Fm_251 = 251.081575 * IMAS_AMU, &
    IMAS_MASS_Fm_252 = 252.082467 * IMAS_AMU, &
    IMAS_MASS_Fm_253 = 253.085185 * IMAS_AMU, &
    IMAS_MASS_Fm_254 = 254.0868542 * IMAS_AMU, &
    IMAS_MASS_Fm_255 = 255.089962 * IMAS_AMU, &
    IMAS_MASS_Fm_256 = 256.091773 * IMAS_AMU, &
    IMAS_MASS_Fm_257 = 257.095105 * IMAS_AMU, &
    IMAS_MASS_Fm_258 = 258.09708 * IMAS_AMU, &
    IMAS_MASS_Fm_259 = 259.10060 * IMAS_AMU, &
    IMAS_MASS_Fm_260 = 260.10268 * IMAS_AMU, &
    IMAS_MASS_Md_245 = 245.08083 * IMAS_AMU, &
    IMAS_MASS_Md_246 = 246.08189 * IMAS_AMU, &
    IMAS_MASS_Md_247 = 247.08164 * IMAS_AMU, &
    IMAS_MASS_Md_248 = 248.08282 * IMAS_AMU, &
    IMAS_MASS_Md_249 = 249.08301 * IMAS_AMU, &
    IMAS_MASS_Md_250 = 250.08442 * IMAS_AMU, &
    IMAS_MASS_Md_251 = 251.08484 * IMAS_AMU, &
    IMAS_MASS_Md_252 = 252.08656 * IMAS_AMU, &
    IMAS_MASS_Md_253 = 253.08728 * IMAS_AMU, &
    IMAS_MASS_Md_254 = 254.08966 * IMAS_AMU, &
    IMAS_MASS_Md_255 = 255.091083 * IMAS_AMU, &
    IMAS_MASS_Md_256 = 256.094060 * IMAS_AMU, &
    IMAS_MASS_Md_257 = 257.095541 * IMAS_AMU, &
    IMAS_MASS_Md_258 = 258.098431 * IMAS_AMU, &
    IMAS_MASS_Md_259 = 259.10051 * IMAS_AMU, &
    IMAS_MASS_Md_260 = 260.10365 * IMAS_AMU, &
    IMAS_MASS_Md_261 = 261.10572 * IMAS_AMU, &
    IMAS_MASS_Md_262 = 262.10887 * IMAS_AMU, &
    IMAS_MASS_No_248 = 248.08660 * IMAS_AMU, &
    IMAS_MASS_No_249 = 249.08783 * IMAS_AMU, &
    IMAS_MASS_No_250 = 250.08751 * IMAS_AMU, &
    IMAS_MASS_No_251 = 251.08901 * IMAS_AMU, &
    IMAS_MASS_No_252 = 252.088977 * IMAS_AMU, &
    IMAS_MASS_No_253 = 253.09068 * IMAS_AMU, &
    IMAS_MASS_No_254 = 254.090955 * IMAS_AMU, &
    IMAS_MASS_No_255 = 255.093241 * IMAS_AMU, &
    IMAS_MASS_No_256 = 256.094283 * IMAS_AMU, &
    IMAS_MASS_No_257 = 257.096877 * IMAS_AMU, &
    IMAS_MASS_No_258 = 258.09821 * IMAS_AMU, &
    IMAS_MASS_No_259 = 259.10103 * IMAS_AMU, &
    IMAS_MASS_No_260 = 260.10264 * IMAS_AMU, &
    IMAS_MASS_No_261 = 261.10575 * IMAS_AMU, &
    IMAS_MASS_No_262 = 262.10730 * IMAS_AMU, &
    IMAS_MASS_No_263 = 263.11055 * IMAS_AMU, &
    IMAS_MASS_No_264 = 264.11235 * IMAS_AMU, &
    IMAS_MASS_Lr_251 = 251.09436 * IMAS_AMU, &
    IMAS_MASS_Lr_252 = 252.09537 * IMAS_AMU, &
    IMAS_MASS_Lr_253 = 253.09521 * IMAS_AMU, &
    IMAS_MASS_Lr_254 = 254.09645 * IMAS_AMU, &
    IMAS_MASS_Lr_255 = 255.09668 * IMAS_AMU, &
    IMAS_MASS_Lr_256 = 256.09863 * IMAS_AMU, &
    IMAS_MASS_Lr_257 = 257.09956 * IMAS_AMU, &
    IMAS_MASS_Lr_258 = 258.10181 * IMAS_AMU, &
    IMAS_MASS_Lr_259 = 259.102900 * IMAS_AMU, &
    IMAS_MASS_Lr_260 = 260.10550 * IMAS_AMU, &
    IMAS_MASS_Lr_261 = 261.10688 * IMAS_AMU, &
    IMAS_MASS_Lr_262 = 262.10963 * IMAS_AMU, &
    IMAS_MASS_Lr_263 = 263.11129 * IMAS_AMU, &
    IMAS_MASS_Lr_264 = 264.11404 * IMAS_AMU, &
    IMAS_MASS_Lr_265 = 265.11584 * IMAS_AMU, &
    IMAS_MASS_Lr_266 = 266.11931 * IMAS_AMU, &
    IMAS_MASS_Rf_253 = 253.10069 * IMAS_AMU, &
    IMAS_MASS_Rf_254 = 254.10018 * IMAS_AMU, &
    IMAS_MASS_Rf_255 = 255.10134 * IMAS_AMU, &
    IMAS_MASS_Rf_256 = 256.101166 * IMAS_AMU, &
    IMAS_MASS_Rf_257 = 257.10299 * IMAS_AMU, &
    IMAS_MASS_Rf_258 = 258.10349 * IMAS_AMU, &
    IMAS_MASS_Rf_259 = 259.105640 * IMAS_AMU, &
    IMAS_MASS_Rf_260 = 260.10644 * IMAS_AMU, &
    IMAS_MASS_Rf_261 = 261.108770 * IMAS_AMU, &
    IMAS_MASS_Rf_262 = 262.10993 * IMAS_AMU, &
    IMAS_MASS_Rf_263 = 263.11255 * IMAS_AMU, &
    IMAS_MASS_Rf_264 = 264.11399 * IMAS_AMU, &
    IMAS_MASS_Rf_265 = 265.11670 * IMAS_AMU, &
    IMAS_MASS_Rf_266 = 266.11796 * IMAS_AMU, &
    IMAS_MASS_Rf_267 = 267.12153 * IMAS_AMU, &
    IMAS_MASS_Rf_268 = 268.12364 * IMAS_AMU, &
    IMAS_MASS_Db_255 = 255.10740 * IMAS_AMU, &
    IMAS_MASS_Db_256 = 256.10813 * IMAS_AMU, &
    IMAS_MASS_Db_257 = 257.10772 * IMAS_AMU, &
    IMAS_MASS_Db_258 = 258.10923 * IMAS_AMU, &
    IMAS_MASS_Db_259 = 259.10961 * IMAS_AMU, &
    IMAS_MASS_Db_260 = 260.11130 * IMAS_AMU, &
    IMAS_MASS_Db_261 = 261.11206 * IMAS_AMU, &
    IMAS_MASS_Db_262 = 262.11408 * IMAS_AMU, &
    IMAS_MASS_Db_263 = 263.11499 * IMAS_AMU, &
    IMAS_MASS_Db_264 = 264.11740 * IMAS_AMU, &
    IMAS_MASS_Db_265 = 265.11860 * IMAS_AMU, &
    IMAS_MASS_Db_266 = 266.12103 * IMAS_AMU, &
    IMAS_MASS_Db_267 = 267.12238 * IMAS_AMU, &
    IMAS_MASS_Db_268 = 268.12545 * IMAS_AMU, &
    IMAS_MASS_Db_269 = 269.12746 * IMAS_AMU, &
    IMAS_MASS_Db_270 = 270.13071 * IMAS_AMU, &
    IMAS_MASS_Sg_258 = 258.11317 * IMAS_AMU, &
    IMAS_MASS_Sg_259 = 259.11450 * IMAS_AMU, &
    IMAS_MASS_Sg_260 = 260.114420 * IMAS_AMU, &
    IMAS_MASS_Sg_261 = 261.11612 * IMAS_AMU, &
    IMAS_MASS_Sg_262 = 262.11640 * IMAS_AMU, &
    IMAS_MASS_Sg_263 = 263.11832 * IMAS_AMU, &
    IMAS_MASS_Sg_264 = 264.11893 * IMAS_AMU, &
    IMAS_MASS_Sg_265 = 265.121110 * IMAS_AMU, &
    IMAS_MASS_Sg_266 = 266.12207 * IMAS_AMU, &
    IMAS_MASS_Sg_267 = 267.12443 * IMAS_AMU, &
    IMAS_MASS_Sg_268 = 268.12561 * IMAS_AMU, &
    IMAS_MASS_Sg_269 = 269.12876 * IMAS_AMU, &
    IMAS_MASS_Sg_270 = 270.13033 * IMAS_AMU, &
    IMAS_MASS_Sg_271 = 271.13347 * IMAS_AMU, &
    IMAS_MASS_Sg_272 = 272.13516 * IMAS_AMU, &
    IMAS_MASS_Sg_273 = 273.13822 * IMAS_AMU, &
    IMAS_MASS_Bh_260 = 260.12197 * IMAS_AMU, &
    IMAS_MASS_Bh_261 = 261.12166 * IMAS_AMU, &
    IMAS_MASS_Bh_262 = 262.12289 * IMAS_AMU, &
    IMAS_MASS_Bh_263 = 263.12304 * IMAS_AMU, &
    IMAS_MASS_Bh_264 = 264.12460 * IMAS_AMU, &
    IMAS_MASS_Bh_265 = 265.12515 * IMAS_AMU, &
    IMAS_MASS_Bh_266 = 266.12694 * IMAS_AMU, &
    IMAS_MASS_Bh_267 = 267.12765 * IMAS_AMU, &
    IMAS_MASS_Bh_268 = 268.12976 * IMAS_AMU, &
    IMAS_MASS_Bh_269 = 269.13069 * IMAS_AMU, &
    IMAS_MASS_Bh_270 = 270.13362 * IMAS_AMU, &
    IMAS_MASS_Bh_271 = 271.13518 * IMAS_AMU, &
    IMAS_MASS_Bh_272 = 272.13803 * IMAS_AMU, &
    IMAS_MASS_Bh_273 = 273.13962 * IMAS_AMU, &
    IMAS_MASS_Bh_274 = 274.14244 * IMAS_AMU, &
    IMAS_MASS_Bh_275 = 275.14425 * IMAS_AMU, &
    IMAS_MASS_Hs_263 = 263.12856 * IMAS_AMU, &
    IMAS_MASS_Hs_264 = 264.128390 * IMAS_AMU, &
    IMAS_MASS_Hs_265 = 265.13009 * IMAS_AMU, &
    IMAS_MASS_Hs_266 = 266.13010 * IMAS_AMU, &
    IMAS_MASS_Hs_267 = 267.13179 * IMAS_AMU, &
    IMAS_MASS_Hs_268 = 268.13216 * IMAS_AMU, &
    IMAS_MASS_Hs_269 = 269.13406 * IMAS_AMU, &
    IMAS_MASS_Hs_270 = 270.13465 * IMAS_AMU, &
    IMAS_MASS_Hs_271 = 271.13766 * IMAS_AMU, &
    IMAS_MASS_Hs_272 = 272.13905 * IMAS_AMU, &
    IMAS_MASS_Hs_273 = 273.14199 * IMAS_AMU, &
    IMAS_MASS_Hs_274 = 274.14313 * IMAS_AMU, &
    IMAS_MASS_Hs_275 = 275.14595 * IMAS_AMU, &
    IMAS_MASS_Hs_276 = 276.14721 * IMAS_AMU, &
    IMAS_MASS_Hs_277 = 277.14984 * IMAS_AMU, &
    IMAS_MASS_Mt_265 = 265.13615 * IMAS_AMU, &
    IMAS_MASS_Mt_266 = 266.13730 * IMAS_AMU, &
    IMAS_MASS_Mt_267 = 267.13731 * IMAS_AMU, &
    IMAS_MASS_Mt_268 = 268.13873 * IMAS_AMU, &
    IMAS_MASS_Mt_269 = 269.13906 * IMAS_AMU, &
    IMAS_MASS_Mt_270 = 270.14066 * IMAS_AMU, &
    IMAS_MASS_Mt_271 = 271.14114 * IMAS_AMU, &
    IMAS_MASS_Mt_272 = 272.14374 * IMAS_AMU, &
    IMAS_MASS_Mt_273 = 273.14491 * IMAS_AMU, &
    IMAS_MASS_Mt_274 = 274.14749 * IMAS_AMU, &
    IMAS_MASS_Mt_275 = 275.14865 * IMAS_AMU, &
    IMAS_MASS_Mt_276 = 276.15116 * IMAS_AMU, &
    IMAS_MASS_Mt_277 = 277.15242 * IMAS_AMU, &
    IMAS_MASS_Mt_278 = 278.15481 * IMAS_AMU, &
    IMAS_MASS_Mt_279 = 279.15619 * IMAS_AMU, &
    IMAS_MASS_Ds_267 = 267.14434 * IMAS_AMU, &
    IMAS_MASS_Ds_268 = 268.14380 * IMAS_AMU, &
    IMAS_MASS_Ds_269 = 269.14512 * IMAS_AMU, &
    IMAS_MASS_Ds_270 = 270.14472 * IMAS_AMU, &
    IMAS_MASS_Ds_271 = 271.14606 * IMAS_AMU, &
    IMAS_MASS_Ds_272 = 272.14632 * IMAS_AMU, &
    IMAS_MASS_Ds_273 = 273.14886 * IMAS_AMU, &
    IMAS_MASS_Ds_274 = 274.14949 * IMAS_AMU, &
    IMAS_MASS_Ds_275 = 275.15218 * IMAS_AMU, &
    IMAS_MASS_Ds_276 = 276.15303 * IMAS_AMU, &
    IMAS_MASS_Ds_277 = 277.15565 * IMAS_AMU, &
    IMAS_MASS_Ds_278 = 278.15647 * IMAS_AMU, &
    IMAS_MASS_Ds_279 = 279.15886 * IMAS_AMU, &
    IMAS_MASS_Ds_280 = 280.15980 * IMAS_AMU, &
    IMAS_MASS_Ds_281 = 281.16206 * IMAS_AMU, &
    IMAS_MASS_Rg_272 = 272.15362 * IMAS_AMU, &
    IMAS_MASS_Rg_273 = 273.15368 * IMAS_AMU, &
    IMAS_MASS_Rg_274 = 274.15571 * IMAS_AMU, &
    IMAS_MASS_Rg_275 = 275.15614 * IMAS_AMU, &
    IMAS_MASS_Rg_276 = 276.15849 * IMAS_AMU, &
    IMAS_MASS_Rg_277 = 277.15952 * IMAS_AMU, &
    IMAS_MASS_Rg_278 = 278.16160 * IMAS_AMU, &
    IMAS_MASS_Rg_279 = 279.16247 * IMAS_AMU, &
    IMAS_MASS_Rg_280 = 280.16447 * IMAS_AMU, &
    IMAS_MASS_Rg_281 = 281.16537 * IMAS_AMU, &
    IMAS_MASS_Rg_282 = 282.16749 * IMAS_AMU, &
    IMAS_MASS_Rg_283 = 283.16842 * IMAS_AMU, &
    IMAS_MASS_Cn_277 = 277.16394 * IMAS_AMU, &
    IMAS_MASS_Cn_278 = 278.16431 * IMAS_AMU, &
    IMAS_MASS_Cn_279 = 279.16655 * IMAS_AMU, &
    IMAS_MASS_Cn_280 = 280.16704 * IMAS_AMU, &
    IMAS_MASS_Cn_281 = 281.16929 * IMAS_AMU, &
    IMAS_MASS_Cn_282 = 282.16977 * IMAS_AMU, &
    IMAS_MASS_Cn_283 = 283.17179 * IMAS_AMU, &
    IMAS_MASS_Cn_284 = 284.17238 * IMAS_AMU, &
    IMAS_MASS_Cn_285 = 285.17411 * IMAS_AMU, &
    IMAS_MASS_Uut_283 = 283.17645 * IMAS_AMU, &
    IMAS_MASS_Uut_284 = 284.17808 * IMAS_AMU, &
    IMAS_MASS_Uut_285 = 285.17873 * IMAS_AMU, &
    IMAS_MASS_Uut_286 = 286.18048 * IMAS_AMU, &
    IMAS_MASS_Uut_287 = 287.18105 * IMAS_AMU, &
    IMAS_MASS_Uuq_285 = 285.18370 * IMAS_AMU, &
    IMAS_MASS_Uuq_286 = 286.18386 * IMAS_AMU, &
    IMAS_MASS_Uuq_287 = 287.18560 * IMAS_AMU, &
    IMAS_MASS_Uuq_288 = 288.18569 * IMAS_AMU, &
    IMAS_MASS_Uuq_289 = 289.18728 * IMAS_AMU, &
    IMAS_MASS_Uup_287 = 287.19119 * IMAS_AMU, &
    IMAS_MASS_Uup_288 = 288.19249 * IMAS_AMU, &
    IMAS_MASS_Uup_289 = 289.19272 * IMAS_AMU, &
    IMAS_MASS_Uup_290 = 290.19414 * IMAS_AMU, &
    IMAS_MASS_Uup_291 = 291.19438 * IMAS_AMU, &
    IMAS_MASS_Uuh_289 = 289.19886 * IMAS_AMU, &
    IMAS_MASS_Uuh_290 = 290.19859 * IMAS_AMU, &
    IMAS_MASS_Uuh_291 = 291.20001 * IMAS_AMU, &
    IMAS_MASS_Uuh_292 = 292.19979 * IMAS_AMU, &
    IMAS_MASS_Uus_291 = 291.20656 * IMAS_AMU, &
    IMAS_MASS_Uus_292 = 292.20755 * IMAS_AMU, &
    IMAS_MASS_Uuo_293 = 293.21467 * IMAS_AMU, &
    version=-999999999)

end module imas_imas_constants
