! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*DECK U20
!*CALL PROCESS
SUBROUTINE RVAR(KNAME,PVALUE)
  !        -----------------------------
  !**********************************************************************
  !                                                                     *
  ! U.20   PRINT NAME AND VALUE OF REAL VARIABLE                        *
  !                                                                     *
  !**********************************************************************
  !
  !
  USE globals
  IMPLICIT NONE
  REAL(RKIND)      ::     PVALUE
  CHARACTER*(*) KNAME
  !
  WRITE (6,9900) KNAME, PVALUE
  !
  RETURN
9900 FORMAT(/,1X,A,' =',1PE17.8)
END SUBROUTINE RVAR
