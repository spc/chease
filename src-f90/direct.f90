! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*DECK C2SE02
!*CALL PROCESS
SUBROUTINE DIRECT
  !        #################
  !
  !                                        AUTHORS:
  !                                        H. LUTJENS,  CRPP-EPFL
  !                                        A. BONDESON, CRPP-EPFL
  !
  !**********************************************************************
  !                                                                     *
  ! C2SE02 GAUSS SEIDEL ELIMINATION                                     *
  !                                                                     *
  !**********************************************************************
  !
  USE globals
  IMPLICIT NONE
  !
  !**********************************************************************
  !                                                                     *
  ! 1. SOLVE L * Y = B                                                  *
  !                                                                     *
  !**********************************************************************
  !
  CALL LYV(A,B,N4NSNT,NP4NST,NBAND,NPBAND)
  !
  !**********************************************************************
  !                                                                     *
  ! 2. SOLVE D * W = Y                                                  *
  !                                                                     *
  !**********************************************************************
  !
  CALL DWY(A,B,N4NSNT,NP4NST,NBAND,NPBAND)
  !
  !**********************************************************************
  !                                                                     *
  ! 3. SOLVE LT * X = W                                                 *
  !                                                                     *
  !**********************************************************************
  !
  CALL LTXW(A,B,N4NSNT,NP4NST,NBAND,NPBAND)
  !
  !**********************************************************************
  !                                                                     *
  ! 4. RESULT IN B                                                      *
  !                                                                     *
  !**********************************************************************
  !
  RETURN
END SUBROUTINE DIRECT
