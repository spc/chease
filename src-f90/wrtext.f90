! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*DECK U36
!*CALL PROCESS
SUBROUTINE WRTEXTSCALAR(VAR,CTEXT,TEXT,K)
  !        --------------------------------------
  ! scalar version for compilers differentiating scalars and dim(1)
  !
  USE globals
  IMPLICIT NONE
  INTEGER          ::     KN
  INTEGER          ::     L
  REAL(RKIND)      ::     VAR
  INTEGER          ::     K
  CHARACTER*(*) TEXT,CTEXT
  !
  IF (K .EQ. 1) THEN
     WRITE(TEXT,9900) CTEXT,VAR
  ELSE IF (K .EQ. 2) THEN
     WRITE(TEXT,9901) CTEXT,VAR
  ELSE
     WRITE(0,*) 'option K=',K,' not defined yet in WRTEXTSCALAR'
  ENDIF
  !
  RETURN
9900 FORMAT(A,' = ',1PG13.5)
9901 FORMAT(A,' = ',1PG13.5,'%')
END SUBROUTINE WRTEXTSCALAR
SUBROUTINE WRTEXTARRAY(VAR,CTEXT,TEXT,K,KN)
  !        --------------------------------------
  !
  USE globals
  IMPLICIT NONE
  INTEGER          ::     KN
  INTEGER          ::     L
  REAL(RKIND)      ::     VAR
  INTEGER          ::     K
  DIMENSION VAR(KN)
  CHARACTER*(*) TEXT,CTEXT
  !
  IF (K .EQ. 1) THEN
     WRITE(TEXT,9900) CTEXT,VAR(1)
  ELSE IF (K .EQ. 2) THEN
     WRITE(TEXT,9901) CTEXT,VAR(1)
  ELSE IF (K .EQ. 3) THEN
     WRITE(TEXT,9902) CTEXT,(VAR(L),L=1,KN)
  ENDIF
  !
  RETURN
9900 FORMAT(A,' = ',1PG13.5)
9901 FORMAT(A,' = ',1PG13.5,'%')
9902 FORMAT(A,' = ',13(1PG12.4))
END SUBROUTINE WRTEXTARRAY
