! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
subroutine write_imas(equil_out,kitmopt,kitmshot,kitmrun,kitmocc,citmtree)
  !
  use globals
  use ids_schemas                       ! module containing the equilibrium type definitions
  use ids_routines
  IMPLICIT NONE
  type(ids_equilibrium)     :: equil_out
  character*120  :: citmtree
  integer        :: kitmopt, kitmshot, kitmrun, kitmocc
  integer        :: idx, status, ids_backend
  !
  character(len=11) :: signal_name = 'equilibrium'
  character(len=16) :: str_equil = 'equilibrium'
  character(len=5)  :: citmtree2
  character(strmaxlen) :: uri
  !
  if (kitmshot .eq. 22) then
    if (nverbose .ge. 3) print *,' Do not write since shot number to write: ',kitmshot
    return
  end if
  if (kitmocc .eq. 0) then
    str_equil = signal_name
  else
    write(str_equil,'(A11,A1,I0)') signal_name,'/',kitmocc
  end if
  if (nverbose .ge. 3) print *,'signal_name= ',str_equil
  !
  !equil_out%datainfo%comment='CHEASE: Reconstruction of \TOP.equilibria.eq.NNN001 in euitm_imp1 tree'
  !equil_out%coord_sys%grid_type='CHEASE: Straight field line coordinates, Power of 2'
  !equil_out%coord_sys%type%dat='CHEASE: Straight field line coordinates'
  !equil_out%coord_sys%type%dat='CHEASE: Equal arc coordinates'
  !equil_out%coord_sys%type%dat='CHEASE: Theta-Psi coordinates'

  !  signal_name ='\\TOP.equilibria.eq.NNN006.'

  citmtree2 = trim(citmtree)
  if (kitmshot .lt. 0) then
    kitmshot = abs(kitmshot)
  else
    if (nverbose .ge. 3) then
      write(*,*) 'before al_build_uri_...,  tree= ',citmtree2,' shot= ',kitmshot,' run= ',kitmrun,' kitmopt= ',kitmopt
      call flush(6)
    end if
    if ((tree_backend(2) .lt. 12) .or. (tree_backend(2) .gt. 13)) then
      if (nverbose .ge. 1) then
        write(0,*) ' WARNING: tree_backend(2) = ',tree_backend(2),' not well defined, should be  13 (hdf5) or 12 (mdsplus), set to default=hdf5'
      end if
      tree_backend(2) = 13
    end if
    ids_backend = tree_backend(2)
    ! create uri
    call al_build_uri_from_legacy_parameters(ids_backend, kitmshot, kitmrun, trim(tree_user(2)), trim(tree_tokamak(2)),tree_majorversion(2), '', uri, status)
    if (nverbose .ge. 3) then
      write(*,*) 'Prepared uri: ', trim(uri)
      write(*,*) 'status: ', status
    end if
    if (kitmopt .ge. 20) then
      call imas_open(uri, OPEN_PULSE, idx , status)
      if (status .ne. 0) then
        write(0,*) ' ERROR: could not open pulse to add new IDS, idx= ', idx,' status= ',status
        write(*,*) ' ERROR: could not open pulse to add new IDS, idx= ', idx,' status= ',status
        equil_out%code%output_flag = -411
        return
      end if
    else
      if (tree_backend(2) .eq. 12) then
        call imas_open(uri, FORCE_CREATE_PULSE, idx , status) ! FORCE create for mdsplus since FORCE_OPEN_PULSE does not work yet
      else
        call imas_open(uri, FORCE_OPEN_PULSE, idx , status)
      end if
    end if
    if (nverbose .ge. 3) then
      write(*,*) 'after imas_open, uri = ',trim(uri) ! trim important since strmaxlen very large
      write(*,*) 'after imas_open, idx= ',idx,' tree= ',citmtree2,'trim(tree_user(2)),trim(tree_tokamak(2)), &
        & tree_majorversion(2)= ',trim(tree_user(2)),', ',trim(tree_tokamak(2)),', ',tree_majorversion(2),', status = ',status
      call flush(6)
    end if
  end if
  !
  if (nverbose .ge. 3) then
    ! print *,' equil_out%time_slice(1)%profiles_1d%b_field_min= ',equil_out%time_slice(1)%profiles_1d%b_field_min
    ! print *,' equil_out%time_slice(1)%profiles_1d%b_min= ',equil_out%time_slice(1)%profiles_1d%b_min
    print *,' equil_out%time= ',equil_out%time
    print *,' equil_out%ids_properties%homogeneous_time = ',equil_out%ids_properties%homogeneous_time
  end if
  call ids_put(idx,trim(str_equil),equil_out)
  call imas_close(idx,status)
  if (nverbose .ge. 3) then
    write(*,*) 'after imas_close: idx, status, trim(str_equil): ', idx, status,trim(str_equil)
  end if

  return
end subroutine write_imas
