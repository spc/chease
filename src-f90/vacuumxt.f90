! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*DECK C2SM13
!*CALL PROCESS
SUBROUTINE VACUUMXT(KCASE)
  !        #####################
  !
  !                                        AUTHORS:
  !                                        H. LUTJENS,  CRPP-EPFL
  !                                        A. BONDESON, CRPP-EPFL
  !**********************************************************************
  !                                                                     *
  ! C2SM13 COMPUTE VACUUM EQ'S FOR MARS (SEE SECTION 5.4.2 AND TABLE 2  *
  !        IN PUBLICATION)                                              *
  !                                                                     *
  !**********************************************************************
  !
  USE globals
  IMPLICIT NONE
  !
  INTEGER          ::     KCASE
  REAL(RKIND)      ::     ZDPDZ
  REAL(RKIND)      ::     ZDPDR
  REAL(RKIND)      ::     ZDTDZ
  REAL(RKIND)      ::     ZDSDZ
  REAL(RKIND)      ::     ZDTDR
  REAL(RKIND)      ::     ZDSDR
  REAL(RKIND)      ::     ZDSUM
  REAL(RKIND)      ::     ZJAC
  REAL(RKIND)      ::     ZDTTDR
  REAL(RKIND)      ::     ZDTTDZ
  REAL(RKIND)      ::     ZDRSDT
  REAL(RKIND)      ::     ZDPDT
  REAL(RKIND)      ::     ZDPDS
  REAL(RKIND)      ::     ZZ
  REAL(RKIND)      ::     ZR
  REAL(RKIND)      ::     ZRHO
  REAL(RKIND)      ::     ZBND2
  REAL(RKIND)      ::     ZSINT
  REAL(RKIND)      ::     ZCOST
  INTEGER          ::     J1,J2,J3,J4,J5,J6,J7,JT,L,NTT
  REAL(RKIND)      ::     ZEPS
  INTEGER,     DIMENSION(:),   ALLOCATABLE :: IC, IS0, IT0
  REAL(RKIND), DIMENSION(:),   ALLOCATABLE :: ZJACV, ZGSSVL, ZGSTVL, ZGTTVL, ZGPPVL, ZR1, ZRV, ZS, ZS1, ZS2, ZSV
  REAL(RKIND), DIMENSION(:),   ALLOCATABLE :: ZT, ZT1, ZT2, ZZ1, ZZV, ZDR1DTT, ZDZ1DTT
  REAL(RKIND), DIMENSION(:,:), ALLOCATABLE :: ZBND, ZDBDS, ZDBDT, ZPCEL, ZTETA
  !
  !----*----*----*---*----*----*----*----*----*----*----*----*----*----*-
  !
  ZEPS = 1.E-3_RKIND
  !
  IF (KCASE==1) THEN
     NTT=NTNOVA
  ELSE IF (KCASE==2) THEN
     NTT=MDT*NTNOVA
  ELSE
     PRINT*, 'OPTION KCASE=',KCASE,' NOT AVAILABLE IN VACUUMXT'
     STOP
  ENDIF
  !
  ALLOCATE(IC(NTT),IS0(NTT),IT0(NTT),ZBND(NTT,5),ZDBDS(NTT,16),ZDBDT(NTT,16), &
       &   ZDR1DTT(NTT),ZDZ1DTT(NTT),ZJACV(NTT),ZGSSVL(NTT),ZGSTVL(NTT),ZGTTVL(NTT),ZGPPVL(NTT), &
       &   ZPCEL(NTT,16),ZR1(NTT),ZRV(NTT),ZS(NTT),ZS1(NTT),ZS2(NTT),ZSV(2*NV+1), &
       &   ZTETA(NTT,5),ZT(NTT),ZT1(NTT),ZT2(NTT),ZZ1(NTT),ZZV(NTT))
  !
  DO J1=1,NV
     !
     ZSV(2*(J1-1)+1) = CSV(J1)
     ZSV(2*J1      ) = CSMV(J1)
     !
  END DO
  !
  ZSV(2*NV+1) = CSV(NV1)
  !
  DO J2=1,NTT
     !
     ZTETA(J2,1) = TETPEN(J2)
     ZTETA(J2,2) = TETPEN(J2) - 2._RKIND * ZEPS
     ZTETA(J2,3) = TETPEN(J2) -      ZEPS
     ZTETA(J2,4) = TETPEN(J2) +      ZEPS
     ZTETA(J2,5) = TETPEN(J2) + 2._RKIND * ZEPS
     !
  END DO
  !
  CALL BOUND(NTT,ZTETA(1,1),ZBND(1,1))
  CALL BOUND(NTT,ZTETA(1,2),ZBND(1,2))
  CALL BOUND(NTT,ZTETA(1,3),ZBND(1,3))
  CALL BOUND(NTT,ZTETA(1,4),ZBND(1,4))
  CALL BOUND(NTT,ZTETA(1,5),ZBND(1,5))
  !
  CALL RESETI(IC,NTT,1)
  DO JT = 1,NT1
     DO J3=1,NTT
        IF (IC(J3).EQ.1) THEN
           IT0(J3) = JT-1
           IF (TETPEN(J3).LE.CT(JT)) IC(J3)  = 0
        ENDIF
     ENDDO
  ENDDO
  DO J4=1,NTT
     IS0(J4) = NS
     !
     IF (IT0(J4) .GT. NT) IT0(J4) = NT
     IF (IT0(J4) .LT. 1)  IT0(J4) = 1
     !
     ZT(J4)  = TETPEN(J4)
     ZS(J4)  = 1._RKIND
     ZS1(J4) = CSIG(NS)
     ZS2(J4) = 1._RKIND
     ZT1(J4) = CT(IT0(J4))
     ZT2(J4) = CT(IT0(J4)+1)
  END DO
  !
  CALL PSICEL(IS0,IT0,NTT,NTT,ZPCEL,CPSICL)
  CALL BASIS2(NTT,NTT,ZS1,ZS2,ZT1,ZT2,ZS,ZT,ZDBDS,ZDBDT)
  !
  DO J5=1,NTT
     !
     ZDRSDT = (ZBND(J5,2) + 8._RKIND*(ZBND(J5,4) - ZBND(J5,3)) - &
          & ZBND(J5,5)) / (12._RKIND * ZEPS)
     !
     ZDPDS = ZDBDS(J5, 1) * ZPCEL(J5, 1) + &
          &           ZDBDS(J5, 2) * ZPCEL(J5, 2) + &
          &           ZDBDS(J5, 3) * ZPCEL(J5, 3) + &
          &           ZDBDS(J5, 4) * ZPCEL(J5, 4) + &
          &           ZDBDS(J5, 5) * ZPCEL(J5, 5) + &
          &           ZDBDS(J5, 6) * ZPCEL(J5, 6) + &
          &           ZDBDS(J5, 7) * ZPCEL(J5, 7) + &
          &           ZDBDS(J5, 8) * ZPCEL(J5, 8) + &
          &           ZDBDS(J5, 9) * ZPCEL(J5, 9) + &
          &           ZDBDS(J5,10) * ZPCEL(J5,10) + &
          &           ZDBDS(J5,11) * ZPCEL(J5,11) + &
          &           ZDBDS(J5,12) * ZPCEL(J5,12) + &
          &           ZDBDS(J5,13) * ZPCEL(J5,13) + &
          &           ZDBDS(J5,14) * ZPCEL(J5,14) + &
          &           ZDBDS(J5,15) * ZPCEL(J5,15) + &
          &           ZDBDS(J5,16) * ZPCEL(J5,16)
     !
     ZDPDT = ZDBDT(J5, 1) * ZPCEL(J5, 1) + &
          &           ZDBDT(J5, 2) * ZPCEL(J5, 2) + &
          &           ZDBDT(J5, 3) * ZPCEL(J5, 3) + &
          &           ZDBDT(J5, 4) * ZPCEL(J5, 4) + &
          &           ZDBDT(J5, 5) * ZPCEL(J5, 5) + &
          &           ZDBDT(J5, 6) * ZPCEL(J5, 6) + &
          &           ZDBDT(J5, 7) * ZPCEL(J5, 7) + &
          &           ZDBDT(J5, 8) * ZPCEL(J5, 8) + &
          &           ZDBDT(J5, 9) * ZPCEL(J5, 9) + &
          &           ZDBDT(J5,10) * ZPCEL(J5,10) + &
          &           ZDBDT(J5,11) * ZPCEL(J5,11) + &
          &           ZDBDT(J5,12) * ZPCEL(J5,12) + &
          &           ZDBDT(J5,13) * ZPCEL(J5,13) + &
          &           ZDBDT(J5,14) * ZPCEL(J5,14) + &
          &           ZDBDT(J5,15) * ZPCEL(J5,15) + &
          &           ZDBDT(J5,16) * ZPCEL(J5,16)
     !
     ZCOST  = COS(ZTETA(J5,1))
     ZSINT  = SIN(ZTETA(J5,1))
     !
     ZRHO  = ZBND(J5,1)
     !
     ZR    = ZRHO * ZCOST + R0
     ZZ    = ZRHO * ZSINT + RZ0
     !
     ZR1(J5)= ZR - RMAG
     ZZ1(J5)= ZZ - RZMAG
     !
     ZBND2  = ZBND(J5,1)**2
     !
     ZDSDR = (ZDRSDT * ZSINT + ZBND(J5,1) * ZCOST) / ZBND2
     ZDTDR = - ZSINT / ZRHO
     ZDSDZ = (ZBND(J5,1) * ZSINT - ZDRSDT * ZCOST) / ZBND2
     ZDTDZ = ZCOST / ZRHO
     !
     ZDPDR  = ZDPDS * ZDSDR + ZDPDT * ZDTDR
     ZDPDZ  = ZDPDS * ZDSDZ + ZDPDT * ZDTDZ
     !
     !  COMPUTE FIRST DERIVATIVES OF THETA-TILD WITH
     !  RESPECT TO R AND Z
     !
     ZDSUM  = ZR1(J5)**2 + ZZ1(J5)**2
     !
     ZDTTDR  = - ZZ1(J5) / ZDSUM
     ZDTTDZ  =   ZR1(J5) / ZDSUM
     !
     ! XTOR Jacobian at plasma edge
     !
     ZJAC  = ZR / (ZDPDR * ZDTTDZ - ZDPDZ * ZDTTDR)
     !
     ! DR/DTheta-Tild and  DZ/DTheta-Tild at plasma edge at constant psi
     ZDR1DTT(J5) = - ZJAC * ZDPDZ / ZR
     ZDZ1DTT(J5) = ZJAC * ZDPDR / ZR
     !
  END DO
  !
  ! Compute and store vacuum mesh, covariant vacuum metrics and sqrt(vacuum g)
  !
  WRITE(NXTOR) 2*NV+1
  WRITE(NXTOR) REXT
  !
  DO J7=1,2*NV+1
     DO J6=1,NTT
        !
        ZRV(J6) = RMAG +ZSV(J7)*ZR1(J6)
        ZZV(J6) = RZMAG+ZSV(J7)*ZZ1(J6)
        !
        ZGSSVL(J6)  = (ZR1(J6)**2 + ZZ1(J6)**2)
        ZGTTVL(J6)  = ZSV(J7)**2*(ZDR1DTT(J6)**2 + ZDZ1DTT(J6)**2)
        ZGPPVL(J6)  = ZRV(J6)**2
        ZGSTVL(J6)  = ZSV(J7)*(ZR1(J6)*ZDR1DTT(J6)+ZZ1(J6)*ZDZ1DTT(J6))
        !
        ZJACV(J6) = ZSV(J7)*ZRV(J6)*(ZR1(J6)*ZDZ1DTT(J6)-ZZ1(J6)*ZDR1DTT(J6))
        !
     ENDDO
     !
     WRITE(NXTOR) ZSV(J7)
     WRITE(NXTOR) (ZRV(L),L=1,NTT)
     WRITE(NXTOR) (ZZV(L),L=1,NTT)
     WRITE(NXTOR) (ZJACV(L),L=1,NTT)
     WRITE(NXTOR) (ZGSSVL(L),L=1,NTT)
     WRITE(NXTOR) (ZGTTVL(L),L=1,NTT)
     WRITE(NXTOR) (ZGPPVL(L),L=1,NTT)
     WRITE(NXTOR) (ZGSTVL(L),L=1,NTT)
     !
  ENDDO
  !
  DEALLOCATE(IC,IS0,IT0,ZBND,ZDBDS,ZDBDT,ZDR1DTT,ZDZ1DTT,ZJACV, &
       &   ZGSSVL,ZGSTVL,ZGTTVL,ZGPPVL,ZPCEL,ZR1,ZRV,ZS,ZS1, &
       &   ZS2,ZSV,ZTETA,ZT,ZT1,ZT2,ZZ1,ZZV)
  !
  RETURN
END SUBROUTINE VACUUMXT
