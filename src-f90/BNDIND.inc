! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*COMDECK BNDIND
! ----------------------------------------------------------------------
! --     STATEMENT FUNCTION FOR BAND MATRIX ADRESSES                  --
! --                         11.07.88            HL        CRPP       --
! --                                                                  --
! --     INDCOL AND INDROW ARE COMPUTING BAND MATRIX ADRESSES STARTING--
! --     FROM SQUARE MATRIX ADRESSES. IF THE ADRESS IS LOWER-DIAGONAL,--
! --     THESE FUNCTIONS FIND OUT AUTOMATICALY THE SYMETRIC UPPER-    --
! --     DIAGONAL ADRESS.                                             --
! --                                                                  --
! ----------------------------------------------------------------------
!
         INTEGER   INDCOL, INDROW, ICOLB, IROWB
!
         INDCOL(ICOLB,IROWB) = SIGN(1,ICOLB - IROWB) * (ICOLB - IROWB) + 1
         INDROW(ICOLB,IROWB) = ((IROWB + ICOLB) + (IROWB - ICOLB) * &
     &                        SIGN(1,ICOLB - IROWB)) / 2
!
