! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*DECK CRAY03
!*CALL PROCESS
FUNCTION ISAMIN(N,PV,NX)
  !        ------------------------
  !
  !  FIND ELEMENT WITH MINIMUM ABSOLUTE VALUE IN REAL ARRAY PV.
  !  PV IS INCREMENTED BY NX
  !
  USE globals
  IMPLICIT NONE
  REAL(RKIND)      ::     PV
  INTEGER          ::     I
  INTEGER          ::     J
  INTEGER          ::     NM1
  INTEGER          ::     ISM
  INTEGER          ::     ISAMIN
  INTEGER          ::     NX
  INTEGER          ::     N
  DIMENSION PV(N*NX)
  !
  ISAMIN = 0
  IF (N .LE. 0) RETURN
  !
  ISM = 1
  !
  IF (N .EQ. 1) GOTO 2
  !
  NM1 = N - 1
  !
  DO J=1,NM1
     I = J * NX + 1
     IF (ABS(PV(I)) .LT. ABS(PV(ISM))) ISM = I
  END DO
2 CONTINUE
  !
  ISAMIN = ISM
  !
  RETURN
END FUNCTION ISAMIN
