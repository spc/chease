! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
subroutine load_imas(equil_in,kitmopt,kitmshot,kitmrun,kitmocc,citmtree,kflag)
  !
  use globals
  use ids_schemas                       ! module containing the equilibrium type definitions
  IMPLICIT NONE
  type(type_equilibrium),pointer      :: equil_in(:)
  character*120  :: citmtree
  integer        :: kitmopt, kitmshot, kitmrun ,kitmocc, kflag = 0
  !
  print *,'This routine should be linked to load_itm.f90 when CHEASE is compiled without libraries for ITM structure'
  print *,'It is not supposed to be called.'
  print *,'NITMOPT should always be -1 in such cases and routines load_itm and write_itm are not called'
  stop 'load_itm'

  return
end subroutine load_imas
