! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*DECK C2SM10
!*CALL PROCESS
SUBROUTINE XTVERIFYDIMS(NPMAX)
  !        ##############################################
  !
  !                                        AUTHORS:
  !                                        H. LUTJENS,  CRPP-EPFL
  !                                        A. BONDESON, CRPP-EPFL
  !**********************************************************************
  !                                                                     *
  ! C2SM10 READS and VERIFIES ARRAY compatibility of array dimensions   *
  !        provided by preprof for xtor                                 *
  !                                                                     *
  !**********************************************************************
  !
  USE globals
  IMPLICIT NONE
  !
  integer          ::     ifile, ierr
  integer          ::     l
  integer          ::     n1
  integer          ::     n2
  integer          ::     n3
  integer          ::     NPMAX
  !
  !
  !----*----*----*---*----*----*----*----*----*----*----*----*----*----*-
  !
  ifile = 44
  open(ifile, file = 'ALL_PROFILES', FORM = 'formatted', action = 'read', err=951)
  rewind ifile
  !
  read(ifile,998) n1,n2,n3
998 format(3i5)
  !
  if (n1-2/=NPMAX) then
    write(0,*) 'in xtinit: 2*(total_lmax-1) and 2*npsi differ in xtinit and program preprof'
    call ivar('2*(total_lmax-1)',n1-2)
    call ivar('niso1eff=2*npsi',npmax)
    write(eqchease_out(1)%codeparam%output_diag(1),*) ' in xtinit: 2*(total_lmax-1) and 2*npsi differ in xtinit and program preprof'
    write(eqchease_out(1)%codeparam%output_diag(2),*) '2*(total_lmax-1)= ',n1-2,' ; niso1eff=2*npsi = ',npmax
    eqchease_out(1)%codeparam%output_flag = -301
    return
  endif
  !
  npopulations=n2+n3
  !
  RETURN
  !
951 write(0,*) 'file ALL_PROFILES not available'
  write(eqchease_out(1)%codeparam%output_diag(1),*) &
    & ' in xtinit: file ALL_PROFILES not available'
  eqchease_out(1)%codeparam%output_flag = -304
  return
END SUBROUTINE XTVERIFYDIMS
