! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*DECK MR03
!*CALL PROCESS
SUBROUTINE DWY(A,U,N,NP,M,MP,NKV)
  !        ---------------------------------
  !
  !     SOLVE D*W=Y
  !
  !     VERSION 1C           13.9.74     RALF GRUBER    CRPP LAUSANNE
  !
  !  U               KV SETS OF N-VECTORS, (BOTH X AND Y)
  !  A               DIAGONAL MATRIX D
  !                    (DIAGONAL IS FIRST COMPONENT OF BAND OF LENGTH M)
  !
  !         USE globals, except_a => a
  USE prec_const
  IMPLICIT NONE
  INTEGER          ::     NKV
  INTEGER          ::     M
  REAL(RKIND)      ::     A
  REAL(RKIND)      ::     U
  INTEGER          ::     J1
  INTEGER          ::     IKD
  INTEGER          ::     NP
  INTEGER          ::     MP
  INTEGER          ::     N
  DIMENSION &
       &   A(N*MP),   U(NP)
  !
  !---------------------------------------------------------------------
  !
  IKD = 1
  !
  !      STORE VALUE
  !
  DO J1=1,N
     U(J1) = U(J1) / A(IKD)
     IKD = IKD + MP
  END DO
  !
  RETURN
END SUBROUTINE DWY
