! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
! The content of this file was copied from the 4.10b ITM-UAL installation:
!    /gw/switm/ual/4.10b.10_R2.3.5/fortraninterface/equilibrium.f90
! The content of the file was reduced to include only the subroutines needed, i.e.
! the euitm_deallocate_equilibrium.
!
! Copying and editing by Thomas Jonsson, 2020-11-19.
!
module equilibrium_euitm_module

! Declaration of the generic CPO DEALLOCATE routine
interface euITM_deallocate
   module procedure euITM_deallocate_equilibrium
end interface euITM_deallocate

 contains

!!!!!! Routines to DEALLOCATE CPOs

subroutine euITM_deallocate_equilibrium(cpo)

use euITM_schemas
implicit none
character(len=3)::ual_debug
integer :: i1,i2,i3,i4,i5,i6,i7


type(type_equilibrium),pointer :: cpo(:)
integer :: itime

call getenv('ual_debug',ual_debug) ! Debug flag

! check whether CPO is empty
if (.NOT.associated(cpo)) then
   return
endif

if (ual_debug =='yes') write(*,*) 'Deallocating equilibrium CPO'
do itime=1,size(cpo)    ! loop on time

   ! deallocate datainfo/dataprovider
   if (associated(cpo(itime)%datainfo%dataprovider)) then
        deallocate(cpo(itime)%datainfo%dataprovider)
   endif

   ! deallocate datainfo/putdate
   if (associated(cpo(itime)%datainfo%putdate)) then
        deallocate(cpo(itime)%datainfo%putdate)
   endif

   ! deallocate datainfo/source
   if (associated(cpo(itime)%datainfo%source)) then
        deallocate(cpo(itime)%datainfo%source)
   endif

   ! deallocate datainfo/comment
   if (associated(cpo(itime)%datainfo%comment)) then
        deallocate(cpo(itime)%datainfo%comment)
   endif

   ! deallocate datainfo/whatref/user
   if (associated(cpo(itime)%datainfo%whatref%user)) then
        deallocate(cpo(itime)%datainfo%whatref%user)
   endif

   ! deallocate datainfo/whatref/machine
   if (associated(cpo(itime)%datainfo%whatref%machine)) then
        deallocate(cpo(itime)%datainfo%whatref%machine)
   endif

   ! deallocate datainfo/putinfo/putmethod
   if (associated(cpo(itime)%datainfo%putinfo%putmethod)) then
        deallocate(cpo(itime)%datainfo%putinfo%putmethod)
   endif

   ! deallocate datainfo/putinfo/putaccess
   if (associated(cpo(itime)%datainfo%putinfo%putaccess)) then
        deallocate(cpo(itime)%datainfo%putinfo%putaccess)
   endif

   ! deallocate datainfo/putinfo/putlocation
   if (associated(cpo(itime)%datainfo%putinfo%putlocation)) then
        deallocate(cpo(itime)%datainfo%putinfo%putlocation)
   endif

   ! deallocate datainfo/putinfo/rights
   if (associated(cpo(itime)%datainfo%putinfo%rights)) then
        deallocate(cpo(itime)%datainfo%putinfo%rights)
   endif

   ! deallocate eqconstraint/bpol/measured
   if (associated(cpo(itime)%eqconstraint%bpol%measured)) then
        deallocate(cpo(itime)%eqconstraint%bpol%measured)
   endif

   ! deallocate eqconstraint/bpol/source
   if (associated(cpo(itime)%eqconstraint%bpol%source)) then
        deallocate(cpo(itime)%eqconstraint%bpol%source)
   endif

   ! deallocate eqconstraint/bpol/exact
   if (associated(cpo(itime)%eqconstraint%bpol%exact)) then
        deallocate(cpo(itime)%eqconstraint%bpol%exact)
   endif

   ! deallocate eqconstraint/bpol/weight
   if (associated(cpo(itime)%eqconstraint%bpol%weight)) then
        deallocate(cpo(itime)%eqconstraint%bpol%weight)
   endif

   ! deallocate eqconstraint/bpol/sigma
   if (associated(cpo(itime)%eqconstraint%bpol%sigma)) then
        deallocate(cpo(itime)%eqconstraint%bpol%sigma)
   endif

   ! deallocate eqconstraint/bpol/calculated
   if (associated(cpo(itime)%eqconstraint%bpol%calculated)) then
        deallocate(cpo(itime)%eqconstraint%bpol%calculated)
   endif

   ! deallocate eqconstraint/bpol/chi2
   if (associated(cpo(itime)%eqconstraint%bpol%chi2)) then
        deallocate(cpo(itime)%eqconstraint%bpol%chi2)
   endif

   ! deallocate eqconstraint/bvac_r/source
   if (associated(cpo(itime)%eqconstraint%bvac_r%source)) then
        deallocate(cpo(itime)%eqconstraint%bvac_r%source)
   endif

   ! deallocate eqconstraint/diamagflux/source
   if (associated(cpo(itime)%eqconstraint%diamagflux%source)) then
        deallocate(cpo(itime)%eqconstraint%diamagflux%source)
   endif

   ! deallocate eqconstraint/faraday/measured
   if (associated(cpo(itime)%eqconstraint%faraday%measured)) then
        deallocate(cpo(itime)%eqconstraint%faraday%measured)
   endif

   ! deallocate eqconstraint/faraday/source
   if (associated(cpo(itime)%eqconstraint%faraday%source)) then
        deallocate(cpo(itime)%eqconstraint%faraday%source)
   endif

   ! deallocate eqconstraint/faraday/exact
   if (associated(cpo(itime)%eqconstraint%faraday%exact)) then
        deallocate(cpo(itime)%eqconstraint%faraday%exact)
   endif

   ! deallocate eqconstraint/faraday/weight
   if (associated(cpo(itime)%eqconstraint%faraday%weight)) then
        deallocate(cpo(itime)%eqconstraint%faraday%weight)
   endif

   ! deallocate eqconstraint/faraday/sigma
   if (associated(cpo(itime)%eqconstraint%faraday%sigma)) then
        deallocate(cpo(itime)%eqconstraint%faraday%sigma)
   endif

   ! deallocate eqconstraint/faraday/calculated
   if (associated(cpo(itime)%eqconstraint%faraday%calculated)) then
        deallocate(cpo(itime)%eqconstraint%faraday%calculated)
   endif

   ! deallocate eqconstraint/faraday/chi2
   if (associated(cpo(itime)%eqconstraint%faraday%chi2)) then
        deallocate(cpo(itime)%eqconstraint%faraday%chi2)
   endif

   ! deallocate eqconstraint/flux/measured
   if (associated(cpo(itime)%eqconstraint%flux%measured)) then
        deallocate(cpo(itime)%eqconstraint%flux%measured)
   endif

   ! deallocate eqconstraint/flux/source
   if (associated(cpo(itime)%eqconstraint%flux%source)) then
        deallocate(cpo(itime)%eqconstraint%flux%source)
   endif

   ! deallocate eqconstraint/flux/exact
   if (associated(cpo(itime)%eqconstraint%flux%exact)) then
        deallocate(cpo(itime)%eqconstraint%flux%exact)
   endif

   ! deallocate eqconstraint/flux/weight
   if (associated(cpo(itime)%eqconstraint%flux%weight)) then
        deallocate(cpo(itime)%eqconstraint%flux%weight)
   endif

   ! deallocate eqconstraint/flux/sigma
   if (associated(cpo(itime)%eqconstraint%flux%sigma)) then
        deallocate(cpo(itime)%eqconstraint%flux%sigma)
   endif

   ! deallocate eqconstraint/flux/calculated
   if (associated(cpo(itime)%eqconstraint%flux%calculated)) then
        deallocate(cpo(itime)%eqconstraint%flux%calculated)
   endif

   ! deallocate eqconstraint/flux/chi2
   if (associated(cpo(itime)%eqconstraint%flux%chi2)) then
        deallocate(cpo(itime)%eqconstraint%flux%chi2)
   endif

   ! deallocate eqconstraint/i_plasma/source
   if (associated(cpo(itime)%eqconstraint%i_plasma%source)) then
        deallocate(cpo(itime)%eqconstraint%i_plasma%source)
   endif

   ! deallocate eqconstraint/isoflux/position/r
   if (associated(cpo(itime)%eqconstraint%isoflux%position%r)) then
        deallocate(cpo(itime)%eqconstraint%isoflux%position%r)
   endif

   ! deallocate eqconstraint/isoflux/position/z
   if (associated(cpo(itime)%eqconstraint%isoflux%position%z)) then
        deallocate(cpo(itime)%eqconstraint%isoflux%position%z)
   endif

   ! deallocate eqconstraint/isoflux/source
   if (associated(cpo(itime)%eqconstraint%isoflux%source)) then
        deallocate(cpo(itime)%eqconstraint%isoflux%source)
   endif

   ! deallocate eqconstraint/isoflux/weight
   if (associated(cpo(itime)%eqconstraint%isoflux%weight)) then
        deallocate(cpo(itime)%eqconstraint%isoflux%weight)
   endif

   ! deallocate eqconstraint/isoflux/sigma
   if (associated(cpo(itime)%eqconstraint%isoflux%sigma)) then
        deallocate(cpo(itime)%eqconstraint%isoflux%sigma)
   endif

   ! deallocate eqconstraint/isoflux/calculated
   if (associated(cpo(itime)%eqconstraint%isoflux%calculated)) then
        deallocate(cpo(itime)%eqconstraint%isoflux%calculated)
   endif

   ! deallocate eqconstraint/isoflux/chi2
   if (associated(cpo(itime)%eqconstraint%isoflux%chi2)) then
        deallocate(cpo(itime)%eqconstraint%isoflux%chi2)
   endif

   ! deallocate eqconstraint/jsurf/measured
   if (associated(cpo(itime)%eqconstraint%jsurf%measured)) then
        deallocate(cpo(itime)%eqconstraint%jsurf%measured)
   endif

   ! deallocate eqconstraint/jsurf/source
   if (associated(cpo(itime)%eqconstraint%jsurf%source)) then
        deallocate(cpo(itime)%eqconstraint%jsurf%source)
   endif

   ! deallocate eqconstraint/jsurf/exact
   if (associated(cpo(itime)%eqconstraint%jsurf%exact)) then
        deallocate(cpo(itime)%eqconstraint%jsurf%exact)
   endif

   ! deallocate eqconstraint/jsurf/weight
   if (associated(cpo(itime)%eqconstraint%jsurf%weight)) then
        deallocate(cpo(itime)%eqconstraint%jsurf%weight)
   endif

   ! deallocate eqconstraint/jsurf/sigma
   if (associated(cpo(itime)%eqconstraint%jsurf%sigma)) then
        deallocate(cpo(itime)%eqconstraint%jsurf%sigma)
   endif

   ! deallocate eqconstraint/jsurf/calculated
   if (associated(cpo(itime)%eqconstraint%jsurf%calculated)) then
        deallocate(cpo(itime)%eqconstraint%jsurf%calculated)
   endif

   ! deallocate eqconstraint/jsurf/chi2
   if (associated(cpo(itime)%eqconstraint%jsurf%chi2)) then
        deallocate(cpo(itime)%eqconstraint%jsurf%chi2)
   endif

   ! deallocate eqconstraint/magnet_iron/mr/measured
   if (associated(cpo(itime)%eqconstraint%magnet_iron%mr%measured)) then
        deallocate(cpo(itime)%eqconstraint%magnet_iron%mr%measured)
   endif

   ! deallocate eqconstraint/magnet_iron/mr/source
   if (associated(cpo(itime)%eqconstraint%magnet_iron%mr%source)) then
        deallocate(cpo(itime)%eqconstraint%magnet_iron%mr%source)
   endif

   ! deallocate eqconstraint/magnet_iron/mr/exact
   if (associated(cpo(itime)%eqconstraint%magnet_iron%mr%exact)) then
        deallocate(cpo(itime)%eqconstraint%magnet_iron%mr%exact)
   endif

   ! deallocate eqconstraint/magnet_iron/mr/weight
   if (associated(cpo(itime)%eqconstraint%magnet_iron%mr%weight)) then
        deallocate(cpo(itime)%eqconstraint%magnet_iron%mr%weight)
   endif

   ! deallocate eqconstraint/magnet_iron/mr/sigma
   if (associated(cpo(itime)%eqconstraint%magnet_iron%mr%sigma)) then
        deallocate(cpo(itime)%eqconstraint%magnet_iron%mr%sigma)
   endif

   ! deallocate eqconstraint/magnet_iron/mr/calculated
   if (associated(cpo(itime)%eqconstraint%magnet_iron%mr%calculated)) then
        deallocate(cpo(itime)%eqconstraint%magnet_iron%mr%calculated)
   endif

   ! deallocate eqconstraint/magnet_iron/mr/chi2
   if (associated(cpo(itime)%eqconstraint%magnet_iron%mr%chi2)) then
        deallocate(cpo(itime)%eqconstraint%magnet_iron%mr%chi2)
   endif

   ! deallocate eqconstraint/magnet_iron/mz/measured
   if (associated(cpo(itime)%eqconstraint%magnet_iron%mz%measured)) then
        deallocate(cpo(itime)%eqconstraint%magnet_iron%mz%measured)
   endif

   ! deallocate eqconstraint/magnet_iron/mz/source
   if (associated(cpo(itime)%eqconstraint%magnet_iron%mz%source)) then
        deallocate(cpo(itime)%eqconstraint%magnet_iron%mz%source)
   endif

   ! deallocate eqconstraint/magnet_iron/mz/exact
   if (associated(cpo(itime)%eqconstraint%magnet_iron%mz%exact)) then
        deallocate(cpo(itime)%eqconstraint%magnet_iron%mz%exact)
   endif

   ! deallocate eqconstraint/magnet_iron/mz/weight
   if (associated(cpo(itime)%eqconstraint%magnet_iron%mz%weight)) then
        deallocate(cpo(itime)%eqconstraint%magnet_iron%mz%weight)
   endif

   ! deallocate eqconstraint/magnet_iron/mz/sigma
   if (associated(cpo(itime)%eqconstraint%magnet_iron%mz%sigma)) then
        deallocate(cpo(itime)%eqconstraint%magnet_iron%mz%sigma)
   endif

   ! deallocate eqconstraint/magnet_iron/mz/calculated
   if (associated(cpo(itime)%eqconstraint%magnet_iron%mz%calculated)) then
        deallocate(cpo(itime)%eqconstraint%magnet_iron%mz%calculated)
   endif

   ! deallocate eqconstraint/magnet_iron/mz/chi2
   if (associated(cpo(itime)%eqconstraint%magnet_iron%mz%chi2)) then
        deallocate(cpo(itime)%eqconstraint%magnet_iron%mz%chi2)
   endif

   ! deallocate eqconstraint/mse/measured
   if (associated(cpo(itime)%eqconstraint%mse%measured)) then
        deallocate(cpo(itime)%eqconstraint%mse%measured)
   endif

   ! deallocate eqconstraint/mse/source
   if (associated(cpo(itime)%eqconstraint%mse%source)) then
        deallocate(cpo(itime)%eqconstraint%mse%source)
   endif

   ! deallocate eqconstraint/mse/exact
   if (associated(cpo(itime)%eqconstraint%mse%exact)) then
        deallocate(cpo(itime)%eqconstraint%mse%exact)
   endif

   ! deallocate eqconstraint/mse/weight
   if (associated(cpo(itime)%eqconstraint%mse%weight)) then
        deallocate(cpo(itime)%eqconstraint%mse%weight)
   endif

   ! deallocate eqconstraint/mse/sigma
   if (associated(cpo(itime)%eqconstraint%mse%sigma)) then
        deallocate(cpo(itime)%eqconstraint%mse%sigma)
   endif

   ! deallocate eqconstraint/mse/calculated
   if (associated(cpo(itime)%eqconstraint%mse%calculated)) then
        deallocate(cpo(itime)%eqconstraint%mse%calculated)
   endif

   ! deallocate eqconstraint/mse/chi2
   if (associated(cpo(itime)%eqconstraint%mse%chi2)) then
        deallocate(cpo(itime)%eqconstraint%mse%chi2)
   endif

   ! deallocate eqconstraint/ne/measured
   if (associated(cpo(itime)%eqconstraint%ne%measured)) then
        deallocate(cpo(itime)%eqconstraint%ne%measured)
   endif

   ! deallocate eqconstraint/ne/source
   if (associated(cpo(itime)%eqconstraint%ne%source)) then
        deallocate(cpo(itime)%eqconstraint%ne%source)
   endif

   ! deallocate eqconstraint/ne/exact
   if (associated(cpo(itime)%eqconstraint%ne%exact)) then
        deallocate(cpo(itime)%eqconstraint%ne%exact)
   endif

   ! deallocate eqconstraint/ne/weight
   if (associated(cpo(itime)%eqconstraint%ne%weight)) then
        deallocate(cpo(itime)%eqconstraint%ne%weight)
   endif

   ! deallocate eqconstraint/ne/sigma
   if (associated(cpo(itime)%eqconstraint%ne%sigma)) then
        deallocate(cpo(itime)%eqconstraint%ne%sigma)
   endif

   ! deallocate eqconstraint/ne/calculated
   if (associated(cpo(itime)%eqconstraint%ne%calculated)) then
        deallocate(cpo(itime)%eqconstraint%ne%calculated)
   endif

   ! deallocate eqconstraint/ne/chi2
   if (associated(cpo(itime)%eqconstraint%ne%chi2)) then
        deallocate(cpo(itime)%eqconstraint%ne%chi2)
   endif

   ! deallocate eqconstraint/pfcurrent/measured
   if (associated(cpo(itime)%eqconstraint%pfcurrent%measured)) then
        deallocate(cpo(itime)%eqconstraint%pfcurrent%measured)
   endif

   ! deallocate eqconstraint/pfcurrent/source
   if (associated(cpo(itime)%eqconstraint%pfcurrent%source)) then
        deallocate(cpo(itime)%eqconstraint%pfcurrent%source)
   endif

   ! deallocate eqconstraint/pfcurrent/exact
   if (associated(cpo(itime)%eqconstraint%pfcurrent%exact)) then
        deallocate(cpo(itime)%eqconstraint%pfcurrent%exact)
   endif

   ! deallocate eqconstraint/pfcurrent/weight
   if (associated(cpo(itime)%eqconstraint%pfcurrent%weight)) then
        deallocate(cpo(itime)%eqconstraint%pfcurrent%weight)
   endif

   ! deallocate eqconstraint/pfcurrent/sigma
   if (associated(cpo(itime)%eqconstraint%pfcurrent%sigma)) then
        deallocate(cpo(itime)%eqconstraint%pfcurrent%sigma)
   endif

   ! deallocate eqconstraint/pfcurrent/calculated
   if (associated(cpo(itime)%eqconstraint%pfcurrent%calculated)) then
        deallocate(cpo(itime)%eqconstraint%pfcurrent%calculated)
   endif

   ! deallocate eqconstraint/pfcurrent/chi2
   if (associated(cpo(itime)%eqconstraint%pfcurrent%chi2)) then
        deallocate(cpo(itime)%eqconstraint%pfcurrent%chi2)
   endif

   ! deallocate eqconstraint/pressure/measured
   if (associated(cpo(itime)%eqconstraint%pressure%measured)) then
        deallocate(cpo(itime)%eqconstraint%pressure%measured)
   endif

   ! deallocate eqconstraint/pressure/source
   if (associated(cpo(itime)%eqconstraint%pressure%source)) then
        deallocate(cpo(itime)%eqconstraint%pressure%source)
   endif

   ! deallocate eqconstraint/pressure/exact
   if (associated(cpo(itime)%eqconstraint%pressure%exact)) then
        deallocate(cpo(itime)%eqconstraint%pressure%exact)
   endif

   ! deallocate eqconstraint/pressure/weight
   if (associated(cpo(itime)%eqconstraint%pressure%weight)) then
        deallocate(cpo(itime)%eqconstraint%pressure%weight)
   endif

   ! deallocate eqconstraint/pressure/sigma
   if (associated(cpo(itime)%eqconstraint%pressure%sigma)) then
        deallocate(cpo(itime)%eqconstraint%pressure%sigma)
   endif

   ! deallocate eqconstraint/pressure/calculated
   if (associated(cpo(itime)%eqconstraint%pressure%calculated)) then
        deallocate(cpo(itime)%eqconstraint%pressure%calculated)
   endif

   ! deallocate eqconstraint/pressure/chi2
   if (associated(cpo(itime)%eqconstraint%pressure%chi2)) then
        deallocate(cpo(itime)%eqconstraint%pressure%chi2)
   endif

   ! deallocate eqconstraint/q/qvalue
   if (associated(cpo(itime)%eqconstraint%q%qvalue)) then
        deallocate(cpo(itime)%eqconstraint%q%qvalue)
   endif

   ! deallocate eqconstraint/q/position/r
   if (associated(cpo(itime)%eqconstraint%q%position%r)) then
        deallocate(cpo(itime)%eqconstraint%q%position%r)
   endif

   ! deallocate eqconstraint/q/position/z
   if (associated(cpo(itime)%eqconstraint%q%position%z)) then
        deallocate(cpo(itime)%eqconstraint%q%position%z)
   endif

   ! deallocate eqconstraint/q/source
   if (associated(cpo(itime)%eqconstraint%q%source)) then
        deallocate(cpo(itime)%eqconstraint%q%source)
   endif

   ! deallocate eqconstraint/q/weight
   if (associated(cpo(itime)%eqconstraint%q%weight)) then
        deallocate(cpo(itime)%eqconstraint%q%weight)
   endif

   ! deallocate eqconstraint/q/sigma
   if (associated(cpo(itime)%eqconstraint%q%sigma)) then
        deallocate(cpo(itime)%eqconstraint%q%sigma)
   endif

   ! deallocate eqconstraint/q/calculated
   if (associated(cpo(itime)%eqconstraint%q%calculated)) then
        deallocate(cpo(itime)%eqconstraint%q%calculated)
   endif

   ! deallocate eqconstraint/q/chi2
   if (associated(cpo(itime)%eqconstraint%q%chi2)) then
        deallocate(cpo(itime)%eqconstraint%q%chi2)
   endif

   ! deallocate eqconstraint/xpts/position/r
   if (associated(cpo(itime)%eqconstraint%xpts%position%r)) then
        deallocate(cpo(itime)%eqconstraint%xpts%position%r)
   endif

   ! deallocate eqconstraint/xpts/position/z
   if (associated(cpo(itime)%eqconstraint%xpts%position%z)) then
        deallocate(cpo(itime)%eqconstraint%xpts%position%z)
   endif

   ! deallocate eqconstraint/xpts/source
   if (associated(cpo(itime)%eqconstraint%xpts%source)) then
        deallocate(cpo(itime)%eqconstraint%xpts%source)
   endif

   ! deallocate eqconstraint/xpts/weight
   if (associated(cpo(itime)%eqconstraint%xpts%weight)) then
        deallocate(cpo(itime)%eqconstraint%xpts%weight)
   endif

   ! deallocate eqconstraint/xpts/sigma
   if (associated(cpo(itime)%eqconstraint%xpts%sigma)) then
        deallocate(cpo(itime)%eqconstraint%xpts%sigma)
   endif

   ! deallocate eqconstraint/xpts/calculated
   if (associated(cpo(itime)%eqconstraint%xpts%calculated)) then
        deallocate(cpo(itime)%eqconstraint%xpts%calculated)
   endif

   ! deallocate eqconstraint/xpts/chi2
   if (associated(cpo(itime)%eqconstraint%xpts%chi2)) then
        deallocate(cpo(itime)%eqconstraint%xpts%chi2)
   endif

   ! deallocate eqgeometry/source
   if (associated(cpo(itime)%eqgeometry%source)) then
        deallocate(cpo(itime)%eqgeometry%source)
   endif

    ! deallocate eqgeometry/boundary
    if (associated(cpo(itime)%eqgeometry%boundary)) then
        do i1 = 1,size(cpo(itime)%eqgeometry%boundary)

   ! deallocate eqgeometry/boundary/r
   if (associated(cpo(itime)%eqgeometry%boundary(i1)%r)) then
        deallocate(cpo(itime)%eqgeometry%boundary(i1)%r)
   endif

   ! deallocate eqgeometry/boundary/z
   if (associated(cpo(itime)%eqgeometry%boundary(i1)%z)) then
        deallocate(cpo(itime)%eqgeometry%boundary(i1)%z)
   endif

        enddo
        deallocate(cpo(itime)%eqgeometry%boundary)
    endif

    ! deallocate eqgeometry/xpts
    if (associated(cpo(itime)%eqgeometry%xpts)) then
        do i1 = 1,size(cpo(itime)%eqgeometry%xpts)

   ! deallocate eqgeometry/xpts/r
   if (associated(cpo(itime)%eqgeometry%xpts(i1)%r)) then
        deallocate(cpo(itime)%eqgeometry%xpts(i1)%r)
   endif

   ! deallocate eqgeometry/xpts/z
   if (associated(cpo(itime)%eqgeometry%xpts(i1)%z)) then
        deallocate(cpo(itime)%eqgeometry%xpts(i1)%z)
   endif

        enddo
        deallocate(cpo(itime)%eqgeometry%xpts)
    endif

   ! deallocate flush/datainfo/dataprovider
   if (associated(cpo(itime)%flush%datainfo%dataprovider)) then
        deallocate(cpo(itime)%flush%datainfo%dataprovider)
   endif

   ! deallocate flush/datainfo/putdate
   if (associated(cpo(itime)%flush%datainfo%putdate)) then
        deallocate(cpo(itime)%flush%datainfo%putdate)
   endif

   ! deallocate flush/datainfo/source
   if (associated(cpo(itime)%flush%datainfo%source)) then
        deallocate(cpo(itime)%flush%datainfo%source)
   endif

   ! deallocate flush/datainfo/comment
   if (associated(cpo(itime)%flush%datainfo%comment)) then
        deallocate(cpo(itime)%flush%datainfo%comment)
   endif

   ! deallocate flush/datainfo/whatref/user
   if (associated(cpo(itime)%flush%datainfo%whatref%user)) then
        deallocate(cpo(itime)%flush%datainfo%whatref%user)
   endif

   ! deallocate flush/datainfo/whatref/machine
   if (associated(cpo(itime)%flush%datainfo%whatref%machine)) then
        deallocate(cpo(itime)%flush%datainfo%whatref%machine)
   endif

   ! deallocate flush/datainfo/putinfo/putmethod
   if (associated(cpo(itime)%flush%datainfo%putinfo%putmethod)) then
        deallocate(cpo(itime)%flush%datainfo%putinfo%putmethod)
   endif

   ! deallocate flush/datainfo/putinfo/putaccess
   if (associated(cpo(itime)%flush%datainfo%putinfo%putaccess)) then
        deallocate(cpo(itime)%flush%datainfo%putinfo%putaccess)
   endif

   ! deallocate flush/datainfo/putinfo/putlocation
   if (associated(cpo(itime)%flush%datainfo%putinfo%putlocation)) then
        deallocate(cpo(itime)%flush%datainfo%putinfo%putlocation)
   endif

   ! deallocate flush/datainfo/putinfo/rights
   if (associated(cpo(itime)%flush%datainfo%putinfo%rights)) then
        deallocate(cpo(itime)%flush%datainfo%putinfo%rights)
   endif

   ! deallocate flush/position/r
   if (associated(cpo(itime)%flush%position%r)) then
        deallocate(cpo(itime)%flush%position%r)
   endif

   ! deallocate flush/position/z
   if (associated(cpo(itime)%flush%position%z)) then
        deallocate(cpo(itime)%flush%position%z)
   endif

   ! deallocate flush/coef
   if (associated(cpo(itime)%flush%coef)) then
        deallocate(cpo(itime)%flush%coef)
   endif

   ! deallocate flush/codeparam/codename
   if (associated(cpo(itime)%flush%codeparam%codename)) then
        deallocate(cpo(itime)%flush%codeparam%codename)
   endif

   ! deallocate flush/codeparam/codeversion
   if (associated(cpo(itime)%flush%codeparam%codeversion)) then
        deallocate(cpo(itime)%flush%codeparam%codeversion)
   endif

   ! deallocate flush/codeparam/parameters
   if (associated(cpo(itime)%flush%codeparam%parameters)) then
        deallocate(cpo(itime)%flush%codeparam%parameters)
   endif

   ! deallocate flush/codeparam/output_diag
   if (associated(cpo(itime)%flush%codeparam%output_diag)) then
        deallocate(cpo(itime)%flush%codeparam%output_diag)
   endif

   ! deallocate profiles_1d/psi
   if (associated(cpo(itime)%profiles_1d%psi)) then
        deallocate(cpo(itime)%profiles_1d%psi)
   endif

   ! deallocate profiles_1d/phi
   if (associated(cpo(itime)%profiles_1d%phi)) then
        deallocate(cpo(itime)%profiles_1d%phi)
   endif

   ! deallocate profiles_1d/pressure
   if (associated(cpo(itime)%profiles_1d%pressure)) then
        deallocate(cpo(itime)%profiles_1d%pressure)
   endif

   ! deallocate profiles_1d/F_dia
   if (associated(cpo(itime)%profiles_1d%F_dia)) then
        deallocate(cpo(itime)%profiles_1d%F_dia)
   endif

   ! deallocate profiles_1d/pprime
   if (associated(cpo(itime)%profiles_1d%pprime)) then
        deallocate(cpo(itime)%profiles_1d%pprime)
   endif

   ! deallocate profiles_1d/ffprime
   if (associated(cpo(itime)%profiles_1d%ffprime)) then
        deallocate(cpo(itime)%profiles_1d%ffprime)
   endif

   ! deallocate profiles_1d/jphi
   if (associated(cpo(itime)%profiles_1d%jphi)) then
        deallocate(cpo(itime)%profiles_1d%jphi)
   endif

   ! deallocate profiles_1d/jparallel
   if (associated(cpo(itime)%profiles_1d%jparallel)) then
        deallocate(cpo(itime)%profiles_1d%jparallel)
   endif

   ! deallocate profiles_1d/q
   if (associated(cpo(itime)%profiles_1d%q)) then
        deallocate(cpo(itime)%profiles_1d%q)
   endif

   ! deallocate profiles_1d/shear
   if (associated(cpo(itime)%profiles_1d%shear)) then
        deallocate(cpo(itime)%profiles_1d%shear)
   endif

   ! deallocate profiles_1d/r_inboard
   if (associated(cpo(itime)%profiles_1d%r_inboard)) then
        deallocate(cpo(itime)%profiles_1d%r_inboard)
   endif

   ! deallocate profiles_1d/r_outboard
   if (associated(cpo(itime)%profiles_1d%r_outboard)) then
        deallocate(cpo(itime)%profiles_1d%r_outboard)
   endif

   ! deallocate profiles_1d/rho_tor
   if (associated(cpo(itime)%profiles_1d%rho_tor)) then
        deallocate(cpo(itime)%profiles_1d%rho_tor)
   endif

   ! deallocate profiles_1d/dpsidrho_tor
   if (associated(cpo(itime)%profiles_1d%dpsidrho_tor)) then
        deallocate(cpo(itime)%profiles_1d%dpsidrho_tor)
   endif

   ! deallocate profiles_1d/rho_vol
   if (associated(cpo(itime)%profiles_1d%rho_vol)) then
        deallocate(cpo(itime)%profiles_1d%rho_vol)
   endif

   ! deallocate profiles_1d/beta_pol
   if (associated(cpo(itime)%profiles_1d%beta_pol)) then
        deallocate(cpo(itime)%profiles_1d%beta_pol)
   endif

   ! deallocate profiles_1d/li
   if (associated(cpo(itime)%profiles_1d%li)) then
        deallocate(cpo(itime)%profiles_1d%li)
   endif

   ! deallocate profiles_1d/elongation
   if (associated(cpo(itime)%profiles_1d%elongation)) then
        deallocate(cpo(itime)%profiles_1d%elongation)
   endif

   ! deallocate profiles_1d/tria_upper
   if (associated(cpo(itime)%profiles_1d%tria_upper)) then
        deallocate(cpo(itime)%profiles_1d%tria_upper)
   endif

   ! deallocate profiles_1d/tria_lower
   if (associated(cpo(itime)%profiles_1d%tria_lower)) then
        deallocate(cpo(itime)%profiles_1d%tria_lower)
   endif

   ! deallocate profiles_1d/volume
   if (associated(cpo(itime)%profiles_1d%volume)) then
        deallocate(cpo(itime)%profiles_1d%volume)
   endif

   ! deallocate profiles_1d/vprime
   if (associated(cpo(itime)%profiles_1d%vprime)) then
        deallocate(cpo(itime)%profiles_1d%vprime)
   endif

   ! deallocate profiles_1d/dvdrho
   if (associated(cpo(itime)%profiles_1d%dvdrho)) then
        deallocate(cpo(itime)%profiles_1d%dvdrho)
   endif

   ! deallocate profiles_1d/area
   if (associated(cpo(itime)%profiles_1d%area)) then
        deallocate(cpo(itime)%profiles_1d%area)
   endif

   ! deallocate profiles_1d/aprime
   if (associated(cpo(itime)%profiles_1d%aprime)) then
        deallocate(cpo(itime)%profiles_1d%aprime)
   endif

   ! deallocate profiles_1d/surface
   if (associated(cpo(itime)%profiles_1d%surface)) then
        deallocate(cpo(itime)%profiles_1d%surface)
   endif

   ! deallocate profiles_1d/ftrap
   if (associated(cpo(itime)%profiles_1d%ftrap)) then
        deallocate(cpo(itime)%profiles_1d%ftrap)
   endif

   ! deallocate profiles_1d/gm1
   if (associated(cpo(itime)%profiles_1d%gm1)) then
        deallocate(cpo(itime)%profiles_1d%gm1)
   endif

   ! deallocate profiles_1d/gm2
   if (associated(cpo(itime)%profiles_1d%gm2)) then
        deallocate(cpo(itime)%profiles_1d%gm2)
   endif

   ! deallocate profiles_1d/gm3
   if (associated(cpo(itime)%profiles_1d%gm3)) then
        deallocate(cpo(itime)%profiles_1d%gm3)
   endif

   ! deallocate profiles_1d/gm4
   if (associated(cpo(itime)%profiles_1d%gm4)) then
        deallocate(cpo(itime)%profiles_1d%gm4)
   endif

   ! deallocate profiles_1d/gm5
   if (associated(cpo(itime)%profiles_1d%gm5)) then
        deallocate(cpo(itime)%profiles_1d%gm5)
   endif

   ! deallocate profiles_1d/gm6
   if (associated(cpo(itime)%profiles_1d%gm6)) then
        deallocate(cpo(itime)%profiles_1d%gm6)
   endif

   ! deallocate profiles_1d/gm7
   if (associated(cpo(itime)%profiles_1d%gm7)) then
        deallocate(cpo(itime)%profiles_1d%gm7)
   endif

   ! deallocate profiles_1d/gm8
   if (associated(cpo(itime)%profiles_1d%gm8)) then
        deallocate(cpo(itime)%profiles_1d%gm8)
   endif

   ! deallocate profiles_1d/gm9
   if (associated(cpo(itime)%profiles_1d%gm9)) then
        deallocate(cpo(itime)%profiles_1d%gm9)
   endif

   ! deallocate profiles_1d/b_av
   if (associated(cpo(itime)%profiles_1d%b_av)) then
        deallocate(cpo(itime)%profiles_1d%b_av)
   endif

   ! deallocate profiles_1d/b_min
   if (associated(cpo(itime)%profiles_1d%b_min)) then
        deallocate(cpo(itime)%profiles_1d%b_min)
   endif

   ! deallocate profiles_1d/b_max
   if (associated(cpo(itime)%profiles_1d%b_max)) then
        deallocate(cpo(itime)%profiles_1d%b_max)
   endif

   ! deallocate profiles_1d/omega
   if (associated(cpo(itime)%profiles_1d%omega)) then
        deallocate(cpo(itime)%profiles_1d%omega)
   endif

   ! deallocate profiles_1d/omegaprime
   if (associated(cpo(itime)%profiles_1d%omegaprime)) then
        deallocate(cpo(itime)%profiles_1d%omegaprime)
   endif

   ! deallocate profiles_1d/mach_a
   if (associated(cpo(itime)%profiles_1d%mach_a)) then
        deallocate(cpo(itime)%profiles_1d%mach_a)
   endif

   ! deallocate profiles_1d/phi_flow
   if (associated(cpo(itime)%profiles_1d%phi_flow)) then
        deallocate(cpo(itime)%profiles_1d%phi_flow)
   endif

   ! deallocate profiles_1d/s_flow
   if (associated(cpo(itime)%profiles_1d%s_flow)) then
        deallocate(cpo(itime)%profiles_1d%s_flow)
   endif

   ! deallocate profiles_1d/h_flow
   if (associated(cpo(itime)%profiles_1d%h_flow)) then
        deallocate(cpo(itime)%profiles_1d%h_flow)
   endif

   ! deallocate profiles_1d/rho_mass
   if (associated(cpo(itime)%profiles_1d%rho_mass)) then
        deallocate(cpo(itime)%profiles_1d%rho_mass)
   endif

    ! deallocate profiles_2d
    if (associated(cpo(itime)%profiles_2d)) then
        do i1 = 1,size(cpo(itime)%profiles_2d)

   ! deallocate profiles_2d/grid_type
   if (associated(cpo(itime)%profiles_2d(i1)%grid_type)) then
        deallocate(cpo(itime)%profiles_2d(i1)%grid_type)
   endif

   ! deallocate profiles_2d/grid/dim1
   if (associated(cpo(itime)%profiles_2d(i1)%grid%dim1)) then
        deallocate(cpo(itime)%profiles_2d(i1)%grid%dim1)
   endif

   ! deallocate profiles_2d/grid/dim2
   if (associated(cpo(itime)%profiles_2d(i1)%grid%dim2)) then
        deallocate(cpo(itime)%profiles_2d(i1)%grid%dim2)
   endif

   ! deallocate profiles_2d/grid/connect
   if (associated(cpo(itime)%profiles_2d(i1)%grid%connect)) then
        deallocate(cpo(itime)%profiles_2d(i1)%grid%connect)
   endif

   ! deallocate profiles_2d/r
   if (associated(cpo(itime)%profiles_2d(i1)%r)) then
        deallocate(cpo(itime)%profiles_2d(i1)%r)
   endif

   ! deallocate profiles_2d/z
   if (associated(cpo(itime)%profiles_2d(i1)%z)) then
        deallocate(cpo(itime)%profiles_2d(i1)%z)
   endif

   ! deallocate profiles_2d/psi
   if (associated(cpo(itime)%profiles_2d(i1)%psi)) then
        deallocate(cpo(itime)%profiles_2d(i1)%psi)
   endif

   ! deallocate profiles_2d/theta
   if (associated(cpo(itime)%profiles_2d(i1)%theta)) then
        deallocate(cpo(itime)%profiles_2d(i1)%theta)
   endif

   ! deallocate profiles_2d/phi
   if (associated(cpo(itime)%profiles_2d(i1)%phi)) then
        deallocate(cpo(itime)%profiles_2d(i1)%phi)
   endif

   ! deallocate profiles_2d/jphi
   if (associated(cpo(itime)%profiles_2d(i1)%jphi)) then
        deallocate(cpo(itime)%profiles_2d(i1)%jphi)
   endif

   ! deallocate profiles_2d/jpar
   if (associated(cpo(itime)%profiles_2d(i1)%jpar)) then
        deallocate(cpo(itime)%profiles_2d(i1)%jpar)
   endif

   ! deallocate profiles_2d/br
   if (associated(cpo(itime)%profiles_2d(i1)%br)) then
        deallocate(cpo(itime)%profiles_2d(i1)%br)
   endif

   ! deallocate profiles_2d/bz
   if (associated(cpo(itime)%profiles_2d(i1)%bz)) then
        deallocate(cpo(itime)%profiles_2d(i1)%bz)
   endif

   ! deallocate profiles_2d/bphi
   if (associated(cpo(itime)%profiles_2d(i1)%bphi)) then
        deallocate(cpo(itime)%profiles_2d(i1)%bphi)
   endif

   ! deallocate profiles_2d/vphi
   if (associated(cpo(itime)%profiles_2d(i1)%vphi)) then
        deallocate(cpo(itime)%profiles_2d(i1)%vphi)
   endif

   ! deallocate profiles_2d/vtheta
   if (associated(cpo(itime)%profiles_2d(i1)%vtheta)) then
        deallocate(cpo(itime)%profiles_2d(i1)%vtheta)
   endif

   ! deallocate profiles_2d/rho_mass
   if (associated(cpo(itime)%profiles_2d(i1)%rho_mass)) then
        deallocate(cpo(itime)%profiles_2d(i1)%rho_mass)
   endif

   ! deallocate profiles_2d/pressure
   if (associated(cpo(itime)%profiles_2d(i1)%pressure)) then
        deallocate(cpo(itime)%profiles_2d(i1)%pressure)
   endif

   ! deallocate profiles_2d/temperature
   if (associated(cpo(itime)%profiles_2d(i1)%temperature)) then
        deallocate(cpo(itime)%profiles_2d(i1)%temperature)
   endif

        enddo
        deallocate(cpo(itime)%profiles_2d)
    endif

   ! deallocate coord_sys/grid_type
   if (associated(cpo(itime)%coord_sys%grid_type)) then
        deallocate(cpo(itime)%coord_sys%grid_type)
   endif

   ! deallocate coord_sys/grid/dim1
   if (associated(cpo(itime)%coord_sys%grid%dim1)) then
        deallocate(cpo(itime)%coord_sys%grid%dim1)
   endif

   ! deallocate coord_sys/grid/dim2
   if (associated(cpo(itime)%coord_sys%grid%dim2)) then
        deallocate(cpo(itime)%coord_sys%grid%dim2)
   endif

   ! deallocate coord_sys/jacobian
   if (associated(cpo(itime)%coord_sys%jacobian)) then
        deallocate(cpo(itime)%coord_sys%jacobian)
   endif

   ! deallocate coord_sys/g_11
   if (associated(cpo(itime)%coord_sys%g_11)) then
        deallocate(cpo(itime)%coord_sys%g_11)
   endif

   ! deallocate coord_sys/g_12
   if (associated(cpo(itime)%coord_sys%g_12)) then
        deallocate(cpo(itime)%coord_sys%g_12)
   endif

   ! deallocate coord_sys/g_13
   if (associated(cpo(itime)%coord_sys%g_13)) then
        deallocate(cpo(itime)%coord_sys%g_13)
   endif

   ! deallocate coord_sys/g_22
   if (associated(cpo(itime)%coord_sys%g_22)) then
        deallocate(cpo(itime)%coord_sys%g_22)
   endif

   ! deallocate coord_sys/g_23
   if (associated(cpo(itime)%coord_sys%g_23)) then
        deallocate(cpo(itime)%coord_sys%g_23)
   endif

   ! deallocate coord_sys/g_33
   if (associated(cpo(itime)%coord_sys%g_33)) then
        deallocate(cpo(itime)%coord_sys%g_33)
   endif

   ! deallocate coord_sys/position/r
   if (associated(cpo(itime)%coord_sys%position%r)) then
        deallocate(cpo(itime)%coord_sys%position%r)
   endif

   ! deallocate coord_sys/position/z
   if (associated(cpo(itime)%coord_sys%position%z)) then
        deallocate(cpo(itime)%coord_sys%position%z)
   endif

   ! deallocate codeparam/codename
   if (associated(cpo(itime)%codeparam%codename)) then
        deallocate(cpo(itime)%codeparam%codename)
   endif

   ! deallocate codeparam/codeversion
   if (associated(cpo(itime)%codeparam%codeversion)) then
        deallocate(cpo(itime)%codeparam%codeversion)
   endif

   ! deallocate codeparam/parameters
   if (associated(cpo(itime)%codeparam%parameters)) then
        deallocate(cpo(itime)%codeparam%parameters)
   endif

   ! deallocate codeparam/output_diag
   if (associated(cpo(itime)%codeparam%output_diag)) then
        deallocate(cpo(itime)%codeparam%output_diag)
   endif

enddo

! Finally, deallocate the CPO itself
deallocate(cpo)


if (ual_debug =='yes') write(*,*) 'Deallocate an equilibrium CPO : done'
end subroutine euITM_deallocate_equilibrium
!!!!!! Routines to COPY CPOs

end module equilibrium_euitm_module
