! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*DECK U35
!*CALL PROCESS
SUBROUTINE OARRAY(NCHAN,KNAME,PA,KDIM)
  !        ---------- ------
  !
  ! U.35   SAME AS RARRAY BUT TO FILE NUMBER NCHAN
  !
  !---*----*----*----*----*----*----*----*----*----*----*----*----*----*
  !
  !
  USE globals
  IMPLICIT NONE
  INTEGER          ::     J
  REAL(RKIND)      ::     PA
  INTEGER          ::     NCHAN
  INTEGER          ::     KDIM
  CHARACTER*(*) KNAME
  DIMENSION &
       &   PA(KDIM)
  !
  WRITE (NCHAN,9900) KNAME
  WRITE (NCHAN,9901) (PA(J),J=1,KDIM)
  !
  RETURN
9900 FORMAT(1X,A)
9901 FORMAT((1X,10(1PE13.4)))
END SUBROUTINE OARRAY
