! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
MODULE checknanos_module
  !
  ! check if there are NaNs in an array or scalar
  ! return kflag=0 if no Nans, otherwise the number of NaNs found and print the message provided
  !
  implicit none
  public :: checknanos
  interface checknanos
    module procedure checknanos_def, checknanos_defxscal, checknanos_defint, checknanos_defxscalint
  end interface

contains

  SUBROUTINE checknanos_def(pxin,knin,tmessage,kflag)
    !
    ! check if there are NaNs in pxin(knin)
    ! output:
    !          kflag = 0: no Nans found
    !                > 0: nb of Nans found
    !
    ! If kflag>0, print the message tmessage and the number of Nans found
    !
    USE prec_const
    implicit none
    REAL(RKIND) :: pxin(knin)
    CHARACTER*(*) tmessage
    INTEGER :: knin, kflag
    !
    INTEGER :: i, inbnans
    !
    kflag = 0
    inbnans=0
    do i=1,knin
      if (isnanos(pxin(i))) inbnans = inbnans + 1
    end do
    if (inbnans .gt. 0) then
      write(0,*) '=========================   checknanos  ========================='
      write(0,*) tmessage
      write(0,*) 'There are ',inbnans,' NaNs found in pxin in checknanos'
      write(0,*) '================================================================='
      call flush(0)
      kflag = inbnans
      return
    end if
  end SUBROUTINE checknanos_def
  SUBROUTINE checknanos_defxscal(pxin,tmessage,kflag)
    !
    ! check if scalar pxin is NaN
    ! output:
    !          kflag = 0: no NaNs found
    !                = 1: pxin is a NaN
    !
    ! If kflag>0, print the message tmessage and the number of Nans found
    !
    USE prec_const
    implicit none
    REAL(RKIND) :: pxin
    CHARACTER*(*) tmessage
    INTEGER :: kflag
    !
    kflag = 0
    if (isnanos(pxin)) then
      write(0,*) '=========================   checknanos  ========================='
      write(0,*) tmessage
      write(0,*) 'pxin is a NaN in checknanos'
      write(0,*) '================================================================='
      call flush(0)
      kflag = 1
      return
    end if
  end SUBROUTINE checknanos_defxscal
  !
  SUBROUTINE checknanos_defint(kxin,knin,tmessage,kflag)
    !
    ! check if there are NaNs in kxin(knin) an integer array
    ! output:
    !          kflag = 0: no Nans found
    !                > 0: nb of Nans found
    !
    ! If kflag>0, print the message tmessage and the number of Nans found
    !
    USE prec_const
    implicit none
    INTEGER :: kxin(knin)
    CHARACTER*(*) tmessage
    INTEGER :: knin, kflag
    !
    INTEGER :: i, inbnans
    !
    kflag = 0
    inbnans=0
    do i=1,knin
      if (isnanosi(kxin(i))) inbnans = inbnans + 1
    end do
    if (inbnans .gt. 0) then
      write(0,*) '=========================   checknanos  ========================='
      write(0,*) tmessage
      write(0,*) 'There are ',inbnans,' NaNs found in kxin in checknanos'
      write(0,*) '================================================================='
      call flush(0)
      kflag = inbnans
      return
    end if
  end SUBROUTINE checknanos_defint
  SUBROUTINE checknanos_defxscalint(kxin,tmessage,kflag)
    !
    ! check if scalar integer kxin is NaN
    ! output:
    !          kflag = 0: no NaNs found
    !                = 1: kxin is a NaN
    !
    ! If kflag>0, print the message tmessage and the number of Nans found
    !
    USE prec_const
    implicit none
    INTEGER :: kxin
    CHARACTER*(*) tmessage
    INTEGER :: kflag
    !
    kflag = 0
    if (isnanosi(kxin)) then
      write(0,*) '=========================   checknanos  ========================='
      write(0,*) tmessage
      write(0,*) 'kxin is a NaN in checknanos'
      write(0,*) '================================================================='
      call flush(0)
      kflag = 1
      return
    end if
  end SUBROUTINE checknanos_defxscalint
  !
logical function isnanos(a)
  USE, INTRINSIC:: IEEE_ARITHMETIC, ONLY: IEEE_IS_NAN
  use prec_const
  real(rkind) a
  isnanos = IEEE_IS_NAN (a)
  return
end function isnanos

logical function isnanosi(a)
  USE, INTRINSIC:: IEEE_ARITHMETIC, ONLY: IEEE_VALUE, IEEE_QUIET_NAN
  use prec_const
  real(rkind) zdum
  integer a, inan
  !
  zdum = IEEE_VALUE(zdum , IEEE_QUIET_NAN)
  inan = transfer(zdum,inan)
  if (a .eq. inan) then
    isnanosi = .true.
  else
    isnanosi = .false.
  end if
  return
end function isnanosi

function realnanos()
  USE, INTRINSIC:: IEEE_ARITHMETIC, ONLY: IEEE_VALUE, IEEE_QUIET_NAN
  use prec_const
  real(rkind) :: realnanos
  integer :: i_defaultNans=1
  !
  if (i_defaultNans .eq. 1) then
    realnanos = IEEE_VALUE(realnanos , IEEE_QUIET_NAN)
  else
    realnanos = 0._rkind
  end if
  return
end function realnanos

function integernanos()
  ! Note it will provide the integer representation of a Nan: int(NaN) like -2147483648
  USE, INTRINSIC:: IEEE_ARITHMETIC, ONLY: IEEE_VALUE, IEEE_QUIET_NAN
  use prec_const
  real(rkind) :: x_nan
  integer :: integernanos
  integer :: i_defaultNans=1
  !
  if (i_defaultNans .eq. 1) then
    x_nan = IEEE_VALUE(x_nan , IEEE_QUIET_NAN)
    integernanos = transfer(x_nan,integernanos)
  else
    integernanos = 0
  end if
  return
end function integernanos

end MODULE checknanos_module
