! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
subroutine copy_ids_to_itm_equilibrium(equil_in_ids,equil_out_itm,index_times_eff_in,flag_status)
  !
  ! copy ids version DD3_5.3PUAL3_3.8 equilibrium structure to eu-itm version 4_10b CPO structure.
  !
  ! Note: should allow minimalist input ids, thus exit from routine only if time or time_slice not associated
  !
  ! flag_status = 0 if OK
  !             between -9 and -1: big problems, cannot use equil_in at all, exit from routine early
  !             <=-10: some quantities are not defined and thus not copied
  !
  USE globals, ONLY : nverbose, rc1p, rkind, nidsbound
  use euITM_schemas ! CPO definitions, note should be linked onto euitm_schemas_4.10b*
  use ids_schemas   ! IDS definitions, note should be linked onto ids_schemas_vDD3_5.3PUAL3_3.8.f90
  !
  IMPLICIT NONE
  !
  type(ids_equilibrium)     :: equil_in_ids ! Note equil_in_ids may be modified here (for simplicity of obsolescent features)
  type(type_equilibrium),pointer      :: equil_out_itm(:)
  integer :: index_times_eff_in(:), flag_status
  !
  CHARACTER  ZDATE*8
  integer :: i, it, nb_times_in, npsi_1d, nb_profiles_2d, ndim1_coord_sys, ndim2_coord_sys, &
    &  nb_points, nb_dim_codename, nb_dim_codeversion, nb_lines_parameters, &
    & is_homogeneous_time, nb_top_time, ndim1_prof2d, ndim2_prof2d, nb_b0, &
    & nb_times_eff_in, index
  !
  !**********************************************************************
  !
  flag_status = 0
  nb_times_eff_in = size(index_times_eff_in)
  !
  ! check ids_properties/homogeneous_time to know which "time" to use (note only for time_slice(:)/time)
  is_homogeneous_time = equil_in_ids%ids_properties%homogeneous_time
  if (nverbose .ge. 3) write(*,*) 'homogeneous_time = ',is_homogeneous_time
  !
  nb_times_in = 1
  if (.not. associated(equil_in_ids%time)) then
    ! needed for vacuum etc later
    ! not associated
    flag_status = -3
    if (associated(equil_out_itm) .and. size(equil_out_itm) .ge. 1) equil_out_itm(1)%codeparam%output_flag = flag_status
    return
  end if
  if (associated(equil_in_ids%time_slice)) then
    if (is_homogeneous_time .eq. 1) then
      nb_times_in = size(equil_in_ids%time)
    else
      nb_times_in = size(equil_in_ids%time_slice)
    end if
  else
    ! not associated, return since cannot do anything if there is no time_slice
    flag_status = -2
    if (associated(equil_out_itm) .and. size(equil_out_itm) .ge. 1) equil_out_itm(1)%codeparam%output_flag = flag_status
    return
  end if
  if (nb_times_in .lt. 1) then
    ! problem with size of input
    if (nverbose .ge. 1) write(0,*) 'problem with size of input: nb_times_in = ',nb_times_in,' in copy_ids_to_itm_equilibrium'
    flag_status = -3
    if (associated(equil_out_itm) .and. size(equil_out_itm) .ge. 1) equil_out_itm(1)%codeparam%output_flag = flag_status
    return ! return since cannot do anything if there is no clear time() or time_slice()
  end if
  !
  if ((nb_times_eff_in .gt. nb_times_in) .or. (index_times_eff_in(nb_times_eff_in) .gt. nb_times_in)) then
    write(0,*) 'unexpected sizes of nb_times_eff_in, nb_times_in and index_times_eff_in(nb_times_eff_in)= ', &
      & nb_times_eff_in, nb_times_in,index_times_eff_in(nb_times_eff_in)
    flag_status = -4
    if (associated(equil_out_itm) .and. size(equil_out_itm) .ge. 1) equil_out_itm(1)%codeparam%output_flag = flag_status
    return
  end if
  if (nverbose .ge. 3)  print *,'nb_times_in, nb_times_eff_in= ',nb_times_in, nb_times_eff_in
  allocate(equil_out_itm(nb_times_eff_in))
  !
  nb_top_time = size(equil_in_ids%time) ! for code and vacuum_toroidal_field
  if (nb_top_time .ne. nb_times_in) then
    if (associated(equil_in_ids%vacuum_toroidal_field%b0)) then
      nb_b0 = size(equil_in_ids%vacuum_toroidal_field%b0)
      if (nb_b0 .eq. nb_times_in) then
        ! then use nb_times_in below for nb of b0
        nb_top_time = nb_b0
      else
        if (nverbose .ge. 1) then
          write(0,*) 'Note that size(equil_in_ids%time) is not the same as size(equil_in_ids%time_slice).', &
            & ' Not sure how to copy code parameters and vacuum_toroidal_field'
        end if
      end if
    end if
  end if
  !
  ! loop over time for time_slice parts and rest if nb_top_time = nb_times_eff_in
  ! for code parameters use (:) of time for line copy of input
  !
  do index=1,nb_times_eff_in
    it = index_times_eff_in(index)
    ! requires at least psi, rho_tor or rho_tor_norm to be defined for radial variable
    if (associated(equil_in_ids%time_slice(it)%profiles_1d%psi)) then
      npsi_1d = size(equil_in_ids%time_slice(it)%profiles_1d%psi)
    else
      npsi_1d = 0
    end if
    if (npsi_1d .le. 0) then
      if (nverbose .ge. 1) write(0,*) 'size(equil_in_ids%time_slice(it)%profiles_1d%psi) = ',npsi_1d,' ; try rho_tor'
      if (associated(equil_in_ids%time_slice(it)%profiles_1d%rho_tor)) npsi_1d = size(equil_in_ids%time_slice(it)%profiles_1d%rho_tor)
    end if
    if (npsi_1d .le. 0) then
      if (nverbose .ge. 1) write(0,*) 'size(equil_in_ids%time_slice(it)%profiles_1d%rho_tor) = ',npsi_1d,' ; try rho_tor_norm'
      if (associated(equil_in_ids%time_slice(it)%profiles_1d%rho_tor_norm)) npsi_1d = size(equil_in_ids%time_slice(it)%profiles_1d%rho_tor_norm)
    end if
    if (npsi_1d .le. 0) then
      if (nverbose .ge. 1) then
        write(0,*) 'associated equil_in_ids%time_slice(it)%profiles_1d%psi/rho_tor/rho_tor_norm= ', &
          & associated(equil_in_ids%time_slice(it)%profiles_1d%psi), associated(equil_in_ids%time_slice(it)%profiles_1d%rho_tor), &
          & associated(equil_in_ids%time_slice(it)%profiles_1d%rho_tor_norm)
        write(0,*) 'size(equil_in_ids%time_slice(it)%profiles_1d%psi) = ',size(equil_in_ids%time_slice(it)%profiles_1d%psi)
        write(0,*) 'size(equil_in_ids%time_slice(it)%profiles_1d%rho_tor) = ', &
          & size(equil_in_ids%time_slice(it)%profiles_1d%rho_tor)
        write(0,*) 'size(equil_in_ids%time_slice(it)%profiles_1d%rho_tor_norm) = ', &
          & size(equil_in_ids%time_slice(it)%profiles_1d%rho_tor_norm)
      end if
      flag_status = -11 + flag_status
      if (associated(equil_out_itm) .and. size(equil_out_itm) .ge. it) equil_out_itm(index)%codeparam%output_flag = flag_status
    elseif (nverbose .ge. 3) then
      write(*,*) 'npsi_1d = ',npsi_1d
      call flush(6)
    end if
    if (associated(equil_in_ids%time_slice(it)%profiles_2d)) then
       nb_profiles_2d = size(equil_in_ids%time_slice(it)%profiles_2d)
    else
       nb_profiles_2d = 0
    end if
    if (nb_profiles_2d .le. 0) then
      if (nverbose .ge. 1) write(0,*) 'nb_profiles_2d = ',nb_profiles_2d
      flag_status = -12 + flag_status
      if (associated(equil_out_itm) .and. size(equil_out_itm) .ge. it) equil_out_itm(index)%codeparam%output_flag = flag_status
    elseif (nverbose .ge. 3) then
      write(*,*) 'nb_profiles_2d = ',nb_profiles_2d
      call flush(6)
    end if
    if (associated(equil_in_ids%time_slice(it)%coordinate_system%grid%dim1)) then
       ndim1_coord_sys = size(equil_in_ids%time_slice(it)%coordinate_system%grid%dim1,1)
    else
       ndim1_coord_sys = 0
    end if
    if (ndim1_coord_sys .le. 0) then
      if (nverbose .ge. 2) write(0,*) 'ndim1_coord_sys = ',ndim1_coord_sys
      flag_status = -13 + flag_status
      if (associated(equil_out_itm) .and. size(equil_out_itm) .ge. it) equil_out_itm(index)%codeparam%output_flag = flag_status
    elseif (nverbose .ge. 3) then
      write(*,*) 'ndim1_coord_sys = ',ndim1_coord_sys
      call flush(6)
    end if
    if (associated(equil_in_ids%time_slice(it)%coordinate_system%grid%dim2)) then
       ndim2_coord_sys = size(equil_in_ids%time_slice(it)%coordinate_system%grid%dim2,1)
    else
       ndim2_coord_sys = 0
    end if
    if (ndim2_coord_sys .le. 0) then
      if (nverbose .ge. 2) write(0,*) 'ndim2_coord_sys = ',ndim2_coord_sys
      flag_status = -14 + flag_status
      if (associated(equil_out_itm) .and. size(equil_out_itm) .ge. it) equil_out_itm(index)%codeparam%output_flag = flag_status
    elseif (nverbose .ge. 3) then
      write(*,*) 'ndim2_coord_sys = ',ndim2_coord_sys
      call flush(6)
    end if
    !
    if (is_homogeneous_time .eq. 1) then
      equil_out_itm(index)%time = equil_in_ids%time(it)
    else
      equil_out_itm(index)%time = equil_in_ids%time_slice(it)%time
    end if
    if (nverbose .ge. 3) then
      write(*,*) 'time(it=',it,') = ',equil_out_itm(index)%time
    end if
    !
    ! datainfo
    !
    allocate(equil_out_itm(index)%datainfo%dataprovider(1))
    equil_out_itm(index)%datainfo%dataprovider(1) = 'copy_ids_vDD3_0.0PUAL3_0.0_to_itm_v4_10a_equilibrium.f90'
    ! should include date
    CALL DATE_AND_TIME(ZDATE)
    allocate(equil_out_itm(index)%datainfo%putdate(1))
    equil_out_itm(index)%datainfo%putdate = ZDATE
    if ( associated(equil_in_ids%ids_properties%comment) ) then
      allocate(equil_out_itm(index)%datainfo%comment(size(equil_in_ids%ids_properties%comment)))
      equil_out_itm(index)%datainfo%comment = equil_in_ids%ids_properties%comment
    end if
    !    equil_out_itm(index)%datainfo%cocos = equil_in_ids%ids_properties%cocos
    !
    ! eqgeometry
    !
    equil_out_itm(index)%eqgeometry%boundarytype = equil_in_ids%time_slice(it)%boundary%type
    select case (nidsbound)
    case (0)
      if (associated(equil_in_ids%time_slice(it)%boundary%outline%r)) then
        nb_points = size(equil_in_ids%time_slice(it)%boundary%outline%r)
        if (.not. associated(equil_in_ids%time_slice(it)%boundary%outline%z)) then
          if (nverbose .ge. 1) write(0,*) 'equil_in_ids%time_slice(it)%boundary%outline%z not associated'
          nb_points = 0
        else
          if (size(equil_in_ids%time_slice(it)%boundary%outline%z) .lt. nb_points) then
            nb_points = 0
          end if
        end if
      else
        nb_points = 0
      end if
      ! patch in case lcfs exist but not outline
      if ((nb_points .le. 0) .and. (associated(equil_in_ids%time_slice(it)%boundary%lcfs%r)) .and. &
        & (size(equil_in_ids%time_slice(it)%boundary%lcfs%r) .gt. 0)) then
        ! copy lcfs values on to outline
        if (nverbose .ge. 0) then
          write(0,*)'======================================================================'
          write(0,*)'equil_in_ids%time_slice(it)%boundary%lcfs%r/z copied on input equil_in_ids%time_slice(it)%boundary%outline%r/z'
        end if
        nb_points = size(equil_in_ids%time_slice(it)%boundary%lcfs%r)
        if ( (associated(equil_in_ids%time_slice(it)%boundary%lcfs%z)) .and. &
          & (size(equil_in_ids%time_slice(it)%boundary%lcfs%z) .ge. nb_points) ) then
          allocate(equil_in_ids%time_slice(it)%boundary%outline%r(nb_points))
          equil_in_ids%time_slice(it)%boundary%outline%r(1:nb_points) = equil_in_ids%time_slice(it)%boundary%lcfs%r(1:nb_points)
          allocate(equil_in_ids%time_slice(it)%boundary%outline%z(nb_points))
          equil_in_ids%time_slice(it)%boundary%outline%z(1:nb_points) = equil_in_ids%time_slice(it)%boundary%lcfs%z(1:nb_points)
        else
          if (nverbose .ge. 1) write(0,*) 'equil_in_ids%time_slice(it)%boundary%lcfs%z not associated or size too small'
          nb_points = 0
        end if
      end if
    case (1)
      if (associated(equil_in_ids%time_slice(it)%boundary%outline%r)) then
        nb_points = size(equil_in_ids%time_slice(it)%boundary%outline%r)
      else
        nb_points = 0
      end if
      if (associated(equil_in_ids%time_slice(it)%boundary%outline%z)) then
        if (size(equil_in_ids%time_slice(it)%boundary%outline%z) .lt. nb_points) then
          if (nverbose .ge. 1) write(0,*) 'size(equil_in_ids%time_slice(it)%boundary%outline%z)=', &
            & size(equil_in_ids%time_slice(it)%boundary%outline%z),' < ', &
            & size(equil_in_ids%time_slice(it)%boundary%outline%r),'=size(equil_in_ids%time_slice(it)%boundary%outline%r)'
          nb_points = 0
        end if
      else
        if (nverbose .ge. 1) write(0,*) 'equil_in_ids%time_slice(it)%boundary%outline%z not associated'
        nb_points = 0
      end if
    case (2)
      if ( (associated(equil_in_ids%time_slice(it)%boundary%lcfs%r)) .and. &
        & (size(equil_in_ids%time_slice(it)%boundary%lcfs%r) .gt. 0) ) then
        ! copy lcfs values on to outline and use only outline below
        nb_points = size(equil_in_ids%time_slice(it)%boundary%lcfs%r)
        if ( (associated(equil_in_ids%time_slice(it)%boundary%lcfs%z)) .and. &
          & (size(equil_in_ids%time_slice(it)%boundary%lcfs%z) .ge. nb_points) ) then
          allocate(equil_in_ids%time_slice(it)%boundary%outline%r(nb_points))
          equil_in_ids%time_slice(it)%boundary%outline%r(1:nb_points) = equil_in_ids%time_slice(it)%boundary%lcfs%r(1:nb_points)
          allocate(equil_in_ids%time_slice(it)%boundary%outline%z(nb_points))
          equil_in_ids%time_slice(it)%boundary%outline%z(1:nb_points) = equil_in_ids%time_slice(it)%boundary%lcfs%z(1:nb_points)
        else
          if (nverbose .ge. 1) write(0,*) 'equil_in_ids%time_slice(it)%boundary%lcfs%z not associated or size too small'
          nb_points = 0
        end if
      else
        if (nverbose .ge. 1) write(0,*) 'equil_in_ids%time_slice(it)%boundary%lcfs%r not associated or size too small'
        nb_points = 0
      end if
    case default
      if (nverbose .ge. 1) write(0,*) 'case nidsbound = ',nidsbound,' not defined'
      nb_points = 0
    end select
    !
    if (nb_points .le. 0) then
      if (nverbose .ge. 1) write(0,*) 'size(equil_in_ids%time_slice(it)%boundary%outline%r) = ',nb_points
      flag_status = -21 + flag_status
      if (associated(equil_out_itm) .and. size(equil_out_itm) .ge. it) equil_out_itm(index)%codeparam%output_flag = flag_status
      return
    end if
    allocate(equil_out_itm(index)%eqgeometry%boundary(1))
    allocate(equil_out_itm(index)%eqgeometry%boundary(1)%r(nb_points))
    allocate(equil_out_itm(index)%eqgeometry%boundary(1)%z(nb_points))
    equil_out_itm(index)%eqgeometry%boundary(1)%r(1:nb_points) = equil_in_ids%time_slice(it)%boundary%outline%r(1:nb_points)
    equil_out_itm(index)%eqgeometry%boundary(1)%z(1:nb_points) = equil_in_ids%time_slice(it)%boundary%outline%z(1:nb_points)
    !
    equil_out_itm(index)%eqgeometry%geom_axis%r = equil_in_ids%time_slice(it)%boundary%geometric_axis%r
    equil_out_itm(index)%eqgeometry%geom_axis%z = equil_in_ids%time_slice(it)%boundary%geometric_axis%z
    equil_out_itm(index)%eqgeometry%a_minor = equil_in_ids%time_slice(it)%boundary%minor_radius
    equil_out_itm(index)%eqgeometry%elongation = equil_in_ids%time_slice(it)%boundary%elongation
    equil_out_itm(index)%eqgeometry%elong_upper = equil_in_ids%time_slice(it)%boundary%elongation_upper
    equil_out_itm(index)%eqgeometry%elong_lower = equil_in_ids%time_slice(it)%boundary%elongation_lower
    equil_out_itm(index)%eqgeometry%tria_upper = equil_in_ids%time_slice(it)%boundary%triangularity_upper
    equil_out_itm(index)%eqgeometry%tria_lower = equil_in_ids%time_slice(it)%boundary%triangularity_lower
    !
    ! global_param
    equil_out_itm(index)%global_param%beta_pol = equil_in_ids%time_slice(it)%global_quantities%beta_pol
    equil_out_itm(index)%global_param%beta_tor = equil_in_ids%time_slice(it)%global_quantities%beta_tor
    equil_out_itm(index)%global_param%beta_normal = equil_in_ids%time_slice(it)%global_quantities%beta_normal
    equil_out_itm(index)%global_param%i_plasma = equil_in_ids%time_slice(it)%global_quantities%ip
    equil_out_itm(index)%global_param%li = equil_in_ids%time_slice(it)%global_quantities%li_3
    equil_out_itm(index)%global_param%volume = equil_in_ids%time_slice(it)%global_quantities%volume
    equil_out_itm(index)%global_param%area = equil_in_ids%time_slice(it)%global_quantities%area
    equil_out_itm(index)%global_param%psi_ax = equil_in_ids%time_slice(it)%global_quantities%psi_axis
    equil_out_itm(index)%global_param%psi_bound = equil_in_ids%time_slice(it)%global_quantities%psi_boundary
    equil_out_itm(index)%global_param%mag_axis%position%r = equil_in_ids%time_slice(it)%global_quantities%magnetic_axis%r
    equil_out_itm(index)%global_param%mag_axis%position%z = equil_in_ids%time_slice(it)%global_quantities%magnetic_axis%z
    equil_out_itm(index)%global_param%mag_axis%bphi = equil_in_ids%time_slice(it)%global_quantities%magnetic_axis%b_field_tor
    equil_out_itm(index)%global_param%mag_axis%q = equil_in_ids%time_slice(it)%global_quantities%q_axis
    equil_out_itm(index)%global_param%q_95 = equil_in_ids%time_slice(it)%global_quantities%q_95
    equil_out_itm(index)%global_param%q_min = equil_in_ids%time_slice(it)%global_quantities%q_min%value
    equil_out_itm(index)%global_param%toroid_field%r0 = equil_in_ids%vacuum_toroidal_field%r0
    if (associated(equil_in_ids%vacuum_toroidal_field%b0) .and. (size(equil_in_ids%vacuum_toroidal_field%b0) .gt. 0)) then
      if (nb_top_time .eq. nb_times_in) then
        equil_out_itm(index)%global_param%toroid_field%b0 = equil_in_ids%vacuum_toroidal_field%b0(it)
      elseif (nb_top_time .eq. 1) then
        equil_out_itm(index)%global_param%toroid_field%b0 = equil_in_ids%vacuum_toroidal_field%b0(1)
      else
        if (nverbose .ge. 1) then
          write(0,*) 'size of equil%time_slice .ne. size equil%vacuum_... and latter not 1'
          write(0,*) 'Should do interpolation? discuss with O. Sauter'
          write(0,*) 'nb slices = ',nb_times_eff_in,' size/equil%time)= ',nb_top_time, &
            & ' size(vacuum...b0)= ',size(equil_in_ids%vacuum_toroidal_field%b0)
        end if
      end if
    end if
    equil_out_itm(index)%global_param%w_mhd = equil_in_ids%time_slice(it)%global_quantities%w_mhd
    !
    ! profiles_1d
    if (npsi_1d .gt. 0) then
      ! Should allocate only if there is finite size in ids, otherwise one does not know it was not provided?
      ! Changed Aug 2016. Add warnings for important quantities usually used as input for a new equilibrium
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%psi)) .and. (size(equil_in_ids%time_slice(it)%profiles_1d%psi) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%psi(npsi_1d))
        equil_out_itm(index)%profiles_1d%psi(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%psi(1:npsi_1d)
      else
        if (nverbose .ge. 1) write(0,*) 'input profiles_1d%psi not provided'
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%phi)) .and. (size(equil_in_ids%time_slice(it)%profiles_1d%phi) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%phi(npsi_1d))
        equil_out_itm(index)%profiles_1d%phi(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%phi(1:npsi_1d)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%pressure)) .and. &
        & (size(equil_in_ids%time_slice(it)%profiles_1d%pressure) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%pressure(npsi_1d))
        equil_out_itm(index)%profiles_1d%pressure(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%pressure(1:npsi_1d)
      else
        if (nverbose .ge. 1) write(0,*) 'input profiles_1d%pressure not provided'
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%f)) .and. (size(equil_in_ids%time_slice(it)%profiles_1d%f) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%f_dia(npsi_1d))
        equil_out_itm(index)%profiles_1d%f_dia(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%f(1:npsi_1d)
      else
        if (nverbose .ge. 1) write(0,*) 'input profiles_1d%f_dia not provided'
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%dpressure_dpsi)) .and. &
        & (size(equil_in_ids%time_slice(it)%profiles_1d%dpressure_dpsi) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%pprime(npsi_1d))
        equil_out_itm(index)%profiles_1d%pprime(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%dpressure_dpsi(1:npsi_1d)
      else
        if (nverbose .ge. 1) write(0,*) 'input profiles_1d%dpressure_dpsi not provided'
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%f_df_dpsi)) .and. &
        & (size(equil_in_ids%time_slice(it)%profiles_1d%f_df_dpsi) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%ffprime(npsi_1d))
        equil_out_itm(index)%profiles_1d%ffprime(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%f_df_dpsi(1:npsi_1d)
      else
        if (nverbose .ge. 1) write(0,*) 'input profiles_1d%f_df_dpsi not provided'
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%j_tor)) .and. (size(equil_in_ids%time_slice(it)%profiles_1d%j_tor) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%jphi(npsi_1d))
        equil_out_itm(index)%profiles_1d%jphi(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%j_tor(1:npsi_1d)
      else
        if (nverbose .ge. 1) write(0,*) 'input profiles_1d%j_phi not provided'
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%j_parallel)) .and. &
        & (size(equil_in_ids%time_slice(it)%profiles_1d%j_parallel) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%jparallel(npsi_1d))
        equil_out_itm(index)%profiles_1d%jparallel(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%j_parallel(1:npsi_1d)
      else
        if (nverbose .ge. 1) write(0,*) 'input profiles_1d%j_parallel not provided'
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%q)) .and. (size(equil_in_ids%time_slice(it)%profiles_1d%q) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%q(npsi_1d))
        equil_out_itm(index)%profiles_1d%q(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%q(1:npsi_1d)
      else
        if (nverbose .ge. 1) write(0,*) 'input profiles_1d%q not provided'
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%magnetic_shear)) .and. &
        & (size(equil_in_ids%time_slice(it)%profiles_1d%magnetic_shear) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%shear(npsi_1d))
        equil_out_itm(index)%profiles_1d%shear(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%magnetic_shear(1:npsi_1d)
      end if
      if (size(equil_in_ids%time_slice(it)%profiles_1d%r_inboard) .ge. npsi_1d) then
        allocate(equil_out_itm(index)%profiles_1d%r_inboard(npsi_1d))
        equil_out_itm(index)%profiles_1d%r_inboard(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%r_inboard(1:npsi_1d)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%r_outboard)) .and. &
        & (size(equil_in_ids%time_slice(it)%profiles_1d%r_outboard) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%r_outboard(npsi_1d))
        equil_out_itm(index)%profiles_1d%r_outboard(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%r_outboard(1:npsi_1d)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%rho_tor)) .and. (size(equil_in_ids%time_slice(it)%profiles_1d%rho_tor) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%rho_tor(npsi_1d))
        equil_out_itm(index)%profiles_1d%rho_tor(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%rho_tor(1:npsi_1d)
      else
        if (nverbose .ge. 1) write(0,*) 'input profiles_1d%rho_tor not provided'
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%dpsi_drho_tor)) .and. &
        & (size(equil_in_ids%time_slice(it)%profiles_1d%dpsi_drho_tor) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%dpsidrho_tor(npsi_1d))
        equil_out_itm(index)%profiles_1d%dpsidrho_tor(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%dpsi_drho_tor(1:npsi_1d)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%volume)) .and. (size(equil_in_ids%time_slice(it)%profiles_1d%volume) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%rho_vol(npsi_1d))
        equil_out_itm(index)%profiles_1d%rho_vol(1:npsi_1d) = sqrt(equil_in_ids%time_slice(it)%profiles_1d%volume(1:npsi_1d) &
          & /equil_in_ids%time_slice(it)%profiles_1d%volume(npsi_1d))
        equil_out_itm(index)%profiles_1d%rho_vol(npsi_1d) = RC1P
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%elongation)) .and. &
        & (size(equil_in_ids%time_slice(it)%profiles_1d%elongation) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%elongation(npsi_1d))
        equil_out_itm(index)%profiles_1d%elongation(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%elongation(1:npsi_1d)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%triangularity_upper)) .and. &
        & (size(equil_in_ids%time_slice(it)%profiles_1d%triangularity_upper) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%tria_upper(npsi_1d))
        equil_out_itm(index)%profiles_1d%tria_upper(1:npsi_1d) = &
          & equil_in_ids%time_slice(it)%profiles_1d%triangularity_upper(1:npsi_1d)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%triangularity_lower)) .and. &
        & (size(equil_in_ids%time_slice(it)%profiles_1d%triangularity_lower) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%tria_lower(npsi_1d))
        equil_out_itm(index)%profiles_1d%tria_lower(1:npsi_1d) = &
          & equil_in_ids%time_slice(it)%profiles_1d%triangularity_lower(1:npsi_1d)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%volume)) .and. (size(equil_in_ids%time_slice(it)%profiles_1d%volume) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%volume(npsi_1d))
        equil_out_itm(index)%profiles_1d%volume(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%volume(1:npsi_1d)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%dvolume_dpsi)) .and. &
        & (size(equil_in_ids%time_slice(it)%profiles_1d%dvolume_dpsi) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%vprime(npsi_1d))
        equil_out_itm(index)%profiles_1d%vprime(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%dvolume_dpsi(1:npsi_1d)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%dvolume_drho_tor)) .and. &
        & (size(equil_in_ids%time_slice(it)%profiles_1d%dvolume_drho_tor) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%dvdrho(npsi_1d))
        equil_out_itm(index)%profiles_1d%dvdrho(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%dvolume_drho_tor(1:npsi_1d)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%area)) .and. (size(equil_in_ids%time_slice(it)%profiles_1d%area) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%area(npsi_1d))
        equil_out_itm(index)%profiles_1d%area(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%area(1:npsi_1d)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%darea_dpsi)) .and. &
        & (size(equil_in_ids%time_slice(it)%profiles_1d%darea_dpsi) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%aprime(npsi_1d))
        equil_out_itm(index)%profiles_1d%aprime(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%darea_dpsi(1:npsi_1d)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%surface)) .and. &
        & (size(equil_in_ids%time_slice(it)%profiles_1d%surface) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%surface(npsi_1d))
        equil_out_itm(index)%profiles_1d%surface(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%surface(1:npsi_1d)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%trapped_fraction)) .and. &
           & (size(equil_in_ids%time_slice(it)%profiles_1d%trapped_fraction) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%ftrap(npsi_1d))
        equil_out_itm(index)%profiles_1d%ftrap(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%trapped_fraction(1:npsi_1d)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%gm1)) .and. (size(equil_in_ids%time_slice(it)%profiles_1d%gm1) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%gm1(npsi_1d))
        equil_out_itm(index)%profiles_1d%gm1(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%gm1(1:npsi_1d)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%gm2)) .and. (size(equil_in_ids%time_slice(it)%profiles_1d%gm2) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%gm2(npsi_1d))
        equil_out_itm(index)%profiles_1d%gm2(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%gm2(1:npsi_1d)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%gm3)) .and. (size(equil_in_ids%time_slice(it)%profiles_1d%gm3) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%gm3(npsi_1d))
        equil_out_itm(index)%profiles_1d%gm3(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%gm3(1:npsi_1d)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%gm4)) .and. (size(equil_in_ids%time_slice(it)%profiles_1d%gm4) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%gm4(npsi_1d))
        equil_out_itm(index)%profiles_1d%gm4(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%gm4(1:npsi_1d)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%gm5)) .and. (size(equil_in_ids%time_slice(it)%profiles_1d%gm5) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%gm5(npsi_1d))
        equil_out_itm(index)%profiles_1d%gm5(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%gm5(1:npsi_1d)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%gm6)) .and. (size(equil_in_ids%time_slice(it)%profiles_1d%gm6) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%gm6(npsi_1d))
        equil_out_itm(index)%profiles_1d%gm6(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%gm6(1:npsi_1d)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%gm7)) .and. (size(equil_in_ids%time_slice(it)%profiles_1d%gm7) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%gm7(npsi_1d))
        equil_out_itm(index)%profiles_1d%gm7(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%gm7(1:npsi_1d)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%gm8)) .and. (size(equil_in_ids%time_slice(it)%profiles_1d%gm8) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%gm8(npsi_1d))
        equil_out_itm(index)%profiles_1d%gm8(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%gm8(1:npsi_1d)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%gm9)) .and. (size(equil_in_ids%time_slice(it)%profiles_1d%gm9) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%gm9(npsi_1d))
        equil_out_itm(index)%profiles_1d%gm9(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%gm9(1:npsi_1d)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%b_field_average)) .and. &
           & (size(equil_in_ids%time_slice(it)%profiles_1d%b_field_average) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%b_av(npsi_1d))
        equil_out_itm(index)%profiles_1d%b_av(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%b_field_average(1:npsi_1d)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%b_field_min)) .and. &
           & (size(equil_in_ids%time_slice(it)%profiles_1d%b_field_min) .ge. npsi_1d) ) then
        allocate(equil_out_itm(index)%profiles_1d%b_min(npsi_1d))
        equil_out_itm(index)%profiles_1d%b_min(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%b_field_min(1:npsi_1d)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%profiles_1d%b_field_max)) .and. &
           & (size(equil_in_ids%time_slice(it)%profiles_1d%b_field_max) .ge. npsi_1d) ) then
         allocate(equil_out_itm(index)%profiles_1d%b_max(npsi_1d))
         equil_out_itm(index)%profiles_1d%b_max(1:npsi_1d) = equil_in_ids%time_slice(it)%profiles_1d%b_field_max(1:npsi_1d)
      end if
      !
    end if
    ! profiles_2d
    if (nb_profiles_2d .gt. 0) then
      allocate(equil_out_itm(index)%profiles_2d(nb_profiles_2d))
      do i=1,nb_profiles_2d
        allocate(equil_out_itm(index)%profiles_2d(i)%grid_type(4))
        if ( (associated(equil_in_ids%time_slice(it)%profiles_2d)) .and. &
             & (associated(equil_in_ids%time_slice(it)%profiles_2d(i)%grid_type%name)) ) then
          if (nverbose .ge. 3) write(*,*) 'size name= ',size(equil_in_ids%time_slice(it)%profiles_2d(i)%grid_type%name)
          if (size(equil_in_ids%time_slice(it)%profiles_2d(i)%grid_type%name) .ge. 1) &
            & equil_out_itm(index)%profiles_2d(i)%grid_type(1) = equil_in_ids%time_slice(it)%profiles_2d(i)%grid_type%name(1)
        else
          if (nverbose .ge. 3) write(*,*) 'profiles_2d(i)%grid_type not associated'
        end if
        write(equil_out_itm(index)%profiles_2d(i)%grid_type(2),*) &
          & equil_in_ids%time_slice(it)%profiles_2d(i)%grid_type%index,' index'
        if ( (associated(equil_in_ids%time_slice(it)%profiles_2d(i)%grid_type%description)) .and. &
          & (size(equil_in_ids%time_slice(it)%profiles_2d(i)%grid_type%description) .ge. 1)) &
          & equil_out_itm(index)%profiles_2d(i)%grid_type(3) = equil_in_ids%time_slice(it)%profiles_2d(i)%grid_type%description(1)
        write(equil_out_itm(index)%profiles_2d(i)%grid_type(4),*) &
          & equil_in_ids%time_slice(it)%profiles_2d(i)%grid_type%index,' index'
        !
        if (associated(equil_in_ids%time_slice(it)%profiles_2d(i)%grid%dim1)) then
          ndim1_prof2d = size(equil_in_ids%time_slice(it)%profiles_2d(i)%grid%dim1)
        else
          ndim1_prof2d = 0
        end if
        if (associated(equil_in_ids%time_slice(it)%profiles_2d(i)%grid%dim2)) then
          ndim2_prof2d = size(equil_in_ids%time_slice(it)%profiles_2d(i)%grid%dim2)
        else
          ndim2_prof2d = 0
        end if
        if ((ndim1_prof2d .le. 0) .or. (ndim2_prof2d .le. 0)) then
          if (nverbose .ge. 1) write(0,*) 'ndim1_prof2d, ndim2_prof2d = ',ndim1_prof2d, ndim2_prof2d
          flag_status = -31 + flag_status
          if (associated(equil_out_itm) .and. size(equil_out_itm) .ge. it) &
            & equil_out_itm(index)%codeparam%output_flag = flag_status
        else
          allocate(equil_out_itm(index)%profiles_2d(i)%grid%dim1(ndim1_prof2d))
          if (size(equil_in_ids%time_slice(it)%profiles_2d(i)%grid%dim1) .ge. ndim1_prof2d) &
            & equil_out_itm(index)%profiles_2d(i)%grid%dim1(1:ndim1_prof2d) = &
            & equil_in_ids%time_slice(it)%profiles_2d(i)%grid%dim1(1:ndim1_prof2d)
          allocate(equil_out_itm(index)%profiles_2d(i)%grid%dim2(ndim2_prof2d))
          if (size(equil_in_ids%time_slice(it)%profiles_2d(i)%grid%dim2) .ge. ndim2_prof2d) &
            & equil_out_itm(index)%profiles_2d(i)%grid%dim2(1:ndim2_prof2d) = &
            & equil_in_ids%time_slice(it)%profiles_2d(i)%grid%dim2(1:ndim2_prof2d)
          if ( (associated(equil_in_ids%time_slice(it)%profiles_2d(i)%r)) .and. &
            & (size(equil_in_ids%time_slice(it)%profiles_2d(i)%r) .ge. ndim1_prof2d*ndim2_prof2d) ) then
            allocate(equil_out_itm(index)%profiles_2d(i)%r(1:ndim1_prof2d,1:ndim2_prof2d))
            equil_out_itm(index)%profiles_2d(i)%r(1:ndim1_prof2d,1:ndim2_prof2d) = &
              & equil_in_ids%time_slice(it)%profiles_2d(i)%r(1:ndim1_prof2d,1:ndim2_prof2d)
          end if
          if ( (associated(equil_in_ids%time_slice(it)%profiles_2d(i)%z)) .and. &
            & (size(equil_in_ids%time_slice(it)%profiles_2d(i)%z) .ge. ndim1_prof2d*ndim2_prof2d) ) then
            allocate(equil_out_itm(index)%profiles_2d(i)%z(1:ndim1_prof2d,1:ndim2_prof2d))
            equil_out_itm(index)%profiles_2d(i)%z(1:ndim1_prof2d,1:ndim2_prof2d) = &
              & equil_in_ids%time_slice(it)%profiles_2d(i)%z(1:ndim1_prof2d,1:ndim2_prof2d)
              end if
              if ( (associated(equil_in_ids%time_slice(it)%profiles_2d(i)%psi)) .and. &
                & (size(equil_in_ids%time_slice(it)%profiles_2d(i)%psi) .ge. ndim1_prof2d*ndim2_prof2d) ) then
            allocate(equil_out_itm(index)%profiles_2d(i)%psi(1:ndim1_prof2d,1:ndim2_prof2d))
            equil_out_itm(index)%profiles_2d(i)%psi(1:ndim1_prof2d,1:ndim2_prof2d) = &
              & equil_in_ids%time_slice(it)%profiles_2d(i)%psi(1:ndim1_prof2d,1:ndim2_prof2d)
          end if
          if ( (associated(equil_in_ids%time_slice(it)%profiles_2d(i)%theta)) .and. &
            & (size(equil_in_ids%time_slice(it)%profiles_2d(i)%theta) .ge. ndim1_prof2d*ndim2_prof2d) ) then
            allocate(equil_out_itm(index)%profiles_2d(i)%theta(1:ndim1_prof2d,1:ndim2_prof2d))
            equil_out_itm(index)%profiles_2d(i)%theta(1:ndim1_prof2d,1:ndim2_prof2d) = &
              & equil_in_ids%time_slice(it)%profiles_2d(i)%theta(1:ndim1_prof2d,1:ndim2_prof2d)
          end if
          if ( (associated(equil_in_ids%time_slice(it)%profiles_2d(i)%phi)) .and. &
            & (size(equil_in_ids%time_slice(it)%profiles_2d(i)%phi) .ge. ndim1_prof2d*ndim2_prof2d) ) then
            allocate(equil_out_itm(index)%profiles_2d(i)%phi(1:ndim1_prof2d,1:ndim2_prof2d))
            equil_out_itm(index)%profiles_2d(i)%phi(1:ndim1_prof2d,1:ndim2_prof2d) = &
              & equil_in_ids%time_slice(it)%profiles_2d(i)%phi(1:ndim1_prof2d,1:ndim2_prof2d)
          end if
          if ( (associated(equil_in_ids%time_slice(it)%profiles_2d(i)%j_tor)) .and. &
            & (size(equil_in_ids%time_slice(it)%profiles_2d(i)%j_tor) .ge. ndim1_prof2d*ndim2_prof2d) ) then
            allocate(equil_out_itm(index)%profiles_2d(i)%jphi(1:ndim1_prof2d,1:ndim2_prof2d))
            equil_out_itm(index)%profiles_2d(i)%jphi(1:ndim1_prof2d,1:ndim2_prof2d) = &
              & equil_in_ids%time_slice(it)%profiles_2d(i)%j_tor(1:ndim1_prof2d,1:ndim2_prof2d)
          end if
          if ( (associated(equil_in_ids%time_slice(it)%profiles_2d(i)%j_parallel)) .and. &
            & (size(equil_in_ids%time_slice(it)%profiles_2d(i)%j_parallel) .ge. ndim1_prof2d*ndim2_prof2d) ) then
            allocate(equil_out_itm(index)%profiles_2d(i)%jpar(1:ndim1_prof2d,1:ndim2_prof2d))
            equil_out_itm(index)%profiles_2d(i)%jpar(1:ndim1_prof2d,1:ndim2_prof2d) = &
              & equil_in_ids%time_slice(it)%profiles_2d(i)%j_parallel(1:ndim1_prof2d,1:ndim2_prof2d)
          end if
          if ( (associated(equil_in_ids%time_slice(it)%profiles_2d(i)%b_field_r)) .and. &
            & (size(equil_in_ids%time_slice(it)%profiles_2d(i)%b_field_r) .ge. ndim1_prof2d*ndim2_prof2d) ) then
            allocate(equil_out_itm(index)%profiles_2d(i)%br(1:ndim1_prof2d,1:ndim2_prof2d))
            equil_out_itm(index)%profiles_2d(i)%br(1:ndim1_prof2d,1:ndim2_prof2d) = &
              & equil_in_ids%time_slice(it)%profiles_2d(i)%b_field_r(1:ndim1_prof2d,1:ndim2_prof2d)
          end if
          if ( (associated(equil_in_ids%time_slice(it)%profiles_2d(i)%b_field_z)) .and. &
            & (size(equil_in_ids%time_slice(it)%profiles_2d(i)%b_field_z) .ge. ndim1_prof2d*ndim2_prof2d) ) then
            allocate(equil_out_itm(index)%profiles_2d(i)%bz(1:ndim1_prof2d,1:ndim2_prof2d))
            equil_out_itm(index)%profiles_2d(i)%bz(1:ndim1_prof2d,1:ndim2_prof2d) = &
              & equil_in_ids%time_slice(it)%profiles_2d(i)%b_field_z(1:ndim1_prof2d,1:ndim2_prof2d)
          end if
          if ( (associated(equil_in_ids%time_slice(it)%profiles_2d(i)%b_field_tor)) .and. &
            & (size(equil_in_ids%time_slice(it)%profiles_2d(i)%b_field_tor) .ge. ndim1_prof2d*ndim2_prof2d) ) then
            allocate(equil_out_itm(index)%profiles_2d(i)%bphi(1:ndim1_prof2d,1:ndim2_prof2d))
            equil_out_itm(index)%profiles_2d(i)%bphi(1:ndim1_prof2d,1:ndim2_prof2d) = &
              & equil_in_ids%time_slice(it)%profiles_2d(i)%b_field_tor(1:ndim1_prof2d,1:ndim2_prof2d)
          end if
        end if
      end do
    end if
    !
    ! coord_sys
    allocate(equil_out_itm(index)%coord_sys%grid_type(4))
    if (associated(equil_in_ids%time_slice(it)%coordinate_system%grid_type%name)) then
      if (size(equil_in_ids%time_slice(it)%coordinate_system%grid_type%name) .ge. 1) &
        & equil_out_itm(index)%coord_sys%grid_type(1) = equil_in_ids%time_slice(it)%coordinate_system%grid_type%name(1)
    end if
    write(equil_out_itm(index)%coord_sys%grid_type(2),*) equil_in_ids%time_slice(it)%coordinate_system%grid_type%index,' index'
    if (associated(equil_in_ids%time_slice(it)%coordinate_system%grid_type%description)) then
      if (size(equil_in_ids%time_slice(it)%coordinate_system%grid_type%description) .ge. 1) &
        & equil_out_itm(index)%coord_sys%grid_type(3) = equil_in_ids%time_slice(it)%coordinate_system%grid_type%description(1)
    end if
    write(equil_out_itm(index)%coord_sys%grid_type(4),*) equil_in_ids%time_slice(it)%coordinate_system%grid_type%index,' index'
    !
    if ((ndim1_coord_sys .gt. 0) .and. (ndim2_coord_sys .gt. 0)) then
       if ( (associated(equil_in_ids%time_slice(it)%coordinate_system%grid%dim1)) .and. &
            & (size(equil_in_ids%time_slice(it)%coordinate_system%grid%dim1) .ge. ndim1_coord_sys) ) then
        allocate(equil_out_itm(index)%coord_sys%grid%dim1(ndim1_coord_sys))
        equil_out_itm(index)%coord_sys%grid%dim1(1:ndim1_coord_sys) = &
          & equil_in_ids%time_slice(it)%coordinate_system%grid%dim1(1:ndim1_coord_sys)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%coordinate_system%grid%dim2)) .and. &
           & (size(equil_in_ids%time_slice(it)%coordinate_system%grid%dim2) .ge. ndim2_coord_sys) ) then
        allocate(equil_out_itm(index)%coord_sys%grid%dim2(ndim2_coord_sys))
        equil_out_itm(index)%coord_sys%grid%dim2(1:ndim2_coord_sys) = &
          & equil_in_ids%time_slice(it)%coordinate_system%grid%dim2(1:ndim2_coord_sys)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%coordinate_system%r)) .and. &
        & (size(equil_in_ids%time_slice(it)%coordinate_system%r) .ge. ndim1_coord_sys*ndim2_coord_sys) ) then
        allocate(equil_out_itm(index)%coord_sys%position%r(1:ndim1_coord_sys,1:ndim2_coord_sys))
        equil_out_itm(index)%coord_sys%position%r(1:ndim1_coord_sys,1:ndim2_coord_sys) = &
          & equil_in_ids%time_slice(it)%coordinate_system%r(1:ndim1_coord_sys,1:ndim2_coord_sys)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%coordinate_system%z)) .and. &
        & (size(equil_in_ids%time_slice(it)%coordinate_system%z) .ge. ndim1_coord_sys*ndim2_coord_sys) ) then
        allocate(equil_out_itm(index)%coord_sys%position%z(1:ndim1_coord_sys,1:ndim2_coord_sys))
        equil_out_itm(index)%coord_sys%position%z(1:ndim1_coord_sys,1:ndim2_coord_sys) = &
          & equil_in_ids%time_slice(it)%coordinate_system%z(1:ndim1_coord_sys,1:ndim2_coord_sys)
      end if
      if ( (associated(equil_in_ids%time_slice(it)%coordinate_system%jacobian)) .and. &
        & (size(equil_in_ids%time_slice(it)%coordinate_system%jacobian) .ge. ndim1_coord_sys*ndim2_coord_sys) ) then
        allocate(equil_out_itm(index)%coord_sys%jacobian(1:ndim1_coord_sys,1:ndim2_coord_sys))
        equil_out_itm(index)%coord_sys%jacobian(1:ndim1_coord_sys,1:ndim2_coord_sys) = &
          & equil_in_ids%time_slice(it)%coordinate_system%jacobian(1:ndim1_coord_sys,1:ndim2_coord_sys)
      end if
      if (associated(equil_in_ids%time_slice(it)%coordinate_system%tensor_contravariant)) then
        if (size(equil_in_ids%time_slice(it)%coordinate_system%tensor_contravariant) .ge. 9*ndim1_coord_sys*ndim2_coord_sys) then
          allocate(equil_out_itm(index)%coord_sys%g_11(1:ndim1_coord_sys,1:ndim2_coord_sys))
          allocate(equil_out_itm(index)%coord_sys%g_12(1:ndim1_coord_sys,1:ndim2_coord_sys))
          allocate(equil_out_itm(index)%coord_sys%g_13(1:ndim1_coord_sys,1:ndim2_coord_sys))
          allocate(equil_out_itm(index)%coord_sys%g_22(1:ndim1_coord_sys,1:ndim2_coord_sys))
          allocate(equil_out_itm(index)%coord_sys%g_23(1:ndim1_coord_sys,1:ndim2_coord_sys))
          allocate(equil_out_itm(index)%coord_sys%g_33(1:ndim1_coord_sys,1:ndim2_coord_sys))
          equil_out_itm(index)%coord_sys%g_11(1:ndim1_coord_sys,1:ndim2_coord_sys) = &
            & equil_in_ids%time_slice(it)%coordinate_system%tensor_contravariant(1:ndim1_coord_sys,1:ndim2_coord_sys,1,1)
          equil_out_itm(index)%coord_sys%g_12(1:ndim1_coord_sys,1:ndim2_coord_sys) = &
            & equil_in_ids%time_slice(it)%coordinate_system%tensor_contravariant(1:ndim1_coord_sys,1:ndim2_coord_sys,1,2)
          equil_out_itm(index)%coord_sys%g_13(1:ndim1_coord_sys,1:ndim2_coord_sys) = &
            & equil_in_ids%time_slice(it)%coordinate_system%tensor_contravariant(1:ndim1_coord_sys,1:ndim2_coord_sys,1,3)
          equil_out_itm(index)%coord_sys%g_22(1:ndim1_coord_sys,1:ndim2_coord_sys) = &
            & equil_in_ids%time_slice(it)%coordinate_system%tensor_contravariant(1:ndim1_coord_sys,1:ndim2_coord_sys,2,2)
          equil_out_itm(index)%coord_sys%g_23(1:ndim1_coord_sys,1:ndim2_coord_sys) = &
            & equil_in_ids%time_slice(it)%coordinate_system%tensor_contravariant(1:ndim1_coord_sys,1:ndim2_coord_sys,2,3)
          equil_out_itm(index)%coord_sys%g_33(1:ndim1_coord_sys,1:ndim2_coord_sys) = &
            & equil_in_ids%time_slice(it)%coordinate_system%tensor_contravariant(1:ndim1_coord_sys,1:ndim2_coord_sys,3,3)
        end if
      end if
    end if
    !
    ! codeparam: time dependent part
    if (nb_top_time .eq. nb_times_in) then
      if (associated(equil_in_ids%code%output_flag) .and. size(equil_in_ids%code%output_flag) .ge. 1) then
        equil_out_itm(index)%codeparam%output_flag = equil_in_ids%code%output_flag(it)
      end if
    elseif (nb_top_time .eq. 1) then
      if (associated(equil_in_ids%code%output_flag) .and. size(equil_in_ids%code%output_flag) .ge. 1) then
        equil_out_itm(index)%codeparam%output_flag = equil_in_ids%code%output_flag(1)
      end if
    else
      if (nverbose .ge. 1) then
        write(0,*) 'size of equil%time_slice .ne. size equil%time and latter not 1'
        write(0,*) 'Should do interpolation? discuss with O. Sauter'
        if (associated(equil_in_ids%code%output_flag)) then
          write(0,*) 'nb slices = ',nb_times_eff_in,' size/equil%time)= ',nb_top_time, &
            & ' size(code%output_flag)= ',size(equil_in_ids%code%output_flag)
        end if
      end if
    end if
    !
    if (associated(equil_in_ids%code%name)) then
      nb_dim_codename = size(equil_in_ids%code%name)
    else
      nb_dim_codename = 0
    end if
    if (nb_dim_codename .gt. 0) then
      allocate(equil_out_itm(index)%codeparam%codename(nb_dim_codename))
      equil_out_itm(index)%codeparam%codename = equil_in_ids%code%name
    else
      allocate(equil_out_itm(index)%codeparam%codename(80))
      equil_out_itm(index)%codeparam%codename = 'chease'
    end if
    if (associated(equil_in_ids%code%version)) then
       nb_dim_codeversion = size(equil_in_ids%code%version)
       if (nb_dim_codeversion .gt. 0) then
          allocate(equil_out_itm(index)%codeparam%codeversion(nb_dim_codeversion))
          equil_out_itm(index)%codeparam%codeversion = equil_in_ids%code%version
       end if
    end if
    if (associated(equil_in_ids%code%parameters)) then
       nb_lines_parameters = size(equil_in_ids%code%parameters)
       if (nb_lines_parameters .GT. 0) then
          allocate(equil_out_itm(index)%codeparam%parameters(nb_lines_parameters))
          equil_out_itm(index)%codeparam%parameters = equil_in_ids%code%parameters
       end if
    end if
    !
  end do
  !
  flag_status = 0
  !
END subroutine copy_ids_to_itm_equilibrium
