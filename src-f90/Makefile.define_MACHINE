# Copyright 2024 SPC-EPFL
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#----------------------------------------------------------------------
#
#
CHEASE_MACHINE = none
 ifneq (,$(findstring efda-itm.eu,$(HOSTNAME)))
   # allows to match all enea14x.efda-itm.eu machines
   CHEASE_MACHINE = gateway
 endif
 ifneq (,$(findstring lac,$(HOSTNAME)))
   # allows to match all lacx.epfl.ch machines
   CHEASE_MACHINE = linux_nohdf5
 endif
 ifneq (,$(findstring lac5,$(HOSTNAME)))
   # allows to match all lacx.epfl.ch machines
   CHEASE_MACHINE = linux_hdf5
 endif
 ifneq (,$(findstring lac8,$(HOSTNAME)))
   # allows to match all lacx.epfl.ch machines
   CHEASE_MACHINE = linux_hdf5
 endif
 ifneq (,$(findstring pleiades,$(HOSTNAME)))
   # allows to match all pleiadesx machines
   CHEASE_MACHINE = pleiades
 endif
 ifneq (,$(findstring darwin,$(HOSTNAME)))
   CHEASE_MACHINE = darwin
 endif
 ifneq (,$(findstring anika,$(HOSTNAME)))
   CHEASE_MACHINE = darwin
 endif
 ifneq (,$(findstring sun,$(HOSTNAME)))
   CHEASE_MACHINE = sun
 endif
 ifneq (,$(findstring crpppc74,$(HOSTNAME)))
   CHEASE_MACHINE = crpppc74
 endif
 ifneq (,$(findstring crpppc214,$(HOSTNAME)))
   CHEASE_MACHINE = crpppc214
 endif
 ifneq (,$(findstring bg-fe1,$(HOSTNAME)))
   CHEASE_MACHINE = bg-fe1
 endif
 ifneq (,$(findstring guiness,$(HOSTNAME)))
   CHEASE_MACHINE = guiness
 endif
 ifneq (,$(findstring iter.org,$(HOSTNAME)))
   CHEASE_MACHINE = ITER
 endif
 ifneq (,$(findstring g0,$(HOSTNAME)))
   # allows to match all enea14x.efda-itm.eu machines
   CHEASE_MACHINE = gateway
 endif
 ifneq (,$(findstring curie,$(HOSTNAME)))
   CHEASE_MACHINE = curie
 endif
 ifneq (,$(findstring hopper,$(HOSTNAME)))
   CHEASE_MACHINE = hopper
 endif
 ifneq (,$(findstring xmhd,$(HOSTNAME)))
   CHEASE_MACHINE = xmhd
 endif
 ifneq (,$(findstring qa0,$(HOSTNAME)))
   CHEASE_MACHINE = aug
 endif
 ifneq (,$(findstring tok,$(HOSTNAME)))
   CHEASE_MACHINE = aug
 endif
 ifneq (,$(findstring cobra,$(HOSTNAME)))
   CHEASE_MACHINE = aug
 endif
 ifeq ($(findstring mpg.de,$(DOMAINNAME)),mpg.de)
   ifeq ($(findstring nb-thay-1,$(HOSTNAME)),nb-thay-1)
     CHEASE_MACHINE = ubuntu_22.04
   else
     CHEASE_MACHINE = aug
   endif
 endif
 ifneq (,$(findstring occigen,$(HOSTNAME)))
   CHEASE_MACHINE = occigen
 endif
 ifneq (,$(findstring irene,$(HOSTNAME)))
   CHEASE_MACHINE = irene
 endif
 ifneq (,$(findstring jean-zay,$(HOSTNAME)))
   CHEASE_MACHINE = jean-zay
 endif
 ifneq (,$(findstring Mac,$(HOSTNAME)))
   CHEASE_MACHINE = darwin
 endif
 ifneq (,$(findstring rfx,$(HOSTNAME)))
   CHEASE_MACHINE = rfx_linux
 endif
 ifneq (,$(findstring partenaires,$(HOSTNAME)))
   CHEASE_MACHINE = partenaires_linux
 endif
 ifneq (,$(findstring yes,$(ITM_ENVIRONMENT_LOADED)))
    # overwrites the CHEASE_MACHINE set in Makefile.define_MACHINE by using hostname
    CHEASE_MACHINE = gateway_with_itm
 endif
 ifneq (,$(findstring yes,$(IMAS_ENVIRONMENT_LOADED)))
    # overwrites the CHEASE_MACHINE set in Makefile.define_MACHINE by using hostname
    CHEASE_MACHINE = gateway_with_itm
    ifneq (,$(findstring iter.org,$(HOSTNAME)))
      CHEASE_MACHINE = ITER
    endif
 endif
 ifeq ($(findstring marconi.cineca.it,$(DOMAINNAME)),marconi.cineca.it)
   CHEASE_MACHINE = marconi
 endif
