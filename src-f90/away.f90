! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*DECK C2SD07
!*CALL PROCESS
SUBROUTINE AWAY(KD,KB1,KB2)
  !        ###########################
  !
  !                                        AUTHORS:
  !                                        H. LUTJENS,  CRPP-EPFL
  !                                        A. BONDESON, CRPP-EPFL
  !
  !**********************************************************************
  !                                                                     *
  ! C2SD07 PERFORMS FOLLOWING LINE-ROW OPERATIONS ON MATRIX A:          *
  !                                                                     *
  !        1) SET LINE KD TO 0                                          *
  !        2) SET ROW KD TO 0                                           *
  !        3) SET DIAGONAL TERM A(1,KD) TO 1                            *
  !                                                                     *
  !        KB1 AND KB2 ARE THE 2 EXTREMAL INDICES OF THE BAND MATRIX.   *
  !                                                                     *
  !**********************************************************************
  !
  !
  USE globals
  IMPLICIT NONE
  !
  !----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*----
  !
  INCLUDE 'BNDIND.inc'
  !
  !----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*----
  !
  INTEGER          ::     IAROW
  INTEGER          ::     KD
  INTEGER          ::     IACOL
  INTEGER          ::     KB2
  INTEGER          ::     KB1
  INTEGER          ::     J1
  !
  DO J1=KB1,KB2
     !
     IACOL = INDCOL(J1,KD)
     IAROW = INDROW(J1,KD)
     !
     A(IACOL,IAROW) = 0
     !
  END DO
  !
  A(1,KD) = 1
  !
  RETURN
END SUBROUTINE AWAY
