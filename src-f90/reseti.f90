! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*DECK U41
!*CALL PROCESS
SUBROUTINE RESETI(KA,KDIM,KVALUE)
  !        #################################
  !
  ! U.41   RESET INTEGER ARRAY TO SPECIFIED VALUE
  !
  !---*----*----*----*----*----*----*----*----*----*----*----*----*----*
  !
  USE globals
  IMPLICIT NONE
  INTEGER            ::       KVALUE      ! <reseti.f90>
  INTEGER            ::       KA      ! <reseti.f90>
  INTEGER            ::       J1      ! <reseti.f90>
  INTEGER            ::       KDIM      ! <reseti.f90>
  DIMENSION &
       &   KA(KDIM)
  !
  DO J1=1,KDIM
     !
     KA(J1) = KVALUE
     !
  END DO
  !
  RETURN
END SUBROUTINE RESETI
