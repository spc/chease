! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*DECK C2S05
!*CALL PROCESS
SUBROUTINE MATRIX
  !        #################
  !
  !                                        AUTHORS:
  !                                        H. LUTJENS,  CRPP-EPFL
  !                                        A. BONDESON, CRPP-EPFL
  !
  !**********************************************************************
  !                                                                     *
  ! C2S05 SET UP MATRIX A AND PERFORMS L * D * LT DECOMPOSITION OF A    *
  !                                                                     *
  !**********************************************************************
  !
  !
  USE globals
  IMPLICIT NONE
  !
  !---*----*----*----*----*----*----*----*----*----*----*----*----*----*
  !
  !
  INTEGER          ::     ISGN
  CALL SETUPA
  CALL ALDLT(A,RC1M14,N4NSNT,NBAND,NPBAND,ISGN)
  !
  IF (ISGN .EQ. -1) THEN
    write(0,*) 'in matrix: ISGN = -1'
    eqchease_out(index_out)%codeparam%output_diag(1) = 'in matrix: ISGN = -1'
    eqchease_out(index_out)%codeparam%output_flag = -151
    return
  end if
  !
  RETURN
END SUBROUTINE MATRIX
