! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*COMDECK COMDAT
!
!**********************************************************************
!                                                                     *
!     C.20   EQUILBRIUM NAMELIST                                      *
!                                                                     *
!**********************************************************************
!
!    ADD COMMENTS ABOUT NAMELIST VARIABLES HERE:
!
!    NIDEAL:  DETERMINES MAPPING OF SPECIFIC CHEASE RESULTS ON TO A SPECIFIC MESH, IN GENERAL FOR A GIVEN CODE
!            =0: FOR MARS
!            =1: FOR ERATO
!            =2: FOR LION
!            =3: FOR NOVA-W (FILE INP1)
!            =4: FOR PENN (FILE NPENN)
!            =5: FOR XTOR (FILES ASTRO, OUTXTOR)
!            =6 (DEFAULT): FOR STANDARD OUTPUT, INCLUDING EQDSK
!            =7: FOR GYROKINETIC CODES (FILE EQCIN)
!            =8: FOR ELITE (FILE NELITE)
!            =9: FOR OGYROPSI (FILE OGYROPSI), REQUIRES STRAIGHT FIELD LINE JACOBIAN
!            =10:FOR GKW AND NEOART WITH HAMADA COORDINATES
!
!     EQDSK and SI units output related:
!
!     R0EXP:  NORMALIZATION OF LENGTH (TYPICALLY GEOMETRICAL AXIS OF LCFS) IN METERS
!     B0EXP:  NORMALIZATION OF MAGNETIC FIELD (VACUUM VALUE AT R=R0EXP) IN TESLA
!     RBOXLFT, RBOXLEN, ZBOXMID, ZBOXLEN: FOR (R,Z) BOX, ALL IN METERS, WILL BE NORMALIZED BY R0EXP
!     ZBOXMID: IF ZBOXMID NOT SET, CHEASE COMPUTES A BOX NEAR THE PLASMA BOUNDARY
!
!     IMAS-EU-IDS/ITM RELATED VARIABLES:
!
!    NITMOPT: DETERMINES TYPE OF INPUT AND POSSIBILITIES. IF SET TO -1 AS DEFAULT VALUE, ASSUME CHEASE NOT COMPILED WITH IDS PACKAGE. IF SET TO 0, THEN CAN BE CHANGED IN NAMELIST SUCH THAT:
!            =01: READ EQUILIBRIUM FROM IDS STRUCTURE, BUT WRITE STANDARD OUTPUT FILES, use OPEN_PULSE with error if input data structure does not exist
!            =11: READ AND WRITE EQUIL FROM AND TO IDS DATA STRUCTURE, for writing create only if does not exist, using FORCE_OPEN_PULSE
!            =10: READ EQUIL FROM STANDARD FILES AND WRITE TO IDS DATA STRUCTURE, for writing create only if does not exist, using FORCE_OPEN_PULSE
!            =21: same as 11 but assumes data exists for writing, hence error if it does not, using OPEN_PULSE
!            =20: same as 10 but assumes data exists for writing, hence error if it does not, using OPEN_PULSE
!            =00: READ FROM FILES AND WRITE ON FILES AS BEFORE
!            =22: READ AND WRITE VIA KEPLER/iwrap/...
!    NITMSHOT(1)/NITMRUN(1)/NITMOCC(1): SHOT, RUN AND OCCURENCE NUMBER TO READ IDS DATA FROM
!    NITMSHOT(2)/NITMRUN(2)/NITMOCC(2): SHOT, RUN AND OCCURENCE NUMBER TO WRITE IDS DATA TO
!    TREEITM: IDS TREE TO USE, TREEITM(1) FOR READING AND TREEITM(2) FOR WRITING THE DATA
!         AT THIS STAGE, IT IS ASSUMED THAT ONLY THE SIGNAL_NAME='EQUILIBRIUM' IS DEALT WITH
!    TIME_REF: (default=-999) If time_ref!=-999 then uses time_ref to define on which time slices to run CHEASE. Not used if NITMOPT=0 or (22).
!              TIME to set in output if nitmopt>=10
!              When reading from the database can provide several times or intervals:
!              =[t1]: run for this time
!              =[t1, t2]: run for this interval t in [t1, t2]
!              =[t1, t2(<0), t3]: if t2<0 use it as delta-t=abs(t2) interval: [t1:abs(t2):t3]
!              =[t1, t2, t3, t4, ... up to t20]: run for these times but only those strictly increasing from t1
!              (to run for only 2 times give: [t1, t1-t2, t2] )
!    TREE_USER(2) (1 for input, 2 for output): username, name for public database, usually $USER (default) except if a public database is used
!    TREE_TOKAMAK(2): name of subtree for the ids database, typically a tokamak name or 'test' (default)
!    TREE_MAJORVERSION(2): top version of datamodel, now only '3' (default) is tested/available, intreface with '4' to be developed when available
!    TREE_BACKEND(2): default: [1, 1] (1 (hdf5), 2 (mdsplus) backends)
!
!    NIDSBOUND: Allows to match older IDS version or misunderstanding between outline and lcfs if both filled in
!              =0 (default) take plasma boundary from default standard IDS for given IMAS version (now: outline if not empty otherwise lcfs)
!              =1 Use boundary.outline (only)
!              =2 Use boundary.lcfs (only)
!
!    NVERBOSE: -1: No messages printed, no write(0,*), no writing onto files
!               0: write(0,*) messages only + outmksa messages with summary table
!               1: add print *,... and simple messages  + standard output after the convergence + write to files (should be standard minimum except when fully performing)
!               2: print messages during convergence as well (corresponds to previous CHEASE output, thus default value)
!               3: and higher, add debug messages
!
!    COCOS_IN/OUT: COordinate COnventionS, that is R, phi, Z orientations, rho,theta, phi orientations and psi B field definition with respect to G=RB_phi and grad psi
!                             _IN for convention expected in input
!                             _OUT for convention desired in output
!                             = 13: original ITM convention: phi counter-clock from top, (R,phi,Z) theta counter-clockwise (rho,phi,theta); B=G grad phi + grad phi x (-grad psi_13/2pi)
!                             = 11: ITER convention: phi counter-clock from top, (R,phi,Z) theta counter-clockwise (rho,theta,phi); B=G grad phi + grad phi x grad psi_11/2pi
!                             = 2: eqdsk(?),CHEASE(+Ip>0,B0>0) convention: phi clockwise from top, (R,Z,phi) theta counter-clockwise (rho,theta,phi); B=G grad phi + grad phi x grad psi_2
!        Note: COCOS = 2 is the original CHEASE convention (See O. Sauter's paper)
!
!  PSIBNDEXP: if -9.e40 (<-1.e40) keep as is (psi=0 on axis and whatever at edge, psi_chease shifted by csprf in norept)
!             if +9.e40 (> 1e40) use input psi_edge to define psibndexp in iodisk then shift as below
!             otherwise shift psi such that psi_edge = PSIBNDEXP in SI units thus all done in metrictoitm, assumes PSIBNDEXP in consistent cocos value
!
Namelist /EQDATA/ &
  &   AFBS,    AFBS2,   AP,      APLACE,  AP2,     ASPCT,   AT,      AT2, &
  &   AT3,     AT4,     AWIDTH,  BEANS,   BENTAXIS,BENTQPROFILE,     BENTRADIUS, &
  &   BPLACE,  BSFRAC,  BWIDTH,  B0EXP,   CETA,    CFBAL,   CFNRESS, CFNRESSO, &
  &   COCOS_IN,COCOS_OUT,CPLACE, CPRESS,  CPRESSO, CQ0,     CSSPEC,  CURRT, &
  &   CWIDTH,  DELTA,   DPLACE,  DWIDTH,  ELONG,   EPLACE, &
  &   ETAEI,   EWIDTH,  EPSLON,  GAMMA,   PANGLE,  PPRIME_BAL_MAX, PREDGE,  PSIBNDEXP, &
  &   PSISCL,  QSPEC,   QPLACE,  QWIDTH,  QVALNEO, RBOXLEN, RBOXLFT, &
  &   RC,      RELAX,   REXT,    RNU,     RODABYROD0, RPEOP,   RZION, &
  &   RZ0,     RZ0C,    RZ0W,    R0,      R0EXP,   R0W,     SCALNE, &
  &   SCEXP,   SIGNB0XP,SIGNIPXP,SGMA,    SLIMIT,  SNUMBER, &
  &   SOLPDA,  SOLPDB,  SOLPDC,  SOLPDD,  SOLPDE,  SOLPDPOL, SHIFT_P, TENSPROF,TENSBND, &
  &   THETA0,  TIME_REF,TREEITM, TREE_USER,    TREE_TOKAMAK,  TREE_MAJORVERSION, TREE_BACKEND, &
  &   TRIANG,  TRIPLT,  XI,      ZBOXLEN, ZBOXMID, &
  &   MDT,     MSMAX,   N0JEDGE, NANAL,   NBAL,    NBLC0,   NBLOPT,   NBPSOUT, NBSEXPQ, &
  &   NBSFUN,  NBSOPT,  NBSTRP,  NCHI,    NCSCAL,  NDIAGOP, NDIFPS, &
  &   NDIFT,   NEGP,    NEONBQS, NEQDSK,  NER,     NIDSBOUND,NITMOCC, NITMOPT,  NITMRUN, NITMSHOT,  NFFTOPT, &
  &   NFIXAXIS,NFIXWALL,NFUNC,   NFUNRHO, NIDEAL,  NINBLOPT, NINMAP,  NINSCA,   NIPR,    NISO,    NMESHA, &
  &   NMESHB,  NMESHC,  NMESHD,  NMESHE,  NMESHPOL, NMESHPOLEXP, NMGAUS,  NOPT,    NOUTXTOR, &
  &   NPLOT,   NPOIDA,  NPOIDB,  NPOIDC,  NPOIDD,  NPOIDE,  &
  &   NPOIDQ,  NPP,     NPPFUN,  NPPR,    NPROF2D, NPROFZ,  NPROPT, &
  &   NPRPSI,  NPSI,    NRBOX,   NRBOX_XTOR,       NRFP,    NRHOMESH, NRSCAL,  NS, &
  &   NSGAUS,  NSMOOTH, NSOUR,   NSTTP,   NSURF,   NSYM, &
  &   NT,      NTCASE,  NTGAUS,  NTEST,   NTMF0,   NTNOVA, &
  &   NTURN,   NV,      NVEXP,   NZBOX,   NZBOX_XTOR, &
  &   NEQDXTPO,NVERBOSE,COMMENTS
!
!    TENSPROF: Tension for input profile smoothing (used in interpos). Set it to 0. for no smoothing
!    TENSBND: Tension for input plasma boundary smoothing. Set it to 0. for no smoothing
!
