! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
! IDS FORTRAN 90 type definitions
! Contains the type definition of all IDSs


module ids_types    ! declare the size of real and integer variables to be used in all sub-trees, along with the invalid numbers.

  use iso_c_binding, only: ids_real => c_double, &
                           ids_int  => c_int32_t


  implicit none

  integer(ids_int), parameter :: ids_string_length = 132_ids_int

  integer(ids_int), parameter :: ids_int_invalid  = -999999999_ids_int
  real(ids_real),   parameter :: ids_real_invalid = -9.0E40_ids_real

  integer(ids_int), parameter :: ids_data_dictionary_version(3) = (/ ids_int_invalid , ids_int_invalid , ids_int_invalid /)  !! NOTE: to be filled with e.g. (/3,7,4/).

  ! ids_is_valid - Function for testing the validity of scalar and arrays of integers and real numbers
  interface ids_is_valid
     module procedure &
          ids_is_valid_int, &
          ids_is_valid_ids_real, &
          ids_is_valid_array_of_int, &
          ids_is_valid_array_of_real
  end interface

contains

  logical function ids_is_valid_int(in)
    implicit none
    integer(ids_int) :: in
    ids_is_valid_int = in .ne. ids_int_invalid
    return
  end function ids_is_valid_int

  logical function ids_is_valid_ids_real(in)
    real(ids_real) :: in
    ids_is_valid_ids_real = abs(in - ids_real_invalid) .gt. tiny(ids_real_invalid)
    return
  end function ids_is_valid_ids_real

  logical function ids_is_valid_array_of_int(in)
    integer(ids_int) :: in(:)
    ids_is_valid_array_of_int = .not. any( in(:) .eq. ids_int_invalid )
    return
  end function ids_is_valid_array_of_int

  logical function ids_is_valid_array_of_real(in)
    real(ids_real) :: in(:)
    ids_is_valid_array_of_real = .not. any( abs(in(:) - ids_real_invalid) .le. tiny(ids_real_invalid) )
    return
  end function ids_is_valid_array_of_real

end module ids_types

module ids_utilities    ! declare the set of types common to all sub-trees

use ids_types

type ids_identifier  !    Standard type for identifiers (constant). The three fields: name, index and description are all representations of the same inform
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Short string identifier
  integer(ids_int)  :: index=ids_int_invalid       ! /index - Integer identifier (enumeration index within a list)
  character(len=ids_string_length), dimension(:), pointer ::description => null()       ! /description - Verbose description
endtype

type ids_identifier_static  !    Standard type for identifiers (static). The three fields: name, index and description are all representations of the same informat
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Short string identifier
  integer(ids_int)  :: index=ids_int_invalid       ! /index - Integer identifier (enumeration index within a list)
  character(len=ids_string_length), dimension(:), pointer ::description => null()       ! /description - Verbose description
endtype

type ids_identifier_static_1d  !    Standard type for identifiers (static, 1D). The three fields: name, index and description are all representations of the same info
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Short string identifier
  integer(ids_int)  :: index=ids_int_invalid       ! /index - Integer identifier (enumeration index within a list)
  character(len=ids_string_length), dimension(:), pointer ::description => null()       ! /description - Verbose description
endtype

type ids_identifier_dynamic_aos3  !    Standard type for identifiers (dynamic within type 3 array of structures (index on time)). The three fields: name, index and descr
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Short string identifier
  integer(ids_int)  :: index=ids_int_invalid       ! /index - Integer identifier (enumeration index within a list)
  character(len=ids_string_length), dimension(:), pointer ::description => null()       ! /description - Verbose description
endtype

type ids_plasma_composition_ion_state_constant  !    Definition of an ion state (when describing the plasma composition) (constant)
  real(ids_real)  :: z_min=ids_real_invalid       ! /z_min - Minimum Z of the charge state bundle (z_min = z_max = 0 for a neutral)
  real(ids_real)  :: z_min_error_upper=ids_real_invalid
  real(ids_real)  :: z_min_error_lower=ids_real_invalid
  integer(ids_int) :: z_min_error_index=ids_int_invalid

  real(ids_real)  :: z_max=ids_real_invalid       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(ids_real)  :: z_max_error_upper=ids_real_invalid
  real(ids_real)  :: z_max_error_lower=ids_real_invalid
  integer(ids_int) :: z_max_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying ion state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
endtype

type ids_plasma_composition_neutral_state_constant  !    Definition of a neutral state (when describing the plasma composition) (constant)
  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying neutral state
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  type (ids_identifier) :: neutral_type  ! /neutral_type - Neutral type, in terms of energy. ID =1: cold; 2: thermal; 3: fast; 4: NBI
endtype

type ids_plasma_composition_ion_state  !    Definition of an ion state (when describing the plasma composition) (within a type 3 AoS)
  real(ids_real)  :: z_min=ids_real_invalid       ! /z_min - Minimum Z of the charge state bundle (z_min = z_max = 0 for a neutral)
  real(ids_real)  :: z_min_error_upper=ids_real_invalid
  real(ids_real)  :: z_min_error_lower=ids_real_invalid
  integer(ids_int) :: z_min_error_index=ids_int_invalid

  real(ids_real)  :: z_max=ids_real_invalid       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(ids_real)  :: z_max_error_upper=ids_real_invalid
  real(ids_real)  :: z_max_error_lower=ids_real_invalid
  integer(ids_int) :: z_max_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying ion state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
endtype

type ids_plasma_composition_ions_constant  !    Description of plasma ions (constant)
  type (ids_plasma_composition_neutral_element_constant),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(ids_real)  :: z_ion=ids_real_invalid       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed)
  real(ids_real)  :: z_ion_error_upper=ids_real_invalid
  real(ids_real)  :: z_ion_error_lower=ids_real_invalid
  integer(ids_int) :: z_ion_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  type (ids_plasma_composition_ion_state_constant) :: state  ! /state - Quantities related to the different states of the species (ionisation, energy, excitation, ...)
endtype

type ids_plasma_composition_neutral_constant  !    Definition of plasma neutral (constant)
  type (ids_plasma_composition_neutral_element_constant),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying neutral (e.g. H, D, T, He, C, ...)
  type (ids_plasma_composition_neutral_state_constant) :: state  ! /state - State of the species (energy, excitation, ...)
endtype

type ids_gas_mixture_constant  !    Description of a neutral species within a gas mixture (constant)
  type (ids_plasma_composition_neutral_element_constant),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying neutral (e.g. H, D, T, He, C, ...)
  integer(ids_int)  :: fraction=ids_int_invalid       ! /fraction - Relative fraction of this species (in molecules) in the gas mixture
endtype

type ids_plasma_composition_ions  !    Array of plasma ions (within a type 3 AoS)
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(ids_real)  :: z_ion=ids_real_invalid       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed)
  real(ids_real)  :: z_ion_error_upper=ids_real_invalid
  real(ids_real)  :: z_ion_error_lower=ids_real_invalid
  integer(ids_int) :: z_ion_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  integer(ids_int)  :: multiple_states_flag=ids_int_invalid       ! /multiple_states_flag - Multiple states calculation flag : 0-Only one state is considered; 1-Multiple states are considered
  type (ids_plasma_composition_ion_state),pointer :: state(:) => null()  ! /state(i) - Quantities related to the different states of the species (ionisation, energy, excitation, ...)
endtype

type ids_plasma_composition_species  !    Description of simple species (elements) without declaration of their ionisation state
  real(ids_real)  :: a=ids_real_invalid       ! /a - Mass of atom
  real(ids_real)  :: a_error_upper=ids_real_invalid
  real(ids_real)  :: a_error_lower=ids_real_invalid
  integer(ids_int) :: a_error_index=ids_int_invalid

  real(ids_real)  :: z_n=ids_real_invalid       ! /z_n - Nuclear charge
  real(ids_real)  :: z_n_error_upper=ids_real_invalid
  real(ids_real)  :: z_n_error_lower=ids_real_invalid
  integer(ids_int) :: z_n_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H, D, T, ...)
endtype

type ids_plasma_composition_neutral_element_constant  !    Element entering in the composition of the neutral atom or molecule (constant)
  real(ids_real)  :: a=ids_real_invalid       ! /a - Mass of atom
  real(ids_real)  :: a_error_upper=ids_real_invalid
  real(ids_real)  :: a_error_lower=ids_real_invalid
  integer(ids_int) :: a_error_index=ids_int_invalid

  real(ids_real)  :: z_n=ids_real_invalid       ! /z_n - Nuclear charge
  real(ids_real)  :: z_n_error_upper=ids_real_invalid
  real(ids_real)  :: z_n_error_lower=ids_real_invalid
  integer(ids_int) :: z_n_error_index=ids_int_invalid

  integer(ids_int)  :: atoms_n=ids_int_invalid       ! /atoms_n - Number of atoms of this element in the molecule
  real(ids_real)  :: multiplicity=ids_real_invalid       ! /multiplicity - Multiplicity of the atom
  real(ids_real)  :: multiplicity_error_upper=ids_real_invalid
  real(ids_real)  :: multiplicity_error_lower=ids_real_invalid
  integer(ids_int) :: multiplicity_error_index=ids_int_invalid

endtype

type ids_plasma_composition_neutral_element  !    Element entering in the composition of the neutral atom or molecule (within a type 3 AoS)
  real(ids_real)  :: a=ids_real_invalid       ! /a - Mass of atom
  real(ids_real)  :: a_error_upper=ids_real_invalid
  real(ids_real)  :: a_error_lower=ids_real_invalid
  integer(ids_int) :: a_error_index=ids_int_invalid

  real(ids_real)  :: z_n=ids_real_invalid       ! /z_n - Nuclear charge
  real(ids_real)  :: z_n_error_upper=ids_real_invalid
  real(ids_real)  :: z_n_error_lower=ids_real_invalid
  integer(ids_int) :: z_n_error_index=ids_int_invalid

  integer(ids_int)  :: atoms_n=ids_int_invalid       ! /atoms_n - Number of atoms of this element in the molecule
  real(ids_real)  :: multiplicity=ids_real_invalid       ! /multiplicity - Multiplicity of the atom
  real(ids_real)  :: multiplicity_error_upper=ids_real_invalid
  real(ids_real)  :: multiplicity_error_lower=ids_real_invalid
  integer(ids_int) :: multiplicity_error_index=ids_int_invalid

endtype

type ids_plasma_composition_neutral  !    Definition of a neutral atom or molecule
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  type (ids_identifier),pointer :: type(:) => null()  ! /type(i) - List of neutral types, in terms of energy, considered for that neutral species. ID =1: cold; 2: ther
  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying the atom or molecule (e.g. D2, DT, CD4, ...)
endtype

type ids_b_tor_vacuum_1  !    Characteristics of the vacuum toroidal field. time assumed to be one level above
  real(ids_real)  :: r0=ids_real_invalid       ! /r0 - Reference major radius where the vacuum toroidal magnetic field is given (usually a fixed position s
  real(ids_real)  :: r0_error_upper=ids_real_invalid
  real(ids_real)  :: r0_error_lower=ids_real_invalid
  integer(ids_int) :: r0_error_index=ids_int_invalid

  real(ids_real),pointer  :: b0(:) => null()     ! /b0 - Vacuum toroidal field at R0 [T]; Positive sign means anti-clockwise when viewed from above. The prod
  real(ids_real),pointer  :: b0_error_upper(:) => null()
  real(ids_real),pointer  :: b0_error_lower(:) => null()
  integer(ids_int) :: b0_error_index=ids_int_invalid

endtype

type ids_b_tor_vacuum_2  !    Characteristics of the vacuum toroidal field. time assumed to be two levels above
  real(ids_real)  :: r0=ids_real_invalid       ! /r0 - Reference major radius where the vacuum toroidal magnetic field is given (usually a fixed position s
  real(ids_real)  :: r0_error_upper=ids_real_invalid
  real(ids_real)  :: r0_error_lower=ids_real_invalid
  integer(ids_int) :: r0_error_index=ids_int_invalid

  real(ids_real),pointer  :: b0(:) => null()     ! /b0 - Vacuum toroidal field at b0. Positive sign means anti-clockwise when viewed from above. The product
  real(ids_real),pointer  :: b0_error_upper(:) => null()
  real(ids_real),pointer  :: b0_error_lower(:) => null()
  integer(ids_int) :: b0_error_index=ids_int_invalid

endtype

type ids_b_tor_vacuum_aos3  !    Characteristics of the vacuum toroidal field, dynamic within a type 3 AoS
  real(ids_real)  :: r0=ids_real_invalid       ! /r0 - Reference major radius where the vacuum toroidal magnetic field is given (usually a fixed position s
  real(ids_real)  :: r0_error_upper=ids_real_invalid
  real(ids_real)  :: r0_error_lower=ids_real_invalid
  integer(ids_int) :: r0_error_index=ids_int_invalid

  real(ids_real)  :: b0=ids_real_invalid       ! /b0 - Vacuum toroidal field at b0. Positive sign means anti-clockwise when viewed from above. The product
  real(ids_real)  :: b0_error_upper=ids_real_invalid
  real(ids_real)  :: b0_error_lower=ids_real_invalid
  integer(ids_int) :: b0_error_index=ids_int_invalid

endtype

type ids_core_profiles_vector_components_1  !    Vector components in predefined directions for 1D profiles, assuming core_radial_grid one level above
  real(ids_real),pointer  :: radial(:) => null()     ! /radial - Radial component
  real(ids_real),pointer  :: radial_error_upper(:) => null()
  real(ids_real),pointer  :: radial_error_lower(:) => null()
  integer(ids_int) :: radial_error_index=ids_int_invalid

  real(ids_real),pointer  :: diamagnetic(:) => null()     ! /diamagnetic - Diamagnetic component
  real(ids_real),pointer  :: diamagnetic_error_upper(:) => null()
  real(ids_real),pointer  :: diamagnetic_error_lower(:) => null()
  integer(ids_int) :: diamagnetic_error_index=ids_int_invalid

  real(ids_real),pointer  :: parallel(:) => null()     ! /parallel - Parallel component
  real(ids_real),pointer  :: parallel_error_upper(:) => null()
  real(ids_real),pointer  :: parallel_error_lower(:) => null()
  integer(ids_int) :: parallel_error_index=ids_int_invalid

  real(ids_real),pointer  :: poloidal(:) => null()     ! /poloidal - Poloidal component
  real(ids_real),pointer  :: poloidal_error_upper(:) => null()
  real(ids_real),pointer  :: poloidal_error_lower(:) => null()
  integer(ids_int) :: poloidal_error_index=ids_int_invalid

  real(ids_real),pointer  :: toroidal(:) => null()     ! /toroidal - Toroidal component
  real(ids_real),pointer  :: toroidal_error_upper(:) => null()
  real(ids_real),pointer  :: toroidal_error_lower(:) => null()
  integer(ids_int) :: toroidal_error_index=ids_int_invalid

endtype

type ids_core_profiles_vector_components_2  !    Vector components in predefined directions for 1D profiles, assuming core_radial_grid two levels above
  real(ids_real),pointer  :: radial(:) => null()     ! /radial - Radial component
  real(ids_real),pointer  :: radial_error_upper(:) => null()
  real(ids_real),pointer  :: radial_error_lower(:) => null()
  integer(ids_int) :: radial_error_index=ids_int_invalid

  real(ids_real),pointer  :: diamagnetic(:) => null()     ! /diamagnetic - Diamagnetic component
  real(ids_real),pointer  :: diamagnetic_error_upper(:) => null()
  real(ids_real),pointer  :: diamagnetic_error_lower(:) => null()
  integer(ids_int) :: diamagnetic_error_index=ids_int_invalid

  real(ids_real),pointer  :: parallel(:) => null()     ! /parallel - Parallel component
  real(ids_real),pointer  :: parallel_error_upper(:) => null()
  real(ids_real),pointer  :: parallel_error_lower(:) => null()
  integer(ids_int) :: parallel_error_index=ids_int_invalid

  real(ids_real),pointer  :: poloidal(:) => null()     ! /poloidal - Poloidal component
  real(ids_real),pointer  :: poloidal_error_upper(:) => null()
  real(ids_real),pointer  :: poloidal_error_lower(:) => null()
  integer(ids_int) :: poloidal_error_index=ids_int_invalid

  real(ids_real),pointer  :: toroidal(:) => null()     ! /toroidal - Toroidal component
  real(ids_real),pointer  :: toroidal_error_upper(:) => null()
  real(ids_real),pointer  :: toroidal_error_lower(:) => null()
  integer(ids_int) :: toroidal_error_index=ids_int_invalid

endtype

type ids_core_profiles_vector_components_3  !    Vector components in predefined directions for 1D profiles, assuming core_radial_grid 3 levels above
  real(ids_real),pointer  :: radial(:) => null()     ! /radial - Radial component
  real(ids_real),pointer  :: radial_error_upper(:) => null()
  real(ids_real),pointer  :: radial_error_lower(:) => null()
  integer(ids_int) :: radial_error_index=ids_int_invalid

  real(ids_real),pointer  :: diamagnetic(:) => null()     ! /diamagnetic - Diamagnetic component
  real(ids_real),pointer  :: diamagnetic_error_upper(:) => null()
  real(ids_real),pointer  :: diamagnetic_error_lower(:) => null()
  integer(ids_int) :: diamagnetic_error_index=ids_int_invalid

  real(ids_real),pointer  :: parallel(:) => null()     ! /parallel - Parallel component
  real(ids_real),pointer  :: parallel_error_upper(:) => null()
  real(ids_real),pointer  :: parallel_error_lower(:) => null()
  integer(ids_int) :: parallel_error_index=ids_int_invalid

  real(ids_real),pointer  :: poloidal(:) => null()     ! /poloidal - Poloidal component
  real(ids_real),pointer  :: poloidal_error_upper(:) => null()
  real(ids_real),pointer  :: poloidal_error_lower(:) => null()
  integer(ids_int) :: poloidal_error_index=ids_int_invalid

  real(ids_real),pointer  :: toroidal(:) => null()     ! /toroidal - Toroidal component
  real(ids_real),pointer  :: toroidal_error_upper(:) => null()
  real(ids_real),pointer  :: toroidal_error_lower(:) => null()
  integer(ids_int) :: toroidal_error_index=ids_int_invalid

endtype

type ids_core_radial_grid  !    1D radial grid for core* IDSs
  real(ids_real),pointer  :: rho_tor_norm(:) => null()     ! /rho_tor_norm - Normalised toroidal flux coordinate. The normalizing value for rho_tor_norm, is the toroidal flux co
  real(ids_real),pointer  :: rho_tor_norm_error_upper(:) => null()
  real(ids_real),pointer  :: rho_tor_norm_error_lower(:) => null()
  integer(ids_int) :: rho_tor_norm_error_index=ids_int_invalid

  real(ids_real),pointer  :: rho_tor(:) => null()     ! /rho_tor - Toroidal flux coordinate. rho_tor = sqrt(b_flux_tor/(pi*b0)) ~ sqrt(pi*r^2*b0/(pi*b0)) ~ r [m]. The
  real(ids_real),pointer  :: rho_tor_error_upper(:) => null()
  real(ids_real),pointer  :: rho_tor_error_lower(:) => null()
  integer(ids_int) :: rho_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: psi(:) => null()     ! /psi - Poloidal magnetic flux
  real(ids_real),pointer  :: psi_error_upper(:) => null()
  real(ids_real),pointer  :: psi_error_lower(:) => null()
  integer(ids_int) :: psi_error_index=ids_int_invalid

  real(ids_real),pointer  :: volume(:) => null()     ! /volume - Volume enclosed inside the magnetic surface
  real(ids_real),pointer  :: volume_error_upper(:) => null()
  real(ids_real),pointer  :: volume_error_lower(:) => null()
  integer(ids_int) :: volume_error_index=ids_int_invalid

  real(ids_real),pointer  :: area(:) => null()     ! /area - Cross-sectional area of the flux surface
  real(ids_real),pointer  :: area_error_upper(:) => null()
  real(ids_real),pointer  :: area_error_lower(:) => null()
  integer(ids_int) :: area_error_index=ids_int_invalid

endtype

type ids_core_profiles_ions_charge_states2  !    Quantities related to the a given state of the ion species
  real(ids_real)  :: z_min=ids_real_invalid       ! /z_min - Minimum Z of the charge state bundle
  real(ids_real)  :: z_min_error_upper=ids_real_invalid
  real(ids_real)  :: z_min_error_lower=ids_real_invalid
  integer(ids_int) :: z_min_error_index=ids_int_invalid

  real(ids_real)  :: z_max=ids_real_invalid       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(ids_real)  :: z_max_error_upper=ids_real_invalid
  real(ids_real)  :: z_max_error_lower=ids_real_invalid
  integer(ids_int) :: z_max_error_index=ids_int_invalid

  real(ids_real)  :: z_average=ids_real_invalid       ! /z_average - Average Z of the charge state bundle (equal to z_min if no bundle), = sum (Z*x_z) where x_z is the r
  real(ids_real)  :: z_average_error_upper=ids_real_invalid
  real(ids_real)  :: z_average_error_lower=ids_real_invalid
  integer(ids_int) :: z_average_error_index=ids_int_invalid

  real(ids_real)  :: z_square_average=ids_real_invalid       ! /z_square_average - Average Z square of the charge state bundle (equal to z_min if no bundle), = sum (Z^2*x_z) where x_z
  real(ids_real)  :: z_square_average_error_upper=ids_real_invalid
  real(ids_real)  :: z_square_average_error_lower=ids_real_invalid
  integer(ids_int) :: z_square_average_error_index=ids_int_invalid

  real(ids_real)  :: ionisation_potential=ids_real_invalid       ! /ionisation_potential - Cumulative and average ionisation potential to reach a given bundle. Defined as sum (x_z* (sum of Ep
  real(ids_real)  :: ionisation_potential_error_upper=ids_real_invalid
  real(ids_real)  :: ionisation_potential_error_lower=ids_real_invalid
  integer(ids_int) :: ionisation_potential_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  type (ids_core_profiles_vector_components_3) :: velocity  ! /velocity - Velocity
  real(ids_real),pointer  :: temperature(:) => null()     ! /temperature - Temperature
  real(ids_real),pointer  :: temperature_error_upper(:) => null()
  real(ids_real),pointer  :: temperature_error_lower(:) => null()
  integer(ids_int) :: temperature_error_index=ids_int_invalid

  real(ids_real),pointer  :: density(:) => null()     ! /density - Density (thermal+non-thermal)
  real(ids_real),pointer  :: density_error_upper(:) => null()
  real(ids_real),pointer  :: density_error_lower(:) => null()
  integer(ids_int) :: density_error_index=ids_int_invalid

  real(ids_real),pointer  :: density_thermal(:) => null()     ! /density_thermal - Density of thermal particles
  real(ids_real),pointer  :: density_thermal_error_upper(:) => null()
  real(ids_real),pointer  :: density_thermal_error_lower(:) => null()
  integer(ids_int) :: density_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: density_fast(:) => null()     ! /density_fast - Density of fast (non-thermal) particles
  real(ids_real),pointer  :: density_fast_error_upper(:) => null()
  real(ids_real),pointer  :: density_fast_error_lower(:) => null()
  integer(ids_int) :: density_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure(:) => null()     ! /pressure - Pressure (thermal+non-thermal)
  real(ids_real),pointer  :: pressure_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_error_lower(:) => null()
  integer(ids_int) :: pressure_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_thermal(:) => null()     ! /pressure_thermal - Pressure (thermal) associated with random motion ~average((v-average(v))^2)
  real(ids_real),pointer  :: pressure_thermal_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_thermal_error_lower(:) => null()
  integer(ids_int) :: pressure_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_fast_perpendicular(:) => null()     ! /pressure_fast_perpendicular - Fast (non-thermal) perpendicular pressure
  real(ids_real),pointer  :: pressure_fast_perpendicular_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_fast_perpendicular_error_lower(:) => null()
  integer(ids_int) :: pressure_fast_perpendicular_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_fast_parallel(:) => null()     ! /pressure_fast_parallel - Fast (non-thermal) parallel pressure
  real(ids_real),pointer  :: pressure_fast_parallel_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_fast_parallel_error_lower(:) => null()
  integer(ids_int) :: pressure_fast_parallel_error_index=ids_int_invalid

endtype

type ids_core_profile_ions  !    Quantities related to a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(ids_real)  :: z_ion=ids_real_invalid       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed), volume averaged over plasma
  real(ids_real)  :: z_ion_error_upper=ids_real_invalid
  real(ids_real)  :: z_ion_error_lower=ids_real_invalid
  integer(ids_int) :: z_ion_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  integer(ids_int)  :: neutral_index=ids_int_invalid       ! /neutral_index - Index of the corresponding neutral species in the ../../neutral array
  real(ids_real),pointer  :: z_ion_1d(:) => null()     ! /z_ion_1d - Average charge of the ion species (sum of states charge weighted by state density and divided by ion
  real(ids_real),pointer  :: z_ion_1d_error_upper(:) => null()
  real(ids_real),pointer  :: z_ion_1d_error_lower(:) => null()
  integer(ids_int) :: z_ion_1d_error_index=ids_int_invalid

  real(ids_real),pointer  :: z_ion_square_1d(:) => null()     ! /z_ion_square_1d - Average sqare charge of the ion species (sum of states square charge weighted by state density and d
  real(ids_real),pointer  :: z_ion_square_1d_error_upper(:) => null()
  real(ids_real),pointer  :: z_ion_square_1d_error_lower(:) => null()
  integer(ids_int) :: z_ion_square_1d_error_index=ids_int_invalid

  real(ids_real),pointer  :: temperature(:) => null()     ! /temperature - Temperature (average over charge states when multiple charge states are considered)
  real(ids_real),pointer  :: temperature_error_upper(:) => null()
  real(ids_real),pointer  :: temperature_error_lower(:) => null()
  integer(ids_int) :: temperature_error_index=ids_int_invalid

  real(ids_real),pointer  :: density(:) => null()     ! /density - Density (thermal+non-thermal) (sum over charge states when multiple charge states are considered)
  real(ids_real),pointer  :: density_error_upper(:) => null()
  real(ids_real),pointer  :: density_error_lower(:) => null()
  integer(ids_int) :: density_error_index=ids_int_invalid

  real(ids_real),pointer  :: density_thermal(:) => null()     ! /density_thermal - Density (thermal) (sum over charge states when multiple charge states are considered)
  real(ids_real),pointer  :: density_thermal_error_upper(:) => null()
  real(ids_real),pointer  :: density_thermal_error_lower(:) => null()
  integer(ids_int) :: density_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: density_fast(:) => null()     ! /density_fast - Density of fast (non-thermal) particles (sum over charge states when multiple charge states are cons
  real(ids_real),pointer  :: density_fast_error_upper(:) => null()
  real(ids_real),pointer  :: density_fast_error_lower(:) => null()
  integer(ids_int) :: density_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure(:) => null()     ! /pressure - Pressure (thermal+non-thermal) (sum over charge states when multiple charge states are considered)
  real(ids_real),pointer  :: pressure_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_error_lower(:) => null()
  integer(ids_int) :: pressure_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_thermal(:) => null()     ! /pressure_thermal - Pressure (thermal) associated with random motion ~average((v-average(v))^2) (sum over charge states
  real(ids_real),pointer  :: pressure_thermal_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_thermal_error_lower(:) => null()
  integer(ids_int) :: pressure_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_fast_perpendicular(:) => null()     ! /pressure_fast_perpendicular - Fast (non-thermal) perpendicular pressure  (sum over charge states when multiple charge states are c
  real(ids_real),pointer  :: pressure_fast_perpendicular_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_fast_perpendicular_error_lower(:) => null()
  integer(ids_int) :: pressure_fast_perpendicular_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_fast_parallel(:) => null()     ! /pressure_fast_parallel - Fast (non-thermal) parallel pressure  (sum over charge states when multiple charge states are consid
  real(ids_real),pointer  :: pressure_fast_parallel_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_fast_parallel_error_lower(:) => null()
  integer(ids_int) :: pressure_fast_parallel_error_index=ids_int_invalid

  real(ids_real),pointer  :: velocity_tor(:) => null()     ! /velocity_tor - Toroidal velocity (average over charge states when multiple charge states are considered)
  real(ids_real),pointer  :: velocity_tor_error_upper(:) => null()
  real(ids_real),pointer  :: velocity_tor_error_lower(:) => null()
  integer(ids_int) :: velocity_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: velocity_pol(:) => null()     ! /velocity_pol - Poloidal velocity (average over charge states when multiple charge states are considered)
  real(ids_real),pointer  :: velocity_pol_error_upper(:) => null()
  real(ids_real),pointer  :: velocity_pol_error_lower(:) => null()
  integer(ids_int) :: velocity_pol_error_index=ids_int_invalid

  type (ids_core_profiles_vector_components_2) :: velocity  ! /velocity - Velocity (average over charge states when multiple charge states are considered)
  integer(ids_int)  :: multiple_states_flag=ids_int_invalid       ! /multiple_states_flag - Multiple states calculation flag : 0-Only one state is considered; 1-Multiple states are considered
  type (ids_core_profiles_ions_charge_states2),pointer :: state(:) => null()  ! /state(i) - Quantities related to the different states of the species (ionisation, energy, excitation, ...)
endtype

type ids_core_profiles_neutral_state  !    Quantities related to the a given state of the neutral species
  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying state
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  type (ids_identifier) :: neutral_type  ! /neutral_type - Neutral type (if the considered state is a neutral), in terms of energy. ID =1: cold; 2: thermal; 3:
  type (ids_core_profiles_vector_components_3) :: velocity  ! /velocity - Velocity
  real(ids_real),pointer  :: temperature(:) => null()     ! /temperature - Temperature
  real(ids_real),pointer  :: temperature_error_upper(:) => null()
  real(ids_real),pointer  :: temperature_error_lower(:) => null()
  integer(ids_int) :: temperature_error_index=ids_int_invalid

  real(ids_real),pointer  :: density(:) => null()     ! /density - Density (thermal+non-thermal)
  real(ids_real),pointer  :: density_error_upper(:) => null()
  real(ids_real),pointer  :: density_error_lower(:) => null()
  integer(ids_int) :: density_error_index=ids_int_invalid

  real(ids_real),pointer  :: density_thermal(:) => null()     ! /density_thermal - Density of thermal particles
  real(ids_real),pointer  :: density_thermal_error_upper(:) => null()
  real(ids_real),pointer  :: density_thermal_error_lower(:) => null()
  integer(ids_int) :: density_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: density_fast(:) => null()     ! /density_fast - Density of fast (non-thermal) particles
  real(ids_real),pointer  :: density_fast_error_upper(:) => null()
  real(ids_real),pointer  :: density_fast_error_lower(:) => null()
  integer(ids_int) :: density_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure(:) => null()     ! /pressure - Pressure (thermal+non-thermal)
  real(ids_real),pointer  :: pressure_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_error_lower(:) => null()
  integer(ids_int) :: pressure_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_thermal(:) => null()     ! /pressure_thermal - Pressure (thermal) associated with random motion ~average((v-average(v))^2)
  real(ids_real),pointer  :: pressure_thermal_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_thermal_error_lower(:) => null()
  integer(ids_int) :: pressure_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_fast_perpendicular(:) => null()     ! /pressure_fast_perpendicular - Fast (non-thermal) perpendicular pressure
  real(ids_real),pointer  :: pressure_fast_perpendicular_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_fast_perpendicular_error_lower(:) => null()
  integer(ids_int) :: pressure_fast_perpendicular_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_fast_parallel(:) => null()     ! /pressure_fast_parallel - Fast (non-thermal) parallel pressure
  real(ids_real),pointer  :: pressure_fast_parallel_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_fast_parallel_error_lower(:) => null()
  integer(ids_int) :: pressure_fast_parallel_error_index=ids_int_invalid

endtype

type ids_core_profile_neutral  !    Quantities related to a given neutral species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H, D, T, He, C, D2, DT, CD4, ...)
  integer(ids_int)  :: ion_index=ids_int_invalid       ! /ion_index - Index of the corresponding ion species in the ../../ion array
  real(ids_real),pointer  :: temperature(:) => null()     ! /temperature - Temperature (average over charge states when multiple charge states are considered)
  real(ids_real),pointer  :: temperature_error_upper(:) => null()
  real(ids_real),pointer  :: temperature_error_lower(:) => null()
  integer(ids_int) :: temperature_error_index=ids_int_invalid

  real(ids_real),pointer  :: density(:) => null()     ! /density - Density (thermal+non-thermal) (sum over charge states when multiple charge states are considered)
  real(ids_real),pointer  :: density_error_upper(:) => null()
  real(ids_real),pointer  :: density_error_lower(:) => null()
  integer(ids_int) :: density_error_index=ids_int_invalid

  real(ids_real),pointer  :: density_thermal(:) => null()     ! /density_thermal - Density (thermal) (sum over charge states when multiple charge states are considered)
  real(ids_real),pointer  :: density_thermal_error_upper(:) => null()
  real(ids_real),pointer  :: density_thermal_error_lower(:) => null()
  integer(ids_int) :: density_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: density_fast(:) => null()     ! /density_fast - Density of fast (non-thermal) particles (sum over charge states when multiple charge states are cons
  real(ids_real),pointer  :: density_fast_error_upper(:) => null()
  real(ids_real),pointer  :: density_fast_error_lower(:) => null()
  integer(ids_int) :: density_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure(:) => null()     ! /pressure - Pressure (thermal+non-thermal) (sum over charge states when multiple charge states are considered)
  real(ids_real),pointer  :: pressure_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_error_lower(:) => null()
  integer(ids_int) :: pressure_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_thermal(:) => null()     ! /pressure_thermal - Pressure (thermal) associated with random motion ~average((v-average(v))^2) (sum over charge states
  real(ids_real),pointer  :: pressure_thermal_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_thermal_error_lower(:) => null()
  integer(ids_int) :: pressure_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_fast_perpendicular(:) => null()     ! /pressure_fast_perpendicular - Fast (non-thermal) perpendicular pressure  (sum over charge states when multiple charge states are c
  real(ids_real),pointer  :: pressure_fast_perpendicular_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_fast_perpendicular_error_lower(:) => null()
  integer(ids_int) :: pressure_fast_perpendicular_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_fast_parallel(:) => null()     ! /pressure_fast_parallel - Fast (non-thermal) parallel pressure  (sum over charge states when multiple charge states are consid
  real(ids_real),pointer  :: pressure_fast_parallel_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_fast_parallel_error_lower(:) => null()
  integer(ids_int) :: pressure_fast_parallel_error_index=ids_int_invalid

  type (ids_core_profiles_vector_components_2) :: velocity  ! /velocity - Velocity (average over charge states when multiple charge states are considered)
  integer(ids_int)  :: multiple_states_flag=ids_int_invalid       ! /multiple_states_flag - Multiple states calculation flag : 0-Only one state is considered; 1-Multiple states are considered
  type (ids_core_profiles_neutral_state),pointer :: state(:) => null()  ! /state(i) - Quantities related to the different states of the species (energy, excitation, ...)
endtype

type ids_core_profiles_profiles_1d_electrons  !    Quantities related to electrons
  real(ids_real),pointer  :: temperature(:) => null()     ! /temperature - Temperature
  real(ids_real),pointer  :: temperature_error_upper(:) => null()
  real(ids_real),pointer  :: temperature_error_lower(:) => null()
  integer(ids_int) :: temperature_error_index=ids_int_invalid

  real(ids_real),pointer  :: density(:) => null()     ! /density - Density (thermal+non-thermal)
  real(ids_real),pointer  :: density_error_upper(:) => null()
  real(ids_real),pointer  :: density_error_lower(:) => null()
  integer(ids_int) :: density_error_index=ids_int_invalid

  real(ids_real),pointer  :: density_thermal(:) => null()     ! /density_thermal - Density of thermal particles
  real(ids_real),pointer  :: density_thermal_error_upper(:) => null()
  real(ids_real),pointer  :: density_thermal_error_lower(:) => null()
  integer(ids_int) :: density_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: density_fast(:) => null()     ! /density_fast - Density of fast (non-thermal) particles
  real(ids_real),pointer  :: density_fast_error_upper(:) => null()
  real(ids_real),pointer  :: density_fast_error_lower(:) => null()
  integer(ids_int) :: density_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure(:) => null()     ! /pressure - Pressure (thermal+non-thermal)
  real(ids_real),pointer  :: pressure_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_error_lower(:) => null()
  integer(ids_int) :: pressure_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_thermal(:) => null()     ! /pressure_thermal - Pressure (thermal) associated with random motion ~average((v-average(v))^2)
  real(ids_real),pointer  :: pressure_thermal_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_thermal_error_lower(:) => null()
  integer(ids_int) :: pressure_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_fast_perpendicular(:) => null()     ! /pressure_fast_perpendicular - Fast (non-thermal) perpendicular pressure
  real(ids_real),pointer  :: pressure_fast_perpendicular_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_fast_perpendicular_error_lower(:) => null()
  integer(ids_int) :: pressure_fast_perpendicular_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_fast_parallel(:) => null()     ! /pressure_fast_parallel - Fast (non-thermal) parallel pressure
  real(ids_real),pointer  :: pressure_fast_parallel_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_fast_parallel_error_lower(:) => null()
  integer(ids_int) :: pressure_fast_parallel_error_index=ids_int_invalid

  real(ids_real),pointer  :: velocity_tor(:) => null()     ! /velocity_tor - Toroidal velocity
  real(ids_real),pointer  :: velocity_tor_error_upper(:) => null()
  real(ids_real),pointer  :: velocity_tor_error_lower(:) => null()
  integer(ids_int) :: velocity_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: velocity_pol(:) => null()     ! /velocity_pol - Poloidal velocity
  real(ids_real),pointer  :: velocity_pol_error_upper(:) => null()
  real(ids_real),pointer  :: velocity_pol_error_lower(:) => null()
  integer(ids_int) :: velocity_pol_error_index=ids_int_invalid

  type (ids_core_profiles_vector_components_2) :: velocity  ! /velocity - Velocity
  real(ids_real),pointer  :: collisionality_norm(:) => null()     ! /collisionality_norm - Collisionality normalised to the bounce frequency
  real(ids_real),pointer  :: collisionality_norm_error_upper(:) => null()
  real(ids_real),pointer  :: collisionality_norm_error_lower(:) => null()
  integer(ids_int) :: collisionality_norm_error_index=ids_int_invalid

endtype

type ids_core_profiles_profiles_1d  !    1D radial profiles for core and edge
  type (ids_core_radial_grid) :: grid  ! /grid - Radial grid
  type (ids_core_profiles_profiles_1d_electrons) :: electrons  ! /electrons - Quantities related to the electrons
  type (ids_core_profile_ions),pointer :: ion(:) => null()  ! /ion(i) - Quantities related to the different ion species
  type (ids_core_profile_neutral),pointer :: neutral(:) => null()  ! /neutral(i) - Quantities related to the different neutral species
  real(ids_real),pointer  :: t_i_average(:) => null()     ! /t_i_average - Ion temperature (averaged on charge states and ion species)
  real(ids_real),pointer  :: t_i_average_error_upper(:) => null()
  real(ids_real),pointer  :: t_i_average_error_lower(:) => null()
  integer(ids_int) :: t_i_average_error_index=ids_int_invalid

  real(ids_real),pointer  :: n_i_total_over_n_e(:) => null()     ! /n_i_total_over_n_e - Ratio of total ion density (sum over species and charge states) over electron density. (thermal+non-
  real(ids_real),pointer  :: n_i_total_over_n_e_error_upper(:) => null()
  real(ids_real),pointer  :: n_i_total_over_n_e_error_lower(:) => null()
  integer(ids_int) :: n_i_total_over_n_e_error_index=ids_int_invalid

  real(ids_real),pointer  :: n_i_thermal_total(:) => null()     ! /n_i_thermal_total - Total ion thermal density (sum over species and charge states)
  real(ids_real),pointer  :: n_i_thermal_total_error_upper(:) => null()
  real(ids_real),pointer  :: n_i_thermal_total_error_lower(:) => null()
  integer(ids_int) :: n_i_thermal_total_error_index=ids_int_invalid

  real(ids_real),pointer  :: momentum_tor(:) => null()     ! /momentum_tor - Total plasma toroidal momentum, summed over ion species and electrons weighted by their density and
  real(ids_real),pointer  :: momentum_tor_error_upper(:) => null()
  real(ids_real),pointer  :: momentum_tor_error_lower(:) => null()
  integer(ids_int) :: momentum_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: zeff(:) => null()     ! /zeff - Effective charge
  real(ids_real),pointer  :: zeff_error_upper(:) => null()
  real(ids_real),pointer  :: zeff_error_lower(:) => null()
  integer(ids_int) :: zeff_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_ion_total(:) => null()     ! /pressure_ion_total - Total (sum over ion species) thermal ion pressure
  real(ids_real),pointer  :: pressure_ion_total_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_ion_total_error_lower(:) => null()
  integer(ids_int) :: pressure_ion_total_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_thermal(:) => null()     ! /pressure_thermal - Thermal pressure (electrons+ions)
  real(ids_real),pointer  :: pressure_thermal_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_thermal_error_lower(:) => null()
  integer(ids_int) :: pressure_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_perpendicular(:) => null()     ! /pressure_perpendicular - Total perpendicular pressure (electrons+ions, thermal+non-thermal)
  real(ids_real),pointer  :: pressure_perpendicular_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_perpendicular_error_lower(:) => null()
  integer(ids_int) :: pressure_perpendicular_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_parallel(:) => null()     ! /pressure_parallel - Total parallel pressure (electrons+ions, thermal+non-thermal)
  real(ids_real),pointer  :: pressure_parallel_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_parallel_error_lower(:) => null()
  integer(ids_int) :: pressure_parallel_error_index=ids_int_invalid

  real(ids_real),pointer  :: j_total(:) => null()     ! /j_total - Total parallel current density = average(jtot.B) / B0, where B0 = Core_Profiles/Vacuum_Toroidal_Fiel
  real(ids_real),pointer  :: j_total_error_upper(:) => null()
  real(ids_real),pointer  :: j_total_error_lower(:) => null()
  integer(ids_int) :: j_total_error_index=ids_int_invalid

  real(ids_real),pointer  :: current_parallel_inside(:) => null()     ! /current_parallel_inside - Parallel current driven inside the flux surface. Cumulative surface integral of j_total
  real(ids_real),pointer  :: current_parallel_inside_error_upper(:) => null()
  real(ids_real),pointer  :: current_parallel_inside_error_lower(:) => null()
  integer(ids_int) :: current_parallel_inside_error_index=ids_int_invalid

  real(ids_real),pointer  :: j_tor(:) => null()     ! /j_tor - Total toroidal current density = average(J_Tor/R) / average(1/R)
  real(ids_real),pointer  :: j_tor_error_upper(:) => null()
  real(ids_real),pointer  :: j_tor_error_lower(:) => null()
  integer(ids_int) :: j_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: j_ohmic(:) => null()     ! /j_ohmic - Ohmic parallel current density = average(J_Ohmic.B) / B0, where B0 = Core_Profiles/Vacuum_Toroidal_F
  real(ids_real),pointer  :: j_ohmic_error_upper(:) => null()
  real(ids_real),pointer  :: j_ohmic_error_lower(:) => null()
  integer(ids_int) :: j_ohmic_error_index=ids_int_invalid

  real(ids_real),pointer  :: j_non_inductive(:) => null()     ! /j_non_inductive - Non-inductive (includes bootstrap) parallel current density = average(jni.B) / B0, where B0 = Core_P
  real(ids_real),pointer  :: j_non_inductive_error_upper(:) => null()
  real(ids_real),pointer  :: j_non_inductive_error_lower(:) => null()
  integer(ids_int) :: j_non_inductive_error_index=ids_int_invalid

  real(ids_real),pointer  :: j_bootstrap(:) => null()     ! /j_bootstrap - Bootstrap current density = average(J_Bootstrap.B) / B0, where B0 = Core_Profiles/Vacuum_Toroidal_Fi
  real(ids_real),pointer  :: j_bootstrap_error_upper(:) => null()
  real(ids_real),pointer  :: j_bootstrap_error_lower(:) => null()
  integer(ids_int) :: j_bootstrap_error_index=ids_int_invalid

  real(ids_real),pointer  :: conductivity_parallel(:) => null()     ! /conductivity_parallel - Parallel conductivity
  real(ids_real),pointer  :: conductivity_parallel_error_upper(:) => null()
  real(ids_real),pointer  :: conductivity_parallel_error_lower(:) => null()
  integer(ids_int) :: conductivity_parallel_error_index=ids_int_invalid

  real(ids_real),pointer  :: e_field_parallel(:) => null()     ! /e_field_parallel - Parallel electric field = average(E.B) / B0, where Core_Profiles/Vacuum_Toroidal_Field/ B0
  real(ids_real),pointer  :: e_field_parallel_error_upper(:) => null()
  real(ids_real),pointer  :: e_field_parallel_error_lower(:) => null()
  integer(ids_int) :: e_field_parallel_error_index=ids_int_invalid

  type (ids_core_profiles_vector_components_1) :: e_field  ! /e_field - Electric field, averaged on the magnetic surface. E.g for the parallel component, average(E.B) / B0,
  real(ids_real),pointer  :: q(:) => null()     ! /q - Safety factor
  real(ids_real),pointer  :: q_error_upper(:) => null()
  real(ids_real),pointer  :: q_error_lower(:) => null()
  integer(ids_int) :: q_error_index=ids_int_invalid

  real(ids_real),pointer  :: magnetic_shear(:) => null()     ! /magnetic_shear - Magnetic shear, defined as rho_tor/q . dq/drho_tor
  real(ids_real),pointer  :: magnetic_shear_error_upper(:) => null()
  real(ids_real),pointer  :: magnetic_shear_error_lower(:) => null()
  integer(ids_int) :: magnetic_shear_error_index=ids_int_invalid

  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_detector_energy_band  !    Detector energy band
  real(ids_real)  :: lower_bound=ids_real_invalid       ! /lower_bound - Lower bound of the energy band
  real(ids_real)  :: lower_bound_error_upper=ids_real_invalid
  real(ids_real)  :: lower_bound_error_lower=ids_real_invalid
  integer(ids_int) :: lower_bound_error_index=ids_int_invalid

  real(ids_real)  :: upper_bound=ids_real_invalid       ! /upper_bound - Upper bound of the energy band
  real(ids_real)  :: upper_bound_error_upper=ids_real_invalid
  real(ids_real)  :: upper_bound_error_lower=ids_real_invalid
  integer(ids_int) :: upper_bound_error_index=ids_int_invalid

endtype

type ids_distribution_markers  !    Test particles for a given time slice
  type (ids_identifier_dynamic_aos3),pointer :: coordinate_identifier(:) => null()  ! /coordinate_identifier(i) - Set of coordinate identifiers, coordinates on which the markers are represented
  real(ids_real),pointer  :: weights(:) => null()     ! /weights - Weight of the markers, i.e. number of real particles represented by each marker. The dimension of th
  real(ids_real),pointer  :: weights_error_upper(:) => null()
  real(ids_real),pointer  :: weights_error_lower(:) => null()
  integer(ids_int) :: weights_error_index=ids_int_invalid

  real(ids_real),pointer  :: positions(:,:) => null()     ! /positions - Position of the markers in the set of coordinates. The first dimension corresponds to the number of
  real(ids_real),pointer  :: positions_error_upper(:,:) => null()
  real(ids_real),pointer  :: positions_error_lower(:,:) => null()
  integer(ids_int) :: positions_error_index=ids_int_invalid

  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_distribution_species  !    Description of a species in a distribution function related IDS
  type (ids_identifier) :: type  ! /type - Species type. index=1 for electron; index=2 for ion species in a single/average state (refer to ion
  type (ids_plasma_composition_ions_constant) :: ion  ! /ion - Description of the ion or neutral species, used if type/index = 2 or 3
  type (ids_plasma_composition_neutral_constant) :: neutral  ! /neutral - Description of the neutral species, used if type/index = 4 or 5
endtype

type ids_distribution_process_identifier  !    Identifier an NBI or fusion reaction process intervening affecting a distribution function
  type (ids_identifier) :: type  ! /type - Process type. index=1 for NBI; index=2 for nuclear reaction (reaction unspecified); index=3 for nucl
  type (ids_identifier) :: reactant_energy  ! /reactant_energy - For nuclear reaction source, energy of the reactants. index = 0 for a sum over all energies; index =
  type (ids_identifier) :: nbi_energy  ! /nbi_energy - For NBI source, energy of the accelerated species considered. index = 0 for a sum over all energies;
  integer(ids_int)  :: nbi_unit=ids_int_invalid       ! /nbi_unit - Index of the NBI unit considered. Refers to the "unit" array of the NBI IDS. 0 means sum over all NB
  integer(ids_int)  :: nbi_beamlets_group=ids_int_invalid       ! /nbi_beamlets_group - Index of the NBI beamlets group considered. Refers to the "unit/beamlets_group" array of the NBI IDS
endtype

type ids_generic_grid_scalar_single_position  !    Scalar values at a single position on a generic grid (dynamic within a type 3 AoS)
  integer(ids_int)  :: grid_index=ids_int_invalid       ! /grid_index - Index of the grid used to represent this quantity
  integer(ids_int)  :: grid_subset_index=ids_int_invalid       ! /grid_subset_index - Index of the grid subset the data is provided on
  real(ids_real)  :: value=ids_real_invalid       ! /value - Scalar value of the quantity on the grid subset (corresponding to a single local position or to an i
  real(ids_real)  :: value_error_upper=ids_real_invalid
  real(ids_real)  :: value_error_lower=ids_real_invalid
  integer(ids_int) :: value_error_index=ids_int_invalid

endtype

type ids_generic_grid_scalar  !    Scalar values on a generic grid (dynamic within a type 3 AoS)
  integer(ids_int)  :: grid_index=ids_int_invalid       ! /grid_index - Index of the grid used to represent this quantity
  integer(ids_int)  :: grid_subset_index=ids_int_invalid       ! /grid_subset_index - Index of the grid subset the data is provided on
  real(ids_real),pointer  :: values(:) => null()     ! /values - One scalar value is provided per element in the grid subset.
  real(ids_real),pointer  :: values_error_upper(:) => null()
  real(ids_real),pointer  :: values_error_lower(:) => null()
  integer(ids_int) :: values_error_index=ids_int_invalid

  real(ids_real),pointer  :: coefficients(:,:) => null()     ! /coefficients - Interpolation coefficients, to be used for a high precision evaluation of the physical quantity with
  real(ids_real),pointer  :: coefficients_error_upper(:,:) => null()
  real(ids_real),pointer  :: coefficients_error_lower(:,:) => null()
  integer(ids_int) :: coefficients_error_index=ids_int_invalid

endtype

type ids_generic_grid_vector  !    Vector values on a generic grid (dynamic within a type 3 AoS)
  integer(ids_int)  :: grid_index=ids_int_invalid       ! /grid_index - Index of the grid used to represent this quantity
  integer(ids_int)  :: grid_subset_index=ids_int_invalid       ! /grid_subset_index - Index of the grid subset the data is provided on
  real(ids_real),pointer  :: values(:,:) => null()     ! /values - List of vector components, one list per element in the grid subset. First dimenstion: element index.
  real(ids_real),pointer  :: values_error_upper(:,:) => null()
  real(ids_real),pointer  :: values_error_lower(:,:) => null()
  integer(ids_int) :: values_error_index=ids_int_invalid

  real(ids_real),pointer  :: coefficients(:,:,:) => null()     ! /coefficients - Interpolation coefficients, to be used for a high precision evaluation of the physical quantity with
  real(ids_real),pointer  :: coefficients_error_upper(:,:,:) => null()
  real(ids_real),pointer  :: coefficients_error_lower(:,:,:) => null()
  integer(ids_int) :: coefficients_error_index=ids_int_invalid

endtype

type ids_generic_grid_vector_components  !    Vector components in predefined directions on a generic grid (dynamic within a type 3 AoS)
  integer(ids_int)  :: grid_index=ids_int_invalid       ! /grid_index - Index of the grid used to represent this quantity
  integer(ids_int)  :: grid_subset_index=ids_int_invalid       ! /grid_subset_index - Index of the grid subset the data is provided on
  real(ids_real),pointer  :: radial(:) => null()     ! /radial - Radial component, one scalar value is provided per element in the grid subset.
  real(ids_real),pointer  :: radial_error_upper(:) => null()
  real(ids_real),pointer  :: radial_error_lower(:) => null()
  integer(ids_int) :: radial_error_index=ids_int_invalid

  real(ids_real),pointer  :: radial_coefficients(:,:) => null()     ! /radial_coefficients - Interpolation coefficients for the radial component, to be used for a high precision evaluation of t
  real(ids_real),pointer  :: radial_coefficients_error_upper(:,:) => null()
  real(ids_real),pointer  :: radial_coefficients_error_lower(:,:) => null()
  integer(ids_int) :: radial_coefficients_error_index=ids_int_invalid

  real(ids_real),pointer  :: diamagnetic(:) => null()     ! /diamagnetic - Diamagnetic component, one scalar value is provided per element in the grid subset.
  real(ids_real),pointer  :: diamagnetic_error_upper(:) => null()
  real(ids_real),pointer  :: diamagnetic_error_lower(:) => null()
  integer(ids_int) :: diamagnetic_error_index=ids_int_invalid

  real(ids_real),pointer  :: diamagnetic_coefficients(:,:) => null()     ! /diamagnetic_coefficients - Interpolation coefficients for the diamagnetic component, to be used for a high precision evaluation
  real(ids_real),pointer  :: diamagnetic_coefficients_error_upper(:,:) => null()
  real(ids_real),pointer  :: diamagnetic_coefficients_error_lower(:,:) => null()
  integer(ids_int) :: diamagnetic_coefficients_error_index=ids_int_invalid

  real(ids_real),pointer  :: parallel(:) => null()     ! /parallel - Parallel component, one scalar value is provided per element in the grid subset.
  real(ids_real),pointer  :: parallel_error_upper(:) => null()
  real(ids_real),pointer  :: parallel_error_lower(:) => null()
  integer(ids_int) :: parallel_error_index=ids_int_invalid

  real(ids_real),pointer  :: parallel_coefficients(:,:) => null()     ! /parallel_coefficients - Interpolation coefficients for the parallel component, to be used for a high precision evaluation of
  real(ids_real),pointer  :: parallel_coefficients_error_upper(:,:) => null()
  real(ids_real),pointer  :: parallel_coefficients_error_lower(:,:) => null()
  integer(ids_int) :: parallel_coefficients_error_index=ids_int_invalid

  real(ids_real),pointer  :: poloidal(:) => null()     ! /poloidal - Poloidal component, one scalar value is provided per element in the grid subset.
  real(ids_real),pointer  :: poloidal_error_upper(:) => null()
  real(ids_real),pointer  :: poloidal_error_lower(:) => null()
  integer(ids_int) :: poloidal_error_index=ids_int_invalid

  real(ids_real),pointer  :: poloidal_coefficients(:,:) => null()     ! /poloidal_coefficients - Interpolation coefficients for the poloidal component, to be used for a high precision evaluation of
  real(ids_real),pointer  :: poloidal_coefficients_error_upper(:,:) => null()
  real(ids_real),pointer  :: poloidal_coefficients_error_lower(:,:) => null()
  integer(ids_int) :: poloidal_coefficients_error_index=ids_int_invalid

  real(ids_real),pointer  :: toroidal(:) => null()     ! /toroidal - Toroidal component, one scalar value is provided per element in the grid subset.
  real(ids_real),pointer  :: toroidal_error_upper(:) => null()
  real(ids_real),pointer  :: toroidal_error_lower(:) => null()
  integer(ids_int) :: toroidal_error_index=ids_int_invalid

  real(ids_real),pointer  :: toroidal_coefficients(:,:) => null()     ! /toroidal_coefficients - Interpolation coefficients for the toroidal component, to be used for a high precision evaluation of
  real(ids_real),pointer  :: toroidal_coefficients_error_upper(:,:) => null()
  real(ids_real),pointer  :: toroidal_coefficients_error_lower(:,:) => null()
  integer(ids_int) :: toroidal_coefficients_error_index=ids_int_invalid

endtype

type ids_generic_grid_matrix  !    Matrix values on a generic grid (dynamic within a type 3 AoS)
  integer(ids_int)  :: grid_index=ids_int_invalid       ! /grid_index - Index of the grid used to represent this quantity
  integer(ids_int)  :: grid_subset_index=ids_int_invalid       ! /grid_subset_index - Index of the grid subset the data is provided on
  real(ids_real),pointer  :: values(:,:,:) => null()     ! /values - List of matrix components, one list per element in the grid subset. First dimenstion: element index.
  real(ids_real),pointer  :: values_error_upper(:,:,:) => null()
  real(ids_real),pointer  :: values_error_lower(:,:,:) => null()
  integer(ids_int) :: values_error_index=ids_int_invalid

  real(ids_real),pointer  :: coefficients(:,:,:,:) => null()     ! /coefficients - Interpolation coefficients, to be used for a high precision evaluation of the physical quantity with
  real(ids_real),pointer  :: coefficients_error_upper(:,:,:,:) => null()
  real(ids_real),pointer  :: coefficients_error_lower(:,:,:,:) => null()
  integer(ids_int) :: coefficients_error_index=ids_int_invalid

endtype

type ids_generic_grid_dynamic_space_dimension_object_boundary  !    Generic grid, description of an object boundary and its neighbours (dynamic within a type 3 AoS)
  integer(ids_int)  :: index=ids_int_invalid       ! /index - Index of this (n-1)-dimensional boundary object
  integer(ids_int),pointer  :: neighbours(:) => null()      ! /neighbours - List of indices of the n-dimensional objects adjacent to the given n-dimensional object. An object c
endtype

type ids_generic_grid_dynamic_space_dimension_object  !    Generic grid, list of objects of a given dimension within a space (dynamic within a type 3 AoS)
  type (ids_generic_grid_dynamic_space_dimension_object_boundary),pointer :: boundary(:) => null()  ! /boundary(i) - Set of  (n-1)-dimensional objects defining the boundary of this n-dimensional object
  real(ids_real),pointer  :: geometry(:) => null()     ! /geometry - Geometry data associated with the object. Its dimension depends on the type of object, geometry and
  real(ids_real),pointer  :: geometry_error_upper(:) => null()
  real(ids_real),pointer  :: geometry_error_lower(:) => null()
  integer(ids_int) :: geometry_error_index=ids_int_invalid

  integer(ids_int),pointer  :: nodes(:) => null()      ! /nodes - List of nodes forming this object (indices to objects_per_dimension(1)%object(:) in Fortran notation
  real(ids_real)  :: measure=ids_real_invalid       ! /measure - Measure of the space object, i.e. physical size (length for 1d, area for 2d, volume for 3d objects,.
  real(ids_real)  :: measure_error_upper=ids_real_invalid
  real(ids_real)  :: measure_error_lower=ids_real_invalid
  integer(ids_int) :: measure_error_index=ids_int_invalid

endtype

type ids_generic_grid_dynamic_space_dimension  !    Generic grid, list of dimensions within a space (dynamic within a type 3 AoS)
  type (ids_generic_grid_dynamic_space_dimension_object),pointer :: object(:) => null()  ! /object(i) - Set of objects for a given dimension
endtype

type ids_generic_grid_dynamic_space  !    Generic grid space (dynamic within a type 3 AoS)
  type (ids_identifier_dynamic_aos3) :: geometry_type  ! /geometry_type - Type of space geometry (0: standard, 1:Fourier)
  integer(ids_int),pointer  :: coordinates_type(:) => null()      ! /coordinates_type - Type of coordinates describing the physical space, for every coordinate of the space. The size of th
  type (ids_generic_grid_dynamic_space_dimension),pointer :: objects_per_dimension(:) => null()  ! /objects_per_dimension(i) - Definition of the space objects for every dimension (from one to the dimension of the highest-dimens
endtype

type ids_generic_grid_dynamic_grid_subset_element_object  !    Generic grid, object part of an element part of a grid_subset (dynamic within a type 3 AoS)
  integer(ids_int)  :: space=ids_int_invalid       ! /space - Index of the space from which that object is taken
  integer(ids_int)  :: dimension=ids_int_invalid       ! /dimension - Dimension of the object
  integer(ids_int)  :: index=ids_int_invalid       ! /index - Object index
endtype

type ids_generic_grid_dynamic_grid_subset_element  !    Generic grid, element part of a grid_subset (dynamic within a type 3 AoS)
  type (ids_generic_grid_dynamic_grid_subset_element_object),pointer :: object(:) => null()  ! /object(i) - Set of objects defining the element
endtype

type ids_generic_grid_dynamic_grid_subset_metric  !    Generic grid, metric description for a given grid_subset and base (dynamic within a type 3 AoS)
  real(ids_real),pointer  :: jacobian(:) => null()     ! /jacobian - Metric Jacobian
  real(ids_real),pointer  :: jacobian_error_upper(:) => null()
  real(ids_real),pointer  :: jacobian_error_lower(:) => null()
  integer(ids_int) :: jacobian_error_index=ids_int_invalid

  real(ids_real),pointer  :: tensor_covariant(:,:,:) => null()     ! /tensor_covariant - Covariant metric tensor, given on each element of the subgrid (first dimension)
  real(ids_real),pointer  :: tensor_covariant_error_upper(:,:,:) => null()
  real(ids_real),pointer  :: tensor_covariant_error_lower(:,:,:) => null()
  integer(ids_int) :: tensor_covariant_error_index=ids_int_invalid

  real(ids_real),pointer  :: tensor_contravariant(:,:,:) => null()     ! /tensor_contravariant - Contravariant metric tensor, given on each element of the subgrid (first dimension)
  real(ids_real),pointer  :: tensor_contravariant_error_upper(:,:,:) => null()
  real(ids_real),pointer  :: tensor_contravariant_error_lower(:,:,:) => null()
  integer(ids_int) :: tensor_contravariant_error_index=ids_int_invalid

endtype

type ids_generic_grid_dynamic_grid_subset  !    Generic grid grid_subset (dynamic within a type 3 AoS)
  type (ids_identifier_dynamic_aos3) :: identifier  ! /identifier - Grid subset identifier
  integer(ids_int)  :: dimension=ids_int_invalid       ! /dimension - Space dimension of the grid subset elements. This must be equal to the sum of the dimensions of the
  type (ids_generic_grid_dynamic_grid_subset_element),pointer :: element(:) => null()  ! /element(i) - Set of elements defining the grid subset. An element is defined by a combination of objects from pot
  type (ids_generic_grid_dynamic_grid_subset_metric),pointer :: base(:) => null()  ! /base(i) - Set of bases for the grid subset. For each base, the structure describes the projection of the base
  type (ids_generic_grid_dynamic_grid_subset_metric) :: metric  ! /metric - Metric of the canonical frame onto Cartesian coordinates
endtype

type ids_generic_grid_dynamic  !    Generic grid (dynamic within a type 3 AoS)
  type (ids_identifier_dynamic_aos3) :: identifier  ! /identifier - Grid identifier
  type (ids_generic_grid_dynamic_space),pointer :: space(:) => null()  ! /space(i) - Set of grid spaces
  type (ids_generic_grid_dynamic_grid_subset),pointer :: grid_subset(:) => null()  ! /grid_subset(i) - Grid subsets
endtype

type ids_generic_grid_aos3_root  !    Generic grid (being itself the root of a type 3 AoS)
  type (ids_identifier_dynamic_aos3) :: identifier  ! /identifier - Grid identifier
  type (ids_generic_grid_dynamic_space),pointer :: space(:) => null()  ! /space(i) - Set of grid spaces
  type (ids_generic_grid_dynamic_grid_subset),pointer :: grid_subset(:) => null()  ! /grid_subset(i) - Grid subsets
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_equilibrium_profiles_2d_grid  !    Definition of the 2D grid
  real(ids_real),pointer  :: dim1(:) => null()     ! /dim1 - First dimension values
  real(ids_real),pointer  :: dim1_error_upper(:) => null()
  real(ids_real),pointer  :: dim1_error_lower(:) => null()
  integer(ids_int) :: dim1_error_index=ids_int_invalid

  real(ids_real),pointer  :: dim2(:) => null()     ! /dim2 - Second dimension values
  real(ids_real),pointer  :: dim2_error_upper(:) => null()
  real(ids_real),pointer  :: dim2_error_lower(:) => null()
  integer(ids_int) :: dim2_error_index=ids_int_invalid

  real(ids_real),pointer  :: volume_element(:,:) => null()     ! /volume_element - Elementary plasma volume of plasma enclosed in the cell formed by the nodes [dim1(i) dim2(j)], [dim1
  real(ids_real),pointer  :: volume_element_error_upper(:,:) => null()
  real(ids_real),pointer  :: volume_element_error_lower(:,:) => null()
  integer(ids_int) :: volume_element_error_index=ids_int_invalid

endtype

type ids_equilibrium_coordinate_system  !    Flux surface coordinate system on a square grid of flux and poloidal angle
  type (ids_identifier) :: grid_type  ! /grid_type - Type of coordinate system
  type (ids_equilibrium_profiles_2d_grid) :: grid  ! /grid - Definition of the 2D grid
  real(ids_real),pointer  :: r(:,:) => null()     ! /r - Values of the major radius on the grid
  real(ids_real),pointer  :: r_error_upper(:,:) => null()
  real(ids_real),pointer  :: r_error_lower(:,:) => null()
  integer(ids_int) :: r_error_index=ids_int_invalid

  real(ids_real),pointer  :: z(:,:) => null()     ! /z - Values of the Height on the grid
  real(ids_real),pointer  :: z_error_upper(:,:) => null()
  real(ids_real),pointer  :: z_error_lower(:,:) => null()
  integer(ids_int) :: z_error_index=ids_int_invalid

  real(ids_real),pointer  :: jacobian(:,:) => null()     ! /jacobian - Absolute value of the jacobian of the coordinate system
  real(ids_real),pointer  :: jacobian_error_upper(:,:) => null()
  real(ids_real),pointer  :: jacobian_error_lower(:,:) => null()
  integer(ids_int) :: jacobian_error_index=ids_int_invalid

  real(ids_real),pointer  :: tensor_covariant(:,:,:,:) => null()     ! /tensor_covariant - Covariant metric tensor on every point of the grid described by grid_type
  real(ids_real),pointer  :: tensor_covariant_error_upper(:,:,:,:) => null()
  real(ids_real),pointer  :: tensor_covariant_error_lower(:,:,:,:) => null()
  integer(ids_int) :: tensor_covariant_error_index=ids_int_invalid

  real(ids_real),pointer  :: tensor_contravariant(:,:,:,:) => null()     ! /tensor_contravariant - Contravariant metric tensor on every point of the grid described by grid_type
  real(ids_real),pointer  :: tensor_contravariant_error_upper(:,:,:,:) => null()
  real(ids_real),pointer  :: tensor_contravariant_error_lower(:,:,:,:) => null()
  integer(ids_int) :: tensor_contravariant_error_index=ids_int_invalid

  real(ids_real),pointer  :: g11_covariant(:,:) => null()     ! /g11_covariant - metric coefficients g11,  covariant metric tensor for the grid described by grid_type
  real(ids_real),pointer  :: g11_covariant_error_upper(:,:) => null()
  real(ids_real),pointer  :: g11_covariant_error_lower(:,:) => null()
  integer(ids_int) :: g11_covariant_error_index=ids_int_invalid

  real(ids_real),pointer  :: g12_covariant(:,:) => null()     ! /g12_covariant - metric coefficients g12,  covariant metric tensor for the grid described by grid_type
  real(ids_real),pointer  :: g12_covariant_error_upper(:,:) => null()
  real(ids_real),pointer  :: g12_covariant_error_lower(:,:) => null()
  integer(ids_int) :: g12_covariant_error_index=ids_int_invalid

  real(ids_real),pointer  :: g13_covariant(:,:) => null()     ! /g13_covariant - metric coefficients g13,  covariant metric tensor for the grid described by grid_type
  real(ids_real),pointer  :: g13_covariant_error_upper(:,:) => null()
  real(ids_real),pointer  :: g13_covariant_error_lower(:,:) => null()
  integer(ids_int) :: g13_covariant_error_index=ids_int_invalid

  real(ids_real),pointer  :: g22_covariant(:,:) => null()     ! /g22_covariant - metric coefficients g22,  covariant metric tensor for the grid described by grid_type
  real(ids_real),pointer  :: g22_covariant_error_upper(:,:) => null()
  real(ids_real),pointer  :: g22_covariant_error_lower(:,:) => null()
  integer(ids_int) :: g22_covariant_error_index=ids_int_invalid

  real(ids_real),pointer  :: g23_covariant(:,:) => null()     ! /g23_covariant - metric coefficients g23,  covariant metric tensor for the grid described by grid_type
  real(ids_real),pointer  :: g23_covariant_error_upper(:,:) => null()
  real(ids_real),pointer  :: g23_covariant_error_lower(:,:) => null()
  integer(ids_int) :: g23_covariant_error_index=ids_int_invalid

  real(ids_real),pointer  :: g33_covariant(:,:) => null()     ! /g33_covariant - metric coefficients g33,  covariant metric tensor for the grid described by grid_type
  real(ids_real),pointer  :: g33_covariant_error_upper(:,:) => null()
  real(ids_real),pointer  :: g33_covariant_error_lower(:,:) => null()
  integer(ids_int) :: g33_covariant_error_index=ids_int_invalid

  real(ids_real),pointer  :: g11_contravariant(:,:) => null()     ! /g11_contravariant - metric coefficients g11,  contravariant metric tensor for the grid described by grid_type
  real(ids_real),pointer  :: g11_contravariant_error_upper(:,:) => null()
  real(ids_real),pointer  :: g11_contravariant_error_lower(:,:) => null()
  integer(ids_int) :: g11_contravariant_error_index=ids_int_invalid

  real(ids_real),pointer  :: g12_contravariant(:,:) => null()     ! /g12_contravariant - metric coefficients g12,  contravariant metric tensor for the grid described by grid_type
  real(ids_real),pointer  :: g12_contravariant_error_upper(:,:) => null()
  real(ids_real),pointer  :: g12_contravariant_error_lower(:,:) => null()
  integer(ids_int) :: g12_contravariant_error_index=ids_int_invalid

  real(ids_real),pointer  :: g13_contravariant(:,:) => null()     ! /g13_contravariant - metric coefficients g13,  contravariant metric tensor for the grid described by grid_type
  real(ids_real),pointer  :: g13_contravariant_error_upper(:,:) => null()
  real(ids_real),pointer  :: g13_contravariant_error_lower(:,:) => null()
  integer(ids_int) :: g13_contravariant_error_index=ids_int_invalid

  real(ids_real),pointer  :: g22_contravariant(:,:) => null()     ! /g22_contravariant - metric coefficients g22,  contravariant metric tensor for the grid described by grid_type
  real(ids_real),pointer  :: g22_contravariant_error_upper(:,:) => null()
  real(ids_real),pointer  :: g22_contravariant_error_lower(:,:) => null()
  integer(ids_int) :: g22_contravariant_error_index=ids_int_invalid

  real(ids_real),pointer  :: g23_contravariant(:,:) => null()     ! /g23_contravariant - metric coefficients g23,  contravariant metric tensor for the grid described by grid_type
  real(ids_real),pointer  :: g23_contravariant_error_upper(:,:) => null()
  real(ids_real),pointer  :: g23_contravariant_error_lower(:,:) => null()
  integer(ids_int) :: g23_contravariant_error_index=ids_int_invalid

  real(ids_real),pointer  :: g33_contravariant(:,:) => null()     ! /g33_contravariant - metric coefficients g33,  contravariant metric tensor for the grid described by grid_type
  real(ids_real),pointer  :: g33_contravariant_error_upper(:,:) => null()
  real(ids_real),pointer  :: g33_contravariant_error_lower(:,:) => null()
  integer(ids_int) :: g33_contravariant_error_index=ids_int_invalid

endtype

type ids_delta_rzphi0d_static  !    Structure for R, Z, Phi relative positions (0D, static)
  real(ids_real)  :: delta_r=ids_real_invalid       ! /delta_r - Major radius (relative to a reference point)
  real(ids_real)  :: delta_r_error_upper=ids_real_invalid
  real(ids_real)  :: delta_r_error_lower=ids_real_invalid
  integer(ids_int) :: delta_r_error_index=ids_int_invalid

  real(ids_real)  :: delta_z=ids_real_invalid       ! /delta_z - Height (relative to a reference point)
  real(ids_real)  :: delta_z_error_upper=ids_real_invalid
  real(ids_real)  :: delta_z_error_lower=ids_real_invalid
  integer(ids_int) :: delta_z_error_index=ids_int_invalid

  real(ids_real)  :: delta_phi=ids_real_invalid       ! /delta_phi - Toroidal angle (relative to a reference point)
  real(ids_real)  :: delta_phi_error_upper=ids_real_invalid
  real(ids_real)  :: delta_phi_error_lower=ids_real_invalid
  integer(ids_int) :: delta_phi_error_index=ids_int_invalid

endtype

type ids_delta_rzphi1d_static  !    Structure for R, Z, Phi relative positions (1D, static)
  real(ids_real),pointer  :: delta_r(:) => null()     ! /delta_r - Major radii (relative to a reference point)
  real(ids_real),pointer  :: delta_r_error_upper(:) => null()
  real(ids_real),pointer  :: delta_r_error_lower(:) => null()
  integer(ids_int) :: delta_r_error_index=ids_int_invalid

  real(ids_real),pointer  :: delta_z(:) => null()     ! /delta_z - Heights (relative to a reference point)
  real(ids_real),pointer  :: delta_z_error_upper(:) => null()
  real(ids_real),pointer  :: delta_z_error_lower(:) => null()
  integer(ids_int) :: delta_z_error_index=ids_int_invalid

  real(ids_real),pointer  :: delta_phi(:) => null()     ! /delta_phi - Toroidal angles (relative to a reference point)
  real(ids_real),pointer  :: delta_phi_error_upper(:) => null()
  real(ids_real),pointer  :: delta_phi_error_lower(:) => null()
  integer(ids_int) :: delta_phi_error_index=ids_int_invalid

endtype

type ids_rzphi0d_static  !    Structure for R, Z, Phi positions (0D, static)
  real(ids_real)  :: r=ids_real_invalid       ! /r - Major radius
  real(ids_real)  :: r_error_upper=ids_real_invalid
  real(ids_real)  :: r_error_lower=ids_real_invalid
  integer(ids_int) :: r_error_index=ids_int_invalid

  real(ids_real)  :: z=ids_real_invalid       ! /z - Height
  real(ids_real)  :: z_error_upper=ids_real_invalid
  real(ids_real)  :: z_error_lower=ids_real_invalid
  integer(ids_int) :: z_error_index=ids_int_invalid

  real(ids_real)  :: phi=ids_real_invalid       ! /phi - Toroidal angle
  real(ids_real)  :: phi_error_upper=ids_real_invalid
  real(ids_real)  :: phi_error_lower=ids_real_invalid
  integer(ids_int) :: phi_error_index=ids_int_invalid

endtype

type ids_rzphi0d_dynamic_aos3  !    Structure for R, Z, Phi positions (0D, dynamic within a type 3 array of structures (index on time))
  real(ids_real)  :: r=ids_real_invalid       ! /r - Major radius
  real(ids_real)  :: r_error_upper=ids_real_invalid
  real(ids_real)  :: r_error_lower=ids_real_invalid
  integer(ids_int) :: r_error_index=ids_int_invalid

  real(ids_real)  :: z=ids_real_invalid       ! /z - Height
  real(ids_real)  :: z_error_upper=ids_real_invalid
  real(ids_real)  :: z_error_lower=ids_real_invalid
  integer(ids_int) :: z_error_index=ids_int_invalid

  real(ids_real)  :: phi=ids_real_invalid       ! /phi - Toroidal angle
  real(ids_real)  :: phi_error_upper=ids_real_invalid
  real(ids_real)  :: phi_error_lower=ids_real_invalid
  integer(ids_int) :: phi_error_index=ids_int_invalid

endtype

type ids_rzphi1d_static  !    Structure for list of R, Z, Phi positions (1D, static)
  real(ids_real),pointer  :: r(:) => null()     ! /r - Major radius
  real(ids_real),pointer  :: r_error_upper(:) => null()
  real(ids_real),pointer  :: r_error_lower(:) => null()
  integer(ids_int) :: r_error_index=ids_int_invalid

  real(ids_real),pointer  :: z(:) => null()     ! /z - Height
  real(ids_real),pointer  :: z_error_upper(:) => null()
  real(ids_real),pointer  :: z_error_lower(:) => null()
  integer(ids_int) :: z_error_index=ids_int_invalid

  real(ids_real),pointer  :: phi(:) => null()     ! /phi - Toroidal angle
  real(ids_real),pointer  :: phi_error_upper(:) => null()
  real(ids_real),pointer  :: phi_error_lower(:) => null()
  integer(ids_int) :: phi_error_index=ids_int_invalid

endtype

! SPECIAL STRUCTURE data / time
type ids_rzphi1d_dynamic_aos1_r  !    Major radius
  real(ids_real), pointer  :: data(:) => null()     ! /r - Major radius
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_rzphi1d_dynamic_aos1_z  !    Height
  real(ids_real), pointer  :: data(:) => null()     ! /z - Height
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_rzphi1d_dynamic_aos1_phi  !    Toroidal angle
  real(ids_real), pointer  :: data(:) => null()     ! /phi - Toroidal angle
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_rzphi1d_dynamic_aos1  !    Structure for list of R, Z, Phi positions (1D, dynamic within a type 1 array of structures (indexed on objects, data/time structur
  type (ids_rzphi1d_dynamic_aos1_r) :: r  ! /r - Major radius
  type (ids_rzphi1d_dynamic_aos1_z) :: z  ! /z - Height
  type (ids_rzphi1d_dynamic_aos1_phi) :: phi  ! /phi - Toroidal angle
endtype

type ids_rzphi1d_dynamic_aos3  !    Structure for R, Z, Phi positions (1D, dynamic within a type 3 array of structure)
  real(ids_real),pointer  :: r(:) => null()     ! /r - Major radius
  real(ids_real),pointer  :: r_error_upper(:) => null()
  real(ids_real),pointer  :: r_error_lower(:) => null()
  integer(ids_int) :: r_error_index=ids_int_invalid

  real(ids_real),pointer  :: z(:) => null()     ! /z - Height
  real(ids_real),pointer  :: z_error_upper(:) => null()
  real(ids_real),pointer  :: z_error_lower(:) => null()
  integer(ids_int) :: z_error_index=ids_int_invalid

  real(ids_real),pointer  :: phi(:) => null()     ! /phi - Toroidal angle
  real(ids_real),pointer  :: phi_error_upper(:) => null()
  real(ids_real),pointer  :: phi_error_lower(:) => null()
  integer(ids_int) :: phi_error_index=ids_int_invalid

endtype

type ids_rzphipsitheta1d_dynamic_aos3  !    Structure for R, Z, Phi, Psi, Theta positions (1D, dynamic within a type 3 array of structures)
  real(ids_real),pointer  :: r(:) => null()     ! /r - Major radius
  real(ids_real),pointer  :: r_error_upper(:) => null()
  real(ids_real),pointer  :: r_error_lower(:) => null()
  integer(ids_int) :: r_error_index=ids_int_invalid

  real(ids_real),pointer  :: z(:) => null()     ! /z - Height
  real(ids_real),pointer  :: z_error_upper(:) => null()
  real(ids_real),pointer  :: z_error_lower(:) => null()
  integer(ids_int) :: z_error_index=ids_int_invalid

  real(ids_real),pointer  :: phi(:) => null()     ! /phi - Toroidal angle
  real(ids_real),pointer  :: phi_error_upper(:) => null()
  real(ids_real),pointer  :: phi_error_lower(:) => null()
  integer(ids_int) :: phi_error_index=ids_int_invalid

  real(ids_real),pointer  :: psi(:) => null()     ! /psi - Poloidal flux
  real(ids_real),pointer  :: psi_error_upper(:) => null()
  real(ids_real),pointer  :: psi_error_lower(:) => null()
  integer(ids_int) :: psi_error_index=ids_int_invalid

  real(ids_real),pointer  :: theta(:) => null()     ! /theta - Poloidal angle
  real(ids_real),pointer  :: theta_error_upper(:) => null()
  real(ids_real),pointer  :: theta_error_lower(:) => null()
  integer(ids_int) :: theta_error_index=ids_int_invalid

endtype

type ids_rz1d_constant  !    Structure for list of R, Z positions (1D, constant)
  real(ids_real),pointer  :: r(:) => null()     ! /r - Major radius
  real(ids_real),pointer  :: r_error_upper(:) => null()
  real(ids_real),pointer  :: r_error_lower(:) => null()
  integer(ids_int) :: r_error_index=ids_int_invalid

  real(ids_real),pointer  :: z(:) => null()     ! /z - Height
  real(ids_real),pointer  :: z_error_upper(:) => null()
  real(ids_real),pointer  :: z_error_lower(:) => null()
  integer(ids_int) :: z_error_index=ids_int_invalid

endtype

type ids_rz0d_dynamic_aos  !    Structure for scalar R, Z positions, dynamic within a type 3 array of structures (index on time)
  real(ids_real)  :: r=ids_real_invalid       ! /r - Major radius
  real(ids_real)  :: r_error_upper=ids_real_invalid
  real(ids_real)  :: r_error_lower=ids_real_invalid
  integer(ids_int) :: r_error_index=ids_int_invalid

  real(ids_real)  :: z=ids_real_invalid       ! /z - Height
  real(ids_real)  :: z_error_upper=ids_real_invalid
  real(ids_real)  :: z_error_lower=ids_real_invalid
  integer(ids_int) :: z_error_index=ids_int_invalid

endtype

type ids_rz1d_dynamic_aos  !    Structure for list of R, Z positions (1D list of Npoints, dynamic within a type 3 array of structures (index on time))
  real(ids_real),pointer  :: r(:) => null()     ! /r - Major radius
  real(ids_real),pointer  :: r_error_upper(:) => null()
  real(ids_real),pointer  :: r_error_lower(:) => null()
  integer(ids_int) :: r_error_index=ids_int_invalid

  real(ids_real),pointer  :: z(:) => null()     ! /z - Height
  real(ids_real),pointer  :: z_error_upper(:) => null()
  real(ids_real),pointer  :: z_error_lower(:) => null()
  integer(ids_int) :: z_error_index=ids_int_invalid

endtype

type ids_rz1d_dynamic_aos_time  !    Structure for list of R, Z positions (1D list of Npoints, dynamic within a type 3 array of structures (index on time), with time a
  real(ids_real),pointer  :: r(:) => null()     ! /r - Major radius
  real(ids_real),pointer  :: r_error_upper(:) => null()
  real(ids_real),pointer  :: r_error_lower(:) => null()
  integer(ids_int) :: r_error_index=ids_int_invalid

  real(ids_real),pointer  :: z(:) => null()     ! /z - Height
  real(ids_real),pointer  :: z_error_upper(:) => null()
  real(ids_real),pointer  :: z_error_lower(:) => null()
  integer(ids_int) :: z_error_index=ids_int_invalid

  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_rz1d_dynamic_2  !    Structure for list of R, Z positions (1D, dynamic), time assumed to be 2 levels above
  real(ids_real),pointer  :: r(:) => null()     ! /r - Major radius
  real(ids_real),pointer  :: r_error_upper(:) => null()
  real(ids_real),pointer  :: r_error_lower(:) => null()
  integer(ids_int) :: r_error_index=ids_int_invalid

  real(ids_real),pointer  :: z(:) => null()     ! /z - Height
  real(ids_real),pointer  :: z_error_upper(:) => null()
  real(ids_real),pointer  :: z_error_lower(:) => null()
  integer(ids_int) :: z_error_index=ids_int_invalid

endtype

type ids_rz1d_dynamic_1  !    Structure for list of R, Z positions (1D, dynamic), time assumed to be 1 level above
  real(ids_real),pointer  :: r(:) => null()     ! /r - Major radius
  real(ids_real),pointer  :: r_error_upper(:) => null()
  real(ids_real),pointer  :: r_error_lower(:) => null()
  integer(ids_int) :: r_error_index=ids_int_invalid

  real(ids_real),pointer  :: z(:) => null()     ! /z - Height
  real(ids_real),pointer  :: z_error_upper(:) => null()
  real(ids_real),pointer  :: z_error_lower(:) => null()
  integer(ids_int) :: z_error_index=ids_int_invalid

endtype

type ids_rz1d_static  !    Structure for list of R, Z positions (1D, constant)
  real(ids_real),pointer  :: r(:) => null()     ! /r - Major radius
  real(ids_real),pointer  :: r_error_upper(:) => null()
  real(ids_real),pointer  :: r_error_lower(:) => null()
  integer(ids_int) :: r_error_index=ids_int_invalid

  real(ids_real),pointer  :: z(:) => null()     ! /z - Height
  real(ids_real),pointer  :: z_error_upper(:) => null()
  real(ids_real),pointer  :: z_error_lower(:) => null()
  integer(ids_int) :: z_error_index=ids_int_invalid

endtype

type ids_x1x21d_static  !    Structure for list of X1, X2 positions (1D, static)
  real(ids_real),pointer  :: x1(:) => null()     ! /x1 - Positions along x1 axis
  real(ids_real),pointer  :: x1_error_upper(:) => null()
  real(ids_real),pointer  :: x1_error_lower(:) => null()
  integer(ids_int) :: x1_error_index=ids_int_invalid

  real(ids_real),pointer  :: x2(:) => null()     ! /x2 - Positions along x2 axis
  real(ids_real),pointer  :: x2_error_upper(:) => null()
  real(ids_real),pointer  :: x2_error_lower(:) => null()
  integer(ids_int) :: x2_error_index=ids_int_invalid

endtype

type ids_xyz0d_static  !    Structure for list of X, Y, Z components (0D, static)
  real(ids_real)  :: x=ids_real_invalid       ! /x - Component along X axis
  real(ids_real)  :: x_error_upper=ids_real_invalid
  real(ids_real)  :: x_error_lower=ids_real_invalid
  integer(ids_int) :: x_error_index=ids_int_invalid

  real(ids_real)  :: y=ids_real_invalid       ! /y - Component along Y axis
  real(ids_real)  :: y_error_upper=ids_real_invalid
  real(ids_real)  :: y_error_lower=ids_real_invalid
  integer(ids_int) :: y_error_index=ids_int_invalid

  real(ids_real)  :: z=ids_real_invalid       ! /z - Component along Z axis
  real(ids_real)  :: z_error_upper=ids_real_invalid
  real(ids_real)  :: z_error_lower=ids_real_invalid
  integer(ids_int) :: z_error_index=ids_int_invalid

endtype

type ids_rz0d_static  !    Structure for a single R, Z position (0D, static)
  real(ids_real)  :: r=ids_real_invalid       ! /r - Major radius
  real(ids_real)  :: r_error_upper=ids_real_invalid
  real(ids_real)  :: r_error_lower=ids_real_invalid
  integer(ids_int) :: r_error_index=ids_int_invalid

  real(ids_real)  :: z=ids_real_invalid       ! /z - Height
  real(ids_real)  :: z_error_upper=ids_real_invalid
  real(ids_real)  :: z_error_lower=ids_real_invalid
  integer(ids_int) :: z_error_index=ids_int_invalid

endtype

type ids_rz0d_constant  !    Structure for a single R, Z position (0D, constant)
  real(ids_real)  :: r=ids_real_invalid       ! /r - Major radius
  real(ids_real)  :: r_error_upper=ids_real_invalid
  real(ids_real)  :: r_error_lower=ids_real_invalid
  integer(ids_int) :: r_error_index=ids_int_invalid

  real(ids_real)  :: z=ids_real_invalid       ! /z - Height
  real(ids_real)  :: z_error_upper=ids_real_invalid
  real(ids_real)  :: z_error_lower=ids_real_invalid
  integer(ids_int) :: z_error_index=ids_int_invalid

endtype

type ids_detector_aperture  !    Generic description of a plane detector or collimating aperture
  integer(ids_int)  :: geometry_type=ids_int_invalid       ! /geometry_type - Type of geometry used to describe the surface of the detector or aperture (1:'outline', 2:'circular'
  type (ids_rzphi0d_static) :: centre  ! /centre - If geometry_type=2, coordinates of the center of the circle. If geometry_type=1, coordinates of the
  real(ids_real)  :: radius=ids_real_invalid       ! /radius - Radius of the circle, used only if geometry_type = 2
  real(ids_real)  :: radius_error_upper=ids_real_invalid
  real(ids_real)  :: radius_error_lower=ids_real_invalid
  integer(ids_int) :: radius_error_index=ids_int_invalid

  type (ids_xyz0d_static) :: x1_unit_vector  ! /x1_unit_vector - Components of the X1 direction unit vector in the (X,Y,Z) coordinate system, where X is the major ra
  type (ids_xyz0d_static) :: x2_unit_vector  ! /x2_unit_vector - Components of the X2 direction unit vector in the (X,Y,Z) coordinate system, where X is the major ra
  type (ids_xyz0d_static) :: x3_unit_vector  ! /x3_unit_vector - Components of the X3 direction unit vector in the (X,Y,Z) coordinate system, where X is the major ra
  type (ids_x1x21d_static) :: outline  ! /outline - Irregular outline of the detector/aperture in the (X1, X2) coordinate system
endtype

type ids_line_of_sight_2points  !    Generic description of a line of sight, defined by two points
  type (ids_rzphi0d_static) :: first_point  ! /first_point - Position of the first point
  type (ids_rzphi0d_static) :: second_point  ! /second_point - Position of the second point
endtype

type ids_line_of_sight_2points_dynamic_aos3  !    Generic description of a line of sight, defined by two points, dynamic within a type 3 array of structures (index on time)
  type (ids_rzphi0d_dynamic_aos3) :: first_point  ! /first_point - Position of the first point
  type (ids_rzphi0d_dynamic_aos3) :: second_point  ! /second_point - Position of the second point
endtype

type ids_line_of_sight_3points  !    Generic description of a line of sight, defined by two points (one way) and an optional third point to indicate the direction of r
  type (ids_rzphi0d_static) :: first_point  ! /first_point - Position of the first point
  type (ids_rzphi0d_static) :: second_point  ! /second_point - Position of the second point
  type (ids_rzphi0d_static) :: third_point  ! /third_point - Position of the third point
endtype

type ids_data_entry  !    Definition of a data entry
  character(len=ids_string_length), dimension(:), pointer ::user => null()       ! /user - Username
  character(len=ids_string_length), dimension(:), pointer ::machine => null()       ! /machine - Name of the experimental device to which this data is related
  character(len=ids_string_length), dimension(:), pointer ::pulse_type => null()       ! /pulse_type - Type of the data entry, e.g. "pulse", "simulation", ...
  integer(ids_int)  :: pulse=ids_int_invalid       ! /pulse - Pulse number
  integer(ids_int)  :: run=ids_int_invalid       ! /run - Run number
endtype

type ids_plasma_composition  !    Generic declaration of Plasma Composition for a simulation
  type (ids_plasma_composition_ions),pointer :: ion(:) => null()  ! /plasma_composition/ion(i) - Array of plasma ions
endtype

type ids_code  !    Generic decription of the code-specific parameters for the code that has produced this IDS
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /code/name - Name of software generating IDS
  character(len=ids_string_length), dimension(:), pointer ::commit => null()       ! /code/commit - Unique commit reference of software
  character(len=ids_string_length), dimension(:), pointer ::version => null()       ! /code/version - Unique version (tag) of software
  character(len=ids_string_length), dimension(:), pointer ::repository => null()       ! /code/repository - URL of software repository
  character(len=ids_string_length), dimension(:), pointer ::parameters => null()       ! /code/parameters - List of the code specific parameters in XML format
  integer(ids_int),pointer  :: output_flag(:) => null()      ! /code/output_flag - Output flag : 0 means the run is successful, other values mean some difficulty has been encountered,
endtype

type ids_parameters_input  !    Code parameters block passed from the wrapper to the subroutine. Does not appear as such in the data structure. This is inserted i
  character(len=ids_string_length), dimension(:), pointer ::parameters_value => null()       ! /parameters_input/parameters_value - Actual value of the code parameters (instance of Code_Parameters/Parameters in XML format)
  character(len=ids_string_length), dimension(:), pointer ::parameters_default => null()       ! /parameters_input/parameters_default - Default value of the code parameters (instance of Code_Parameters/Parameters in XML format)
  character(len=ids_string_length), dimension(:), pointer ::schema => null()       ! /parameters_input/schema - Code parameters schema
endtype

type ids_error_description  !    Error description, an array of this structure is passed as argument of the access layer calls (get and put) for handling errorbars
  integer(ids_int)  :: symmetric=ids_int_invalid       ! /error_description/symmetric - Flag indicating whether the error is “+/-“ symmetric (1) or not (0)
  type (ids_identifier) :: type  ! /error_description/type - Type of error bar description which is used (assumed to be identical for the lower and upper error):
  character(len=ids_string_length), dimension(:), pointer ::expression_upper_0d => null()       ! /error_description/expression_upper_0d - Upper error expression (absolute value taken), for 1D dynamic quantities
  character(len=ids_string_length), dimension(:), pointer ::expression_upper_1d => null()       ! /error_description/expression_upper_1d - Upper error expression (absolute value taken), for 2D dynamic quantities. If its dimension is equal
  character(len=ids_string_length), dimension(:), pointer ::expression_lower_0d => null()       ! /error_description/expression_lower_0d - Lower error expression (absolute value taken), for 1D dynamic quantities
  character(len=ids_string_length), dimension(:), pointer ::expression_lower_1d => null()       ! /error_description/expression_lower_1d - Lower error expression (absolute value taken), for 2D dynamic quantities. If its dimension is equal
endtype

type ids_ids_properties  !    Interface Data Structure properties. This element identifies the node above as an IDS
  character(len=ids_string_length), dimension(:), pointer ::comment => null()       ! /ids_properties/comment - Any comment describing the content of this IDS
  integer(ids_int)  :: homogeneous_time=ids_int_invalid       ! /ids_properties/homogeneous_time - 1 if the time of this IDS is homogeneous. In this case, the time values for this IDS are stored in .
  character(len=ids_string_length), dimension(:), pointer ::source => null()       ! /ids_properties/source - Source of the data (any comment describing the origin of the data : code, path to diagnostic signals
  character(len=ids_string_length), dimension(:), pointer ::provider => null()       ! /ids_properties/provider - Name of the person in charge of producing this data
  character(len=ids_string_length), dimension(:), pointer ::creation_date => null()       ! /ids_properties/creation_date - Date at which this data has been produced
endtype


end module ! end of the utilities module

module ids_schemas       ! declaration of all IDSs

use ids_utilities

integer(ids_int), parameter :: NON_TIMED=0
integer(ids_int), parameter :: TIMED=1
integer(ids_int), parameter :: TIMED_CLEAR=2


! ***********  Include neutron_diagnostic/dd_neutron_diagnostic.xsd
! SPECIAL STRUCTURE data / time
type ids_neutron_diagnostic_temperature_sensor_temperature  !    Temperature measured by the sensor
  real(ids_real), pointer  :: data(:) => null()     ! /temperature - Temperature measured by the sensor
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_neutron_diagnostic_temperature_sensor  !    Temperature sensor
  integer(ids_int)  :: power_switch=ids_int_invalid       ! /power_switch - Power switch (1=on, 0=off)
  type (ids_neutron_diagnostic_temperature_sensor_temperature) :: temperature  ! /temperature - Temperature measured by the sensor
endtype

! SPECIAL STRUCTURE data / time
type ids_neutron_diagnostic_b_field_sensor_b_field  !    Magnetic field measured by the sensor
  real(ids_real), pointer  :: data(:) => null()     ! /b_field - Magnetic field measured by the sensor
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_neutron_diagnostic_b_field_sensor  !    Magnetic field sensor
  integer(ids_int)  :: power_switch=ids_int_invalid       ! /power_switch - Power switch (1=on, 0=off)
  type (ids_neutron_diagnostic_b_field_sensor_b_field) :: b_field  ! /b_field - Magnetic field measured by the sensor
endtype

! SPECIAL STRUCTURE data / time
type ids_neutron_diagnostic_test_generator_frequency  !    Generated signal frequency
  real(ids_real), pointer  :: data(:) => null()     ! /frequency - Generated signal frequency
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_neutron_diagnostic_test_generator_amplitude  !    Generated signal amplitude
  real(ids_real), pointer  :: data(:) => null()     ! /amplitude - Generated signal amplitude
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_neutron_diagnostic_test_generator  !    Test generator
  integer(ids_int)  :: power_switch=ids_int_invalid       ! /power_switch - Power switch (1=on, 0=off)
  type (ids_identifier) :: shape  ! /shape - Signal shape. Index : 1 – rectangular, 2 – gaussian
  real(ids_real)  :: rise_time=ids_real_invalid       ! /rise_time - Peak rise time
  real(ids_real)  :: rise_time_error_upper=ids_real_invalid
  real(ids_real)  :: rise_time_error_lower=ids_real_invalid
  integer(ids_int) :: rise_time_error_index=ids_int_invalid

  real(ids_real)  :: fall_time=ids_real_invalid       ! /fall_time - Peak fall time
  real(ids_real)  :: fall_time_error_upper=ids_real_invalid
  real(ids_real)  :: fall_time_error_lower=ids_real_invalid
  integer(ids_int) :: fall_time_error_index=ids_int_invalid

  type (ids_neutron_diagnostic_test_generator_frequency) :: frequency  ! /frequency - Generated signal frequency
  type (ids_neutron_diagnostic_test_generator_amplitude) :: amplitude  ! /amplitude - Generated signal amplitude
endtype

! SPECIAL STRUCTURE data / time
type ids_neutron_diagnostic_supply_voltage_set  !    Voltage set
  real(ids_real), pointer  :: data(:) => null()     ! /voltage_set - Voltage set
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_neutron_diagnostic_supply_voltage_out  !    Voltage at the supply output
  real(ids_real), pointer  :: data(:) => null()     ! /voltage_out - Voltage at the supply output
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_neutron_diagnostic_supply  !    Power supply
  integer(ids_int)  :: power_switch=ids_int_invalid       ! /power_switch - Power switch (1=on, 0=off)
  type (ids_neutron_diagnostic_supply_voltage_set) :: voltage_set  ! /voltage_set - Voltage set
  type (ids_neutron_diagnostic_supply_voltage_out) :: voltage_out  ! /voltage_out - Voltage at the supply output
endtype

type ids_neutron_diagnostic_adc  !    ADC
  integer(ids_int)  :: power_switch=ids_int_invalid       ! /power_switch - Power switch (1=on, 0=off)
  integer(ids_int)  :: discriminator_level_lower=ids_int_invalid       ! /discriminator_level_lower - Lower level discriminator of ADC
  integer(ids_int)  :: discriminator_level_upper=ids_int_invalid       ! /discriminator_level_upper - Upper level discriminator of ADC
  integer(ids_int)  :: sampling_rate=ids_int_invalid       ! /sampling_rate - Number of samples recorded per second
  real(ids_real)  :: bias=ids_real_invalid       ! /bias - ADC signal bias
  real(ids_real)  :: bias_error_upper=ids_real_invalid
  real(ids_real)  :: bias_error_lower=ids_real_invalid
  integer(ids_int) :: bias_error_index=ids_int_invalid

  real(ids_real)  :: input_range=ids_real_invalid       ! /input_range - ADC input range
  real(ids_real)  :: input_range_error_upper=ids_real_invalid
  real(ids_real)  :: input_range_error_lower=ids_real_invalid
  integer(ids_int) :: input_range_error_index=ids_int_invalid

  real(ids_real)  :: impedance=ids_real_invalid       ! /impedance - ADC impedance
  real(ids_real)  :: impedance_error_upper=ids_real_invalid
  real(ids_real)  :: impedance_error_lower=ids_real_invalid
  integer(ids_int) :: impedance_error_index=ids_int_invalid

endtype

type ids_neutron_diagnostic_characteristics_reaction_mode  !
  integer(ids_int)  :: index=ids_int_invalid       ! /index - Index of Measuring Mode
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of Measuring Mode
  real(ids_real)  :: count_limit_max=ids_real_invalid       ! /count_limit_max - Maximum count limit of recent Measuring Mode and plasma reaction
  real(ids_real)  :: count_limit_max_error_upper=ids_real_invalid
  real(ids_real)  :: count_limit_max_error_lower=ids_real_invalid
  integer(ids_int) :: count_limit_max_error_index=ids_int_invalid

  real(ids_real)  :: count_limit_min=ids_real_invalid       ! /count_limit_min - Minimum count limit of recent Measuring Mode and plasma reaction
  real(ids_real)  :: count_limit_min_error_upper=ids_real_invalid
  real(ids_real)  :: count_limit_min_error_lower=ids_real_invalid
  integer(ids_int) :: count_limit_min_error_index=ids_int_invalid

endtype

type ids_neutron_diagnostic_characteristics_reaction  !
  integer(ids_int)  :: index=ids_int_invalid       ! /index - Index of plasma reaction type
  real(ids_real)  :: error=ids_real_invalid       ! /error - Diagnostic's relative uncertainty for recent plasma reaction
  real(ids_real)  :: error_error_upper=ids_real_invalid
  real(ids_real)  :: error_error_lower=ids_real_invalid
  integer(ids_int) :: error_error_index=ids_int_invalid

  real(ids_real)  :: probability_overlap=ids_real_invalid       ! /probability_overlap - Pulse probability overlap for recent plasma reaction
  real(ids_real)  :: probability_overlap_error_upper=ids_real_invalid
  real(ids_real)  :: probability_overlap_error_lower=ids_real_invalid
  integer(ids_int) :: probability_overlap_error_index=ids_int_invalid

  type (ids_neutron_diagnostic_characteristics_reaction_mode),pointer :: mode(:) => null()  ! /mode(i) - Characteristics of counting linear limits in recent Measuring modes for recent Plasma reaction type
endtype

type ids_neutron_diagnostic_characteristics  !
  real(ids_real)  :: dead_time=ids_real_invalid       ! /dead_time - Dead time of detectors
  real(ids_real)  :: dead_time_error_upper=ids_real_invalid
  real(ids_real)  :: dead_time_error_lower=ids_real_invalid
  integer(ids_int) :: dead_time_error_index=ids_int_invalid

  real(ids_real)  :: pulse_length=ids_real_invalid       ! /pulse_length - Lower counting limit of recent Measuring Mode and plasma reaction
  real(ids_real)  :: pulse_length_error_upper=ids_real_invalid
  real(ids_real)  :: pulse_length_error_lower=ids_real_invalid
  integer(ids_int) :: pulse_length_error_index=ids_int_invalid

  type (ids_neutron_diagnostic_characteristics_reaction),pointer :: reaction(:) => null()  ! /reaction(i) - Plasma reaction (1 -'DT'; 2 - 'DD')
endtype

type ids_neutron_diagnostic_detectors_radiation  !
  integer(ids_int)  :: index=ids_int_invalid       ! /index - Index of radiation type
  character(len=ids_string_length), dimension(:), pointer ::converter_name => null()       ! /converter_name - Name of detector's converter for resent particle
  real(ids_real)  :: converter_volume=ids_real_invalid       ! /converter_volume - Volume of detector's converter for resent particle
  real(ids_real)  :: converter_volume_error_upper=ids_real_invalid
  real(ids_real)  :: converter_volume_error_lower=ids_real_invalid
  integer(ids_int) :: converter_volume_error_index=ids_int_invalid

  real(ids_real)  :: converter_nuclear_density=ids_real_invalid       ! /converter_nuclear_density - Nuclear density of detector's converter for resent particle
  real(ids_real)  :: converter_nuclear_density_error_upper=ids_real_invalid
  real(ids_real)  :: converter_nuclear_density_error_lower=ids_real_invalid
  integer(ids_int) :: converter_nuclear_density_error_index=ids_int_invalid

  real(ids_real),pointer  :: converter_temperature(:) => null()     ! /converter_temperature - Temperature of detector's converter
  real(ids_real),pointer  :: converter_temperature_error_upper(:) => null()
  real(ids_real),pointer  :: converter_temperature_error_lower(:) => null()
  integer(ids_int) :: converter_temperature_error_index=ids_int_invalid

endtype

! SPECIAL STRUCTURE data / time
type ids_neutron_diagnostic_detectors_mode_counting  !    Counting in Measuring Mode in Dynamic
  real(ids_real), pointer  :: data(:) => null()     ! /counting - Counting in Measuring Mode in Dynamic
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_neutron_diagnostic_detectors_mode  !
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of Measuring Mode
  type (ids_neutron_diagnostic_detectors_mode_counting) :: counting  ! /counting - Counting in Measuring Mode in Dynamic
endtype

! SPECIAL STRUCTURE data / time
type ids_neutron_diagnostic_detectors_amplitude_raw  !    Raw amplitude of the measured signal
  real(ids_real), pointer  :: data(:) => null()     ! /amplitude_raw - Raw amplitude of the measured signal
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_neutron_diagnostic_detectors_amplitude_peak  !    Processed peak amplitude of the measured signal
  real(ids_real), pointer  :: data(:) => null()     ! /amplitude_peak - Processed peak amplitude of the measured signal
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_neutron_diagnostic_detectors_spectrum  !    Detected count per energy channel as a function of time
  integer(ids_int), pointer  :: data(:,:) => null()     ! /spectrum - Detected count per energy channel as a function of time
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_neutron_diagnostic_detectors  !
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of Detector
  type (ids_neutron_diagnostic_detectors_radiation),pointer :: radiation(:) => null()  ! /radiation(i) - Radiation type on detector's converter (1 - 'neutrons'; 2 - 'gamma-rays')
  type (ids_rzphi0d_static) :: position  ! /position - Detector Position Data SHOULD BE REMOVED, REDUNDANT WITH THE NEW DETECTOR DESCRIPTION
  type (ids_detector_aperture) :: detector  ! /detector - Detector description
  type (ids_detector_aperture),pointer :: aperture(:) => null()  ! /aperture(i) - Description of a set of collimating apertures
  type (ids_neutron_diagnostic_detectors_mode),pointer :: mode(:) => null()  ! /mode(i) - Measuring Mode Properties and Data
  type (ids_detector_energy_band),pointer :: energy_band(:) => null()  ! /energy_band(i) - Set of energy bands in which neutrons are counted by the detector
  real(ids_real)  :: start_time=ids_real_invalid       ! /start_time - Time stamp of the moment diagnostic starts recording data
  real(ids_real)  :: start_time_error_upper=ids_real_invalid
  real(ids_real)  :: start_time_error_lower=ids_real_invalid
  integer(ids_int) :: start_time_error_index=ids_int_invalid

  real(ids_real)  :: end_time=ids_real_invalid       ! /end_time - Time stamp of the moment diagnostic ends recording data
  real(ids_real)  :: end_time_error_upper=ids_real_invalid
  real(ids_real)  :: end_time_error_lower=ids_real_invalid
  integer(ids_int) :: end_time_error_index=ids_int_invalid

  real(ids_real)  :: spectrum_sampling_time=ids_real_invalid       ! /spectrum_sampling_time - Sampling time used to obtain one spectrum time slice
  real(ids_real)  :: spectrum_sampling_time_error_upper=ids_real_invalid
  real(ids_real)  :: spectrum_sampling_time_error_lower=ids_real_invalid
  integer(ids_int) :: spectrum_sampling_time_error_index=ids_int_invalid

  type (ids_neutron_diagnostic_detectors_amplitude_raw) :: amplitude_raw  ! /amplitude_raw - Raw amplitude of the measured signal
  type (ids_neutron_diagnostic_detectors_amplitude_peak) :: amplitude_peak  ! /amplitude_peak - Processed peak amplitude of the measured signal
  integer(ids_int),pointer  :: spectrum_total(:) => null()      ! /spectrum_total - Detected count per energy channel, integrated over the whole acquisition duration
  type (ids_neutron_diagnostic_detectors_spectrum) :: spectrum  ! /spectrum - Detected count per energy channel as a function of time
  type (ids_neutron_diagnostic_adc) :: adc  ! /adc - Description of analogic-digital converter
  type (ids_neutron_diagnostic_supply) :: supply_high_voltage  ! /supply_high_voltage - Description of high voltage power supply
  type (ids_neutron_diagnostic_supply) :: supply_low_voltage  ! /supply_low_voltage - Description of low voltage power supply
  type (ids_neutron_diagnostic_test_generator) :: test_generator  ! /test_generator - Test generator characteristics
  type (ids_neutron_diagnostic_test_generator) :: b_field_sensor  ! /b_field_sensor - Magnetic field sensor
  type (ids_neutron_diagnostic_test_generator) :: temperature_sensor  ! /temperature_sensor - Temperature sensor
endtype

type ids_neutron_diagnostic_synthetic_signals  !
  real(ids_real),pointer  :: total_neutron_flux(:) => null()     ! /total_neutron_flux - Total Neutron Flux in Dynamic
  real(ids_real),pointer  :: total_neutron_flux_error_upper(:) => null()
  real(ids_real),pointer  :: total_neutron_flux_error_lower(:) => null()
  integer(ids_int) :: total_neutron_flux_error_index=ids_int_invalid

  real(ids_real),pointer  :: fusion_power(:) => null()     ! /fusion_power - Fusion Power
  real(ids_real),pointer  :: fusion_power_error_upper(:) => null()
  real(ids_real),pointer  :: fusion_power_error_lower(:) => null()
  integer(ids_int) :: fusion_power_error_index=ids_int_invalid

endtype

type ids_neutron_diagnostic_unit_source_radiation_reaction  !
  integer(ids_int)  :: index=ids_int_invalid       ! /index - Index of plasma reaction type. To be removed, since it is given by the index of the reaction array a
  real(ids_real),pointer  :: energy(:) => null()     ! /energy - Energy boundaries for Detector Radiator Flux
  real(ids_real),pointer  :: energy_error_upper(:) => null()
  real(ids_real),pointer  :: energy_error_lower(:) => null()
  integer(ids_int) :: energy_error_index=ids_int_invalid

  real(ids_real),pointer  :: flux(:,:) => null()     ! /flux - Radiation flux from Unit Ring Source in recent detector's converter (1 - Number of Detector, 2 - Ene
  real(ids_real),pointer  :: flux_error_upper(:,:) => null()
  real(ids_real),pointer  :: flux_error_lower(:,:) => null()
  integer(ids_int) :: flux_error_index=ids_int_invalid

  real(ids_real),pointer  :: d2flux_drdz(:,:) => null()     ! /d2flux_drdz - Second deriviation of Radiation flux from Unit Ring Source in recent detector's converter for "splin
  real(ids_real),pointer  :: d2flux_drdz_error_upper(:,:) => null()
  real(ids_real),pointer  :: d2flux_drdz_error_lower(:,:) => null()
  integer(ids_int) :: d2flux_drdz_error_index=ids_int_invalid

  real(ids_real),pointer  :: reaction_rate(:,:) => null()     ! /reaction_rate - Reaction Rate on converter's material from Unit Ring Source in recent detector's converter (1 - Numb
  real(ids_real),pointer  :: reaction_rate_error_upper(:,:) => null()
  real(ids_real),pointer  :: reaction_rate_error_lower(:,:) => null()
  integer(ids_int) :: reaction_rate_error_index=ids_int_invalid

  real(ids_real),pointer  :: sensitivity(:,:) => null()     ! /sensitivity - Sensitivity of converter's material in recent detector's converter (1 - Number of Detector, 2 - Ener
  real(ids_real),pointer  :: sensitivity_error_upper(:,:) => null()
  real(ids_real),pointer  :: sensitivity_error_lower(:,:) => null()
  integer(ids_int) :: sensitivity_error_index=ids_int_invalid

endtype

type ids_neutron_diagnostic_unit_source_radiation  !
  integer(ids_int)  :: index=ids_int_invalid       ! /index - Index of radiation type. TO BE REMOVED, since it is given by the index of the radiation array above.
  type (ids_neutron_diagnostic_unit_source_radiation_reaction),pointer :: reaction(:) => null()  ! /reaction(i) - Plasma reaction (1 - 'DT'; 2 - 'DD')
endtype

type ids_neutron_diagnostic_unit_source  !    Unit ring sources distribution
  type (ids_rz0d_static) :: position  ! /position - Position of ring unit sources inside ITER vacuum vessel
  type (ids_neutron_diagnostic_unit_source_radiation),pointer :: radiation(:) => null()  ! /radiation(i) - Radiation type on detector's converter (1 - 'neutrons'; 2 - 'gamma-rays')
endtype

type ids_neutron_diagnostic  !    Neutron diagnostic such as DNFM, NFM or MFC
  type (ids_ids_properties) :: ids_properties  ! /neutron_diagnostic/ids_properties -
  type (ids_neutron_diagnostic_characteristics) :: characteristics  ! /neutron_diagnostic/characteristics - Description of Diagnostic's module detection characteristics for differen plasma modes based on Desi
  type (ids_neutron_diagnostic_detectors),pointer :: detectors(:) => null()  ! /neutron_diagnostic/detectors(i) - Description of Detectors properties and Data in Neutron Diagnostic Module
  type (ids_neutron_diagnostic_synthetic_signals) :: synthetic_signals  ! /neutron_diagnostic/synthetic_signals - Output Data from Neutron Diagnostic's Module
  type (ids_neutron_diagnostic_unit_source),pointer :: unit_source(:) => null()  ! /neutron_diagnostic/unit_source(i) - Unit ring sources description
  type (ids_code) :: code  ! /neutron_diagnostic/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include amns_data/dd_amns_data.xsd
type ids_amns_data_data_entry  !    Definition of a given AMNS data entry
  character(len=ids_string_length), dimension(:), pointer ::description => null()       ! /description - Description of this data entry
  integer(ids_int)  :: shot=ids_int_invalid       ! /shot - Shot number = Mass*100+Nuclear_charge
  integer(ids_int)  :: run=ids_int_invalid       ! /run - Which run number is the active run number for this version
endtype

type ids_amns_data_release  !    Definition of a given release of an AMNS data release
  character(len=ids_string_length), dimension(:), pointer ::description => null()       ! /description - Description of this release
  character(len=ids_string_length), dimension(:), pointer ::date => null()       ! /date - Date of this release
  type (ids_amns_data_data_entry),pointer :: data_entry(:) => null()  ! /data_entry(i) - For this release, list of each data item (i.e. shot/run pair containing the actual data) included in
endtype

type ids_amns_data_process_element  !    Constituent entering in the composition of a reactant or product (constant)
  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifier for reaction constituent (e.g. "D", "C")
  integer(ids_int)  :: a=ids_int_invalid       ! /a - Number of nucleons in the nucleus (nuclear mass); 0 if none (e-, gamma); Not set if not important (e
  integer(ids_int)  :: z_n=ids_int_invalid       ! /z_n - Nuclear charge. 0 if none (e-, gamma)
  real(ids_real)  :: multiplicity=ids_real_invalid       ! /multiplicity - Multiplicity of the atom in the molecule
  real(ids_real)  :: multiplicity_error_upper=ids_real_invalid
  real(ids_real)  :: multiplicity_error_lower=ids_real_invalid
  integer(ids_int) :: multiplicity_error_index=ids_int_invalid

endtype

type ids_amns_data_process_reactant  !    Process reactant or product definition
  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying reaction participant (e.g. "D", "e", "W", "CD4", "photon", "n")
  type (ids_amns_data_process_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom (in such cas, this array should be of size 1) or molecule
  type (ids_identifier) :: role  ! /role - Identifier for the role of this paricipant in the reaction. For surface reactions distinguish betwee
  real(ids_real)  :: mass=ids_real_invalid       ! /mass - Mass of the participant
  real(ids_real)  :: mass_error_upper=ids_real_invalid
  real(ids_real)  :: mass_error_lower=ids_real_invalid
  integer(ids_int) :: mass_error_index=ids_int_invalid

  real(ids_real)  :: charge=ids_real_invalid       ! /charge - Charge number of the participant
  real(ids_real)  :: charge_error_upper=ids_real_invalid
  real(ids_real)  :: charge_error_lower=ids_real_invalid
  integer(ids_int) :: charge_error_index=ids_int_invalid

  integer(ids_int)  :: relative_charge=ids_int_invalid       ! /relative_charge - This is a flag indicating that charges are absolute (if set to 0), relative (if 1) or irrelevant (-1
  real(ids_real)  :: multiplicity=ids_real_invalid       ! /multiplicity - Multiplicity in the reaction
  real(ids_real)  :: multiplicity_error_upper=ids_real_invalid
  real(ids_real)  :: multiplicity_error_lower=ids_real_invalid
  integer(ids_int) :: multiplicity_error_index=ids_int_invalid

  integer(ids_int),pointer  :: metastable(:) => null()      ! /metastable - An array identifying the metastable; if zero-length, then not a metastable; if of length 1, then the
  character(len=ids_string_length), dimension(:), pointer ::metastable_label => null()       ! /metastable_label - Label identifying in text form the metastable

endtype

type ids_amns_data_process_charge_state  !    Process tables for a given charge state. Only one table is used for that process, defined by process(:)/table_dimension
  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  real(ids_real)  :: z_min=ids_real_invalid       ! /z_min - Minimum Z of the charge state bundle
  real(ids_real)  :: z_min_error_upper=ids_real_invalid
  real(ids_real)  :: z_min_error_lower=ids_real_invalid
  integer(ids_int) :: z_min_error_index=ids_int_invalid

  real(ids_real)  :: z_max=ids_real_invalid       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(ids_real)  :: z_max_error_upper=ids_real_invalid
  real(ids_real)  :: z_max_error_lower=ids_real_invalid
  integer(ids_int) :: z_max_error_index=ids_int_invalid

  real(ids_real)  :: table_0d=ids_real_invalid       ! /table_0d - 0D table describing the process data
  real(ids_real)  :: table_0d_error_upper=ids_real_invalid
  real(ids_real)  :: table_0d_error_lower=ids_real_invalid
  integer(ids_int) :: table_0d_error_index=ids_int_invalid

  real(ids_real),pointer  :: table_1d(:) => null()     ! /table_1d - 1D table describing the process data
  real(ids_real),pointer  :: table_1d_error_upper(:) => null()
  real(ids_real),pointer  :: table_1d_error_lower(:) => null()
  integer(ids_int) :: table_1d_error_index=ids_int_invalid

  real(ids_real),pointer  :: table_2d(:,:) => null()     ! /table_2d - 2D table describing the process data
  real(ids_real),pointer  :: table_2d_error_upper(:,:) => null()
  real(ids_real),pointer  :: table_2d_error_lower(:,:) => null()
  integer(ids_int) :: table_2d_error_index=ids_int_invalid

  real(ids_real),pointer  :: table_3d(:,:,:) => null()     ! /table_3d - 3D table describing the process data
  real(ids_real),pointer  :: table_3d_error_upper(:,:,:) => null()
  real(ids_real),pointer  :: table_3d_error_lower(:,:,:) => null()
  integer(ids_int) :: table_3d_error_index=ids_int_invalid

  real(ids_real),pointer  :: table_4d(:,:,:,:) => null()     ! /table_4d - 4D table describing the process data
  real(ids_real),pointer  :: table_4d_error_upper(:,:,:,:) => null()
  real(ids_real),pointer  :: table_4d_error_lower(:,:,:,:) => null()
  integer(ids_int) :: table_4d_error_index=ids_int_invalid

  real(ids_real),pointer  :: table_5d(:,:,:,:,:) => null()     ! /table_5d - 5D table describing the process data
  real(ids_real),pointer  :: table_5d_error_upper(:,:,:,:,:) => null()
  real(ids_real),pointer  :: table_5d_error_lower(:,:,:,:,:) => null()
  integer(ids_int) :: table_5d_error_index=ids_int_invalid

  real(ids_real),pointer  :: table_6d(:,:,:,:,:,:) => null()     ! /table_6d - 6D table describing the process data
  real(ids_real),pointer  :: table_6d_error_upper(:,:,:,:,:,:) => null()
  real(ids_real),pointer  :: table_6d_error_lower(:,:,:,:,:,:) => null()
  integer(ids_int) :: table_6d_error_index=ids_int_invalid

endtype

type ids_amns_data_process  !    Definition of a process and its data
  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying the process (e.g. EI, RC, ...)
  type (ids_amns_data_process_reactant),pointer :: reactants(:) => null()  ! /reactants(i) - Set of reactants involved in this process
  type (ids_amns_data_process_reactant),pointer :: products(:) => null()  ! /products(i) - Set of products resulting of this process
  integer(ids_int)  :: table_dimension=ids_int_invalid       ! /table_dimension - Table dimensionality of the process (1 to 6), valid for all charge states. Indicates which of the ta
  integer(ids_int)  :: coordinate_index=ids_int_invalid       ! /coordinate_index - Index in tables_coord, specifying what coordinate systems to use for this process (valid for all tab
  character(len=ids_string_length), dimension(:), pointer ::result_label => null()       ! /result_label - Description of the process result (rate, cross section, sputtering yield, ...)
  character(len=ids_string_length), dimension(:), pointer ::result_units => null()       ! /result_units - Units of the process result
  integer(ids_int)  :: result_transformation=ids_int_invalid       ! /result_transformation - Transformation of the process result. Integer flag: 0=no transformation; 1=10^; 2=exp()
  type (ids_amns_data_process_charge_state),pointer :: charge_state(:) => null()  ! /charge_state(i) - Process tables for a set of charge states. Only one table is used for that process, defined by proce
endtype

type ids_amns_data_coordinate_system_coordinate  !    Description of a coordinate for atomic data tables. Can be either a range of real values or a set of discrete values (if interp_ty
  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - Description of coordinate (e.g. "Electron temperature")
  real(ids_real),pointer  :: values(:) => null()     ! /values - Coordinate values
  real(ids_real),pointer  :: values_error_upper(:) => null()
  real(ids_real),pointer  :: values_error_lower(:) => null()
  integer(ids_int) :: values_error_index=ids_int_invalid

  integer(ids_int)  :: interpolation_type=ids_int_invalid       ! /interpolation_type - Interpolation strategy in this coordinate direction. Integer flag: 0=discrete (no interpolation); 1=
  integer(ids_int),pointer  :: extrapolation_type(:) => null()      ! /extrapolation_type - Extrapolation strategy when leaving the domain. The first value of the vector describes the behaviou
  character(len=ids_string_length), dimension(:), pointer ::value_labels => null()       ! /value_labels - String description of discrete coordinate values (if interpolation_type=0). E.g., for spectroscopic
  character(len=ids_string_length), dimension(:), pointer ::units => null()       ! /units - Units of coordinate (e.g. eV)
  integer(ids_int)  :: transformation=ids_int_invalid       ! /transformation - Coordinate transformation applied to coordinate values stored in coord. Integer flag: 0=none; 1=log1
  integer(ids_int)  :: spacing=ids_int_invalid       ! /spacing - Flag for specific coordinate spacing (for optimization purposes). Integer flag: 0=undefined; 1=unifo
endtype

type ids_amns_data_coordinate_system  !    Description of a coordinate system for atomic data tables
  type (ids_amns_data_coordinate_system_coordinate),pointer :: coordinate(:) => null()  ! /coordinate(i) - Set of coordinates for that coordinate system. A coordinate an be either a range of real values or a
endtype

type ids_amns_data  !    Atomic, molecular, nuclear and surface physics data. Each occurrence contains the data for a given element (nuclear charge), descr
  type (ids_ids_properties) :: ids_properties  ! /amns_data/ids_properties -
  real(ids_real)  :: z_n=ids_real_invalid       ! /amns_data/z_n - Nuclear charge
  real(ids_real)  :: z_n_error_upper=ids_real_invalid
  real(ids_real)  :: z_n_error_lower=ids_real_invalid
  integer(ids_int) :: z_n_error_index=ids_int_invalid

  real(ids_real)  :: a=ids_real_invalid       ! /amns_data/a - Mass of atom
  real(ids_real)  :: a_error_upper=ids_real_invalid
  real(ids_real)  :: a_error_lower=ids_real_invalid
  integer(ids_int) :: a_error_index=ids_int_invalid

  type (ids_amns_data_process),pointer :: process(:) => null()  ! /amns_data/process(i) - Description and data for a set of physical processes.
  type (ids_amns_data_coordinate_system),pointer :: coordinate_system(:) => null()  ! /amns_data/coordinate_system(i) - Array of possible coordinate systems for process tables
  type (ids_amns_data_release),pointer :: release(:) => null()  ! /amns_data/release(i) - List of available releases of the AMNS data; each element contains information about the AMNS data t
  type (ids_code) :: code  ! /amns_data/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include bolometer/dd_bolometer.xsd
! SPECIAL STRUCTURE data / time
type ids_bolometer_channel_power  !    Power received on the detector
  real(ids_real), pointer  :: data(:) => null()     ! /power - Power received on the detector
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_bolometer_channel_validity_timed  !    Indicator of the validity of the channel as a function of time (0 means valid, negative values mean non-valid)
  integer(ids_int), pointer  :: data(:) => null()      ! /validity_timed - Indicator of the validity of the channel as a function of time (0 means valid, negative values mean
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_bolometer_channel  !    Bolometer channel
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the channel
  character(len=ids_string_length), dimension(:), pointer ::identifier => null()       ! /identifier - ID of the channel
  type (ids_detector_aperture) :: detector  ! /detector - Detector description
  type (ids_detector_aperture),pointer :: aperture(:) => null()  ! /aperture(i) - Description of a set of collimating apertures
  type (ids_line_of_sight_3points) :: line_of_sight  ! /line_of_sight - Description of the reference line of sight of the channel, defined by two points when the beam is no
  type (ids_bolometer_channel_power) :: power  ! /power - Power received on the detector
  type (ids_bolometer_channel_validity_timed) :: validity_timed  ! /validity_timed - Indicator of the validity of the channel as a function of time (0 means valid, negative values mean
  integer(ids_int)  :: validity=ids_int_invalid       ! /validity - Indicator of the validity of the channel for the whole acquisition period (0 means valid, negative v
endtype

type ids_bolometer  !    Bolometer diagnostic
  type (ids_ids_properties) :: ids_properties  ! /bolometer/ids_properties -
  type (ids_bolometer_channel),pointer :: channel(:) => null()  ! /bolometer/channel(i) - Set of channels (detector or pixel of a camera)
  real(ids_real),pointer  :: power_radiated_total(:) => null()     ! /bolometer/power_radiated_total - Total radiated power reconstructed from bolometry data
  real(ids_real),pointer  :: power_radiated_total_error_upper(:) => null()
  real(ids_real),pointer  :: power_radiated_total_error_lower(:) => null()
  integer(ids_int) :: power_radiated_total_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_radiated_inside_lcfs(:) => null()     ! /bolometer/power_radiated_inside_lcfs - Radiated power from the plasma inside the Last Closed Flux Surface, reconstructed from bolometry dat
  real(ids_real),pointer  :: power_radiated_inside_lcfs_error_upper(:) => null()
  real(ids_real),pointer  :: power_radiated_inside_lcfs_error_lower(:) => null()
  integer(ids_int) :: power_radiated_inside_lcfs_error_index=ids_int_invalid

  integer(ids_int),pointer  :: power_radiated_validity(:) => null()      ! /bolometer/power_radiated_validity - Validity flag related to the radiated power reconstructions
  type (ids_code) :: code  ! /bolometer/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include charge_exchange/dd_charge_exchange.xsd
! SPECIAL STRUCTURE data / time
type ids_charge_exchange_channel_temperature  !    Ion temperature (of the emmitting impurity)
  real(ids_real), pointer  :: data(:) => null()     ! /temperature - Ion temperature (of the emmitting impurity)
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_charge_exchange_channel_velocity_tor  !    Toroidal velocity (of the emmitting impurity)
  real(ids_real), pointer  :: data(:) => null()     ! /velocity_tor - Toroidal velocity (of the emmitting impurity)
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_charge_exchange_channel_velocity_pol  !    Poloidal velocity (of the emmitting impurity)
  real(ids_real), pointer  :: data(:) => null()     ! /velocity_pol - Poloidal velocity (of the emmitting impurity)
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_charge_exchange_channel  !    Charge exchange channel
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the channel
  character(len=ids_string_length), dimension(:), pointer ::identifier => null()       ! /identifier - ID of the channel
  real(ids_real)  :: a=ids_real_invalid       ! /a - Mass of atom of the emmitting impurity for this channel
  real(ids_real)  :: a_error_upper=ids_real_invalid
  real(ids_real)  :: a_error_lower=ids_real_invalid
  integer(ids_int) :: a_error_index=ids_int_invalid

  real(ids_real)  :: z_ion=ids_real_invalid       ! /z_ion - Ion charge of the emmitting impurity for this channel
  real(ids_real)  :: z_ion_error_upper=ids_real_invalid
  real(ids_real)  :: z_ion_error_lower=ids_real_invalid
  integer(ids_int) :: z_ion_error_index=ids_int_invalid

  real(ids_real)  :: z_n=ids_real_invalid       ! /z_n - Nuclear charge of the emmitting impurity for this channel
  real(ids_real)  :: z_n_error_upper=ids_real_invalid
  real(ids_real)  :: z_n_error_lower=ids_real_invalid
  integer(ids_int) :: z_n_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying the emmitting impurity (e.g. H+, D+, T+, He+2, C+6, ...) for this channel
  type (ids_rzphi1d_dynamic_aos1) :: position  ! /position - Position of the measurements
  type (ids_charge_exchange_channel_temperature) :: temperature  ! /temperature - Ion temperature (of the emmitting impurity)
  type (ids_charge_exchange_channel_velocity_tor) :: velocity_tor  ! /velocity_tor - Toroidal velocity (of the emmitting impurity)
  type (ids_charge_exchange_channel_velocity_pol) :: velocity_pol  ! /velocity_pol - Poloidal velocity (of the emmitting impurity)
endtype

type ids_charge_exchange  !    Charge exchange spectroscopy diagnostic
  type (ids_ids_properties) :: ids_properties  ! /charge_exchange/ids_properties -
  type (ids_charge_exchange_channel),pointer :: channel(:) => null()  ! /charge_exchange/channel(i) - Set of channels (lines-of-sight)
  type (ids_code) :: code  ! /charge_exchange/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include controllers/dd_controllers.xsd
! SPECIAL STRUCTURE data / time
type ids_controllers_statespace_a  !    A matrix
  real(ids_real), pointer  :: data(:,:,:) => null()     ! /a - A matrix
  real(ids_real), pointer  :: data_error_upper(:,:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_controllers_statespace_b  !    B matrix
  real(ids_real), pointer  :: data(:,:,:) => null()     ! /b - B matrix
  real(ids_real), pointer  :: data_error_upper(:,:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_controllers_statespace_c  !    C matrix
  real(ids_real), pointer  :: data(:,:,:) => null()     ! /c - C matrix
  real(ids_real), pointer  :: data_error_upper(:,:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_controllers_statespace_d  !    D matrix, normally proper and D=0
  real(ids_real), pointer  :: data(:,:,:) => null()     ! /d - D matrix, normally proper and D=0
  real(ids_real), pointer  :: data_error_upper(:,:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_controllers_statespace_deltat  !    Discrete time sampling interval ; if less than 1e-10, the controller is considered to be expressed in continuous time
  real(ids_real), pointer  :: data(:) => null()     ! /deltat - Discrete time sampling interval ; if less than 1e-10, the controller is considered to be expressed i
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_controllers_statespace  !    type for a statespace controller
  character(len=ids_string_length), dimension(:), pointer ::state_names => null()       ! /state_names - Names of the states
  type (ids_controllers_statespace_a) :: a  ! /a - A matrix
  type (ids_controllers_statespace_b) :: b  ! /b - B matrix
  type (ids_controllers_statespace_c) :: c  ! /c - C matrix
  type (ids_controllers_statespace_d) :: d  ! /d - D matrix, normally proper and D=0
  type (ids_controllers_statespace_deltat) :: deltat  ! /deltat - Discrete time sampling interval ; if less than 1e-10, the controller is considered to be expressed i
endtype

! SPECIAL STRUCTURE data / time
type ids_controllers_pid_p  !    Proportional term
  real(ids_real), pointer  :: data(:,:,:) => null()     ! /p - Proportional term
  real(ids_real), pointer  :: data_error_upper(:,:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_controllers_pid_i  !    Integral term
  real(ids_real), pointer  :: data(:,:,:) => null()     ! /i - Integral term
  real(ids_real), pointer  :: data_error_upper(:,:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_controllers_pid_d  !    Derivative term
  real(ids_real), pointer  :: data(:,:,:) => null()     ! /d - Derivative term
  real(ids_real), pointer  :: data_error_upper(:,:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_controllers_pid_tau  !    Filter time-constant for the D-term
  real(ids_real), pointer  :: data(:) => null()     ! /tau - Filter time-constant for the D-term
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_controllers_pid  !    type for a MIMO PID controller
  type (ids_controllers_pid_p) :: p  ! /p - Proportional term
  type (ids_controllers_pid_i) :: i  ! /i - Integral term
  type (ids_controllers_pid_d) :: d  ! /d - Derivative term
  type (ids_controllers_pid_tau) :: tau  ! /tau - Filter time-constant for the D-term
endtype

! SPECIAL STRUCTURE data / time
type ids_controllers_linear_controller_inputs  !    Input signals; the timebase is common to inputs and outputs for any particular controller
  real(ids_real), pointer  :: data(:,:) => null()     ! /inputs - Input signals; the timebase is common to inputs and outputs for any particular controller
  real(ids_real), pointer  :: data_error_upper(:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_controllers_linear_controller_outputs  !    Output signals; the timebase is common to inputs and outputs for any particular controller
  real(ids_real), pointer  :: data(:,:) => null()     ! /outputs - Output signals; the timebase is common to inputs and outputs for any particular controller
  real(ids_real), pointer  :: data_error_upper(:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_controllers_linear_controller  !    type for a linear controller
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of this controller
  character(len=ids_string_length), dimension(:), pointer ::description => null()       ! /description - Description of this controller
  character(len=ids_string_length), dimension(:), pointer ::controller_class => null()       ! /controller_class - One of a known class of controllers
  character(len=ids_string_length), dimension(:), pointer ::input_names => null()       ! /input_names - Names of the input signals, following the SDN convention
  character(len=ids_string_length), dimension(:), pointer ::output_names => null()       ! /output_names - Names of the output signals following the SDN convention
  type (ids_controllers_statespace) :: statespace  ! /statespace - Statespace controller in discrete or continuous time
  type (ids_controllers_pid) :: pid  ! /pid - Filtered PID controller
  type (ids_controllers_linear_controller_inputs) :: inputs  ! /inputs - Input signals; the timebase is common to inputs and outputs for any particular controller
  type (ids_controllers_linear_controller_outputs) :: outputs  ! /outputs - Output signals; the timebase is common to inputs and outputs for any particular controller
endtype

! SPECIAL STRUCTURE data / time
type ids_controllers_nonlinear_controller_inputs  !    Input signals; the timebase is common  to inputs and outputs for any particular controller
  real(ids_real), pointer  :: data(:,:) => null()     ! /inputs - Input signals; the timebase is common  to inputs and outputs for any particular controller
  real(ids_real), pointer  :: data_error_upper(:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_controllers_nonlinear_controller_outputs  !    Output signals; the timebase is common  to inputs and outputs for any particular controller
  real(ids_real), pointer  :: data(:,:) => null()     ! /outputs - Output signals; the timebase is common  to inputs and outputs for any particular controller
  real(ids_real), pointer  :: data_error_upper(:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_controllers_nonlinear_controller  !    Type for a nonlinear controller
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of this controller
  character(len=ids_string_length), dimension(:), pointer ::description => null()       ! /description - Description of this controller
  character(len=ids_string_length), dimension(:), pointer ::controller_class => null()       ! /controller_class - One of a known class of controllers
  character(len=ids_string_length), dimension(:), pointer ::input_names => null()       ! /input_names - Names of the input signals, following the SDN convention
  character(len=ids_string_length), dimension(:), pointer ::output_names => null()       ! /output_names - Output signal names following the SDN convention
  character(len=ids_string_length), dimension(:), pointer ::function => null()       ! /function - Method to be defined
  type (ids_controllers_nonlinear_controller_inputs) :: inputs  ! /inputs - Input signals; the timebase is common  to inputs and outputs for any particular controller
  type (ids_controllers_nonlinear_controller_outputs) :: outputs  ! /outputs - Output signals; the timebase is common  to inputs and outputs for any particular controller
endtype

type ids_controllers  !    Feedback and feedforward controllers
  type (ids_ids_properties) :: ids_properties  ! /controllers/ids_properties -
  type (ids_controllers_linear_controller),pointer :: linear_controller(:) => null()  ! /controllers/linear_controller(i) - A linear controller, this is rather conventional
  type (ids_controllers_nonlinear_controller),pointer :: nonlinear_controller(:) => null()  ! /controllers/nonlinear_controller(i) - A non-linear controller, this is less conventional and will have to be developed
  real(ids_real), pointer  :: time(:) => null()  ! time
  type (ids_code) :: code  ! /controllers/code -
endtype

! ***********  Include core_instant_changes/dd_core_instant_changes.xsd
type ids_core_instant_changes_change_profiles  !    instant_change terms for a given time slice
  type (ids_core_radial_grid) :: grid  ! /grid - Radial grid
  type (ids_core_profiles_profiles_1d_electrons) :: electrons  ! /electrons - Change of electrons-related quantities
  real(ids_real),pointer  :: t_i_average(:) => null()     ! /t_i_average - change of average ion temperature
  real(ids_real),pointer  :: t_i_average_error_upper(:) => null()
  real(ids_real),pointer  :: t_i_average_error_lower(:) => null()
  integer(ids_int) :: t_i_average_error_index=ids_int_invalid

  real(ids_real),pointer  :: momentum_tor(:) => null()     ! /momentum_tor - change of total toroidal momentum
  real(ids_real),pointer  :: momentum_tor_error_upper(:) => null()
  real(ids_real),pointer  :: momentum_tor_error_lower(:) => null()
  integer(ids_int) :: momentum_tor_error_index=ids_int_invalid

  type (ids_core_profile_ions),pointer :: ion(:) => null()  ! /ion(i) - changes related to the different ions species
  type (ids_core_profile_neutral),pointer :: neutral(:) => null()  ! /neutral(i) - changes related to the different neutral species
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_core_instant_changes_change  !    instant_change terms for a given instant_change
  type (ids_identifier) :: identifier  ! /identifier - Instant change term identifier
  type (ids_core_profiles_profiles_1d),pointer :: profiles_1d(:) => null()  ! /profiles_1d(i) - Changes in 1D core profiles for various time slices. This structure mirrors core_profiles/profiles_1
endtype

type ids_core_instant_changes  !    Instant changes of the radial core plasma profiles due to pellet, MHD, ...
  type (ids_ids_properties) :: ids_properties  ! /core_instant_changes/ids_properties -
  type (ids_b_tor_vacuum_1) :: vacuum_toroidal_field  ! /core_instant_changes/vacuum_toroidal_field - Characteristics of the vacuum toroidal field (used in Rho_Tor definition and in the normalization of
  type (ids_core_instant_changes_change),pointer :: change(:) => null()  ! /core_instant_changes/change(i) - Set of instant change terms (each being due to a different phenomenon)
  type (ids_code) :: code  ! /core_instant_changes/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include core_profiles/dd_core_profiles.xsd
type ids_core_profiles_global_quantities  !    Various global quantities calculated from the fields solved in the transport equations and from the Derived Profiles
  real(ids_real),pointer  :: ip(:) => null()     ! /ip - Total plasma current
  real(ids_real),pointer  :: ip_error_upper(:) => null()
  real(ids_real),pointer  :: ip_error_lower(:) => null()
  integer(ids_int) :: ip_error_index=ids_int_invalid

  real(ids_real),pointer  :: current_non_inductive(:) => null()     ! /current_non_inductive - Total non-inductive parallel current
  real(ids_real),pointer  :: current_non_inductive_error_upper(:) => null()
  real(ids_real),pointer  :: current_non_inductive_error_lower(:) => null()
  integer(ids_int) :: current_non_inductive_error_index=ids_int_invalid

  real(ids_real),pointer  :: current_bootstrap(:) => null()     ! /current_bootstrap - Bootstrap current
  real(ids_real),pointer  :: current_bootstrap_error_upper(:) => null()
  real(ids_real),pointer  :: current_bootstrap_error_lower(:) => null()
  integer(ids_int) :: current_bootstrap_error_index=ids_int_invalid

  real(ids_real),pointer  :: v_loop(:) => null()     ! /v_loop - LCFS loop voltage
  real(ids_real),pointer  :: v_loop_error_upper(:) => null()
  real(ids_real),pointer  :: v_loop_error_lower(:) => null()
  integer(ids_int) :: v_loop_error_index=ids_int_invalid

  real(ids_real),pointer  :: li(:) => null()     ! /li - Internal inductance. The li_3 definition is used, i.e. li_3 = 2/R0/mu0^2/Ip^2 * int(Bp^2 dV).
  real(ids_real),pointer  :: li_error_upper(:) => null()
  real(ids_real),pointer  :: li_error_lower(:) => null()
  integer(ids_int) :: li_error_index=ids_int_invalid

  real(ids_real),pointer  :: li_3(:) => null()     ! /li_3 - Internal inductance. The li_3 definition is used, i.e. li_3 = 2/R0/mu0^2/Ip^2 * int(Bp^2 dV).
  real(ids_real),pointer  :: li_3_error_upper(:) => null()
  real(ids_real),pointer  :: li_3_error_lower(:) => null()
  integer(ids_int) :: li_3_error_index=ids_int_invalid

  real(ids_real),pointer  :: beta_tor(:) => null()     ! /beta_tor - Toroidal beta, defined as the volume-averaged total perpendicular pressure divided by (B0^2/(2*mu0))
  real(ids_real),pointer  :: beta_tor_error_upper(:) => null()
  real(ids_real),pointer  :: beta_tor_error_lower(:) => null()
  integer(ids_int) :: beta_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: beta_tor_norm(:) => null()     ! /beta_tor_norm - Normalised toroidal beta, defined as 100 * beta_tor * a[m] * B0 [T] / ip [MA]
  real(ids_real),pointer  :: beta_tor_norm_error_upper(:) => null()
  real(ids_real),pointer  :: beta_tor_norm_error_lower(:) => null()
  integer(ids_int) :: beta_tor_norm_error_index=ids_int_invalid

  real(ids_real),pointer  :: beta_pol(:) => null()     ! /beta_pol - Poloidal beta. Defined as betap = 4 int(p dV) / [R_0 * mu_0 * Ip^2]
  real(ids_real),pointer  :: beta_pol_error_upper(:) => null()
  real(ids_real),pointer  :: beta_pol_error_lower(:) => null()
  integer(ids_int) :: beta_pol_error_index=ids_int_invalid

  real(ids_real),pointer  :: energy_diamagnetic(:) => null()     ! /energy_diamagnetic - Plasma energy content = 3/2 * integral over the plasma volume of the total perpendicular pressure
  real(ids_real),pointer  :: energy_diamagnetic_error_upper(:) => null()
  real(ids_real),pointer  :: energy_diamagnetic_error_lower(:) => null()
  integer(ids_int) :: energy_diamagnetic_error_index=ids_int_invalid

endtype

type ids_core_profiles  !    Core plasma radial profiles
  type (ids_ids_properties) :: ids_properties  ! /core_profiles/ids_properties -
  type (ids_core_profiles_profiles_1d),pointer :: profiles_1d(:) => null()  ! /core_profiles/profiles_1d(i) - Core plasma radial profiles for various time slices
  type (ids_core_profiles_global_quantities) :: global_quantities  ! /core_profiles/global_quantities - Various global quantities derived from the profiles
  type (ids_b_tor_vacuum_1) :: vacuum_toroidal_field  ! /core_profiles/vacuum_toroidal_field - Characteristics of the vacuum toroidal field (used in rho_tor definition and in the normalization of
  type (ids_code) :: code  ! /core_profiles/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include core_sources/dd_core_sources.xsd
type ids_core_sources_source_profiles_1d_particles_decomposed_3  !    Source terms decomposed for the particle transport equation, assuming core_radial_grid 3 levels above
  real(ids_real),pointer  :: implicit_part(:) => null()     ! /implicit_part - Implicit part of the source term, i.e. to be multiplied by the equation's primary quantity
  real(ids_real),pointer  :: implicit_part_error_upper(:) => null()
  real(ids_real),pointer  :: implicit_part_error_lower(:) => null()
  integer(ids_int) :: implicit_part_error_index=ids_int_invalid

  real(ids_real),pointer  :: explicit_part(:) => null()     ! /explicit_part - Explicit part of the source term
  real(ids_real),pointer  :: explicit_part_error_upper(:) => null()
  real(ids_real),pointer  :: explicit_part_error_lower(:) => null()
  integer(ids_int) :: explicit_part_error_index=ids_int_invalid

endtype

type ids_core_sources_source_profiles_1d_particles_decomposed_4  !    Source terms decomposed for the particle transport equation, assuming core_radial_grid 4 levels above
  real(ids_real),pointer  :: implicit_part(:) => null()     ! /implicit_part - Implicit part of the source term, i.e. to be multiplied by the equation's primary quantity
  real(ids_real),pointer  :: implicit_part_error_upper(:) => null()
  real(ids_real),pointer  :: implicit_part_error_lower(:) => null()
  integer(ids_int) :: implicit_part_error_index=ids_int_invalid

  real(ids_real),pointer  :: explicit_part(:) => null()     ! /explicit_part - Explicit part of the source term
  real(ids_real),pointer  :: explicit_part_error_upper(:) => null()
  real(ids_real),pointer  :: explicit_part_error_lower(:) => null()
  integer(ids_int) :: explicit_part_error_index=ids_int_invalid

endtype

type ids_core_sources_source_profiles_1d_momentum_decomposed_4  !    Source terms decomposed for the momentum transport equation, assuming core_radial_grid 4 levels above
  real(ids_real),pointer  :: implicit_part(:) => null()     ! /implicit_part - Implicit part of the source term, i.e. to be multiplied by the equation's primary quantity
  real(ids_real),pointer  :: implicit_part_error_upper(:) => null()
  real(ids_real),pointer  :: implicit_part_error_lower(:) => null()
  integer(ids_int) :: implicit_part_error_index=ids_int_invalid

  real(ids_real),pointer  :: explicit_part(:) => null()     ! /explicit_part - Explicit part of the source term
  real(ids_real),pointer  :: explicit_part_error_upper(:) => null()
  real(ids_real),pointer  :: explicit_part_error_lower(:) => null()
  integer(ids_int) :: explicit_part_error_index=ids_int_invalid

endtype

type ids_core_sources_source_profiles_1d_energy_decomposed_4  !    Source terms decomposed for the energy transport equation, assuming core_radial_grid 4 levels above
  real(ids_real),pointer  :: implicit_part(:) => null()     ! /implicit_part - Implicit part of the source term, i.e. to be multiplied by the equation's primary quantity
  real(ids_real),pointer  :: implicit_part_error_upper(:) => null()
  real(ids_real),pointer  :: implicit_part_error_lower(:) => null()
  integer(ids_int) :: implicit_part_error_index=ids_int_invalid

  real(ids_real),pointer  :: explicit_part(:) => null()     ! /explicit_part - Explicit part of the source term
  real(ids_real),pointer  :: explicit_part_error_upper(:) => null()
  real(ids_real),pointer  :: explicit_part_error_lower(:) => null()
  integer(ids_int) :: explicit_part_error_index=ids_int_invalid

endtype

type ids_core_sources_source_profiles_1d_energy_decomposed_3  !    Source terms decomposed for the energy transport equation, assuming core_radial_grid 3 levels above
  real(ids_real),pointer  :: implicit_part(:) => null()     ! /implicit_part - Implicit part of the source term, i.e. to be multiplied by the equation's primary quantity
  real(ids_real),pointer  :: implicit_part_error_upper(:) => null()
  real(ids_real),pointer  :: implicit_part_error_lower(:) => null()
  integer(ids_int) :: implicit_part_error_index=ids_int_invalid

  real(ids_real),pointer  :: explicit_part(:) => null()     ! /explicit_part - Explicit part of the source term
  real(ids_real),pointer  :: explicit_part_error_upper(:) => null()
  real(ids_real),pointer  :: explicit_part_error_lower(:) => null()
  integer(ids_int) :: explicit_part_error_index=ids_int_invalid

endtype

type ids_core_sources_source_profiles_1d_energy_decomposed_2  !    Source terms decomposed for the energy transport equation, assuming core_radial_grid 2 levels above
  real(ids_real),pointer  :: implicit_part(:) => null()     ! /implicit_part - Implicit part of the source term, i.e. to be multiplied by the equation's primary quantity
  real(ids_real),pointer  :: implicit_part_error_upper(:) => null()
  real(ids_real),pointer  :: implicit_part_error_lower(:) => null()
  integer(ids_int) :: implicit_part_error_index=ids_int_invalid

  real(ids_real),pointer  :: explicit_part(:) => null()     ! /explicit_part - Explicit part of the source term
  real(ids_real),pointer  :: explicit_part_error_upper(:) => null()
  real(ids_real),pointer  :: explicit_part_error_lower(:) => null()
  integer(ids_int) :: explicit_part_error_index=ids_int_invalid

endtype

type ids_core_sources_source_profiles_1d_neutral_state  !    Source terms related to the a given state of the neutral species
  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying state
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  type (ids_identifier) :: neutral_type  ! /neutral_type - Neutral type (if the considered state is a neutral), in terms of energy. ID =1: cold; 2: thermal; 3:
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(ids_real),pointer  :: particles(:) => null()     ! /particles - Source term for the state density transport equation
  real(ids_real),pointer  :: particles_error_upper(:) => null()
  real(ids_real),pointer  :: particles_error_lower(:) => null()
  integer(ids_int) :: particles_error_index=ids_int_invalid

  real(ids_real),pointer  :: energy(:) => null()     ! /energy - Source terms for the state energy transport equation
  real(ids_real),pointer  :: energy_error_upper(:) => null()
  real(ids_real),pointer  :: energy_error_lower(:) => null()
  integer(ids_int) :: energy_error_index=ids_int_invalid

endtype

type ids_core_sources_source_profiles_1d_neutral  !    Source terms related to a given neutral species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying the neutral species (e.g. H, D, T, He, C, ...)
  integer(ids_int)  :: ion_index=ids_int_invalid       ! /ion_index - Index of the corresponding ion species in the ../../ion array
  real(ids_real),pointer  :: particles(:) => null()     ! /particles - Source term for neutral density equation
  real(ids_real),pointer  :: particles_error_upper(:) => null()
  real(ids_real),pointer  :: particles_error_lower(:) => null()
  integer(ids_int) :: particles_error_index=ids_int_invalid

  real(ids_real),pointer  :: energy(:) => null()     ! /energy - Source term for the neutral energy transport equation.
  real(ids_real),pointer  :: energy_error_upper(:) => null()
  real(ids_real),pointer  :: energy_error_lower(:) => null()
  integer(ids_int) :: energy_error_index=ids_int_invalid

  integer(ids_int)  :: multiple_states_flag=ids_int_invalid       ! /multiple_states_flag - Multiple states calculation flag : 0-Only one state is considered; 1-Multiple states are considered
  type (ids_core_sources_source_profiles_1d_neutral_state),pointer :: state(:) => null()  ! /state(i) - Source terms related to the different charge states of the species (energy, excitation, ...)
endtype

type ids_core_sources_source_profiles_1d_components_2  !    Source terms for vector components in predefined directions, assuming core_radial_grid 2 levels above
  real(ids_real),pointer  :: radial(:) => null()     ! /radial - Radial component
  real(ids_real),pointer  :: radial_error_upper(:) => null()
  real(ids_real),pointer  :: radial_error_lower(:) => null()
  integer(ids_int) :: radial_error_index=ids_int_invalid

  real(ids_real),pointer  :: diamagnetic(:) => null()     ! /diamagnetic - Diamagnetic component
  real(ids_real),pointer  :: diamagnetic_error_upper(:) => null()
  real(ids_real),pointer  :: diamagnetic_error_lower(:) => null()
  integer(ids_int) :: diamagnetic_error_index=ids_int_invalid

  real(ids_real),pointer  :: parallel(:) => null()     ! /parallel - Parallel component
  real(ids_real),pointer  :: parallel_error_upper(:) => null()
  real(ids_real),pointer  :: parallel_error_lower(:) => null()
  integer(ids_int) :: parallel_error_index=ids_int_invalid

  real(ids_real),pointer  :: poloidal(:) => null()     ! /poloidal - Poloidal component
  real(ids_real),pointer  :: poloidal_error_upper(:) => null()
  real(ids_real),pointer  :: poloidal_error_lower(:) => null()
  integer(ids_int) :: poloidal_error_index=ids_int_invalid

  real(ids_real),pointer  :: toroidal(:) => null()     ! /toroidal - Toroidal component
  real(ids_real),pointer  :: toroidal_error_upper(:) => null()
  real(ids_real),pointer  :: toroidal_error_lower(:) => null()
  integer(ids_int) :: toroidal_error_index=ids_int_invalid

  type (ids_core_sources_source_profiles_1d_energy_decomposed_4) :: toroidal_decomposed  ! /toroidal_decomposed - Decomposition of the source term for ion toroidal momentum equation into implicit and explicit parts
endtype

type ids_core_sources_source_profiles_1d_ions_charge_states  !    Source terms related to the a given state of the ion species
  real(ids_real)  :: z_min=ids_real_invalid       ! /z_min - Minimum Z of the charge state bundle
  real(ids_real)  :: z_min_error_upper=ids_real_invalid
  real(ids_real)  :: z_min_error_lower=ids_real_invalid
  integer(ids_int) :: z_min_error_index=ids_int_invalid

  real(ids_real)  :: z_max=ids_real_invalid       ! /z_max - Maximum Z of the charge state bundle
  real(ids_real)  :: z_max_error_upper=ids_real_invalid
  real(ids_real)  :: z_max_error_lower=ids_real_invalid
  integer(ids_int) :: z_max_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  integer(ids_int)  :: is_neutral=ids_int_invalid       ! /is_neutral - Flag specifying if this state corresponds to a neutral (1) or not (0)
  type (ids_identifier) :: neutral_type  ! /neutral_type - Neutral type (if the considered state is a neutral), in terms of energy. ID =1: cold; 2: thermal; 3:
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(ids_real),pointer  :: particles(:) => null()     ! /particles - Source term for the charge state density transport equation
  real(ids_real),pointer  :: particles_error_upper(:) => null()
  real(ids_real),pointer  :: particles_error_lower(:) => null()
  integer(ids_int) :: particles_error_index=ids_int_invalid

  type (ids_core_sources_source_profiles_1d_particles_decomposed_4) :: particles_decomposed  ! /particles_decomposed - Decomposition of the source term for state density equation into implicit and explicit parts
  real(ids_real),pointer  :: energy(:) => null()     ! /energy - Source terms for the charge state energy transport equation
  real(ids_real),pointer  :: energy_error_upper(:) => null()
  real(ids_real),pointer  :: energy_error_lower(:) => null()
  integer(ids_int) :: energy_error_index=ids_int_invalid

  type (ids_core_sources_source_profiles_1d_energy_decomposed_4) :: energy_decomposed  ! /energy_decomposed - Decomposition of the source term for state energy equation into implicit and explicit parts
endtype

type ids_core_sources_source_profiles_1d_ions  !    Source terms related to a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(ids_real)  :: z_ion=ids_real_invalid       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed)
  real(ids_real)  :: z_ion_error_upper=ids_real_invalid
  real(ids_real)  :: z_ion_error_lower=ids_real_invalid
  integer(ids_int) :: z_ion_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  integer(ids_int)  :: neutral_index=ids_int_invalid       ! /neutral_index - Index of the corresponding neutral species in the ../../neutral array
  real(ids_real),pointer  :: particles(:) => null()     ! /particles - Source term for ion density equation
  real(ids_real),pointer  :: particles_error_upper(:) => null()
  real(ids_real),pointer  :: particles_error_lower(:) => null()
  integer(ids_int) :: particles_error_index=ids_int_invalid

  type (ids_core_sources_source_profiles_1d_particles_decomposed_3) :: particles_decomposed  ! /particles_decomposed - Decomposition of the source term for ion density equation into implicit and explicit parts
  real(ids_real),pointer  :: energy(:) => null()     ! /energy - Source term for the ion energy transport equation.
  real(ids_real),pointer  :: energy_error_upper(:) => null()
  real(ids_real),pointer  :: energy_error_lower(:) => null()
  integer(ids_int) :: energy_error_index=ids_int_invalid

  type (ids_core_sources_source_profiles_1d_energy_decomposed_3) :: energy_decomposed  ! /energy_decomposed - Decomposition of the source term for ion energy equation into implicit and explicit parts
  type (ids_core_sources_source_profiles_1d_components_2) :: momentum  ! /momentum - Source term for the ion momentum transport equations along various components (directions)
  integer(ids_int)  :: multiple_states_flag=ids_int_invalid       ! /multiple_states_flag - Multiple states calculation flag : 0-Only one state is considered; 1-Multiple states are considered
  type (ids_core_sources_source_profiles_1d_ions_charge_states),pointer :: state(:) => null()  ! /state(i) - Source terms related to the different charge states of the species (ionisation, energy, excitation,
endtype

type ids_core_sources_source_profiles_1d_electrons  !    Source terms related to electrons
  real(ids_real),pointer  :: particles(:) => null()     ! /particles - Source term for electron density equation
  real(ids_real),pointer  :: particles_error_upper(:) => null()
  real(ids_real),pointer  :: particles_error_lower(:) => null()
  integer(ids_int) :: particles_error_index=ids_int_invalid

  type (ids_core_sources_source_profiles_1d_particles_decomposed_3) :: particles_decomposed  ! /particles_decomposed - Decomposition of the source term for electron density equation into implicit and explicit parts
  real(ids_real),pointer  :: particles_inside(:) => null()     ! /particles_inside - Electron source inside the flux surface. Cumulative volume integral of the source term for the elect
  real(ids_real),pointer  :: particles_inside_error_upper(:) => null()
  real(ids_real),pointer  :: particles_inside_error_lower(:) => null()
  integer(ids_int) :: particles_inside_error_index=ids_int_invalid

  real(ids_real),pointer  :: energy(:) => null()     ! /energy - Source term for the electron energy equation
  real(ids_real),pointer  :: energy_error_upper(:) => null()
  real(ids_real),pointer  :: energy_error_lower(:) => null()
  integer(ids_int) :: energy_error_index=ids_int_invalid

  type (ids_core_sources_source_profiles_1d_energy_decomposed_3) :: energy_decomposed  ! /energy_decomposed - Decomposition of the source term for electron energy equation into implicit and explicit parts
  real(ids_real),pointer  :: power_inside(:) => null()     ! /power_inside - Power coupled to electrons inside the flux surface. Cumulative volume integral of the source term fo
  real(ids_real),pointer  :: power_inside_error_upper(:) => null()
  real(ids_real),pointer  :: power_inside_error_lower(:) => null()
  integer(ids_int) :: power_inside_error_index=ids_int_invalid

endtype

type ids_core_sources_source_profiles_1d  !    Source terms for a given time slice
  type (ids_core_radial_grid) :: grid  ! /grid - Radial grid
  type (ids_core_sources_source_profiles_1d_electrons) :: electrons  ! /electrons - Sources for electrons
  real(ids_real),pointer  :: total_ion_energy(:) => null()     ! /total_ion_energy - Source term for the total (summed over ion  species) energy equation
  real(ids_real),pointer  :: total_ion_energy_error_upper(:) => null()
  real(ids_real),pointer  :: total_ion_energy_error_lower(:) => null()
  integer(ids_int) :: total_ion_energy_error_index=ids_int_invalid

  type (ids_core_sources_source_profiles_1d_energy_decomposed_3) :: total_ion_energy_decomposed  ! /total_ion_energy_decomposed - Decomposition of the source term for total ion energy equation into implicit and explicit parts
  real(ids_real),pointer  :: total_ion_power_inside(:) => null()     ! /total_ion_power_inside - Total power coupled to ion species (summed over ion  species) inside the flux surface. Cumulative vo
  real(ids_real),pointer  :: total_ion_power_inside_error_upper(:) => null()
  real(ids_real),pointer  :: total_ion_power_inside_error_lower(:) => null()
  integer(ids_int) :: total_ion_power_inside_error_index=ids_int_invalid

  real(ids_real),pointer  :: momentum_tor(:) => null()     ! /momentum_tor - Source term for total toroidal momentum equation
  real(ids_real),pointer  :: momentum_tor_error_upper(:) => null()
  real(ids_real),pointer  :: momentum_tor_error_lower(:) => null()
  integer(ids_int) :: momentum_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_tor_inside(:) => null()     ! /torque_tor_inside - Toroidal torque inside the flux surface. Cumulative volume integral of the source term for the total
  real(ids_real),pointer  :: torque_tor_inside_error_upper(:) => null()
  real(ids_real),pointer  :: torque_tor_inside_error_lower(:) => null()
  integer(ids_int) :: torque_tor_inside_error_index=ids_int_invalid

  real(ids_real),pointer  :: j_parallel(:) => null()     ! /j_parallel - Parallel current density source, average(J.B) / B0, where B0 = core_sources/vacuum_toroidal_field/b0
  real(ids_real),pointer  :: j_parallel_error_upper(:) => null()
  real(ids_real),pointer  :: j_parallel_error_lower(:) => null()
  integer(ids_int) :: j_parallel_error_index=ids_int_invalid

  real(ids_real),pointer  :: current_parallel_inside(:) => null()     ! /current_parallel_inside - Parallel current driven inside the flux surface. Cumulative surface integral of j_parallel
  real(ids_real),pointer  :: current_parallel_inside_error_upper(:) => null()
  real(ids_real),pointer  :: current_parallel_inside_error_lower(:) => null()
  integer(ids_int) :: current_parallel_inside_error_index=ids_int_invalid

  real(ids_real),pointer  :: conductivity_parallel(:) => null()     ! /conductivity_parallel - Parallel conductivity due to this source
  real(ids_real),pointer  :: conductivity_parallel_error_upper(:) => null()
  real(ids_real),pointer  :: conductivity_parallel_error_lower(:) => null()
  integer(ids_int) :: conductivity_parallel_error_index=ids_int_invalid

  type (ids_core_sources_source_profiles_1d_ions),pointer :: ion(:) => null()  ! /ion(i) - Source terms related to the different ions species
  type (ids_core_sources_source_profiles_1d_neutral),pointer :: neutral(:) => null()  ! /neutral(i) - Source terms related to the different neutral species
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_core_sources_source_global_electrons  !    Source terms related to electrons
  real(ids_real)  :: particles=ids_real_invalid       ! /particles - Electron particle source
  real(ids_real)  :: particles_error_upper=ids_real_invalid
  real(ids_real)  :: particles_error_lower=ids_real_invalid
  integer(ids_int) :: particles_error_index=ids_int_invalid

  real(ids_real)  :: power=ids_real_invalid       ! /power - Power coupled to electrons
  real(ids_real)  :: power_error_upper=ids_real_invalid
  real(ids_real)  :: power_error_lower=ids_real_invalid
  integer(ids_int) :: power_error_index=ids_int_invalid

endtype

type ids_core_sources_source_global  !    Source global quantities for a given time slice
  real(ids_real)  :: power=ids_real_invalid       ! /power - Total power coupled to the plasma
  real(ids_real)  :: power_error_upper=ids_real_invalid
  real(ids_real)  :: power_error_lower=ids_real_invalid
  integer(ids_int) :: power_error_index=ids_int_invalid

  real(ids_real)  :: total_ion_power=ids_real_invalid       ! /total_ion_power - Total power coupled to ion species (summed over ion  species)
  real(ids_real)  :: total_ion_power_error_upper=ids_real_invalid
  real(ids_real)  :: total_ion_power_error_lower=ids_real_invalid
  integer(ids_int) :: total_ion_power_error_index=ids_int_invalid

  type (ids_core_sources_source_global_electrons) :: electrons  ! /electrons - Sources for electrons
  real(ids_real)  :: torque_tor=ids_real_invalid       ! /torque_tor - Toroidal torque
  real(ids_real)  :: torque_tor_error_upper=ids_real_invalid
  real(ids_real)  :: torque_tor_error_lower=ids_real_invalid
  integer(ids_int) :: torque_tor_error_index=ids_int_invalid

  real(ids_real)  :: current_parallel=ids_real_invalid       ! /current_parallel - Parallel current driven
  real(ids_real)  :: current_parallel_error_upper=ids_real_invalid
  real(ids_real)  :: current_parallel_error_lower=ids_real_invalid
  integer(ids_int) :: current_parallel_error_index=ids_int_invalid

  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_core_sources_source  !    Source terms for a given actuator
  type (ids_identifier) :: identifier  ! /identifier - Source term identifier
  type (ids_core_sources_source_global),pointer :: global_quantities(:) => null()  ! /global_quantities(i) - Total source quantities integrated over the plasma volume or surface
  type (ids_core_sources_source_profiles_1d),pointer :: profiles_1d(:) => null()  ! /profiles_1d(i) - Source profiles for various time slices. Source terms are positive (resp. negative) when there is a
endtype

type ids_core_sources  !    Core plasma source terms (for the transport equations). Energy terms correspond to the full kinetic energy equation (i.e. the ener
  type (ids_ids_properties) :: ids_properties  ! /core_sources/ids_properties -
  type (ids_b_tor_vacuum_1) :: vacuum_toroidal_field  ! /core_sources/vacuum_toroidal_field - Characteristics of the vacuum toroidal field (used in Rho_Tor definition and in the normalization of
  type (ids_core_sources_source),pointer :: source(:) => null()  ! /core_sources/source(i) - Set of source terms
  type (ids_code) :: code  ! /core_sources/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include core_transport/dd_core_transport.xsd
type ids_core_transport_model_1_density  !    Transport coefficients for density equations. Coordinates one level above.
  real(ids_real),pointer  :: d(:) => null()     ! /d - Effective diffusivity
  real(ids_real),pointer  :: d_error_upper(:) => null()
  real(ids_real),pointer  :: d_error_lower(:) => null()
  integer(ids_int) :: d_error_index=ids_int_invalid

  real(ids_real),pointer  :: v(:) => null()     ! /v - Effective convection
  real(ids_real),pointer  :: v_error_upper(:) => null()
  real(ids_real),pointer  :: v_error_lower(:) => null()
  integer(ids_int) :: v_error_index=ids_int_invalid

  real(ids_real),pointer  :: flux(:) => null()     ! /flux - Flux
  real(ids_real),pointer  :: flux_error_upper(:) => null()
  real(ids_real),pointer  :: flux_error_lower(:) => null()
  integer(ids_int) :: flux_error_index=ids_int_invalid

endtype

type ids_core_transport_model_1_energy  !    Transport coefficients for energy equations. Coordinates one level above.
  real(ids_real),pointer  :: d(:) => null()     ! /d - Effective diffusivity
  real(ids_real),pointer  :: d_error_upper(:) => null()
  real(ids_real),pointer  :: d_error_lower(:) => null()
  integer(ids_int) :: d_error_index=ids_int_invalid

  real(ids_real),pointer  :: v(:) => null()     ! /v - Effective convection
  real(ids_real),pointer  :: v_error_upper(:) => null()
  real(ids_real),pointer  :: v_error_lower(:) => null()
  integer(ids_int) :: v_error_index=ids_int_invalid

  real(ids_real),pointer  :: flux(:) => null()     ! /flux - Flux
  real(ids_real),pointer  :: flux_error_upper(:) => null()
  real(ids_real),pointer  :: flux_error_lower(:) => null()
  integer(ids_int) :: flux_error_index=ids_int_invalid

endtype

type ids_core_transport_model_1_momentum  !    Transport coefficients for momentum equations. Coordinates one level above.
  real(ids_real),pointer  :: d(:) => null()     ! /d - Effective diffusivity
  real(ids_real),pointer  :: d_error_upper(:) => null()
  real(ids_real),pointer  :: d_error_lower(:) => null()
  integer(ids_int) :: d_error_index=ids_int_invalid

  real(ids_real),pointer  :: v(:) => null()     ! /v - Effective convection
  real(ids_real),pointer  :: v_error_upper(:) => null()
  real(ids_real),pointer  :: v_error_lower(:) => null()
  integer(ids_int) :: v_error_index=ids_int_invalid

  real(ids_real),pointer  :: flux(:) => null()     ! /flux - Flux
  real(ids_real),pointer  :: flux_error_upper(:) => null()
  real(ids_real),pointer  :: flux_error_lower(:) => null()
  integer(ids_int) :: flux_error_index=ids_int_invalid

endtype

type ids_core_transport_model_2_density  !    Transport coefficients for density equations. Coordinates two levels above.
  real(ids_real),pointer  :: d(:) => null()     ! /d - Effective diffusivity
  real(ids_real),pointer  :: d_error_upper(:) => null()
  real(ids_real),pointer  :: d_error_lower(:) => null()
  integer(ids_int) :: d_error_index=ids_int_invalid

  real(ids_real),pointer  :: v(:) => null()     ! /v - Effective convection
  real(ids_real),pointer  :: v_error_upper(:) => null()
  real(ids_real),pointer  :: v_error_lower(:) => null()
  integer(ids_int) :: v_error_index=ids_int_invalid

  real(ids_real),pointer  :: flux(:) => null()     ! /flux - Flux
  real(ids_real),pointer  :: flux_error_upper(:) => null()
  real(ids_real),pointer  :: flux_error_lower(:) => null()
  integer(ids_int) :: flux_error_index=ids_int_invalid

endtype

type ids_core_transport_model_2_energy  !    Transport coefficients for energy equations. Coordinates two levels above.
  real(ids_real),pointer  :: d(:) => null()     ! /d - Effective diffusivity
  real(ids_real),pointer  :: d_error_upper(:) => null()
  real(ids_real),pointer  :: d_error_lower(:) => null()
  integer(ids_int) :: d_error_index=ids_int_invalid

  real(ids_real),pointer  :: v(:) => null()     ! /v - Effective convection
  real(ids_real),pointer  :: v_error_upper(:) => null()
  real(ids_real),pointer  :: v_error_lower(:) => null()
  integer(ids_int) :: v_error_index=ids_int_invalid

  real(ids_real),pointer  :: flux(:) => null()     ! /flux - Flux
  real(ids_real),pointer  :: flux_error_upper(:) => null()
  real(ids_real),pointer  :: flux_error_lower(:) => null()
  integer(ids_int) :: flux_error_index=ids_int_invalid

endtype

type ids_core_transport_model_3_density  !    Transport coefficients for density equations. Coordinates three levels above.
  real(ids_real),pointer  :: d(:) => null()     ! /d - Effective diffusivity
  real(ids_real),pointer  :: d_error_upper(:) => null()
  real(ids_real),pointer  :: d_error_lower(:) => null()
  integer(ids_int) :: d_error_index=ids_int_invalid

  real(ids_real),pointer  :: v(:) => null()     ! /v - Effective convection
  real(ids_real),pointer  :: v_error_upper(:) => null()
  real(ids_real),pointer  :: v_error_lower(:) => null()
  integer(ids_int) :: v_error_index=ids_int_invalid

  real(ids_real),pointer  :: flux(:) => null()     ! /flux - Flux
  real(ids_real),pointer  :: flux_error_upper(:) => null()
  real(ids_real),pointer  :: flux_error_lower(:) => null()
  integer(ids_int) :: flux_error_index=ids_int_invalid

endtype

type ids_core_transport_model_3_energy  !    Transport coefficients for energy equations. Coordinates three levels above.
  real(ids_real),pointer  :: d(:) => null()     ! /d - Effective diffusivity
  real(ids_real),pointer  :: d_error_upper(:) => null()
  real(ids_real),pointer  :: d_error_lower(:) => null()
  integer(ids_int) :: d_error_index=ids_int_invalid

  real(ids_real),pointer  :: v(:) => null()     ! /v - Effective convection
  real(ids_real),pointer  :: v_error_upper(:) => null()
  real(ids_real),pointer  :: v_error_lower(:) => null()
  integer(ids_int) :: v_error_index=ids_int_invalid

  real(ids_real),pointer  :: flux(:) => null()     ! /flux - Flux
  real(ids_real),pointer  :: flux_error_upper(:) => null()
  real(ids_real),pointer  :: flux_error_lower(:) => null()
  integer(ids_int) :: flux_error_index=ids_int_invalid

endtype

type ids_core_transport_model_3_momentum  !    Transport coefficients for momentum equation in a given direction. Coordinates three levels above.
  real(ids_real),pointer  :: d(:) => null()     ! /d - Effective diffusivity
  real(ids_real),pointer  :: d_error_upper(:) => null()
  real(ids_real),pointer  :: d_error_lower(:) => null()
  integer(ids_int) :: d_error_index=ids_int_invalid

  real(ids_real),pointer  :: v(:) => null()     ! /v - Effective convection
  real(ids_real),pointer  :: v_error_upper(:) => null()
  real(ids_real),pointer  :: v_error_lower(:) => null()
  integer(ids_int) :: v_error_index=ids_int_invalid

  real(ids_real),pointer  :: flux(:) => null()     ! /flux - Flux
  real(ids_real),pointer  :: flux_error_upper(:) => null()
  real(ids_real),pointer  :: flux_error_lower(:) => null()
  integer(ids_int) :: flux_error_index=ids_int_invalid

endtype

type ids_core_transport_model_components_3_momentum  !    Transport coefficients for momentum equations on various components. Coordinates three levels above the leaves
  type (ids_core_transport_model_3_momentum) :: radial  ! /radial - Radial component
  type (ids_core_transport_model_3_momentum) :: diamagnetic  ! /diamagnetic - Diamagnetic component
  type (ids_core_transport_model_3_momentum) :: parallel  ! /parallel - Parallel component
  type (ids_core_transport_model_3_momentum) :: poloidal  ! /poloidal - Poloidal component
  type (ids_core_transport_model_3_momentum) :: toroidal  ! /toroidal - Toroidal component
endtype

type ids_core_transport_model_ions_charge_states  !    Transport coefficients related to the a given state of the ion species
  real(ids_real)  :: z_min=ids_real_invalid       ! /z_min - Minimum Z of the charge state bundle
  real(ids_real)  :: z_min_error_upper=ids_real_invalid
  real(ids_real)  :: z_min_error_lower=ids_real_invalid
  integer(ids_int) :: z_min_error_index=ids_int_invalid

  real(ids_real)  :: z_max=ids_real_invalid       ! /z_max - Maximum Z of the charge state bundle
  real(ids_real)  :: z_max_error_upper=ids_real_invalid
  real(ids_real)  :: z_max_error_lower=ids_real_invalid
  integer(ids_int) :: z_max_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  integer(ids_int)  :: is_neutral=ids_int_invalid       ! /is_neutral - Flag specifying if this state corresponds to a neutral (1) or not (0)
  type (ids_identifier) :: neutral_type  ! /neutral_type - Neutral type (if the considered state is a neutral), in terms of energy. ID =1: cold; 2: thermal; 3:
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  type (ids_core_transport_model_3_density) :: particles  ! /particles - Transport quantities related to density equation of the charge state considered (thermal+non-thermal
  type (ids_core_transport_model_3_energy) :: energy  ! /energy - Transport quantities related to the energy equation of the charge state considered
endtype

type ids_core_transport_model_neutral_state  !    Transport coefficients related to the a given state of the neutral species
  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying state
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  type (ids_core_transport_model_3_density) :: particles  ! /particles - Transport quantities related to density equation of the charge state considered (thermal+non-thermal
  type (ids_core_transport_model_3_energy) :: energy  ! /energy - Transport quantities related to the energy equation of the charge state considered
endtype

type ids_core_transport_model_ions  !    Transport coefficients related to a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(ids_real)  :: z_ion=ids_real_invalid       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed)
  real(ids_real)  :: z_ion_error_upper=ids_real_invalid
  real(ids_real)  :: z_ion_error_lower=ids_real_invalid
  integer(ids_int) :: z_ion_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  integer(ids_int)  :: neutral_index=ids_int_invalid       ! /neutral_index - Index of the corresponding neutral species in the ../../neutral array
  type (ids_core_transport_model_2_density) :: particles  ! /particles - Transport related to the ion density equation
  type (ids_core_transport_model_2_energy) :: energy  ! /energy - Transport coefficients related to the ion energy equation
  type (ids_core_transport_model_components_3_momentum) :: momentum  ! /momentum - Transport coefficients related to the ion momentum equations for various components (directions)
  integer(ids_int)  :: multiple_states_flag=ids_int_invalid       ! /multiple_states_flag - Multiple states calculation flag : 0-Only one state is considered; 1-Multiple states are considered
  type (ids_core_transport_model_ions_charge_states),pointer :: state(:) => null()  ! /state(i) - Transport coefficients related to the different states of the species
endtype

type ids_core_transport_model_neutral  !    Transport coefficients related to a given neutral species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  integer(ids_int)  :: ion_index=ids_int_invalid       ! /ion_index - Index of the corresponding ion species in the ../../ion array
  type (ids_core_transport_model_2_density) :: particles  ! /particles - Transport related to the neutral density equation
  type (ids_core_transport_model_2_energy) :: energy  ! /energy - Transport coefficients related to the neutral energy equation
  integer(ids_int)  :: multiple_states_flag=ids_int_invalid       ! /multiple_states_flag - Multiple states calculation flag : 0-Only one state is considered; 1-Multiple states are considered
  type (ids_core_transport_model_neutral_state),pointer :: state(:) => null()  ! /state(i) - Transport coefficients related to the different states of the species
endtype

type ids_core_transport_model_electrons  !    Transport coefficients related to electrons
  type (ids_core_transport_model_2_density) :: particles  ! /particles - Transport quantities for the electron density equation
  type (ids_core_transport_model_2_energy) :: energy  ! /energy - Transport quantities for the electron energy equation
endtype

type ids_core_transport_model_profiles_1d  !    Transport coefficient profiles at a given time slice
  type (ids_core_radial_grid) :: grid_d  ! /grid_d - Grid for effective diffusivities and parallel conductivity
  type (ids_core_radial_grid) :: grid_v  ! /grid_v - Grid for effective convections
  type (ids_core_radial_grid) :: grid_flux  ! /grid_flux - Grid for fluxes
  real(ids_real),pointer  :: conductivity_parallel(:) => null()     ! /conductivity_parallel - Parallel conductivity
  real(ids_real),pointer  :: conductivity_parallel_error_upper(:) => null()
  real(ids_real),pointer  :: conductivity_parallel_error_lower(:) => null()
  integer(ids_int) :: conductivity_parallel_error_index=ids_int_invalid

  type (ids_core_transport_model_electrons) :: electrons  ! /electrons - Transport quantities related to the electrons
  type (ids_core_transport_model_1_energy) :: total_ion_energy  ! /total_ion_energy - Transport coefficients for the total (summed over ion  species) energy equation
  type (ids_core_transport_model_1_momentum) :: momentum_tor  ! /momentum_tor - Transport coefficients for total toroidal momentum equation
  type (ids_core_transport_model_ions),pointer :: ion(:) => null()  ! /ion(i) - Transport coefficients related to the various ion species
  type (ids_core_transport_model_neutral),pointer :: neutral(:) => null()  ! /neutral(i) - Transport coefficients related to the various neutral species
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_core_transport_model  !    Transport coefficients for a given model
  type (ids_identifier) :: identifier  ! /identifier - Transport model identifier
  real(ids_real)  :: flux_multiplier=ids_real_invalid       ! /flux_multiplier - Multiplier applied to the particule flux when adding its contribution in the expression of the heat
  real(ids_real)  :: flux_multiplier_error_upper=ids_real_invalid
  real(ids_real)  :: flux_multiplier_error_lower=ids_real_invalid
  integer(ids_int) :: flux_multiplier_error_index=ids_int_invalid

  type (ids_core_transport_model_profiles_1d),pointer :: profiles_1d(:) => null()  ! /profiles_1d(i) - Transport coefficient profiles for various time slices. Fluxes and convection are positive (resp. ne
endtype

type ids_core_transport  !    Core plasma transport. Energy terms correspond to the full kinetic energy equation (i.e. the energy flux takes into account the en
  type (ids_ids_properties) :: ids_properties  ! /core_transport/ids_properties -
  type (ids_b_tor_vacuum_1) :: vacuum_toroidal_field  ! /core_transport/vacuum_toroidal_field - Characteristics of the vacuum toroidal field (used in Rho_Tor definition and in the normalization of
  type (ids_core_transport_model),pointer :: model(:) => null()  ! /core_transport/model(i) - Transport is described by a combination of various transport models
  type (ids_code) :: code  ! /core_transport/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include dataset_description/dd_dataset_description.xsd
type ids_dataset_description_simulation  !    Description of the general simulation characteristics, if this data entry has been produced by a simulation. Several nodes describ
  character(len=ids_string_length), dimension(:), pointer ::comment_before => null()       ! /comment_before - Comment made when launching a simulation
  character(len=ids_string_length), dimension(:), pointer ::comment_after => null()       ! /comment_after - Comment made at the end of a simulation
  real(ids_real)  :: time_begin=ids_real_invalid       ! /time_begin - Start time
  real(ids_real)  :: time_begin_error_upper=ids_real_invalid
  real(ids_real)  :: time_begin_error_lower=ids_real_invalid
  integer(ids_int) :: time_begin_error_index=ids_int_invalid

  real(ids_real)  :: time_step=ids_real_invalid       ! /time_step - Time interval between main steps, e.g. storage step (if relevant and constant)
  real(ids_real)  :: time_step_error_upper=ids_real_invalid
  real(ids_real)  :: time_step_error_lower=ids_real_invalid
  integer(ids_int) :: time_step_error_index=ids_int_invalid

  real(ids_real)  :: time_end=ids_real_invalid       ! /time_end - Stop time
  real(ids_real)  :: time_end_error_upper=ids_real_invalid
  real(ids_real)  :: time_end_error_lower=ids_real_invalid
  integer(ids_int) :: time_end_error_index=ids_int_invalid

  real(ids_real)  :: time_restart=ids_real_invalid       ! /time_restart - Time of the last restart done during the simulation
  real(ids_real)  :: time_restart_error_upper=ids_real_invalid
  real(ids_real)  :: time_restart_error_lower=ids_real_invalid
  integer(ids_int) :: time_restart_error_index=ids_int_invalid

  real(ids_real)  :: time_current=ids_real_invalid       ! /time_current - Current time of the simulation
  real(ids_real)  :: time_current_error_upper=ids_real_invalid
  real(ids_real)  :: time_current_error_lower=ids_real_invalid
  integer(ids_int) :: time_current_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::time_begun => null()       ! /time_begun - Actual wall-clock time simulation started
  character(len=ids_string_length), dimension(:), pointer ::time_ended => null()       ! /time_ended - Actual wall-clock time simulation finished
  character(len=ids_string_length), dimension(:), pointer ::workflow => null()       ! /workflow - Description of the workflow which has been used to produce this data entry (e.g. copy of the Kepler
endtype

type ids_dataset_description  !    General description of the dataset (collection of all IDSs within the given database entry). Main description text to be put in id
  type (ids_ids_properties) :: ids_properties  ! /dataset_description/ids_properties -
  type (ids_data_entry) :: data_entry  ! /dataset_description/data_entry - Definition of this data entry
  type (ids_data_entry) :: parent_entry  ! /dataset_description/parent_entry - Definition of the parent data entry, if the present data entry has been generated by applying a give
  character(len=ids_string_length), dimension(:), pointer ::imas_version => null()       ! /dataset_description/imas_version - Version of the IMAS infrastructure used to produce this data entry. Refers to the global IMAS reposi
  character(len=ids_string_length), dimension(:), pointer ::dd_version => null()       ! /dataset_description/dd_version - Version of the physics data dictionary of this dataset
  type (ids_dataset_description_simulation) :: simulation  ! /dataset_description/simulation - Description of the general simulation characteristics, if this data entry has been produced by a sim
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include distribution_sources/dd_distribution_sources.xsd
type ids_distribution_sources_source_ggd  !    Source terms for a given time slice, using a GGD representation
  type (ids_generic_grid_dynamic) :: grid  ! /grid - Grid description
  type (ids_generic_grid_scalar),pointer :: particles(:) => null()  ! /particles(i) - Source density of particles in phase space, for various grid subsets
  integer(ids_int),pointer  :: discrete(:) => null()      ! /discrete - List of indices of grid spaces (refers to ../grid/space) for which the source is discretely distribu
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_distribution_sources_source_profiles_1d  !    Radial profile of source terms for a given time slice
  type (ids_core_radial_grid) :: grid  ! /grid - Radial grid
  real(ids_real),pointer  :: energy(:) => null()     ! /energy - Source term for the energy transport equation
  real(ids_real),pointer  :: energy_error_upper(:) => null()
  real(ids_real),pointer  :: energy_error_lower(:) => null()
  integer(ids_int) :: energy_error_index=ids_int_invalid

  real(ids_real),pointer  :: momentum_tor(:) => null()     ! /momentum_tor - Source term for the toroidal momentum equation
  real(ids_real),pointer  :: momentum_tor_error_upper(:) => null()
  real(ids_real),pointer  :: momentum_tor_error_lower(:) => null()
  integer(ids_int) :: momentum_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: particles(:) => null()     ! /particles - Source term for the density transport equation
  real(ids_real),pointer  :: particles_error_upper(:) => null()
  real(ids_real),pointer  :: particles_error_lower(:) => null()
  integer(ids_int) :: particles_error_index=ids_int_invalid

  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_distribution_sources_source_global_shinethrough  !    Global quantities related to shinethrough, for a given time slice
  real(ids_real)  :: power=ids_real_invalid       ! /power - Power losses due to shinethrough
  real(ids_real)  :: power_error_upper=ids_real_invalid
  real(ids_real)  :: power_error_lower=ids_real_invalid
  integer(ids_int) :: power_error_index=ids_int_invalid

  real(ids_real)  :: particles=ids_real_invalid       ! /particles - Particle losses due to shinethrough
  real(ids_real)  :: particles_error_upper=ids_real_invalid
  real(ids_real)  :: particles_error_lower=ids_real_invalid
  integer(ids_int) :: particles_error_index=ids_int_invalid

  real(ids_real)  :: torque_tor=ids_real_invalid       ! /torque_tor - Toroidal torque losses due to shinethrough
  real(ids_real)  :: torque_tor_error_upper=ids_real_invalid
  real(ids_real)  :: torque_tor_error_lower=ids_real_invalid
  integer(ids_int) :: torque_tor_error_index=ids_int_invalid

endtype

type ids_distribution_sources_source_global_quantities  !    Global quantities of distribution_source for a given time slice
  real(ids_real)  :: power=ids_real_invalid       ! /power - Total power of the source
  real(ids_real)  :: power_error_upper=ids_real_invalid
  real(ids_real)  :: power_error_lower=ids_real_invalid
  integer(ids_int) :: power_error_index=ids_int_invalid

  real(ids_real)  :: torque_tor=ids_real_invalid       ! /torque_tor - Total toroidal torque of the source
  real(ids_real)  :: torque_tor_error_upper=ids_real_invalid
  real(ids_real)  :: torque_tor_error_lower=ids_real_invalid
  integer(ids_int) :: torque_tor_error_index=ids_int_invalid

  real(ids_real)  :: particles=ids_real_invalid       ! /particles - Particle source rate
  real(ids_real)  :: particles_error_upper=ids_real_invalid
  real(ids_real)  :: particles_error_lower=ids_real_invalid
  integer(ids_int) :: particles_error_index=ids_int_invalid

  type (ids_distribution_sources_source_global_shinethrough) :: shinethrough  ! /shinethrough - Shinethrough losses
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_distribution_sources_source  !    Source terms for a given actuator
  type (ids_distribution_process_identifier),pointer :: process(:) => null()  ! /process(i) - Set of processes (NBI units, fusion reactions, ...) that provide the source.
  integer(ids_int)  :: gyro_type=ids_int_invalid       ! /gyro_type - Defines how to interpret the spatial coordinates: 1 = given at the actual particle birth point; 2 =g
  type (ids_distribution_species) :: species  ! /species - Species injected or consumed by this source/sink
  type (ids_distribution_sources_source_global_quantities),pointer :: global_quantities(:) => null()  ! /global_quantities(i) - Global quantities for various time slices
  type (ids_distribution_sources_source_profiles_1d),pointer :: profiles_1d(:) => null()  ! /profiles_1d(i) - Source radial profiles (flux surface averaged quantities) for various time slices
  type (ids_distribution_sources_source_ggd),pointer :: ggd(:) => null()  ! /ggd(i) - Source terms in phase space (real space, velocity space, spin state), represented using the ggd, for
  type (ids_distribution_markers),pointer :: markers(:) => null()  ! /markers(i) - Source given as a group of markers (test particles) born per second, for various time slices
endtype

type ids_distribution_sources  !    Sources of particles for input to kinetic equations, e.g. Fokker-Planck calculation. The sources could originate from e.g. NBI or
  type (ids_ids_properties) :: ids_properties  ! /distribution_sources/ids_properties -
  type (ids_distribution_sources_source),pointer :: source(:) => null()  ! /distribution_sources/source(i) - Set of source/sink terms. A source/sink term corresponds to the particle source due to an NBI inject
  type (ids_b_tor_vacuum_1) :: vacuum_toroidal_field  ! /distribution_sources/vacuum_toroidal_field - Characteristics of the vacuum toroidal field (used in rho_tor definition)
  type (ids_rz1d_dynamic_1) :: magnetic_axis  ! /distribution_sources/magnetic_axis - Magnetic axis position (used to define a poloidal angle for the 2D profiles)
  type (ids_code) :: code  ! /distribution_sources/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include distributions/dd_distributions.xsd
type ids_waves_coherent_wave_identifier  !    Wave identifier
  type (ids_identifier) :: type  ! /type - Wave/antenna type. index=1 for name=EC; index=2 for name=IC; index=3 for name=LH
  character(len=ids_string_length), dimension(:), pointer ::antenna_name => null()       ! /antenna_name - Name of the antenna that launches this wave. Corresponds to the name specified in antennas/ec(i)/nam
  integer(ids_int)  :: index_in_antenna=ids_int_invalid       ! /index_in_antenna - Index of the wave (starts at 1), separating different waves generated from a single antenna.
endtype

type ids_distributions_d_ggd_expansion  !    Expansion of the distribution function for a given time slice, using a GGD representation
  type (ids_generic_grid_scalar),pointer :: grid_subset(:) => null()  ! /grid_subset(i) - Values of the distribution function expansion, for various grid subsets
endtype

type ids_distributions_d_ggd  !    Distribution function for a given time slice, using a GGD representation
  type (ids_generic_grid_dynamic) :: grid  ! /grid - Grid description
  real(ids_real),pointer  :: temperature(:) => null()     ! /temperature - Reference temperature profile used to define the local thermal energy and the thermal velocity (for
  real(ids_real),pointer  :: temperature_error_upper(:) => null()
  real(ids_real),pointer  :: temperature_error_lower(:) => null()
  integer(ids_int) :: temperature_error_index=ids_int_invalid

  type (ids_distributions_d_ggd_expansion),pointer :: expansion(:) => null()  ! /expansion(i) - Distribution function expanded into a vector of successive approximations. The first element in the
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_distributions_d_source_identifier  !    Identifier of the source/sink term (wave or particle source process)
  type (ids_identifier_dynamic_aos3) :: type  ! /type - Type of the source term. Index  = 1 for a wave, index = 2 for a particle source process
  integer(ids_int)  :: wave_index=ids_int_invalid       ! /wave_index - Index into distribution/wave
  integer(ids_int)  :: process_index=ids_int_invalid       ! /process_index - Index into distribution/process
endtype

type ids_distributions_d_global_quantities_source  !    Global quantities for a given source/sink term
  type (ids_distributions_d_source_identifier) :: identifier  ! /identifier - Identifier of the wave or particle source process, defined respectively in distribution/wave or dist
  real(ids_real)  :: particles=ids_real_invalid       ! /particles - Particle source rate
  real(ids_real)  :: particles_error_upper=ids_real_invalid
  real(ids_real)  :: particles_error_lower=ids_real_invalid
  integer(ids_int) :: particles_error_index=ids_int_invalid

  real(ids_real)  :: power=ids_real_invalid       ! /power - Total power of the source
  real(ids_real)  :: power_error_upper=ids_real_invalid
  real(ids_real)  :: power_error_lower=ids_real_invalid
  integer(ids_int) :: power_error_index=ids_int_invalid

  real(ids_real)  :: torque_tor=ids_real_invalid       ! /torque_tor - Total toroidal torque of the source
  real(ids_real)  :: torque_tor_error_upper=ids_real_invalid
  real(ids_real)  :: torque_tor_error_lower=ids_real_invalid
  integer(ids_int) :: torque_tor_error_index=ids_int_invalid

endtype

type ids_distributions_d_global_quantities_collisions_ion_state  !    Global quantities for collisions with a given ion species state
  real(ids_real)  :: z_min=ids_real_invalid       ! /z_min - Minimum Z of the charge state bundle (z_min = z_max = 0 for a neutral)
  real(ids_real)  :: z_min_error_upper=ids_real_invalid
  real(ids_real)  :: z_min_error_lower=ids_real_invalid
  integer(ids_int) :: z_min_error_index=ids_int_invalid

  real(ids_real)  :: z_max=ids_real_invalid       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(ids_real)  :: z_max_error_upper=ids_real_invalid
  real(ids_real)  :: z_max_error_lower=ids_real_invalid
  integer(ids_int) :: z_max_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  real(ids_real)  :: power_thermal=ids_real_invalid       ! /power_thermal - Collisional power to the thermal particle population
  real(ids_real)  :: power_thermal_error_upper=ids_real_invalid
  real(ids_real)  :: power_thermal_error_lower=ids_real_invalid
  integer(ids_int) :: power_thermal_error_index=ids_int_invalid

  real(ids_real)  :: power_fast=ids_real_invalid       ! /power_fast - Collisional power to the fast particle population
  real(ids_real)  :: power_fast_error_upper=ids_real_invalid
  real(ids_real)  :: power_fast_error_lower=ids_real_invalid
  integer(ids_int) :: power_fast_error_index=ids_int_invalid

  real(ids_real)  :: torque_thermal_tor=ids_real_invalid       ! /torque_thermal_tor - Collisional toroidal torque to the thermal particle population
  real(ids_real)  :: torque_thermal_tor_error_upper=ids_real_invalid
  real(ids_real)  :: torque_thermal_tor_error_lower=ids_real_invalid
  integer(ids_int) :: torque_thermal_tor_error_index=ids_int_invalid

  real(ids_real)  :: torque_fast_tor=ids_real_invalid       ! /torque_fast_tor - Collisional toroidal torque to the fast particle population
  real(ids_real)  :: torque_fast_tor_error_upper=ids_real_invalid
  real(ids_real)  :: torque_fast_tor_error_lower=ids_real_invalid
  integer(ids_int) :: torque_fast_tor_error_index=ids_int_invalid

endtype

type ids_distributions_d_global_quantities_collisions_ion  !    Global quantities for collisions with a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(ids_real)  :: z_ion=ids_real_invalid       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed).
  real(ids_real)  :: z_ion_error_upper=ids_real_invalid
  real(ids_real)  :: z_ion_error_lower=ids_real_invalid
  integer(ids_int) :: z_ion_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H+, D+, T+, He+2, C+, D2, DT, CD4, ...)
  integer(ids_int)  :: neutral_index=ids_int_invalid       ! /neutral_index - Index of the corresponding neutral species in the ../../neutral array
  real(ids_real)  :: power_thermal=ids_real_invalid       ! /power_thermal - Collisional power to the thermal particle population
  real(ids_real)  :: power_thermal_error_upper=ids_real_invalid
  real(ids_real)  :: power_thermal_error_lower=ids_real_invalid
  integer(ids_int) :: power_thermal_error_index=ids_int_invalid

  real(ids_real)  :: power_fast=ids_real_invalid       ! /power_fast - Collisional power to the fast particle population
  real(ids_real)  :: power_fast_error_upper=ids_real_invalid
  real(ids_real)  :: power_fast_error_lower=ids_real_invalid
  integer(ids_int) :: power_fast_error_index=ids_int_invalid

  real(ids_real)  :: torque_thermal_tor=ids_real_invalid       ! /torque_thermal_tor - Collisional toroidal torque to the thermal particle population
  real(ids_real)  :: torque_thermal_tor_error_upper=ids_real_invalid
  real(ids_real)  :: torque_thermal_tor_error_lower=ids_real_invalid
  integer(ids_int) :: torque_thermal_tor_error_index=ids_int_invalid

  real(ids_real)  :: torque_fast_tor=ids_real_invalid       ! /torque_fast_tor - Collisional toroidal torque to the fast particle population
  real(ids_real)  :: torque_fast_tor_error_upper=ids_real_invalid
  real(ids_real)  :: torque_fast_tor_error_lower=ids_real_invalid
  integer(ids_int) :: torque_fast_tor_error_index=ids_int_invalid

  integer(ids_int)  :: multiple_states_flag=ids_int_invalid       ! /multiple_states_flag - Multiple state calculation flag : 0-Only one state is considered; 1-Multiple states are considered a
  type (ids_distributions_d_global_quantities_collisions_ion_state),pointer :: state(:) => null()  ! /state(i) - Collisional exchange with the various states of the ion species (ionisation, energy, excitation, ...
endtype

type ids_distributions_d_global_quantities_collisions_electrons  !    Global quantities for collisions with electrons
  real(ids_real)  :: power_thermal=ids_real_invalid       ! /power_thermal - Collisional power to the thermal particle population
  real(ids_real)  :: power_thermal_error_upper=ids_real_invalid
  real(ids_real)  :: power_thermal_error_lower=ids_real_invalid
  integer(ids_int) :: power_thermal_error_index=ids_int_invalid

  real(ids_real)  :: power_fast=ids_real_invalid       ! /power_fast - Collisional power to the fast particle population
  real(ids_real)  :: power_fast_error_upper=ids_real_invalid
  real(ids_real)  :: power_fast_error_lower=ids_real_invalid
  integer(ids_int) :: power_fast_error_index=ids_int_invalid

  real(ids_real)  :: torque_thermal_tor=ids_real_invalid       ! /torque_thermal_tor - Collisional toroidal torque to the thermal particle population
  real(ids_real)  :: torque_thermal_tor_error_upper=ids_real_invalid
  real(ids_real)  :: torque_thermal_tor_error_lower=ids_real_invalid
  integer(ids_int) :: torque_thermal_tor_error_index=ids_int_invalid

  real(ids_real)  :: torque_fast_tor=ids_real_invalid       ! /torque_fast_tor - Collisional toroidal torque to the fast particle population
  real(ids_real)  :: torque_fast_tor_error_upper=ids_real_invalid
  real(ids_real)  :: torque_fast_tor_error_lower=ids_real_invalid
  integer(ids_int) :: torque_fast_tor_error_index=ids_int_invalid

endtype

type ids_distributions_d_global_quantities_collisions  !    Global quantities for collisions
  type (ids_distributions_d_global_quantities_collisions_electrons) :: electrons  ! /electrons - Collisional exchange with electrons
  type (ids_distributions_d_global_quantities_collisions_ion),pointer :: ion(:) => null()  ! /ion(i) - Collisional exchange with the various ion species
endtype

type ids_distributions_d_global_quantities  !    Global quantities from the distribution, for a given time slice
  real(ids_real)  :: particles_n=ids_real_invalid       ! /particles_n - Number of particles in the distribution, i.e. the volume integral of the density (note: this is the
  real(ids_real)  :: particles_n_error_upper=ids_real_invalid
  real(ids_real)  :: particles_n_error_lower=ids_real_invalid
  integer(ids_int) :: particles_n_error_index=ids_int_invalid

  real(ids_real)  :: particles_fast_n=ids_real_invalid       ! /particles_fast_n - Number of fast particles in the distribution, i.e. the volume integral of the density (note: this is
  real(ids_real)  :: particles_fast_n_error_upper=ids_real_invalid
  real(ids_real)  :: particles_fast_n_error_lower=ids_real_invalid
  integer(ids_int) :: particles_fast_n_error_index=ids_int_invalid

  real(ids_real)  :: energy=ids_real_invalid       ! /energy - Total energy in the distribution
  real(ids_real)  :: energy_error_upper=ids_real_invalid
  real(ids_real)  :: energy_error_lower=ids_real_invalid
  integer(ids_int) :: energy_error_index=ids_int_invalid

  real(ids_real)  :: energy_fast=ids_real_invalid       ! /energy_fast - Total energy of the fast particles in the distribution
  real(ids_real)  :: energy_fast_error_upper=ids_real_invalid
  real(ids_real)  :: energy_fast_error_lower=ids_real_invalid
  integer(ids_int) :: energy_fast_error_index=ids_int_invalid

  real(ids_real)  :: energy_fast_parallel=ids_real_invalid       ! /energy_fast_parallel - Parallel energy of the fast particles in the distribution
  real(ids_real)  :: energy_fast_parallel_error_upper=ids_real_invalid
  real(ids_real)  :: energy_fast_parallel_error_lower=ids_real_invalid
  integer(ids_int) :: energy_fast_parallel_error_index=ids_int_invalid

  real(ids_real)  :: torque_tor_j_radial=ids_real_invalid       ! /torque_tor_j_radial - Toroidal torque due to radial currents
  real(ids_real)  :: torque_tor_j_radial_error_upper=ids_real_invalid
  real(ids_real)  :: torque_tor_j_radial_error_lower=ids_real_invalid
  integer(ids_int) :: torque_tor_j_radial_error_index=ids_int_invalid

  real(ids_real)  :: current_tor=ids_real_invalid       ! /current_tor - Toroidal current driven by the distribution
  real(ids_real)  :: current_tor_error_upper=ids_real_invalid
  real(ids_real)  :: current_tor_error_lower=ids_real_invalid
  integer(ids_int) :: current_tor_error_index=ids_int_invalid

  type (ids_distributions_d_global_quantities_collisions) :: collisions  ! /collisions - Power and torque exchanged between the species described by the distribution and the different plasm
  type (ids_distributions_d_global_quantities_source),pointer :: source(:) => null()  ! /source(i) - Set of volume integrated sources and sinks of particles, momentum and energy included in the Fokker-
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_distributions_d_profiles_1d_thermalised  !    1D profiles of termalisation source/sinks
  real(ids_real),pointer  :: particles(:) => null()     ! /particles - Source rate of thermal particle density due to the thermalisation of fast particles
  real(ids_real),pointer  :: particles_error_upper(:) => null()
  real(ids_real),pointer  :: particles_error_lower(:) => null()
  integer(ids_int) :: particles_error_index=ids_int_invalid

  real(ids_real),pointer  :: energy(:) => null()     ! /energy - Source rate of energy density within the thermal particle population due to the thermalisation of fa
  real(ids_real),pointer  :: energy_error_upper(:) => null()
  real(ids_real),pointer  :: energy_error_lower(:) => null()
  integer(ids_int) :: energy_error_index=ids_int_invalid

  real(ids_real),pointer  :: momentum_tor(:) => null()     ! /momentum_tor - Source rate of toroidal angular momentum density within the thermal particle population due to the t
  real(ids_real),pointer  :: momentum_tor_error_upper(:) => null()
  real(ids_real),pointer  :: momentum_tor_error_lower(:) => null()
  integer(ids_int) :: momentum_tor_error_index=ids_int_invalid

endtype

type ids_distributions_d_profiles_1d_source  !    1D profiles for a given source/sink term
  type (ids_distributions_d_source_identifier) :: identifier  ! /identifier - Identifier of the wave or particle source process, defined respectively in distribution/wave or dist
  real(ids_real),pointer  :: particles(:) => null()     ! /particles - Source rate of thermal particle density
  real(ids_real),pointer  :: particles_error_upper(:) => null()
  real(ids_real),pointer  :: particles_error_lower(:) => null()
  integer(ids_int) :: particles_error_index=ids_int_invalid

  real(ids_real),pointer  :: energy(:) => null()     ! /energy - Source rate of energy density
  real(ids_real),pointer  :: energy_error_upper(:) => null()
  real(ids_real),pointer  :: energy_error_lower(:) => null()
  integer(ids_int) :: energy_error_index=ids_int_invalid

  real(ids_real),pointer  :: momentum_tor(:) => null()     ! /momentum_tor - Source rate of toroidal angular momentum density
  real(ids_real),pointer  :: momentum_tor_error_upper(:) => null()
  real(ids_real),pointer  :: momentum_tor_error_lower(:) => null()
  integer(ids_int) :: momentum_tor_error_index=ids_int_invalid

endtype

type ids_distributions_d_profiles_1d_partial_source  !    1D profiles for a given source/sink term
  type (ids_distributions_d_source_identifier) :: identifier  ! /identifier - Identifier of the wave or particle source process, defined respectively in distribution/wave or dist
  real(ids_real),pointer  :: particles(:) => null()     ! /particles - Source rate of thermal particle density
  real(ids_real),pointer  :: particles_error_upper(:) => null()
  real(ids_real),pointer  :: particles_error_lower(:) => null()
  integer(ids_int) :: particles_error_index=ids_int_invalid

  real(ids_real),pointer  :: energy(:) => null()     ! /energy - Source rate of energy density
  real(ids_real),pointer  :: energy_error_upper(:) => null()
  real(ids_real),pointer  :: energy_error_lower(:) => null()
  integer(ids_int) :: energy_error_index=ids_int_invalid

  real(ids_real),pointer  :: momentum_tor(:) => null()     ! /momentum_tor - Source rate of toroidal angular momentum density
  real(ids_real),pointer  :: momentum_tor_error_upper(:) => null()
  real(ids_real),pointer  :: momentum_tor_error_lower(:) => null()
  integer(ids_int) :: momentum_tor_error_index=ids_int_invalid

endtype

type ids_distributions_d_profiles_2d_collisions_ion  !    2D profiles for collisions with a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(ids_real)  :: z_ion=ids_real_invalid       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed).
  real(ids_real)  :: z_ion_error_upper=ids_real_invalid
  real(ids_real)  :: z_ion_error_lower=ids_real_invalid
  integer(ids_int) :: z_ion_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H+, D+, T+, He+2, C+, D2, DT, CD4, ...)
  integer(ids_int)  :: neutral_index=ids_int_invalid       ! /neutral_index - Index of the corresponding neutral species in the ../../neutral array
  real(ids_real),pointer  :: power_thermal(:,:) => null()     ! /power_thermal - Collisional power density to the thermal particle population
  real(ids_real),pointer  :: power_thermal_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_thermal_error_lower(:,:) => null()
  integer(ids_int) :: power_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_fast(:,:) => null()     ! /power_fast - Collisional power density to the fast particle population
  real(ids_real),pointer  :: power_fast_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_fast_error_lower(:,:) => null()
  integer(ids_int) :: power_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_thermal_tor(:,:) => null()     ! /torque_thermal_tor - Collisional toroidal torque density to the thermal particle population
  real(ids_real),pointer  :: torque_thermal_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: torque_thermal_tor_error_lower(:,:) => null()
  integer(ids_int) :: torque_thermal_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_fast_tor(:,:) => null()     ! /torque_fast_tor - Collisional toroidal torque density to the fast particle population
  real(ids_real),pointer  :: torque_fast_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: torque_fast_tor_error_lower(:,:) => null()
  integer(ids_int) :: torque_fast_tor_error_index=ids_int_invalid

  integer(ids_int)  :: multiple_states_flag=ids_int_invalid       ! /multiple_states_flag - Multiple state calculation flag : 0-Only one state is considered; 1-Multiple states are considered a
  type (ids_distributions_d_profiles_2d_collisions_ion_state),pointer :: state(:) => null()  ! /state(i) - Collisional exchange with the various states of the ion species (ionisation, energy, excitation, ...
endtype

type ids_distributions_d_profiles_2d_partial_collisions_ion  !    2D profiles for collisions with a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(ids_real)  :: z_ion=ids_real_invalid       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed).
  real(ids_real)  :: z_ion_error_upper=ids_real_invalid
  real(ids_real)  :: z_ion_error_lower=ids_real_invalid
  integer(ids_int) :: z_ion_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H+, D+, T+, He+2, C+, D2, DT, CD4, ...)
  integer(ids_int)  :: neutral_index=ids_int_invalid       ! /neutral_index - Index of the corresponding neutral species in the ../../neutral array
  real(ids_real),pointer  :: power_thermal(:,:) => null()     ! /power_thermal - Collisional power density to the thermal particle population
  real(ids_real),pointer  :: power_thermal_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_thermal_error_lower(:,:) => null()
  integer(ids_int) :: power_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_fast(:,:) => null()     ! /power_fast - Collisional power density to the fast particle population
  real(ids_real),pointer  :: power_fast_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_fast_error_lower(:,:) => null()
  integer(ids_int) :: power_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_thermal_tor(:,:) => null()     ! /torque_thermal_tor - Collisional toroidal torque density to the thermal particle population
  real(ids_real),pointer  :: torque_thermal_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: torque_thermal_tor_error_lower(:,:) => null()
  integer(ids_int) :: torque_thermal_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_fast_tor(:,:) => null()     ! /torque_fast_tor - Collisional toroidal torque density to the fast particle population
  real(ids_real),pointer  :: torque_fast_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: torque_fast_tor_error_lower(:,:) => null()
  integer(ids_int) :: torque_fast_tor_error_index=ids_int_invalid

  integer(ids_int)  :: multiple_states_flag=ids_int_invalid       ! /multiple_states_flag - Multiple state calculation flag : 0-Only one state is considered; 1-Multiple states are considered a
  type (ids_distributions_d_profiles_2d_partial_collisions_ion_state),pointer :: state(:) => null()  ! /state(i) - Collisional exchange with the various states of the ion species (ionisation, energy, excitation, ...
endtype

type ids_distributions_d_profiles_2d_collisions_ion_state  !    2D profiles for collisions with a given ion species state
  real(ids_real)  :: z_min=ids_real_invalid       ! /z_min - Minimum Z of the charge state bundle (z_min = z_max = 0 for a neutral)
  real(ids_real)  :: z_min_error_upper=ids_real_invalid
  real(ids_real)  :: z_min_error_lower=ids_real_invalid
  integer(ids_int) :: z_min_error_index=ids_int_invalid

  real(ids_real)  :: z_max=ids_real_invalid       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(ids_real)  :: z_max_error_upper=ids_real_invalid
  real(ids_real)  :: z_max_error_lower=ids_real_invalid
  integer(ids_int) :: z_max_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  real(ids_real),pointer  :: power_thermal(:,:) => null()     ! /power_thermal - Collisional power density to the thermal particle population
  real(ids_real),pointer  :: power_thermal_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_thermal_error_lower(:,:) => null()
  integer(ids_int) :: power_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_fast(:,:) => null()     ! /power_fast - Collisional power density to the fast particle population
  real(ids_real),pointer  :: power_fast_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_fast_error_lower(:,:) => null()
  integer(ids_int) :: power_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_thermal_tor(:,:) => null()     ! /torque_thermal_tor - Collisional toroidal torque density to the thermal particle population
  real(ids_real),pointer  :: torque_thermal_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: torque_thermal_tor_error_lower(:,:) => null()
  integer(ids_int) :: torque_thermal_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_fast_tor(:,:) => null()     ! /torque_fast_tor - Collisional toroidal torque density to the fast particle population
  real(ids_real),pointer  :: torque_fast_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: torque_fast_tor_error_lower(:,:) => null()
  integer(ids_int) :: torque_fast_tor_error_index=ids_int_invalid

endtype

type ids_distributions_d_profiles_2d_partial_collisions_ion_state  !    2D profiles for collisions with a given ion species state
  real(ids_real)  :: z_min=ids_real_invalid       ! /z_min - Minimum Z of the charge state bundle (z_min = z_max = 0 for a neutral)
  real(ids_real)  :: z_min_error_upper=ids_real_invalid
  real(ids_real)  :: z_min_error_lower=ids_real_invalid
  integer(ids_int) :: z_min_error_index=ids_int_invalid

  real(ids_real)  :: z_max=ids_real_invalid       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(ids_real)  :: z_max_error_upper=ids_real_invalid
  real(ids_real)  :: z_max_error_lower=ids_real_invalid
  integer(ids_int) :: z_max_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  real(ids_real),pointer  :: power_thermal(:,:) => null()     ! /power_thermal - Collisional power density to the thermal particle population
  real(ids_real),pointer  :: power_thermal_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_thermal_error_lower(:,:) => null()
  integer(ids_int) :: power_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_fast(:,:) => null()     ! /power_fast - Collisional power density to the fast particle population
  real(ids_real),pointer  :: power_fast_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_fast_error_lower(:,:) => null()
  integer(ids_int) :: power_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_thermal_tor(:,:) => null()     ! /torque_thermal_tor - Collisional toroidal torque density to the thermal particle population
  real(ids_real),pointer  :: torque_thermal_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: torque_thermal_tor_error_lower(:,:) => null()
  integer(ids_int) :: torque_thermal_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_fast_tor(:,:) => null()     ! /torque_fast_tor - Collisional toroidal torque density to the fast particle population
  real(ids_real),pointer  :: torque_fast_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: torque_fast_tor_error_lower(:,:) => null()
  integer(ids_int) :: torque_fast_tor_error_index=ids_int_invalid

endtype

type ids_distributions_d_profiles_2d_collisions_electrons  !    2D profiles for collisions with electrons
  real(ids_real),pointer  :: power_thermal(:,:) => null()     ! /power_thermal - Collisional power density to the thermal particle population
  real(ids_real),pointer  :: power_thermal_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_thermal_error_lower(:,:) => null()
  integer(ids_int) :: power_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_fast(:,:) => null()     ! /power_fast - Collisional power density to the fast particle population
  real(ids_real),pointer  :: power_fast_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_fast_error_lower(:,:) => null()
  integer(ids_int) :: power_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_thermal_tor(:,:) => null()     ! /torque_thermal_tor - Collisional toroidal torque density to the thermal particle population
  real(ids_real),pointer  :: torque_thermal_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: torque_thermal_tor_error_lower(:,:) => null()
  integer(ids_int) :: torque_thermal_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_fast_tor(:,:) => null()     ! /torque_fast_tor - Collisional toroidal torque density to the fast particle population
  real(ids_real),pointer  :: torque_fast_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: torque_fast_tor_error_lower(:,:) => null()
  integer(ids_int) :: torque_fast_tor_error_index=ids_int_invalid

endtype

type ids_distributions_d_profiles_2d_partial_collisions_electrons  !    2D profiles for collisions with electrons
  real(ids_real),pointer  :: power_thermal(:,:) => null()     ! /power_thermal - Collisional power density to the thermal particle population
  real(ids_real),pointer  :: power_thermal_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_thermal_error_lower(:,:) => null()
  integer(ids_int) :: power_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_fast(:,:) => null()     ! /power_fast - Collisional power density to the fast particle population
  real(ids_real),pointer  :: power_fast_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_fast_error_lower(:,:) => null()
  integer(ids_int) :: power_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_thermal_tor(:,:) => null()     ! /torque_thermal_tor - Collisional toroidal torque density to the thermal particle population
  real(ids_real),pointer  :: torque_thermal_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: torque_thermal_tor_error_lower(:,:) => null()
  integer(ids_int) :: torque_thermal_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_fast_tor(:,:) => null()     ! /torque_fast_tor - Collisional toroidal torque density to the fast particle population
  real(ids_real),pointer  :: torque_fast_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: torque_fast_tor_error_lower(:,:) => null()
  integer(ids_int) :: torque_fast_tor_error_index=ids_int_invalid

endtype

type ids_distributions_d_profiles_2d_collisions  !    2D profiles for collisions
  type (ids_distributions_d_profiles_2d_collisions_electrons) :: electrons  ! /electrons - Collisional exchange with electrons
  type (ids_distributions_d_profiles_2d_collisions_ion),pointer :: ion(:) => null()  ! /ion(i) - Collisional exchange with the various ion species
endtype

type ids_distributions_d_profiles_2d_partial_collisions  !    2D profiles for collisions
  type (ids_distributions_d_profiles_2d_partial_collisions_electrons) :: electrons  ! /electrons - Collisional exchange with electrons
  type (ids_distributions_d_profiles_2d_partial_collisions_ion),pointer :: ion(:) => null()  ! /ion(i) - Collisional exchange with the various ion species
endtype

type ids_distributions_d_profiles_2d_partial  !    2D profiles from specific particles in the distribution (trapped, co or counter-passing)
  real(ids_real),pointer  :: density(:,:) => null()     ! /density - Density (thermal+non-thermal)
  real(ids_real),pointer  :: density_error_upper(:,:) => null()
  real(ids_real),pointer  :: density_error_lower(:,:) => null()
  integer(ids_int) :: density_error_index=ids_int_invalid

  real(ids_real),pointer  :: density_fast(:,:) => null()     ! /density_fast - Density of fast particles
  real(ids_real),pointer  :: density_fast_error_upper(:,:) => null()
  real(ids_real),pointer  :: density_fast_error_lower(:,:) => null()
  integer(ids_int) :: density_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure(:,:) => null()     ! /pressure - Pressure (thermal+non-thermal)
  real(ids_real),pointer  :: pressure_error_upper(:,:) => null()
  real(ids_real),pointer  :: pressure_error_lower(:,:) => null()
  integer(ids_int) :: pressure_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_fast(:,:) => null()     ! /pressure_fast - Pressure of fast particles
  real(ids_real),pointer  :: pressure_fast_error_upper(:,:) => null()
  real(ids_real),pointer  :: pressure_fast_error_lower(:,:) => null()
  integer(ids_int) :: pressure_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_fast_parallel(:,:) => null()     ! /pressure_fast_parallel - Pressure of fast particles in the parallel direction
  real(ids_real),pointer  :: pressure_fast_parallel_error_upper(:,:) => null()
  real(ids_real),pointer  :: pressure_fast_parallel_error_lower(:,:) => null()
  integer(ids_int) :: pressure_fast_parallel_error_index=ids_int_invalid

  real(ids_real),pointer  :: current_tor(:,:) => null()     ! /current_tor - Total toroidal driven current density (including electron and thermal ion back-current, or drag-curr
  real(ids_real),pointer  :: current_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: current_tor_error_lower(:,:) => null()
  integer(ids_int) :: current_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: current_fast_tor(:,:) => null()     ! /current_fast_tor - Total toroidal driven current density of fast (non-thermal) particles (excluding electron and therma
  real(ids_real),pointer  :: current_fast_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: current_fast_tor_error_lower(:,:) => null()
  integer(ids_int) :: current_fast_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_tor_j_radial(:,:) => null()     ! /torque_tor_j_radial - Toroidal torque due to radial currents
  real(ids_real),pointer  :: torque_tor_j_radial_error_upper(:,:) => null()
  real(ids_real),pointer  :: torque_tor_j_radial_error_lower(:,:) => null()
  integer(ids_int) :: torque_tor_j_radial_error_index=ids_int_invalid

  type (ids_distributions_d_profiles_2d_partial_collisions) :: collisions  ! /collisions - Power and torque exchanged between the species described by the distribution and the different plasm
endtype

type ids_distributions_d_profiles_1d_collisions_ion  !    1D profiles for collisions with a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(ids_real)  :: z_ion=ids_real_invalid       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed).
  real(ids_real)  :: z_ion_error_upper=ids_real_invalid
  real(ids_real)  :: z_ion_error_lower=ids_real_invalid
  integer(ids_int) :: z_ion_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H+, D+, T+, He+2, C+, D2, DT, CD4, ...)
  integer(ids_int)  :: neutral_index=ids_int_invalid       ! /neutral_index - Index of the corresponding neutral species in the ../../neutral array
  real(ids_real),pointer  :: power_thermal(:) => null()     ! /power_thermal - Collisional power density to the thermal particle population
  real(ids_real),pointer  :: power_thermal_error_upper(:) => null()
  real(ids_real),pointer  :: power_thermal_error_lower(:) => null()
  integer(ids_int) :: power_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_fast(:) => null()     ! /power_fast - Collisional power density to the fast particle population
  real(ids_real),pointer  :: power_fast_error_upper(:) => null()
  real(ids_real),pointer  :: power_fast_error_lower(:) => null()
  integer(ids_int) :: power_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_thermal_tor(:) => null()     ! /torque_thermal_tor - Collisional toroidal torque density to the thermal particle population
  real(ids_real),pointer  :: torque_thermal_tor_error_upper(:) => null()
  real(ids_real),pointer  :: torque_thermal_tor_error_lower(:) => null()
  integer(ids_int) :: torque_thermal_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_fast_tor(:) => null()     ! /torque_fast_tor - Collisional toroidal torque density to the fast particle population
  real(ids_real),pointer  :: torque_fast_tor_error_upper(:) => null()
  real(ids_real),pointer  :: torque_fast_tor_error_lower(:) => null()
  integer(ids_int) :: torque_fast_tor_error_index=ids_int_invalid

  integer(ids_int)  :: multiple_states_flag=ids_int_invalid       ! /multiple_states_flag - Multiple state calculation flag : 0-Only one state is considered; 1-Multiple states are considered a
  type (ids_distributions_d_profiles_1d_collisions_ion_state),pointer :: state(:) => null()  ! /state(i) - Collisional exchange with the various states of the ion species (ionisation, energy, excitation, ...
endtype

type ids_distributions_d_profiles_1d_partial_collisions_ion  !    1D profiles for collisions with a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(ids_real)  :: z_ion=ids_real_invalid       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed).
  real(ids_real)  :: z_ion_error_upper=ids_real_invalid
  real(ids_real)  :: z_ion_error_lower=ids_real_invalid
  integer(ids_int) :: z_ion_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H+, D+, T+, He+2, C+, D2, DT, CD4, ...)
  integer(ids_int)  :: neutral_index=ids_int_invalid       ! /neutral_index - Index of the corresponding neutral species in the ../../neutral array
  real(ids_real),pointer  :: power_thermal(:) => null()     ! /power_thermal - Collisional power density to the thermal particle population
  real(ids_real),pointer  :: power_thermal_error_upper(:) => null()
  real(ids_real),pointer  :: power_thermal_error_lower(:) => null()
  integer(ids_int) :: power_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_fast(:) => null()     ! /power_fast - Collisional power density to the fast particle population
  real(ids_real),pointer  :: power_fast_error_upper(:) => null()
  real(ids_real),pointer  :: power_fast_error_lower(:) => null()
  integer(ids_int) :: power_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_thermal_tor(:) => null()     ! /torque_thermal_tor - Collisional toroidal torque density to the thermal particle population
  real(ids_real),pointer  :: torque_thermal_tor_error_upper(:) => null()
  real(ids_real),pointer  :: torque_thermal_tor_error_lower(:) => null()
  integer(ids_int) :: torque_thermal_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_fast_tor(:) => null()     ! /torque_fast_tor - Collisional toroidal torque density to the fast particle population
  real(ids_real),pointer  :: torque_fast_tor_error_upper(:) => null()
  real(ids_real),pointer  :: torque_fast_tor_error_lower(:) => null()
  integer(ids_int) :: torque_fast_tor_error_index=ids_int_invalid

  integer(ids_int)  :: multiple_states_flag=ids_int_invalid       ! /multiple_states_flag - Multiple state calculation flag : 0-Only one state is considered; 1-Multiple states are considered a
  type (ids_distributions_d_profiles_1d_partial_collisions_ion_state),pointer :: state(:) => null()  ! /state(i) - Collisional exchange with the various states of the ion species (ionisation, energy, excitation, ...
endtype

type ids_distributions_d_profiles_1d_collisions_ion_state  !    1D profiles for collisions with a given ion species state
  real(ids_real)  :: z_min=ids_real_invalid       ! /z_min - Minimum Z of the charge state bundle (z_min = z_max = 0 for a neutral)
  real(ids_real)  :: z_min_error_upper=ids_real_invalid
  real(ids_real)  :: z_min_error_lower=ids_real_invalid
  integer(ids_int) :: z_min_error_index=ids_int_invalid

  real(ids_real)  :: z_max=ids_real_invalid       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(ids_real)  :: z_max_error_upper=ids_real_invalid
  real(ids_real)  :: z_max_error_lower=ids_real_invalid
  integer(ids_int) :: z_max_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  real(ids_real),pointer  :: power_thermal(:) => null()     ! /power_thermal - Collisional power density to the thermal particle population
  real(ids_real),pointer  :: power_thermal_error_upper(:) => null()
  real(ids_real),pointer  :: power_thermal_error_lower(:) => null()
  integer(ids_int) :: power_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_fast(:) => null()     ! /power_fast - Collisional power density to the fast particle population
  real(ids_real),pointer  :: power_fast_error_upper(:) => null()
  real(ids_real),pointer  :: power_fast_error_lower(:) => null()
  integer(ids_int) :: power_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_thermal_tor(:) => null()     ! /torque_thermal_tor - Collisional toroidal torque density to the thermal particle population
  real(ids_real),pointer  :: torque_thermal_tor_error_upper(:) => null()
  real(ids_real),pointer  :: torque_thermal_tor_error_lower(:) => null()
  integer(ids_int) :: torque_thermal_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_fast_tor(:) => null()     ! /torque_fast_tor - Collisional toroidal torque density to the fast particle population
  real(ids_real),pointer  :: torque_fast_tor_error_upper(:) => null()
  real(ids_real),pointer  :: torque_fast_tor_error_lower(:) => null()
  integer(ids_int) :: torque_fast_tor_error_index=ids_int_invalid

endtype

type ids_distributions_d_profiles_1d_partial_collisions_ion_state  !    1D profiles for collisions with a given ion species state
  real(ids_real)  :: z_min=ids_real_invalid       ! /z_min - Minimum Z of the charge state bundle (z_min = z_max = 0 for a neutral)
  real(ids_real)  :: z_min_error_upper=ids_real_invalid
  real(ids_real)  :: z_min_error_lower=ids_real_invalid
  integer(ids_int) :: z_min_error_index=ids_int_invalid

  real(ids_real)  :: z_max=ids_real_invalid       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(ids_real)  :: z_max_error_upper=ids_real_invalid
  real(ids_real)  :: z_max_error_lower=ids_real_invalid
  integer(ids_int) :: z_max_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  real(ids_real),pointer  :: power_thermal(:) => null()     ! /power_thermal - Collisional power density to the thermal particle population
  real(ids_real),pointer  :: power_thermal_error_upper(:) => null()
  real(ids_real),pointer  :: power_thermal_error_lower(:) => null()
  integer(ids_int) :: power_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_fast(:) => null()     ! /power_fast - Collisional power density to the fast particle population
  real(ids_real),pointer  :: power_fast_error_upper(:) => null()
  real(ids_real),pointer  :: power_fast_error_lower(:) => null()
  integer(ids_int) :: power_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_thermal_tor(:) => null()     ! /torque_thermal_tor - Collisional toroidal torque density to the thermal particle population
  real(ids_real),pointer  :: torque_thermal_tor_error_upper(:) => null()
  real(ids_real),pointer  :: torque_thermal_tor_error_lower(:) => null()
  integer(ids_int) :: torque_thermal_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_fast_tor(:) => null()     ! /torque_fast_tor - Collisional toroidal torque density to the fast particle population
  real(ids_real),pointer  :: torque_fast_tor_error_upper(:) => null()
  real(ids_real),pointer  :: torque_fast_tor_error_lower(:) => null()
  integer(ids_int) :: torque_fast_tor_error_index=ids_int_invalid

endtype

type ids_distributions_d_profiles_1d_collisions_electrons  !    1D profiles for collisions with electrons
  real(ids_real),pointer  :: power_thermal(:) => null()     ! /power_thermal - Collisional power density to the thermal particle population
  real(ids_real),pointer  :: power_thermal_error_upper(:) => null()
  real(ids_real),pointer  :: power_thermal_error_lower(:) => null()
  integer(ids_int) :: power_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_fast(:) => null()     ! /power_fast - Collisional power density to the fast particle population
  real(ids_real),pointer  :: power_fast_error_upper(:) => null()
  real(ids_real),pointer  :: power_fast_error_lower(:) => null()
  integer(ids_int) :: power_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_thermal_tor(:) => null()     ! /torque_thermal_tor - Collisional toroidal torque density to the thermal particle population
  real(ids_real),pointer  :: torque_thermal_tor_error_upper(:) => null()
  real(ids_real),pointer  :: torque_thermal_tor_error_lower(:) => null()
  integer(ids_int) :: torque_thermal_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_fast_tor(:) => null()     ! /torque_fast_tor - Collisional toroidal torque density to the fast particle population
  real(ids_real),pointer  :: torque_fast_tor_error_upper(:) => null()
  real(ids_real),pointer  :: torque_fast_tor_error_lower(:) => null()
  integer(ids_int) :: torque_fast_tor_error_index=ids_int_invalid

endtype

type ids_distributions_d_profiles_1d_partial_collisions_electrons  !    1D profiles for collisions with electrons
  real(ids_real),pointer  :: power_thermal(:) => null()     ! /power_thermal - Collisional power density to the thermal particle population
  real(ids_real),pointer  :: power_thermal_error_upper(:) => null()
  real(ids_real),pointer  :: power_thermal_error_lower(:) => null()
  integer(ids_int) :: power_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_fast(:) => null()     ! /power_fast - Collisional power density to the fast particle population
  real(ids_real),pointer  :: power_fast_error_upper(:) => null()
  real(ids_real),pointer  :: power_fast_error_lower(:) => null()
  integer(ids_int) :: power_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_thermal_tor(:) => null()     ! /torque_thermal_tor - Collisional toroidal torque density to the thermal particle population
  real(ids_real),pointer  :: torque_thermal_tor_error_upper(:) => null()
  real(ids_real),pointer  :: torque_thermal_tor_error_lower(:) => null()
  integer(ids_int) :: torque_thermal_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_fast_tor(:) => null()     ! /torque_fast_tor - Collisional toroidal torque density to the fast particle population
  real(ids_real),pointer  :: torque_fast_tor_error_upper(:) => null()
  real(ids_real),pointer  :: torque_fast_tor_error_lower(:) => null()
  integer(ids_int) :: torque_fast_tor_error_index=ids_int_invalid

endtype

type ids_distributions_d_profiles_1d_collisions  !    1D profiles for collisions
  type (ids_distributions_d_profiles_1d_collisions_electrons) :: electrons  ! /electrons - Collisional exchange with electrons
  type (ids_distributions_d_profiles_1d_collisions_ion),pointer :: ion(:) => null()  ! /ion(i) - Collisional exchange with the various ion species
endtype

type ids_distributions_d_profiles_1d_partial_collisions  !    1D profiles for collisions
  type (ids_distributions_d_profiles_1d_partial_collisions_electrons) :: electrons  ! /electrons - Collisional exchange with electrons
  type (ids_distributions_d_profiles_1d_partial_collisions_ion),pointer :: ion(:) => null()  ! /ion(i) - Collisional exchange with the various ion species
endtype

type ids_distributions_d_profiles_1d_partial  !    1D profiles from specific particles in the distribution (trapped, co or counter-passing)
  real(ids_real),pointer  :: density(:) => null()     ! /density - Density (thermal+non-thermal)
  real(ids_real),pointer  :: density_error_upper(:) => null()
  real(ids_real),pointer  :: density_error_lower(:) => null()
  integer(ids_int) :: density_error_index=ids_int_invalid

  real(ids_real),pointer  :: density_fast(:) => null()     ! /density_fast - Density of fast particles
  real(ids_real),pointer  :: density_fast_error_upper(:) => null()
  real(ids_real),pointer  :: density_fast_error_lower(:) => null()
  integer(ids_int) :: density_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure(:) => null()     ! /pressure - Pressure (thermal+non-thermal)
  real(ids_real),pointer  :: pressure_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_error_lower(:) => null()
  integer(ids_int) :: pressure_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_fast(:) => null()     ! /pressure_fast - Pressure of fast particles
  real(ids_real),pointer  :: pressure_fast_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_fast_error_lower(:) => null()
  integer(ids_int) :: pressure_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_fast_parallel(:) => null()     ! /pressure_fast_parallel - Pressure of fast particles in the parallel direction
  real(ids_real),pointer  :: pressure_fast_parallel_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_fast_parallel_error_lower(:) => null()
  integer(ids_int) :: pressure_fast_parallel_error_index=ids_int_invalid

  real(ids_real),pointer  :: current_tor(:) => null()     ! /current_tor - Total toroidal driven current density (including electron and thermal ion back-current, or drag-curr
  real(ids_real),pointer  :: current_tor_error_upper(:) => null()
  real(ids_real),pointer  :: current_tor_error_lower(:) => null()
  integer(ids_int) :: current_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: current_fast_tor(:) => null()     ! /current_fast_tor - Total toroidal driven current density of fast (non-thermal) particles (excluding electron and therma
  real(ids_real),pointer  :: current_fast_tor_error_upper(:) => null()
  real(ids_real),pointer  :: current_fast_tor_error_lower(:) => null()
  integer(ids_int) :: current_fast_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_tor_j_radial(:) => null()     ! /torque_tor_j_radial - Toroidal torque due to radial currents
  real(ids_real),pointer  :: torque_tor_j_radial_error_upper(:) => null()
  real(ids_real),pointer  :: torque_tor_j_radial_error_lower(:) => null()
  integer(ids_int) :: torque_tor_j_radial_error_index=ids_int_invalid

  type (ids_distributions_d_profiles_1d_partial_collisions) :: collisions  ! /collisions - Power and torque exchanged between the species described by the distribution and the different plasm
  type (ids_distributions_d_profiles_1d_partial_source),pointer :: source(:) => null()  ! /source(i) - Set of flux averaged sources and sinks of particles, momentum and energy included in the Fokker-Plan
endtype

type ids_distributions_d_profiles_1d  !    1D profiles from the distribution, for a given time slice
  type (ids_core_radial_grid) :: grid  ! /grid - Radial grid
  real(ids_real),pointer  :: density(:) => null()     ! /density - Density (thermal+non-thermal)
  real(ids_real),pointer  :: density_error_upper(:) => null()
  real(ids_real),pointer  :: density_error_lower(:) => null()
  integer(ids_int) :: density_error_index=ids_int_invalid

  real(ids_real),pointer  :: density_fast(:) => null()     ! /density_fast - Density of fast particles
  real(ids_real),pointer  :: density_fast_error_upper(:) => null()
  real(ids_real),pointer  :: density_fast_error_lower(:) => null()
  integer(ids_int) :: density_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure(:) => null()     ! /pressure - Pressure (thermal+non-thermal)
  real(ids_real),pointer  :: pressure_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_error_lower(:) => null()
  integer(ids_int) :: pressure_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_fast(:) => null()     ! /pressure_fast - Pressure of fast particles
  real(ids_real),pointer  :: pressure_fast_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_fast_error_lower(:) => null()
  integer(ids_int) :: pressure_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_fast_parallel(:) => null()     ! /pressure_fast_parallel - Pressure of fast particles in the parallel direction
  real(ids_real),pointer  :: pressure_fast_parallel_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_fast_parallel_error_lower(:) => null()
  integer(ids_int) :: pressure_fast_parallel_error_index=ids_int_invalid

  real(ids_real),pointer  :: current_tor(:) => null()     ! /current_tor - Total toroidal driven current density (including electron and thermal ion back-current, or drag-curr
  real(ids_real),pointer  :: current_tor_error_upper(:) => null()
  real(ids_real),pointer  :: current_tor_error_lower(:) => null()
  integer(ids_int) :: current_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: current_fast_tor(:) => null()     ! /current_fast_tor - Total toroidal driven current density of fast (non-thermal) particles (excluding electron and therma
  real(ids_real),pointer  :: current_fast_tor_error_upper(:) => null()
  real(ids_real),pointer  :: current_fast_tor_error_lower(:) => null()
  integer(ids_int) :: current_fast_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_tor_j_radial(:) => null()     ! /torque_tor_j_radial - Toroidal torque due to radial currents
  real(ids_real),pointer  :: torque_tor_j_radial_error_upper(:) => null()
  real(ids_real),pointer  :: torque_tor_j_radial_error_lower(:) => null()
  integer(ids_int) :: torque_tor_j_radial_error_index=ids_int_invalid

  type (ids_distributions_d_profiles_1d_collisions) :: collisions  ! /collisions - Power and torque exchanged between the species described by the distribution and the different plasm
  type (ids_distributions_d_profiles_1d_thermalised) :: thermalisation  ! /thermalisation - Flux surface averaged source of thermal particles, momentum and energy due to thermalisation. Here t
  type (ids_distributions_d_profiles_1d_source),pointer :: source(:) => null()  ! /source(i) - Set of flux averaged sources and sinks of particles, momentum and energy included in the Fokker-Plan
  type (ids_distributions_d_profiles_1d_partial) :: trapped  ! /trapped - Flux surface averaged profile evaluated using the trapped particle part of the distribution.
  type (ids_distributions_d_profiles_1d_partial) :: co_passing  ! /co_passing - Flux surface averaged profile evaluated using the co-passing particle part of the distribution.
  type (ids_distributions_d_profiles_1d_partial) :: counter_passing  ! /counter_passing - Flux surface averaged profile evaluated using the counter-passing particle part of the distribution.
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_distributions_d_profiles_2d_grid  !    2D grid for the distribution
  type (ids_identifier_dynamic_aos3) :: type  ! /type - Grid type: index=0: Rectangular grid in the (R,Z) coordinates; index=1: Rectangular grid in the (rad
  real(ids_real),pointer  :: r(:) => null()     ! /r - Major radius
  real(ids_real),pointer  :: r_error_upper(:) => null()
  real(ids_real),pointer  :: r_error_lower(:) => null()
  integer(ids_int) :: r_error_index=ids_int_invalid

  real(ids_real),pointer  :: z(:) => null()     ! /z - Height
  real(ids_real),pointer  :: z_error_upper(:) => null()
  real(ids_real),pointer  :: z_error_lower(:) => null()
  integer(ids_int) :: z_error_index=ids_int_invalid

  real(ids_real),pointer  :: theta_straight(:) => null()     ! /theta_straight - Straight field line poloidal angle
  real(ids_real),pointer  :: theta_straight_error_upper(:) => null()
  real(ids_real),pointer  :: theta_straight_error_lower(:) => null()
  integer(ids_int) :: theta_straight_error_index=ids_int_invalid

  real(ids_real),pointer  :: theta_geometric(:) => null()     ! /theta_geometric - Geometrical poloidal angle
  real(ids_real),pointer  :: theta_geometric_error_upper(:) => null()
  real(ids_real),pointer  :: theta_geometric_error_lower(:) => null()
  integer(ids_int) :: theta_geometric_error_index=ids_int_invalid

  real(ids_real),pointer  :: rho_tor_norm(:) => null()     ! /rho_tor_norm - Normalised toroidal flux coordinate. The normalizing value for rho_tor_norm, is the toroidal flux co
  real(ids_real),pointer  :: rho_tor_norm_error_upper(:) => null()
  real(ids_real),pointer  :: rho_tor_norm_error_lower(:) => null()
  integer(ids_int) :: rho_tor_norm_error_index=ids_int_invalid

  real(ids_real),pointer  :: rho_tor(:) => null()     ! /rho_tor - Toroidal flux coordinate. The toroidal field used in its definition is indicated under vacuum_toroid
  real(ids_real),pointer  :: rho_tor_error_upper(:) => null()
  real(ids_real),pointer  :: rho_tor_error_lower(:) => null()
  integer(ids_int) :: rho_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: psi(:) => null()     ! /psi - Poloidal magnetic flux
  real(ids_real),pointer  :: psi_error_upper(:) => null()
  real(ids_real),pointer  :: psi_error_lower(:) => null()
  integer(ids_int) :: psi_error_index=ids_int_invalid

  real(ids_real),pointer  :: volume(:) => null()     ! /volume - Volume enclosed inside the magnetic surface
  real(ids_real),pointer  :: volume_error_upper(:) => null()
  real(ids_real),pointer  :: volume_error_lower(:) => null()
  integer(ids_int) :: volume_error_index=ids_int_invalid

  real(ids_real),pointer  :: area(:) => null()     ! /area - Cross-sectional area of the flux surface
  real(ids_real),pointer  :: area_error_upper(:) => null()
  real(ids_real),pointer  :: area_error_lower(:) => null()
  integer(ids_int) :: area_error_index=ids_int_invalid

endtype

type ids_distributions_d_profiles_2d  !    2D profiles from the distribution, for a given time slice
  type (ids_distributions_d_profiles_2d_grid) :: grid  ! /grid - Grid. The grid has to be rectangular in a pair of coordinates, as specified in type
  real(ids_real),pointer  :: density(:,:) => null()     ! /density - Density (thermal+non-thermal)
  real(ids_real),pointer  :: density_error_upper(:,:) => null()
  real(ids_real),pointer  :: density_error_lower(:,:) => null()
  integer(ids_int) :: density_error_index=ids_int_invalid

  real(ids_real),pointer  :: density_fast(:,:) => null()     ! /density_fast - Density of fast particles
  real(ids_real),pointer  :: density_fast_error_upper(:,:) => null()
  real(ids_real),pointer  :: density_fast_error_lower(:,:) => null()
  integer(ids_int) :: density_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure(:,:) => null()     ! /pressure - Pressure (thermal+non-thermal)
  real(ids_real),pointer  :: pressure_error_upper(:,:) => null()
  real(ids_real),pointer  :: pressure_error_lower(:,:) => null()
  integer(ids_int) :: pressure_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_fast(:,:) => null()     ! /pressure_fast - Pressure of fast particles
  real(ids_real),pointer  :: pressure_fast_error_upper(:,:) => null()
  real(ids_real),pointer  :: pressure_fast_error_lower(:,:) => null()
  integer(ids_int) :: pressure_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_fast_parallel(:,:) => null()     ! /pressure_fast_parallel - Pressure of fast particles in the parallel direction
  real(ids_real),pointer  :: pressure_fast_parallel_error_upper(:,:) => null()
  real(ids_real),pointer  :: pressure_fast_parallel_error_lower(:,:) => null()
  integer(ids_int) :: pressure_fast_parallel_error_index=ids_int_invalid

  real(ids_real),pointer  :: current_tor(:,:) => null()     ! /current_tor - Total toroidal driven current density (including electron and thermal ion back-current, or drag-curr
  real(ids_real),pointer  :: current_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: current_tor_error_lower(:,:) => null()
  integer(ids_int) :: current_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: current_fast_tor(:,:) => null()     ! /current_fast_tor - Total toroidal driven current density of fast (non-thermal) particles (excluding electron and therma
  real(ids_real),pointer  :: current_fast_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: current_fast_tor_error_lower(:,:) => null()
  integer(ids_int) :: current_fast_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: torque_tor_j_radial(:,:) => null()     ! /torque_tor_j_radial - Toroidal torque due to radial currents
  real(ids_real),pointer  :: torque_tor_j_radial_error_upper(:,:) => null()
  real(ids_real),pointer  :: torque_tor_j_radial_error_lower(:,:) => null()
  integer(ids_int) :: torque_tor_j_radial_error_index=ids_int_invalid

  type (ids_distributions_d_profiles_2d_collisions) :: collisions  ! /collisions - Power and torque exchanged between the species described by the distribution and the different plasm
  type (ids_distributions_d_profiles_2d_partial) :: trapped  ! /trapped - Flux surface averaged profile evaluated using the trapped particle part of the distribution.
  type (ids_distributions_d_profiles_2d_partial) :: co_passing  ! /co_passing - Flux surface averaged profile evaluated using the co-passing particle part of the distribution.
  type (ids_distributions_d_profiles_2d_partial) :: counter_passing  ! /counter_passing - Flux surface averaged profile evaluated using the counter-passing particle part of the distribution.
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_distributions_d_fast_filter  !    Description of how the fast and the thermal particle populations are separated
  type (ids_identifier) :: method  ! /method - Method used to separate the fast and thermal particle population (indices TBD)
  real(ids_real),pointer  :: energy(:) => null()     ! /energy - Energy at which the fast and thermal particle populations were separated, as a function of radius
  real(ids_real),pointer  :: energy_error_upper(:) => null()
  real(ids_real),pointer  :: energy_error_lower(:) => null()
  integer(ids_int) :: energy_error_index=ids_int_invalid

endtype

type ids_distributions_d  !    Description of a given distribution function
  type (ids_waves_coherent_wave_identifier),pointer :: wave(:) => null()  ! /wave(i) - List all waves affecting the distribution, identified as in waves/coherent_wave(i)/identifier in the
  type (ids_distribution_process_identifier),pointer :: process(:) => null()  ! /process(i) - List all processes (NBI units, fusion reactions, ...) affecting the distribution, identified as in d
  integer(ids_int)  :: gyro_type=ids_int_invalid       ! /gyro_type - Defines how to interpret the spatial coordinates: 1 = given at the actual particle birth point; 2 =g
  type (ids_distribution_species) :: species  ! /species - Species described by this distribution
  type (ids_distributions_d_fast_filter) :: fast_filter  ! /fast_filter - Description of how the fast and the thermal particle populations are separated
  type (ids_distributions_d_global_quantities),pointer :: global_quantities(:) => null()  ! /global_quantities(i) - Global quantities (integrated over plasma volume for moments of the distribution, collisional exchan
  type (ids_distributions_d_profiles_1d),pointer :: profiles_1d(:) => null()  ! /profiles_1d(i) - Radial profiles (flux surface averaged quantities) for various time slices
  type (ids_distributions_d_profiles_2d),pointer :: profiles_2d(:) => null()  ! /profiles_2d(i) - 2D profiles in the poloidal plane for various time slices
  integer(ids_int)  :: is_delta_f=ids_int_invalid       ! /is_delta_f - If is_delta_f=1, then the distribution represents the deviation from a Maxwellian; is_delta_f=0, the
  type (ids_distributions_d_ggd),pointer :: ggd(:) => null()  ! /ggd(i) - Distribution represented using the ggd, for various time slices
  type (ids_distribution_markers),pointer :: markers(:) => null()  ! /markers(i) - Distribution represented by a set of markers (test particles)
endtype

type ids_distributions  !    Distribution function(s) of one or many particle species. This structure is specifically designed to handle non-Maxwellian distrib
  type (ids_ids_properties) :: ids_properties  ! /distributions/ids_properties -
  type (ids_distributions_d),pointer :: distribution(:) => null()  ! /distributions/distribution(i) - Set of distribution functions. Every distribution function has to be associated with only one partic
  type (ids_b_tor_vacuum_1) :: vacuum_toroidal_field  ! /distributions/vacuum_toroidal_field - Characteristics of the vacuum toroidal field (used in rho_tor definition and in the normalization of
  type (ids_rz1d_dynamic_1) :: magnetic_axis  ! /distributions/magnetic_axis - Magnetic axis position (used to define a poloidal angle for the 2D profiles)
  type (ids_code) :: code  ! /distributions/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include ec_antennas/dd_ec_antennas.xsd
! SPECIAL STRUCTURE data / time
type ids_ec_antennas_beam_spot_size  !    Size of the spot ellipse
  real(ids_real), pointer  :: data(:,:) => null()     ! /size - Size of the spot ellipse
  real(ids_real), pointer  :: data_error_upper(:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_ec_antennas_beam_spot_angle  !    Rotation angle for the spot ellipse
  real(ids_real), pointer  :: data(:) => null()     ! /angle - Rotation angle for the spot ellipse
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_ec_antennas_beam_spot  !    Spot ellipse characteristics
  type (ids_ec_antennas_beam_spot_size) :: size  ! /size - Size of the spot ellipse
  type (ids_ec_antennas_beam_spot_angle) :: angle  ! /angle - Rotation angle for the spot ellipse
endtype

! SPECIAL STRUCTURE data / time
type ids_ec_antennas_beam_phase_curvature  !    Inverse curvature radii for the phase ellipse, positive/negative for divergent/convergent beams
  real(ids_real), pointer  :: data(:,:) => null()     ! /curvature - Inverse curvature radii for the phase ellipse, positive/negative for divergent/convergent beams
  real(ids_real), pointer  :: data_error_upper(:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_ec_antennas_beam_phase_angle  !    Rotation angle for the phase ellipse
  real(ids_real), pointer  :: data(:) => null()     ! /angle - Rotation angle for the phase ellipse
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_ec_antennas_beam_phase  !    Phase ellipse characteristics
  type (ids_ec_antennas_beam_phase_curvature) :: curvature  ! /curvature - Inverse curvature radii for the phase ellipse, positive/negative for divergent/convergent beams
  type (ids_ec_antennas_beam_phase_angle) :: angle  ! /angle - Rotation angle for the phase ellipse
endtype

type ids_ec_antennas_beam  !    Beam characteristics
  type (ids_ec_antennas_beam_spot) :: spot  ! /spot - Spot ellipse characteristics
  type (ids_ec_antennas_beam_phase) :: phase  ! /phase - Phase ellipse characteristics
endtype

! SPECIAL STRUCTURE data / time
type ids_ec_antennas_antenna_power  !    Power
  real(ids_real), pointer  :: data(:) => null()     ! /power - Power
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_ec_antennas_antenna_mode  !    Incoming wave mode (+ or -1 for O/X mode)
  integer(ids_int), pointer  :: data(:) => null()      ! /mode - Incoming wave mode (+ or -1 for O/X mode)
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_ec_antennas_antenna_launching_angle_pol  !    Poloidal launching angle between the horizontal plane and the poloidal component of the nominal beam centerline. tan(angle_pol)=-k
  real(ids_real), pointer  :: data(:) => null()     ! /launching_angle_pol - Poloidal launching angle between the horizontal plane and the poloidal component of the nominal beam
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_ec_antennas_antenna_launching_angle_tor  !    Toroidal launching angle between the poloidal plane and the nominal beam centerline. sin(angle_tor)=k_phi
  real(ids_real), pointer  :: data(:) => null()     ! /launching_angle_tor - Toroidal launching angle between the poloidal plane and the nominal beam centerline. sin(angle_tor)=
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_ec_antennas_antenna  !    Electron Cyclotron Antenna
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the antenna (unique within the set of all antennas of the experiment)
  character(len=ids_string_length), dimension(:), pointer ::identifier => null()       ! /identifier - Identifier of the antenna (unique within the set of all antennas of the experiment)
  real(ids_real)  :: frequency=ids_real_invalid       ! /frequency - Frequency
  real(ids_real)  :: frequency_error_upper=ids_real_invalid
  real(ids_real)  :: frequency_error_lower=ids_real_invalid
  integer(ids_int) :: frequency_error_index=ids_int_invalid

  type (ids_ec_antennas_antenna_power) :: power  ! /power - Power
  type (ids_ec_antennas_antenna_mode) :: mode  ! /mode - Incoming wave mode (+ or -1 for O/X mode)
  type (ids_rzphi1d_dynamic_aos1) :: launching_position  ! /launching_position - Launching position of the beam
  type (ids_ec_antennas_antenna_launching_angle_pol) :: launching_angle_pol  ! /launching_angle_pol - Poloidal launching angle between the horizontal plane and the poloidal component of the nominal beam
  type (ids_ec_antennas_antenna_launching_angle_tor) :: launching_angle_tor  ! /launching_angle_tor - Toroidal launching angle between the poloidal plane and the nominal beam centerline. sin(angle_tor)=
  type (ids_ec_antennas_beam) :: beam  ! /beam - Beam characteristics
endtype

type ids_ec_antennas  !    Antenna systems for heating and current drive in the electron cyclotron (EC) frequencies.
  type (ids_ids_properties) :: ids_properties  ! /ec_antennas/ids_properties -
  type (ids_ec_antennas_antenna),pointer :: antenna(:) => null()  ! /ec_antennas/antenna(i) - Set of Electron Cyclotron antennas
  type (ids_code) :: code  ! /ec_antennas/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include ece/dd_ece.xsd
! SPECIAL STRUCTURE data / time
type ids_ece_channel_frequency  !    Frequency of the channel
  real(ids_real), pointer  :: data(:) => null()     ! /frequency - Frequency of the channel
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_ece_channel_harmonic  !    Harmonic detected by the channel. 1 corresponds to the "O1" mode, while 2 corresponds to the "X2" mode.
  integer(ids_int), pointer  :: data(:) => null()      ! /harmonic - Harmonic detected by the channel. 1 corresponds to the "O1" mode, while 2 corresponds to the "X2" mo
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_ece_channel_t_e  !    Electron temperature
  real(ids_real), pointer  :: data(:) => null()     ! /t_e - Electron temperature
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_ece_channel_t_e_voltage  !    Raw voltage measured on each channel, from which the calibrated temperature data is then derived
  real(ids_real), pointer  :: data(:) => null()     ! /t_e_voltage - Raw voltage measured on each channel, from which the calibrated temperature data is then derived
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_ece_channel  !    Charge exchange channel
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the channel
  character(len=ids_string_length), dimension(:), pointer ::identifier => null()       ! /identifier - ID of the channel
  type (ids_ece_channel_frequency) :: frequency  ! /frequency - Frequency of the channel
  type (ids_ece_channel_harmonic) :: harmonic  ! /harmonic - Harmonic detected by the channel. 1 corresponds to the "O1" mode, while 2 corresponds to the "X2" mo
  type (ids_rzphi1d_dynamic_aos1) :: position  ! /position - Position of the measurements
  type (ids_ece_channel_t_e) :: t_e  ! /t_e - Electron temperature
  type (ids_ece_channel_t_e_voltage) :: t_e_voltage  ! /t_e_voltage - Raw voltage measured on each channel, from which the calibrated temperature data is then derived
endtype

type ids_ece  !    Electron cyclotron emission diagnostic
  type (ids_ids_properties) :: ids_properties  ! /ece/ids_properties -
  type (ids_line_of_sight_2points) :: line_of_sight  ! /ece/line_of_sight - Description of the line of sight of the diagnostic (valid for all channels), defined by two points
  type (ids_ece_channel),pointer :: channel(:) => null()  ! /ece/channel(i) - Set of channels (frequency)
  type (ids_code) :: code  ! /ece/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include edge_profiles/dd_edge_profiles.xsd
type ids_edge_profiles_time_slice_neutral_state  !    Quantities related to a given state of the neutral species
  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying state
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  type (ids_identifier) :: neutral_type  ! /neutral_type - Neutral type, in terms of energy. ID =1: cold; 2: thermal; 3: fast; 4: NBI
  type (ids_generic_grid_scalar),pointer :: temperature(:) => null()  ! /temperature(i) - Temperature, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: density(:) => null()  ! /density(i) - Density (thermal+non-thermal), given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: density_fast(:) => null()  ! /density_fast(i) - Density of fast (non-thermal) particles, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure(:) => null()  ! /pressure(i) - Pressure, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure_fast_perpendicular(:) => null()  ! /pressure_fast_perpendicular(i) - Fast (non-thermal) perpendicular pressure, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure_fast_parallel(:) => null()  ! /pressure_fast_parallel(i) - Fast (non-thermal) parallel pressure, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: velocity(:) => null()  ! /velocity(i) - Velocity, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: velocity_diamagnetic(:) => null()  ! /velocity_diamagnetic(i) - Velocity due to the diamagnetic drift, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: velocity_exb(:) => null()  ! /velocity_exb(i) - Velocity due to the ExB drift, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: energy_density_kinetic(:) => null()  ! /energy_density_kinetic(i) - Kinetic energy density, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: distribution_function(:) => null()  ! /distribution_function(i) - Distribution function, given on various grid subsets
endtype

type ids_edge_profiles_time_slice_ion_charge_state  !    Quantities related to a given charge state of the ion species
  real(ids_real)  :: z_min=ids_real_invalid       ! /z_min - Minimum Z of the charge state bundle (z_min = z_max = 0 for a neutral)
  real(ids_real)  :: z_min_error_upper=ids_real_invalid
  real(ids_real)  :: z_min_error_lower=ids_real_invalid
  integer(ids_int) :: z_min_error_index=ids_int_invalid

  real(ids_real)  :: z_max=ids_real_invalid       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(ids_real)  :: z_max_error_upper=ids_real_invalid
  real(ids_real)  :: z_max_error_lower=ids_real_invalid
  integer(ids_int) :: z_max_error_index=ids_int_invalid

  type (ids_generic_grid_scalar),pointer :: z_average(:) => null()  ! /z_average(i) - Average Z of the charge state bundle (equal to z_min if no bundle), = sum (Z*x_z) where x_z is the r
  type (ids_generic_grid_scalar),pointer :: z_square_average(:) => null()  ! /z_square_average(i) - Average Z square of the charge state bundle (equal to z_min if no bundle), = sum (Z^2*x_z) where x_z
  type (ids_generic_grid_scalar),pointer :: ionisation_potential(:) => null()  ! /ionisation_potential(i) - Cumulative and average ionisation potential to reach a given bundle. Defined as sum (x_z* (sum of Ep
  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  type (ids_generic_grid_scalar),pointer :: temperature(:) => null()  ! /temperature(i) - Temperature, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: density(:) => null()  ! /density(i) - Density (thermal+non-thermal), given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: density_fast(:) => null()  ! /density_fast(i) - Density of fast (non-thermal) particles, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure(:) => null()  ! /pressure(i) - Pressure, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure_fast_perpendicular(:) => null()  ! /pressure_fast_perpendicular(i) - Fast (non-thermal) perpendicular pressure, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure_fast_parallel(:) => null()  ! /pressure_fast_parallel(i) - Fast (non-thermal) parallel pressure, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: velocity(:) => null()  ! /velocity(i) - Velocity, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: velocity_diamagnetic(:) => null()  ! /velocity_diamagnetic(i) - Velocity due to the diamagnetic drift, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: velocity_exb(:) => null()  ! /velocity_exb(i) - Velocity due to the ExB drift, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: energy_density_kinetic(:) => null()  ! /energy_density_kinetic(i) - Kinetic energy density, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: distribution_function(:) => null()  ! /distribution_function(i) - Distribution function, given on various grid subsets
endtype

type ids_edge_profiles_time_slice_neutral  !    Quantities related to a given neutral species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H, D, T, He, C, D2, DT, CD4, ...)
  integer(ids_int)  :: ion_index=ids_int_invalid       ! /ion_index - Index of the corresponding ion species in the ../../ion array
  type (ids_generic_grid_scalar),pointer :: temperature(:) => null()  ! /temperature(i) - Temperature (average over states when multiple states are considered), given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: density(:) => null()  ! /density(i) - Density (thermal+non-thermal) (sum over states when multiple states are considered), given on variou
  type (ids_generic_grid_scalar),pointer :: density_fast(:) => null()  ! /density_fast(i) - Density of fast (non-thermal) particles (sum over states when multiple states are considered), given
  type (ids_generic_grid_scalar),pointer :: pressure(:) => null()  ! /pressure(i) - Pressure (average over states when multiple states are considered), given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure_fast_perpendicular(:) => null()  ! /pressure_fast_perpendicular(i) - Fast (non-thermal) perpendicular pressure (average over states when multiple states are considered),
  type (ids_generic_grid_scalar),pointer :: pressure_fast_parallel(:) => null()  ! /pressure_fast_parallel(i) - Fast (non-thermal) parallel pressure (average over states when multiple states are considered), give
  type (ids_generic_grid_vector_components),pointer :: velocity(:) => null()  ! /velocity(i) - Velocity (average over states when multiple states are considered), given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: energy_density_kinetic(:) => null()  ! /energy_density_kinetic(i) - Kinetic energy density (sum over states when multiple states are considered), given on various grid
  integer(ids_int)  :: multiple_states_flag=ids_int_invalid       ! /multiple_states_flag - Multiple state calculation flag : 0-Only one state is considered; 1-Multiple states are considered a
  type (ids_edge_profiles_time_slice_neutral_state),pointer :: state(:) => null()  ! /state(i) - Quantities related to the different states of the species (energy, excitation, ...)
endtype

type ids_edge_profiles_time_slice_ion  !    Quantities related to a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(ids_real)  :: z_ion=ids_real_invalid       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed).
  real(ids_real)  :: z_ion_error_upper=ids_real_invalid
  real(ids_real)  :: z_ion_error_lower=ids_real_invalid
  integer(ids_int) :: z_ion_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H+, D+, T+, He+2, C+, D2, DT, CD4, ...)
  integer(ids_int)  :: neutral_index=ids_int_invalid       ! /neutral_index - Index of the corresponding neutral species in the ../../neutral array
  type (ids_generic_grid_scalar),pointer :: temperature(:) => null()  ! /temperature(i) - Temperature (average over states when multiple states are considered), given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: density(:) => null()  ! /density(i) - Density (thermal+non-thermal) (sum over states when multiple states are considered), given on variou
  type (ids_generic_grid_scalar),pointer :: density_fast(:) => null()  ! /density_fast(i) - Density of fast (non-thermal) particles (sum over states when multiple states are considered), given
  type (ids_generic_grid_scalar),pointer :: pressure(:) => null()  ! /pressure(i) - Pressure (average over states when multiple states are considered), given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure_fast_perpendicular(:) => null()  ! /pressure_fast_perpendicular(i) - Fast (non-thermal) perpendicular pressure (average over states when multiple states are considered),
  type (ids_generic_grid_scalar),pointer :: pressure_fast_parallel(:) => null()  ! /pressure_fast_parallel(i) - Fast (non-thermal) parallel pressure (average over states when multiple states are considered), give
  type (ids_generic_grid_vector_components),pointer :: velocity(:) => null()  ! /velocity(i) - Velocity (average over states when multiple states are considered), given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: energy_density_kinetic(:) => null()  ! /energy_density_kinetic(i) - Kinetic energy density (sum over states when multiple states are considered), given on various grid
  integer(ids_int)  :: multiple_states_flag=ids_int_invalid       ! /multiple_states_flag - Multiple state calculation flag : 0-Only one state is considered; 1-Multiple states are considered a
  type (ids_edge_profiles_time_slice_ion_charge_state),pointer :: state(:) => null()  ! /state(i) - Quantities related to the different states of the species (ionisation, energy, excitation, ...)
endtype

type ids_edge_profiles_ggd_fast_ion  !    Fast sampled quantities related to a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(ids_real)  :: z_ion=ids_real_invalid       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed).
  real(ids_real)  :: z_ion_error_upper=ids_real_invalid
  real(ids_real)  :: z_ion_error_lower=ids_real_invalid
  integer(ids_int) :: z_ion_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H+, D+, T+, He+2, C+, D2, DT, CD4, ...)
  integer(ids_int)  :: neutral_index=ids_int_invalid       ! /neutral_index - Index of the corresponding neutral species in the ../../neutral array
  integer(ids_int)  :: content=ids_int_invalid       ! /content - Total number of particles for this ion species in the computational volume
  type (ids_generic_grid_scalar_single_position),pointer :: temperature(:) => null()  ! /temperature(i) - Temperature (average over states when multiple states are considered), given at various positions (g
  type (ids_generic_grid_scalar_single_position),pointer :: density(:) => null()  ! /density(i) - Density (thermal+non-thermal) (sum over states when multiple states are considered), given at variou
endtype

type ids_edge_profiles_time_slice_electrons  !    Quantities related to electrons
  type (ids_generic_grid_scalar),pointer :: temperature(:) => null()  ! /temperature(i) - Temperature, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: density(:) => null()  ! /density(i) - Density (thermal+non-thermal), given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: density_fast(:) => null()  ! /density_fast(i) - Density of fast (non-thermal) particles, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure(:) => null()  ! /pressure(i) - Pressure, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure_fast_perpendicular(:) => null()  ! /pressure_fast_perpendicular(i) - Fast (non-thermal) perpendicular pressure, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure_fast_parallel(:) => null()  ! /pressure_fast_parallel(i) - Fast (non-thermal) parallel pressure, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: velocity(:) => null()  ! /velocity(i) - Velocity, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: distribution_function(:) => null()  ! /distribution_function(i) - Distribution function, given on various grid subsets
endtype

type ids_edge_profiles_ggd_fast_electrons  !    Fast sampled quantities related to electrons
  type (ids_generic_grid_scalar_single_position),pointer :: temperature(:) => null()  ! /temperature(i) - Temperature, given at various positions (grid subset of size 1)
  type (ids_generic_grid_scalar_single_position),pointer :: density(:) => null()  ! /density(i) - Density (thermal+non-thermal), given at various positions (grid subset of size 1)
endtype

type ids_edge_profiles_time_slice  !    edge plasma description for a given time slice
  type (ids_edge_profiles_time_slice_electrons) :: electrons  ! /electrons - Quantities related to the electrons
  type (ids_edge_profiles_time_slice_ion),pointer :: ion(:) => null()  ! /ion(i) - Quantities related to the different ion species
  type (ids_edge_profiles_time_slice_neutral),pointer :: neutral(:) => null()  ! /neutral(i) - Quantities related to the different neutral species
  type (ids_generic_grid_scalar),pointer :: t_i_average(:) => null()  ! /t_i_average(i) - Ion temperature (averaged on ion species), given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: n_i_total_over_n_e(:) => null()  ! /n_i_total_over_n_e(i) - Ratio of total ion density (sum over ion species) over electron density. (thermal+non-thermal), give
  type (ids_generic_grid_scalar),pointer :: zeff(:) => null()  ! /zeff(i) - Effective charge, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure_thermal(:) => null()  ! /pressure_thermal(i) - Thermal pressure (electrons+ions), given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure_perpendicular(:) => null()  ! /pressure_perpendicular(i) - Total perpendicular pressure (electrons+ions, thermal+non-thermal), given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure_parallel(:) => null()  ! /pressure_parallel(i) - Total parallel pressure (electrons+ions, thermal+non-thermal), given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: j_anomalous(:) => null()  ! /j_anomalous(i) - Anomalous current density, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: j_inertial(:) => null()  ! /j_inertial(i) - Inertial current density, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: j_ion_neutral_friction(:) => null()  ! /j_ion_neutral_friction(i) - Current density due to ion neutral friction, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: j_parallel_viscosity(:) => null()  ! /j_parallel_viscosity(i) - Current density due to the parallel viscosity, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: j_perpendicular_viscosity(:) => null()  ! /j_perpendicular_viscosity(i) - Current density due to the perpendicular viscosity, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: j_heat_viscosity(:) => null()  ! /j_heat_viscosity(i) - Current density due to the heat viscosity, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: j_pfirsch_schlueter(:) => null()  ! /j_pfirsch_schlueter(i) - Current density due to Pfirsch-Schlüter effects, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: j_diamagnetic(:) => null()  ! /j_diamagnetic(i) - Current density due to the diamgnetic drift, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: e_field(:) => null()  ! /e_field(i) - Electric field, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: phi_potential(:) => null()  ! /phi_potential(i) - Electric potential, given on various grid subsets
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_edge_profiles_ggd_fast  !    Quantities provided at a faster sampling rate than the full ggd quantities, on a reduced set of positions. Positions are described
  type (ids_edge_profiles_ggd_fast_electrons) :: electrons  ! /electrons - Quantities related to the electrons
  type (ids_edge_profiles_ggd_fast_ion),pointer :: ion(:) => null()  ! /ion(i) - Quantities related to the different ion species
  type (ids_generic_grid_scalar_single_position),pointer :: energy_thermal(:) => null()  ! /energy_thermal(i) - Plasma energy content = 3/2 * integral over the volume of the grid subset of the thermal pressure (s
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_edge_profiles_global_quantities  !    Various 0D quantities calculated from the fields solved in the transport equations, may be stored at faster frequency than the ful
  real(ids_real),pointer  :: energy_thermal(:) => null()     ! /energy_thermal - Plasma energy content = 3/2 * integral over the computational volume of the thermal pressure (summed
  real(ids_real),pointer  :: energy_thermal_error_upper(:) => null()
  real(ids_real),pointer  :: energy_thermal_error_lower(:) => null()
  integer(ids_int) :: energy_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_separatrix(:) => null()     ! /power_separatrix - Power flowing accross the separatrix
  real(ids_real),pointer  :: power_separatrix_error_upper(:) => null()
  real(ids_real),pointer  :: power_separatrix_error_lower(:) => null()
  integer(ids_int) :: power_separatrix_error_index=ids_int_invalid

endtype

type ids_edge_profiles  !    Edge plasma profiles (includes the scrape-off layer and possibly part of the confined plasma)
  type (ids_ids_properties) :: ids_properties  ! /edge_profiles/ids_properties -
  type (ids_core_profiles_profiles_1d),pointer :: profiles_1d(:) => null()  ! /edge_profiles/profiles_1d(i) - SOL radial profiles for various time slices, taken on outboard equatorial mid-plane
  type (ids_generic_grid_aos3_root),pointer :: grid_ggd(:) => null()  ! /edge_profiles/grid_ggd(i) - Grid (using the Generic Grid Description), for various time slices. The timebase of this array of st
  type (ids_edge_profiles_time_slice),pointer :: ggd(:) => null()  ! /edge_profiles/ggd(i) - Edge plasma quantities represented using the general grid description, for various time slices. The
  type (ids_edge_profiles_ggd_fast),pointer :: ggd_fast(:) => null()  ! /edge_profiles/ggd_fast(i) - Quantities provided at a faster sampling rate than the full ggd quantities. These are either integra
  type (ids_code) :: code  ! /edge_profiles/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include edge_sources/dd_edge_sources.xsd
type ids_edge_sources_source_ggd_fast_ion  !    Integrated source terms related to a given ion species (fast sampled data)
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(ids_real)  :: z_ion=ids_real_invalid       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed)
  real(ids_real)  :: z_ion_error_upper=ids_real_invalid
  real(ids_real)  :: z_ion_error_lower=ids_real_invalid
  integer(ids_int) :: z_ion_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  integer(ids_int)  :: neutral_index=ids_int_invalid       ! /neutral_index - Index of the corresponding neutral species in the ../../neutral array
  type (ids_generic_grid_scalar_single_position),pointer :: power(:) => null()  ! /power(i) - Total power source or sink related to this ion species, integrated over the volume of the grid subse
endtype

type ids_edge_sources_source_ggd_fast  !    Integrated source terms given on the ggd at a given time slice (fast sampled data)
  type (ids_edge_sources_source_ggd_fast_ion),pointer :: ion(:) => null()  ! /ion(i) - Source term integrals related to the various ion species
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_edge_sources_source_ggd_neutral_state  !    Source terms related to the a given state of the neutral species
  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying state
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  type (ids_identifier) :: neutral_type  ! /neutral_type - Neutral type, in terms of energy. ID =1: cold; 2: thermal; 3: fast; 4: NBI
  type (ids_generic_grid_scalar),pointer :: particles(:) => null()  ! /particles(i) - Source term for the charge state density transport equation
  type (ids_generic_grid_scalar),pointer :: energy(:) => null()  ! /energy(i) - Source terms for the charge state energy transport equation
  type (ids_generic_grid_vector_components),pointer :: momentum(:) => null()  ! /momentum(i) - Source term for momentum equations, on various grid subsets
endtype

type ids_edge_sources_source_ggd_ion_state  !    Source terms related to the a given state of the ion species
  real(ids_real)  :: z_min=ids_real_invalid       ! /z_min - Minimum Z of the charge state bundle
  real(ids_real)  :: z_min_error_upper=ids_real_invalid
  real(ids_real)  :: z_min_error_lower=ids_real_invalid
  integer(ids_int) :: z_min_error_index=ids_int_invalid

  real(ids_real)  :: z_max=ids_real_invalid       ! /z_max - Maximum Z of the charge state bundle
  real(ids_real)  :: z_max_error_upper=ids_real_invalid
  real(ids_real)  :: z_max_error_lower=ids_real_invalid
  integer(ids_int) :: z_max_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  type (ids_generic_grid_scalar),pointer :: particles(:) => null()  ! /particles(i) - Source term for the charge state density transport equation
  type (ids_generic_grid_scalar),pointer :: energy(:) => null()  ! /energy(i) - Source terms for the charge state energy transport equation
  type (ids_generic_grid_vector_components),pointer :: momentum(:) => null()  ! /momentum(i) - Source term for momentum equations, on various grid subsets
endtype

type ids_edge_sources_source_ggd_ion  !    Source terms related to a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(ids_real)  :: z_ion=ids_real_invalid       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed)
  real(ids_real)  :: z_ion_error_upper=ids_real_invalid
  real(ids_real)  :: z_ion_error_lower=ids_real_invalid
  integer(ids_int) :: z_ion_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  integer(ids_int)  :: neutral_index=ids_int_invalid       ! /neutral_index - Index of the corresponding neutral species in the ../../neutral array
  type (ids_generic_grid_scalar),pointer :: particles(:) => null()  ! /particles(i) - Source term for ion density equation, on various grid subsets
  type (ids_generic_grid_scalar),pointer :: energy(:) => null()  ! /energy(i) - Source term for the ion energy transport equation, on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: momentum(:) => null()  ! /momentum(i) - Source term for momentum equations (sum over states when multiple states are considered), on various
  integer(ids_int)  :: multiple_states_flag=ids_int_invalid       ! /multiple_states_flag - Multiple states calculation flag : 0-Only one state is considered; 1-Multiple states are considered
  type (ids_edge_sources_source_ggd_ion_state),pointer :: state(:) => null()  ! /state(i) - Source terms related to the different charge states of the species (ionisation, energy, excitation,
endtype

type ids_edge_sources_source_ggd_neutral  !    Source terms related to a given neutral species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying neutral (e.g. H, D, T, He, C, ...)
  integer(ids_int)  :: ion_index=ids_int_invalid       ! /ion_index - Index of the corresponding ion species in the ../../ion array
  type (ids_generic_grid_scalar),pointer :: particles(:) => null()  ! /particles(i) - Source term for ion density equation, on various grid subsets
  type (ids_generic_grid_scalar),pointer :: energy(:) => null()  ! /energy(i) - Source term for the ion energy transport equation, on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: momentum(:) => null()  ! /momentum(i) - Source term for momentum equations (sum over states when multiple states are considered), on various
  integer(ids_int)  :: multiple_states_flag=ids_int_invalid       ! /multiple_states_flag - Multiple states calculation flag : 0-Only one state is considered; 1-Multiple states are considered
  type (ids_edge_sources_source_ggd_neutral_state),pointer :: state(:) => null()  ! /state(i) - Source terms related to the different charge states of the species (energy, excitation, ...)
endtype

type ids_edge_sources_source_ggd_electrons  !    Source terms related to electrons
  type (ids_generic_grid_scalar),pointer :: particles(:) => null()  ! /particles(i) - Source term for electron density equation, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: energy(:) => null()  ! /energy(i) - Source term for the electron energy equation, given on various grid subsets
endtype

type ids_edge_sources_source_ggd  !    Source terms for a given time slice
  type (ids_edge_sources_source_ggd_electrons) :: electrons  ! /electrons - Sources for electrons
  type (ids_edge_sources_source_ggd_ion),pointer :: ion(:) => null()  ! /ion(i) - Source terms related to the different ion species
  type (ids_edge_sources_source_ggd_neutral),pointer :: neutral(:) => null()  ! /neutral(i) - Source terms related to the different neutral species
  type (ids_generic_grid_scalar),pointer :: total_ion_energy(:) => null()  ! /total_ion_energy(i) - Source term for the total (summed over ion  species) energy equation, on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: momentum(:) => null()  ! /momentum(i) - Source term for total momentum equations, on various grid subsets
  type (ids_generic_grid_scalar),pointer :: current(:) => null()  ! /current(i) - Current density source
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_edge_sources_source  !    Source terms for a given actuator
  type (ids_identifier) :: identifier  ! /identifier - Source term identifier
  type (ids_edge_sources_source_ggd),pointer :: ggd(:) => null()  ! /ggd(i) - Source terms represented using the general grid description, for various time slices
  type (ids_edge_sources_source_ggd_fast),pointer :: ggd_fast(:) => null()  ! /ggd_fast(i) - Quantities provided at a faster sampling rate than the full ggd quantities. These are either integra
endtype

type ids_edge_sources  !    Edge plasma sources. Energy terms correspond to the full kinetic energy equation (i.e. the energy flux takes into account the ener
  type (ids_ids_properties) :: ids_properties  ! /edge_sources/ids_properties -
  type (ids_generic_grid_aos3_root),pointer :: grid_ggd(:) => null()  ! /edge_sources/grid_ggd(i) - Grid (using the Generic Grid Description), for various time slices. The timebase of this array of st
  type (ids_edge_sources_source),pointer :: source(:) => null()  ! /edge_sources/source(i) - Set of source terms
  type (ids_code) :: code  ! /edge_sources/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include edge_transport/dd_edge_transport.xsd
type ids_edge_transport_model_energy  !    Transport coefficients for energy equations.
  type (ids_generic_grid_scalar),pointer :: d(:) => null()  ! /d(i) - Effective diffusivity, on various grid subsets
  type (ids_generic_grid_scalar),pointer :: v(:) => null()  ! /v(i) - Effective convection, on various grid subsets
  type (ids_generic_grid_scalar),pointer :: flux(:) => null()  ! /flux(i) - Flux, on various grid subsets
  type (ids_generic_grid_scalar),pointer :: flux_limiter(:) => null()  ! /flux_limiter(i) - Flux limiter coefficient, on various grid subsets
endtype

type ids_edge_transport_model_momentum  !    Transport coefficients for momentum equations.
  type (ids_generic_grid_vector_components),pointer :: d(:) => null()  ! /d(i) - Effective diffusivity, on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: v(:) => null()  ! /v(i) - Effective convection, on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: flux(:) => null()  ! /flux(i) - Flux, on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: flux_limiter(:) => null()  ! /flux_limiter(i) - Flux limiter coefficient, on various grid subsets
endtype

type ids_edge_transport_model_density  !    Transport coefficients for energy equations.
  type (ids_generic_grid_scalar),pointer :: d(:) => null()  ! /d(i) - Effective diffusivity, on various grid subsets
  type (ids_generic_grid_scalar),pointer :: v(:) => null()  ! /v(i) - Effective convection, on various grid subsets
  type (ids_generic_grid_scalar),pointer :: flux(:) => null()  ! /flux(i) - Flux, on various grid subsets
  type (ids_generic_grid_scalar),pointer :: flux_limiter(:) => null()  ! /flux_limiter(i) - Flux limiter coefficient, on various grid subsets
endtype

type ids_edge_transport_model_neutral_state  !    Transport coefficients related to a given state of the neutral species
  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying state
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  type (ids_identifier) :: neutral_type  ! /neutral_type - Neutral type, in terms of energy. ID =1: cold; 2: thermal; 3: fast; 4: NBI
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  type (ids_edge_transport_model_density) :: particles  ! /particles - Transport quantities related to density equation of the charge state considered (thermal+non-thermal
  type (ids_edge_transport_model_energy) :: energy  ! /energy - Transport quantities related to the energy equation of the charge state considered
  type (ids_edge_transport_model_momentum) :: momentum  ! /momentum - Transport coefficients related to the momentum equations of the charge state considered
endtype

type ids_edge_transport_model_ion_state  !    Transport coefficients related to a given state of the ion species
  real(ids_real)  :: z_min=ids_real_invalid       ! /z_min - Minimum Z of the charge state bundle
  real(ids_real)  :: z_min_error_upper=ids_real_invalid
  real(ids_real)  :: z_min_error_lower=ids_real_invalid
  integer(ids_int) :: z_min_error_index=ids_int_invalid

  real(ids_real)  :: z_max=ids_real_invalid       ! /z_max - Maximum Z of the charge state bundle
  real(ids_real)  :: z_max_error_upper=ids_real_invalid
  real(ids_real)  :: z_max_error_lower=ids_real_invalid
  integer(ids_int) :: z_max_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  type (ids_edge_transport_model_density) :: particles  ! /particles - Transport quantities related to density equation of the charge state considered (thermal+non-thermal
  type (ids_edge_transport_model_energy) :: energy  ! /energy - Transport quantities related to the energy equation of the charge state considered
  type (ids_edge_transport_model_momentum) :: momentum  ! /momentum - Transport coefficients related to the momentum equations of the charge state considered
endtype

type ids_edge_transport_model_neutral  !    Transport coefficients related to a given neutral species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying neutral (e.g. H, D, T, He, C, ...)
  integer(ids_int)  :: ion_index=ids_int_invalid       ! /ion_index - Index of the corresponding ion species in the ../../ion array
  type (ids_edge_transport_model_density) :: particles  ! /particles - Transport related to the ion density equation
  type (ids_edge_transport_model_energy) :: energy  ! /energy - Transport coefficients related to the ion energy equation
  type (ids_edge_transport_model_momentum) :: momentum  ! /momentum - Transport coefficients for the ion momentum equations
  integer(ids_int)  :: multiple_states_flag=ids_int_invalid       ! /multiple_states_flag - Multiple states calculation flag : 0-Only one state is considered; 1-Multiple states are considered
  type (ids_edge_transport_model_neutral_state),pointer :: state(:) => null()  ! /state(i) - Transport coefficients related to the different states of the species
endtype

type ids_edge_transport_model_ggd_fast_neutral  !    Transport coefficients related to a given neutral species (fast sampled data)
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying neutral (e.g. H, D, T, He, C, ...)
  integer(ids_int)  :: ion_index=ids_int_invalid       ! /ion_index - Index of the corresponding ion species in the ../../ion array
  type (ids_generic_grid_scalar_single_position),pointer :: particle_flux_integrated(:) => null()  ! /particle_flux_integrated(i) - Total number of particles of this species crossing a surface per unit time, for various surfaces (gr
endtype

type ids_edge_transport_model_ion  !    Transport coefficients related to a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(ids_real)  :: z_ion=ids_real_invalid       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed)
  real(ids_real)  :: z_ion_error_upper=ids_real_invalid
  real(ids_real)  :: z_ion_error_lower=ids_real_invalid
  integer(ids_int) :: z_ion_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  integer(ids_int)  :: neutral_index=ids_int_invalid       ! /neutral_index - Index of the corresponding neutral species in the ../../neutral array
  type (ids_edge_transport_model_density) :: particles  ! /particles - Transport related to the ion density equation
  type (ids_edge_transport_model_energy) :: energy  ! /energy - Transport coefficients related to the ion energy equation
  type (ids_edge_transport_model_momentum) :: momentum  ! /momentum - Transport coefficients for the ion momentum equations
  integer(ids_int)  :: multiple_states_flag=ids_int_invalid       ! /multiple_states_flag - Multiple states calculation flag : 0-Only one state is considered; 1-Multiple states are considered
  type (ids_edge_transport_model_ion_state),pointer :: state(:) => null()  ! /state(i) - Transport coefficients related to the different states of the species
endtype

type ids_edge_transport_model_ggd_fast_ion  !    Transport coefficients related to a given ion species (fast sampled data)
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(ids_real)  :: z_ion=ids_real_invalid       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed)
  real(ids_real)  :: z_ion_error_upper=ids_real_invalid
  real(ids_real)  :: z_ion_error_lower=ids_real_invalid
  integer(ids_int) :: z_ion_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  integer(ids_int)  :: neutral_index=ids_int_invalid       ! /neutral_index - Index of the corresponding neutral species in the ../../neutral array
  type (ids_generic_grid_scalar_single_position),pointer :: particle_flux_integrated(:) => null()  ! /particle_flux_integrated(i) - Total number of particles of this species crossing a surface per unit time, for various surfaces (gr
endtype

type ids_edge_transport_model_electrons  !    Transport coefficients related to electrons
  type (ids_edge_transport_model_density) :: particles  ! /particles - Transport quantities for the electron density equation
  type (ids_edge_transport_model_energy) :: energy  ! /energy - Transport quantities for the electron energy equation
endtype

type ids_edge_transport_model_ggd_fast_electrons  !    Transport coefficients related to electrons (fast sampled data)
  type (ids_generic_grid_scalar_single_position),pointer :: particle_flux_integrated(:) => null()  ! /particle_flux_integrated(i) - Total number of particles of this species crossing a surface per unit time, for various surfaces (gr
  type (ids_generic_grid_scalar_single_position),pointer :: power(:) => null()  ! /power(i) - Power carried by this species crossing a surface, for various surfaces (grid subsets)
endtype

type ids_edge_transport_model_ggd_fast  !    Transport coefficient given on the ggd at a given time slice (fast sampled data)
  type (ids_edge_transport_model_ggd_fast_electrons) :: electrons  ! /electrons - Transport quantities and flux integrals related to the electrons
  type (ids_edge_transport_model_ggd_fast_ion),pointer :: ion(:) => null()  ! /ion(i) - Transport coefficients and flux integrals related to the various ion species
  type (ids_edge_transport_model_ggd_fast_neutral),pointer :: neutral(:) => null()  ! /neutral(i) - Transport coefficients and flux integrals related to the various ion and neutral species
  type (ids_generic_grid_scalar_single_position),pointer :: power_ion_total(:) => null()  ! /power_ion_total(i) - Power carried by all ions (sum over ions species) crossing a surface, for various surfaces (grid sub
  type (ids_generic_grid_scalar_single_position),pointer :: energy_flux_max(:) => null()  ! /energy_flux_max(i) - Maximum power density over a surface, for various surfaces (grid subsets)
  type (ids_generic_grid_scalar_single_position),pointer :: power(:) => null()  ! /power(i) - Power (sum over all species) crossing a surface, for various surfaces (grid subsets)
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_edge_transport_model_ggd  !    Transport coefficient given on the ggd at a given time slice
  type (ids_generic_grid_vector_components),pointer :: conductivity(:) => null()  ! /conductivity(i) - Conductivity, on various grid subsets
  type (ids_edge_transport_model_electrons) :: electrons  ! /electrons - Transport quantities related to the electrons
  type (ids_edge_transport_model_energy) :: total_ion_energy  ! /total_ion_energy - Transport coefficients for the total (summed over ion  species) energy equation
  type (ids_edge_transport_model_momentum) :: momentum  ! /momentum - Transport coefficients for total momentum equation
  type (ids_edge_transport_model_ion),pointer :: ion(:) => null()  ! /ion(i) - Transport coefficients related to the various ion and neutral species
  type (ids_edge_transport_model_neutral),pointer :: neutral(:) => null()  ! /neutral(i) - Transport coefficients related to the various ion and neutral species
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_edge_transport_model  !    Transport coefficients for a given model
  type (ids_identifier) :: identifier  ! /identifier - Transport model identifier
  real(ids_real)  :: flux_multiplier=ids_real_invalid       ! /flux_multiplier - Multiplier applied to the particule flux when adding its contribution in the expression of the heat
  real(ids_real)  :: flux_multiplier_error_upper=ids_real_invalid
  real(ids_real)  :: flux_multiplier_error_lower=ids_real_invalid
  integer(ids_int) :: flux_multiplier_error_index=ids_int_invalid

  type (ids_edge_transport_model_ggd),pointer :: ggd(:) => null()  ! /ggd(i) - Transport coefficients represented using the general grid description, for various time slices
  type (ids_edge_transport_model_ggd_fast),pointer :: ggd_fast(:) => null()  ! /ggd_fast(i) - Quantities provided at a faster sampling rate than the full ggd quantities. These are either integra
endtype

type ids_edge_transport  !    Edge plasma transport. Energy terms correspond to the full kinetic energy equation (i.e. the energy flux takes into account the en
  type (ids_ids_properties) :: ids_properties  ! /edge_transport/ids_properties -
  type (ids_generic_grid_aos3_root),pointer :: grid_ggd(:) => null()  ! /edge_transport/grid_ggd(i) - Grid (using the Generic Grid Description), for various time slices. The timebase of this array of st
  type (ids_edge_transport_model),pointer :: model(:) => null()  ! /edge_transport/model(i) - Transport is described by a combination of various transport models
  type (ids_code) :: code  ! /edge_transport/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include em_coupling/dd_em_coupling.xsd
type ids_em_coupling  !    Description of the axisymmetric mutual electromagnetics; does not include non-axisymmetric coil systems; the convention is Quantit
  type (ids_ids_properties) :: ids_properties  ! /em_coupling/ids_properties -
  real(ids_real),pointer  :: mutual_active_active(:,:) => null()     ! /em_coupling/mutual_active_active - Mutual inductance coupling from active coils to active coils
  real(ids_real),pointer  :: mutual_active_active_error_upper(:,:) => null()
  real(ids_real),pointer  :: mutual_active_active_error_lower(:,:) => null()
  integer(ids_int) :: mutual_active_active_error_index=ids_int_invalid

  real(ids_real),pointer  :: mutual_passive_active(:,:) => null()     ! /em_coupling/mutual_passive_active - Mutual inductance coupling from active coils to passive loops
  real(ids_real),pointer  :: mutual_passive_active_error_upper(:,:) => null()
  real(ids_real),pointer  :: mutual_passive_active_error_lower(:,:) => null()
  integer(ids_int) :: mutual_passive_active_error_index=ids_int_invalid

  real(ids_real),pointer  :: mutual_loops_active(:,:) => null()     ! /em_coupling/mutual_loops_active - Mutual inductance coupling from active coils to poloidal flux loops
  real(ids_real),pointer  :: mutual_loops_active_error_upper(:,:) => null()
  real(ids_real),pointer  :: mutual_loops_active_error_lower(:,:) => null()
  integer(ids_int) :: mutual_loops_active_error_index=ids_int_invalid

  real(ids_real),pointer  :: field_probes_active(:,:) => null()     ! /em_coupling/field_probes_active - Poloidal field coupling from active coils to poloidal field probes
  real(ids_real),pointer  :: field_probes_active_error_upper(:,:) => null()
  real(ids_real),pointer  :: field_probes_active_error_lower(:,:) => null()
  integer(ids_int) :: field_probes_active_error_index=ids_int_invalid

  real(ids_real),pointer  :: mutual_passive_passive(:,:) => null()     ! /em_coupling/mutual_passive_passive - Mutual inductance coupling from passive loops to passive loops
  real(ids_real),pointer  :: mutual_passive_passive_error_upper(:,:) => null()
  real(ids_real),pointer  :: mutual_passive_passive_error_lower(:,:) => null()
  integer(ids_int) :: mutual_passive_passive_error_index=ids_int_invalid

  real(ids_real),pointer  :: mutual_loops_passive(:,:) => null()     ! /em_coupling/mutual_loops_passive - Mutual  inductance coupling from passive  loops to poloidal flux loops
  real(ids_real),pointer  :: mutual_loops_passive_error_upper(:,:) => null()
  real(ids_real),pointer  :: mutual_loops_passive_error_lower(:,:) => null()
  integer(ids_int) :: mutual_loops_passive_error_index=ids_int_invalid

  real(ids_real),pointer  :: field_probes_passive(:,:) => null()     ! /em_coupling/field_probes_passive - Poloidal field coupling from passive loops to poloidal field probes
  real(ids_real),pointer  :: field_probes_passive_error_upper(:,:) => null()
  real(ids_real),pointer  :: field_probes_passive_error_lower(:,:) => null()
  integer(ids_int) :: field_probes_passive_error_index=ids_int_invalid

  real(ids_real),pointer  :: mutual_grid_grid(:,:) => null()     ! /em_coupling/mutual_grid_grid - Mutual inductance from equilibrium grid to itself
  real(ids_real),pointer  :: mutual_grid_grid_error_upper(:,:) => null()
  real(ids_real),pointer  :: mutual_grid_grid_error_lower(:,:) => null()
  integer(ids_int) :: mutual_grid_grid_error_index=ids_int_invalid

  real(ids_real),pointer  :: mutual_grid_active(:,:) => null()     ! /em_coupling/mutual_grid_active - Mutual inductance coupling from active coils to equilibrium grid
  real(ids_real),pointer  :: mutual_grid_active_error_upper(:,:) => null()
  real(ids_real),pointer  :: mutual_grid_active_error_lower(:,:) => null()
  integer(ids_int) :: mutual_grid_active_error_index=ids_int_invalid

  real(ids_real),pointer  :: mutual_grid_passive(:,:) => null()     ! /em_coupling/mutual_grid_passive - Mutual inductance coupling from passive loops to equilibrium grid
  real(ids_real),pointer  :: mutual_grid_passive_error_upper(:,:) => null()
  real(ids_real),pointer  :: mutual_grid_passive_error_lower(:,:) => null()
  integer(ids_int) :: mutual_grid_passive_error_index=ids_int_invalid

  real(ids_real),pointer  :: field_probes_grid(:,:) => null()     ! /em_coupling/field_probes_grid - Poloidal field coupling from equilibrium grid to poloidal field probes
  real(ids_real),pointer  :: field_probes_grid_error_upper(:,:) => null()
  real(ids_real),pointer  :: field_probes_grid_error_lower(:,:) => null()
  integer(ids_int) :: field_probes_grid_error_index=ids_int_invalid

  real(ids_real),pointer  :: mutual_loops_grid(:,:) => null()     ! /em_coupling/mutual_loops_grid - Mutual inductance from equilibrium grid to poloidal flux loops
  real(ids_real),pointer  :: mutual_loops_grid_error_upper(:,:) => null()
  real(ids_real),pointer  :: mutual_loops_grid_error_lower(:,:) => null()
  integer(ids_int) :: mutual_loops_grid_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::active_coils => null()       ! /em_coupling/active_coils - List of the names of the active PF+CS coils
  character(len=ids_string_length), dimension(:), pointer ::passive_loops => null()       ! /em_coupling/passive_loops - List of the names of the passive loops
  character(len=ids_string_length), dimension(:), pointer ::poloidal_probes => null()       ! /em_coupling/poloidal_probes - List of the names of poloidal field probes
  character(len=ids_string_length), dimension(:), pointer ::flux_loops => null()       ! /em_coupling/flux_loops - List of the names of the axisymmetric flux loops
  character(len=ids_string_length), dimension(:), pointer ::grid_points => null()       ! /em_coupling/grid_points - List of the names of the plasma region grid points
  type (ids_code) :: code  ! /em_coupling/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include equilibrium/dd_equilibrium.xsd
type ids_equilibrium_boundary  !    Geometry of the plasma boundary
  integer(ids_int)  :: type=ids_int_invalid       ! /type - 0 (limiter) or 1 (diverted)
  type (ids_rz1d_dynamic_aos) :: outline  ! /outline - RZ outline of the plasma boundary
  type (ids_rz1d_dynamic_aos) :: lcfs  ! /lcfs - RZ description of the plasma boundary
  real(ids_real)  :: b_flux_pol_norm=ids_real_invalid       ! /b_flux_pol_norm - Value of the normalised poloidal flux at which the boundary is taken
  real(ids_real)  :: b_flux_pol_norm_error_upper=ids_real_invalid
  real(ids_real)  :: b_flux_pol_norm_error_lower=ids_real_invalid
  integer(ids_int) :: b_flux_pol_norm_error_index=ids_int_invalid

  type (ids_rz0d_dynamic_aos) :: geometric_axis  ! /geometric_axis - RZ position of the geometric axis (defined as (Rmin+Rmax) / 2 and (Zmin+Zmax) / 2 of the boundary)
  real(ids_real)  :: minor_radius=ids_real_invalid       ! /minor_radius - Minor radius of the plasma boundary (defined as (Rmax-Rmin) / 2 of the boundary)
  real(ids_real)  :: minor_radius_error_upper=ids_real_invalid
  real(ids_real)  :: minor_radius_error_lower=ids_real_invalid
  integer(ids_int) :: minor_radius_error_index=ids_int_invalid

  real(ids_real)  :: elongation=ids_real_invalid       ! /elongation - Elongation of the plasma boundary
  real(ids_real)  :: elongation_error_upper=ids_real_invalid
  real(ids_real)  :: elongation_error_lower=ids_real_invalid
  integer(ids_int) :: elongation_error_index=ids_int_invalid

  real(ids_real)  :: elongation_upper=ids_real_invalid       ! /elongation_upper - Elongation (upper half w.r.t. geometric axis) of the plasma boundary
  real(ids_real)  :: elongation_upper_error_upper=ids_real_invalid
  real(ids_real)  :: elongation_upper_error_lower=ids_real_invalid
  integer(ids_int) :: elongation_upper_error_index=ids_int_invalid

  real(ids_real)  :: elongation_lower=ids_real_invalid       ! /elongation_lower - Elongation (lower half w.r.t. geometric axis) of the plasma boundary
  real(ids_real)  :: elongation_lower_error_upper=ids_real_invalid
  real(ids_real)  :: elongation_lower_error_lower=ids_real_invalid
  integer(ids_int) :: elongation_lower_error_index=ids_int_invalid

  real(ids_real)  :: triangularity=ids_real_invalid       ! /triangularity - Triangularity of the plasma boundary
  real(ids_real)  :: triangularity_error_upper=ids_real_invalid
  real(ids_real)  :: triangularity_error_lower=ids_real_invalid
  integer(ids_int) :: triangularity_error_index=ids_int_invalid

  real(ids_real)  :: triangularity_upper=ids_real_invalid       ! /triangularity_upper - Upper triangularity of the plasma boundary
  real(ids_real)  :: triangularity_upper_error_upper=ids_real_invalid
  real(ids_real)  :: triangularity_upper_error_lower=ids_real_invalid
  integer(ids_int) :: triangularity_upper_error_index=ids_int_invalid

  real(ids_real)  :: triangularity_lower=ids_real_invalid       ! /triangularity_lower - Lower triangularity of the plasma boundary
  real(ids_real)  :: triangularity_lower_error_upper=ids_real_invalid
  real(ids_real)  :: triangularity_lower_error_lower=ids_real_invalid
  integer(ids_int) :: triangularity_lower_error_index=ids_int_invalid

  type (ids_rz0d_dynamic_aos),pointer :: x_point(:) => null()  ! /x_point(i) - Array of X-points, for each of them the RZ position is given
  type (ids_rz0d_dynamic_aos),pointer :: strike_point(:) => null()  ! /strike_point(i) - Array of strike points, for each of them the RZ position is given
  type (ids_rz0d_dynamic_aos) :: active_limiter_point  ! /active_limiter_point - RZ position of the active limiter point (point of the plasma boundary in contact with the limiter)
endtype

type ids_equilibrium_global_quantities_magnetic_axis  !    R, Z, and Btor at magnetic axis, dynamic within a type 3 array of structure (index on time)
  real(ids_real)  :: r=ids_real_invalid       ! /r - Major radius of the magnetic axis
  real(ids_real)  :: r_error_upper=ids_real_invalid
  real(ids_real)  :: r_error_lower=ids_real_invalid
  integer(ids_int) :: r_error_index=ids_int_invalid

  real(ids_real)  :: z=ids_real_invalid       ! /z - Height of the magnetic axis
  real(ids_real)  :: z_error_upper=ids_real_invalid
  real(ids_real)  :: z_error_lower=ids_real_invalid
  integer(ids_int) :: z_error_index=ids_int_invalid

  real(ids_real)  :: b_tor=ids_real_invalid       ! /b_tor - Total toroidal magnetic field at the magnetic axis
  real(ids_real)  :: b_tor_error_upper=ids_real_invalid
  real(ids_real)  :: b_tor_error_lower=ids_real_invalid
  integer(ids_int) :: b_tor_error_index=ids_int_invalid

  real(ids_real)  :: b_field_tor=ids_real_invalid       ! /b_field_tor - Total toroidal magnetic field at the magnetic axis
  real(ids_real)  :: b_field_tor_error_upper=ids_real_invalid
  real(ids_real)  :: b_field_tor_error_lower=ids_real_invalid
  integer(ids_int) :: b_field_tor_error_index=ids_int_invalid

endtype

type ids_equilibrium_global_quantities_qmin  !    Position and value of q_min
  real(ids_real)  :: value=ids_real_invalid       ! /value - Minimum q value
  real(ids_real)  :: value_error_upper=ids_real_invalid
  real(ids_real)  :: value_error_lower=ids_real_invalid
  integer(ids_int) :: value_error_index=ids_int_invalid

  real(ids_real)  :: rho_tor_norm=ids_real_invalid       ! /rho_tor_norm - Minimum q position in normalised toroidal flux coordinate
  real(ids_real)  :: rho_tor_norm_error_upper=ids_real_invalid
  real(ids_real)  :: rho_tor_norm_error_lower=ids_real_invalid
  integer(ids_int) :: rho_tor_norm_error_index=ids_int_invalid

endtype

type ids_equlibrium_global_quantities  !    0D parameters of the equilibrium
  real(ids_real)  :: beta_pol=ids_real_invalid       ! /beta_pol - Poloidal beta. Defined as betap = 4 int(p dV) / [R_0 * mu_0 * Ip^2]
  real(ids_real)  :: beta_pol_error_upper=ids_real_invalid
  real(ids_real)  :: beta_pol_error_lower=ids_real_invalid
  integer(ids_int) :: beta_pol_error_index=ids_int_invalid

  real(ids_real)  :: beta_tor=ids_real_invalid       ! /beta_tor - Toroidal beta, defined as the volume-averaged total perpendicular pressure divided by (B0^2/(2*mu0))
  real(ids_real)  :: beta_tor_error_upper=ids_real_invalid
  real(ids_real)  :: beta_tor_error_lower=ids_real_invalid
  integer(ids_int) :: beta_tor_error_index=ids_int_invalid

  real(ids_real)  :: beta_normal=ids_real_invalid       ! /beta_normal - Normalised toroidal beta, defined as 100 * beta_tor * a[m] * B0 [T] / ip [MA]
  real(ids_real)  :: beta_normal_error_upper=ids_real_invalid
  real(ids_real)  :: beta_normal_error_lower=ids_real_invalid
  integer(ids_int) :: beta_normal_error_index=ids_int_invalid

  real(ids_real)  :: ip=ids_real_invalid       ! /ip - Plasma current. Positive sign means anti-clockwise when viewed from above.
  real(ids_real)  :: ip_error_upper=ids_real_invalid
  real(ids_real)  :: ip_error_lower=ids_real_invalid
  integer(ids_int) :: ip_error_index=ids_int_invalid

  real(ids_real)  :: li_3=ids_real_invalid       ! /li_3 - Internal inductance
  real(ids_real)  :: li_3_error_upper=ids_real_invalid
  real(ids_real)  :: li_3_error_lower=ids_real_invalid
  integer(ids_int) :: li_3_error_index=ids_int_invalid

  real(ids_real)  :: volume=ids_real_invalid       ! /volume - Total plasma volume
  real(ids_real)  :: volume_error_upper=ids_real_invalid
  real(ids_real)  :: volume_error_lower=ids_real_invalid
  integer(ids_int) :: volume_error_index=ids_int_invalid

  real(ids_real)  :: area=ids_real_invalid       ! /area - Area of the LCFS poloidal cross section
  real(ids_real)  :: area_error_upper=ids_real_invalid
  real(ids_real)  :: area_error_lower=ids_real_invalid
  integer(ids_int) :: area_error_index=ids_int_invalid

  real(ids_real)  :: surface=ids_real_invalid       ! /surface - Surface area of the toroidal flux surface
  real(ids_real)  :: surface_error_upper=ids_real_invalid
  real(ids_real)  :: surface_error_lower=ids_real_invalid
  integer(ids_int) :: surface_error_index=ids_int_invalid

  real(ids_real)  :: length_pol=ids_real_invalid       ! /length_pol - Poloidal length of the magnetic surface
  real(ids_real)  :: length_pol_error_upper=ids_real_invalid
  real(ids_real)  :: length_pol_error_lower=ids_real_invalid
  integer(ids_int) :: length_pol_error_index=ids_int_invalid

  real(ids_real)  :: psi_axis=ids_real_invalid       ! /psi_axis - Poloidal flux at the magnetic axis
  real(ids_real)  :: psi_axis_error_upper=ids_real_invalid
  real(ids_real)  :: psi_axis_error_lower=ids_real_invalid
  integer(ids_int) :: psi_axis_error_index=ids_int_invalid

  real(ids_real)  :: psi_boundary=ids_real_invalid       ! /psi_boundary - Poloidal flux at the selected plasma boundary
  real(ids_real)  :: psi_boundary_error_upper=ids_real_invalid
  real(ids_real)  :: psi_boundary_error_lower=ids_real_invalid
  integer(ids_int) :: psi_boundary_error_index=ids_int_invalid

  type (ids_equilibrium_global_quantities_magnetic_axis) :: magnetic_axis  ! /magnetic_axis - Magnetic axis position and toroidal field
  real(ids_real)  :: q_axis=ids_real_invalid       ! /q_axis - q at the magnetic axis
  real(ids_real)  :: q_axis_error_upper=ids_real_invalid
  real(ids_real)  :: q_axis_error_lower=ids_real_invalid
  integer(ids_int) :: q_axis_error_index=ids_int_invalid

  real(ids_real)  :: q_95=ids_real_invalid       ! /q_95 - q at the 95% poloidal flux surface
  real(ids_real)  :: q_95_error_upper=ids_real_invalid
  real(ids_real)  :: q_95_error_lower=ids_real_invalid
  integer(ids_int) :: q_95_error_index=ids_int_invalid

  type (ids_equilibrium_global_quantities_qmin) :: q_min  ! /q_min - Minimum q value and position
  real(ids_real)  :: energy_mhd=ids_real_invalid       ! /energy_mhd - Plasma energy content = 3/2 * int(p,dV) with p being the total pressure (thermal + fast particles) [
  real(ids_real)  :: energy_mhd_error_upper=ids_real_invalid
  real(ids_real)  :: energy_mhd_error_lower=ids_real_invalid
  integer(ids_int) :: energy_mhd_error_index=ids_int_invalid

  real(ids_real)  :: w_mhd=ids_real_invalid       ! /w_mhd - Plasma energy content = 3/2 * int(p,dV) with p being the total pressure (thermal + fast particles) [
  real(ids_real)  :: w_mhd_error_upper=ids_real_invalid
  real(ids_real)  :: w_mhd_error_lower=ids_real_invalid
  integer(ids_int) :: w_mhd_error_index=ids_int_invalid

endtype

type ids_equilibrium_profiles_1d  !    Equilibrium profiles (1D radial grid) as a function of the poloidal flux
  real(ids_real),pointer  :: psi(:) => null()     ! /psi - Poloidal flux
  real(ids_real),pointer  :: psi_error_upper(:) => null()
  real(ids_real),pointer  :: psi_error_lower(:) => null()
  integer(ids_int) :: psi_error_index=ids_int_invalid

  real(ids_real),pointer  :: phi(:) => null()     ! /phi - Toroidal flux
  real(ids_real),pointer  :: phi_error_upper(:) => null()
  real(ids_real),pointer  :: phi_error_lower(:) => null()
  integer(ids_int) :: phi_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure(:) => null()     ! /pressure - Pressure
  real(ids_real),pointer  :: pressure_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_error_lower(:) => null()
  integer(ids_int) :: pressure_error_index=ids_int_invalid

  real(ids_real),pointer  :: f(:) => null()     ! /f - Diamagnetic function (F=R B_Phi)
  real(ids_real),pointer  :: f_error_upper(:) => null()
  real(ids_real),pointer  :: f_error_lower(:) => null()
  integer(ids_int) :: f_error_index=ids_int_invalid

  real(ids_real),pointer  :: dpressure_dpsi(:) => null()     ! /dpressure_dpsi - Derivative of pressure w.r.t. psi
  real(ids_real),pointer  :: dpressure_dpsi_error_upper(:) => null()
  real(ids_real),pointer  :: dpressure_dpsi_error_lower(:) => null()
  integer(ids_int) :: dpressure_dpsi_error_index=ids_int_invalid

  real(ids_real),pointer  :: f_df_dpsi(:) => null()     ! /f_df_dpsi - Derivative of F w.r.t. Psi, multiplied with F
  real(ids_real),pointer  :: f_df_dpsi_error_upper(:) => null()
  real(ids_real),pointer  :: f_df_dpsi_error_lower(:) => null()
  integer(ids_int) :: f_df_dpsi_error_index=ids_int_invalid

  real(ids_real),pointer  :: j_tor(:) => null()     ! /j_tor - Flux surface averaged toroidal current density = average(j_tor/R) / average(1/R)
  real(ids_real),pointer  :: j_tor_error_upper(:) => null()
  real(ids_real),pointer  :: j_tor_error_lower(:) => null()
  integer(ids_int) :: j_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: j_parallel(:) => null()     ! /j_parallel - Flux surface averaged parallel current density = average(j.B) / B0, where B0 = Equilibrium/Global/To
  real(ids_real),pointer  :: j_parallel_error_upper(:) => null()
  real(ids_real),pointer  :: j_parallel_error_lower(:) => null()
  integer(ids_int) :: j_parallel_error_index=ids_int_invalid

  real(ids_real),pointer  :: q(:) => null()     ! /q - Safety factor
  real(ids_real),pointer  :: q_error_upper(:) => null()
  real(ids_real),pointer  :: q_error_lower(:) => null()
  integer(ids_int) :: q_error_index=ids_int_invalid

  real(ids_real),pointer  :: magnetic_shear(:) => null()     ! /magnetic_shear - Magnetic shear, defined as rho_tor/q . dq/drho_tor
  real(ids_real),pointer  :: magnetic_shear_error_upper(:) => null()
  real(ids_real),pointer  :: magnetic_shear_error_lower(:) => null()
  integer(ids_int) :: magnetic_shear_error_index=ids_int_invalid

  real(ids_real),pointer  :: r_inboard(:) => null()     ! /r_inboard - Radial coordinate (major radius) on the inboard side of the magnetic axis
  real(ids_real),pointer  :: r_inboard_error_upper(:) => null()
  real(ids_real),pointer  :: r_inboard_error_lower(:) => null()
  integer(ids_int) :: r_inboard_error_index=ids_int_invalid

  real(ids_real),pointer  :: r_outboard(:) => null()     ! /r_outboard - Radial coordinate (major radius) on the outboard side of the magnetic axis
  real(ids_real),pointer  :: r_outboard_error_upper(:) => null()
  real(ids_real),pointer  :: r_outboard_error_lower(:) => null()
  integer(ids_int) :: r_outboard_error_index=ids_int_invalid

  real(ids_real),pointer  :: rho_tor(:) => null()     ! /rho_tor - Toroidal flux coordinate. The toroidal field used in its definition is indicated under vacuum_toroid
  real(ids_real),pointer  :: rho_tor_error_upper(:) => null()
  real(ids_real),pointer  :: rho_tor_error_lower(:) => null()
  integer(ids_int) :: rho_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: rho_tor_norm(:) => null()     ! /rho_tor_norm - Normalised toroidal flux coordinate. The normalizing value for rho_tor_norm, is the toroidal flux co
  real(ids_real),pointer  :: rho_tor_norm_error_upper(:) => null()
  real(ids_real),pointer  :: rho_tor_norm_error_lower(:) => null()
  integer(ids_int) :: rho_tor_norm_error_index=ids_int_invalid

  real(ids_real),pointer  :: dpsi_drho_tor(:) => null()     ! /dpsi_drho_tor - Derivative of Psi with respect to Rho_Tor
  real(ids_real),pointer  :: dpsi_drho_tor_error_upper(:) => null()
  real(ids_real),pointer  :: dpsi_drho_tor_error_lower(:) => null()
  integer(ids_int) :: dpsi_drho_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: elongation(:) => null()     ! /elongation - Elongation
  real(ids_real),pointer  :: elongation_error_upper(:) => null()
  real(ids_real),pointer  :: elongation_error_lower(:) => null()
  integer(ids_int) :: elongation_error_index=ids_int_invalid

  real(ids_real),pointer  :: triangularity_upper(:) => null()     ! /triangularity_upper - Upper triangularity w.r.t. magnetic axis
  real(ids_real),pointer  :: triangularity_upper_error_upper(:) => null()
  real(ids_real),pointer  :: triangularity_upper_error_lower(:) => null()
  integer(ids_int) :: triangularity_upper_error_index=ids_int_invalid

  real(ids_real),pointer  :: triangularity_lower(:) => null()     ! /triangularity_lower - Lower triangularity w.r.t. magnetic axis
  real(ids_real),pointer  :: triangularity_lower_error_upper(:) => null()
  real(ids_real),pointer  :: triangularity_lower_error_lower(:) => null()
  integer(ids_int) :: triangularity_lower_error_index=ids_int_invalid

  real(ids_real),pointer  :: volume(:) => null()     ! /volume - Volume enclosed in the flux surface
  real(ids_real),pointer  :: volume_error_upper(:) => null()
  real(ids_real),pointer  :: volume_error_lower(:) => null()
  integer(ids_int) :: volume_error_index=ids_int_invalid

  real(ids_real),pointer  :: rho_volume_norm(:) => null()     ! /rho_volume_norm - Normalised square root of enclosed volume (radial coordinate). The normalizing value is the enclosed
  real(ids_real),pointer  :: rho_volume_norm_error_upper(:) => null()
  real(ids_real),pointer  :: rho_volume_norm_error_lower(:) => null()
  integer(ids_int) :: rho_volume_norm_error_index=ids_int_invalid

  real(ids_real),pointer  :: dvolume_dpsi(:) => null()     ! /dvolume_dpsi - Radial derivative of the volume enclosed in the flux surface with respect to Psi
  real(ids_real),pointer  :: dvolume_dpsi_error_upper(:) => null()
  real(ids_real),pointer  :: dvolume_dpsi_error_lower(:) => null()
  integer(ids_int) :: dvolume_dpsi_error_index=ids_int_invalid

  real(ids_real),pointer  :: dvolume_drho_tor(:) => null()     ! /dvolume_drho_tor - Radial derivative of the volume enclosed in the flux surface with respect to Rho_Tor
  real(ids_real),pointer  :: dvolume_drho_tor_error_upper(:) => null()
  real(ids_real),pointer  :: dvolume_drho_tor_error_lower(:) => null()
  integer(ids_int) :: dvolume_drho_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: area(:) => null()     ! /area - Cross-sectional area of the flux surface
  real(ids_real),pointer  :: area_error_upper(:) => null()
  real(ids_real),pointer  :: area_error_lower(:) => null()
  integer(ids_int) :: area_error_index=ids_int_invalid

  real(ids_real),pointer  :: darea_dpsi(:) => null()     ! /darea_dpsi - Radial derivative of the cross-sectional area of the flux surface with respect to psi
  real(ids_real),pointer  :: darea_dpsi_error_upper(:) => null()
  real(ids_real),pointer  :: darea_dpsi_error_lower(:) => null()
  integer(ids_int) :: darea_dpsi_error_index=ids_int_invalid

  real(ids_real),pointer  :: darea_drho_tor(:) => null()     ! /darea_drho_tor - Radial derivative of the cross-sectional area of the flux surface with respect to rho_tor
  real(ids_real),pointer  :: darea_drho_tor_error_upper(:) => null()
  real(ids_real),pointer  :: darea_drho_tor_error_lower(:) => null()
  integer(ids_int) :: darea_drho_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: surface(:) => null()     ! /surface - Surface area of the toroidal flux surface
  real(ids_real),pointer  :: surface_error_upper(:) => null()
  real(ids_real),pointer  :: surface_error_lower(:) => null()
  integer(ids_int) :: surface_error_index=ids_int_invalid

  real(ids_real),pointer  :: trapped_fraction(:) => null()     ! /trapped_fraction - Trapped particle fraction
  real(ids_real),pointer  :: trapped_fraction_error_upper(:) => null()
  real(ids_real),pointer  :: trapped_fraction_error_lower(:) => null()
  integer(ids_int) :: trapped_fraction_error_index=ids_int_invalid

  real(ids_real),pointer  :: gm1(:) => null()     ! /gm1 - Flux surface averaged 1/R^2
  real(ids_real),pointer  :: gm1_error_upper(:) => null()
  real(ids_real),pointer  :: gm1_error_lower(:) => null()
  integer(ids_int) :: gm1_error_index=ids_int_invalid

  real(ids_real),pointer  :: gm2(:) => null()     ! /gm2 - Flux surface averaged grad_rho^2/R^2
  real(ids_real),pointer  :: gm2_error_upper(:) => null()
  real(ids_real),pointer  :: gm2_error_lower(:) => null()
  integer(ids_int) :: gm2_error_index=ids_int_invalid

  real(ids_real),pointer  :: gm3(:) => null()     ! /gm3 - Flux surface averaged grad_rho^2
  real(ids_real),pointer  :: gm3_error_upper(:) => null()
  real(ids_real),pointer  :: gm3_error_lower(:) => null()
  integer(ids_int) :: gm3_error_index=ids_int_invalid

  real(ids_real),pointer  :: gm4(:) => null()     ! /gm4 - Flux surface averaged 1/B^2
  real(ids_real),pointer  :: gm4_error_upper(:) => null()
  real(ids_real),pointer  :: gm4_error_lower(:) => null()
  integer(ids_int) :: gm4_error_index=ids_int_invalid

  real(ids_real),pointer  :: gm5(:) => null()     ! /gm5 - Flux surface averaged B^2
  real(ids_real),pointer  :: gm5_error_upper(:) => null()
  real(ids_real),pointer  :: gm5_error_lower(:) => null()
  integer(ids_int) :: gm5_error_index=ids_int_invalid

  real(ids_real),pointer  :: gm6(:) => null()     ! /gm6 - Flux surface averaged grad_rho^2/B^2
  real(ids_real),pointer  :: gm6_error_upper(:) => null()
  real(ids_real),pointer  :: gm6_error_lower(:) => null()
  integer(ids_int) :: gm6_error_index=ids_int_invalid

  real(ids_real),pointer  :: gm7(:) => null()     ! /gm7 - Flux surface averaged grad_rho
  real(ids_real),pointer  :: gm7_error_upper(:) => null()
  real(ids_real),pointer  :: gm7_error_lower(:) => null()
  integer(ids_int) :: gm7_error_index=ids_int_invalid

  real(ids_real),pointer  :: gm8(:) => null()     ! /gm8 - Flux surface averaged R
  real(ids_real),pointer  :: gm8_error_upper(:) => null()
  real(ids_real),pointer  :: gm8_error_lower(:) => null()
  integer(ids_int) :: gm8_error_index=ids_int_invalid

  real(ids_real),pointer  :: gm9(:) => null()     ! /gm9 - Flux surface averaged 1/R
  real(ids_real),pointer  :: gm9_error_upper(:) => null()
  real(ids_real),pointer  :: gm9_error_lower(:) => null()
  integer(ids_int) :: gm9_error_index=ids_int_invalid

  real(ids_real),pointer  :: b_average(:) => null()     ! /b_average - Flux surface averaged B
  real(ids_real),pointer  :: b_average_error_upper(:) => null()
  real(ids_real),pointer  :: b_average_error_lower(:) => null()
  integer(ids_int) :: b_average_error_index=ids_int_invalid

  real(ids_real),pointer  :: b_field_average(:) => null()     ! /b_field_average - Flux surface averaged B
  real(ids_real),pointer  :: b_field_average_error_upper(:) => null()
  real(ids_real),pointer  :: b_field_average_error_lower(:) => null()
  integer(ids_int) :: b_field_average_error_index=ids_int_invalid

  real(ids_real),pointer  :: b_min(:) => null()     ! /b_min - Minimum(B) on the flux surface
  real(ids_real),pointer  :: b_min_error_upper(:) => null()
  real(ids_real),pointer  :: b_min_error_lower(:) => null()
  integer(ids_int) :: b_min_error_index=ids_int_invalid

  real(ids_real),pointer  :: b_field_min(:) => null()     ! /b_field_min - Minimum(B) on the flux surface
  real(ids_real),pointer  :: b_field_min_error_upper(:) => null()
  real(ids_real),pointer  :: b_field_min_error_lower(:) => null()
  integer(ids_int) :: b_field_min_error_index=ids_int_invalid

  real(ids_real),pointer  :: b_max(:) => null()     ! /b_max - Maximum(B) on the flux surface
  real(ids_real),pointer  :: b_max_error_upper(:) => null()
  real(ids_real),pointer  :: b_max_error_lower(:) => null()
  integer(ids_int) :: b_max_error_index=ids_int_invalid

  real(ids_real),pointer  :: b_field_max(:) => null()     ! /b_field_max - Maximum(B) on the flux surface
  real(ids_real),pointer  :: b_field_max_error_upper(:) => null()
  real(ids_real),pointer  :: b_field_max_error_lower(:) => null()
  integer(ids_int) :: b_field_max_error_index=ids_int_invalid

  real(ids_real),pointer  :: beta_pol(:) => null()     ! /beta_pol - Poloidal beta profile. Defined as betap = 4 int(p dV) / [R_0 * mu_0 * Ip^2]
  real(ids_real),pointer  :: beta_pol_error_upper(:) => null()
  real(ids_real),pointer  :: beta_pol_error_lower(:) => null()
  integer(ids_int) :: beta_pol_error_index=ids_int_invalid

  real(ids_real),pointer  :: mass_density(:) => null()     ! /mass_density - Mass density
  real(ids_real),pointer  :: mass_density_error_upper(:) => null()
  real(ids_real),pointer  :: mass_density_error_lower(:) => null()
  integer(ids_int) :: mass_density_error_index=ids_int_invalid

endtype

type ids_equilibrium_profiles_2d  !    Equilibrium 2D profiles in the poloidal plane
  type (ids_identifier) :: grid_type  ! /grid_type - Selection of one of a set of grid types
  type (ids_equilibrium_profiles_2d_grid) :: grid  ! /grid - Definition of the 2D grid
  real(ids_real),pointer  :: r(:,:) => null()     ! /r - Values of the major radius on the grid
  real(ids_real),pointer  :: r_error_upper(:,:) => null()
  real(ids_real),pointer  :: r_error_lower(:,:) => null()
  integer(ids_int) :: r_error_index=ids_int_invalid

  real(ids_real),pointer  :: z(:,:) => null()     ! /z - Values of the Height on the grid
  real(ids_real),pointer  :: z_error_upper(:,:) => null()
  real(ids_real),pointer  :: z_error_lower(:,:) => null()
  integer(ids_int) :: z_error_index=ids_int_invalid

  real(ids_real),pointer  :: psi(:,:) => null()     ! /psi - Values of the poloidal flux at the grid in the poloidal plane
  real(ids_real),pointer  :: psi_error_upper(:,:) => null()
  real(ids_real),pointer  :: psi_error_lower(:,:) => null()
  integer(ids_int) :: psi_error_index=ids_int_invalid

  real(ids_real),pointer  :: theta(:,:) => null()     ! /theta - Values of the poloidal angle on the grid
  real(ids_real),pointer  :: theta_error_upper(:,:) => null()
  real(ids_real),pointer  :: theta_error_lower(:,:) => null()
  integer(ids_int) :: theta_error_index=ids_int_invalid

  real(ids_real),pointer  :: phi(:,:) => null()     ! /phi - Toroidal flux
  real(ids_real),pointer  :: phi_error_upper(:,:) => null()
  real(ids_real),pointer  :: phi_error_lower(:,:) => null()
  integer(ids_int) :: phi_error_index=ids_int_invalid

  real(ids_real),pointer  :: j_tor(:,:) => null()     ! /j_tor - Toroidal plasma current density
  real(ids_real),pointer  :: j_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: j_tor_error_lower(:,:) => null()
  integer(ids_int) :: j_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: j_parallel(:,:) => null()     ! /j_parallel - Parallel (to magnetic field) plasma current density
  real(ids_real),pointer  :: j_parallel_error_upper(:,:) => null()
  real(ids_real),pointer  :: j_parallel_error_lower(:,:) => null()
  integer(ids_int) :: j_parallel_error_index=ids_int_invalid

  real(ids_real),pointer  :: b_r(:,:) => null()     ! /b_r - R component of the poloidal magnetic field
  real(ids_real),pointer  :: b_r_error_upper(:,:) => null()
  real(ids_real),pointer  :: b_r_error_lower(:,:) => null()
  integer(ids_int) :: b_r_error_index=ids_int_invalid

  real(ids_real),pointer  :: b_field_r(:,:) => null()     ! /b_field_r - R component of the poloidal magnetic field
  real(ids_real),pointer  :: b_field_r_error_upper(:,:) => null()
  real(ids_real),pointer  :: b_field_r_error_lower(:,:) => null()
  integer(ids_int) :: b_field_r_error_index=ids_int_invalid

  real(ids_real),pointer  :: b_z(:,:) => null()     ! /b_z - Z component of the poloidal magnetic field
  real(ids_real),pointer  :: b_z_error_upper(:,:) => null()
  real(ids_real),pointer  :: b_z_error_lower(:,:) => null()
  integer(ids_int) :: b_z_error_index=ids_int_invalid

  real(ids_real),pointer  :: b_field_z(:,:) => null()     ! /b_field_z - Z component of the poloidal magnetic field
  real(ids_real),pointer  :: b_field_z_error_upper(:,:) => null()
  real(ids_real),pointer  :: b_field_z_error_lower(:,:) => null()
  integer(ids_int) :: b_field_z_error_index=ids_int_invalid

  real(ids_real),pointer  :: b_tor(:,:) => null()     ! /b_tor - Toroidal component of the magnetic field
  real(ids_real),pointer  :: b_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: b_tor_error_lower(:,:) => null()
  integer(ids_int) :: b_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: b_field_tor(:,:) => null()     ! /b_field_tor - Toroidal component of the magnetic field
  real(ids_real),pointer  :: b_field_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: b_field_tor_error_lower(:,:) => null()
  integer(ids_int) :: b_field_tor_error_index=ids_int_invalid

endtype

type ids_equilibrium_ggd  !    Equilibrium ggd representation
  type (ids_generic_grid_dynamic) :: grid  ! /grid - Grid description
  type (ids_generic_grid_scalar),pointer :: r(:) => null()  ! /r(i) - Values of the major radius on various grid subsets
  type (ids_generic_grid_scalar),pointer :: z(:) => null()  ! /z(i) - Values of the Height on various grid subsets
  type (ids_generic_grid_scalar),pointer :: psi(:) => null()  ! /psi(i) - Values of the poloidal flux, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: phi(:) => null()  ! /phi(i) - Values of the toroidal flux, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: theta(:) => null()  ! /theta(i) - Values of the poloidal angle, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: j_tor(:) => null()  ! /j_tor(i) - Toroidal plasma current density, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: j_parallel(:) => null()  ! /j_parallel(i) - Parallel (to magnetic field) plasma current density, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: b_field_r(:) => null()  ! /b_field_r(i) - R component of the poloidal magnetic field, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: b_field_z(:) => null()  ! /b_field_z(i) - Z component of the poloidal magnetic field, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: b_field_tor(:) => null()  ! /b_field_tor(i) - Toroidal component of the magnetic field, given on various grid subsets
endtype

type ids_equilibrium_time_slice  !    Equilibrium at a given time slice
  type (ids_equilibrium_boundary) :: boundary  ! /boundary - Description of the plasma boundary
  type (ids_equlibrium_global_quantities) :: global_quantities  ! /global_quantities - 0D parameters of the equilibrium
  type (ids_equilibrium_profiles_1d) :: profiles_1d  ! /profiles_1d - Equilibrium profiles (1D radial grid) as a function of the poloidal flux
  type (ids_equilibrium_profiles_2d),pointer :: profiles_2d(:) => null()  ! /profiles_2d(i) - Equilibrium 2D profiles in the poloidal plane. Multiple 2D representations of the equilibrium can be
  type (ids_equilibrium_ggd),pointer :: ggd(:) => null()  ! /ggd(i) - Equilibrium representation using the generic grid description. Multiple GGD representations of the e
  type (ids_equilibrium_coordinate_system) :: coordinate_system  ! /coordinate_system - Flux surface coordinate system on a square grid of flux and poloidal angle
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_equilibrium  !    Description of a 2D, axi-symmetric, tokamak equilibrium; result of an equilibrium code.
  type (ids_ids_properties) :: ids_properties  ! /equilibrium/ids_properties -
  type (ids_b_tor_vacuum_1) :: vacuum_toroidal_field  ! /equilibrium/vacuum_toroidal_field - Characteristics of the vacuum toroidal field (used in rho_tor definition and in the normalization of
  type (ids_equilibrium_time_slice),pointer :: time_slice(:) => null()  ! /equilibrium/time_slice(i) - Set of equilibria at various time slices
  type (ids_code) :: code  ! /equilibrium/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include gas_injection/dd_gas_injection.xsd
! SPECIAL STRUCTURE data / time
type ids_gas_injection_pipe_valve_flow_rate  !    Flow rate at the exit of the valve
  real(ids_real), pointer  :: data(:) => null()     ! /flow_rate - Flow rate at the exit of the valve
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_gas_injection_pipe_valve_electron_rate  !    Number of electrons injected per second
  real(ids_real), pointer  :: data(:) => null()     ! /electron_rate - Number of electrons injected per second
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_gas_injection_pipe_valve  !    Gas injection valve
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the valve
  character(len=ids_string_length), dimension(:), pointer ::identifier => null()       ! /identifier - ID of the valve
  type (ids_gas_mixture_constant),pointer :: species(:) => null()  ! /species(i) - Species injected by the valve (may be more than one in case the valve injects a gas mixture)
  real(ids_real)  :: flow_rate_min=ids_real_invalid       ! /flow_rate_min - Minimum flow rate of the valve
  real(ids_real)  :: flow_rate_min_error_upper=ids_real_invalid
  real(ids_real)  :: flow_rate_min_error_lower=ids_real_invalid
  integer(ids_int) :: flow_rate_min_error_index=ids_int_invalid

  real(ids_real)  :: flow_rate_max=ids_real_invalid       ! /flow_rate_max - Maximum flow rate of the valve
  real(ids_real)  :: flow_rate_max_error_upper=ids_real_invalid
  real(ids_real)  :: flow_rate_max_error_lower=ids_real_invalid
  integer(ids_int) :: flow_rate_max_error_index=ids_int_invalid

  type (ids_gas_injection_pipe_valve_flow_rate) :: flow_rate  ! /flow_rate - Flow rate at the exit of the valve
  type (ids_gas_injection_pipe_valve_electron_rate) :: electron_rate  ! /electron_rate - Number of electrons injected per second
endtype

! SPECIAL STRUCTURE data / time
type ids_gas_injection_pipe_flow_rate  !    Flow rate at the exit of the pipe
  real(ids_real), pointer  :: data(:) => null()     ! /flow_rate - Flow rate at the exit of the pipe
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_gas_injection_pipe  !    Gas injection pipe
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the injection pipe
  character(len=ids_string_length), dimension(:), pointer ::identifier => null()       ! /identifier - ID of the injection pipe
  type (ids_gas_mixture_constant),pointer :: species(:) => null()  ! /species(i) - Species injected by the pipe (may be more than one in case the valve injects a gas mixture)
  real(ids_real)  :: length=ids_real_invalid       ! /length - Pipe length
  real(ids_real)  :: length_error_upper=ids_real_invalid
  real(ids_real)  :: length_error_lower=ids_real_invalid
  integer(ids_int) :: length_error_index=ids_int_invalid

  type (ids_rzphi0d_static) :: exit_position  ! /exit_position - Exit position of the pipe in the vaccum vessel
  type (ids_gas_injection_pipe_flow_rate) :: flow_rate  ! /flow_rate - Flow rate at the exit of the pipe
  type (ids_gas_injection_pipe_valve),pointer :: valve(:) => null()  ! /valve(i) - Set of valves connecting a gas bottle the the pipe
endtype

type ids_gas_injection  !    Gas injection by a system of pipes and valves
  type (ids_ids_properties) :: ids_properties  ! /gas_injection/ids_properties -
  type (ids_gas_injection_pipe),pointer :: pipe(:) => null()  ! /gas_injection/pipe(i) - Set of gas injection pipes
  type (ids_code) :: code  ! /gas_injection/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include ic_antennas/dd_ic_antennas.xsd
type ids_oblique_static  !    Oblique description of a 2D object
  real(ids_real)  :: r=ids_real_invalid       ! /r - Geometric centre R
  real(ids_real)  :: r_error_upper=ids_real_invalid
  real(ids_real)  :: r_error_lower=ids_real_invalid
  integer(ids_int) :: r_error_index=ids_int_invalid

  real(ids_real)  :: z=ids_real_invalid       ! /z - Geometric centre Z
  real(ids_real)  :: z_error_upper=ids_real_invalid
  real(ids_real)  :: z_error_lower=ids_real_invalid
  integer(ids_int) :: z_error_index=ids_int_invalid

  real(ids_real)  :: length=ids_real_invalid       ! /length - Length
  real(ids_real)  :: length_error_upper=ids_real_invalid
  real(ids_real)  :: length_error_lower=ids_real_invalid
  integer(ids_int) :: length_error_index=ids_int_invalid

  real(ids_real)  :: thickness=ids_real_invalid       ! /thickness - Thickness
  real(ids_real)  :: thickness_error_upper=ids_real_invalid
  real(ids_real)  :: thickness_error_lower=ids_real_invalid
  integer(ids_int) :: thickness_error_index=ids_int_invalid

  real(ids_real)  :: alpha=ids_real_invalid       ! /alpha - Inclination of first angle TBD
  real(ids_real)  :: alpha_error_upper=ids_real_invalid
  real(ids_real)  :: alpha_error_lower=ids_real_invalid
  integer(ids_int) :: alpha_error_index=ids_int_invalid

  real(ids_real)  :: beta=ids_real_invalid       ! /beta - Inclination of second angle TBD
  real(ids_real)  :: beta_error_upper=ids_real_invalid
  real(ids_real)  :: beta_error_lower=ids_real_invalid
  integer(ids_int) :: beta_error_index=ids_int_invalid

endtype

type ids_arcs_of_circle_static  !    Arcs of circle description of a 2D contour
  real(ids_real),pointer  :: r(:) => null()     ! /r - Major radii of the start point of each arc of circle
  real(ids_real),pointer  :: r_error_upper(:) => null()
  real(ids_real),pointer  :: r_error_lower(:) => null()
  integer(ids_int) :: r_error_index=ids_int_invalid

  real(ids_real),pointer  :: z(:) => null()     ! /z - Height of the start point of each arc of circle
  real(ids_real),pointer  :: z_error_upper(:) => null()
  real(ids_real),pointer  :: z_error_lower(:) => null()
  integer(ids_int) :: z_error_index=ids_int_invalid

  real(ids_real),pointer  :: curvature_radii(:) => null()     ! /curvature_radii - Curvature radius of each arc of circle
  real(ids_real),pointer  :: curvature_radii_error_upper(:) => null()
  real(ids_real),pointer  :: curvature_radii_error_lower(:) => null()
  integer(ids_int) :: curvature_radii_error_index=ids_int_invalid

endtype

type ids_rectangle_static  !    Rectangular description of a 2D object
  real(ids_real)  :: r=ids_real_invalid       ! /r - Geometric centre R
  real(ids_real)  :: r_error_upper=ids_real_invalid
  real(ids_real)  :: r_error_lower=ids_real_invalid
  integer(ids_int) :: r_error_index=ids_int_invalid

  real(ids_real)  :: z=ids_real_invalid       ! /z - Geometric centre Z
  real(ids_real)  :: z_error_upper=ids_real_invalid
  real(ids_real)  :: z_error_lower=ids_real_invalid
  integer(ids_int) :: z_error_index=ids_int_invalid

  real(ids_real)  :: width=ids_real_invalid       ! /width - Horizontal full width
  real(ids_real)  :: width_error_upper=ids_real_invalid
  real(ids_real)  :: width_error_lower=ids_real_invalid
  integer(ids_int) :: width_error_index=ids_int_invalid

  real(ids_real)  :: height=ids_real_invalid       ! /height - Vertical full height
  real(ids_real)  :: height_error_upper=ids_real_invalid
  real(ids_real)  :: height_error_lower=ids_real_invalid
  integer(ids_int) :: height_error_index=ids_int_invalid

endtype

type ids_outline_2d_geometry_static  !    Description of 2D geometry
  integer(ids_int)  :: geometry_type=ids_int_invalid       ! /geometry_type - Type used to describe the element shape (1:'outline', 2:'rectangle', 3:'oblique', 4:'arcs of circle'
  type (ids_rz1d_static) :: outline  ! /outline - Irregular outline of the element
  type (ids_rectangle_static) :: rectangle  ! /rectangle - Rectangular description of the element
  type (ids_oblique_static) :: oblique  ! /oblique - Trapezoidal description of the element
  type (ids_arcs_of_circle_static) :: arcs_of_circle  ! /arcs_of_circle - Description of the element contour by a set of arcs of circle. For each of these, the position of th
endtype

! SPECIAL STRUCTURE data / time
type ids_ic_antennas_strap_current  !    Root mean square current flowing along the strap
  real(ids_real), pointer  :: data(:) => null()     ! /current - Root mean square current flowing along the strap
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_ic_antennas_strap_phase  !    Phase of the strap current
  real(ids_real), pointer  :: data(:) => null()     ! /phase - Phase of the strap current
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_ic_antennas_strap  !    Properties of IC antenna strap
  type (ids_rzphi1d_static) :: outline  ! /outline - Strap outline
  real(ids_real)  :: width_tor=ids_real_invalid       ! /width_tor - Width of strap in the toroidal direction
  real(ids_real)  :: width_tor_error_upper=ids_real_invalid
  real(ids_real)  :: width_tor_error_lower=ids_real_invalid
  integer(ids_int) :: width_tor_error_index=ids_int_invalid

  real(ids_real)  :: distance_to_conductor=ids_real_invalid       ! /distance_to_conductor - Distance to conducting wall or other conductor behind the antenna strap
  real(ids_real)  :: distance_to_conductor_error_upper=ids_real_invalid
  real(ids_real)  :: distance_to_conductor_error_lower=ids_real_invalid
  integer(ids_int) :: distance_to_conductor_error_index=ids_int_invalid

  type (ids_outline_2d_geometry_static) :: geometry  ! /geometry - Cross-sectional shape of the strap
  type (ids_ic_antennas_strap_current) :: current  ! /current - Root mean square current flowing along the strap
  type (ids_ic_antennas_strap_phase) :: phase  ! /phase - Phase of the strap current
endtype

type ids_ic_antennas_surface_current  !    Description of the IC surface current on the antenna straps and on passive components.
  integer(ids_int),pointer  :: m_pol(:) => null()      ! /m_pol - Poloidal mode numbers, used to describe the spectrum of the antenna current. The poloidal angle is d
  integer(ids_int),pointer  :: n_tor(:) => null()      ! /n_tor - Toroidal mode numbers, used to describe the spectrum of the antenna current
  integer(ids_int),pointer  :: spectrum(:) => null()      ! /spectrum - Spectrum of the total surface current on the antenna strap and passive components expressed in poloi
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

! SPECIAL STRUCTURE data / time
type ids_ic_antennas_antenna_frequency  !    Frequency
  real(ids_real), pointer  :: data(:) => null()     ! /frequency - Frequency
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_ic_antennas_antenna_power  !    Power
  real(ids_real), pointer  :: data(:) => null()     ! /power - Power
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_ic_antennas_antenna  !    Ion Cyclotron Antenna
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the antenna (unique within the set of all antennas of the experiment)
  character(len=ids_string_length), dimension(:), pointer ::identifier => null()       ! /identifier - Identifier of the antenna (unique within the set of all antennas of the experiment)
  type (ids_ic_antennas_antenna_frequency) :: frequency  ! /frequency - Frequency
  type (ids_ic_antennas_antenna_power) :: power  ! /power - Power
  type (ids_ic_antennas_strap),pointer :: strap(:) => null()  ! /strap(i) - Properties of IC antenna straps
  type (ids_ic_antennas_surface_current),pointer :: surface_current(:) => null()  ! /surface_current(i) - Description of the IC surface current on the antenna straps and on passive components, for every tim
endtype

type ids_ic_antennas  !    Antenna systems for heating and current drive in the ion cylcotron (IC) frequencies.
  type (ids_ids_properties) :: ids_properties  ! /ic_antennas/ids_properties -
  type (ids_rz0d_constant) :: reference_point  ! /ic_antennas/reference_point - Reference point used to define the poloidal angle, e.g. the geometrical centre of the vacuum vessel.
  type (ids_ic_antennas_antenna),pointer :: antenna(:) => null()  ! /ic_antennas/antenna(i) - Set of Ion Cyclotron antennas
  type (ids_code) :: code  ! /ic_antennas/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include interferometer/dd_interferometer.xsd
! SPECIAL STRUCTURE data / time
type ids_interferometer_channel_wavelength_interf_phase_corrected  !    Phase measured for this wavelength, corrected from fringe jumps
  real(ids_real), pointer  :: data(:) => null()     ! /phase_corrected - Phase measured for this wavelength, corrected from fringe jumps
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_interferometer_channel_wavelength_interf  !    Value of the wavelength and density estimators associated to an interferometry wavelength
  real(ids_real)  :: value=ids_real_invalid       ! /value - Wavelength value
  real(ids_real)  :: value_error_upper=ids_real_invalid
  real(ids_real)  :: value_error_lower=ids_real_invalid
  integer(ids_int) :: value_error_index=ids_int_invalid

  type (ids_interferometer_channel_wavelength_interf_phase_corrected) :: phase_corrected  ! /phase_corrected - Phase measured for this wavelength, corrected from fringe jumps
  integer(ids_int),pointer  :: fring_jump_correction(:) => null()      ! /fring_jump_correction - Signed number of 2pi phase corrections applied to remove a fringe jump, for each time slice on which
  real(ids_real),pointer  :: fring_jump_correction_times(:) => null()     ! /fring_jump_correction_times - List of time slices of the pulse on which a fringe jump correction has been made
  real(ids_real),pointer  :: fring_jump_correction_times_error_upper(:) => null()
  real(ids_real),pointer  :: fring_jump_correction_times_error_lower(:) => null()
  integer(ids_int) :: fring_jump_correction_times_error_index=ids_int_invalid

  real(ids_real)  :: phase_to_n_e_line=ids_real_invalid       ! /phase_to_n_e_line - Conversion factor to be used to convert phase into line density for this wavelength
  real(ids_real)  :: phase_to_n_e_line_error_upper=ids_real_invalid
  real(ids_real)  :: phase_to_n_e_line_error_lower=ids_real_invalid
  integer(ids_int) :: phase_to_n_e_line_error_index=ids_int_invalid

endtype

! SPECIAL STRUCTURE data / time
type ids_interferometer_channel_path_length_variation  !    Optical path length variation due to the plasma
  real(ids_real), pointer  :: data(:) => null()     ! /path_length_variation - Optical path length variation due to the plasma
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_interferometer_channel_n_e_line  !    Line integrated density, possibly obtained by a combination of multiple interferometry wavelengths. Corresponds to the density int
  real(ids_real), pointer  :: data(:) => null()     ! /n_e_line - Line integrated density, possibly obtained by a combination of multiple interferometry wavelengths.
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_interferometer_channel_n_e_line_average  !    Line average density, possibly obtained by a combination of multiple interferometry wavelengths. Corresponds to the density integr
  real(ids_real), pointer  :: data(:) => null()     ! /n_e_line_average - Line average density, possibly obtained by a combination of multiple interferometry wavelengths. Cor
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_interferometer_channel  !    Charge exchange channel
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the channel
  character(len=ids_string_length), dimension(:), pointer ::identifier => null()       ! /identifier - ID of the channel
  type (ids_line_of_sight_3points) :: line_of_sight  ! /line_of_sight - Description of the line of sight of the channel, defined by two points when the beam is not reflecte
  type (ids_interferometer_channel_wavelength_interf),pointer :: wavelength(:) => null()  ! /wavelength(i) - Set of wavelengths used for interferometry
  type (ids_interferometer_channel_path_length_variation) :: path_length_variation  ! /path_length_variation - Optical path length variation due to the plasma
  type (ids_interferometer_channel_n_e_line) :: n_e_line  ! /n_e_line - Line integrated density, possibly obtained by a combination of multiple interferometry wavelengths.
  integer(ids_int)  :: n_e_line_validity=ids_int_invalid       ! /n_e_line_validity - Indicator of the validity of the n_e_line data for the whole acquisition period. 0: valid from autom
  type (ids_interferometer_channel_n_e_line_average) :: n_e_line_average  ! /n_e_line_average - Line average density, possibly obtained by a combination of multiple interferometry wavelengths. Cor
  integer(ids_int)  :: n_e_line_average_validity=ids_int_invalid       ! /n_e_line_average_validity - Indicator of the validity of the n_e_line_average data for the whole acquisition period. 0: valid fr
endtype

! SPECIAL STRUCTURE data / time
type ids_interferometer_n_e_volume_average  !    Volume average plasma density estimated from the line densities measured by the various channels
  real(ids_real), pointer  :: data(:) => null()     ! /interferometer/n_e_volume_average - Volume average plasma density estimated from the line densities measured by the various channels
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_interferometer  !    Interferometer diagnostic
  type (ids_ids_properties) :: ids_properties  ! /interferometer/ids_properties -
  type (ids_interferometer_channel),pointer :: channel(:) => null()  ! /interferometer/channel(i) - Set of channels (lines-of-sight)
  type (ids_interferometer_n_e_volume_average) :: n_e_volume_average  ! /interferometer/n_e_volume_average - Volume average plasma density estimated from the line densities measured by the various channels
  type (ids_code) :: code  ! /interferometer/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include iron_core/dd_iron_core.xsd
! SPECIAL STRUCTURE data / time
type ids_iron_core_segment_magnetisation_r  !    Magnetisation M of the iron segment along the major radius axis, assumed to be constant inside a given iron segment. Reminder : H
  real(ids_real), pointer  :: data(:) => null()     ! /magnetisation_r - Magnetisation M of the iron segment along the major radius axis, assumed to be constant inside a giv
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_iron_core_segment_magnetisation_z  !    Magnetisation M of the iron segment along the vertical axis, assumed to be constant inside a given iron segment. Reminder : H = 1/
  real(ids_real), pointer  :: data(:) => null()     ! /magnetisation_z - Magnetisation M of the iron segment along the vertical axis, assumed to be constant inside a given i
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_iron_core_segment  !    Segment of the iron core
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the segment
  character(len=ids_string_length), dimension(:), pointer ::identifier => null()       ! /identifier - ID of the segment
  real(ids_real),pointer  :: b_field(:) => null()     ! /b_field - Array of magnetic field values, for each of which the relative permeability is given
  real(ids_real),pointer  :: b_field_error_upper(:) => null()
  real(ids_real),pointer  :: b_field_error_lower(:) => null()
  integer(ids_int) :: b_field_error_index=ids_int_invalid

  real(ids_real),pointer  :: permeability_relative(:) => null()     ! /permeability_relative - Relative permeability of the iron segment
  real(ids_real),pointer  :: permeability_relative_error_upper(:) => null()
  real(ids_real),pointer  :: permeability_relative_error_lower(:) => null()
  integer(ids_int) :: permeability_relative_error_index=ids_int_invalid

  type (ids_outline_2d_geometry_static) :: geometry  ! /geometry - Cross-sectional shape of the segment
  type (ids_iron_core_segment_magnetisation_r) :: magnetisation_r  ! /magnetisation_r - Magnetisation M of the iron segment along the major radius axis, assumed to be constant inside a giv
  type (ids_iron_core_segment_magnetisation_z) :: magnetisation_z  ! /magnetisation_z - Magnetisation M of the iron segment along the vertical axis, assumed to be constant inside a given i
endtype

type ids_iron_core  !    Iron core description
  type (ids_ids_properties) :: ids_properties  ! /iron_core/ids_properties -
  type (ids_iron_core_segment),pointer :: segment(:) => null()  ! /iron_core/segment(i) - The iron core is describred as a set of segments
  type (ids_code) :: code  ! /iron_core/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include magnetics/dd_magnetics.xsd
! SPECIAL STRUCTURE data / time
type ids_magnetics_flux_loop_flux  !    Measured flux
  real(ids_real), pointer  :: data(:) => null()     ! /flux - Measured flux
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_magnetics_flux_loop  !    Flux loops
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the flux loop
  character(len=ids_string_length), dimension(:), pointer ::identifier => null()       ! /identifier - ID of the flux loop
  type (ids_rzphi0d_static),pointer :: position(:) => null()  ! /position(i) - List of (R,Z,phi) points defining the position of the loop (see data structure documentation FLUXLOO
  type (ids_magnetics_flux_loop_flux) :: flux  ! /flux - Measured flux
endtype

! SPECIAL STRUCTURE data / time
type ids_magnetics_bpol_probe_field  !    Measured magnetic field
  real(ids_real), pointer  :: data(:) => null()     ! /field - Measured magnetic field
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_magnetics_bpol_probe  !    Poloidal field probes
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the probe
  character(len=ids_string_length), dimension(:), pointer ::identifier => null()       ! /identifier - ID of the probe
  type (ids_rzphi0d_static) :: position  ! /position - R, Z, Phi position of the coil centre
  real(ids_real)  :: poloidal_angle=ids_real_invalid       ! /poloidal_angle - Poloidal angle of the coil orientation
  real(ids_real)  :: poloidal_angle_error_upper=ids_real_invalid
  real(ids_real)  :: poloidal_angle_error_lower=ids_real_invalid
  integer(ids_int) :: poloidal_angle_error_index=ids_int_invalid

  real(ids_real)  :: toroidal_angle=ids_real_invalid       ! /toroidal_angle - Toroidal angle of coil orientation (0 if fully in the poloidal plane)
  real(ids_real)  :: toroidal_angle_error_upper=ids_real_invalid
  real(ids_real)  :: toroidal_angle_error_lower=ids_real_invalid
  integer(ids_int) :: toroidal_angle_error_index=ids_int_invalid

  real(ids_real)  :: area=ids_real_invalid       ! /area - Area of each turn of the coil
  real(ids_real)  :: area_error_upper=ids_real_invalid
  real(ids_real)  :: area_error_lower=ids_real_invalid
  integer(ids_int) :: area_error_index=ids_int_invalid

  real(ids_real)  :: length=ids_real_invalid       ! /length - Length of the coil
  real(ids_real)  :: length_error_upper=ids_real_invalid
  real(ids_real)  :: length_error_lower=ids_real_invalid
  integer(ids_int) :: length_error_index=ids_int_invalid

  integer(ids_int)  :: turns=ids_int_invalid       ! /turns - Turns in the coil, including sign
  type (ids_magnetics_bpol_probe_field) :: field  ! /field - Measured magnetic field
endtype

! SPECIAL STRUCTURE data / time
type ids_magnetics_method_ip  !    Plasma current. Positive sign means anti-clockwise when viewed from above.
  real(ids_real), pointer  :: data(:) => null()     ! /ip - Plasma current. Positive sign means anti-clockwise when viewed from above.
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_magnetics_method_diamagnetic_flux  !    Diamagnetic flux
  real(ids_real), pointer  :: data(:) => null()     ! /diamagnetic_flux - Diamagnetic flux
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_magnetics_method  !    Processed quantities derived from the magnetic measurements, using various methods
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the data processing method
  type (ids_magnetics_method_ip) :: ip  ! /ip - Plasma current. Positive sign means anti-clockwise when viewed from above.
  type (ids_magnetics_method_diamagnetic_flux) :: diamagnetic_flux  ! /diamagnetic_flux - Diamagnetic flux
endtype

type ids_magnetics  !    Magnetic diagnostics for equilibrium identification and plasma shape control.
  type (ids_ids_properties) :: ids_properties  ! /magnetics/ids_properties -
  type (ids_magnetics_flux_loop),pointer :: flux_loop(:) => null()  ! /magnetics/flux_loop(i) - Flux loops; partial flux loops can be described
  type (ids_magnetics_bpol_probe),pointer :: bpol_probe(:) => null()  ! /magnetics/bpol_probe(i) - Poloidal field probes
  type (ids_magnetics_method),pointer :: method(:) => null()  ! /magnetics/method(i) - A method generating processed quantities derived from the magnetic measurements
  type (ids_code) :: code  ! /magnetics/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include mhd_linear/dd_mhd_linear.xsd
type ids_complex_2d_dynamic_aos_mhd_linear_vector  !    Structure (temporary) for real and imaginary part, while waiting for the implementation of complex numbers, dynamic within a type
  real(ids_real),pointer  :: real(:,:) => null()     ! /real - Real part
  real(ids_real),pointer  :: real_error_upper(:,:) => null()
  real(ids_real),pointer  :: real_error_lower(:,:) => null()
  integer(ids_int) :: real_error_index=ids_int_invalid

  real(ids_real),pointer  :: imaginary(:,:) => null()     ! /imaginary - Imaginary part
  real(ids_real),pointer  :: imaginary_error_upper(:,:) => null()
  real(ids_real),pointer  :: imaginary_error_lower(:,:) => null()
  integer(ids_int) :: imaginary_error_index=ids_int_invalid

endtype

type ids_complex_2d_dynamic_aos_mhd_scalar  !    Structure (temporary) for real and imaginary part, while waiting for the implementation of complex numbers, dynamic within a type
  real(ids_real),pointer  :: real(:,:) => null()     ! /real - Real part
  real(ids_real),pointer  :: real_error_upper(:,:) => null()
  real(ids_real),pointer  :: real_error_lower(:,:) => null()
  integer(ids_int) :: real_error_index=ids_int_invalid

  real(ids_real),pointer  :: imaginary(:,:) => null()     ! /imaginary - Imaginary part
  real(ids_real),pointer  :: imaginary_error_upper(:,:) => null()
  real(ids_real),pointer  :: imaginary_error_lower(:,:) => null()
  integer(ids_int) :: imaginary_error_index=ids_int_invalid

endtype

type ids_mhd_linear_vector  !    Vector structure for the MHD IDS
  type (ids_complex_2d_dynamic_aos_mhd_linear_vector) :: coordinate1  ! /coordinate1 - First coordinate (radial)
  type (ids_complex_2d_dynamic_aos_mhd_linear_vector) :: coordinate2  ! /coordinate2 - Second coordinate (poloidal)
  type (ids_complex_2d_dynamic_aos_mhd_linear_vector) :: coordinate3  ! /coordinate3 - Third coordinate (toroidal)
endtype

type ids_mhd_linear_time_slice_toroidal_mode_vacuum  !    MHD modes in the vacuum
  type (ids_identifier) :: grid_type  ! /grid_type - Selection of one of a set of grid types
  type (ids_equilibrium_profiles_2d_grid) :: grid  ! /grid - Definition of the 2D grid
  type (ids_mhd_linear_vector) :: a_field_perturbed  ! /a_field_perturbed - Pertubed vector potential for given toroidal mode number
  type (ids_mhd_linear_vector) :: b_field_perturbed  ! /b_field_perturbed - Pertubed magnetic field for given toroidal mode number
endtype

type ids_mhd_linear_time_slice_toroidal_mode_plasma  !    MHD modes in the confined plasma
  type (ids_identifier) :: grid_type  ! /grid_type - Selection of one of a set of grid types
  type (ids_equilibrium_profiles_2d_grid) :: grid  ! /grid - Definition of the 2D grid
  type (ids_complex_2d_dynamic_aos_mhd_scalar) :: displacement_perpendicular  ! /displacement_perpendicular - Perpendicular displacement of the modes
  type (ids_complex_2d_dynamic_aos_mhd_scalar) :: displacement_parallel  ! /displacement_parallel - Parallel displacement of the modes
  real(ids_real),pointer  :: tau_alfven(:) => null()     ! /tau_alfven - Alven time=R/vA=R0 sqrt(mi ni(rho))/B0
  real(ids_real),pointer  :: tau_alfven_error_upper(:) => null()
  real(ids_real),pointer  :: tau_alfven_error_lower(:) => null()
  integer(ids_int) :: tau_alfven_error_index=ids_int_invalid

  real(ids_real),pointer  :: tau_resistive(:) => null()     ! /tau_resistive - Resistive time = mu_0 rho*rho/1.22/eta_neo
  real(ids_real),pointer  :: tau_resistive_error_upper(:) => null()
  real(ids_real),pointer  :: tau_resistive_error_lower(:) => null()
  integer(ids_int) :: tau_resistive_error_index=ids_int_invalid

  type (ids_mhd_linear_vector) :: a_field_perturbed  ! /a_field_perturbed - Pertubed vector potential for given toroidal mode number
  type (ids_mhd_linear_vector) :: b_field_perturbed  ! /b_field_perturbed - Pertubed magnetic field for given toroidal mode number
  type (ids_mhd_linear_vector) :: velocity_perturbed  ! /velocity_perturbed - Pertubed velocity for given toroidal mode number
  type (ids_complex_2d_dynamic_aos_mhd_scalar) :: pressure_perturbed  ! /pressure_perturbed - Perturbed pressure for given toroidal mode number
  type (ids_complex_2d_dynamic_aos_mhd_scalar) :: mass_density_perturbed  ! /mass_density_perturbed - Perturbed mass density for given toroidal mode number
  type (ids_complex_2d_dynamic_aos_mhd_scalar) :: temperature_perturbed  ! /temperature_perturbed - Perturbed temperature for given toroidal mode number
endtype

type ids_mhd_linear_time_slice_toroidal_modes  !    Vector of toroidal modes
  integer(ids_int)  :: n_tor=ids_int_invalid       ! /n_tor - Toroidal mode number of the MHD mode
  real(ids_real)  :: growthrate=ids_real_invalid       ! /growthrate - Linear growthrate of the mode
  real(ids_real)  :: growthrate_error_upper=ids_real_invalid
  real(ids_real)  :: growthrate_error_lower=ids_real_invalid
  integer(ids_int) :: growthrate_error_index=ids_int_invalid

  real(ids_real)  :: frequency=ids_real_invalid       ! /frequency - Frequency of the mode
  real(ids_real)  :: frequency_error_upper=ids_real_invalid
  real(ids_real)  :: frequency_error_lower=ids_real_invalid
  integer(ids_int) :: frequency_error_index=ids_int_invalid

  type (ids_mhd_linear_time_slice_toroidal_mode_plasma) :: plasma  ! /plasma - MHD modes in the confined plasma
  type (ids_mhd_linear_time_slice_toroidal_mode_vacuum) :: vacuum  ! /vacuum - MHD modes in the vacuum
endtype

type ids_mhd_linear_time_slice  !    Time slice description of linear MHD stability
  type (ids_mhd_linear_time_slice_toroidal_modes),pointer :: toroidal_mode(:) => null()  ! /toroidal_mode(i) - Vector of toroidal modes
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_mhd_linear  !    Magnetohydronamic linear stability
  type (ids_ids_properties) :: ids_properties  ! /mhd_linear/ids_properties -
  type (ids_b_tor_vacuum_1) :: vacuum_toroidal_field  ! /mhd_linear/vacuum_toroidal_field - Characteristics of the vacuum toroidal field (used in rho_tor definition and in the normalization of
  type (ids_mhd_linear_time_slice),pointer :: time_slice(:) => null()  ! /mhd_linear/time_slice(i) - Core plasma radial profiles for various time slices
  type (ids_code) :: code  ! /mhd_linear/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include mse/dd_mse.xsd
type ids_mse_channel_resolution  !    In case of active spectroscopy, spatial resolution of the measurement
  type (ids_rzphi0d_dynamic_aos3) :: centre  ! /centre - Position of the centre of the spatially resolved zone
  type (ids_rzphi0d_dynamic_aos3) :: width  ! /width - Full width of the spatially resolved zone in the R, Z and phi directions
  real(ids_real),pointer  :: geometric_coefficients(:) => null()     ! /geometric_coefficients - Set of 9 geometric coefficients providing the MSE polarisation angle as a function of the local elec
  real(ids_real),pointer  :: geometric_coefficients_error_upper(:) => null()
  real(ids_real),pointer  :: geometric_coefficients_error_lower(:) => null()
  integer(ids_int) :: geometric_coefficients_error_index=ids_int_invalid

  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_mse_channel_intersection  !    MSE beam-los intersection
  real(ids_real)  :: r=ids_real_invalid       ! /r - Major radius
  real(ids_real)  :: r_error_upper=ids_real_invalid
  real(ids_real)  :: r_error_lower=ids_real_invalid
  integer(ids_int) :: r_error_index=ids_int_invalid

  real(ids_real)  :: z=ids_real_invalid       ! /z - Height
  real(ids_real)  :: z_error_upper=ids_real_invalid
  real(ids_real)  :: z_error_lower=ids_real_invalid
  integer(ids_int) :: z_error_index=ids_int_invalid

  real(ids_real)  :: phi=ids_real_invalid       ! /phi - Toroidal angle
  real(ids_real)  :: phi_error_upper=ids_real_invalid
  real(ids_real)  :: phi_error_lower=ids_real_invalid
  integer(ids_int) :: phi_error_index=ids_int_invalid

  real(ids_real)  :: delta_r=ids_real_invalid       ! /delta_r - Full width along major radius
  real(ids_real)  :: delta_r_error_upper=ids_real_invalid
  real(ids_real)  :: delta_r_error_lower=ids_real_invalid
  integer(ids_int) :: delta_r_error_index=ids_int_invalid

  real(ids_real)  :: delta_z=ids_real_invalid       ! /delta_z - Full width in height
  real(ids_real)  :: delta_z_error_upper=ids_real_invalid
  real(ids_real)  :: delta_z_error_lower=ids_real_invalid
  integer(ids_int) :: delta_z_error_index=ids_int_invalid

  real(ids_real)  :: delta_phi=ids_real_invalid       ! /delta_phi - Full width in toroidal angle
  real(ids_real)  :: delta_phi_error_upper=ids_real_invalid
  real(ids_real)  :: delta_phi_error_lower=ids_real_invalid
  integer(ids_int) :: delta_phi_error_index=ids_int_invalid

endtype

! SPECIAL STRUCTURE data / time
type ids_mse_channel_polarisation_angle  !    MSE polarisation angle
  real(ids_real), pointer  :: data(:) => null()     ! /polarisation_angle - MSE polarisation angle
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_mse_channel  !    MSE channel
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the channel
  type (ids_detector_aperture) :: detector  ! /detector - Detector description
  type (ids_detector_aperture),pointer :: aperture(:) => null()  ! /aperture(i) - Description of a set of collimating apertures
  type (ids_line_of_sight_2points) :: line_of_sight  ! /line_of_sight - Description of the line of sight of the channel, given by 2 points
  type (ids_mse_channel_resolution),pointer :: active_spatial_resolution(:) => null()  ! /active_spatial_resolution(i) - Spatial resolution of the measurement, calculated as a convolution of the atomic smearing, magnetic
  type (ids_mse_channel_polarisation_angle) :: polarisation_angle  ! /polarisation_angle - MSE polarisation angle
  integer(ids_int)  :: polarisation_angle_validity=ids_int_invalid       ! /polarisation_angle_validity - Indicator of the validity of the polarisation_angle data for the whole acquisition period. 0: valid
endtype

type ids_mse  !    Motional Stark Effect diagnostic
  type (ids_ids_properties) :: ids_properties  ! /mse/ids_properties -
  type (ids_mse_channel),pointer :: channel(:) => null()  ! /mse/channel(i) - Set of channels (lines of sight)
  type (ids_code) :: code  ! /mse/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include nbi/dd_nbi.xsd
type ids_nbi_unit_beamlets_group_tilting  !    Variation of position, tangency radius and angle in case of dynamic beam tilting, for a given time slice
  type (ids_rzphi0d_dynamic_aos3) :: delta_position  ! /delta_position - Variation of the position of the beamlet group centre
  real(ids_real)  :: delta_tangency_radius=ids_real_invalid       ! /delta_tangency_radius - Variation of the tangency radius (major radius where the central line of a NBI unit is tangent to a
  real(ids_real)  :: delta_tangency_radius_error_upper=ids_real_invalid
  real(ids_real)  :: delta_tangency_radius_error_lower=ids_real_invalid
  integer(ids_int) :: delta_tangency_radius_error_index=ids_int_invalid

  real(ids_real)  :: delta_angle=ids_real_invalid       ! /delta_angle - Variation of the angle of inclination between a beamlet at the centre of the injection unit surface
  real(ids_real)  :: delta_angle_error_upper=ids_real_invalid
  real(ids_real)  :: delta_angle_error_lower=ids_real_invalid
  integer(ids_int) :: delta_angle_error_index=ids_int_invalid

  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_nbi_unit_beamlets_group_beamlets  !    Detailed information on beamlets
  type (ids_rzphi1d_static) :: positions  ! /positions - Position of each beamlet
  real(ids_real),pointer  :: tangency_radii(:) => null()     ! /tangency_radii - Tangency radius (major radius where the central line of a beamlet is tangent to a circle around the
  real(ids_real),pointer  :: tangency_radii_error_upper(:) => null()
  real(ids_real),pointer  :: tangency_radii_error_lower(:) => null()
  integer(ids_int) :: tangency_radii_error_index=ids_int_invalid

  real(ids_real),pointer  :: angles(:) => null()     ! /angles - Angle of inclination between a line at the centre of a beamlet and the horiontal plane, for each bea
  real(ids_real),pointer  :: angles_error_upper(:) => null()
  real(ids_real),pointer  :: angles_error_lower(:) => null()
  integer(ids_int) :: angles_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_fractions(:) => null()     ! /power_fractions - Fraction of power of a unit injected by each beamlet
  real(ids_real),pointer  :: power_fractions_error_upper(:) => null()
  real(ids_real),pointer  :: power_fractions_error_lower(:) => null()
  integer(ids_int) :: power_fractions_error_index=ids_int_invalid

endtype

type ids_nbi_unit_beamlets_group_divergence  !    Describes a divergence component of a group of beamlets
  real(ids_real)  :: particles_fraction=ids_real_invalid       ! /particles_fraction - Fraction of injected particles in the component
  real(ids_real)  :: particles_fraction_error_upper=ids_real_invalid
  real(ids_real)  :: particles_fraction_error_lower=ids_real_invalid
  integer(ids_int) :: particles_fraction_error_index=ids_int_invalid

  real(ids_real)  :: vertical=ids_real_invalid       ! /vertical - The vertical beamlet divergence of the component. Here the divergence is defined for Gaussian beams
  real(ids_real)  :: vertical_error_upper=ids_real_invalid
  real(ids_real)  :: vertical_error_lower=ids_real_invalid
  integer(ids_int) :: vertical_error_index=ids_int_invalid

  real(ids_real)  :: horizontal=ids_real_invalid       ! /horizontal - The horiztonal beamlet divergence of the component. Here the divergence is defined for Gaussian beam
  real(ids_real)  :: horizontal_error_upper=ids_real_invalid
  real(ids_real)  :: horizontal_error_lower=ids_real_invalid
  integer(ids_int) :: horizontal_error_index=ids_int_invalid

endtype

type ids_nbi_unit_beamlets_group_focus  !    Describes of a group of beamlets is focused
  real(ids_real)  :: focal_length_horizontal=ids_real_invalid       ! /focal_length_horizontal - Horizontal focal length along the beam line, i.e. the point along the centre of the beamlet-group wh
  real(ids_real)  :: focal_length_horizontal_error_upper=ids_real_invalid
  real(ids_real)  :: focal_length_horizontal_error_lower=ids_real_invalid
  integer(ids_int) :: focal_length_horizontal_error_index=ids_int_invalid

  real(ids_real)  :: focal_length_vertical=ids_real_invalid       ! /focal_length_vertical - Vertical focal length along the beam line, i.e. the point along the centre of the beamlet-group wher
  real(ids_real)  :: focal_length_vertical_error_upper=ids_real_invalid
  real(ids_real)  :: focal_length_vertical_error_lower=ids_real_invalid
  integer(ids_int) :: focal_length_vertical_error_index=ids_int_invalid

  real(ids_real)  :: width_min_horizontal=ids_real_invalid       ! /width_min_horizontal - The horizontal width of the beamlets group at the at the horizontal focal point
  real(ids_real)  :: width_min_horizontal_error_upper=ids_real_invalid
  real(ids_real)  :: width_min_horizontal_error_lower=ids_real_invalid
  integer(ids_int) :: width_min_horizontal_error_index=ids_int_invalid

  real(ids_real)  :: width_min_vertical=ids_real_invalid       ! /width_min_vertical - The vertical width of the beamlets group at the at the vertical focal point
  real(ids_real)  :: width_min_vertical_error_upper=ids_real_invalid
  real(ids_real)  :: width_min_vertical_error_lower=ids_real_invalid
  integer(ids_int) :: width_min_vertical_error_index=ids_int_invalid

endtype

type ids_nbi_unit_beamlets_group  !    Group of beamlets
  type (ids_rzphi0d_static) :: position  ! /position - R, Z, Phi position of the beamlet group centre
  real(ids_real)  :: tangency_radius=ids_real_invalid       ! /tangency_radius - Tangency radius (major radius where the central line of a NBI unit is tangent to a circle around the
  real(ids_real)  :: tangency_radius_error_upper=ids_real_invalid
  real(ids_real)  :: tangency_radius_error_lower=ids_real_invalid
  integer(ids_int) :: tangency_radius_error_index=ids_int_invalid

  real(ids_real)  :: angle=ids_real_invalid       ! /angle - Angle of inclination between a beamlet at the centre of the injection unit surface and the horiontal
  real(ids_real)  :: angle_error_upper=ids_real_invalid
  real(ids_real)  :: angle_error_lower=ids_real_invalid
  integer(ids_int) :: angle_error_index=ids_int_invalid

  type (ids_nbi_unit_beamlets_group_tilting),pointer :: tilting(:) => null()  ! /tilting(i) - In case of dynamic beam tilting (i.e. during the pulse), e.g. for some Beam Emission Spectroscopy us
  integer(ids_int)  :: direction=ids_int_invalid       ! /direction - Direction of the beam seen from above the torus: -1 = clockwise; 1 = counter clockwise
  real(ids_real)  :: width_horizontal=ids_real_invalid       ! /width_horizontal - Horizontal width of the beam group at the injection unit surface (or grounded grid)
  real(ids_real)  :: width_horizontal_error_upper=ids_real_invalid
  real(ids_real)  :: width_horizontal_error_lower=ids_real_invalid
  integer(ids_int) :: width_horizontal_error_index=ids_int_invalid

  real(ids_real)  :: width_vertical=ids_real_invalid       ! /width_vertical - Vertical width of the beam group at the injection unit surface (or grounded grid)
  real(ids_real)  :: width_vertical_error_upper=ids_real_invalid
  real(ids_real)  :: width_vertical_error_lower=ids_real_invalid
  integer(ids_int) :: width_vertical_error_index=ids_int_invalid

  type (ids_nbi_unit_beamlets_group_focus) :: focus  ! /focus - Describes how the beamlet group is focused
  type (ids_nbi_unit_beamlets_group_divergence),pointer :: divergence_component(:) => null()  ! /divergence_component(i) - Detailed information on beamlet divergence. Divergence is described as a superposition of Gaussian c
  type (ids_nbi_unit_beamlets_group_beamlets) :: beamlets  ! /beamlets - Detailed information on beamlets
endtype

! SPECIAL STRUCTURE data / time
type ids_nbi_unit_power  !    Power
  real(ids_real), pointer  :: data(:) => null()     ! /power - Power
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_nbi_unit_energy  !    Full energy of the injected species (acceleration of a single atom)
  real(ids_real), pointer  :: data(:) => null()     ! /energy - Full energy of the injected species (acceleration of a single atom)
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_nbi_unit_beam_current_fraction  !    Fractions of beam current distributed among the different energies, the first index corresponds to the fast neutrals energy (1:ful
  real(ids_real), pointer  :: data(:,:) => null()     ! /beam_current_fraction - Fractions of beam current distributed among the different energies, the first index corresponds to t
  real(ids_real), pointer  :: data_error_upper(:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_nbi_unit_beam_power_fraction  !    Fractions of beam power distributed among the different energies, the first index corresponds to the fast neutrals energy (1:full,
  real(ids_real), pointer  :: data(:,:) => null()     ! /beam_power_fraction - Fractions of beam power distributed among the different energies, the first index corresponds to the
  real(ids_real), pointer  :: data_error_upper(:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_nbi_unit  !    NBI unit
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the NBI unit
  character(len=ids_string_length), dimension(:), pointer ::identifier => null()       ! /identifier - ID of the NBI unit
  type (ids_plasma_composition_species) :: species  ! /species - Injected species
  type (ids_nbi_unit_power) :: power  ! /power - Power
  type (ids_nbi_unit_energy) :: energy  ! /energy - Full energy of the injected species (acceleration of a single atom)
  type (ids_nbi_unit_beam_current_fraction) :: beam_current_fraction  ! /beam_current_fraction - Fractions of beam current distributed among the different energies, the first index corresponds to t
  type (ids_nbi_unit_beam_power_fraction) :: beam_power_fraction  ! /beam_power_fraction - Fractions of beam power distributed among the different energies, the first index corresponds to the
  type (ids_nbi_unit_beamlets_group),pointer :: beamlets_group(:) => null()  ! /beamlets_group(i) - Group of beamlets with common vertical and horizontal focal point. If there are no common focal poin
endtype

type ids_nbi  !    Neutral Beam Injection systems and description of the fast neutrals that arrive into the torus
  type (ids_ids_properties) :: ids_properties  ! /nbi/ids_properties -
  type (ids_nbi_unit),pointer :: unit(:) => null()  ! /nbi/unit(i) - The NBI system is described as a set of units of which the power can be controlled individually.
  type (ids_code) :: code  ! /nbi/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include ntms/dd_ntms.xsd
type ids_ntm_time_slice_mode_detailed_evolution_deltaw  !    deltaw contribution to the Rutherford equation (detailed evolution)
  real(ids_real),pointer  :: value(:) => null()     ! /value - Value of the contribution
  real(ids_real),pointer  :: value_error_upper(:) => null()
  real(ids_real),pointer  :: value_error_lower(:) => null()
  integer(ids_int) :: value_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the contribution
endtype

type ids_ntm_time_slice_mode_detailed_evolution_torque  !    torque contribution to the Rutherford equation (detailed evolution)
  real(ids_real),pointer  :: value(:) => null()     ! /value - Value of the contribution
  real(ids_real),pointer  :: value_error_upper(:) => null()
  real(ids_real),pointer  :: value_error_lower(:) => null()
  integer(ids_int) :: value_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the contribution
endtype

type ids_ntm_time_slice_mode_detailed_evolution  !    Detailed NTM evolution on a finer timebase than the time_slice array of structure
  real(ids_real),pointer  :: time_detailed(:) => null()     ! /time_detailed - Time array used to describe the detailed evolution of the NTM
  real(ids_real),pointer  :: time_detailed_error_upper(:) => null()
  real(ids_real),pointer  :: time_detailed_error_lower(:) => null()
  integer(ids_int) :: time_detailed_error_index=ids_int_invalid

  real(ids_real),pointer  :: width(:) => null()     ! /width - Full width of the mode
  real(ids_real),pointer  :: width_error_upper(:) => null()
  real(ids_real),pointer  :: width_error_lower(:) => null()
  integer(ids_int) :: width_error_index=ids_int_invalid

  real(ids_real),pointer  :: dwidth_dt(:) => null()     ! /dwidth_dt - Time derivative of the full width of the mode
  real(ids_real),pointer  :: dwidth_dt_error_upper(:) => null()
  real(ids_real),pointer  :: dwidth_dt_error_lower(:) => null()
  integer(ids_int) :: dwidth_dt_error_index=ids_int_invalid

  real(ids_real),pointer  :: phase(:) => null()     ! /phase - Phase of the mode
  real(ids_real),pointer  :: phase_error_upper(:) => null()
  real(ids_real),pointer  :: phase_error_lower(:) => null()
  integer(ids_int) :: phase_error_index=ids_int_invalid

  real(ids_real),pointer  :: dphase_dt(:) => null()     ! /dphase_dt - Time derivative of the phase of the mode
  real(ids_real),pointer  :: dphase_dt_error_upper(:) => null()
  real(ids_real),pointer  :: dphase_dt_error_lower(:) => null()
  integer(ids_int) :: dphase_dt_error_index=ids_int_invalid

  real(ids_real),pointer  :: frequency(:) => null()     ! /frequency - Frequency of the mode
  real(ids_real),pointer  :: frequency_error_upper(:) => null()
  real(ids_real),pointer  :: frequency_error_lower(:) => null()
  integer(ids_int) :: frequency_error_index=ids_int_invalid

  real(ids_real),pointer  :: dfrequency_dt(:) => null()     ! /dfrequency_dt - Time derivative of the frequency of the mode
  real(ids_real),pointer  :: dfrequency_dt_error_upper(:) => null()
  real(ids_real),pointer  :: dfrequency_dt_error_lower(:) => null()
  integer(ids_int) :: dfrequency_dt_error_index=ids_int_invalid

  integer(ids_int)  :: n_tor=ids_int_invalid       ! /n_tor - Toroidal mode number
  integer(ids_int)  :: m_pol=ids_int_invalid       ! /m_pol - Poloidal mode number
  type (ids_ntm_time_slice_mode_detailed_evolution_deltaw),pointer :: deltaw(:) => null()  ! /deltaw(i) - deltaw contributions to the Rutherford equation
  type (ids_ntm_time_slice_mode_detailed_evolution_torque),pointer :: torque(:) => null()  ! /torque(i) - torque contributions to the Rutherford equation
  character(len=ids_string_length), dimension(:), pointer ::calculation_method => null()       ! /calculation_method - Description of how the mode evolution is calculated
  real(ids_real),pointer  :: delta_diff(:,:) => null()     ! /delta_diff - Extra diffusion coefficient for the transport equations of Te, ne, Ti
  real(ids_real),pointer  :: delta_diff_error_upper(:,:) => null()
  real(ids_real),pointer  :: delta_diff_error_lower(:,:) => null()
  integer(ids_int) :: delta_diff_error_index=ids_int_invalid

  real(ids_real),pointer  :: rho_tor_norm(:) => null()     ! /rho_tor_norm - Normalised flux coordinate on which the mode is centred
  real(ids_real),pointer  :: rho_tor_norm_error_upper(:) => null()
  real(ids_real),pointer  :: rho_tor_norm_error_lower(:) => null()
  integer(ids_int) :: rho_tor_norm_error_index=ids_int_invalid

  real(ids_real),pointer  :: rho_tor(:) => null()     ! /rho_tor - Flux coordinate on which the mode is centred
  real(ids_real),pointer  :: rho_tor_error_upper(:) => null()
  real(ids_real),pointer  :: rho_tor_error_lower(:) => null()
  integer(ids_int) :: rho_tor_error_index=ids_int_invalid

endtype

type ids_ntm_time_slice_mode_evolution_deltaw  !    deltaw contribution to the Rutherford equation
  real(ids_real)  :: value=ids_real_invalid       ! /value - Value of the contribution
  real(ids_real)  :: value_error_upper=ids_real_invalid
  real(ids_real)  :: value_error_lower=ids_real_invalid
  integer(ids_int) :: value_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the contribution
endtype

type ids_ntm_time_slice_mode_evolution_torque  !    torque contribution to the Rutherford equation
  real(ids_real)  :: value=ids_real_invalid       ! /value - Value of the contribution
  real(ids_real)  :: value_error_upper=ids_real_invalid
  real(ids_real)  :: value_error_lower=ids_real_invalid
  integer(ids_int) :: value_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the contribution
endtype

type ids_ntm_time_slice_mode_onset  !    Onset characteristics of an NTM
  real(ids_real)  :: width=ids_real_invalid       ! /width - Seed island full width at onset time
  real(ids_real)  :: width_error_upper=ids_real_invalid
  real(ids_real)  :: width_error_lower=ids_real_invalid
  integer(ids_int) :: width_error_index=ids_int_invalid

  real(ids_real)  :: time_onset=ids_real_invalid       ! /time_onset - Onset time
  real(ids_real)  :: time_onset_error_upper=ids_real_invalid
  real(ids_real)  :: time_onset_error_lower=ids_real_invalid
  integer(ids_int) :: time_onset_error_index=ids_int_invalid

  real(ids_real)  :: time_offset=ids_real_invalid       ! /time_offset - Offset time (when a mode disappears). If the mode reappears later in the simulation, use another ind
  real(ids_real)  :: time_offset_error_upper=ids_real_invalid
  real(ids_real)  :: time_offset_error_lower=ids_real_invalid
  integer(ids_int) :: time_offset_error_index=ids_int_invalid

  real(ids_real)  :: phase=ids_real_invalid       ! /phase - Phase of the mode at onset
  real(ids_real)  :: phase_error_upper=ids_real_invalid
  real(ids_real)  :: phase_error_lower=ids_real_invalid
  integer(ids_int) :: phase_error_index=ids_int_invalid

  integer(ids_int)  :: n_tor=ids_int_invalid       ! /n_tor - Toroidal mode number
  integer(ids_int)  :: m_pol=ids_int_invalid       ! /m_pol - Poloidal mode number
  character(len=ids_string_length), dimension(:), pointer ::cause => null()       ! /cause - Cause of the mode onset
endtype

type ids_ntm_time_slice_mode  !    Description of an NTM
  type (ids_ntm_time_slice_mode_onset) :: onset  ! /onset - NTM onset characteristics
  real(ids_real)  :: width=ids_real_invalid       ! /width - Full width of the mode
  real(ids_real)  :: width_error_upper=ids_real_invalid
  real(ids_real)  :: width_error_lower=ids_real_invalid
  integer(ids_int) :: width_error_index=ids_int_invalid

  real(ids_real)  :: dwidth_dt=ids_real_invalid       ! /dwidth_dt - Time derivative of the full width of the mode
  real(ids_real)  :: dwidth_dt_error_upper=ids_real_invalid
  real(ids_real)  :: dwidth_dt_error_lower=ids_real_invalid
  integer(ids_int) :: dwidth_dt_error_index=ids_int_invalid

  real(ids_real)  :: phase=ids_real_invalid       ! /phase - Phase of the mode
  real(ids_real)  :: phase_error_upper=ids_real_invalid
  real(ids_real)  :: phase_error_lower=ids_real_invalid
  integer(ids_int) :: phase_error_index=ids_int_invalid

  real(ids_real)  :: dphase_dt=ids_real_invalid       ! /dphase_dt - Time derivative of the phase of the mode
  real(ids_real)  :: dphase_dt_error_upper=ids_real_invalid
  real(ids_real)  :: dphase_dt_error_lower=ids_real_invalid
  integer(ids_int) :: dphase_dt_error_index=ids_int_invalid

  real(ids_real)  :: frequency=ids_real_invalid       ! /frequency - Frequency of the mode
  real(ids_real)  :: frequency_error_upper=ids_real_invalid
  real(ids_real)  :: frequency_error_lower=ids_real_invalid
  integer(ids_int) :: frequency_error_index=ids_int_invalid

  real(ids_real)  :: dfrequency_dt=ids_real_invalid       ! /dfrequency_dt - Time derivative of the frequency of the mode
  real(ids_real)  :: dfrequency_dt_error_upper=ids_real_invalid
  real(ids_real)  :: dfrequency_dt_error_lower=ids_real_invalid
  integer(ids_int) :: dfrequency_dt_error_index=ids_int_invalid

  integer(ids_int)  :: n_tor=ids_int_invalid       ! /n_tor - Toroidal mode number
  integer(ids_int)  :: m_pol=ids_int_invalid       ! /m_pol - Poloidal mode number
  type (ids_ntm_time_slice_mode_evolution_deltaw),pointer :: deltaw(:) => null()  ! /deltaw(i) - deltaw contributions to the Rutherford equation
  type (ids_ntm_time_slice_mode_evolution_torque),pointer :: torque(:) => null()  ! /torque(i) - torque contributions to the Rutherford equation
  character(len=ids_string_length), dimension(:), pointer ::calculation_method => null()       ! /calculation_method - Description of how the mode evolution is calculated
  real(ids_real),pointer  :: delta_diff(:) => null()     ! /delta_diff - Extra diffusion coefficient for the transport equations of Te, ne, Ti
  real(ids_real),pointer  :: delta_diff_error_upper(:) => null()
  real(ids_real),pointer  :: delta_diff_error_lower(:) => null()
  integer(ids_int) :: delta_diff_error_index=ids_int_invalid

  real(ids_real)  :: rho_tor_norm=ids_real_invalid       ! /rho_tor_norm - Normalised flux coordinate on which the mode is centred
  real(ids_real)  :: rho_tor_norm_error_upper=ids_real_invalid
  real(ids_real)  :: rho_tor_norm_error_lower=ids_real_invalid
  integer(ids_int) :: rho_tor_norm_error_index=ids_int_invalid

  real(ids_real)  :: rho_tor=ids_real_invalid       ! /rho_tor - Flux coordinate on which the mode is centred
  real(ids_real)  :: rho_tor_error_upper=ids_real_invalid
  real(ids_real)  :: rho_tor_error_lower=ids_real_invalid
  integer(ids_int) :: rho_tor_error_index=ids_int_invalid

  type (ids_ntm_time_slice_mode_detailed_evolution) :: detailed_evolution  ! /detailed_evolution - Detailed NTM evolution on a finer timebase than the time_slice array of structure
endtype

type ids_ntm_time_slice  !    Time slice description of NTMs
  type (ids_ntm_time_slice_mode),pointer :: mode(:) => null()  ! /mode(i) - List of the various NTM modes appearing during the simulation. If a mode appears several times, use
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_ntms  !    Description of neoclassical tearing modes
  type (ids_ids_properties) :: ids_properties  ! /ntms/ids_properties -
  type (ids_b_tor_vacuum_1) :: vacuum_toroidal_field  ! /ntms/vacuum_toroidal_field - Characteristics of the vacuum toroidal field (used in rho_tor definition)
  type (ids_ntm_time_slice),pointer :: time_slice(:) => null()  ! /ntms/time_slice(i) - Description of neoclassical tearing modes for various time slices
  type (ids_code) :: code  ! /ntms/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include pellets/dd_pellets.xsd
type ids_pellets_time_slice_pellet_shape  !    Initial shape of a pellet at launch
  type (ids_identifier) :: type  ! /type - Identifier structure for the shape type: 1-spherical; 2-cylindrical; 3-rectangular
  real(ids_real),pointer  :: size(:) => null()     ! /size - Size of the pellet in the various dimensions, depending on the shape type. Spherical pellets: size(1
  real(ids_real),pointer  :: size_error_upper(:) => null()
  real(ids_real),pointer  :: size_error_lower(:) => null()
  integer(ids_int) :: size_error_index=ids_int_invalid

endtype

type ids_pellets_time_slice_pellet_species  !    Species included in pellet compoisition
  real(ids_real)  :: a=ids_real_invalid       ! /a - Mass of atom
  real(ids_real)  :: a_error_upper=ids_real_invalid
  real(ids_real)  :: a_error_lower=ids_real_invalid
  integer(ids_int) :: a_error_index=ids_int_invalid

  real(ids_real)  :: z_n=ids_real_invalid       ! /z_n - Nuclear charge
  real(ids_real)  :: z_n_error_upper=ids_real_invalid
  real(ids_real)  :: z_n_error_lower=ids_real_invalid
  integer(ids_int) :: z_n_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H, D, T, ...)
  real(ids_real)  :: density=ids_real_invalid       ! /density - Material density of the species in the pellet
  real(ids_real)  :: density_error_upper=ids_real_invalid
  real(ids_real)  :: density_error_lower=ids_real_invalid
  integer(ids_int) :: density_error_index=ids_int_invalid

  real(ids_real)  :: fraction=ids_real_invalid       ! /fraction - Fraction of the species atoms in the pellet
  real(ids_real)  :: fraction_error_upper=ids_real_invalid
  real(ids_real)  :: fraction_error_lower=ids_real_invalid
  integer(ids_int) :: fraction_error_index=ids_int_invalid

  real(ids_real)  :: sublimation_energy=ids_real_invalid       ! /sublimation_energy - Sublimation energy per atom
  real(ids_real)  :: sublimation_energy_error_upper=ids_real_invalid
  real(ids_real)  :: sublimation_energy_error_lower=ids_real_invalid
  integer(ids_int) :: sublimation_energy_error_index=ids_int_invalid

endtype

type ids_pellets_time_slice_pellet_path_profiles  !    1-D profiles of plasma and pellet along the pellet path
  real(ids_real),pointer  :: distance(:) => null()     ! /distance - Distance along the pellet path, with the origin taken at path_geometry/first_point. Used as the main
  real(ids_real),pointer  :: distance_error_upper(:) => null()
  real(ids_real),pointer  :: distance_error_lower(:) => null()
  integer(ids_int) :: distance_error_index=ids_int_invalid

  real(ids_real),pointer  :: rho_tor_norm(:) => null()     ! /rho_tor_norm - Normalised toroidal coordinate along the pellet path
  real(ids_real),pointer  :: rho_tor_norm_error_upper(:) => null()
  real(ids_real),pointer  :: rho_tor_norm_error_lower(:) => null()
  integer(ids_int) :: rho_tor_norm_error_index=ids_int_invalid

  real(ids_real),pointer  :: psi(:) => null()     ! /psi - Poloidal flux along the pellet path
  real(ids_real),pointer  :: psi_error_upper(:) => null()
  real(ids_real),pointer  :: psi_error_lower(:) => null()
  integer(ids_int) :: psi_error_index=ids_int_invalid

  real(ids_real),pointer  :: velocity(:) => null()     ! /velocity - Pellet velocity along the pellet path
  real(ids_real),pointer  :: velocity_error_upper(:) => null()
  real(ids_real),pointer  :: velocity_error_lower(:) => null()
  integer(ids_int) :: velocity_error_index=ids_int_invalid

  real(ids_real),pointer  :: n_e(:) => null()     ! /n_e - Electron density along the pellet path
  real(ids_real),pointer  :: n_e_error_upper(:) => null()
  real(ids_real),pointer  :: n_e_error_lower(:) => null()
  integer(ids_int) :: n_e_error_index=ids_int_invalid

  real(ids_real),pointer  :: t_e(:) => null()     ! /t_e - Electron temperature along the pellet path
  real(ids_real),pointer  :: t_e_error_upper(:) => null()
  real(ids_real),pointer  :: t_e_error_lower(:) => null()
  integer(ids_int) :: t_e_error_index=ids_int_invalid

  real(ids_real),pointer  :: ablation_rate(:) => null()     ! /ablation_rate - Ablation rate (electrons) along the pellet path
  real(ids_real),pointer  :: ablation_rate_error_upper(:) => null()
  real(ids_real),pointer  :: ablation_rate_error_lower(:) => null()
  integer(ids_int) :: ablation_rate_error_index=ids_int_invalid

  real(ids_real),pointer  :: ablated_particles(:) => null()     ! /ablated_particles - Number of ablated particles (electrons) along the pellet path
  real(ids_real),pointer  :: ablated_particles_error_upper(:) => null()
  real(ids_real),pointer  :: ablated_particles_error_lower(:) => null()
  integer(ids_int) :: ablated_particles_error_index=ids_int_invalid

  real(ids_real),pointer  :: rho_tor_norm_drift(:) => null()     ! /rho_tor_norm_drift - Difference to due ExB drifts between the ablation and the final deposition locations, in terms of th
  real(ids_real),pointer  :: rho_tor_norm_drift_error_upper(:) => null()
  real(ids_real),pointer  :: rho_tor_norm_drift_error_lower(:) => null()
  integer(ids_int) :: rho_tor_norm_drift_error_index=ids_int_invalid

  type (ids_rzphi1d_dynamic_aos3) :: position  ! /position - Position along the pellet path
endtype

type ids_pellets_time_slice_pellet  !    Description of a pellet
  type (ids_pellets_time_slice_pellet_shape) :: shape  ! /shape - Initial shape of a pellet at launch
  type (ids_pellets_time_slice_pellet_species),pointer :: species(:) => null()  ! /species(i) - Set of species included in the pellet composition
  real(ids_real)  :: velocity_initial=ids_real_invalid       ! /velocity_initial - Initial velocity of the pellet as it enters the vaccum chamber
  real(ids_real)  :: velocity_initial_error_upper=ids_real_invalid
  real(ids_real)  :: velocity_initial_error_lower=ids_real_invalid
  integer(ids_int) :: velocity_initial_error_index=ids_int_invalid

  type (ids_line_of_sight_2points_dynamic_aos3) :: path_geometry  ! /path_geometry - Geometry of the pellet path in the vaccuum chamber
  type (ids_pellets_time_slice_pellet_path_profiles) :: path_profiles  ! /path_profiles - 1-D profiles of plasma and pellet along the pellet path
endtype

type ids_pellets_time_slice  !    Time slice description of pellets
  type (ids_pellets_time_slice_pellet),pointer :: pellet(:) => null()  ! /pellet(i) - Set of pellets ablated in the plasma at a given time
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_pellets  !    Description of pellets launched into the plasma
  type (ids_ids_properties) :: ids_properties  ! /pellets/ids_properties -
  type (ids_pellets_time_slice),pointer :: time_slice(:) => null()  ! /pellets/time_slice(i) - Description of the pellets launched at various time slices. The time of this structure corresponds t
  type (ids_code) :: code  ! /pellets/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include pf_active/dd_pf_active.xsd
! SPECIAL STRUCTURE data / time
type ids_pf_supplies_voltage  !    Voltage at the supply output
  real(ids_real), pointer  :: data(:) => null()     ! /voltage - Voltage at the supply output
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_pf_supplies_current  !    Current at the supply output, defined positive if it flows from point 1 to point 2 of the component in the pfcircuit description
  real(ids_real), pointer  :: data(:) => null()     ! /current - Current at the supply output, defined positive if it flows from point 1 to point 2 of the component
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_pf_supplies  !    PF power supplies
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the PF supply
  character(len=ids_string_length), dimension(:), pointer ::identifier => null()       ! /identifier - Identifier of the supply
  integer(ids_int)  :: type=ids_int_invalid       ! /type - Type of the supply; TBD add free description of non-linear power supplies
  real(ids_real)  :: resistance=ids_real_invalid       ! /resistance - Power supply internal resistance
  real(ids_real)  :: resistance_error_upper=ids_real_invalid
  real(ids_real)  :: resistance_error_lower=ids_real_invalid
  integer(ids_int) :: resistance_error_index=ids_int_invalid

  real(ids_real)  :: delay=ids_real_invalid       ! /delay - Pure delay in the supply
  real(ids_real)  :: delay_error_upper=ids_real_invalid
  real(ids_real)  :: delay_error_lower=ids_real_invalid
  integer(ids_int) :: delay_error_index=ids_int_invalid

  real(ids_real),pointer  :: filter_numerator(:) => null()     ! /filter_numerator - Coefficients of the numerator, in increasing order : a0 + a1*s + ... + an*s^n; used for a linear sup
  real(ids_real),pointer  :: filter_numerator_error_upper(:) => null()
  real(ids_real),pointer  :: filter_numerator_error_lower(:) => null()
  integer(ids_int) :: filter_numerator_error_index=ids_int_invalid

  real(ids_real),pointer  :: filter_denominator(:) => null()     ! /filter_denominator - Coefficients of the denominator, in increasing order : b0 + b1*s + ... + bm*s^m; used for a linear s
  real(ids_real),pointer  :: filter_denominator_error_upper(:) => null()
  real(ids_real),pointer  :: filter_denominator_error_lower(:) => null()
  integer(ids_int) :: filter_denominator_error_index=ids_int_invalid

  real(ids_real)  :: current_limit_max=ids_real_invalid       ! /current_limit_max - Maximum current in the supply
  real(ids_real)  :: current_limit_max_error_upper=ids_real_invalid
  real(ids_real)  :: current_limit_max_error_lower=ids_real_invalid
  integer(ids_int) :: current_limit_max_error_index=ids_int_invalid

  real(ids_real)  :: current_limit_min=ids_real_invalid       ! /current_limit_min - Minimum current in the supply
  real(ids_real)  :: current_limit_min_error_upper=ids_real_invalid
  real(ids_real)  :: current_limit_min_error_lower=ids_real_invalid
  integer(ids_int) :: current_limit_min_error_index=ids_int_invalid

  real(ids_real)  :: voltage_limit_max=ids_real_invalid       ! /voltage_limit_max - Maximum voltage from the supply
  real(ids_real)  :: voltage_limit_max_error_upper=ids_real_invalid
  real(ids_real)  :: voltage_limit_max_error_lower=ids_real_invalid
  integer(ids_int) :: voltage_limit_max_error_index=ids_int_invalid

  real(ids_real)  :: voltage_limit_min=ids_real_invalid       ! /voltage_limit_min - Minimum voltage from the supply
  real(ids_real)  :: voltage_limit_min_error_upper=ids_real_invalid
  real(ids_real)  :: voltage_limit_min_error_lower=ids_real_invalid
  integer(ids_int) :: voltage_limit_min_error_index=ids_int_invalid

  real(ids_real)  :: current_limiter_gain=ids_real_invalid       ! /current_limiter_gain - Gain to prevent overcurrent in a linear model of the supply
  real(ids_real)  :: current_limiter_gain_error_upper=ids_real_invalid
  real(ids_real)  :: current_limiter_gain_error_lower=ids_real_invalid
  integer(ids_int) :: current_limiter_gain_error_index=ids_int_invalid

  real(ids_real)  :: energy_limit_max=ids_real_invalid       ! /energy_limit_max - Maximum energy to be dissipated in the supply during a pulse
  real(ids_real)  :: energy_limit_max_error_upper=ids_real_invalid
  real(ids_real)  :: energy_limit_max_error_lower=ids_real_invalid
  integer(ids_int) :: energy_limit_max_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::nonlinear_model => null()       ! /nonlinear_model - Description of the nonlinear transfer function of the supply
  type (ids_pf_supplies_voltage) :: voltage  ! /voltage - Voltage at the supply output
  type (ids_pf_supplies_current) :: current  ! /current - Current at the supply output, defined positive if it flows from point 1 to point 2 of the component
endtype

! SPECIAL STRUCTURE data / time
type ids_pf_circuits_voltage  !    Voltage on the circuit
  real(ids_real), pointer  :: data(:) => null()     ! /voltage - Voltage on the circuit
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_pf_circuits_current  !    Current in the circuit
  real(ids_real), pointer  :: data(:) => null()     ! /current - Current in the circuit
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_pf_circuits  !    Circuits, connecting multiple PF coils to multiple supplies, defining the current and voltage relationships in the system
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the circuit
  character(len=ids_string_length), dimension(:), pointer ::identifier => null()       ! /identifier - ID of the circuit
  character(len=ids_string_length), dimension(:), pointer ::type => null()       ! /type - Type of the circuit
  integer(ids_int),pointer  :: connections(:,:) => null()     ! /connections - Description of the supplies and coils connections (nodes) across the circuit. Nodes of the circuit a
  type (ids_pf_circuits_voltage) :: voltage  ! /voltage - Voltage on the circuit
  type (ids_pf_circuits_current) :: current  ! /current - Current in the circuit
endtype

type ids_pf_coils_elements  !    Each PF coil is comprised of a number of cross-section elements described  individually
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of this element
  character(len=ids_string_length), dimension(:), pointer ::identifier => null()       ! /identifier - Identifier of this element
  real(ids_real)  :: turns_with_sign=ids_real_invalid       ! /turns_with_sign - Number of effective turns in the element for calculating magnetic fields of the coil/loop; includes
  real(ids_real)  :: turns_with_sign_error_upper=ids_real_invalid
  real(ids_real)  :: turns_with_sign_error_lower=ids_real_invalid
  integer(ids_int) :: turns_with_sign_error_index=ids_int_invalid

  real(ids_real)  :: area=ids_real_invalid       ! /area - Cross-sectional areas of the element
  real(ids_real)  :: area_error_upper=ids_real_invalid
  real(ids_real)  :: area_error_lower=ids_real_invalid
  integer(ids_int) :: area_error_index=ids_int_invalid

  type (ids_outline_2d_geometry_static) :: geometry  ! /geometry - Cross-sectional shape of the element
endtype

! SPECIAL STRUCTURE data / time
type ids_pf_coils_current  !    Current in the coil
  real(ids_real), pointer  :: data(:) => null()     ! /current - Current in the coil
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_pf_coils_voltage  !    Voltage on the coil terminals
  real(ids_real), pointer  :: data(:) => null()     ! /voltage - Voltage on the coil terminals
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_pf_coils  !    Active PF coils
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the coil
  character(len=ids_string_length), dimension(:), pointer ::identifier => null()       ! /identifier - Alphanumeric identifier of coils used for convenience
  real(ids_real)  :: resistance=ids_real_invalid       ! /resistance - Coil resistance
  real(ids_real)  :: resistance_error_upper=ids_real_invalid
  real(ids_real)  :: resistance_error_lower=ids_real_invalid
  integer(ids_int) :: resistance_error_index=ids_int_invalid

  real(ids_real)  :: energy_limit_max=ids_real_invalid       ! /energy_limit_max - Maximum Energy to be dissipated in the coil
  real(ids_real)  :: energy_limit_max_error_upper=ids_real_invalid
  real(ids_real)  :: energy_limit_max_error_lower=ids_real_invalid
  integer(ids_int) :: energy_limit_max_error_index=ids_int_invalid

  real(ids_real),pointer  :: current_limit_max(:,:) => null()     ! /current_limit_max - Maximum tolerable current in the conductor
  real(ids_real),pointer  :: current_limit_max_error_upper(:,:) => null()
  real(ids_real),pointer  :: current_limit_max_error_lower(:,:) => null()
  integer(ids_int) :: current_limit_max_error_index=ids_int_invalid

  real(ids_real),pointer  :: b_field_max(:) => null()     ! /b_field_max - List of values of the maximum magnetic field on the conductor surface (coordinate for current_limit_
  real(ids_real),pointer  :: b_field_max_error_upper(:) => null()
  real(ids_real),pointer  :: b_field_max_error_lower(:) => null()
  integer(ids_int) :: b_field_max_error_index=ids_int_invalid

  real(ids_real),pointer  :: temperature(:) => null()     ! /temperature - List of values of the conductor temperature (coordinate for current_limit_max)
  real(ids_real),pointer  :: temperature_error_upper(:) => null()
  real(ids_real),pointer  :: temperature_error_lower(:) => null()
  integer(ids_int) :: temperature_error_index=ids_int_invalid

  type (ids_pf_coils_elements),pointer :: element(:) => null()  ! /element(i) - Each PF coil is comprised of a number of cross-section elements described  individually
  type (ids_pf_coils_current) :: current  ! /current - Current in the coil
  type (ids_pf_coils_voltage) :: voltage  ! /voltage - Voltage on the coil terminals
endtype

! SPECIAL STRUCTURE data / time
type ids_pf_vertical_forces_force  !    Force
  real(ids_real), pointer  :: data(:) => null()     ! /force - Force
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_pf_vertical_forces  !    Vertical forces on the axisymmetric PF+CS coil system
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the force combination
  real(ids_real),pointer  :: combination(:) => null()     ! /combination - Coils involved in the force combinations. Normally the vertical force would be the full set of coils
  real(ids_real),pointer  :: combination_error_upper(:) => null()
  real(ids_real),pointer  :: combination_error_lower(:) => null()
  integer(ids_int) :: combination_error_index=ids_int_invalid

  real(ids_real)  :: limit_max=ids_real_invalid       ! /limit_max - Vertical force combination limit
  real(ids_real)  :: limit_max_error_upper=ids_real_invalid
  real(ids_real)  :: limit_max_error_lower=ids_real_invalid
  integer(ids_int) :: limit_max_error_index=ids_int_invalid

  real(ids_real)  :: limit_min=ids_real_invalid       ! /limit_min - Vertical force combination limit
  real(ids_real)  :: limit_min_error_upper=ids_real_invalid
  real(ids_real)  :: limit_min_error_lower=ids_real_invalid
  integer(ids_int) :: limit_min_error_index=ids_int_invalid

  type (ids_pf_vertical_forces_force) :: force  ! /force - Force
endtype

type ids_pf_active  !    Description of the axisymmetric active poloidal field (PF) coils and supplies; includes the limits of these systems; includes the
  type (ids_ids_properties) :: ids_properties  ! /pf_active/ids_properties -
  type (ids_pf_coils),pointer :: coil(:) => null()  ! /pf_active/coil(i) - Active PF coils
  type (ids_pf_vertical_forces),pointer :: vertical_force(:) => null()  ! /pf_active/vertical_force(i) - Vertical forces on the axisymmetric PF coil system
  type (ids_pf_circuits),pointer :: circuit(:) => null()  ! /pf_active/circuit(i) - Circuits, connecting multiple PF coils to multiple supplies, defining the current and voltage relati
  type (ids_pf_supplies),pointer :: supply(:) => null()  ! /pf_active/supply(i) - PF power supplies
  type (ids_code) :: code  ! /pf_active/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include pf_passive/dd_pf_passive.xsd
type ids_pf_passive_loops  !    Passive axisymmetric conductor description in the form of non-connected loops; any connected loops are expressed as active coil ci
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the loop
  real(ids_real)  :: resistance=ids_real_invalid       ! /resistance - Passive loop resistance
  real(ids_real)  :: resistance_error_upper=ids_real_invalid
  real(ids_real)  :: resistance_error_lower=ids_real_invalid
  integer(ids_int) :: resistance_error_index=ids_int_invalid

  real(ids_real)  :: resistivity=ids_real_invalid       ! /resistivity - Passive loop resistivity
  real(ids_real)  :: resistivity_error_upper=ids_real_invalid
  real(ids_real)  :: resistivity_error_lower=ids_real_invalid
  integer(ids_int) :: resistivity_error_index=ids_int_invalid

  type (ids_pf_coils_elements),pointer :: element(:) => null()  ! /element(i) - Each loop is comprised of a number of cross-section elements described  individually
  real(ids_real),pointer  :: current(:) => null()     ! /current - Passive loop current
  real(ids_real),pointer  :: current_error_upper(:) => null()
  real(ids_real),pointer  :: current_error_lower(:) => null()
  integer(ids_int) :: current_error_index=ids_int_invalid

endtype

type ids_pf_passive  !    Description of the axisymmetric passive conductors, currents flowing in them
  type (ids_ids_properties) :: ids_properties  ! /pf_passive/ids_properties -
  type (ids_pf_passive_loops),pointer :: loop(:) => null()  ! /pf_passive/loop(i) - Passive axisymmetric conductor description in the form of non-connected loops; any connected loops a
  type (ids_code) :: code  ! /pf_passive/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include polarimeter/dd_polarimeter.xsd
! SPECIAL STRUCTURE data / time
type ids_polarimeter_channel_wavelength_interf_phase_corrected  !    Phase measured for this wavelength, corrected from fringe jumps
  real(ids_real), pointer  :: data(:) => null()     ! /phase_corrected - Phase measured for this wavelength, corrected from fringe jumps
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_polarimeter_channel_wavelength_interf  !    Value of the wavelength and density estimators associated to an interferometry wavelength
  real(ids_real)  :: value=ids_real_invalid       ! /value - Wavelength value
  real(ids_real)  :: value_error_upper=ids_real_invalid
  real(ids_real)  :: value_error_lower=ids_real_invalid
  integer(ids_int) :: value_error_index=ids_int_invalid

  type (ids_polarimeter_channel_wavelength_interf_phase_corrected) :: phase_corrected  ! /phase_corrected - Phase measured for this wavelength, corrected from fringe jumps
  integer(ids_int),pointer  :: fring_jump_correction(:) => null()      ! /fring_jump_correction - Signed number of 2pi phase corrections applied to remove a fringe jump, for each time slice on which
  real(ids_real),pointer  :: fring_jump_correction_times(:) => null()     ! /fring_jump_correction_times - List of time slices of the pulse on which a fringe jump correction has been made
  real(ids_real),pointer  :: fring_jump_correction_times_error_upper(:) => null()
  real(ids_real),pointer  :: fring_jump_correction_times_error_lower(:) => null()
  integer(ids_int) :: fring_jump_correction_times_error_index=ids_int_invalid

  real(ids_real)  :: phase_to_n_e_line=ids_real_invalid       ! /phase_to_n_e_line - Conversion factor to be used to convert phase into line density for this wavelength
  real(ids_real)  :: phase_to_n_e_line_error_upper=ids_real_invalid
  real(ids_real)  :: phase_to_n_e_line_error_lower=ids_real_invalid
  integer(ids_int) :: phase_to_n_e_line_error_index=ids_int_invalid

endtype

! SPECIAL STRUCTURE data / time
type ids_polarimeter_channel_faraday_angle  !    Faraday angle (variation of the Faraday angle induced by crossing the plasma)
  real(ids_real), pointer  :: data(:) => null()     ! /faraday_angle - Faraday angle (variation of the Faraday angle induced by crossing the plasma)
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_polarimeter_channel_ellipticity  !    Ellipticity
  real(ids_real), pointer  :: data(:) => null()     ! /ellipticity - Ellipticity
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_polarimeter_channel  !    Charge exchange channel
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the channel
  character(len=ids_string_length), dimension(:), pointer ::identifier => null()       ! /identifier - ID of the channel
  type (ids_line_of_sight_3points) :: line_of_sight  ! /line_of_sight - Description of the line of sight of the channel, defined by two points when the beam is not reflecte
  real(ids_real)  :: wavelength=ids_real_invalid       ! /wavelength - Wavelength used for polarimetry
  real(ids_real)  :: wavelength_error_upper=ids_real_invalid
  real(ids_real)  :: wavelength_error_lower=ids_real_invalid
  integer(ids_int) :: wavelength_error_index=ids_int_invalid

  real(ids_real)  :: polarisation_initial=ids_real_invalid       ! /polarisation_initial - Initial polarisation vector before entering the plasma
  real(ids_real)  :: polarisation_initial_error_upper=ids_real_invalid
  real(ids_real)  :: polarisation_initial_error_lower=ids_real_invalid
  integer(ids_int) :: polarisation_initial_error_index=ids_int_invalid

  real(ids_real)  :: ellipticity_initial=ids_real_invalid       ! /ellipticity_initial - Initial ellipticity before entering the plasma
  real(ids_real)  :: ellipticity_initial_error_upper=ids_real_invalid
  real(ids_real)  :: ellipticity_initial_error_lower=ids_real_invalid
  integer(ids_int) :: ellipticity_initial_error_index=ids_int_invalid

  type (ids_polarimeter_channel_faraday_angle) :: faraday_angle  ! /faraday_angle - Faraday angle (variation of the Faraday angle induced by crossing the plasma)
  integer(ids_int)  :: faraday_angle_validity=ids_int_invalid       ! /faraday_angle_validity - Indicator of the validity of the faraday_angle data for the whole acquisition period. 0: valid from
  type (ids_polarimeter_channel_ellipticity) :: ellipticity  ! /ellipticity - Ellipticity
  integer(ids_int)  :: ellipticity_validity=ids_int_invalid       ! /ellipticity_validity - Indicator of the validity of the ellipticity data for the whole acquisition period. 0: valid from au
endtype

type ids_polarimeter  !    Polarimeter diagnostic
  type (ids_ids_properties) :: ids_properties  ! /polarimeter/ids_properties -
  type (ids_polarimeter_channel),pointer :: channel(:) => null()  ! /polarimeter/channel(i) - Set of channels (lines-of-sight)
  type (ids_code) :: code  ! /polarimeter/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include pulse_schedule/dd_pulse_schedule.xsd
! SPECIAL STRUCTURE data / time
type ids_pulse_schedule_reference_reference  !    Reference waveform
  real(ids_real), pointer  :: data(:) => null()     ! /reference - Reference waveform
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_pulse_schedule_reference  !    General structure for pulse schedule reference. NB: we propose to use the automatically generated reference/data_error_upper and r
  character(len=ids_string_length), dimension(:), pointer ::reference_name => null()       ! /reference_name - Reference name (e.g. in the native pulse schedule system of the device)
  type (ids_pulse_schedule_reference_reference) :: reference  ! /reference - Reference waveform
  integer(ids_int)  :: reference_type=ids_int_invalid       ! /reference_type - Reference type:  0:relative; 1: absolute; refers to the reference/data node
  integer(ids_int)  :: envelope_type=ids_int_invalid       ! /envelope_type - Envelope type:  0:relative; 1: absolute; refers to the envelope bounds which are given by the refere
endtype

type ids_pulse_schedule_rz  !    R,Z position
  type (ids_pulse_schedule_reference) :: r  ! /r - Major radius
  type (ids_pulse_schedule_reference) :: z  ! /z - Height
endtype

type ids_pulse_schedule_gap  !    Gap for describing the plasma boundary
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the gap
  character(len=ids_string_length), dimension(:), pointer ::identifier => null()       ! /identifier - Identifier of the gap
  real(ids_real)  :: r=ids_real_invalid       ! /r - Major radius of the reference point
  real(ids_real)  :: r_error_upper=ids_real_invalid
  real(ids_real)  :: r_error_lower=ids_real_invalid
  integer(ids_int) :: r_error_index=ids_int_invalid

  real(ids_real)  :: z=ids_real_invalid       ! /z - Height of the reference point
  real(ids_real)  :: z_error_upper=ids_real_invalid
  real(ids_real)  :: z_error_lower=ids_real_invalid
  integer(ids_int) :: z_error_index=ids_int_invalid

  real(ids_real)  :: angle=ids_real_invalid       ! /angle - Angle between the direction in which the gap is measured (in the poloidal cross-section) and the hor
  real(ids_real)  :: angle_error_upper=ids_real_invalid
  real(ids_real)  :: angle_error_lower=ids_real_invalid
  integer(ids_int) :: angle_error_index=ids_int_invalid

  type (ids_pulse_schedule_reference) :: value  ! /value - Value of the gap, i.e. distance between the reference point and the separatrix along the gap directi
endtype

type ids_pulse_schedule_outline  !    RZ outline
  type (ids_pulse_schedule_reference) :: r  ! /r - Major radius
  type (ids_pulse_schedule_reference) :: z  ! /z - Height
endtype

type ids_pulse_schedule_ic_antenna  !    IC antenna
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the antenna
  character(len=ids_string_length), dimension(:), pointer ::identifier => null()       ! /identifier - Identifier of the antenna
  type (ids_pulse_schedule_reference) :: power  ! /power - Power
  type (ids_pulse_schedule_reference) :: phase  ! /phase - Phase
  type (ids_pulse_schedule_reference) :: frequency  ! /frequency - Frequency
endtype

type ids_pulse_schedule_ec_antenna  !    EC antenna
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the antenna
  character(len=ids_string_length), dimension(:), pointer ::identifier => null()       ! /identifier - Identifier of the antenna
  type (ids_pulse_schedule_reference) :: power  ! /power - Power
  type (ids_pulse_schedule_reference) :: phase  ! /phase - Phase
  type (ids_pulse_schedule_reference) :: frequency  ! /frequency - Frequency
  type (ids_pulse_schedule_reference) :: deposition_rho_tor_norm  ! /deposition_rho_tor_norm - Normalised toroidal flux coordinate at which the main deposition should occur
  type (ids_pulse_schedule_reference) :: launching_angle_pol  ! /launching_angle_pol - Poloidal launching angle between the horizontal plane and the poloidal component of the nominal beam
  type (ids_pulse_schedule_reference) :: launching_angle_tor  ! /launching_angle_tor - Toroidal launching angle between the poloidal plane and the nominal beam centerline. sin(angle_tor)=
endtype

type ids_pulse_schedule_lh_antenna  !    LH antenna
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the antenna
  character(len=ids_string_length), dimension(:), pointer ::identifier => null()       ! /identifier - Identifier of the antenna
  type (ids_pulse_schedule_reference) :: power  ! /power - Power
  type (ids_pulse_schedule_reference) :: phase  ! /phase - Phasing between neighbour waveguides (in the toroidal direction)
  type (ids_pulse_schedule_reference) :: n_parallel  ! /n_parallel - Main parallel refractive index of the injected wave power spectrum
  type (ids_pulse_schedule_reference) :: frequency  ! /frequency - Frequency
endtype

type ids_pulse_schedule_nbi_unit  !    NBI unit
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the NBI unit
  character(len=ids_string_length), dimension(:), pointer ::identifier => null()       ! /identifier - Identifier of the NBI unit
  type (ids_gas_mixture_constant),pointer :: species(:) => null()  ! /species(i) - Species injected by the NBI unit (may be more than one in case the unit injects a gas mixture)
  type (ids_pulse_schedule_reference) :: power  ! /power - Power
  type (ids_pulse_schedule_reference) :: energy  ! /energy - Full energy of the injected species (acceleration of a single atom)
endtype

! SPECIAL STRUCTURE data / time
type ids_pulse_schedule_nbi_mode  !    Control mode (operation mode and/or settings used by the controller)
  integer(ids_int), pointer  :: data(:) => null()      ! /mode - Control mode (operation mode and/or settings used by the controller)
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_pulse_schedule_nbi  !    Neutral beam heating and current drive system
  type (ids_pulse_schedule_nbi_unit),pointer :: unit(:) => null()  ! /unit(i) - Set of NBI units
  type (ids_pulse_schedule_nbi_mode) :: mode  ! /mode - Control mode (operation mode and/or settings used by the controller)
endtype

! SPECIAL STRUCTURE data / time
type ids_pulse_schedule_ic_mode  !    Control mode (operation mode and/or settings used by the controller)
  integer(ids_int), pointer  :: data(:) => null()      ! /mode - Control mode (operation mode and/or settings used by the controller)
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_pulse_schedule_ic  !    Ion cyclotron heating and current drive system
  type (ids_pulse_schedule_ic_antenna),pointer :: antenna(:) => null()  ! /antenna(i) - Set of ICRH antennas
  type (ids_pulse_schedule_ic_mode) :: mode  ! /mode - Control mode (operation mode and/or settings used by the controller)
endtype

! SPECIAL STRUCTURE data / time
type ids_pulse_schedule_lh_mode  !    Control mode (operation mode and/or settings used by the controller)
  integer(ids_int), pointer  :: data(:) => null()      ! /mode - Control mode (operation mode and/or settings used by the controller)
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_pulse_schedule_lh  !    Lower hybrid heating and current drive system
  type (ids_pulse_schedule_lh_antenna),pointer :: antenna(:) => null()  ! /antenna(i) - Set of LH antennas
  type (ids_pulse_schedule_lh_mode) :: mode  ! /mode - Control mode (operation mode and/or settings used by the controller)
endtype

! SPECIAL STRUCTURE data / time
type ids_pulse_schedule_ec_mode  !    Control mode (operation mode and/or settings used by the controller)
  integer(ids_int), pointer  :: data(:) => null()      ! /mode - Control mode (operation mode and/or settings used by the controller)
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_pulse_schedule_ec  !    Electron cyclotron heating and current drive system
  type (ids_pulse_schedule_ec_antenna),pointer :: antenna(:) => null()  ! /antenna(i) - Set of ECRH antennas
  type (ids_pulse_schedule_ec_mode) :: mode  ! /mode - Control mode (operation mode and/or settings used by the controller)
endtype

type ids_pulse_schedule_density_control_valve  !    Gas injection valve
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the valve
  character(len=ids_string_length), dimension(:), pointer ::identifier => null()       ! /identifier - Identifier of the valve
  type (ids_pulse_schedule_reference) :: flow_rate  ! /flow_rate - Flow rate of the valve
  type (ids_gas_mixture_constant),pointer :: species(:) => null()  ! /species(i) - Species injected by the valve (may be more than one in case the valve injects a gas mixture)
endtype

! SPECIAL STRUCTURE data / time
type ids_pulse_schedule_density_control_mode  !    Control mode (operation mode and/or settings used by the controller)
  integer(ids_int), pointer  :: data(:) => null()      ! /mode - Control mode (operation mode and/or settings used by the controller)
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_pulse_schedule_density_control  !    Gas injection system
  type (ids_pulse_schedule_density_control_valve),pointer :: valve(:) => null()  ! /valve(i) - Set of injection valves. Time-dependent
  type (ids_pulse_schedule_reference) :: n_e_line  ! /n_e_line - Line integrated electron density
  type (ids_pulse_schedule_reference) :: zeff  ! /zeff - Line average effective charge
  type (ids_pulse_schedule_reference) :: n_t_over_n_d  ! /n_t_over_n_d - Average ratio of tritium over deuterium density
  type (ids_pulse_schedule_reference) :: n_h_over_n_d  ! /n_h_over_n_d - Average ratio of hydrogen over deuterium density
  type (ids_pulse_schedule_density_control_mode) :: mode  ! /mode - Control mode (operation mode and/or settings used by the controller)
endtype

! SPECIAL STRUCTURE data / time
type ids_pulse_schedule_flux_control_mode  !    Control mode (operation mode and/or settings used by the controller)
  integer(ids_int), pointer  :: data(:) => null()      ! /mode - Control mode (operation mode and/or settings used by the controller)
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_pulse_schedule_flux_control  !    Flux control references
  type (ids_pulse_schedule_reference) :: i_plasma  ! /i_plasma - Plasma current
  type (ids_pulse_schedule_reference) :: loop_voltage  ! /loop_voltage - Loop voltage
  type (ids_pulse_schedule_reference) :: li_3  ! /li_3 - Internal inductance
  type (ids_pulse_schedule_reference) :: beta_normal  ! /beta_normal - Normalised toroidal beta, defined as 100 * beta_tor * a[m] * B0 [T] / ip [MA]
  type (ids_pulse_schedule_flux_control_mode) :: mode  ! /mode - Control mode (operation mode and/or settings used by the controller)
endtype

! SPECIAL STRUCTURE data / time
type ids_pulse_schedule_position_mode  !    Control mode (operation mode and/or settings used by the controller)
  integer(ids_int), pointer  :: data(:) => null()      ! /mode - Control mode (operation mode and/or settings used by the controller)
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_pulse_schedule_position  !    Flux control references
  type (ids_pulse_schedule_rz) :: magnetic_axis  ! /magnetic_axis - Magnetic axis position
  type (ids_pulse_schedule_rz) :: geometric_axis  ! /geometric_axis - RZ position of the geometric axis (defined as (Rmin+Rmax) / 2 and (Zmin+Zmax) / 2 of the boundary)
  type (ids_pulse_schedule_reference) :: minor_radius  ! /minor_radius - Minor radius of the plasma boundary (defined as (Rmax-Rmin) / 2 of the boundary)
  type (ids_pulse_schedule_reference) :: elongation  ! /elongation - Elongation of the plasma boundary
  type (ids_pulse_schedule_reference) :: elongation_upper  ! /elongation_upper - Elongation (upper half w.r.t. geometric axis) of the plasma boundary
  type (ids_pulse_schedule_reference) :: elongation_lower  ! /elongation_lower - Elongation (lower half w.r.t. geometric axis) of the plasma boundary
  type (ids_pulse_schedule_reference) :: triangularity  ! /triangularity - Triangularity of the plasma boundary
  type (ids_pulse_schedule_reference) :: triangularity_upper  ! /triangularity_upper - Upper triangularity of the plasma boundary
  type (ids_pulse_schedule_reference) :: triangularity_lower  ! /triangularity_lower - Lower triangularity of the plasma boundary
  type (ids_pulse_schedule_rz),pointer :: x_point(:) => null()  ! /x_point(i) - Array of X-points, for each of them the RZ position is given
  type (ids_pulse_schedule_rz),pointer :: strike_point(:) => null()  ! /strike_point(i) - Array of strike points, for each of them the RZ position is given
  type (ids_pulse_schedule_rz) :: active_limiter_point  ! /active_limiter_point - RZ position of the active limiter point (point of the plasma boundary in contact with the limiter)
  type (ids_pulse_schedule_outline),pointer :: boundary_outline(:) => null()  ! /boundary_outline(i) - Set of (R,Z) points defining the outline of the plasma boundary
  type (ids_pulse_schedule_gap),pointer :: gap(:) => null()  ! /gap(i) - Set of gaps, defined by a reference point and a direction.
  type (ids_pulse_schedule_position_mode) :: mode  ! /mode - Control mode (operation mode and/or settings used by the controller)
endtype

! SPECIAL STRUCTURE data / time
type ids_pulse_schedule_tf_mode  !    Control mode (operation mode and/or settings used by the controller)
  integer(ids_int), pointer  :: data(:) => null()      ! /mode - Control mode (operation mode and/or settings used by the controller)
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_pulse_schedule_tf  !    Toroidal field references
  type (ids_pulse_schedule_reference) :: b_field_tor_vacuum_r  ! /b_field_tor_vacuum_r - Vacuum field times major radius in the toroidal field magnet. Positive sign means anti-clockwise whe
  type (ids_pulse_schedule_tf_mode) :: mode  ! /mode - Control mode (operation mode and/or settings used by the controller)
endtype

type ids_pulse_schedule  !    Description of Pulse Schedule, described by subsystems waveform references and an enveloppe around them. The controllers, pulse sc
  type (ids_ids_properties) :: ids_properties  ! /pulse_schedule/ids_properties -
  type (ids_pulse_schedule_ic) :: ic  ! /pulse_schedule/ic - Ion cyclotron heating and current drive system
  type (ids_pulse_schedule_ec) :: ec  ! /pulse_schedule/ec - Electron cyclotron heating and current drive system
  type (ids_pulse_schedule_lh) :: lh  ! /pulse_schedule/lh - Lower Hybrid heating and current drive system
  type (ids_pulse_schedule_nbi) :: nbi  ! /pulse_schedule/nbi - Neutral beam heating and current drive system
  type (ids_pulse_schedule_density_control) :: density_control  ! /pulse_schedule/density_control - Gas injection system and density control references
  type (ids_pulse_schedule_flux_control) :: flux_control  ! /pulse_schedule/flux_control - Magnetic flux control references
  type (ids_pulse_schedule_position) :: position_control  ! /pulse_schedule/position_control - Plasma position and shape control references
  type (ids_pulse_schedule_tf) :: tf  ! /pulse_schedule/tf - Toroidal field references
  type (ids_code) :: code  ! /pulse_schedule/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include reflectometer_profile/dd_reflectometer_profile.xsd
! SPECIAL STRUCTURE data / time
type ids_rzphi2d_dynamic_aos1_r  !    Major radius
  real(ids_real), pointer  :: data(:,:) => null()     ! /r - Major radius
  real(ids_real), pointer  :: data_error_upper(:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_rzphi2d_dynamic_aos1_z  !    Height
  real(ids_real), pointer  :: data(:,:) => null()     ! /z - Height
  real(ids_real), pointer  :: data_error_upper(:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_rzphi2d_dynamic_aos1_phi  !    Toroidal angle
  real(ids_real), pointer  :: data(:,:) => null()     ! /phi - Toroidal angle
  real(ids_real), pointer  :: data_error_upper(:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_rzphi2d_dynamic_aos1  !    Structure for list of R, Z, Phi positions (2D, dynamic within a type 1 array of structure (indexed on objects, data/time structure
  type (ids_rzphi2d_dynamic_aos1_r) :: r  ! /r - Major radius
  type (ids_rzphi2d_dynamic_aos1_z) :: z  ! /z - Height
  type (ids_rzphi2d_dynamic_aos1_phi) :: phi  ! /phi - Toroidal angle
endtype

! SPECIAL STRUCTURE data / time
type ids_reflectometer_channel_phase  !    Measured phase of the probing wave for each frequency and time slice (corresponding to the begin time of a sweep), relative to the
  real(ids_real), pointer  :: data(:,:) => null()     ! /phase - Measured phase of the probing wave for each frequency and time slice (corresponding to the begin tim
  real(ids_real), pointer  :: data_error_upper(:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_reflectometer_channel_n_e  !    Electron density
  real(ids_real), pointer  :: data(:,:) => null()     ! /n_e - Electron density
  real(ids_real), pointer  :: data_error_upper(:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_reflectometer_channel  !    Reflectometer channel
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the channel
  character(len=ids_string_length), dimension(:), pointer ::identifier => null()       ! /identifier - ID of the channel
  character(len=ids_string_length), dimension(:), pointer ::mode => null()       ! /mode - Detection mode "X" or "O"
  type (ids_line_of_sight_2points) :: line_of_sight_emission  ! /line_of_sight_emission - Description of the line of sight of the emission antenna. The first point corresponds to the antenna
  type (ids_line_of_sight_2points) :: line_of_sight_detection  ! /line_of_sight_detection - Description of the line of sight of the detection antenna, to be filled only if its position is dist
  real(ids_real)  :: sweep_time=ids_real_invalid       ! /sweep_time - Duration of a sweep
  real(ids_real)  :: sweep_time_error_upper=ids_real_invalid
  real(ids_real)  :: sweep_time_error_lower=ids_real_invalid
  integer(ids_int) :: sweep_time_error_index=ids_int_invalid

  real(ids_real),pointer  :: frequencies(:) => null()     ! /frequencies - Array of frequencies scanned during a sweep
  real(ids_real),pointer  :: frequencies_error_upper(:) => null()
  real(ids_real),pointer  :: frequencies_error_lower(:) => null()
  integer(ids_int) :: frequencies_error_index=ids_int_invalid

  type (ids_reflectometer_channel_phase) :: phase  ! /phase - Measured phase of the probing wave for each frequency and time slice (corresponding to the begin tim
  type (ids_rzphi2d_dynamic_aos1) :: position  ! /position - Position of the density measurements
  type (ids_reflectometer_channel_n_e) :: n_e  ! /n_e - Electron density
endtype

type ids_reflectometer_profile  !    Profile reflectometer diagnostic. Multiple reflectometers are considered as independent diagnostics to be handled with different o
  type (ids_ids_properties) :: ids_properties  ! /reflectometer_profile/ids_properties -
  character(len=ids_string_length), dimension(:), pointer ::type => null()       ! /reflectometer_profile/type - Type of reflectometer (frequency_swept, radar, ...)
  type (ids_reflectometer_channel),pointer :: channel(:) => null()  ! /reflectometer_profile/channel(i) - Set of channels, e.g. different reception antennas or frequency bandwidths of the reflectometer
  type (ids_code) :: code  ! /reflectometer_profile/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include sawteeth/dd_sawteeth.xsd
type ids_sawteeth_profiles_1d  !    Core profiles after sawtooth crash
  type (ids_core_radial_grid) :: grid  ! /grid - Radial grid
  real(ids_real),pointer  :: t_e(:) => null()     ! /t_e - Electron temperature
  real(ids_real),pointer  :: t_e_error_upper(:) => null()
  real(ids_real),pointer  :: t_e_error_lower(:) => null()
  integer(ids_int) :: t_e_error_index=ids_int_invalid

  real(ids_real),pointer  :: t_i_average(:) => null()     ! /t_i_average - Ion temperature (averaged on charge states and ion species)
  real(ids_real),pointer  :: t_i_average_error_upper(:) => null()
  real(ids_real),pointer  :: t_i_average_error_lower(:) => null()
  integer(ids_int) :: t_i_average_error_index=ids_int_invalid

  real(ids_real),pointer  :: n_e(:) => null()     ! /n_e - Electron density (thermal+non-thermal)
  real(ids_real),pointer  :: n_e_error_upper(:) => null()
  real(ids_real),pointer  :: n_e_error_lower(:) => null()
  integer(ids_int) :: n_e_error_index=ids_int_invalid

  real(ids_real),pointer  :: n_e_fast(:) => null()     ! /n_e_fast - Density of fast (non-thermal) electrons
  real(ids_real),pointer  :: n_e_fast_error_upper(:) => null()
  real(ids_real),pointer  :: n_e_fast_error_lower(:) => null()
  integer(ids_int) :: n_e_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: n_i_total_over_n_e(:) => null()     ! /n_i_total_over_n_e - Ratio of total ion density (sum over species and charge states) over electron density. (thermal+non-
  real(ids_real),pointer  :: n_i_total_over_n_e_error_upper(:) => null()
  real(ids_real),pointer  :: n_i_total_over_n_e_error_lower(:) => null()
  integer(ids_int) :: n_i_total_over_n_e_error_index=ids_int_invalid

  real(ids_real),pointer  :: momentum_tor(:) => null()     ! /momentum_tor - Total plasma toroidal momentum, summed over ion species and electrons
  real(ids_real),pointer  :: momentum_tor_error_upper(:) => null()
  real(ids_real),pointer  :: momentum_tor_error_lower(:) => null()
  integer(ids_int) :: momentum_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: zeff(:) => null()     ! /zeff - Effective charge
  real(ids_real),pointer  :: zeff_error_upper(:) => null()
  real(ids_real),pointer  :: zeff_error_lower(:) => null()
  integer(ids_int) :: zeff_error_index=ids_int_invalid

  real(ids_real),pointer  :: p_e(:) => null()     ! /p_e - Electron pressure
  real(ids_real),pointer  :: p_e_error_upper(:) => null()
  real(ids_real),pointer  :: p_e_error_lower(:) => null()
  integer(ids_int) :: p_e_error_index=ids_int_invalid

  real(ids_real),pointer  :: p_e_fast_perpendicular(:) => null()     ! /p_e_fast_perpendicular - Fast (non-thermal) electron perpendicular pressure
  real(ids_real),pointer  :: p_e_fast_perpendicular_error_upper(:) => null()
  real(ids_real),pointer  :: p_e_fast_perpendicular_error_lower(:) => null()
  integer(ids_int) :: p_e_fast_perpendicular_error_index=ids_int_invalid

  real(ids_real),pointer  :: p_e_fast_parallel(:) => null()     ! /p_e_fast_parallel - Fast (non-thermal) electron parallel pressure
  real(ids_real),pointer  :: p_e_fast_parallel_error_upper(:) => null()
  real(ids_real),pointer  :: p_e_fast_parallel_error_lower(:) => null()
  integer(ids_int) :: p_e_fast_parallel_error_index=ids_int_invalid

  real(ids_real),pointer  :: p_i_total(:) => null()     ! /p_i_total - Total ion pressure (sum over the ion species)
  real(ids_real),pointer  :: p_i_total_error_upper(:) => null()
  real(ids_real),pointer  :: p_i_total_error_lower(:) => null()
  integer(ids_int) :: p_i_total_error_index=ids_int_invalid

  real(ids_real),pointer  :: p_i_total_fast_perpendicular(:) => null()     ! /p_i_total_fast_perpendicular - Fast (non-thermal) total ion (sum over the ion species) perpendicular pressure
  real(ids_real),pointer  :: p_i_total_fast_perpendicular_error_upper(:) => null()
  real(ids_real),pointer  :: p_i_total_fast_perpendicular_error_lower(:) => null()
  integer(ids_int) :: p_i_total_fast_perpendicular_error_index=ids_int_invalid

  real(ids_real),pointer  :: p_i_total_fast_parallel(:) => null()     ! /p_i_total_fast_parallel - Fast (non-thermal) total ion (sum over the ion species) parallel pressure
  real(ids_real),pointer  :: p_i_total_fast_parallel_error_upper(:) => null()
  real(ids_real),pointer  :: p_i_total_fast_parallel_error_lower(:) => null()
  integer(ids_int) :: p_i_total_fast_parallel_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_thermal(:) => null()     ! /pressure_thermal - Thermal pressure (electrons+ions)
  real(ids_real),pointer  :: pressure_thermal_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_thermal_error_lower(:) => null()
  integer(ids_int) :: pressure_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_perpendicular(:) => null()     ! /pressure_perpendicular - Total perpendicular pressure (electrons+ions, thermal+non-thermal)
  real(ids_real),pointer  :: pressure_perpendicular_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_perpendicular_error_lower(:) => null()
  integer(ids_int) :: pressure_perpendicular_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_parallel(:) => null()     ! /pressure_parallel - Total parallel pressure (electrons+ions, thermal+non-thermal)
  real(ids_real),pointer  :: pressure_parallel_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_parallel_error_lower(:) => null()
  integer(ids_int) :: pressure_parallel_error_index=ids_int_invalid

  real(ids_real),pointer  :: j_total(:) => null()     ! /j_total - Total parallel current density = average(jtot.B) / B0, where B0 = Core_Profiles/Vacuum_Toroidal_Fiel
  real(ids_real),pointer  :: j_total_error_upper(:) => null()
  real(ids_real),pointer  :: j_total_error_lower(:) => null()
  integer(ids_int) :: j_total_error_index=ids_int_invalid

  real(ids_real),pointer  :: j_tor(:) => null()     ! /j_tor - Total toroidal current density = average(J_Tor/R) / average(1/R)
  real(ids_real),pointer  :: j_tor_error_upper(:) => null()
  real(ids_real),pointer  :: j_tor_error_lower(:) => null()
  integer(ids_int) :: j_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: j_ohmic(:) => null()     ! /j_ohmic - Ohmic parallel current density = average(J_Ohmic.B) / B0, where B0 = Core_Profiles/Vacuum_Toroidal_F
  real(ids_real),pointer  :: j_ohmic_error_upper(:) => null()
  real(ids_real),pointer  :: j_ohmic_error_lower(:) => null()
  integer(ids_int) :: j_ohmic_error_index=ids_int_invalid

  real(ids_real),pointer  :: j_non_inductive(:) => null()     ! /j_non_inductive - Non-inductive (includes bootstrap) parallel current density = average(jni.B) / B0, where B0 = Core_P
  real(ids_real),pointer  :: j_non_inductive_error_upper(:) => null()
  real(ids_real),pointer  :: j_non_inductive_error_lower(:) => null()
  integer(ids_int) :: j_non_inductive_error_index=ids_int_invalid

  real(ids_real),pointer  :: j_bootstrap(:) => null()     ! /j_bootstrap - Bootstrap current density = average(J_Bootstrap.B) / B0, where B0 = Core_Profiles/Vacuum_Toroidal_Fi
  real(ids_real),pointer  :: j_bootstrap_error_upper(:) => null()
  real(ids_real),pointer  :: j_bootstrap_error_lower(:) => null()
  integer(ids_int) :: j_bootstrap_error_index=ids_int_invalid

  real(ids_real),pointer  :: conductivity_parallel(:) => null()     ! /conductivity_parallel - Parallel conductivity
  real(ids_real),pointer  :: conductivity_parallel_error_upper(:) => null()
  real(ids_real),pointer  :: conductivity_parallel_error_lower(:) => null()
  integer(ids_int) :: conductivity_parallel_error_index=ids_int_invalid

  real(ids_real),pointer  :: e_field_parallel(:) => null()     ! /e_field_parallel - Parallel electric field = average(E.B) / B0, where Core_Profiles/Vacuum_Toroidal_Field/ B0
  real(ids_real),pointer  :: e_field_parallel_error_upper(:) => null()
  real(ids_real),pointer  :: e_field_parallel_error_lower(:) => null()
  integer(ids_int) :: e_field_parallel_error_index=ids_int_invalid

  real(ids_real),pointer  :: q(:) => null()     ! /q - Safety factor
  real(ids_real),pointer  :: q_error_upper(:) => null()
  real(ids_real),pointer  :: q_error_lower(:) => null()
  integer(ids_int) :: q_error_index=ids_int_invalid

  real(ids_real),pointer  :: magnetic_shear(:) => null()     ! /magnetic_shear - Magnetic shear, defined as rho_tor/q . dq/drho_tor
  real(ids_real),pointer  :: magnetic_shear_error_upper(:) => null()
  real(ids_real),pointer  :: magnetic_shear_error_lower(:) => null()
  integer(ids_int) :: magnetic_shear_error_index=ids_int_invalid

  real(ids_real),pointer  :: phi(:) => null()     ! /phi - Toroidal flux
  real(ids_real),pointer  :: phi_error_upper(:) => null()
  real(ids_real),pointer  :: phi_error_lower(:) => null()
  integer(ids_int) :: phi_error_index=ids_int_invalid

  real(ids_real),pointer  :: psi_star_pre_crash(:) => null()     ! /psi_star_pre_crash - Psi* = psi - phi, just before the sawtooth crash
  real(ids_real),pointer  :: psi_star_pre_crash_error_upper(:) => null()
  real(ids_real),pointer  :: psi_star_pre_crash_error_lower(:) => null()
  integer(ids_int) :: psi_star_pre_crash_error_index=ids_int_invalid

  real(ids_real),pointer  :: psi_star_post_crash(:) => null()     ! /psi_star_post_crash - Psi* = psi - phi, after the sawtooth crash
  real(ids_real),pointer  :: psi_star_post_crash_error_upper(:) => null()
  real(ids_real),pointer  :: psi_star_post_crash_error_lower(:) => null()
  integer(ids_int) :: psi_star_post_crash_error_index=ids_int_invalid

  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_sawteeth_diagnostics  !    Detailed information about the sawtooth characteristics
  real(ids_real),pointer  :: magnetic_shear_q1(:) => null()     ! /magnetic_shear_q1 - Magnetic shear at surface q = 1, defined as rho_tor/q . dq/drho_tor
  real(ids_real),pointer  :: magnetic_shear_q1_error_upper(:) => null()
  real(ids_real),pointer  :: magnetic_shear_q1_error_lower(:) => null()
  integer(ids_int) :: magnetic_shear_q1_error_index=ids_int_invalid

  real(ids_real),pointer  :: rho_tor_norm_q1(:) => null()     ! /rho_tor_norm_q1 - Normalised toroidal flux coordinate at surface q = 1
  real(ids_real),pointer  :: rho_tor_norm_q1_error_upper(:) => null()
  real(ids_real),pointer  :: rho_tor_norm_q1_error_lower(:) => null()
  integer(ids_int) :: rho_tor_norm_q1_error_index=ids_int_invalid

  real(ids_real),pointer  :: rho_tor_norm_inversion(:) => null()     ! /rho_tor_norm_inversion - Normalised toroidal flux coordinate at inversion radius
  real(ids_real),pointer  :: rho_tor_norm_inversion_error_upper(:) => null()
  real(ids_real),pointer  :: rho_tor_norm_inversion_error_lower(:) => null()
  integer(ids_int) :: rho_tor_norm_inversion_error_index=ids_int_invalid

  real(ids_real),pointer  :: rho_tor_norm_mixing(:) => null()     ! /rho_tor_norm_mixing - Normalised toroidal flux coordinate at mixing radius
  real(ids_real),pointer  :: rho_tor_norm_mixing_error_upper(:) => null()
  real(ids_real),pointer  :: rho_tor_norm_mixing_error_lower(:) => null()
  integer(ids_int) :: rho_tor_norm_mixing_error_index=ids_int_invalid

  integer(ids_int),pointer  :: previous_crash_trigger(:) => null()      ! /previous_crash_trigger - Previous crash trigger. Flag indicating whether a crash condition has been satisfied : 0 = no crash.
  real(ids_real),pointer  :: previous_crash_time(:) => null()     ! /previous_crash_time - Time at which the previous sawtooth crash occured
  real(ids_real),pointer  :: previous_crash_time_error_upper(:) => null()
  real(ids_real),pointer  :: previous_crash_time_error_lower(:) => null()
  integer(ids_int) :: previous_crash_time_error_index=ids_int_invalid

  real(ids_real),pointer  :: previous_period(:) => null()     ! /previous_period - Previous sawtooth period
  real(ids_real),pointer  :: previous_period_error_upper(:) => null()
  real(ids_real),pointer  :: previous_period_error_lower(:) => null()
  integer(ids_int) :: previous_period_error_index=ids_int_invalid

endtype

type ids_sawteeth  !    Description of sawtooth events. This IDS must be used in homogeneous_time = 1 mode
  type (ids_ids_properties) :: ids_properties  ! /sawteeth/ids_properties -
  type (ids_b_tor_vacuum_1) :: vacuum_toroidal_field  ! /sawteeth/vacuum_toroidal_field - Characteristics of the vacuum toroidal field (used in rho_tor definition)
  integer(ids_int),pointer  :: crash_trigger(:) => null()      ! /sawteeth/crash_trigger - Flag indicating whether a crash condition has been satisfied : 0 = no crash. N(>0) = crash triggered
  type (ids_sawteeth_profiles_1d),pointer :: profiles_1d(:) => null()  ! /sawteeth/profiles_1d(i) - Core profiles after sawtooth crash for various time slices
  type (ids_sawteeth_diagnostics) :: diagnostics  ! /sawteeth/diagnostics - Detailed information about the sawtooth characteristics
  type (ids_code) :: code  ! /sawteeth/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include sdn/dd_sdn.xsd
type ids_sdn_topic  !    List of the topics
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Topic name
  type (ids_sdn_allocatable_signals),pointer :: signal(:) => null()  ! /signal(i) - List of signals which can be allocated to the SDN
endtype

! SPECIAL STRUCTURE data / time
type ids_sdn_allocatable_signals_value  !    Signal value
  real(ids_real), pointer  :: data(:) => null()     ! /value - Signal value
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_sdn_allocatable_signals_quality  !    Indicator of the quality of the signal. Following ITER PCS documentation (https://user.iter.org/?uid=354SJ3&action=get_document),
  integer(ids_int), pointer  :: data(:) => null()      ! /quality - Indicator of the quality of the signal. Following ITER PCS documentation (https://user.iter.org/?uid
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_sdn_allocatable_signals  !    List of signals which can be allocated to the SDN
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Signal name
  character(len=ids_string_length), dimension(:), pointer ::definition => null()       ! /definition - Signal definition
  integer(ids_int)  :: allocated_position=ids_int_invalid       ! /allocated_position - Allocation of signal to a position in the SDN (1..N); this will be implementation specific
  type (ids_sdn_allocatable_signals_value) :: value  ! /value - Signal value
  type (ids_sdn_allocatable_signals_quality) :: quality  ! /quality - Indicator of the quality of the signal. Following ITER PCS documentation (https://user.iter.org/?uid
endtype

type ids_sdn  !    Description of the Synchronous Data Network parameters and the signals on it
  type (ids_ids_properties) :: ids_properties  ! /sdn/ids_properties -
  type (ids_sdn_topic),pointer :: topic(:) => null()  ! /sdn/topic(i) - List of topics. SDN signals are grouped by topic
  type (ids_code) :: code  ! /sdn/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include soft_x_rays/dd_soft_x_rays.xsd
! SPECIAL STRUCTURE data / time
type ids_sxr_channel_intensity  !    Intensity of the soft X-rays received on the detector, in multiple energy bands if available from the detector
  real(ids_real), pointer  :: data(:,:) => null()     ! /intensity - Intensity of the soft X-rays received on the detector, in multiple energy bands if available from th
  real(ids_real), pointer  :: data_error_upper(:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_sxr_channel_power_density  !    Power received on the detector per unit surface, in multiple energy bands if available from the detector
  real(ids_real), pointer  :: data(:,:) => null()     ! /power_density - Power received on the detector per unit surface, in multiple energy bands if available from the dete
  real(ids_real), pointer  :: data_error_upper(:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_sxr_channel_validity_timed  !    Indicator of the validity of the channel as a function of time (0 means valid, negative values mean non-valid)
  integer(ids_int), pointer  :: data(:) => null()      ! /validity_timed - Indicator of the validity of the channel as a function of time (0 means valid, negative values mean
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_sxr_channel  !    Soft X-rays channel
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the channel
  character(len=ids_string_length), dimension(:), pointer ::identifier => null()       ! /identifier - ID of the channel
  type (ids_detector_aperture) :: detector  ! /detector - Detector description
  type (ids_detector_aperture),pointer :: aperture(:) => null()  ! /aperture(i) - Description of a set of collimating apertures
  type (ids_line_of_sight_2points) :: line_of_sight  ! /line_of_sight - Description of the line of sight of the channel, given by 2 points
  type (ids_detector_energy_band),pointer :: energy_band(:) => null()  ! /energy_band(i) - Set of energy bands in which photons are counted by the detector
  type (ids_sxr_channel_intensity) :: intensity  ! /intensity - Intensity of the soft X-rays received on the detector, in multiple energy bands if available from th
  type (ids_sxr_channel_power_density) :: power_density  ! /power_density - Power received on the detector per unit surface, in multiple energy bands if available from the dete
  type (ids_sxr_channel_validity_timed) :: validity_timed  ! /validity_timed - Indicator of the validity of the channel as a function of time (0 means valid, negative values mean
  integer(ids_int)  :: validity=ids_int_invalid       ! /validity - Indicator of the validity of the channel for the whole acquisition period (0 means valid, negative v
endtype

type ids_soft_x_rays  !    Soft X-rays tomography diagnostic
  type (ids_ids_properties) :: ids_properties  ! /soft_x_rays/ids_properties -
  type (ids_sxr_channel),pointer :: channel(:) => null()  ! /soft_x_rays/channel(i) - Set of channels (detector or pixel of a camera)
  type (ids_code) :: code  ! /soft_x_rays/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include spectrometer_visible/dd_spectrometer_visible.xsd
type ids_spectro_vis_channel_light_collection  !    Emission weights for various points
  real(ids_real),pointer  :: values(:) => null()     ! /values - Values of the light collection efficiencies
  real(ids_real),pointer  :: values_error_upper(:) => null()
  real(ids_real),pointer  :: values_error_lower(:) => null()
  integer(ids_int) :: values_error_index=ids_int_invalid

  type (ids_rzphi1d_static) :: positions  ! /positions - List of positions for which the light collection efficiencies are provided
endtype

type ids_detector_image_circular  !    Description of circular or elliptic observation cones
  real(ids_real)  :: radius=ids_real_invalid       ! /radius - Radius of the circle
  real(ids_real)  :: radius_error_upper=ids_real_invalid
  real(ids_real)  :: radius_error_lower=ids_real_invalid
  integer(ids_int) :: radius_error_index=ids_int_invalid

  real(ids_real)  :: ellipticity=ids_real_invalid       ! /ellipticity - Ellipticity
  real(ids_real)  :: ellipticity_error_upper=ids_real_invalid
  real(ids_real)  :: ellipticity_error_lower=ids_real_invalid
  integer(ids_int) :: ellipticity_error_index=ids_int_invalid

endtype

type ids_detector_image  !    Description of the observation volume of the detector or detector pixel at the focal plane of the optical system. This is basicall
  integer(ids_int)  :: geometry_type=ids_int_invalid       ! /geometry_type - Type of geometry used to describe the detector image (1:'outline', 2:'circular')
  type (ids_rzphi1d_static) :: outline  ! /outline - Coordinates of the points shaping the polygon of the detector image
  type (ids_detector_image_circular) :: circular  ! /circular - Description of circular or elliptic detector image
endtype

type ids_spectro_vis_channel_resolution  !    In case of active spectroscopy, spatial resolution of the measurement
  type (ids_rzphi0d_dynamic_aos3) :: centre  ! /centre - Position of the centre of the spatially resolved zone
  type (ids_rzphi0d_dynamic_aos3) :: width  ! /width - Full width of the spatially resolved zone in the R, Z and phi directions
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

! SPECIAL STRUCTURE data / time
type ids_spectro_vis_channel_filter_line_radiances  !    Calibrated, background subtracted line integrals
  real(ids_real), pointer  :: data(:,:) => null()     ! /line_radiances - Calibrated, background subtracted line integrals
  real(ids_real), pointer  :: data_error_upper(:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_spectro_vis_channel_filter_photon_count  !    Detected photon count
  real(ids_real), pointer  :: data(:,:) => null()     ! /photon_count - Detected photon count
  real(ids_real), pointer  :: data_error_upper(:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_spectro_vis_channel_filter_line_intensities  !    Line gross integral intensities
  real(ids_real), pointer  :: data(:,:) => null()     ! /line_intensities - Line gross integral intensities
  real(ids_real), pointer  :: data_error_upper(:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_spectro_vis_channel_filter_calibrated_line_integrals  !    Calibrated line gross areas integrals
  real(ids_real), pointer  :: data(:,:) => null()     ! /calibrated_line_integrals - Calibrated line gross areas integrals
  real(ids_real), pointer  :: data_error_upper(:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_spectro_vis_channel_filter  !    Filter spectrometer
  real(ids_real),pointer  :: processed_lines(:) => null()     ! /processed_lines - Central wavelength of the processed lines
  real(ids_real),pointer  :: processed_lines_error_upper(:) => null()
  real(ids_real),pointer  :: processed_lines_error_lower(:) => null()
  integer(ids_int) :: processed_lines_error_index=ids_int_invalid

  type (ids_spectro_vis_channel_filter_line_radiances) :: line_radiances  ! /line_radiances - Calibrated, background subtracted line integrals
  real(ids_real),pointer  :: raw_lines(:) => null()     ! /raw_lines - Central wavelength of the raw lines
  real(ids_real),pointer  :: raw_lines_error_upper(:) => null()
  real(ids_real),pointer  :: raw_lines_error_lower(:) => null()
  integer(ids_int) :: raw_lines_error_index=ids_int_invalid

  type (ids_spectro_vis_channel_filter_photon_count) :: photon_count  ! /photon_count - Detected photon count
  type (ids_spectro_vis_channel_filter_line_intensities) :: line_intensities  ! /line_intensities - Line gross integral intensities
  real(ids_real),pointer  :: calibrated_lines(:) => null()     ! /calibrated_lines - Central wavelength of the calibrated lines
  real(ids_real),pointer  :: calibrated_lines_error_upper(:) => null()
  real(ids_real),pointer  :: calibrated_lines_error_lower(:) => null()
  integer(ids_int) :: calibrated_lines_error_index=ids_int_invalid

  type (ids_spectro_vis_channel_filter_calibrated_line_integrals) :: calibrated_line_integrals  ! /calibrated_line_integrals - Calibrated line gross areas integrals
  real(ids_real)  :: exposure_time=ids_real_invalid       ! /exposure_time - Exposure time
  real(ids_real)  :: exposure_time_error_upper=ids_real_invalid
  real(ids_real)  :: exposure_time_error_lower=ids_real_invalid
  integer(ids_int) :: exposure_time_error_index=ids_int_invalid

  real(ids_real)  :: radiance_calibration=ids_real_invalid       ! /radiance_calibration - Radiance calibration
  real(ids_real)  :: radiance_calibration_error_upper=ids_real_invalid
  real(ids_real)  :: radiance_calibration_error_lower=ids_real_invalid
  integer(ids_int) :: radiance_calibration_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::radiance_calibration_date => null()       ! /radiance_calibration_date - Date of the radiance calibration (yyyy_mm_dd)
endtype

! SPECIAL STRUCTURE data / time
type ids_spectro_vis_channel_grating_line_radiances  !    Calibrated, background subtracted line integrals
  real(ids_real), pointer  :: data(:,:) => null()     ! /line_radiances - Calibrated, background subtracted line integrals
  real(ids_real), pointer  :: data_error_upper(:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_spectro_vis_channel_grating_radiance_spectra  !    Calibrated spectra
  real(ids_real), pointer  :: data(:,:) => null()     ! /radiance_spectra - Calibrated spectra
  real(ids_real), pointer  :: data_error_upper(:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_spectro_vis_channel_grating_intensity_spectra  !    Intensity spectra (not calibrated)
  real(ids_real), pointer  :: data(:,:) => null()     ! /intensity_spectra - Intensity spectra (not calibrated)
  real(ids_real), pointer  :: data_error_upper(:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_spectro_vis_channel_grating  !    Grating spectrometer
  real(ids_real),pointer  :: processed_lines(:) => null()     ! /processed_lines - Central wavelength of the processed lines
  real(ids_real),pointer  :: processed_lines_error_upper(:) => null()
  real(ids_real),pointer  :: processed_lines_error_lower(:) => null()
  integer(ids_int) :: processed_lines_error_index=ids_int_invalid

  type (ids_spectro_vis_channel_grating_line_radiances) :: line_radiances  ! /line_radiances - Calibrated, background subtracted line integrals
  real(ids_real),pointer  :: wavelengths(:) => null()     ! /wavelengths - Measured wavelengths
  real(ids_real),pointer  :: wavelengths_error_upper(:) => null()
  real(ids_real),pointer  :: wavelengths_error_lower(:) => null()
  integer(ids_int) :: wavelengths_error_index=ids_int_invalid

  type (ids_spectro_vis_channel_grating_radiance_spectra) :: radiance_spectra  ! /radiance_spectra - Calibrated spectra
  type (ids_spectro_vis_channel_grating_intensity_spectra) :: intensity_spectra  ! /intensity_spectra - Intensity spectra (not calibrated)
  real(ids_real)  :: exposure_time=ids_real_invalid       ! /exposure_time - Exposure time
  real(ids_real)  :: exposure_time_error_upper=ids_real_invalid
  real(ids_real)  :: exposure_time_error_lower=ids_real_invalid
  integer(ids_int) :: exposure_time_error_index=ids_int_invalid

  real(ids_real),pointer  :: radiance_calibration(:) => null()     ! /radiance_calibration - Radiance calibration
  real(ids_real),pointer  :: radiance_calibration_error_upper(:) => null()
  real(ids_real),pointer  :: radiance_calibration_error_lower(:) => null()
  integer(ids_int) :: radiance_calibration_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::radiance_calibration_date => null()       ! /radiance_calibration_date - Date of the radiance calibration (yyyy_mm_dd)
  character(len=ids_string_length), dimension(:), pointer ::wavelength_calibration_date => null()       ! /wavelength_calibration_date - Date of the wavelength calibration (yyyy_mm_dd)
endtype

! SPECIAL STRUCTURE data / time
type ids_spectro_vis_channel_validity_timed  !    Indicator of the validity of the channel as a function of time (0 means valid, negative values mean non-valid)
  integer(ids_int), pointer  :: data(:) => null()      ! /validity_timed - Indicator of the validity of the channel as a function of time (0 means valid, negative values mean
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_spectro_vis_channel  !    Charge exchange channel
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the channel
  type (ids_identifier_static) :: type  ! /type - Type of spectrometer the channel is connected to (index=1: grating, 2: filter)
  type (ids_detector_aperture) :: detector  ! /detector - Detector description
  type (ids_detector_aperture),pointer :: aperture(:) => null()  ! /aperture(i) - Description of a set of collimating apertures
  type (ids_line_of_sight_2points) :: line_of_sight  ! /line_of_sight - Description of the line of sight of the channel, given by 2 points
  type (ids_detector_image) :: detector_image  ! /detector_image - Image of the detector or pixel on the focal plane of the optical system
  real(ids_real)  :: spot_diameter=ids_real_invalid       ! /spot_diameter - Image of the optical fiber on the focal plane of the optical system
  real(ids_real)  :: spot_diameter_error_upper=ids_real_invalid
  real(ids_real)  :: spot_diameter_error_lower=ids_real_invalid
  integer(ids_int) :: spot_diameter_error_index=ids_int_invalid

  type (ids_spectro_vis_channel_light_collection) :: light_collection_efficiencies  ! /light_collection_efficiencies - Light collection efficiencies (fraction of the local emission detected by the optical system) for a
  type (ids_spectro_vis_channel_resolution),pointer :: active_spatial_resolution(:) => null()  ! /active_spatial_resolution(i) - In case of active spectroscopy, describes the spatial resolution of the measurement, calculated as a
  type (ids_spectro_vis_channel_grating) :: grating  ! /grating - Quantities measured by the channel if connected to a grating spectrometer
  type (ids_spectro_vis_channel_filter) :: filter  ! /filter - Quantities measured by the channel if connected to a filter spectrometer
  type (ids_spectro_vis_channel_validity_timed) :: validity_timed  ! /validity_timed - Indicator of the validity of the channel as a function of time (0 means valid, negative values mean
  integer(ids_int)  :: validity=ids_int_invalid       ! /validity - Indicator of the validity of the channel for the whole acquisition period (0 means valid, negative v
endtype

type ids_spectrometer_visible  !    Spectrometer in visible light range diagnostic
  type (ids_ids_properties) :: ids_properties  ! /spectrometer_visible/ids_properties -
  character(len=ids_string_length), dimension(:), pointer ::detector_layout => null()       ! /spectrometer_visible/detector_layout - Layout of the detector grid employed. Ex: '4x16', '4x32', '1x18'
  type (ids_spectro_vis_channel),pointer :: channel(:) => null()  ! /spectrometer_visible/channel(i) - Set of channels (detector or pixel of a camera)
  type (ids_code) :: code  ! /spectrometer_visible/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include summary/dd_summary.xsd
type ids_summary_dynamic_flt_1d_3_parent_2  !    Summary dynamic FLT_1D + source information, time three levels above this node and units as parent level 2
  real(ids_real),pointer  :: value(:) => null()     ! /value - Value
  real(ids_real),pointer  :: value_error_upper(:) => null()
  real(ids_real),pointer  :: value_error_lower(:) => null()
  integer(ids_int) :: value_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::source => null()       ! /source - Source of the data (any comment describing the origin of the data : code, path to diagnostic signals
endtype

type ids_summary_dynamic_flt_1d_2_parent_2  !    Summary dynamic FLT_1D + source information, time two levels above this node and units as parent level 2
  real(ids_real),pointer  :: value(:) => null()     ! /value - Value
  real(ids_real),pointer  :: value_error_upper(:) => null()
  real(ids_real),pointer  :: value_error_lower(:) => null()
  integer(ids_int) :: value_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::source => null()       ! /source - Source of the data (any comment describing the origin of the data : code, path to diagnostic signals
endtype

type ids_summary_dynamic_flt_2d_fraction_2  !    Summary dynamic FL2_1D + source information, time two levels above this node, first dimension 1...3 (beam fractions)
  real(ids_real),pointer  :: value(:,:) => null()     ! /value - Value
  real(ids_real),pointer  :: value_error_upper(:,:) => null()
  real(ids_real),pointer  :: value_error_lower(:,:) => null()
  integer(ids_int) :: value_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::source => null()       ! /source - Source of the data (any comment describing the origin of the data : code, path to diagnostic signals
endtype

type ids_summary_dynamic_flt_1d_3  !    Summary dynamic FLT_1D + source information, time three levels above this node
  real(ids_real),pointer  :: value(:) => null()     ! /value - Value
  real(ids_real),pointer  :: value_error_upper(:) => null()
  real(ids_real),pointer  :: value_error_lower(:) => null()
  integer(ids_int) :: value_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::source => null()       ! /source - Source of the data (any comment describing the origin of the data : code, path to diagnostic signals
endtype

type ids_summary_dynamic_flt_1d_2  !    Summary dynamic FLT_1D + source information, time two levels above this node
  real(ids_real),pointer  :: value(:) => null()     ! /value - Value
  real(ids_real),pointer  :: value_error_upper(:) => null()
  real(ids_real),pointer  :: value_error_lower(:) => null()
  integer(ids_int) :: value_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::source => null()       ! /source - Source of the data (any comment describing the origin of the data : code, path to diagnostic signals
endtype

type ids_summary_dynamic_flt_1d_1  !    Summary dynamic FLT_1D + source information, time one level above this node
  real(ids_real),pointer  :: value(:) => null()     ! /value - Value
  real(ids_real),pointer  :: value_error_upper(:) => null()
  real(ids_real),pointer  :: value_error_lower(:) => null()
  integer(ids_int) :: value_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::source => null()       ! /source - Source of the data (any comment describing the origin of the data : code, path to diagnostic signals
endtype

type ids_summary_static_flt_0d  !    Summary static FLT_0D + source information
  real(ids_real)  :: value=ids_real_invalid       ! /value - Value
  real(ids_real)  :: value_error_upper=ids_real_invalid
  real(ids_real)  :: value_error_lower=ids_real_invalid
  integer(ids_int) :: value_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::source => null()       ! /source - Source of the data (any comment describing the origin of the data : code, path to diagnostic signals
endtype

type ids_summary_static_int_0d  !    Summary static INT_0D + source information
  integer(ids_int)  :: value=ids_int_invalid       ! /value - Value
  character(len=ids_string_length), dimension(:), pointer ::source => null()       ! /source - Source of the data (any comment describing the origin of the data : code, path to diagnostic signals
endtype

type ids_summary_constant_int_0d  !    Summary constant INT_0D + source information
  integer(ids_int)  :: value=ids_int_invalid       ! /value - Value
  character(len=ids_string_length), dimension(:), pointer ::source => null()       ! /source - Source of the data (any comment describing the origin of the data : code, path to diagnostic signals
endtype

type ids_summary_constant_flt_0d  !    Summary constant FLT_0D + source information
  real(ids_real)  :: value=ids_real_invalid       ! /value - Value
  real(ids_real)  :: value_error_upper=ids_real_invalid
  real(ids_real)  :: value_error_lower=ids_real_invalid
  integer(ids_int) :: value_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::source => null()       ! /source - Source of the data (any comment describing the origin of the data : code, path to diagnostic signals
endtype

type ids_summary_constant_str_0d  !    Summary constant STR_0D + source information
  character(len=ids_string_length), dimension(:), pointer ::value => null()       ! /value - Value
  character(len=ids_string_length), dimension(:), pointer ::source => null()       ! /source - Source of the data (any comment describing the origin of the data : code, path to diagnostic signals
endtype

type ids_summary_rzphi0d_static  !    Structure for R, Z, Phi positions (0D, static) + source information
  type (ids_summary_static_flt_0d) :: r  ! /r - Major radius
  type (ids_summary_static_flt_0d) :: z  ! /z - Height
  type (ids_summary_static_flt_0d) :: phi  ! /phi - Toroidal angle
endtype

type ids_summary_dynamic_int_1d_2  !    Summary dynamic INT_1D + source information, time two levels above this node
  integer(ids_int),pointer  :: value(:) => null()      ! /value - Value
  character(len=ids_string_length), dimension(:), pointer ::source => null()       ! /source - Source of the data (any comment describing the origin of the data : code, path to diagnostic signals
endtype

type ids_summary_dynamic_int_1d_1  !    Summary dynamic INT_1D + source information, time one level above this node
  integer(ids_int),pointer  :: value(:) => null()      ! /value - Value
  character(len=ids_string_length), dimension(:), pointer ::source => null()       ! /source - Source of the data (any comment describing the origin of the data : code, path to diagnostic signals
endtype

type ids_summary_species_level_3  !    List of ion species used in summary, three levels below the top
  type (ids_summary_dynamic_flt_1d_3_parent_2) :: hydrogen  ! /hydrogen - Hydrogen
  type (ids_summary_dynamic_flt_1d_3_parent_2) :: deuterium  ! /deuterium - Deuterium
  type (ids_summary_dynamic_flt_1d_3_parent_2) :: tritium  ! /tritium - Tritium
  type (ids_summary_dynamic_flt_1d_3_parent_2) :: helium_3  ! /helium_3 - Helium isotope with 3 nucleons
  type (ids_summary_dynamic_flt_1d_3_parent_2) :: helium_4  ! /helium_4 - Helium isotope with 4 nucleons
  type (ids_summary_dynamic_flt_1d_3_parent_2) :: berylium  ! /berylium - Berylium
  type (ids_summary_dynamic_flt_1d_3_parent_2) :: lithium  ! /lithium - Lithium
  type (ids_summary_dynamic_flt_1d_3_parent_2) :: carbon  ! /carbon - Carbon
  type (ids_summary_dynamic_flt_1d_3_parent_2) :: nitrogen  ! /nitrogen - Nitrogen
  type (ids_summary_dynamic_flt_1d_3_parent_2) :: neon  ! /neon - Neon
  type (ids_summary_dynamic_flt_1d_3_parent_2) :: argon  ! /argon - Argon
  type (ids_summary_dynamic_flt_1d_3_parent_2) :: xenon  ! /xenon - Xenon
  type (ids_summary_dynamic_flt_1d_3_parent_2) :: oxygen  ! /oxygen - Oxygen
  type (ids_summary_dynamic_flt_1d_3_parent_2) :: tungsten  ! /tungsten - Tungsten
endtype

type ids_summary_species_level_2  !    List of ion species used in summary, two levels below the top
  type (ids_summary_dynamic_flt_1d_2_parent_2) :: hydrogen  ! /hydrogen - Hydrogen
  type (ids_summary_dynamic_flt_1d_2_parent_2) :: deuterium  ! /deuterium - Deuterium
  type (ids_summary_dynamic_flt_1d_2_parent_2) :: tritium  ! /tritium - Tritium
  type (ids_summary_dynamic_flt_1d_2_parent_2) :: helium_3  ! /helium_3 - Helium isotope with 3 nucleons
  type (ids_summary_dynamic_flt_1d_2_parent_2) :: helium_4  ! /helium_4 - Helium isotope with 4 nucleons
  type (ids_summary_dynamic_flt_1d_2_parent_2) :: berylium  ! /berylium - Berylium
  type (ids_summary_dynamic_flt_1d_2_parent_2) :: lithium  ! /lithium - Lithium
  type (ids_summary_dynamic_flt_1d_2_parent_2) :: carbon  ! /carbon - Carbon
  type (ids_summary_dynamic_flt_1d_2_parent_2) :: nitrogen  ! /nitrogen - Nitrogen
  type (ids_summary_dynamic_flt_1d_2_parent_2) :: neon  ! /neon - Neon
  type (ids_summary_dynamic_flt_1d_2_parent_2) :: argon  ! /argon - Argon
  type (ids_summary_dynamic_flt_1d_2_parent_2) :: xenon  ! /xenon - Xenon
  type (ids_summary_dynamic_flt_1d_2_parent_2) :: oxygen  ! /oxygen - Oxygen
  type (ids_summary_dynamic_flt_1d_2_parent_2) :: tungsten  ! /tungsten - Tungsten
endtype

type ids_summary_plasma_composition_species  !    Description of simple species (elements) without declaration of their ionisation state
  type (ids_summary_constant_flt_0d) :: a  ! /a - Mass of atom
  type (ids_summary_constant_flt_0d) :: z_n  ! /z_n - Nuclear charge
  type (ids_summary_constant_str_0d) :: label  ! /label - String identifying the species (e.g. H, D, T, ...)
endtype

type ids_summary_local_position_r_z  !    Radial position at which physics quantities are evaluated, including an R,Z position
  real(ids_real),pointer  :: rho_tor_norm(:) => null()     ! /rho_tor_norm - Normalised toroidal flux coordinate. The normalizing value for rho_tor_norm, is the toroidal flux co
  real(ids_real),pointer  :: rho_tor_norm_error_upper(:) => null()
  real(ids_real),pointer  :: rho_tor_norm_error_lower(:) => null()
  integer(ids_int) :: rho_tor_norm_error_index=ids_int_invalid

  real(ids_real),pointer  :: rho_tor(:) => null()     ! /rho_tor - Toroidal flux coordinate. rho_tor = sqrt(b_flux_tor/(pi*b0)) ~ sqrt(pi*r^2*b0/(pi*b0)) ~ r [m]. The
  real(ids_real),pointer  :: rho_tor_error_upper(:) => null()
  real(ids_real),pointer  :: rho_tor_error_lower(:) => null()
  integer(ids_int) :: rho_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: psi(:) => null()     ! /psi - Poloidal magnetic flux
  real(ids_real),pointer  :: psi_error_upper(:) => null()
  real(ids_real),pointer  :: psi_error_lower(:) => null()
  integer(ids_int) :: psi_error_index=ids_int_invalid

  real(ids_real),pointer  :: r(:) => null()     ! /r - Major radius
  real(ids_real),pointer  :: r_error_upper(:) => null()
  real(ids_real),pointer  :: r_error_lower(:) => null()
  integer(ids_int) :: r_error_index=ids_int_invalid

  real(ids_real),pointer  :: z(:) => null()     ! /z - Height
  real(ids_real),pointer  :: z_error_upper(:) => null()
  real(ids_real),pointer  :: z_error_lower(:) => null()
  integer(ids_int) :: z_error_index=ids_int_invalid

endtype

type ids_summary_local_position  !    Radial position at which physics quantities are evaluated
  real(ids_real),pointer  :: rho_tor_norm(:) => null()     ! /rho_tor_norm - Normalised toroidal flux coordinate. The normalizing value for rho_tor_norm, is the toroidal flux co
  real(ids_real),pointer  :: rho_tor_norm_error_upper(:) => null()
  real(ids_real),pointer  :: rho_tor_norm_error_lower(:) => null()
  integer(ids_int) :: rho_tor_norm_error_index=ids_int_invalid

  real(ids_real),pointer  :: rho_tor(:) => null()     ! /rho_tor - Toroidal flux coordinate. rho_tor = sqrt(b_flux_tor/(pi*b0)) ~ sqrt(pi*r^2*b0/(pi*b0)) ~ r [m]. The
  real(ids_real),pointer  :: rho_tor_error_upper(:) => null()
  real(ids_real),pointer  :: rho_tor_error_lower(:) => null()
  integer(ids_int) :: rho_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: psi(:) => null()     ! /psi - Poloidal magnetic flux
  real(ids_real),pointer  :: psi_error_upper(:) => null()
  real(ids_real),pointer  :: psi_error_lower(:) => null()
  integer(ids_int) :: psi_error_index=ids_int_invalid

endtype

type ids_summary_local_quantities_r_z  !    Set of local quantities, including an R,Z position
  type (ids_summary_local_position_r_z) :: position  ! /position - Radial position at which physics quantities are evaluated
  type (ids_summary_dynamic_flt_1d_2) :: t_e  ! /t_e - Electron temperature
  type (ids_summary_dynamic_flt_1d_2) :: t_i_average  ! /t_i_average - Ion temperature (average over ion species)
  type (ids_summary_dynamic_flt_1d_2) :: n_e  ! /n_e - Electron density
  type (ids_summary_species_level_3) :: n_i  ! /n_i - Ion density per species
  type (ids_summary_dynamic_flt_1d_2) :: n_i_total  ! /n_i_total - Total ion density (sum over species)
  type (ids_summary_dynamic_flt_1d_2) :: zeff  ! /zeff - Effective charge
  type (ids_summary_dynamic_flt_1d_2) :: momentum_tor  ! /momentum_tor - Total plasma toroidal momentum, summed over ion species and electrons
  type (ids_summary_species_level_3) :: velocity_tor  ! /velocity_tor - Ion toroidal rotation velocity, per species
  type (ids_summary_dynamic_flt_1d_2) :: q  ! /q - Safety factor
  type (ids_summary_dynamic_flt_1d_2) :: magnetic_shear  ! /magnetic_shear - Magnetic shear, defined as rho_tor/q . dq/drho_tor
endtype

type ids_summary_local_quantities_no_position_name  !    Set of local quantities without radial position, for localisations outside the LCFS, with a name
  type (ids_summary_constant_str_0d) :: name  ! /name - Name of the element
  type (ids_summary_dynamic_flt_1d_2) :: t_e  ! /t_e - Electron temperature
  type (ids_summary_dynamic_flt_1d_2) :: t_i_average  ! /t_i_average - Ion temperature (average over ion species)
  type (ids_summary_dynamic_flt_1d_2) :: n_e  ! /n_e - Electron density
  type (ids_summary_species_level_3) :: n_i  ! /n_i - Ion density per species
  type (ids_summary_dynamic_flt_1d_2) :: n_i_total  ! /n_i_total - Total ion density (sum over species)
  type (ids_summary_dynamic_flt_1d_2) :: zeff  ! /zeff - Effective charge
  type (ids_summary_dynamic_flt_1d_2) :: flux_expansion  ! /flux_expansion - Flux expansion
endtype

type ids_summary_local_quantities_no_position  !    Set of local quantities without radial position, for localisations outside the LCFS
  type (ids_summary_dynamic_flt_1d_2) :: t_e  ! /t_e - Electron temperature
  type (ids_summary_dynamic_flt_1d_2) :: t_i_average  ! /t_i_average - Ion temperature (average over ion species)
  type (ids_summary_dynamic_flt_1d_2) :: n_e  ! /n_e - Electron density
  type (ids_summary_species_level_3) :: n_i  ! /n_i - Ion density per species
  type (ids_summary_dynamic_flt_1d_2) :: n_i_total  ! /n_i_total - Total ion density (sum over species)
  type (ids_summary_dynamic_flt_1d_2) :: zeff  ! /zeff - Effective charge
  type (ids_summary_dynamic_flt_1d_2) :: flux_expansion  ! /flux_expansion - Flux expansion
endtype

type ids_summary_local_quantities  !    Set of local quantities
  type (ids_summary_local_position) :: position  ! /position - Radial position at which physics quantities are evaluated
  type (ids_summary_dynamic_flt_1d_2) :: t_e  ! /t_e - Electron temperature
  type (ids_summary_dynamic_flt_1d_2) :: t_i_average  ! /t_i_average - Ion temperature (average over ion species)
  type (ids_summary_dynamic_flt_1d_2) :: n_e  ! /n_e - Electron density
  type (ids_summary_species_level_3) :: n_i  ! /n_i - Ion density per species
  type (ids_summary_dynamic_flt_1d_2) :: n_i_total  ! /n_i_total - Total ion density (sum over species)
  type (ids_summary_dynamic_flt_1d_2) :: zeff  ! /zeff - Effective charge
  type (ids_summary_dynamic_flt_1d_2) :: momentum_tor  ! /momentum_tor - Total plasma toroidal momentum, summed over ion species and electrons
  type (ids_summary_species_level_3) :: velocity_tor  ! /velocity_tor - Ion toroidal rotation velocity, per species
  type (ids_summary_dynamic_flt_1d_2) :: q  ! /q - Safety factor
  type (ids_summary_dynamic_flt_1d_2) :: magnetic_shear  ! /magnetic_shear - Magnetic shear, defined as rho_tor/q . dq/drho_tor
endtype

type ids_summary_local  !    Set of locations
  type (ids_summary_local_quantities_r_z) :: magnetic_axis  ! /magnetic_axis - Parameters at magnetic axis
  type (ids_summary_local_quantities) :: edge  ! /edge - Parameters at last closed flux surface
  type (ids_summary_local_quantities) :: pedestal  ! /pedestal - Parameters at pedestal top
  type (ids_summary_local_quantities) :: itb  ! /itb - Parameters at internal transport barrier
  type (ids_summary_local_quantities_no_position) :: limiter  ! /limiter - Parameters at the limiter
  type (ids_summary_local_quantities_no_position),pointer :: divertor_plate(:) => null()  ! /divertor_plate(i) - Parameters at a divertor plate. RAJOUTER NAME DANS LA STRUCTURE dESSOUS
endtype

type ids_summary_sol  !    Scrape-Off-Layer characteristics
  type (ids_summary_dynamic_flt_1d_1) :: t_e_decay_length  ! /t_e_decay_length - Electron temperature radial decay length inv(grad Te/Te)
  type (ids_summary_dynamic_flt_1d_1) :: t_i_average_decay_length  ! /t_i_average_decay_length - Ion temperature (average over ion species) radial decay length inv(grad Ti/Ti)
  type (ids_summary_dynamic_flt_1d_1) :: n_e_decay_length  ! /n_e_decay_length - Electron density radial decay length inv(grad ne/ne)
  type (ids_summary_dynamic_flt_1d_1) :: n_i_total_decay_length  ! /n_i_total_decay_length - Ion density radial decay length inv(grad ni/ni)
  type (ids_summary_dynamic_flt_1d_1) :: heat_flux_e_decay_length  ! /heat_flux_e_decay_length - Electron heat flux radial decay length inv(grad qe/qe)
  type (ids_summary_dynamic_flt_1d_1) :: heat_flux_i_decay_length  ! /heat_flux_i_decay_length - Ion heat flux radial decay length inv(grad qi/qi)
  type (ids_summary_dynamic_flt_1d_1) :: power_radiated  ! /power_radiated - Power radiated from the SOL
  type (ids_summary_dynamic_flt_1d_1) :: pressure_neutral  ! /pressure_neutral - Neutral pressure in the SOL
endtype

type ids_summary_average_quantities  !    Set of average quantities
  type (ids_summary_dynamic_flt_1d_1) :: t_e  ! /t_e - Electron temperature
  type (ids_summary_dynamic_flt_1d_1) :: t_i_average  ! /t_i_average - Ion temperature (average over ion species)
  type (ids_summary_dynamic_flt_1d_1) :: n_e  ! /n_e - Electron density
  type (ids_summary_species_level_2) :: n_i  ! /n_i - Ion density per species
  type (ids_summary_dynamic_flt_1d_1) :: n_i_total  ! /n_i_total - Total ion density (sum over species)
  type (ids_summary_dynamic_flt_1d_1) :: zeff  ! /zeff - Effective charge
endtype

type ids_summary_global_quantities  !    Various global quantities calculated from the fields solved in the transport equations and from the Derived Profiles
  type (ids_summary_dynamic_flt_1d_1) :: ip  ! /ip - Total plasma current
  type (ids_summary_dynamic_flt_1d_1) :: current_non_inductive  ! /current_non_inductive - Total non-inductive parallel current
  type (ids_summary_dynamic_flt_1d_1) :: current_bootstrap  ! /current_bootstrap - Bootstrap parallel current
  type (ids_summary_dynamic_flt_1d_1) :: current_ohm  ! /current_ohm - Ohmic parallel current
  type (ids_summary_dynamic_flt_1d_1) :: current_alignment  ! /current_alignment - Figure of merit of the alignment of the current profile sources, defined in the following reference:
  type (ids_summary_dynamic_flt_1d_1) :: v_loop  ! /v_loop - LCFS loop voltage
  type (ids_summary_dynamic_flt_1d_1) :: li  ! /li - Internal inductance. The li_3 definition is used, i.e. li_3 = 2/R0/mu0^2/Ip^2 * int(Bp^2 dV).
  type (ids_summary_dynamic_flt_1d_1) :: beta_tor  ! /beta_tor - Toroidal beta, defined as the volume-averaged total perpendicular pressure divided by (B0^2/(2*mu0))
  type (ids_summary_dynamic_flt_1d_1) :: beta_tor_norm  ! /beta_tor_norm - Normalised toroidal beta, defined as 100 * beta_tor * a[m] * B0 [T] / ip [MA]
  type (ids_summary_dynamic_flt_1d_1) :: beta_pol  ! /beta_pol - Poloidal beta. Defined as betap = 4 int(p dV) / [R_0 * mu_0 * Ip^2]
  type (ids_summary_dynamic_flt_1d_1) :: energy_diamagnetic  ! /energy_diamagnetic - Plasma energy content = 3/2 * integral over the plasma volume of the total perpendicular pressure
  type (ids_summary_dynamic_flt_1d_1) :: energy_total  ! /energy_total - Plasma energy content = 3/2 * integral over the plasma volume of the total kinetic pressure
  type (ids_summary_dynamic_flt_1d_1) :: energy_thermal  ! /energy_thermal - Thermal plasma energy content = 3/2 * integral over the plasma volume of the thermal pressure
  type (ids_summary_dynamic_flt_1d_1) :: energy_b_field_pol  ! /energy_b_field_pol - Thermal plasma energy content = 1/(2.mu0) * integral over the plasma volume of b_field_pol^2
  type (ids_summary_dynamic_flt_1d_1) :: volume  ! /volume - Volume of the confined plasma
  type (ids_summary_dynamic_int_1d_1) :: h_mode  ! /h_mode - H-mode flag: 0 when the plasma is in L-mode and 1 when in H-mode
  type (ids_summary_constant_flt_0d) :: r0  ! /r0 - Reference major radius where the vacuum toroidal magnetic field is given (usually a fixed position s
  type (ids_summary_dynamic_flt_1d_1) :: b0  ! /b0 - Vacuum toroidal field at R0 [T]; Positive sign means anti-clockwise when viewed from above. The prod
  type (ids_summary_dynamic_flt_1d_1) :: h_98  ! /h_98 - Energy confinement time enhancement factor over the IPB98(y,2) scaling
  type (ids_summary_dynamic_flt_1d_1) :: tau_energy  ! /tau_energy - Energy confinement time
  type (ids_summary_dynamic_flt_1d_1) :: tau_helium  ! /tau_helium - Helium confinement time
  type (ids_summary_dynamic_flt_1d_1) :: tau_resistive  ! /tau_resistive - Current diffusion characteristic time
  type (ids_summary_dynamic_flt_1d_1) :: resistance  ! /resistance - Plasma electric resistance
endtype

type ids_summary_neutron_reaction  !    Neutron fluxes per reaction
  type (ids_summary_dynamic_flt_1d_3) :: total  ! /total - Total neutron flux coming from this reaction
  type (ids_summary_dynamic_flt_1d_3) :: thermal  ! /thermal - Neutron flux coming from thermal plasma
  type (ids_summary_dynamic_flt_1d_3) :: beam_thermal  ! /beam_thermal - Neutron flux coming from NBI beam - plasma reactions
  type (ids_summary_dynamic_flt_1d_3) :: beam_beam  ! /beam_beam - Neutron flux coming from NBI beam self reactions
endtype

type ids_summary_neutron  !    Description of neutron fluxes
  type (ids_summary_dynamic_flt_1d_2) :: total  ! /total - Total neutron flux from all reactions
  type (ids_summary_dynamic_flt_1d_2) :: thermal  ! /thermal - Neutron flux from all plasma thermal reactions
  type (ids_summary_neutron_reaction) :: dd  ! /dd - Neutron fluxes from DD reactions
  type (ids_summary_neutron_reaction) :: dt  ! /dt - Neutron fluxes from DT reactions
  type (ids_summary_neutron_reaction) :: tt  ! /tt - Neutron fluxes from TT reactions
endtype

type ids_summary_fusion  !    Fusion reactions
  type (ids_summary_dynamic_flt_1d_1) :: power  ! /power - Power coupled to the plasma by fusion reactions
  type (ids_summary_dynamic_flt_1d_1) :: current  ! /current - Parallel current driven by this fusion reactions
  type (ids_summary_neutron) :: neutron_fluxes  ! /neutron_fluxes - Neutron fluxes from various reactions
  type (ids_summary_dynamic_flt_1d_1) :: neutron_power_total  ! /neutron_power_total - Total neutron power (from all reactions)
endtype

type ids_summary_runaways  !    Runaway electrons
  type (ids_summary_dynamic_flt_1d_1) :: particles  ! /particles - Number of runaway electrons
  type (ids_summary_dynamic_flt_1d_1) :: current  ! /current - Parallel current driven by the runaway electrons
endtype

type ids_summary_h_cd_nbi  !    NBI unit
  type (ids_summary_plasma_composition_species) :: species  ! /species - Injected species
  type (ids_summary_dynamic_flt_1d_2) :: power  ! /power - NBI power coupled to the plasma by this unit
  type (ids_summary_dynamic_flt_1d_2) :: current  ! /current - Parallel current driven by this NBI unit
  type (ids_summary_rzphi0d_static) :: position  ! /position - R, Z, Phi position of the NBI unit centre
  type (ids_summary_static_flt_0d) :: tangency_radius  ! /tangency_radius - Tangency radius (major radius where the central line of a NBI unit is tangent to a circle around the
  type (ids_summary_static_flt_0d) :: angle  ! /angle - Angle of inclination between a beamlet at the centre of the injection unit surface and the horiontal
  type (ids_summary_static_int_0d) :: direction  ! /direction - Direction of the beam seen from above the torus: -1 = clockwise; 1 = counter clockwise
  type (ids_summary_dynamic_flt_1d_2) :: energy  ! /energy - Full energy of the injected species (acceleration of a single atom)
  type (ids_summary_dynamic_flt_2d_fraction_2) :: beam_current_fraction  ! /beam_current_fraction - Fractions of beam current distributed among the different energies, the first index corresponds to t
  type (ids_summary_dynamic_flt_2d_fraction_2) :: beam_power_fraction  ! /beam_power_fraction - Fractions of beam power distributed among the different energies, the first index corresponds to the
endtype

type ids_summary_h_cd_ec  !    ECRH/CD related parameters
  type (ids_summary_dynamic_flt_1d_2) :: frequency  ! /frequency - ECRH frequency
  type (ids_summary_dynamic_flt_1d_2) :: position  ! /position - Position of the maximum of the ECRH power deposition, in rho_tor_norm
  type (ids_summary_dynamic_int_1d_2) :: polarisation  ! /polarisation - Polarisation of the ECRH waves (0 = O mode, 1 = X mode)
  type (ids_summary_dynamic_int_1d_2) :: harmonic  ! /harmonic - Harmonic number of the absorbed ECRH waves
  type (ids_summary_dynamic_flt_1d_2) :: angle_tor  ! /angle_tor - Toroidal angle of ECRH at resonance
  type (ids_summary_dynamic_flt_1d_2) :: angle_pol  ! /angle_pol - Poloidal angle of ECRH at resonance
  type (ids_summary_dynamic_flt_1d_2) :: power  ! /power - Electron cyclotron heating power coupled to the plasma
  type (ids_summary_dynamic_flt_1d_2) :: current  ! /current - Parallel current driven by EC waves
  type (ids_summary_dynamic_flt_1d_2) :: energy_fast  ! /energy_fast - Fast particle energy content driven by EC waves
endtype

type ids_summary_h_cd_lh  !    LHCD related parameters
  type (ids_summary_dynamic_flt_1d_2) :: frequency  ! /frequency - LH wave frequency
  type (ids_summary_dynamic_flt_1d_2) :: position  ! /position - Position of the maximum of the LH power deposition, in rho_tor_norm
  type (ids_summary_dynamic_flt_1d_2) :: n_parallel  ! /n_parallel - Main parallel refractive index of LH waves at launch
  type (ids_summary_dynamic_flt_1d_2) :: power  ! /power - LH heating power coupled to the plasma
  type (ids_summary_dynamic_flt_1d_2) :: current  ! /current - Parallel current driven by LH waves
  type (ids_summary_dynamic_flt_1d_2) :: energy_fast  ! /energy_fast - Fast particle energy content driven by LH waves
endtype

type ids_summary_h_cd_ic  !    ICRH related parameters
  type (ids_summary_dynamic_flt_1d_2) :: frequency  ! /frequency - ICRH frequency
  type (ids_summary_dynamic_flt_1d_2) :: position  ! /position - Position of the maximum of the ICRH power deposition, in rho_tor_norm
  type (ids_summary_dynamic_int_1d_2) :: n_tor  ! /n_tor - Main toroidal mode number of IC waves
  type (ids_summary_dynamic_flt_1d_2) :: k_perpendicular  ! /k_perpendicular - Main perpendicular wave number of IC waves
  type (ids_summary_dynamic_flt_1d_2) :: e_field_plus_minus_ratio  ! /e_field_plus_minus_ratio - Average E+/E- power ratio of IC waves
  type (ids_summary_dynamic_int_1d_2) :: harmonic  ! /harmonic - Harmonic number of the absorbed ICRH waves
  type (ids_summary_dynamic_flt_1d_2) :: phase  ! /phase - Phase between straps
  type (ids_summary_dynamic_flt_1d_2) :: power  ! /power - IC heating power coupled to the plasma
  type (ids_summary_dynamic_flt_1d_2) :: current  ! /current - Parallel current driven by IC waves
  type (ids_summary_dynamic_flt_1d_2) :: energy_fast  ! /energy_fast - Fast particle energy content driven by IC waves
endtype

type ids_summary_h_cd  !    Heating and current drive related parameters
  type (ids_summary_h_cd_ec),pointer :: ec(:) => null()  ! /ec(i) - Set of ECRH/ECCD launchers
  type (ids_summary_h_cd_nbi),pointer :: nbi(:) => null()  ! /nbi(i) - Set of NBI units
  type (ids_summary_h_cd_ic),pointer :: ic(:) => null()  ! /ic(i) - Set of ICRH launchers
  type (ids_summary_h_cd_lh),pointer :: lh(:) => null()  ! /lh(i) - Set of LHCD launchers
endtype

type ids_summary_disruption  !    Disruption related parameters
  type (ids_summary_constant_flt_0d) :: time  ! /time - Time of the disruption
  type (ids_summary_constant_flt_0d) :: time_radiated_power_max  ! /time_radiated_power_max - Time of maximum radiated power, relative to the time of the disruption
  type (ids_summary_constant_flt_0d) :: time_half_ip  ! /time_half_ip - Time at which the plasma current has fallen to half of the initial current at the start of the disru
  type (ids_summary_constant_int_0d) :: vertical_displacement  ! /vertical_displacement - Direction of the plasma vertical displacement just before the disruption 1 (upwards) / 0 (no displac
  type (ids_summary_constant_int_0d) :: mitigation_valve  ! /mitigation_valve - Flag indicating whether any disruption mitigation valve has been used (1) or none (0)
endtype

type ids_summary  !    Summary of physics quantities from a simulation or an experiment. Dynamic quantities are either taken at given time slices (indica
  type (ids_ids_properties) :: ids_properties  ! /summary/ids_properties -
  type (ids_summary_global_quantities) :: global_quantities  ! /summary/global_quantities - Various global quantities derived from the profiles
  type (ids_summary_local) :: local  ! /summary/local - Plasma parameter values at different locations
  type (ids_summary_average_quantities) :: line_average  ! /summary/line_average - Line average plasma parameters
  type (ids_summary_average_quantities) :: volume_average  ! /summary/volume_average - Volume average plasma parameters
  type (ids_summary_disruption) :: disruption  ! /summary/disruption - Disruption characteristics, if the pulse is terminated by a disruption
  type (ids_summary_h_cd) :: heating_current_drive  ! /summary/heating_current_drive - Heating and current drive parameters
  type (ids_summary_fusion) :: fusion  ! /summary/fusion - Fusion reactions
  type (ids_summary_runaways) :: runaways  ! /summary/runaways - Runaway electrons
  type (ids_summary_sol) :: scrape_off_layer  ! /summary/scrape_off_layer - Scrape-Off-Layer (SOL) characteristics
  real(ids_real),pointer  :: time_width(:) => null()     ! /summary/time_width - In case the time-dependent quantities of this IDS are averaged over a time interval, this node is th
  real(ids_real),pointer  :: time_width_error_upper(:) => null()
  real(ids_real),pointer  :: time_width_error_lower(:) => null()
  integer(ids_int) :: time_width_error_index=ids_int_invalid

  type (ids_code) :: code  ! /summary/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include temporary/dd_temporary.xsd
type ids_temporary_constant_quantities_float_0d  !    Temporary constant Float_0D
  real(ids_real)  :: value=ids_real_invalid       ! /value - Value
  real(ids_real)  :: value_error_upper=ids_real_invalid
  real(ids_real)  :: value_error_lower=ids_real_invalid
  integer(ids_int) :: value_error_index=ids_int_invalid

  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

type ids_temporary_constant_quantities_int_0d  !    Temporary constant INT_0D
  integer(ids_int)  :: value=ids_int_invalid       ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

type ids_temporary_constant_quantities_string_0d  !    Temporary constant STR_0D
  character(len=ids_string_length), dimension(:), pointer ::value => null()       ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

type ids_temporary_constant_quantities_float_1d  !    Temporary constant Float_1D
  real(ids_real),pointer  :: value(:) => null()     ! /value - Value
  real(ids_real),pointer  :: value_error_upper(:) => null()
  real(ids_real),pointer  :: value_error_lower(:) => null()
  integer(ids_int) :: value_error_index=ids_int_invalid

  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

type ids_temporary_constant_quantities_int_1d  !    Temporary constant INT_1D
  integer(ids_int),pointer  :: value(:) => null()      ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

type ids_temporary_constant_quantities_string_1d  !    Temporary constant STR_1D
  character(len=ids_string_length), dimension(:), pointer ::value => null()       ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

! SPECIAL STRUCTURE data / time
type ids_temporary_dynamic_quantities_float_1d_value  !    Value
  real(ids_real), pointer  :: data(:) => null()     ! /value - Value
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_temporary_dynamic_quantities_float_1d  !    Temporary dynamic Float_1D
  type (ids_temporary_dynamic_quantities_float_1d_value) :: value  ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

! SPECIAL STRUCTURE data / time
type ids_temporary_dynamic_quantities_int_1d_value  !    Value
  integer(ids_int), pointer  :: data(:) => null()      ! /value - Value
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_temporary_dynamic_quantities_int_1d  !    Temporary dynamic Int_1D
  type (ids_temporary_dynamic_quantities_int_1d_value) :: value  ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

type ids_temporary_constant_quantities_float_2d  !    Temporary constant Float_2D
  real(ids_real),pointer  :: value(:,:) => null()     ! /value - Value
  real(ids_real),pointer  :: value_error_upper(:,:) => null()
  real(ids_real),pointer  :: value_error_lower(:,:) => null()
  integer(ids_int) :: value_error_index=ids_int_invalid

  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

type ids_temporary_constant_quantities_int_2d  !    Temporary constant INT_2D
  integer(ids_int),pointer  :: value(:,:) => null()     ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

! SPECIAL STRUCTURE data / time
type ids_temporary_dynamic_quantities_float_2d_value  !    Value
  real(ids_real), pointer  :: data(:,:) => null()     ! /value - Value
  real(ids_real), pointer  :: data_error_upper(:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_temporary_dynamic_quantities_float_2d  !    Temporary dynamic Float_2D
  type (ids_temporary_dynamic_quantities_float_2d_value) :: value  ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

! SPECIAL STRUCTURE data / time
type ids_temporary_dynamic_quantities_int_2d_value  !    Value
  integer(ids_int), pointer  :: data(:,:) => null()     ! /value - Value
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_temporary_dynamic_quantities_int_2d  !    Temporary dynamic INT_2D
  type (ids_temporary_dynamic_quantities_int_2d_value) :: value  ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

type ids_temporary_constant_quantities_float_3d  !    Temporary constant Float_3D
  real(ids_real),pointer  :: value(:,:,:) => null()     ! /value - Value
  real(ids_real),pointer  :: value_error_upper(:,:,:) => null()
  real(ids_real),pointer  :: value_error_lower(:,:,:) => null()
  integer(ids_int) :: value_error_index=ids_int_invalid

  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

type ids_temporary_constant_quantities_int_3d  !    Temporary constant INT_3D
  integer(ids_int),pointer  :: value(:,:,:) => null()     ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

! SPECIAL STRUCTURE data / time
type ids_temporary_dynamic_quantities_float_3d_value  !    Value
  real(ids_real), pointer  :: data(:,:,:) => null()     ! /value - Value
  real(ids_real), pointer  :: data_error_upper(:,:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_temporary_dynamic_quantities_float_3d  !    Temporary dynamic Float_3D
  type (ids_temporary_dynamic_quantities_float_3d_value) :: value  ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

! SPECIAL STRUCTURE data / time
type ids_temporary_dynamic_quantities_int_3d_value  !    Value
  integer(ids_int), pointer  :: data(:,:,:) => null()     ! /value - Value
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_temporary_dynamic_quantities_int_3d  !    Temporary dynamic INT_3D
  type (ids_temporary_dynamic_quantities_int_3d_value) :: value  ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

! SPECIAL STRUCTURE data / time
type ids_temporary_dynamic_quantities_float_4d_value  !    Value
  real(ids_real), pointer  :: data(:,:,:,:) => null()     ! /value - Value
  real(ids_real), pointer  :: data_error_upper(:,:,:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:,:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_temporary_dynamic_quantities_float_4d  !    Temporary dynamic Float_4D
  type (ids_temporary_dynamic_quantities_float_4d_value) :: value  ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

type ids_temporary_constant_quantities_float_4d  !    Temporary constant Float_4D
  real(ids_real),pointer  :: value(:,:,:,:) => null()     ! /value - Value
  real(ids_real),pointer  :: value_error_upper(:,:,:,:) => null()
  real(ids_real),pointer  :: value_error_lower(:,:,:,:) => null()
  integer(ids_int) :: value_error_index=ids_int_invalid

  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

! SPECIAL STRUCTURE data / time
type ids_temporary_dynamic_quantities_float_5d_value  !    Value
  real(ids_real), pointer  :: data(:,:,:,:,:) => null()     ! /value - Value
  real(ids_real), pointer  :: data_error_upper(:,:,:,:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:,:,:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_temporary_dynamic_quantities_float_5d  !    Temporary dynamic Float_5D
  type (ids_temporary_dynamic_quantities_float_5d_value) :: value  ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

type ids_temporary_constant_quantities_float_5d  !    Temporary constant Float_5D
  real(ids_real),pointer  :: value(:,:,:,:,:) => null()     ! /value - Value
  real(ids_real),pointer  :: value_error_upper(:,:,:,:,:) => null()
  real(ids_real),pointer  :: value_error_lower(:,:,:,:,:) => null()
  integer(ids_int) :: value_error_index=ids_int_invalid

  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

! SPECIAL STRUCTURE data / time
type ids_temporary_dynamic_quantities_float_6d_value  !    Value
  real(ids_real), pointer  :: data(:,:,:,:,:,:) => null()     ! /value - Value
  real(ids_real), pointer  :: data_error_upper(:,:,:,:,:,:) => null()
  real(ids_real), pointer  :: data_error_lower(:,:,:,:,:,:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_temporary_dynamic_quantities_float_6d  !    Temporary dynamic Float_6D
  type (ids_temporary_dynamic_quantities_float_6d_value) :: value  ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

type ids_temporary_constant_quantities_float_6d  !    Temporary constant Float_6D
  real(ids_real),pointer  :: value(:,:,:,:,:,:) => null()     ! /value - Value
  real(ids_real),pointer  :: value_error_upper(:,:,:,:,:,:) => null()
  real(ids_real),pointer  :: value_error_lower(:,:,:,:,:,:) => null()
  integer(ids_int) :: value_error_index=ids_int_invalid

  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

type ids_temporary  !    Storage of undeclared data model components
  type (ids_ids_properties) :: ids_properties  ! /temporary/ids_properties -
  type (ids_temporary_constant_quantities_float_0d),pointer :: constant_float0d(:) => null()  ! /temporary/constant_float0d(i) - Constant 0D float
  type (ids_temporary_constant_quantities_int_0d),pointer :: constant_integer0d(:) => null()  ! /temporary/constant_integer0d(i) - Constant 0D integer
  type (ids_temporary_constant_quantities_string_0d),pointer :: constant_string0d(:) => null()  ! /temporary/constant_string0d(i) - Constant 0D string
  type (ids_temporary_constant_quantities_int_1d),pointer :: constant_integer1d(:) => null()  ! /temporary/constant_integer1d(i) - Constant 1D integer
  type (ids_temporary_constant_quantities_string_1d),pointer :: constant_string1d(:) => null()  ! /temporary/constant_string1d(i) - Constant 1D string
  type (ids_temporary_constant_quantities_float_1d),pointer :: constant_float1d(:) => null()  ! /temporary/constant_float1d(i) - Constant 1D float
  type (ids_temporary_dynamic_quantities_float_1d),pointer :: dynamic_float1d(:) => null()  ! /temporary/dynamic_float1d(i) - Dynamic 1D float
  type (ids_temporary_dynamic_quantities_int_1d),pointer :: dynamic_integer1d(:) => null()  ! /temporary/dynamic_integer1d(i) - Dynamic 1D integer
  type (ids_temporary_constant_quantities_float_2d),pointer :: constant_float2d(:) => null()  ! /temporary/constant_float2d(i) - Constant 2D float
  type (ids_temporary_constant_quantities_int_2d),pointer :: constant_integer2d(:) => null()  ! /temporary/constant_integer2d(i) - Constant 2D integer
  type (ids_temporary_dynamic_quantities_float_2d),pointer :: dynamic_float2d(:) => null()  ! /temporary/dynamic_float2d(i) - Dynamic 2D float
  type (ids_temporary_dynamic_quantities_int_2d),pointer :: dynamic_integer2d(:) => null()  ! /temporary/dynamic_integer2d(i) - Dynamic 2D integer
  type (ids_temporary_constant_quantities_float_3d),pointer :: constant_float3d(:) => null()  ! /temporary/constant_float3d(i) - Constant 3D float
  type (ids_temporary_constant_quantities_int_3d),pointer :: constant_integer3d(:) => null()  ! /temporary/constant_integer3d(i) - Constant 3D integer
  type (ids_temporary_dynamic_quantities_float_3d),pointer :: dynamic_float3d(:) => null()  ! /temporary/dynamic_float3d(i) - Dynamic 3D float
  type (ids_temporary_dynamic_quantities_int_3d),pointer :: dynamic_integer3d(:) => null()  ! /temporary/dynamic_integer3d(i) - Dynamic 3D integer
  type (ids_temporary_constant_quantities_float_4d),pointer :: constant_float4d(:) => null()  ! /temporary/constant_float4d(i) - Constant 4D float
  type (ids_temporary_dynamic_quantities_float_4d),pointer :: dynamic_float4d(:) => null()  ! /temporary/dynamic_float4d(i) - Dynamic 4D float
  type (ids_temporary_constant_quantities_float_5d),pointer :: constant_float5d(:) => null()  ! /temporary/constant_float5d(i) - Constant 5D float
  type (ids_temporary_dynamic_quantities_float_5d),pointer :: dynamic_float5d(:) => null()  ! /temporary/dynamic_float5d(i) - Dynamic 5D float
  type (ids_temporary_constant_quantities_float_6d),pointer :: constant_float6d(:) => null()  ! /temporary/constant_float6d(i) - Constant 6D float
  type (ids_temporary_dynamic_quantities_float_6d),pointer :: dynamic_float6d(:) => null()  ! /temporary/dynamic_float6d(i) - Dynamic 6D float
  type (ids_code) :: code  ! /temporary/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include tf/dd_tf.xsd
type ids_tf_coil_conductor_elements  !    Elements descibring the conductor contour
  character(len=ids_string_length), dimension(:), pointer ::names => null()       ! /names - Name or description of every element
  integer(ids_int),pointer  :: types(:) => null()      ! /types - Type of every element: 1: line segment, its ends are given by the start and end points; index = 2: a
  type (ids_rzphi1d_static) :: start_points  ! /start_points - Position of the start point of every element
  type (ids_rzphi1d_static) :: intermediate_points  ! /intermediate_points - Position of an intermediate point along the arc of circle, for every element, providing the orientat
  type (ids_rzphi1d_static) :: end_points  ! /end_points - Position of the end point of every element. Meaningful only if type/index = 1 or 2, fill with defaul
  type (ids_rzphi1d_static) :: centres  ! /centres - Position of the centre of the arc of a circle of every element (meaningful only if type/index = 2 or
endtype

! SPECIAL STRUCTURE data / time
type ids_tf_coil_conductor_current  !    Current in the conductor
  real(ids_real), pointer  :: data(:) => null()     ! /current - Current in the conductor
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_tf_coil_conductor_voltage  !    Voltage on the conductor terminals
  real(ids_real), pointer  :: data(:) => null()     ! /voltage - Voltage on the conductor terminals
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_tf_coil_conductor  !    Description of a conductor
  type (ids_tf_coil_conductor_elements) :: elements  ! /elements - Set of geometrical elements (line segments and/or arcs of a circle) describing the contour of the TF
  type (ids_delta_rzphi1d_static) :: cross_section  ! /cross_section - The cross-section perpendicular to the TF conductor contour is described by a series of contour poin
  real(ids_real)  :: resistance=ids_real_invalid       ! /resistance - conductor resistance
  real(ids_real)  :: resistance_error_upper=ids_real_invalid
  real(ids_real)  :: resistance_error_lower=ids_real_invalid
  integer(ids_int) :: resistance_error_index=ids_int_invalid

  type (ids_tf_coil_conductor_current) :: current  ! /current - Current in the conductor
  type (ids_tf_coil_conductor_voltage) :: voltage  ! /voltage - Voltage on the conductor terminals
endtype

! SPECIAL STRUCTURE data / time
type ids_tf_coil_current  !    Current in the coil
  real(ids_real), pointer  :: data(:) => null()     ! /current - Current in the coil
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_tf_coil_voltage  !    Voltage on the coil terminals
  real(ids_real), pointer  :: data(:) => null()     ! /voltage - Voltage on the coil terminals
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_tf_coil  !    Description of a given coil
  type (ids_tf_coil_conductor),pointer :: conductor(:) => null()  ! /conductor(i) - Set of conductors inside the coil. The structure can be used with size 1 for a simplified descriptio
  real(ids_real)  :: turns=ids_real_invalid       ! /turns - Number of total turns in a toroidal field coil. May be a fraction when describing the coil connectio
  real(ids_real)  :: turns_error_upper=ids_real_invalid
  real(ids_real)  :: turns_error_lower=ids_real_invalid
  integer(ids_int) :: turns_error_index=ids_int_invalid

  real(ids_real)  :: resistance=ids_real_invalid       ! /resistance - Coil resistance
  real(ids_real)  :: resistance_error_upper=ids_real_invalid
  real(ids_real)  :: resistance_error_lower=ids_real_invalid
  integer(ids_int) :: resistance_error_index=ids_int_invalid

  type (ids_tf_coil_current) :: current  ! /current - Current in the coil
  type (ids_tf_coil_voltage) :: voltage  ! /voltage - Voltage on the coil terminals
endtype

type ids_tf_ggd  !    Toroidal field map represented on ggd
  type (ids_generic_grid_dynamic) :: grid  ! /grid - Grid description
  type (ids_generic_grid_scalar),pointer :: b_field_r(:) => null()  ! /b_field_r(i) - R component of the vacuum magnetic field, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: b_field_z(:) => null()  ! /b_field_z(i) - Z component of the vacuum magnetic field, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: b_field_tor(:) => null()  ! /b_field_tor(i) - Toroidal component of the vacuum magnetic field, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: a_field_r(:) => null()  ! /a_field_r(i) - R component of the vacuum vector potential, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: a_field_z(:) => null()  ! /a_field_z(i) - Z component of the vacuum vector potential, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: a_field_tor(:) => null()  ! /a_field_tor(i) - Toroidal component of the vacuum vector potential, given on various grid subsets
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

! SPECIAL STRUCTURE data / time
type ids_tf_b_field_tor_vacuum_r  !    Vacuum field times major radius in the toroidal field magnet. Positive sign means anti-clockwise when viewed from above
  real(ids_real), pointer  :: data(:) => null()     ! /tf/b_field_tor_vacuum_r - Vacuum field times major radius in the toroidal field magnet. Positive sign means anti-clockwise whe
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_tf  !    Toroidal field coils
  type (ids_ids_properties) :: ids_properties  ! /tf/ids_properties -
  real(ids_real)  :: r0=ids_real_invalid       ! /tf/r0 - Reference major radius of the device (from the official description of the device). This node is the
  real(ids_real)  :: r0_error_upper=ids_real_invalid
  real(ids_real)  :: r0_error_lower=ids_real_invalid
  integer(ids_int) :: r0_error_index=ids_int_invalid

  integer(ids_int)  :: is_periodic=ids_int_invalid       ! /tf/is_periodic - Flag indicating whether coils are described one by one in the coil() structure (flag=0) or whether t
  integer(ids_int)  :: coils_n=ids_int_invalid       ! /tf/coils_n - Number of coils around the torus, in case is_periodic = 1
  type (ids_tf_coil),pointer :: coil(:) => null()  ! /tf/coil(i) - Set of coils around the tokamak
  type (ids_tf_ggd),pointer :: field_map(:) => null()  ! /tf/field_map(i) - Map of the vacuum field at various time slices, represented using the generic grid description
  type (ids_tf_b_field_tor_vacuum_r) :: b_field_tor_vacuum_r  ! /tf/b_field_tor_vacuum_r - Vacuum field times major radius in the toroidal field magnet. Positive sign means anti-clockwise whe
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include thomson_scattering/dd_thomson_scattering.xsd
! SPECIAL STRUCTURE data / time
type ids_thomson_scattering_channel_t_e  !    Electron temperature
  real(ids_real), pointer  :: data(:) => null()     ! /t_e - Electron temperature
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_thomson_scattering_channel_n_e  !    Electron density
  real(ids_real), pointer  :: data(:) => null()     ! /n_e - Electron density
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_thomson_scattering_channel  !    Thomson scattering channel
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the channel
  character(len=ids_string_length), dimension(:), pointer ::identifier => null()       ! /identifier - ID of the channel
  type (ids_rzphi0d_static) :: position  ! /position - Position of the measurements (intersection between laser beam and line of sight)
  type (ids_thomson_scattering_channel_t_e) :: t_e  ! /t_e - Electron temperature
  type (ids_thomson_scattering_channel_n_e) :: n_e  ! /n_e - Electron density
endtype

type ids_thomson_scattering  !    Thomson scattering diagnostic
  type (ids_ids_properties) :: ids_properties  ! /thomson_scattering/ids_properties -
  type (ids_thomson_scattering_channel),pointer :: channel(:) => null()  ! /thomson_scattering/channel(i) - Set of channels (lines-of-sight)
  type (ids_code) :: code  ! /thomson_scattering/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include transport_solver_numerics/dd_transport_solver_numerics.xsd
type ids_numerics_profiles_1d_derivatives_charge_state_d  !    Quantities related to a given charge state, derivatives with respect to a given quantity
  real(ids_real),pointer  :: temperature(:) => null()     ! /temperature - Temperature
  real(ids_real),pointer  :: temperature_error_upper(:) => null()
  real(ids_real),pointer  :: temperature_error_lower(:) => null()
  integer(ids_int) :: temperature_error_index=ids_int_invalid

  real(ids_real),pointer  :: density(:) => null()     ! /density - Density (thermal+non-thermal)
  real(ids_real),pointer  :: density_error_upper(:) => null()
  real(ids_real),pointer  :: density_error_lower(:) => null()
  integer(ids_int) :: density_error_index=ids_int_invalid

  real(ids_real),pointer  :: density_fast(:) => null()     ! /density_fast - Density of fast (non-thermal) particles
  real(ids_real),pointer  :: density_fast_error_upper(:) => null()
  real(ids_real),pointer  :: density_fast_error_lower(:) => null()
  integer(ids_int) :: density_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure(:) => null()     ! /pressure - Pressure
  real(ids_real),pointer  :: pressure_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_error_lower(:) => null()
  integer(ids_int) :: pressure_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_fast_perpendicular(:) => null()     ! /pressure_fast_perpendicular - Fast (non-thermal) perpendicular pressure
  real(ids_real),pointer  :: pressure_fast_perpendicular_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_fast_perpendicular_error_lower(:) => null()
  integer(ids_int) :: pressure_fast_perpendicular_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_fast_parallel(:) => null()     ! /pressure_fast_parallel - Fast (non-thermal) parallel pressure
  real(ids_real),pointer  :: pressure_fast_parallel_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_fast_parallel_error_lower(:) => null()
  integer(ids_int) :: pressure_fast_parallel_error_index=ids_int_invalid

  real(ids_real),pointer  :: velocity_tor(:) => null()     ! /velocity_tor - Toroidal velocity
  real(ids_real),pointer  :: velocity_tor_error_upper(:) => null()
  real(ids_real),pointer  :: velocity_tor_error_lower(:) => null()
  integer(ids_int) :: velocity_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: velocity_pol(:) => null()     ! /velocity_pol - Poloidal velocity
  real(ids_real),pointer  :: velocity_pol_error_upper(:) => null()
  real(ids_real),pointer  :: velocity_pol_error_lower(:) => null()
  integer(ids_int) :: velocity_pol_error_index=ids_int_invalid

endtype

type ids_numerics_profiles_1d_derivatives_charge_state  !    Quantities related to a given charge state
  real(ids_real)  :: z_min=ids_real_invalid       ! /z_min - Minimum Z of the charge state bundle
  real(ids_real)  :: z_min_error_upper=ids_real_invalid
  real(ids_real)  :: z_min_error_lower=ids_real_invalid
  integer(ids_int) :: z_min_error_index=ids_int_invalid

  real(ids_real)  :: z_max=ids_real_invalid       ! /z_max - Maximum Z of the charge state bundle
  real(ids_real)  :: z_max_error_upper=ids_real_invalid
  real(ids_real)  :: z_max_error_lower=ids_real_invalid
  integer(ids_int) :: z_max_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  integer(ids_int)  :: is_neutral=ids_int_invalid       ! /is_neutral - Flag specifying if this state corresponds to a neutral (1) or not (0)
  type (ids_identifier_dynamic_aos3) :: neutral_type  ! /neutral_type - Neutral type (if the considered state is a neutral), in terms of energy. ID =1: cold; 2: thermal; 3:
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  type (ids_numerics_profiles_1d_derivatives_charge_state_d) :: d_drho_tor_norm  ! /d_drho_tor_norm - Derivatives with respect to the normalised toroidal flux
  type (ids_numerics_profiles_1d_derivatives_charge_state_d) :: d2_drho_tor_norm2  ! /d2_drho_tor_norm2 - Second derivatives with respect to the normalised toroidal flux
  type (ids_numerics_profiles_1d_derivatives_charge_state_d) :: d_dt  ! /d_dt - Derivatives with respect to time
endtype

type ids_numerics_profiles_1d_derivatives_ion_d  !    Quantities related to an ion species, derivatives with respect to a given quantity
  real(ids_real),pointer  :: temperature(:) => null()     ! /temperature - Temperature (average over charge states when multiple charge states are considered)
  real(ids_real),pointer  :: temperature_error_upper(:) => null()
  real(ids_real),pointer  :: temperature_error_lower(:) => null()
  integer(ids_int) :: temperature_error_index=ids_int_invalid

  real(ids_real),pointer  :: density(:) => null()     ! /density - Density (thermal+non-thermal) (sum over charge states when multiple charge states are considered)
  real(ids_real),pointer  :: density_error_upper(:) => null()
  real(ids_real),pointer  :: density_error_lower(:) => null()
  integer(ids_int) :: density_error_index=ids_int_invalid

  real(ids_real),pointer  :: density_fast(:) => null()     ! /density_fast - Density of fast (non-thermal) particles (sum over charge states when multiple charge states are cons
  real(ids_real),pointer  :: density_fast_error_upper(:) => null()
  real(ids_real),pointer  :: density_fast_error_lower(:) => null()
  integer(ids_int) :: density_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure(:) => null()     ! /pressure - Pressure (average over charge states when multiple charge states are considered)
  real(ids_real),pointer  :: pressure_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_error_lower(:) => null()
  integer(ids_int) :: pressure_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_fast_perpendicular(:) => null()     ! /pressure_fast_perpendicular - Fast (non-thermal) perpendicular pressure  (average over charge states when multiple charge states a
  real(ids_real),pointer  :: pressure_fast_perpendicular_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_fast_perpendicular_error_lower(:) => null()
  integer(ids_int) :: pressure_fast_perpendicular_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_fast_parallel(:) => null()     ! /pressure_fast_parallel - Fast (non-thermal) parallel pressure  (average over charge states when multiple charge states are co
  real(ids_real),pointer  :: pressure_fast_parallel_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_fast_parallel_error_lower(:) => null()
  integer(ids_int) :: pressure_fast_parallel_error_index=ids_int_invalid

  real(ids_real),pointer  :: velocity_tor(:) => null()     ! /velocity_tor - Toroidal velocity (average over charge states when multiple charge states are considered)
  real(ids_real),pointer  :: velocity_tor_error_upper(:) => null()
  real(ids_real),pointer  :: velocity_tor_error_lower(:) => null()
  integer(ids_int) :: velocity_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: velocity_pol(:) => null()     ! /velocity_pol - Poloidal velocity (average over charge states when multiple charge states are considered)
  real(ids_real),pointer  :: velocity_pol_error_upper(:) => null()
  real(ids_real),pointer  :: velocity_pol_error_lower(:) => null()
  integer(ids_int) :: velocity_pol_error_index=ids_int_invalid

endtype

type ids_numerics_profiles_1d_derivatives_ion  !    Quantities related to an ion species
  real(ids_real)  :: a=ids_real_invalid       ! /a - Mass of atom
  real(ids_real)  :: a_error_upper=ids_real_invalid
  real(ids_real)  :: a_error_lower=ids_real_invalid
  integer(ids_int) :: a_error_index=ids_int_invalid

  real(ids_real)  :: z_ion=ids_real_invalid       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed)
  real(ids_real)  :: z_ion_error_upper=ids_real_invalid
  real(ids_real)  :: z_ion_error_lower=ids_real_invalid
  integer(ids_int) :: z_ion_error_index=ids_int_invalid

  real(ids_real)  :: z_n=ids_real_invalid       ! /z_n - Nuclear charge
  real(ids_real)  :: z_n_error_upper=ids_real_invalid
  real(ids_real)  :: z_n_error_lower=ids_real_invalid
  integer(ids_int) :: z_n_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  type (ids_numerics_profiles_1d_derivatives_ion_d) :: d_drho_tor_norm  ! /d_drho_tor_norm - Derivatives with respect to the normalised toroidal flux
  type (ids_numerics_profiles_1d_derivatives_ion_d) :: d2_drho_tor_norm2  ! /d2_drho_tor_norm2 - Second derivatives with respect to the normalised toroidal flux
  type (ids_numerics_profiles_1d_derivatives_ion_d) :: d_dt  ! /d_dt - Derivatives with respect to time
  integer(ids_int)  :: multiple_states_flag=ids_int_invalid       ! /multiple_states_flag - Multiple state calculation flag : 0-Only one state is considered; 1-Multiple states are considered a
  type (ids_numerics_profiles_1d_derivatives_charge_state),pointer :: state(:) => null()  ! /state(i) - Quantities related to the different states of the species (ionisation, energy, excitation, ...)
endtype

type ids_numerics_profiles_1d_current_derivatives  !    Derivatives of the current equation primary quantity
  real(ids_real),pointer  :: d_dt(:) => null()     ! /d_dt - Time derivative
  real(ids_real),pointer  :: d_dt_error_upper(:) => null()
  real(ids_real),pointer  :: d_dt_error_lower(:) => null()
  integer(ids_int) :: d_dt_error_index=ids_int_invalid

  real(ids_real),pointer  :: d_drho_tor_norm(:) => null()     ! /d_drho_tor_norm - Derivative with respect to the normalised toroidal flux
  real(ids_real),pointer  :: d_drho_tor_norm_error_upper(:) => null()
  real(ids_real),pointer  :: d_drho_tor_norm_error_lower(:) => null()
  integer(ids_int) :: d_drho_tor_norm_error_index=ids_int_invalid

  real(ids_real),pointer  :: d2_drho_tor_norm2(:) => null()     ! /d2_drho_tor_norm2 - Second derivative with respect to the normalised toroidal flux
  real(ids_real),pointer  :: d2_drho_tor_norm2_error_upper(:) => null()
  real(ids_real),pointer  :: d2_drho_tor_norm2_error_lower(:) => null()
  integer(ids_int) :: d2_drho_tor_norm2_error_index=ids_int_invalid

  real(ids_real),pointer  :: d_drho_tor(:) => null()     ! /d_drho_tor - Derivative with respect to the toroidal flux
  real(ids_real),pointer  :: d_drho_tor_error_upper(:) => null()
  real(ids_real),pointer  :: d_drho_tor_error_lower(:) => null()
  integer(ids_int) :: d_drho_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: d2_drho_tor2(:) => null()     ! /d2_drho_tor2 - Second derivative with respect to the toroidal flux
  real(ids_real),pointer  :: d2_drho_tor2_error_upper(:) => null()
  real(ids_real),pointer  :: d2_drho_tor2_error_lower(:) => null()
  integer(ids_int) :: d2_drho_tor2_error_index=ids_int_invalid

  real(ids_real),pointer  :: d_dt_cphi(:) => null()     ! /d_dt_cphi - Derivative with respect to time, at constant toroidal flux
  real(ids_real),pointer  :: d_dt_cphi_error_upper(:) => null()
  real(ids_real),pointer  :: d_dt_cphi_error_lower(:) => null()
  integer(ids_int) :: d_dt_cphi_error_index=ids_int_invalid

  real(ids_real),pointer  :: d_dt_crho_tor_norm(:) => null()     ! /d_dt_crho_tor_norm - Derivative with respect to time, at constant normalised toroidal flux coordinate
  real(ids_real),pointer  :: d_dt_crho_tor_norm_error_upper(:) => null()
  real(ids_real),pointer  :: d_dt_crho_tor_norm_error_lower(:) => null()
  integer(ids_int) :: d_dt_crho_tor_norm_error_index=ids_int_invalid

endtype

type ids_numerics_profiles_1d_derivatives_depth5  !    Derivatives of a transport equation primary quantity, depth 5 with respect to the 1D grid
  real(ids_real),pointer  :: d_dt(:) => null()     ! /d_dt - Time derivative
  real(ids_real),pointer  :: d_dt_error_upper(:) => null()
  real(ids_real),pointer  :: d_dt_error_lower(:) => null()
  integer(ids_int) :: d_dt_error_index=ids_int_invalid

  real(ids_real),pointer  :: d_drho_tor_norm(:) => null()     ! /d_drho_tor_norm - Derivative with respect to the normalised toroidal flux
  real(ids_real),pointer  :: d_drho_tor_norm_error_upper(:) => null()
  real(ids_real),pointer  :: d_drho_tor_norm_error_lower(:) => null()
  integer(ids_int) :: d_drho_tor_norm_error_index=ids_int_invalid

  real(ids_real),pointer  :: d2_drho_tor_norm2(:) => null()     ! /d2_drho_tor_norm2 - Second derivative with respect to the normalised toroidal flux
  real(ids_real),pointer  :: d2_drho_tor_norm2_error_upper(:) => null()
  real(ids_real),pointer  :: d2_drho_tor_norm2_error_lower(:) => null()
  integer(ids_int) :: d2_drho_tor_norm2_error_index=ids_int_invalid

  real(ids_real),pointer  :: d_drho_tor(:) => null()     ! /d_drho_tor - Derivative with respect to the toroidal flux
  real(ids_real),pointer  :: d_drho_tor_error_upper(:) => null()
  real(ids_real),pointer  :: d_drho_tor_error_lower(:) => null()
  integer(ids_int) :: d_drho_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: d2_drho_tor2(:) => null()     ! /d2_drho_tor2 - Second derivative with respect to the toroidal flux
  real(ids_real),pointer  :: d2_drho_tor2_error_upper(:) => null()
  real(ids_real),pointer  :: d2_drho_tor2_error_lower(:) => null()
  integer(ids_int) :: d2_drho_tor2_error_index=ids_int_invalid

endtype

type ids_numerics_profiles_1d_derivatives_depth4  !    Derivatives of a transport equation primary quantity, depth 4 with respect to the 1D grid
  real(ids_real),pointer  :: d_dt(:) => null()     ! /d_dt - Time derivative
  real(ids_real),pointer  :: d_dt_error_upper(:) => null()
  real(ids_real),pointer  :: d_dt_error_lower(:) => null()
  integer(ids_int) :: d_dt_error_index=ids_int_invalid

  real(ids_real),pointer  :: d_drho_tor_norm(:) => null()     ! /d_drho_tor_norm - Derivative with respect to the normalised toroidal flux
  real(ids_real),pointer  :: d_drho_tor_norm_error_upper(:) => null()
  real(ids_real),pointer  :: d_drho_tor_norm_error_lower(:) => null()
  integer(ids_int) :: d_drho_tor_norm_error_index=ids_int_invalid

  real(ids_real),pointer  :: d2_drho_tor_norm2(:) => null()     ! /d2_drho_tor_norm2 - Second derivative with respect to the normalised toroidal flux
  real(ids_real),pointer  :: d2_drho_tor_norm2_error_upper(:) => null()
  real(ids_real),pointer  :: d2_drho_tor_norm2_error_lower(:) => null()
  integer(ids_int) :: d2_drho_tor_norm2_error_index=ids_int_invalid

  real(ids_real),pointer  :: d_drho_tor(:) => null()     ! /d_drho_tor - Derivative with respect to the toroidal flux
  real(ids_real),pointer  :: d_drho_tor_error_upper(:) => null()
  real(ids_real),pointer  :: d_drho_tor_error_lower(:) => null()
  integer(ids_int) :: d_drho_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: d2_drho_tor2(:) => null()     ! /d2_drho_tor2 - Second derivative with respect to the toroidal flux
  real(ids_real),pointer  :: d2_drho_tor2_error_upper(:) => null()
  real(ids_real),pointer  :: d2_drho_tor2_error_lower(:) => null()
  integer(ids_int) :: d2_drho_tor2_error_index=ids_int_invalid

endtype

type ids_numerics_profiles_1d_derivatives_depth3  !    Derivatives of a transport equation primary quantity, depth 3 with respect to the 1D grid
  real(ids_real),pointer  :: d_dt(:) => null()     ! /d_dt - Time derivative
  real(ids_real),pointer  :: d_dt_error_upper(:) => null()
  real(ids_real),pointer  :: d_dt_error_lower(:) => null()
  integer(ids_int) :: d_dt_error_index=ids_int_invalid

  real(ids_real),pointer  :: d_drho_tor_norm(:) => null()     ! /d_drho_tor_norm - Derivative with respect to the normalised toroidal flux
  real(ids_real),pointer  :: d_drho_tor_norm_error_upper(:) => null()
  real(ids_real),pointer  :: d_drho_tor_norm_error_lower(:) => null()
  integer(ids_int) :: d_drho_tor_norm_error_index=ids_int_invalid

  real(ids_real),pointer  :: d2_drho_tor_norm2(:) => null()     ! /d2_drho_tor_norm2 - Second derivative with respect to the normalised toroidal flux
  real(ids_real),pointer  :: d2_drho_tor_norm2_error_upper(:) => null()
  real(ids_real),pointer  :: d2_drho_tor_norm2_error_lower(:) => null()
  integer(ids_int) :: d2_drho_tor_norm2_error_index=ids_int_invalid

  real(ids_real),pointer  :: d_drho_tor(:) => null()     ! /d_drho_tor - Derivative with respect to the toroidal flux
  real(ids_real),pointer  :: d_drho_tor_error_upper(:) => null()
  real(ids_real),pointer  :: d_drho_tor_error_lower(:) => null()
  integer(ids_int) :: d_drho_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: d2_drho_tor2(:) => null()     ! /d2_drho_tor2 - Second derivative with respect to the toroidal flux
  real(ids_real),pointer  :: d2_drho_tor2_error_upper(:) => null()
  real(ids_real),pointer  :: d2_drho_tor2_error_lower(:) => null()
  integer(ids_int) :: d2_drho_tor2_error_index=ids_int_invalid

endtype

type ids_numerics_profiles_1d_derivatives_electrons_d  !    Quantities related to electrons, derivatives with respect to a given quantity
  real(ids_real),pointer  :: temperature(:) => null()     ! /temperature - Temperature
  real(ids_real),pointer  :: temperature_error_upper(:) => null()
  real(ids_real),pointer  :: temperature_error_lower(:) => null()
  integer(ids_int) :: temperature_error_index=ids_int_invalid

  real(ids_real),pointer  :: density(:) => null()     ! /density - Density (thermal+non-thermal)
  real(ids_real),pointer  :: density_error_upper(:) => null()
  real(ids_real),pointer  :: density_error_lower(:) => null()
  integer(ids_int) :: density_error_index=ids_int_invalid

  real(ids_real),pointer  :: density_fast(:) => null()     ! /density_fast - Density of fast (non-thermal) particles
  real(ids_real),pointer  :: density_fast_error_upper(:) => null()
  real(ids_real),pointer  :: density_fast_error_lower(:) => null()
  integer(ids_int) :: density_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure(:) => null()     ! /pressure - Pressure
  real(ids_real),pointer  :: pressure_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_error_lower(:) => null()
  integer(ids_int) :: pressure_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_fast_perpendicular(:) => null()     ! /pressure_fast_perpendicular - Fast (non-thermal) perpendicular pressure
  real(ids_real),pointer  :: pressure_fast_perpendicular_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_fast_perpendicular_error_lower(:) => null()
  integer(ids_int) :: pressure_fast_perpendicular_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_fast_parallel(:) => null()     ! /pressure_fast_parallel - Fast (non-thermal) parallel pressure
  real(ids_real),pointer  :: pressure_fast_parallel_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_fast_parallel_error_lower(:) => null()
  integer(ids_int) :: pressure_fast_parallel_error_index=ids_int_invalid

  real(ids_real),pointer  :: velocity_tor(:) => null()     ! /velocity_tor - Toroidal velocity
  real(ids_real),pointer  :: velocity_tor_error_upper(:) => null()
  real(ids_real),pointer  :: velocity_tor_error_lower(:) => null()
  integer(ids_int) :: velocity_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: velocity_pol(:) => null()     ! /velocity_pol - Poloidal velocity
  real(ids_real),pointer  :: velocity_pol_error_upper(:) => null()
  real(ids_real),pointer  :: velocity_pol_error_lower(:) => null()
  integer(ids_int) :: velocity_pol_error_index=ids_int_invalid

endtype

type ids_numerics_profiles_1d_derivatives_electrons  !    Quantities related to electrons
  type (ids_numerics_profiles_1d_derivatives_electrons_d) :: d_drho_tor_norm  ! /d_drho_tor_norm - Derivatives with respect to the normalised toroidal flux
  type (ids_numerics_profiles_1d_derivatives_electrons_d) :: d2_drho_tor_norm2  ! /d2_drho_tor_norm2 - Second derivatives with respect to the normalised toroidal flux
  type (ids_numerics_profiles_1d_derivatives_electrons_d) :: d_dt  ! /d_dt - Derivatives with respect to time
endtype

type ids_numerics_profiles_1d_derivatives_total_ions  !    Quantities related to total ion quantities, derivatives with respect to a given quantity
  real(ids_real),pointer  :: n_i_total_over_n_e(:) => null()     ! /n_i_total_over_n_e - Ratio of total ion density (sum over species and charge states) over electron density. (thermal+non-
  real(ids_real),pointer  :: n_i_total_over_n_e_error_upper(:) => null()
  real(ids_real),pointer  :: n_i_total_over_n_e_error_lower(:) => null()
  integer(ids_int) :: n_i_total_over_n_e_error_index=ids_int_invalid

  real(ids_real),pointer  :: pressure_ion_total(:) => null()     ! /pressure_ion_total - Total thermal ion pressure
  real(ids_real),pointer  :: pressure_ion_total_error_upper(:) => null()
  real(ids_real),pointer  :: pressure_ion_total_error_lower(:) => null()
  integer(ids_int) :: pressure_ion_total_error_index=ids_int_invalid

endtype

type ids_numerics_profiles_1d_derivatives  !    Radial profiles derivatives for a given time slice
  type (ids_core_radial_grid) :: grid  ! /grid - Radial grid
  type (ids_numerics_profiles_1d_derivatives_electrons) :: electrons  ! /electrons - Quantities related to the electrons
  type (ids_numerics_profiles_1d_derivatives_ion),pointer :: ion(:) => null()  ! /ion(i) - Quantities related to the different ion species
  type (ids_numerics_profiles_1d_derivatives_total_ions) :: d_drho_tor_norm  ! /d_drho_tor_norm - Derivatives of total ion quantities with respect to the normalised toroidal flux
  type (ids_numerics_profiles_1d_derivatives_total_ions) :: d2_drho_tor_norm2  ! /d2_drho_tor_norm2 - Second derivatives of total ion quantities with respect to the normalised toroidal flux
  type (ids_numerics_profiles_1d_derivatives_total_ions) :: d_dt  ! /d_dt - Derivatives of total ion quantities with respect to time
  real(ids_real),pointer  :: dpsi_dt(:) => null()     ! /dpsi_dt - Derivative of the poloidal flux profile with respect to time
  real(ids_real),pointer  :: dpsi_dt_error_upper(:) => null()
  real(ids_real),pointer  :: dpsi_dt_error_lower(:) => null()
  integer(ids_int) :: dpsi_dt_error_index=ids_int_invalid

  real(ids_real),pointer  :: dpsi_dt_cphi(:) => null()     ! /dpsi_dt_cphi - Derivative of the poloidal flux profile with respect to time, at constant toroidal flux
  real(ids_real),pointer  :: dpsi_dt_cphi_error_upper(:) => null()
  real(ids_real),pointer  :: dpsi_dt_cphi_error_lower(:) => null()
  integer(ids_int) :: dpsi_dt_cphi_error_index=ids_int_invalid

  real(ids_real),pointer  :: dpsi_dt_crho_tor_norm(:) => null()     ! /dpsi_dt_crho_tor_norm - Derivative of the poloidal flux profile with respect to time, at constant normalised toroidal flux c
  real(ids_real),pointer  :: dpsi_dt_crho_tor_norm_error_upper(:) => null()
  real(ids_real),pointer  :: dpsi_dt_crho_tor_norm_error_lower(:) => null()
  integer(ids_int) :: dpsi_dt_crho_tor_norm_error_index=ids_int_invalid

  real(ids_real),pointer  :: drho_tor_dt(:) => null()     ! /drho_tor_dt - Partial derivative of the toroidal flux coordinate profile with respect to time
  real(ids_real),pointer  :: drho_tor_dt_error_upper(:) => null()
  real(ids_real),pointer  :: drho_tor_dt_error_lower(:) => null()
  integer(ids_int) :: drho_tor_dt_error_index=ids_int_invalid

  real(ids_real),pointer  :: d_dvolume_drho_tor_dt(:) => null()     ! /d_dvolume_drho_tor_dt - Partial derivative with respect to time of the derivative of the volume with respect to the toroidal
  real(ids_real),pointer  :: d_dvolume_drho_tor_dt_error_upper(:) => null()
  real(ids_real),pointer  :: d_dvolume_drho_tor_dt_error_lower(:) => null()
  integer(ids_int) :: d_dvolume_drho_tor_dt_error_index=ids_int_invalid

  real(ids_real),pointer  :: dpsi_drho_tor(:) => null()     ! /dpsi_drho_tor - Derivative of the poloidal flux profile with respect to the toroidal flux coordinate
  real(ids_real),pointer  :: dpsi_drho_tor_error_upper(:) => null()
  real(ids_real),pointer  :: dpsi_drho_tor_error_lower(:) => null()
  integer(ids_int) :: dpsi_drho_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: d2psi_drho_tor2(:) => null()     ! /d2psi_drho_tor2 - Second derivative of the poloidal flux profile with respect to the toroidal flux coordinate
  real(ids_real),pointer  :: d2psi_drho_tor2_error_upper(:) => null()
  real(ids_real),pointer  :: d2psi_drho_tor2_error_lower(:) => null()
  integer(ids_int) :: d2psi_drho_tor2_error_index=ids_int_invalid

  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_numerics_restart  !    Description of a restart file
  character(len=ids_string_length), dimension(:), pointer ::names => null()       ! /names - Names of the restart files
  character(len=ids_string_length), dimension(:), pointer ::descriptions => null()       ! /descriptions - Descriptions of the restart files
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_numerics_bc_1d_current_new  !    Boundary conditions for the current diffusion equation
  type (ids_identifier_dynamic_aos3) :: identifier  ! /identifier - Identifier of the boundary condition type. ID = 1: poloidal flux; 2: ip; 3: loop voltage; 4: undefin
  real(ids_real),pointer  :: value(:) => null()     ! /value - Value of the boundary condition. For ID = 1 to 3, only the first position in the vector is used. For
  real(ids_real),pointer  :: value_error_upper(:) => null()
  real(ids_real),pointer  :: value_error_lower(:) => null()
  integer(ids_int) :: value_error_index=ids_int_invalid

  real(ids_real)  :: rho_tor_norm=ids_real_invalid       ! /rho_tor_norm - Position, in normalised toroidal flux, at which the boundary condition is imposed. Outside this posi
  real(ids_real)  :: rho_tor_norm_error_upper=ids_real_invalid
  real(ids_real)  :: rho_tor_norm_error_lower=ids_real_invalid
  integer(ids_int) :: rho_tor_norm_error_index=ids_int_invalid

  real(ids_real)  :: rho_tor=ids_real_invalid       ! /rho_tor - Position, in toroidal flux, at which the boundary condition is imposed. Outside this position, the v
  real(ids_real)  :: rho_tor_error_upper=ids_real_invalid
  real(ids_real)  :: rho_tor_error_lower=ids_real_invalid
  integer(ids_int) :: rho_tor_error_index=ids_int_invalid

endtype

type ids_numerics_bc_1d_current  !    Boundary conditions for the current diffusion equation
  type (ids_identifier_dynamic_aos3) :: identifier  ! /identifier - Identifier of the boundary condition type. ID = 1: poloidal flux; 2: ip; 3: loop voltage; 4: undefin
  real(ids_real),pointer  :: value(:) => null()     ! /value - Value of the boundary condition. For ID = 1 to 3, only the first position in the vector is used. For
  real(ids_real),pointer  :: value_error_upper(:) => null()
  real(ids_real),pointer  :: value_error_lower(:) => null()
  integer(ids_int) :: value_error_index=ids_int_invalid

  real(ids_real)  :: rho_tor_norm=ids_real_invalid       ! /rho_tor_norm - Position, in normalised toroidal flux, at which the boundary condition is imposed. Outside this posi
  real(ids_real)  :: rho_tor_norm_error_upper=ids_real_invalid
  real(ids_real)  :: rho_tor_norm_error_lower=ids_real_invalid
  integer(ids_int) :: rho_tor_norm_error_index=ids_int_invalid

endtype

type ids_numerics_bc_ggd_current  !    Boundary conditions for the current diffusion equation
  type (ids_identifier_dynamic_aos3) :: identifier  ! /identifier - Identifier of the boundary condition type. List of options TBD.
  integer(ids_int)  :: grid_index=ids_int_invalid       ! /grid_index - Index of the grid used to represent this quantity
  integer(ids_int)  :: grid_subset_index=ids_int_invalid       ! /grid_subset_index - Index of the grid subset the data is provided on
  real(ids_real),pointer  :: values(:,:) => null()     ! /values - List of vector components, one list per element in the grid subset. First dimenstion: element index.
  real(ids_real),pointer  :: values_error_upper(:,:) => null()
  real(ids_real),pointer  :: values_error_lower(:,:) => null()
  integer(ids_int) :: values_error_index=ids_int_invalid

endtype

type ids_numerics_bc_ggd_bc  !    Boundary conditions for a given transport equation
  type (ids_identifier_dynamic_aos3) :: identifier  ! /identifier - Identifier of the boundary condition type. List of options TBD.
  integer(ids_int)  :: grid_index=ids_int_invalid       ! /grid_index - Index of the grid used to represent this quantity
  integer(ids_int)  :: grid_subset_index=ids_int_invalid       ! /grid_subset_index - Index of the grid subset the data is provided on
  real(ids_real),pointer  :: values(:,:) => null()     ! /values - List of vector components, one list per element in the grid subset. First dimenstion: element index.
  real(ids_real),pointer  :: values_error_upper(:,:) => null()
  real(ids_real),pointer  :: values_error_lower(:,:) => null()
  integer(ids_int) :: values_error_index=ids_int_invalid

endtype

type ids_numerics_bc_1d_bc  !    Boundary conditions for a given transport equation
  type (ids_identifier_dynamic_aos3) :: identifier  ! /identifier - Identifier of the boundary condition type. ID = 1: value of the field y; 2: radial derivative of the
  real(ids_real),pointer  :: value(:) => null()     ! /value - Value of the boundary condition. For ID = 1 to 4, only the first position in the vector is used. For
  real(ids_real),pointer  :: value_error_upper(:) => null()
  real(ids_real),pointer  :: value_error_lower(:) => null()
  integer(ids_int) :: value_error_index=ids_int_invalid

  real(ids_real)  :: rho_tor_norm=ids_real_invalid       ! /rho_tor_norm - Position, in normalised toroidal flux, at which the boundary condition is imposed. Outside this posi
  real(ids_real)  :: rho_tor_norm_error_upper=ids_real_invalid
  real(ids_real)  :: rho_tor_norm_error_lower=ids_real_invalid
  integer(ids_int) :: rho_tor_norm_error_index=ids_int_invalid

endtype

type ids_numerics_bc_1d_electrons  !    Boundary conditions for the electron related transport equations
  type (ids_numerics_bc_1d_bc) :: particles  ! /particles - Boundary condition for the electron density equation (density if ID = 1)
  type (ids_numerics_bc_1d_bc) :: energy  ! /energy - Boundary condition for the electron energy equation (temperature if ID = 1)
endtype

type ids_numerics_bc_ggd_electrons  !    Boundary conditions for the electron related transport equations
  type (ids_numerics_bc_ggd_bc),pointer :: particles(:) => null()  ! /particles(i) - Boundary condition for the electron density equation (density if ID = 1), on various grid subsets
  type (ids_numerics_bc_ggd_bc),pointer :: energy(:) => null()  ! /energy(i) - Boundary condition for the electron energy equation (temperature if ID = 1), on various grid subsets
endtype

type ids_numerics_bc_1d_ion_charge_state  !    Boundary conditions for a given charge state related transport equations
  real(ids_real)  :: z_min=ids_real_invalid       ! /z_min - Minimum Z of the charge state bundle
  real(ids_real)  :: z_min_error_upper=ids_real_invalid
  real(ids_real)  :: z_min_error_lower=ids_real_invalid
  integer(ids_int) :: z_min_error_index=ids_int_invalid

  real(ids_real)  :: z_max=ids_real_invalid       ! /z_max - Maximum Z of the charge state bundle
  real(ids_real)  :: z_max_error_upper=ids_real_invalid
  real(ids_real)  :: z_max_error_lower=ids_real_invalid
  integer(ids_int) :: z_max_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  integer(ids_int)  :: is_neutral=ids_int_invalid       ! /is_neutral - Flag specifying if this state corresponds to a neutral (1) or not (0)
  type (ids_identifier_dynamic_aos3) :: neutral_type  ! /neutral_type - Neutral type (if the considered state is a neutral), in terms of energy. ID =1: cold; 2: thermal; 3:
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  type (ids_numerics_bc_1d_bc) :: particles  ! /particles - Boundary condition for the charge state density equation (density if ID = 1)
  type (ids_numerics_bc_1d_bc) :: energy  ! /energy - Boundary condition for the charge state energy equation (temperature if ID = 1)
endtype

type ids_numerics_bc_ggd_ion_charge_state  !    Boundary conditions for a given charge state related transport equations
  real(ids_real)  :: z_min=ids_real_invalid       ! /z_min - Minimum Z of the charge state bundle
  real(ids_real)  :: z_min_error_upper=ids_real_invalid
  real(ids_real)  :: z_min_error_lower=ids_real_invalid
  integer(ids_int) :: z_min_error_index=ids_int_invalid

  real(ids_real)  :: z_max=ids_real_invalid       ! /z_max - Maximum Z of the charge state bundle
  real(ids_real)  :: z_max_error_upper=ids_real_invalid
  real(ids_real)  :: z_max_error_lower=ids_real_invalid
  integer(ids_int) :: z_max_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  integer(ids_int)  :: is_neutral=ids_int_invalid       ! /is_neutral - Flag specifying if this state corresponds to a neutral (1) or not (0)
  type (ids_identifier_dynamic_aos3) :: neutral_type  ! /neutral_type - Neutral type (if the considered state is a neutral), in terms of energy. ID =1: cold; 2: thermal; 3:
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  type (ids_numerics_bc_ggd_bc),pointer :: particles(:) => null()  ! /particles(i) - Boundary condition for the charge state density equation (density if ID = 1), on various grid subset
  type (ids_numerics_bc_ggd_bc),pointer :: energy(:) => null()  ! /energy(i) - Boundary condition for the charge state energy equation (temperature if ID = 1), on various grid sub
endtype

type ids_numerics_bc_1d_ion  !    Boundary conditions for a given ion species related transport equations
  real(ids_real)  :: a=ids_real_invalid       ! /a - Mass of atom
  real(ids_real)  :: a_error_upper=ids_real_invalid
  real(ids_real)  :: a_error_lower=ids_real_invalid
  integer(ids_int) :: a_error_index=ids_int_invalid

  real(ids_real)  :: z_ion=ids_real_invalid       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed)
  real(ids_real)  :: z_ion_error_upper=ids_real_invalid
  real(ids_real)  :: z_ion_error_lower=ids_real_invalid
  integer(ids_int) :: z_ion_error_index=ids_int_invalid

  real(ids_real)  :: z_n=ids_real_invalid       ! /z_n - Nuclear charge
  real(ids_real)  :: z_n_error_upper=ids_real_invalid
  real(ids_real)  :: z_n_error_lower=ids_real_invalid
  integer(ids_int) :: z_n_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  type (ids_numerics_bc_1d_bc) :: particles  ! /particles - Boundary condition for the ion density equation (density if ID = 1)
  type (ids_numerics_bc_1d_bc) :: energy  ! /energy - Boundary condition for the ion energy equation (temperature if ID = 1)
  integer(ids_int)  :: multiple_states_flag=ids_int_invalid       ! /multiple_states_flag - Multiple states calculation flag : 0-Only one state is considered; 1-Multiple states are considered
  type (ids_numerics_bc_1d_ion_charge_state),pointer :: state(:) => null()  ! /state(i) - Quantities related to the different states of the species (ionisation, energy, excitation, ...)
endtype

type ids_numerics_bc_ggd_ion  !    Boundary conditions for a given ion species related transport equations
  real(ids_real)  :: a=ids_real_invalid       ! /a - Mass of atom
  real(ids_real)  :: a_error_upper=ids_real_invalid
  real(ids_real)  :: a_error_lower=ids_real_invalid
  integer(ids_int) :: a_error_index=ids_int_invalid

  real(ids_real)  :: z_ion=ids_real_invalid       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed)
  real(ids_real)  :: z_ion_error_upper=ids_real_invalid
  real(ids_real)  :: z_ion_error_lower=ids_real_invalid
  integer(ids_int) :: z_ion_error_index=ids_int_invalid

  real(ids_real)  :: z_n=ids_real_invalid       ! /z_n - Nuclear charge
  real(ids_real)  :: z_n_error_upper=ids_real_invalid
  real(ids_real)  :: z_n_error_lower=ids_real_invalid
  integer(ids_int) :: z_n_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  type (ids_numerics_bc_ggd_bc),pointer :: particles(:) => null()  ! /particles(i) - Boundary condition for the ion density equation (density if ID = 1), on various grid subsets
  type (ids_numerics_bc_ggd_bc),pointer :: energy(:) => null()  ! /energy(i) - Boundary condition for the ion energy equation (temperature if ID = 1), on various grid subsets
  integer(ids_int)  :: multiple_states_flag=ids_int_invalid       ! /multiple_states_flag - Multiple states calculation flag : 0-Only one state is considered; 1-Multiple states are considered
  type (ids_numerics_bc_ggd_ion_charge_state),pointer :: state(:) => null()  ! /state(i) - Quantities related to the different states of the species (ionisation, energy, excitation, ...)
endtype

type ids_numerics_bc_1d  !    Boundary conditions of radial transport equations for a given time slice
  type (ids_numerics_bc_1d_current) :: current  ! /current - Boundary condition for the current diffusion equation.
  type (ids_numerics_bc_1d_electrons) :: electrons  ! /electrons - Quantities related to the electrons
  type (ids_numerics_bc_1d_ion),pointer :: ion(:) => null()  ! /ion(i) - Quantities related to the different ion species
  type (ids_numerics_bc_1d_bc) :: energy_ion_total  ! /energy_ion_total - Boundary condition for the ion total (sum over ion species) energy equation (temperature if ID = 1)
  type (ids_numerics_bc_1d_bc) :: momentum_tor  ! /momentum_tor - Boundary condition for the total plasma toroidal momentum equation (summed over ion species and elec
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_numerics_bc_ggd  !    Boundary conditions of radial transport equations for a given time slice
  type (ids_generic_grid_dynamic) :: grid  ! /grid - Grid description
  type (ids_numerics_bc_ggd_current),pointer :: current(:) => null()  ! /current(i) - Boundary condition for the current diffusion equation, on various grid subsets
  type (ids_numerics_bc_ggd_electrons) :: electrons  ! /electrons - Quantities related to the electrons
  type (ids_numerics_bc_ggd_ion),pointer :: ion(:) => null()  ! /ion(i) - Quantities related to the different ion species
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_numerics_convergence_equations_single  !    Convergence details of a given transport equation
  integer(ids_int)  :: iterations_n=ids_int_invalid       ! /iterations_n - Number of iterations carried out in the convergence loop
endtype

type ids_numerics_convergence_equations_ion_charge_state  !    Boundary conditions for a given charge state related transport equations
  real(ids_real)  :: z_min=ids_real_invalid       ! /z_min - Minimum Z of the charge state bundle
  real(ids_real)  :: z_min_error_upper=ids_real_invalid
  real(ids_real)  :: z_min_error_lower=ids_real_invalid
  integer(ids_int) :: z_min_error_index=ids_int_invalid

  real(ids_real)  :: z_max=ids_real_invalid       ! /z_max - Maximum Z of the charge state bundle
  real(ids_real)  :: z_max_error_upper=ids_real_invalid
  real(ids_real)  :: z_max_error_lower=ids_real_invalid
  integer(ids_int) :: z_max_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  integer(ids_int)  :: is_neutral=ids_int_invalid       ! /is_neutral - Flag specifying if this state corresponds to a neutral (1) or not (0)
  type (ids_identifier_dynamic_aos3) :: neutral_type  ! /neutral_type - Neutral type (if the considered state is a neutral), in terms of energy. ID =1: cold; 2: thermal; 3:
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  type (ids_numerics_convergence_equations_single) :: particles  ! /particles - Convergence details of the charge state density equation
  type (ids_numerics_convergence_equations_single) :: energy  ! /energy - Convergence details of the charge state energy equation
endtype

type ids_numerics_convergence_equations_ion  !    Convergence details of a given ion species related transport equations
  real(ids_real)  :: a=ids_real_invalid       ! /a - Mass of atom
  real(ids_real)  :: a_error_upper=ids_real_invalid
  real(ids_real)  :: a_error_lower=ids_real_invalid
  integer(ids_int) :: a_error_index=ids_int_invalid

  real(ids_real)  :: z_ion=ids_real_invalid       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed)
  real(ids_real)  :: z_ion_error_upper=ids_real_invalid
  real(ids_real)  :: z_ion_error_lower=ids_real_invalid
  integer(ids_int) :: z_ion_error_index=ids_int_invalid

  real(ids_real)  :: z_n=ids_real_invalid       ! /z_n - Nuclear charge
  real(ids_real)  :: z_n_error_upper=ids_real_invalid
  real(ids_real)  :: z_n_error_lower=ids_real_invalid
  integer(ids_int) :: z_n_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  type (ids_numerics_convergence_equations_single) :: particles  ! /particles - Convergence details of the  ion density equation
  type (ids_numerics_convergence_equations_single) :: energy  ! /energy - Convergence details of the ion energy equation
  integer(ids_int)  :: multiple_states_flag=ids_int_invalid       ! /multiple_states_flag - Multiple state calculation flag : 0-Only one state is considered; 1-Multiple states are considered a
  type (ids_numerics_convergence_equations_ion_charge_state),pointer :: state(:) => null()  ! /state(i) - Convergence details of the related to the different states transport equations
endtype

type ids_numerics_convergence_equations_electrons  !    Convergence details for the electron related equations
  type (ids_numerics_convergence_equations_single) :: particles  ! /particles - Convergence details of the electron density equation
  type (ids_numerics_convergence_equations_single) :: energy  ! /energy - Convergence details of the electron energy equation
endtype

type ids_numerics_convergence_equation  !    Convergence details of a given transport equation
  type (ids_numerics_convergence_equations_single) :: current  ! /current - Convergence details of the current diffusion equation
  type (ids_numerics_convergence_equations_electrons) :: electrons  ! /electrons - Quantities related to the electrons
  type (ids_numerics_convergence_equations_ion),pointer :: ion(:) => null()  ! /ion(i) - Quantities related to the different ion species
  type (ids_numerics_convergence_equations_single) :: energy_ion_total  ! /energy_ion_total - Convergence details of the ion total (sum over ion species) energy equation
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

! SPECIAL STRUCTURE data / time
type ids_numerics_convergence_time_step  !    Internal time step used by the transport solver (assuming all transport equations are solved with the same time step)
  real(ids_real), pointer  :: data(:) => null()     ! /time_step - Internal time step used by the transport solver (assuming all transport equations are solved with th
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_numerics_convergence  !    Convergence details
  type (ids_numerics_convergence_time_step) :: time_step  ! /time_step - Internal time step used by the transport solver (assuming all transport equations are solved with th
  type (ids_numerics_convergence_equation),pointer :: equations(:) => null()  ! /equations(i) - Convergence details of the transport equations, for various time slices
endtype

type ids_numerics_solver_1d_equation_control_float  !    FLT0D for control parameters
  real(ids_real)  :: value=ids_real_invalid       ! /value - Value of the control parameter
  real(ids_real)  :: value_error_upper=ids_real_invalid
  real(ids_real)  :: value_error_lower=ids_real_invalid
  integer(ids_int) :: value_error_index=ids_int_invalid

endtype

type ids_numerics_solver_1d_equation_control_int  !    INT0D for control parameters
  integer(ids_int)  :: value=ids_int_invalid       ! /value - Value of the control parameter
endtype

type ids_numerics_solver_1d_equation_control_parameters  !    Solver-specific input or output quantities
  type (ids_numerics_solver_1d_equation_control_int),pointer :: integer0d(:) => null()  ! /integer0d(i) - Set of integer type scalar control parameters
  type (ids_numerics_solver_1d_equation_control_float),pointer :: real0d(:) => null()  ! /real0d(i) - Set of real type scalar control parameters
endtype

type ids_numerics_solver_1d_equation_coefficient  !    Coefficient for transport equation
  real(ids_real),pointer  :: profile(:) => null()     ! /profile - Radial profile of the numerical coefficient
  real(ids_real),pointer  :: profile_error_upper(:) => null()
  real(ids_real),pointer  :: profile_error_lower(:) => null()
  integer(ids_int) :: profile_error_index=ids_int_invalid

endtype

type ids_numerics_solver_1d_equation_primary  !    Profile and derivatives a the primary quantity for a 1D transport equation
  type (ids_identifier_dynamic_aos3) :: identifier  ! /identifier - Identifier of the primary quantity of the transport equation. The description node contains the path
  real(ids_real),pointer  :: profile(:) => null()     ! /profile - Profile of the primary quantity
  real(ids_real),pointer  :: profile_error_upper(:) => null()
  real(ids_real),pointer  :: profile_error_lower(:) => null()
  integer(ids_int) :: profile_error_index=ids_int_invalid

  real(ids_real),pointer  :: d_dr(:) => null()     ! /d_dr - Radial derivative with respect to the primary coordinate
  real(ids_real),pointer  :: d_dr_error_upper(:) => null()
  real(ids_real),pointer  :: d_dr_error_lower(:) => null()
  integer(ids_int) :: d_dr_error_index=ids_int_invalid

  real(ids_real),pointer  :: d2_dr2(:) => null()     ! /d2_dr2 - Second order radial derivative with respect to the primary coordinate
  real(ids_real),pointer  :: d2_dr2_error_upper(:) => null()
  real(ids_real),pointer  :: d2_dr2_error_lower(:) => null()
  integer(ids_int) :: d2_dr2_error_index=ids_int_invalid

  real(ids_real),pointer  :: d_dt(:) => null()     ! /d_dt - Time derivative
  real(ids_real),pointer  :: d_dt_error_upper(:) => null()
  real(ids_real),pointer  :: d_dt_error_lower(:) => null()
  integer(ids_int) :: d_dt_error_index=ids_int_invalid

  real(ids_real),pointer  :: d_dt_cphi(:) => null()     ! /d_dt_cphi - Derivative with respect to time, at constant toroidal flux (for current diffusion equation)
  real(ids_real),pointer  :: d_dt_cphi_error_upper(:) => null()
  real(ids_real),pointer  :: d_dt_cphi_error_lower(:) => null()
  integer(ids_int) :: d_dt_cphi_error_index=ids_int_invalid

  real(ids_real),pointer  :: d_dt_cr(:) => null()     ! /d_dt_cr - Derivative with respect to time, at constant primary coordinate coordinate (for current diffusion eq
  real(ids_real),pointer  :: d_dt_cr_error_upper(:) => null()
  real(ids_real),pointer  :: d_dt_cr_error_lower(:) => null()
  integer(ids_int) :: d_dt_cr_error_index=ids_int_invalid

endtype

type ids_numerics_solver_1d_equation_bc  !    Boundary conditions for a 1D transport equation
  type (ids_identifier_dynamic_aos3) :: identifier  ! /identifier - Identifier of the boundary condition
  type (ids_identifier_dynamic_aos3) :: type  ! /type - Boundary condition type
  real(ids_real),pointer  :: value(:) => null()     ! /value - Value of the boundary condition. For type/index = 1 to 3, only the first position in the vector is u
  real(ids_real),pointer  :: value_error_upper(:) => null()
  real(ids_real),pointer  :: value_error_lower(:) => null()
  integer(ids_int) :: value_error_index=ids_int_invalid

  real(ids_real)  :: position=ids_real_invalid       ! /position - Position, in terms of the primary coordinate, at which the boundary condition is imposed. Outside th
  real(ids_real)  :: position_error_upper=ids_real_invalid
  real(ids_real)  :: position_error_lower=ids_real_invalid
  integer(ids_int) :: position_error_index=ids_int_invalid

endtype

type ids_numerics_solver_1d_equation  !    Numeric of a given 1D transport equation
  type (ids_numerics_solver_1d_equation_primary) :: primary_quantity  ! /primary_quantity - Profile and derivatives of the primary quantity of the transport equation
  type (ids_numerics_solver_1d_equation_bc),pointer :: boundary_condition(:) => null()  ! /boundary_condition(i) - Set of boundary conditions of the transport equation
  type (ids_numerics_solver_1d_equation_coefficient),pointer :: coefficient(:) => null()  ! /coefficient(i) - Set of numerical coefficients involved in the transport equation
  type (ids_numerics_convergence_equations_single) :: convergence  ! /convergence - Convergence details
endtype

type ids_numerics_solver_1d  !    Numerics related to 1D radial solver for a given time slice
  type (ids_core_radial_grid) :: grid  ! /grid - Radial grid
  type (ids_numerics_solver_1d_equation),pointer :: equation(:) => null()  ! /equation(i) - Set of transport equations
  type (ids_numerics_solver_1d_equation_control_parameters) :: control_parameters  ! /control_parameters - Solver-specific input or output quantities
  real(ids_real),pointer  :: drho_tor_dt(:) => null()     ! /drho_tor_dt - Partial derivative of the toroidal flux coordinate profile with respect to time
  real(ids_real),pointer  :: drho_tor_dt_error_upper(:) => null()
  real(ids_real),pointer  :: drho_tor_dt_error_lower(:) => null()
  integer(ids_int) :: drho_tor_dt_error_index=ids_int_invalid

  real(ids_real),pointer  :: d_dvolume_drho_tor_dt(:) => null()     ! /d_dvolume_drho_tor_dt - Partial derivative with respect to time of the derivative of the volume with respect to the toroidal
  real(ids_real),pointer  :: d_dvolume_drho_tor_dt_error_upper(:) => null()
  real(ids_real),pointer  :: d_dvolume_drho_tor_dt_error_lower(:) => null()
  integer(ids_int) :: d_dvolume_drho_tor_dt_error_index=ids_int_invalid

  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

! SPECIAL STRUCTURE data / time
type ids_transport_solver_numerics_time_step  !    Internal time step used by the transport solver (assuming all transport equations are solved with the same time step)
  real(ids_real), pointer  :: data(:) => null()     ! /transport_solver_numerics/time_step - Internal time step used by the transport solver (assuming all transport equations are solved with th
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_transport_solver_numerics_time_step_average  !    Average internal time step used by the transport solver between the previous and the current time stored for this quantity (assumi
  real(ids_real), pointer  :: data(:) => null()     ! /transport_solver_numerics/time_step_average - Average internal time step used by the transport solver between the previous and the current time st
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_transport_solver_numerics_time_step_min  !    Minimum internal time step used by the transport solver between the previous and the current time stored for this quantity (assumi
  real(ids_real), pointer  :: data(:) => null()     ! /transport_solver_numerics/time_step_min - Minimum internal time step used by the transport solver between the previous and the current time st
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_transport_solver_numerics  !    Numerical quantities used by transport solvers and convergence details
  type (ids_ids_properties) :: ids_properties  ! /transport_solver_numerics/ids_properties -
  type (ids_transport_solver_numerics_time_step) :: time_step  ! /transport_solver_numerics/time_step - Internal time step used by the transport solver (assuming all transport equations are solved with th
  type (ids_transport_solver_numerics_time_step_average) :: time_step_average  ! /transport_solver_numerics/time_step_average - Average internal time step used by the transport solver between the previous and the current time st
  type (ids_transport_solver_numerics_time_step_min) :: time_step_min  ! /transport_solver_numerics/time_step_min - Minimum internal time step used by the transport solver between the previous and the current time st
  type (ids_identifier) :: solver  ! /transport_solver_numerics/solver - Solver identifier
  type (ids_identifier) :: primary_coordinate  ! /transport_solver_numerics/primary_coordinate - Primary coordinate system with which the transport equations are solved. For a 1D transport solver:
  type (ids_numerics_solver_1d),pointer :: solver_1d(:) => null()  ! /transport_solver_numerics/solver_1d(i) - Numerics related to 1D radial solver, for various time slices.
  type (ids_numerics_profiles_1d_derivatives),pointer :: derivatives_1d(:) => null()  ! /transport_solver_numerics/derivatives_1d(i) - Radial profiles derivatives for various time slices. To be removed when the solver_1d structure is f
  type (ids_numerics_bc_1d),pointer :: boundary_conditions_1d(:) => null()  ! /transport_solver_numerics/boundary_conditions_1d(i) - Boundary conditions of the radial transport equations for various time slices. To be removed when th
  type (ids_numerics_bc_ggd),pointer :: boundary_conditions_ggd(:) => null()  ! /transport_solver_numerics/boundary_conditions_ggd(i) - Boundary conditions of the transport equations, provided on the GGD, for various time slices
  type (ids_numerics_convergence) :: convergence  ! /transport_solver_numerics/convergence - Convergence details To be removed when the solver_1d structure is finalized.
  type (ids_b_tor_vacuum_1) :: vacuum_toroidal_field  ! /transport_solver_numerics/vacuum_toroidal_field - Characteristics of the vacuum toroidal field (used in rho_tor definition and in the normalization of
  type (ids_numerics_restart),pointer :: restart_files(:) => null()  ! /transport_solver_numerics/restart_files(i) - Set of code-specific restart files for a given time slice. These files are managed by a physical app
  type (ids_code) :: code  ! /transport_solver_numerics/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include turbulence/dd_turbulence.xsd
type ids_turbulence_profiles_2d_neutral  !    Quantities related to a given neutral species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  integer(ids_int)  :: ion_index=ids_int_invalid       ! /ion_index - Index of the corresponding ion species in the ../../ion array
  real(ids_real),pointer  :: temperature(:,:) => null()     ! /temperature - Temperature (average over charge states when multiple charge states are considered)
  real(ids_real),pointer  :: temperature_error_upper(:,:) => null()
  real(ids_real),pointer  :: temperature_error_lower(:,:) => null()
  integer(ids_int) :: temperature_error_index=ids_int_invalid

  real(ids_real),pointer  :: density(:,:) => null()     ! /density - Density (thermal+non-thermal) (sum over charge states when multiple charge states are considered)
  real(ids_real),pointer  :: density_error_upper(:,:) => null()
  real(ids_real),pointer  :: density_error_lower(:,:) => null()
  integer(ids_int) :: density_error_index=ids_int_invalid

  real(ids_real),pointer  :: density_thermal(:,:) => null()     ! /density_thermal - Density (thermal) (sum over charge states when multiple charge states are considered)
  real(ids_real),pointer  :: density_thermal_error_upper(:,:) => null()
  real(ids_real),pointer  :: density_thermal_error_lower(:,:) => null()
  integer(ids_int) :: density_thermal_error_index=ids_int_invalid

endtype

type ids_turbulence_profiles_2d_ions  !    Quantities related to a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(ids_real)  :: z_ion=ids_real_invalid       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed), volume averaged over plasma
  real(ids_real)  :: z_ion_error_upper=ids_real_invalid
  real(ids_real)  :: z_ion_error_lower=ids_real_invalid
  integer(ids_int) :: z_ion_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  integer(ids_int)  :: neutral_index=ids_int_invalid       ! /neutral_index - Index of the corresponding neutral species in the ../../neutral array
  real(ids_real),pointer  :: temperature(:,:) => null()     ! /temperature - Temperature (average over charge states when multiple charge states are considered)
  real(ids_real),pointer  :: temperature_error_upper(:,:) => null()
  real(ids_real),pointer  :: temperature_error_lower(:,:) => null()
  integer(ids_int) :: temperature_error_index=ids_int_invalid

  real(ids_real),pointer  :: density(:,:) => null()     ! /density - Density (thermal+non-thermal) (sum over charge states when multiple charge states are considered)
  real(ids_real),pointer  :: density_error_upper(:,:) => null()
  real(ids_real),pointer  :: density_error_lower(:,:) => null()
  integer(ids_int) :: density_error_index=ids_int_invalid

  real(ids_real),pointer  :: density_thermal(:,:) => null()     ! /density_thermal - Density (thermal) (sum over charge states when multiple charge states are considered)
  real(ids_real),pointer  :: density_thermal_error_upper(:,:) => null()
  real(ids_real),pointer  :: density_thermal_error_lower(:,:) => null()
  integer(ids_int) :: density_thermal_error_index=ids_int_invalid

endtype

type ids_turbulence_profiles_2d_electrons  !    Quantities related to electrons
  real(ids_real),pointer  :: temperature(:,:) => null()     ! /temperature - Temperature
  real(ids_real),pointer  :: temperature_error_upper(:,:) => null()
  real(ids_real),pointer  :: temperature_error_lower(:,:) => null()
  integer(ids_int) :: temperature_error_index=ids_int_invalid

  real(ids_real),pointer  :: density(:,:) => null()     ! /density - Density (thermal+non-thermal)
  real(ids_real),pointer  :: density_error_upper(:,:) => null()
  real(ids_real),pointer  :: density_error_lower(:,:) => null()
  integer(ids_int) :: density_error_index=ids_int_invalid

  real(ids_real),pointer  :: density_thermal(:,:) => null()     ! /density_thermal - Density of thermal particles
  real(ids_real),pointer  :: density_thermal_error_upper(:,:) => null()
  real(ids_real),pointer  :: density_thermal_error_lower(:,:) => null()
  integer(ids_int) :: density_thermal_error_index=ids_int_invalid

endtype

type ids_turbulence_profiles_2d  !    Fluctuating physical quantities for various time slices
  type (ids_turbulence_profiles_2d_electrons) :: electrons  ! /electrons - Quantities related to electrons
  type (ids_turbulence_profiles_2d_ions),pointer :: ion(:) => null()  ! /ion(i) - Quantities related to the various ion species
  type (ids_turbulence_profiles_2d_neutral),pointer :: neutral(:) => null()  ! /neutral(i) - Quantities related to the various neutral species
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_turbulence_profiles_2d_grid  !    Definition of the 2D grid with time
  real(ids_real),pointer  :: dim1(:) => null()     ! /dim1 - First dimension values
  real(ids_real),pointer  :: dim1_error_upper(:) => null()
  real(ids_real),pointer  :: dim1_error_lower(:) => null()
  integer(ids_int) :: dim1_error_index=ids_int_invalid

  real(ids_real),pointer  :: dim2(:) => null()     ! /dim2 - Second dimension values
  real(ids_real),pointer  :: dim2_error_upper(:) => null()
  real(ids_real),pointer  :: dim2_error_lower(:) => null()
  integer(ids_int) :: dim2_error_index=ids_int_invalid

  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_turbulence  !    Description of plasma turbulence
  type (ids_ids_properties) :: ids_properties  ! /turbulence/ids_properties -
  type (ids_identifier) :: grid_2d_type  ! /turbulence/grid_2d_type - Selection of one of a set of grid types for grid_2d
  type (ids_turbulence_profiles_2d_grid),pointer :: grid_2d(:) => null()  ! /turbulence/grid_2d(i) - Values for the 2D grid, for various time slices. The timebase of this array of structure must be a s
  type (ids_turbulence_profiles_2d),pointer :: profiles_2d(:) => null()  ! /turbulence/profiles_2d(i) - Fluctuating physical quantities for various time slices
  type (ids_code) :: code  ! /turbulence/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include wall/dd_wall.xsd
type ids_wall_global_quantitites_electrons  !    Simple 0D description of plasma-wall interaction, related to electrons
  real(ids_real),pointer  :: pumping_speed(:) => null()     ! /pumping_speed - Pumped particle flux (in equivalent electrons)
  real(ids_real),pointer  :: pumping_speed_error_upper(:) => null()
  real(ids_real),pointer  :: pumping_speed_error_lower(:) => null()
  integer(ids_int) :: pumping_speed_error_index=ids_int_invalid

  real(ids_real),pointer  :: particle_flux_from_plasma(:) => null()     ! /particle_flux_from_plasma - Particle flux from the plasma (in equivalent electrons)
  real(ids_real),pointer  :: particle_flux_from_plasma_error_upper(:) => null()
  real(ids_real),pointer  :: particle_flux_from_plasma_error_lower(:) => null()
  integer(ids_int) :: particle_flux_from_plasma_error_index=ids_int_invalid

  real(ids_real),pointer  :: particle_flux_from_wall(:,:) => null()     ! /particle_flux_from_wall - Particle flux from the wall corresponding to the conversion into various neutral types (first dimens
  real(ids_real),pointer  :: particle_flux_from_wall_error_upper(:,:) => null()
  real(ids_real),pointer  :: particle_flux_from_wall_error_lower(:,:) => null()
  integer(ids_int) :: particle_flux_from_wall_error_index=ids_int_invalid

  real(ids_real),pointer  :: gas_puff(:) => null()     ! /gas_puff - Gas puff rate (in equivalent electrons)
  real(ids_real),pointer  :: gas_puff_error_upper(:) => null()
  real(ids_real),pointer  :: gas_puff_error_lower(:) => null()
  integer(ids_int) :: gas_puff_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_inner_target(:) => null()     ! /power_inner_target - Electron power on the inner target
  real(ids_real),pointer  :: power_inner_target_error_upper(:) => null()
  real(ids_real),pointer  :: power_inner_target_error_lower(:) => null()
  integer(ids_int) :: power_inner_target_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_outer_target(:) => null()     ! /power_outer_target - Electron power on the inner target
  real(ids_real),pointer  :: power_outer_target_error_upper(:) => null()
  real(ids_real),pointer  :: power_outer_target_error_lower(:) => null()
  integer(ids_int) :: power_outer_target_error_index=ids_int_invalid

endtype

type ids_wall_global_quantitites_neutral  !    Simple 0D description of plasma-wall interaction, related to a given neutral species
  type (ids_plasma_composition_neutral_element_constant),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H, D, CD4, ...)
  real(ids_real),pointer  :: pumping_speed(:) => null()     ! /pumping_speed - Pumped particle flux for that species
  real(ids_real),pointer  :: pumping_speed_error_upper(:) => null()
  real(ids_real),pointer  :: pumping_speed_error_lower(:) => null()
  integer(ids_int) :: pumping_speed_error_index=ids_int_invalid

  real(ids_real),pointer  :: particle_flux_from_plasma(:) => null()     ! /particle_flux_from_plasma - Particle flux from the plasma for that species
  real(ids_real),pointer  :: particle_flux_from_plasma_error_upper(:) => null()
  real(ids_real),pointer  :: particle_flux_from_plasma_error_lower(:) => null()
  integer(ids_int) :: particle_flux_from_plasma_error_index=ids_int_invalid

  real(ids_real),pointer  :: particle_flux_from_wall(:,:) => null()     ! /particle_flux_from_wall - Particle flux from the wall corresponding to the conversion into various neutral types (first dimens
  real(ids_real),pointer  :: particle_flux_from_wall_error_upper(:,:) => null()
  real(ids_real),pointer  :: particle_flux_from_wall_error_lower(:,:) => null()
  integer(ids_int) :: particle_flux_from_wall_error_index=ids_int_invalid

  real(ids_real),pointer  :: gas_puff(:) => null()     ! /gas_puff - Gas puff rate for that species
  real(ids_real),pointer  :: gas_puff_error_upper(:) => null()
  real(ids_real),pointer  :: gas_puff_error_lower(:) => null()
  integer(ids_int) :: gas_puff_error_index=ids_int_invalid

  real(ids_real),pointer  :: wall_inventory(:) => null()     ! /wall_inventory - Wall inventory, i.e. cumulated exchange of neutral species between plasma and wall from t = 0, posit
  real(ids_real),pointer  :: wall_inventory_error_upper(:) => null()
  real(ids_real),pointer  :: wall_inventory_error_lower(:) => null()
  integer(ids_int) :: wall_inventory_error_index=ids_int_invalid

  real(ids_real),pointer  :: recycling_particles_coefficient(:,:) => null()     ! /recycling_particles_coefficient - Particle recycling coefficient corresponding to the conversion into various neutral types (first dim
  real(ids_real),pointer  :: recycling_particles_coefficient_error_upper(:,:) => null()
  real(ids_real),pointer  :: recycling_particles_coefficient_error_lower(:,:) => null()
  integer(ids_int) :: recycling_particles_coefficient_error_index=ids_int_invalid

  real(ids_real),pointer  :: recycling_energy_coefficient(:,:) => null()     ! /recycling_energy_coefficient - Energy recycling coefficient corresponding to the conversion into various neutral types (first dimen
  real(ids_real),pointer  :: recycling_energy_coefficient_error_upper(:,:) => null()
  real(ids_real),pointer  :: recycling_energy_coefficient_error_lower(:,:) => null()
  integer(ids_int) :: recycling_energy_coefficient_error_index=ids_int_invalid

  real(ids_real),pointer  :: sputtering_physical_coefficient(:,:) => null()     ! /sputtering_physical_coefficient - Effective coefficient of physical sputtering for various neutral types (first dimension: 1: cold; 2:
  real(ids_real),pointer  :: sputtering_physical_coefficient_error_upper(:,:) => null()
  real(ids_real),pointer  :: sputtering_physical_coefficient_error_lower(:,:) => null()
  integer(ids_int) :: sputtering_physical_coefficient_error_index=ids_int_invalid

  real(ids_real),pointer  :: sputtering_chemical_coefficient(:,:) => null()     ! /sputtering_chemical_coefficient - Effective coefficient of chemical sputtering for various neutral types (first dimension: 1: cold; 2:
  real(ids_real),pointer  :: sputtering_chemical_coefficient_error_upper(:,:) => null()
  real(ids_real),pointer  :: sputtering_chemical_coefficient_error_lower(:,:) => null()
  integer(ids_int) :: sputtering_chemical_coefficient_error_index=ids_int_invalid

endtype

type ids_wall_global_quantitites  !    Simple 0D description of plasma-wall interaction
  type (ids_wall_global_quantitites_electrons) :: electrons  ! /electrons - Quantities related to electrons
  type (ids_wall_global_quantitites_neutral),pointer :: neutral(:) => null()  ! /neutral(i) - Quantities related to the various neutral species
  real(ids_real),pointer  :: temperature(:) => null()     ! /temperature - Wall temperature
  real(ids_real),pointer  :: temperature_error_upper(:) => null()
  real(ids_real),pointer  :: temperature_error_lower(:) => null()
  integer(ids_int) :: temperature_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_from_plasma(:) => null()     ! /power_from_plasma - Power flowing from the plasma to the wall
  real(ids_real),pointer  :: power_from_plasma_error_upper(:) => null()
  real(ids_real),pointer  :: power_from_plasma_error_lower(:) => null()
  integer(ids_int) :: power_from_plasma_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_to_cooling(:) => null()     ! /power_to_cooling - Power to cooling systems
  real(ids_real),pointer  :: power_to_cooling_error_upper(:) => null()
  real(ids_real),pointer  :: power_to_cooling_error_lower(:) => null()
  integer(ids_int) :: power_to_cooling_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_inner_target_ion_total(:) => null()     ! /power_inner_target_ion_total - Total ion (summed over ion species) power on the inner target
  real(ids_real),pointer  :: power_inner_target_ion_total_error_upper(:) => null()
  real(ids_real),pointer  :: power_inner_target_ion_total_error_lower(:) => null()
  integer(ids_int) :: power_inner_target_ion_total_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_density_inner_target_max(:) => null()     ! /power_density_inner_target_max - Maximum power density on the inner target
  real(ids_real),pointer  :: power_density_inner_target_max_error_upper(:) => null()
  real(ids_real),pointer  :: power_density_inner_target_max_error_lower(:) => null()
  integer(ids_int) :: power_density_inner_target_max_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_density_outer_target_max(:) => null()     ! /power_density_outer_target_max - Maximum power density on the outer target
  real(ids_real),pointer  :: power_density_outer_target_max_error_upper(:) => null()
  real(ids_real),pointer  :: power_density_outer_target_max_error_lower(:) => null()
  integer(ids_int) :: power_density_outer_target_max_error_index=ids_int_invalid

endtype

! SPECIAL STRUCTURE data / time
type ids_wall_2d_vessel_element_j_tor  !    Toroidal current induced in this block element
  real(ids_real), pointer  :: data(:) => null()     ! /j_tor - Toroidal current induced in this block element
  real(ids_real), pointer  :: data_error_upper(:) => null()
  real(ids_real), pointer  :: data_error_lower(:) => null()
  integer(ids_int)  :: data_error_index=ids_int_invalid

  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

type ids_wall_2d_vessel_element  !    2D vessel block element description
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the block element
  type (ids_rz1d_static) :: outline  ! /outline - Outline of the block element. Do NOT repeat the first point for closed contours
  real(ids_real)  :: resistivity=ids_real_invalid       ! /resistivity - Resistivity of the block element
  real(ids_real)  :: resistivity_error_upper=ids_real_invalid
  real(ids_real)  :: resistivity_error_lower=ids_real_invalid
  integer(ids_int) :: resistivity_error_index=ids_int_invalid

  type (ids_wall_2d_vessel_element_j_tor) :: j_tor  ! /j_tor - Toroidal current induced in this block element
  real(ids_real)  :: resistance=ids_real_invalid       ! /resistance - Resistance of the block element
  real(ids_real)  :: resistance_error_upper=ids_real_invalid
  real(ids_real)  :: resistance_error_lower=ids_real_invalid
  integer(ids_int) :: resistance_error_index=ids_int_invalid

endtype

type ids_wall_2d_vessel_annular  !    2D vessel annular description
  type (ids_rz1d_static) :: outline_inner  ! /outline_inner - Inner vessel outline. Do NOT repeat the first point for closed contours
  type (ids_rz1d_static) :: outline_outer  ! /outline_outer - Outer vessel outline. Do NOT repeat the first point for closed contours
  real(ids_real)  :: resistivity=ids_real_invalid       ! /resistivity - Resistivity of the vessel unit
  real(ids_real)  :: resistivity_error_upper=ids_real_invalid
  real(ids_real)  :: resistivity_error_lower=ids_real_invalid
  integer(ids_int) :: resistivity_error_index=ids_int_invalid

endtype

type ids_wall_2d_vessel_unit  !    2D vessel unit description
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the vessel unit
  type (ids_wall_2d_vessel_annular) :: annular  ! /annular - Annular representation of a vessel layer by two contours, inner and outer
  type (ids_wall_2d_vessel_element),pointer :: element(:) => null()  ! /element(i) - Set of block elements
endtype

type ids_wall_2d_vessel  !    2D vessel description
  type (ids_identifier_static) :: type  ! /type - Type of the vessel description. index = 0 for the official single/multiple annular vessel and 1 for
  type (ids_wall_2d_vessel_unit),pointer :: unit(:) => null()  ! /unit(i) - Set of vessel units
endtype

type ids_wall_2d_limiter_unit  !    2D limiter unit description
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the limiter unit
  integer(ids_int)  :: closed=ids_int_invalid       ! /closed - Flag identifying whether the contour is closed (1) or open (0)
  type (ids_rz1d_static) :: outline  ! /outline - Irregular outline of the limiting surface. Do NOT repeat the first point for closed contours
  real(ids_real)  :: resistivity=ids_real_invalid       ! /resistivity - Resistivity of the limiter unit
  real(ids_real)  :: resistivity_error_upper=ids_real_invalid
  real(ids_real)  :: resistivity_error_lower=ids_real_invalid
  integer(ids_int) :: resistivity_error_index=ids_int_invalid

endtype

type ids_wall_2d_limiter  !    2D limiter description
  type (ids_identifier_static) :: type  ! /type - Type of the limiter description. index = 0 for the official single contour limiter and 1 for the off
  type (ids_wall_2d_limiter_unit),pointer :: unit(:) => null()  ! /unit(i) - Set of limiter units
endtype

type ids_wall_2d_mobile_unit  !    2D mobile parts description
  character(len=ids_string_length), dimension(:), pointer ::name => null()       ! /name - Name of the mobile unit
  integer(ids_int)  :: closed=ids_int_invalid       ! /closed - Flag identifying whether the contour is closed (1) or open (0)
  type (ids_rz1d_dynamic_aos_time),pointer :: outline(:) => null()  ! /outline(i) - Irregular outline of the mobile unit, for a set of time slices. Do NOT repeat the first point for cl
  real(ids_real)  :: resistivity=ids_real_invalid       ! /resistivity - Resistivity of the mobile unit
  real(ids_real)  :: resistivity_error_upper=ids_real_invalid
  real(ids_real)  :: resistivity_error_lower=ids_real_invalid
  integer(ids_int) :: resistivity_error_index=ids_int_invalid

endtype

type ids_wall_2d_mobile  !    2D mobile parts description
  type (ids_identifier_static) :: type  ! /type - Type of the description
  type (ids_wall_2d_mobile_unit),pointer :: unit(:) => null()  ! /unit(i) - Set of mobile units
endtype

type ids_wall_2d  !    2D wall description
  type (ids_identifier_static) :: type  ! /type - Type of the description. index = 0 for equilibrium codes (single closed limiter and vessel); 1 for g
  type (ids_wall_2d_limiter) :: limiter  ! /limiter - Description of the immobile limiting surface(s) or plasma facing components for defining the Last Cl
  type (ids_wall_2d_mobile) :: mobile  ! /mobile - In case of mobile plasma facing components, use the time-dependent description below this node to pr
  type (ids_wall_2d_vessel) :: vessel  ! /vessel - Mechanical structure of the vacuum vessel. The vessel is described as a set of nested layers with gi
endtype

type ids_wall  !    Description of the torus wall and its interaction with the plasma
  type (ids_ids_properties) :: ids_properties  ! /wall/ids_properties -
  type (ids_wall_global_quantitites) :: global_quantities  ! /wall/global_quantities - Simple 0D description of plasma-wall interaction
  type (ids_wall_2d),pointer :: description_2d(:) => null()  ! /wall/description_2d(i) - Set of 2D wall descriptions, for each type of possible physics or engineering configurations necessa
  type (ids_code) :: code  ! /wall/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include waves/dd_waves.xsd
type ids_waves_CPX_1D  !    Structure for 1D complex number, real and imaginary parts
  real(ids_real),pointer  :: real(:) => null()     ! /real - Real part
  real(ids_real),pointer  :: real_error_upper(:) => null()
  real(ids_real),pointer  :: real_error_lower(:) => null()
  integer(ids_int) :: real_error_index=ids_int_invalid

  real(ids_real),pointer  :: imaginary(:) => null()     ! /imaginary - Imaginary part
  real(ids_real),pointer  :: imaginary_error_upper(:) => null()
  real(ids_real),pointer  :: imaginary_error_lower(:) => null()
  integer(ids_int) :: imaginary_error_index=ids_int_invalid

endtype

type ids_waves_CPX_amp_phase_1D  !    Structure for 1D complex number, amplitude and phase
  real(ids_real),pointer  :: amplitude(:) => null()     ! /amplitude - Amplitude
  real(ids_real),pointer  :: amplitude_error_upper(:) => null()
  real(ids_real),pointer  :: amplitude_error_lower(:) => null()
  integer(ids_int) :: amplitude_error_index=ids_int_invalid

  real(ids_real),pointer  :: phase(:) => null()     ! /phase - Phase
  real(ids_real),pointer  :: phase_error_upper(:) => null()
  real(ids_real),pointer  :: phase_error_lower(:) => null()
  integer(ids_int) :: phase_error_index=ids_int_invalid

endtype

type ids_waves_CPX_amp_phase_2D  !    Structure for 2D complex number, amplitude and phase
  real(ids_real),pointer  :: amplitude(:,:) => null()     ! /amplitude - Amplitude
  real(ids_real),pointer  :: amplitude_error_upper(:,:) => null()
  real(ids_real),pointer  :: amplitude_error_lower(:,:) => null()
  integer(ids_int) :: amplitude_error_index=ids_int_invalid

  real(ids_real),pointer  :: phase(:,:) => null()     ! /phase - Phase
  real(ids_real),pointer  :: phase_error_upper(:,:) => null()
  real(ids_real),pointer  :: phase_error_lower(:,:) => null()
  integer(ids_int) :: phase_error_index=ids_int_invalid

endtype

type ids_waves_rzphipsitheta1d_dynamic_aos3  !    Structure for R, Z, Phi, Psi, Theta positions (1D, dynamic within a type 3 array of structure)
  real(ids_real),pointer  :: r(:) => null()     ! /r - Major radius
  real(ids_real),pointer  :: r_error_upper(:) => null()
  real(ids_real),pointer  :: r_error_lower(:) => null()
  integer(ids_int) :: r_error_index=ids_int_invalid

  real(ids_real),pointer  :: z(:) => null()     ! /z - Height
  real(ids_real),pointer  :: z_error_upper(:) => null()
  real(ids_real),pointer  :: z_error_lower(:) => null()
  integer(ids_int) :: z_error_index=ids_int_invalid

  real(ids_real),pointer  :: phi(:) => null()     ! /phi - Toroidal angle
  real(ids_real),pointer  :: phi_error_upper(:) => null()
  real(ids_real),pointer  :: phi_error_lower(:) => null()
  integer(ids_int) :: phi_error_index=ids_int_invalid

  real(ids_real),pointer  :: psi(:) => null()     ! /psi - Poloidal flux
  real(ids_real),pointer  :: psi_error_upper(:) => null()
  real(ids_real),pointer  :: psi_error_lower(:) => null()
  integer(ids_int) :: psi_error_index=ids_int_invalid

  real(ids_real),pointer  :: theta(:) => null()     ! /theta - Poloidal angle
  real(ids_real),pointer  :: theta_error_upper(:) => null()
  real(ids_real),pointer  :: theta_error_lower(:) => null()
  integer(ids_int) :: theta_error_index=ids_int_invalid

endtype

type ids_waves_coherent_wave_beam_tracing_ion_state  !    State related quantities for beam tracing
  real(ids_real)  :: z_min=ids_real_invalid       ! /z_min - Minimum Z of the charge state bundle (z_min = z_max = 0 for a neutral)
  real(ids_real)  :: z_min_error_upper=ids_real_invalid
  real(ids_real)  :: z_min_error_lower=ids_real_invalid
  integer(ids_int) :: z_min_error_index=ids_int_invalid

  real(ids_real)  :: z_max=ids_real_invalid       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(ids_real)  :: z_max_error_upper=ids_real_invalid
  real(ids_real)  :: z_max_error_lower=ids_real_invalid
  integer(ids_int) :: z_max_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  real(ids_real),pointer  :: power(:) => null()     ! /power - Power absorbed along the beam by the species
  real(ids_real),pointer  :: power_error_upper(:) => null()
  real(ids_real),pointer  :: power_error_lower(:) => null()
  integer(ids_int) :: power_error_index=ids_int_invalid

endtype

type ids_waves_coherent_wave_beam_tracing_ion  !    Ion related quantities for beam tracing
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(ids_real)  :: z_ion=ids_real_invalid       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed).
  real(ids_real)  :: z_ion_error_upper=ids_real_invalid
  real(ids_real)  :: z_ion_error_lower=ids_real_invalid
  integer(ids_int) :: z_ion_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H+, D+, T+, He+2, C+, D2, DT, CD4, ...)
  real(ids_real),pointer  :: power(:) => null()     ! /power - Power absorbed along the beam by the species
  real(ids_real),pointer  :: power_error_upper(:) => null()
  real(ids_real),pointer  :: power_error_lower(:) => null()
  integer(ids_int) :: power_error_index=ids_int_invalid

  integer(ids_int)  :: multiple_states_flag=ids_int_invalid       ! /multiple_states_flag - Multiple state calculation flag : 0-Only one state is considered; 1-Multiple states are considered a
  type (ids_waves_coherent_wave_beam_tracing_ion_state),pointer :: state(:) => null()  ! /state(i) - Collisional exchange with the various states of the ion species (ionisation, energy, excitation, ...
endtype

type ids_waves_coherent_wave_beam_tracing_electrons  !    Electrons related quantities for beam tracing
  real(ids_real),pointer  :: power(:) => null()     ! /power - Power absorbed along the beam by the species
  real(ids_real),pointer  :: power_error_upper(:) => null()
  real(ids_real),pointer  :: power_error_lower(:) => null()
  integer(ids_int) :: power_error_index=ids_int_invalid

endtype

type ids_waves_coherent_wave_beam_tracing_power_flow  !    Power flow for beam tracing
  real(ids_real),pointer  :: perpendicular(:) => null()     ! /perpendicular - Normalized power flow in the direction perpendicular to the magnetic field
  real(ids_real),pointer  :: perpendicular_error_upper(:) => null()
  real(ids_real),pointer  :: perpendicular_error_lower(:) => null()
  integer(ids_int) :: perpendicular_error_index=ids_int_invalid

  real(ids_real),pointer  :: parallel(:) => null()     ! /parallel - Normalized power flow in the direction parallel to the magnetic field
  real(ids_real),pointer  :: parallel_error_upper(:) => null()
  real(ids_real),pointer  :: parallel_error_lower(:) => null()
  integer(ids_int) :: parallel_error_index=ids_int_invalid

endtype

type ids_waves_coherent_wave_beam_tracing_beam_e_field  !    Components of the electric field for beam tracing
  type (ids_waves_CPX_1D) :: plus  ! /plus - Left hand polarised electric field component
  type (ids_waves_CPX_1D) :: minus  ! /minus - Right hand polarised electric field component
  type (ids_waves_CPX_1D) :: parallel  ! /parallel - Parallel to magnetic field polarised electric field component
endtype

type ids_waves_coherent_wave_beam_tracing_beam_k  !    Beam wave vector
  real(ids_real),pointer  :: k_r(:) => null()     ! /k_r - Wave vector component in the major radius direction
  real(ids_real),pointer  :: k_r_error_upper(:) => null()
  real(ids_real),pointer  :: k_r_error_lower(:) => null()
  integer(ids_int) :: k_r_error_index=ids_int_invalid

  real(ids_real),pointer  :: k_z(:) => null()     ! /k_z - Wave vector component in the vertical direction
  real(ids_real),pointer  :: k_z_error_upper(:) => null()
  real(ids_real),pointer  :: k_z_error_lower(:) => null()
  integer(ids_int) :: k_z_error_index=ids_int_invalid

  real(ids_real),pointer  :: k_tor(:) => null()     ! /k_tor - Wave vector component in the toroidal direction
  real(ids_real),pointer  :: k_tor_error_upper(:) => null()
  real(ids_real),pointer  :: k_tor_error_lower(:) => null()
  integer(ids_int) :: k_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: n_parallel(:) => null()     ! /n_parallel - Parallel refractive index
  real(ids_real),pointer  :: n_parallel_error_upper(:) => null()
  real(ids_real),pointer  :: n_parallel_error_lower(:) => null()
  integer(ids_int) :: n_parallel_error_index=ids_int_invalid

  real(ids_real),pointer  :: n_perpendicular(:) => null()     ! /n_perpendicular - Perpendicular refractive index
  real(ids_real),pointer  :: n_perpendicular_error_upper(:) => null()
  real(ids_real),pointer  :: n_perpendicular_error_lower(:) => null()
  integer(ids_int) :: n_perpendicular_error_index=ids_int_invalid

  integer(ids_int),pointer  :: n_tor(:) => null()      ! /n_tor - Toroidal wave number, contains a single value if varying_ntor = 1 to avoid useless repetition consta
  integer(ids_int)  :: varying_n_tor=ids_int_invalid       ! /varying_n_tor - Flag telling whether n_tor is constant along the ray path (0) or varying (1)
endtype

type ids_waves_coherent_wave_beam_tracing_beam  !    Beam description
  real(ids_real)  :: power_initial=ids_real_invalid       ! /power_initial - Initial power in the ray/beam
  real(ids_real)  :: power_initial_error_upper=ids_real_invalid
  real(ids_real)  :: power_initial_error_lower=ids_real_invalid
  integer(ids_int) :: power_initial_error_index=ids_int_invalid

  real(ids_real),pointer  :: length(:) => null()     ! /length - Ray/beam curvilinear length
  real(ids_real),pointer  :: length_error_upper(:) => null()
  real(ids_real),pointer  :: length_error_lower(:) => null()
  integer(ids_int) :: length_error_index=ids_int_invalid

  type (ids_waves_rzphipsitheta1d_dynamic_aos3) :: position  ! /position - Position of the ray/beam along its path
  type (ids_waves_coherent_wave_beam_tracing_beam_k) :: wave_vector  ! /wave_vector - Wave vector of the ray/beam along its path
  type (ids_waves_coherent_wave_beam_tracing_beam_e_field) :: e_field  ! /e_field - Electric field polarization of the ray/beam along its path
  type (ids_waves_coherent_wave_beam_tracing_power_flow) :: power_flow_norm  ! /power_flow_norm - Normalised power flow
  type (ids_waves_coherent_wave_beam_tracing_electrons) :: electrons  ! /electrons - Quantities related to the electrons
  type (ids_waves_coherent_wave_beam_tracing_ion),pointer :: ion(:) => null()  ! /ion(i) - Quantities related to the different ion species
endtype

type ids_waves_coherent_wave_beam_tracing  !    Beam tracing calculations for a given time slice
  type (ids_waves_coherent_wave_beam_tracing_beam),pointer :: beam(:) => null()  ! /beam(i) - Set of rays/beams describing the wave propagation
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_waves_coherent_wave_global_quantities_ion_state  !    Global quantities related to a given ion species state
  real(ids_real)  :: z_min=ids_real_invalid       ! /z_min - Minimum Z of the charge state bundle (z_min = z_max = 0 for a neutral)
  real(ids_real)  :: z_min_error_upper=ids_real_invalid
  real(ids_real)  :: z_min_error_lower=ids_real_invalid
  integer(ids_int) :: z_min_error_index=ids_int_invalid

  real(ids_real)  :: z_max=ids_real_invalid       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(ids_real)  :: z_max_error_upper=ids_real_invalid
  real(ids_real)  :: z_max_error_lower=ids_real_invalid
  integer(ids_int) :: z_max_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  real(ids_real)  :: power_thermal=ids_real_invalid       ! /power_thermal - Wave power absorbed by the thermal particle population
  real(ids_real)  :: power_thermal_error_upper=ids_real_invalid
  real(ids_real)  :: power_thermal_error_lower=ids_real_invalid
  integer(ids_int) :: power_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_thermal_n_tor(:) => null()     ! /power_thermal_n_tor - Wave power absorbed by the thermal particle population per toroidal mode number
  real(ids_real),pointer  :: power_thermal_n_tor_error_upper(:) => null()
  real(ids_real),pointer  :: power_thermal_n_tor_error_lower(:) => null()
  integer(ids_int) :: power_thermal_n_tor_error_index=ids_int_invalid

  real(ids_real)  :: power_fast=ids_real_invalid       ! /power_fast - Wave power absorbed by the fast particle population
  real(ids_real)  :: power_fast_error_upper=ids_real_invalid
  real(ids_real)  :: power_fast_error_lower=ids_real_invalid
  integer(ids_int) :: power_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_fast_n_tor(:) => null()     ! /power_fast_n_tor - Wave power absorbed by the fast particle population per toroidal mode number
  real(ids_real),pointer  :: power_fast_n_tor_error_upper(:) => null()
  real(ids_real),pointer  :: power_fast_n_tor_error_lower(:) => null()
  integer(ids_int) :: power_fast_n_tor_error_index=ids_int_invalid

endtype

type ids_waves_coherent_wave_global_quantities_ion  !    Global quantities related to a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(ids_real)  :: z_ion=ids_real_invalid       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed).
  real(ids_real)  :: z_ion_error_upper=ids_real_invalid
  real(ids_real)  :: z_ion_error_lower=ids_real_invalid
  integer(ids_int) :: z_ion_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H+, D+, T+, He+2, C+, D2, DT, CD4, ...)
  real(ids_real)  :: power_thermal=ids_real_invalid       ! /power_thermal - Wave power absorbed by the thermal particle population
  real(ids_real)  :: power_thermal_error_upper=ids_real_invalid
  real(ids_real)  :: power_thermal_error_lower=ids_real_invalid
  integer(ids_int) :: power_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_thermal_n_tor(:) => null()     ! /power_thermal_n_tor - Wave power absorbed by the thermal particle population per toroidal mode number
  real(ids_real),pointer  :: power_thermal_n_tor_error_upper(:) => null()
  real(ids_real),pointer  :: power_thermal_n_tor_error_lower(:) => null()
  integer(ids_int) :: power_thermal_n_tor_error_index=ids_int_invalid

  real(ids_real)  :: power_fast=ids_real_invalid       ! /power_fast - Wave power absorbed by the fast particle population
  real(ids_real)  :: power_fast_error_upper=ids_real_invalid
  real(ids_real)  :: power_fast_error_lower=ids_real_invalid
  integer(ids_int) :: power_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_fast_n_tor(:) => null()     ! /power_fast_n_tor - Wave power absorbed by the fast particle population per toroidal mode number
  real(ids_real),pointer  :: power_fast_n_tor_error_upper(:) => null()
  real(ids_real),pointer  :: power_fast_n_tor_error_lower(:) => null()
  integer(ids_int) :: power_fast_n_tor_error_index=ids_int_invalid

  integer(ids_int)  :: multiple_states_flag=ids_int_invalid       ! /multiple_states_flag - Multiple state calculation flag : 0-Only one state is considered; 1-Multiple states are considered a
  integer(ids_int)  :: distribution_assumption=ids_int_invalid       ! /distribution_assumption - Assumption on the distribution function used by the wave solver to calculate the power deposition on
  type (ids_waves_coherent_wave_global_quantities_ion_state),pointer :: state(:) => null()  ! /state(i) - Collisional exchange with the various states of the ion species (ionisation, energy, excitation, ...
endtype

type ids_waves_coherent_wave_global_quantities_electrons  !    Global quantities related to electrons
  real(ids_real)  :: power_thermal=ids_real_invalid       ! /power_thermal - Wave power absorbed by the thermal particle population
  real(ids_real)  :: power_thermal_error_upper=ids_real_invalid
  real(ids_real)  :: power_thermal_error_lower=ids_real_invalid
  integer(ids_int) :: power_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_thermal_n_tor(:) => null()     ! /power_thermal_n_tor - Wave power absorbed by the thermal particle population per toroidal mode number
  real(ids_real),pointer  :: power_thermal_n_tor_error_upper(:) => null()
  real(ids_real),pointer  :: power_thermal_n_tor_error_lower(:) => null()
  integer(ids_int) :: power_thermal_n_tor_error_index=ids_int_invalid

  real(ids_real)  :: power_fast=ids_real_invalid       ! /power_fast - Wave power absorbed by the fast particle population
  real(ids_real)  :: power_fast_error_upper=ids_real_invalid
  real(ids_real)  :: power_fast_error_lower=ids_real_invalid
  integer(ids_int) :: power_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_fast_n_tor(:) => null()     ! /power_fast_n_tor - Wave power absorbed by the fast particle population per toroidal mode number
  real(ids_real),pointer  :: power_fast_n_tor_error_upper(:) => null()
  real(ids_real),pointer  :: power_fast_n_tor_error_lower(:) => null()
  integer(ids_int) :: power_fast_n_tor_error_index=ids_int_invalid

  integer(ids_int)  :: distribution_assumption=ids_int_invalid       ! /distribution_assumption - Assumption on the distribution function used by the wave solver to calculate the power deposition on
endtype

type ids_waves_coherent_wave_global_quantities  !    Global quantities (RF waves) for a given time slice
  real(ids_real)  :: frequency=ids_real_invalid       ! /frequency - Wave frequency
  real(ids_real)  :: frequency_error_upper=ids_real_invalid
  real(ids_real)  :: frequency_error_lower=ids_real_invalid
  integer(ids_int) :: frequency_error_index=ids_int_invalid

  integer(ids_int),pointer  :: n_tor(:) => null()      ! /n_tor - Toroidal mode numbers
  real(ids_real)  :: power=ids_real_invalid       ! /power - Total absorbed wave power
  real(ids_real)  :: power_error_upper=ids_real_invalid
  real(ids_real)  :: power_error_lower=ids_real_invalid
  integer(ids_int) :: power_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_n_tor(:) => null()     ! /power_n_tor - Absorbed wave power per toroidal mode number
  real(ids_real),pointer  :: power_n_tor_error_upper(:) => null()
  real(ids_real),pointer  :: power_n_tor_error_lower(:) => null()
  integer(ids_int) :: power_n_tor_error_index=ids_int_invalid

  real(ids_real)  :: current_tor=ids_real_invalid       ! /current_tor - Wave driven toroidal current from a stand alone calculation (not consistent with other sources)
  real(ids_real)  :: current_tor_error_upper=ids_real_invalid
  real(ids_real)  :: current_tor_error_lower=ids_real_invalid
  integer(ids_int) :: current_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: current_tor_n_tor(:) => null()     ! /current_tor_n_tor - Wave driven toroidal current from a stand alone calculation (not consistent with other sources) per
  real(ids_real),pointer  :: current_tor_n_tor_error_upper(:) => null()
  real(ids_real),pointer  :: current_tor_n_tor_error_lower(:) => null()
  integer(ids_int) :: current_tor_n_tor_error_index=ids_int_invalid

  type (ids_waves_coherent_wave_global_quantities_electrons) :: electrons  ! /electrons - Quantities related to the electrons
  type (ids_waves_coherent_wave_global_quantities_ion),pointer :: ion(:) => null()  ! /ion(i) - Quantities related to the different ion species
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_waves_coherent_wave_profiles_1d_ion_state  !    Radial profiles (RF waves) related to a given ion species state
  real(ids_real)  :: z_min=ids_real_invalid       ! /z_min - Minimum Z of the charge state bundle (z_min = z_max = 0 for a neutral)
  real(ids_real)  :: z_min_error_upper=ids_real_invalid
  real(ids_real)  :: z_min_error_lower=ids_real_invalid
  integer(ids_int) :: z_min_error_index=ids_int_invalid

  real(ids_real)  :: z_max=ids_real_invalid       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(ids_real)  :: z_max_error_upper=ids_real_invalid
  real(ids_real)  :: z_max_error_lower=ids_real_invalid
  integer(ids_int) :: z_max_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  real(ids_real),pointer  :: power_density_thermal(:) => null()     ! /power_density_thermal - Flux surface averaged absorbed wave power density on the thermal species
  real(ids_real),pointer  :: power_density_thermal_error_upper(:) => null()
  real(ids_real),pointer  :: power_density_thermal_error_lower(:) => null()
  integer(ids_int) :: power_density_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_density_thermal_n_tor(:,:) => null()     ! /power_density_thermal_n_tor - Flux surface averaged absorbed wave power density on the thermal species, per toroidal mode number
  real(ids_real),pointer  :: power_density_thermal_n_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_density_thermal_n_tor_error_lower(:,:) => null()
  integer(ids_int) :: power_density_thermal_n_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_density_fast(:) => null()     ! /power_density_fast - Flux surface averaged absorbed wave power density on the fast species
  real(ids_real),pointer  :: power_density_fast_error_upper(:) => null()
  real(ids_real),pointer  :: power_density_fast_error_lower(:) => null()
  integer(ids_int) :: power_density_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_density_fast_n_tor(:,:) => null()     ! /power_density_fast_n_tor - Flux surface averaged absorbed wave power density on the fast species, per toroidal mode number
  real(ids_real),pointer  :: power_density_fast_n_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_density_fast_n_tor_error_lower(:,:) => null()
  integer(ids_int) :: power_density_fast_n_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_inside_thermal(:) => null()     ! /power_inside_thermal - Absorbed wave power on thermal species inside a flux surface (cumulative volume integral of the abso
  real(ids_real),pointer  :: power_inside_thermal_error_upper(:) => null()
  real(ids_real),pointer  :: power_inside_thermal_error_lower(:) => null()
  integer(ids_int) :: power_inside_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_inside_thermal_n_tor(:,:) => null()     ! /power_inside_thermal_n_tor - Absorbed wave power on thermal species inside a flux surface (cumulative volume integral of the abso
  real(ids_real),pointer  :: power_inside_thermal_n_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_inside_thermal_n_tor_error_lower(:,:) => null()
  integer(ids_int) :: power_inside_thermal_n_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_inside_fast(:) => null()     ! /power_inside_fast - Absorbed wave power on thermal species inside a flux surface (cumulative volume integral of the abso
  real(ids_real),pointer  :: power_inside_fast_error_upper(:) => null()
  real(ids_real),pointer  :: power_inside_fast_error_lower(:) => null()
  integer(ids_int) :: power_inside_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_inside_fast_n_tor(:,:) => null()     ! /power_inside_fast_n_tor - Absorbed wave power on thermal species inside a flux surface (cumulative volume integral of the abso
  real(ids_real),pointer  :: power_inside_fast_n_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_inside_fast_n_tor_error_lower(:,:) => null()
  integer(ids_int) :: power_inside_fast_n_tor_error_index=ids_int_invalid

endtype

type ids_waves_coherent_wave_profiles_1d_ion  !    Radial profiles (RF waves) related to a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(ids_real)  :: z_ion=ids_real_invalid       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed).
  real(ids_real)  :: z_ion_error_upper=ids_real_invalid
  real(ids_real)  :: z_ion_error_lower=ids_real_invalid
  integer(ids_int) :: z_ion_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H+, D+, T+, He+2, C+, D2, DT, CD4, ...)
  real(ids_real),pointer  :: power_density_thermal(:) => null()     ! /power_density_thermal - Flux surface averaged absorbed wave power density on the thermal species
  real(ids_real),pointer  :: power_density_thermal_error_upper(:) => null()
  real(ids_real),pointer  :: power_density_thermal_error_lower(:) => null()
  integer(ids_int) :: power_density_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_density_thermal_n_tor(:,:) => null()     ! /power_density_thermal_n_tor - Flux surface averaged absorbed wave power density on the thermal species, per toroidal mode number
  real(ids_real),pointer  :: power_density_thermal_n_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_density_thermal_n_tor_error_lower(:,:) => null()
  integer(ids_int) :: power_density_thermal_n_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_density_fast(:) => null()     ! /power_density_fast - Flux surface averaged absorbed wave power density on the fast species
  real(ids_real),pointer  :: power_density_fast_error_upper(:) => null()
  real(ids_real),pointer  :: power_density_fast_error_lower(:) => null()
  integer(ids_int) :: power_density_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_density_fast_n_tor(:,:) => null()     ! /power_density_fast_n_tor - Flux surface averaged absorbed wave power density on the fast species, per toroidal mode number
  real(ids_real),pointer  :: power_density_fast_n_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_density_fast_n_tor_error_lower(:,:) => null()
  integer(ids_int) :: power_density_fast_n_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_inside_thermal(:) => null()     ! /power_inside_thermal - Absorbed wave power on thermal species inside a flux surface (cumulative volume integral of the abso
  real(ids_real),pointer  :: power_inside_thermal_error_upper(:) => null()
  real(ids_real),pointer  :: power_inside_thermal_error_lower(:) => null()
  integer(ids_int) :: power_inside_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_inside_thermal_n_tor(:,:) => null()     ! /power_inside_thermal_n_tor - Absorbed wave power on thermal species inside a flux surface (cumulative volume integral of the abso
  real(ids_real),pointer  :: power_inside_thermal_n_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_inside_thermal_n_tor_error_lower(:,:) => null()
  integer(ids_int) :: power_inside_thermal_n_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_inside_fast(:) => null()     ! /power_inside_fast - Absorbed wave power on thermal species inside a flux surface (cumulative volume integral of the abso
  real(ids_real),pointer  :: power_inside_fast_error_upper(:) => null()
  real(ids_real),pointer  :: power_inside_fast_error_lower(:) => null()
  integer(ids_int) :: power_inside_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_inside_fast_n_tor(:,:) => null()     ! /power_inside_fast_n_tor - Absorbed wave power on thermal species inside a flux surface (cumulative volume integral of the abso
  real(ids_real),pointer  :: power_inside_fast_n_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_inside_fast_n_tor_error_lower(:,:) => null()
  integer(ids_int) :: power_inside_fast_n_tor_error_index=ids_int_invalid

  integer(ids_int)  :: multiple_states_flag=ids_int_invalid       ! /multiple_states_flag - Multiple state calculation flag : 0-Only one state is considered; 1-Multiple states are considered a
  type (ids_waves_coherent_wave_profiles_1d_ion_state),pointer :: state(:) => null()  ! /state(i) - Collisional exchange with the various states of the ion species (ionisation, energy, excitation, ...
endtype

type ids_waves_coherent_wave_profiles_1d_electrons  !    Radial profiles (RF waves) related to electrons
  real(ids_real),pointer  :: power_density_thermal(:) => null()     ! /power_density_thermal - Flux surface averaged absorbed wave power density on the thermal species
  real(ids_real),pointer  :: power_density_thermal_error_upper(:) => null()
  real(ids_real),pointer  :: power_density_thermal_error_lower(:) => null()
  integer(ids_int) :: power_density_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_density_thermal_n_tor(:,:) => null()     ! /power_density_thermal_n_tor - Flux surface averaged absorbed wave power density on the thermal species, per toroidal mode number
  real(ids_real),pointer  :: power_density_thermal_n_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_density_thermal_n_tor_error_lower(:,:) => null()
  integer(ids_int) :: power_density_thermal_n_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_density_fast(:) => null()     ! /power_density_fast - Flux surface averaged absorbed wave power density on the fast species
  real(ids_real),pointer  :: power_density_fast_error_upper(:) => null()
  real(ids_real),pointer  :: power_density_fast_error_lower(:) => null()
  integer(ids_int) :: power_density_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_density_fast_n_tor(:,:) => null()     ! /power_density_fast_n_tor - Flux surface averaged absorbed wave power density on the fast species, per toroidal mode number
  real(ids_real),pointer  :: power_density_fast_n_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_density_fast_n_tor_error_lower(:,:) => null()
  integer(ids_int) :: power_density_fast_n_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_inside_thermal(:) => null()     ! /power_inside_thermal - Absorbed wave power on thermal species inside a flux surface (cumulative volume integral of the abso
  real(ids_real),pointer  :: power_inside_thermal_error_upper(:) => null()
  real(ids_real),pointer  :: power_inside_thermal_error_lower(:) => null()
  integer(ids_int) :: power_inside_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_inside_thermal_n_tor(:,:) => null()     ! /power_inside_thermal_n_tor - Absorbed wave power on thermal species inside a flux surface (cumulative volume integral of the abso
  real(ids_real),pointer  :: power_inside_thermal_n_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_inside_thermal_n_tor_error_lower(:,:) => null()
  integer(ids_int) :: power_inside_thermal_n_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_inside_fast(:) => null()     ! /power_inside_fast - Absorbed wave power on thermal species inside a flux surface (cumulative volume integral of the abso
  real(ids_real),pointer  :: power_inside_fast_error_upper(:) => null()
  real(ids_real),pointer  :: power_inside_fast_error_lower(:) => null()
  integer(ids_int) :: power_inside_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_inside_fast_n_tor(:,:) => null()     ! /power_inside_fast_n_tor - Absorbed wave power on thermal species inside a flux surface (cumulative volume integral of the abso
  real(ids_real),pointer  :: power_inside_fast_n_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_inside_fast_n_tor_error_lower(:,:) => null()
  integer(ids_int) :: power_inside_fast_n_tor_error_index=ids_int_invalid

endtype

type ids_waves_profiles_1d_e_field_n_tor  !    Components of the surface averaged electric field
  type (ids_waves_CPX_amp_phase_1D) :: plus  ! /plus - Left hand polarised electric field component for every flux surface
  type (ids_waves_CPX_amp_phase_1D) :: minus  ! /minus - Right hand polarised electric field component for every flux surface
  type (ids_waves_CPX_amp_phase_1D) :: parallel  ! /parallel - Parallel electric field component for every flux surface
endtype

type ids_waves_coherent_wave_profiles_1d  !    Radial profiles (RF waves) for a given time slice
  type (ids_core_radial_grid) :: grid  ! /grid - Radial grid
  integer(ids_int),pointer  :: n_tor(:) => null()      ! /n_tor - Toroidal mode numbers
  real(ids_real),pointer  :: power_density(:) => null()     ! /power_density - Flux surface averaged total absorbed wave power density (electrons + ion + fast populations)
  real(ids_real),pointer  :: power_density_error_upper(:) => null()
  real(ids_real),pointer  :: power_density_error_lower(:) => null()
  integer(ids_int) :: power_density_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_density_n_tor(:,:) => null()     ! /power_density_n_tor - Flux surface averaged absorbed wave power density per toroidal mode number
  real(ids_real),pointer  :: power_density_n_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_density_n_tor_error_lower(:,:) => null()
  integer(ids_int) :: power_density_n_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_inside(:) => null()     ! /power_inside - Total absorbed wave power (electrons + ion + fast populations) inside a flux surface (cumulative vol
  real(ids_real),pointer  :: power_inside_error_upper(:) => null()
  real(ids_real),pointer  :: power_inside_error_lower(:) => null()
  integer(ids_int) :: power_inside_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_inside_n_tor(:,:) => null()     ! /power_inside_n_tor - Total absorbed wave power (electrons + ion + fast populations) inside a flux surface (cumulative vol
  real(ids_real),pointer  :: power_inside_n_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_inside_n_tor_error_lower(:,:) => null()
  integer(ids_int) :: power_inside_n_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: current_tor_inside(:) => null()     ! /current_tor_inside - Wave driven toroidal current, inside a flux surface
  real(ids_real),pointer  :: current_tor_inside_error_upper(:) => null()
  real(ids_real),pointer  :: current_tor_inside_error_lower(:) => null()
  integer(ids_int) :: current_tor_inside_error_index=ids_int_invalid

  real(ids_real),pointer  :: current_tor_inside_n_tor(:,:) => null()     ! /current_tor_inside_n_tor - Wave driven toroidal current, inside a flux surface, per toroidal mode number
  real(ids_real),pointer  :: current_tor_inside_n_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: current_tor_inside_n_tor_error_lower(:,:) => null()
  integer(ids_int) :: current_tor_inside_n_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: current_parallel_density(:) => null()     ! /current_parallel_density - Flux surface averaged wave driven parallel current density = average(j.B) / B0, where B0 = vacuum_to
  real(ids_real),pointer  :: current_parallel_density_error_upper(:) => null()
  real(ids_real),pointer  :: current_parallel_density_error_lower(:) => null()
  integer(ids_int) :: current_parallel_density_error_index=ids_int_invalid

  real(ids_real),pointer  :: current_parallel_density_n_tor(:,:) => null()     ! /current_parallel_density_n_tor - Flux surface averaged wave driven parallel current density, per toroidal mode number
  real(ids_real),pointer  :: current_parallel_density_n_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: current_parallel_density_n_tor_error_lower(:,:) => null()
  integer(ids_int) :: current_parallel_density_n_tor_error_index=ids_int_invalid

  type (ids_waves_profiles_1d_e_field_n_tor),pointer :: e_field_n_tor(:) => null()  ! /e_field_n_tor(i) - Components of the electric field per toroidal mode number, averaged over the flux surface, where the
  real(ids_real),pointer  :: k_perpendicular(:,:) => null()     ! /k_perpendicular - Perpendicular wave vector,  averaged over the flux surface, where the averaged is weighted with the
  real(ids_real),pointer  :: k_perpendicular_error_upper(:,:) => null()
  real(ids_real),pointer  :: k_perpendicular_error_lower(:,:) => null()
  integer(ids_int) :: k_perpendicular_error_index=ids_int_invalid

  type (ids_waves_coherent_wave_profiles_1d_electrons) :: electrons  ! /electrons - Quantities related to the electrons
  type (ids_waves_coherent_wave_profiles_1d_ion),pointer :: ion(:) => null()  ! /ion(i) - Quantities related to the different ion species
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_waves_coherent_wave_full_wave_e_field  !    Components of the full wave electric field
  type (ids_generic_grid_scalar),pointer :: plus(:) => null()  ! /plus(i) - Left hand circularly polarised component of the perpendicular (to the static magnetic field) electri
  type (ids_generic_grid_scalar),pointer :: minus(:) => null()  ! /minus(i) - Right hand circularly polarised component of the perpendicular (to the static magnetic field) electr
  type (ids_generic_grid_scalar),pointer :: parallel(:) => null()  ! /parallel(i) - Parallel (to the static magnetic field) component of electric field, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: normal(:) => null()  ! /normal(i) - Magnitude of wave electric field normal to a flux surface, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: bi_normal(:) => null()  ! /bi_normal(i) - Magnitude of perpendicular (to the static magnetic field) wave electric field tangent to a flux surf
endtype

type ids_waves_coherent_wave_full_wave_b_field  !    Components of the full wave magnetic field
  type (ids_generic_grid_scalar),pointer :: parallel(:) => null()  ! /parallel(i) - Parallel (to the static magnetic field) component of the wave magnetic field, given on various grid
  type (ids_generic_grid_scalar),pointer :: normal(:) => null()  ! /normal(i) - Magnitude of wave magnetic field normal to a flux surface, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: bi_normal(:) => null()  ! /bi_normal(i) - Magnitude of perpendicular (to the static magnetic field) wave magnetic field tangent to a flux surf
endtype

type ids_waves_coherent_wave_full_wave  !    Full wave solution for a given time slice
  type (ids_generic_grid_dynamic) :: grid  ! /grid - Grid description
  type (ids_waves_coherent_wave_full_wave_e_field) :: e_field  ! /e_field - Components of the wave electric field
  type (ids_waves_coherent_wave_full_wave_b_field) :: b_field  ! /b_field - Components of the wave magnetic field
  type (ids_generic_grid_scalar),pointer :: k_perpendicular(:) => null()  ! /k_perpendicular(i) - Perpendicular wave vector, given on various grid subsets
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_waves_coherent_wave_profiles_2d_grid  !    2D grid for waves
  type (ids_identifier_dynamic_aos3) :: type  ! /type - Grid type: index=0: Rectangular grid in the (R,Z) coordinates; index=1: Rectangular grid in the (rad
  real(ids_real),pointer  :: r(:,:) => null()     ! /r - Major radius
  real(ids_real),pointer  :: r_error_upper(:,:) => null()
  real(ids_real),pointer  :: r_error_lower(:,:) => null()
  integer(ids_int) :: r_error_index=ids_int_invalid

  real(ids_real),pointer  :: z(:,:) => null()     ! /z - Height
  real(ids_real),pointer  :: z_error_upper(:,:) => null()
  real(ids_real),pointer  :: z_error_lower(:,:) => null()
  integer(ids_int) :: z_error_index=ids_int_invalid

  real(ids_real),pointer  :: theta_straight(:,:) => null()     ! /theta_straight - Straight field line poloidal angle
  real(ids_real),pointer  :: theta_straight_error_upper(:,:) => null()
  real(ids_real),pointer  :: theta_straight_error_lower(:,:) => null()
  integer(ids_int) :: theta_straight_error_index=ids_int_invalid

  real(ids_real),pointer  :: theta_geometric(:,:) => null()     ! /theta_geometric - Geometrical poloidal angle
  real(ids_real),pointer  :: theta_geometric_error_upper(:,:) => null()
  real(ids_real),pointer  :: theta_geometric_error_lower(:,:) => null()
  integer(ids_int) :: theta_geometric_error_index=ids_int_invalid

  real(ids_real),pointer  :: rho_tor_norm(:,:) => null()     ! /rho_tor_norm - Normalised toroidal flux coordinate. The normalizing value for rho_tor_norm, is the toroidal flux co
  real(ids_real),pointer  :: rho_tor_norm_error_upper(:,:) => null()
  real(ids_real),pointer  :: rho_tor_norm_error_lower(:,:) => null()
  integer(ids_int) :: rho_tor_norm_error_index=ids_int_invalid

  real(ids_real),pointer  :: rho_tor(:,:) => null()     ! /rho_tor - Toroidal flux coordinate. The toroidal field used in its definition is indicated under vacuum_toroid
  real(ids_real),pointer  :: rho_tor_error_upper(:,:) => null()
  real(ids_real),pointer  :: rho_tor_error_lower(:,:) => null()
  integer(ids_int) :: rho_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: psi(:,:) => null()     ! /psi - Poloidal magnetic flux
  real(ids_real),pointer  :: psi_error_upper(:,:) => null()
  real(ids_real),pointer  :: psi_error_lower(:,:) => null()
  integer(ids_int) :: psi_error_index=ids_int_invalid

  real(ids_real),pointer  :: volume(:,:) => null()     ! /volume - Volume enclosed inside the magnetic surface
  real(ids_real),pointer  :: volume_error_upper(:,:) => null()
  real(ids_real),pointer  :: volume_error_lower(:,:) => null()
  integer(ids_int) :: volume_error_index=ids_int_invalid

  real(ids_real),pointer  :: area(:,:) => null()     ! /area - Cross-sectional area of the flux surface
  real(ids_real),pointer  :: area_error_upper(:,:) => null()
  real(ids_real),pointer  :: area_error_lower(:,:) => null()
  integer(ids_int) :: area_error_index=ids_int_invalid

endtype

type ids_waves_coherent_wave_profiles_2d_ion_state  !    Global quantities related to a given ion species state
  real(ids_real)  :: z_min=ids_real_invalid       ! /z_min - Minimum Z of the charge state bundle (z_min = z_max = 0 for a neutral)
  real(ids_real)  :: z_min_error_upper=ids_real_invalid
  real(ids_real)  :: z_min_error_lower=ids_real_invalid
  integer(ids_int) :: z_min_error_index=ids_int_invalid

  real(ids_real)  :: z_max=ids_real_invalid       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(ids_real)  :: z_max_error_upper=ids_real_invalid
  real(ids_real)  :: z_max_error_lower=ids_real_invalid
  integer(ids_int) :: z_max_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  character(len=ids_string_length), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(ids_real)  :: vibrational_level=ids_real_invalid       ! /vibrational_level - Vibrational level (can be bundled)
  real(ids_real)  :: vibrational_level_error_upper=ids_real_invalid
  real(ids_real)  :: vibrational_level_error_lower=ids_real_invalid
  integer(ids_int) :: vibrational_level_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  real(ids_real),pointer  :: power_density_thermal(:,:) => null()     ! /power_density_thermal - Absorbed wave power density on the thermal species
  real(ids_real),pointer  :: power_density_thermal_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_density_thermal_error_lower(:,:) => null()
  integer(ids_int) :: power_density_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_density_thermal_n_tor(:,:,:) => null()     ! /power_density_thermal_n_tor - Absorbed wave power density on the thermal species, per toroidal mode number
  real(ids_real),pointer  :: power_density_thermal_n_tor_error_upper(:,:,:) => null()
  real(ids_real),pointer  :: power_density_thermal_n_tor_error_lower(:,:,:) => null()
  integer(ids_int) :: power_density_thermal_n_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_density_fast(:,:) => null()     ! /power_density_fast - Absorbed wave power density on the fast species
  real(ids_real),pointer  :: power_density_fast_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_density_fast_error_lower(:,:) => null()
  integer(ids_int) :: power_density_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_density_fast_n_tor(:,:,:) => null()     ! /power_density_fast_n_tor - Absorbed wave power density on the fast species, per toroidal mode number
  real(ids_real),pointer  :: power_density_fast_n_tor_error_upper(:,:,:) => null()
  real(ids_real),pointer  :: power_density_fast_n_tor_error_lower(:,:,:) => null()
  integer(ids_int) :: power_density_fast_n_tor_error_index=ids_int_invalid

endtype

type ids_waves_coherent_wave_profiles_2d_ion  !    Global quantities related to a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(ids_real)  :: z_ion=ids_real_invalid       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed).
  real(ids_real)  :: z_ion_error_upper=ids_real_invalid
  real(ids_real)  :: z_ion_error_lower=ids_real_invalid
  integer(ids_int) :: z_ion_error_index=ids_int_invalid

  character(len=ids_string_length), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H+, D+, T+, He+2, C+, D2, DT, CD4, ...)
  real(ids_real),pointer  :: power_density_thermal(:,:) => null()     ! /power_density_thermal - Absorbed wave power density on the thermal species
  real(ids_real),pointer  :: power_density_thermal_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_density_thermal_error_lower(:,:) => null()
  integer(ids_int) :: power_density_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_density_thermal_n_tor(:,:,:) => null()     ! /power_density_thermal_n_tor - Absorbed wave power density on the thermal species, per toroidal mode number
  real(ids_real),pointer  :: power_density_thermal_n_tor_error_upper(:,:,:) => null()
  real(ids_real),pointer  :: power_density_thermal_n_tor_error_lower(:,:,:) => null()
  integer(ids_int) :: power_density_thermal_n_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_density_fast(:,:) => null()     ! /power_density_fast - Absorbed wave power density on the fast species
  real(ids_real),pointer  :: power_density_fast_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_density_fast_error_lower(:,:) => null()
  integer(ids_int) :: power_density_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_density_fast_n_tor(:,:,:) => null()     ! /power_density_fast_n_tor - Absorbed wave power density on the fast species, per toroidal mode number
  real(ids_real),pointer  :: power_density_fast_n_tor_error_upper(:,:,:) => null()
  real(ids_real),pointer  :: power_density_fast_n_tor_error_lower(:,:,:) => null()
  integer(ids_int) :: power_density_fast_n_tor_error_index=ids_int_invalid

  integer(ids_int)  :: multiple_states_flag=ids_int_invalid       ! /multiple_states_flag - Multiple state calculation flag : 0-Only one state is considered; 1-Multiple states are considered a
  type (ids_waves_coherent_wave_profiles_2d_ion_state),pointer :: state(:) => null()  ! /state(i) - Collisional exchange with the various states of the ion species (ionisation, energy, excitation, ...
endtype

type ids_waves_coherent_wave_profiles_2d_electrons  !    Global quantities related to electrons
  real(ids_real),pointer  :: power_density_thermal(:,:) => null()     ! /power_density_thermal - Absorbed wave power density on the thermal species
  real(ids_real),pointer  :: power_density_thermal_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_density_thermal_error_lower(:,:) => null()
  integer(ids_int) :: power_density_thermal_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_density_thermal_n_tor(:,:,:) => null()     ! /power_density_thermal_n_tor - Absorbed wave power density on the thermal species, per toroidal mode number
  real(ids_real),pointer  :: power_density_thermal_n_tor_error_upper(:,:,:) => null()
  real(ids_real),pointer  :: power_density_thermal_n_tor_error_lower(:,:,:) => null()
  integer(ids_int) :: power_density_thermal_n_tor_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_density_fast(:,:) => null()     ! /power_density_fast - Absorbed wave power density on the fast species
  real(ids_real),pointer  :: power_density_fast_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_density_fast_error_lower(:,:) => null()
  integer(ids_int) :: power_density_fast_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_density_fast_n_tor(:,:,:) => null()     ! /power_density_fast_n_tor - Absorbed wave power density on the fast species, per toroidal mode number
  real(ids_real),pointer  :: power_density_fast_n_tor_error_upper(:,:,:) => null()
  real(ids_real),pointer  :: power_density_fast_n_tor_error_lower(:,:,:) => null()
  integer(ids_int) :: power_density_fast_n_tor_error_index=ids_int_invalid

endtype

type ids_waves_profiles_2d_e_field_n_tor  !    Components of the surface averaged electric field
  type (ids_waves_CPX_amp_phase_2D) :: plus  ! /plus - Left hand polarised electric field component
  type (ids_waves_CPX_amp_phase_2D) :: minus  ! /minus - Right hand polarised electric field component
  type (ids_waves_CPX_amp_phase_2D) :: parallel  ! /parallel - Parallel electric field component
endtype

type ids_waves_coherent_wave_profiles_2d  !    2D profiles (RF waves) for a given time slice
  type (ids_waves_coherent_wave_profiles_2d_grid) :: grid  ! /grid - 2D grid in a poloidal cross-section
  integer(ids_int),pointer  :: n_tor(:) => null()      ! /n_tor - Toroidal mode numbers
  real(ids_real),pointer  :: power_density(:,:) => null()     ! /power_density - Total absorbed wave power density (electrons + ion + fast populations)
  real(ids_real),pointer  :: power_density_error_upper(:,:) => null()
  real(ids_real),pointer  :: power_density_error_lower(:,:) => null()
  integer(ids_int) :: power_density_error_index=ids_int_invalid

  real(ids_real),pointer  :: power_density_n_tor(:,:,:) => null()     ! /power_density_n_tor - Absorbed wave power density per toroidal mode number
  real(ids_real),pointer  :: power_density_n_tor_error_upper(:,:,:) => null()
  real(ids_real),pointer  :: power_density_n_tor_error_lower(:,:,:) => null()
  integer(ids_int) :: power_density_n_tor_error_index=ids_int_invalid

  type (ids_waves_profiles_2d_e_field_n_tor),pointer :: e_field_n_tor(:) => null()  ! /e_field_n_tor(i) - Components of the electric field per toroidal mode number
  type (ids_waves_coherent_wave_profiles_2d_electrons) :: electrons  ! /electrons - Quantities related to the electrons
  type (ids_waves_coherent_wave_profiles_2d_ion),pointer :: ion(:) => null()  ! /ion(i) - Quantities related to the different ion species
  real(ids_real)  :: time=ids_real_invalid       ! /time - Time
  real(ids_real)  :: time_error_upper=ids_real_invalid
  real(ids_real)  :: time_error_lower=ids_real_invalid
  integer(ids_int) :: time_error_index=ids_int_invalid

endtype

type ids_waves_coherent_wave  !    Source terms for a given actuator
  type (ids_waves_coherent_wave_identifier) :: identifier  ! /identifier - Identifier of the coherent wave, in terms of the type and name of the antenna driving the wave and a
  type (ids_identifier) :: wave_solver_type  ! /wave_solver_type - Type of wave deposition solver used for this wave. Index = 1 for beam/ray tracing; index = 2 for ful
  type (ids_waves_coherent_wave_global_quantities),pointer :: global_quantities(:) => null()  ! /global_quantities(i) - Global quantities for various time slices
  type (ids_waves_coherent_wave_profiles_1d),pointer :: profiles_1d(:) => null()  ! /profiles_1d(i) - Source radial profiles (flux surface averaged quantities) for various time slices
  type (ids_waves_coherent_wave_profiles_2d),pointer :: profiles_2d(:) => null()  ! /profiles_2d(i) - 2D profiles in poloidal cross-section, for various time slices
  type (ids_waves_coherent_wave_beam_tracing),pointer :: beam_tracing(:) => null()  ! /beam_tracing(i) - Beam tracing calculations, for various time slices
  type (ids_waves_coherent_wave_full_wave),pointer :: full_wave(:) => null()  ! /full_wave(i) - Solution by a full wave code, given on a generic grid description, for various time slices
endtype

type ids_waves  !    RF wave propagation and deposition. Note that current estimates in this IDS are a priori not taking into account synergies between
  type (ids_ids_properties) :: ids_properties  ! /waves/ids_properties -
  type (ids_waves_coherent_wave),pointer :: coherent_wave(:) => null()  ! /waves/coherent_wave(i) - Wave description for each frequency
  type (ids_b_tor_vacuum_1) :: vacuum_toroidal_field  ! /waves/vacuum_toroidal_field - Characteristics of the vacuum toroidal field (used in rho_tor definition)
  type (ids_rz1d_dynamic_1) :: magnetic_axis  ! /waves/magnetic_axis - Magnetic axis position (used to define a poloidal angle for the 2D profiles)
  type (ids_code) :: code  ! /waves/code -
  real(ids_real), pointer  :: time(:) => null()  ! time
endtype

end module
