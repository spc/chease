! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*DECK U40
!*CALL PROCESS
SUBROUTINE RESETR(PA,KDIM,PVALUE)
  !        #################################
  !
  ! U.40   RESET REAL ARRAY TO SPECIFIED VALUE
  !
  !---*----*----*----*----*----*----*----*----*----*----*----*----*----*
  !
  !
  USE globals
  IMPLICIT NONE
  REAL(RKIND)        ::       PVALUE      ! <resetr.f90>
  REAL(RKIND)        ::       PA      ! <resetr.f90>
  INTEGER            ::       J1      ! <resetr.f90>
  INTEGER            ::       KDIM      ! <resetr.f90>
  DIMENSION &
       &   PA(KDIM)
  !
  DO J1=1,KDIM
     !
     PA(J1) = PVALUE
     !
  END DO
  !
  RETURN
END SUBROUTINE RESETR
