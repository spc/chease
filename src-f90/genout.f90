! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*DECK C3SB06
!*CALL PROCESS
SUBROUTINE GENOUT(VECTOR,ALPHA,NUNIT,NCRAY,NR,MSMAX,NSMAX,RM,RN)
  !     ================================================================
  !
  !                                        AUTHORS:
  !                                        G. VLAD,  ENEA FRASCATI
  !
  !.....GENERAL  INPUT ROUTINE:...........................................
  !.....                       VECTOR        :  VECTOR....................
  !.....                       ALPHA         :  ALPHANUMERIC STRING 6 CH..
  !.....                       NUNIT         :  INPUT  UNIT NUMBER........
  !.....                       NCRAY         :  RADIAL VECTOR DIMENSION...
  !.....                       NR,MSMAX,NSMAX:  CURRENT VECTOR DIM........
  !.....                       RM,RN         :  POLOIDAL AND TOROIDAL # ..
  !
  !...!!IF NSMAX .GT. 1, MSMAX MUST BE THE TRUE DIMENSION OF THE VECTORS..
  !.....TO PRESERVE THE CORRECT ORDERING OF THE VECTOR ELEMENTS...........
  !
  !
  !         USE globals, except_MSMAX => MSMAX, except_NSMAX => NSMAX, except_RN => RN, except_RM => RM
  USE prec_const
  USE globals, ONLY: NVERBOSE
  IMPLICIT NONE
  INTEGER          ::     NR
  INTEGER          ::     NCRAY
  INTEGER          ::     I
  REAL(RKIND)      ::     RN
  REAL(RKIND)      ::     RM
  INTEGER          ::     NUNIT
  INTEGER          ::     MSMAX
  INTEGER          ::     MS
  INTEGER          ::     NSMAX
  INTEGER          ::     NS
  COMPLEX(CKIND)    VECTOR
  DIMENSION &
       &   VECTOR(*)
  !
  DIMENSION &
       &   RM(*),   RN(*)
  !
  CHARACTER*6      ALPHA
  !
  !----*----*----*---*----*----*----*----*----*----*----*----*----*----*-
  !
  DO NS=1,NSMAX
    DO MS=1,MSMAX
      IF (NVERBOSE .GE. 1) WRITE(NUNIT,1010) ALPHA,RM(MS+(NS-1)*MSMAX),RN(NS)
      IF (NVERBOSE .GE. 1) WRITE(NUNIT,1020) &
        &         (VECTOR(I+(MS-1)*NCRAY+(NS-1)*MSMAX*NCRAY),I=1,NR)
    END DO
  END DO
  !
1010 FORMAT(//,1X,A,2F20.0)
1020 FORMAT(2D30.15)
  !
  RETURN
END SUBROUTINE GENOUT
