! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!
! Contains the type definition for utilities used to copy itm CPO to IDS and vice versa, to provide missing data in a generic way
!

module itm_ids_utilities    ! declare the set of types common to all sub-trees

  integer, parameter, private :: RP = kind(1.0d0)

  type struct_prof0d
     character(len=132) :: prof0d_name = "" ! name of each extra_prof0d to check
     real(RP) :: prof0d ! scrolls through various extra 0D arrays
  end type struct_prof0d
  type struct_prof1d
     character(len=132) :: name = "" ! name of each extra_prof1d to check
     real(RP), pointer :: values(:) ! scrolls through various extra 1D arrays
  end type struct_prof1d
  type struct_prof2d
     character(len=132) :: prof2d_name = "" ! name of each extra_prof2d to check
     real(RP), pointer :: prof2d(:) ! scrolls through various extra 2D arrays
  end type struct_prof2d
  type extra_data_gen ! to be able to give scalar and arrays not available in itm CPO
     ! construct it as arrays of structures to be able to check the size for each extra element to check if present
     type(struct_prof0d),pointer :: prof0d_list(:) => null()
     type(struct_prof1d),pointer :: prof1d_list(:) => null()
     type(struct_prof2d),pointer :: prof2d_list(:) => null()
  end type extra_data_gen

contains

  subroutine free_struct_prof1d(in)
    type(struct_prof1d), pointer :: in(:)
    integer :: j
    if (associated(in)) then
        do j=1,size(in)
            if (associated(in(j)%values)) deallocate(in(j)%values)
        end do
        deallocate(in)
    end if
  end subroutine free_struct_prof1d

  subroutine free_struct_prof2d(in)
    type(struct_prof2d), pointer :: in(:)
    integer :: j
    if (associated(in)) then
        do j=1,size(in)
            if (associated(in(j)%prof2d)) deallocate(in(j)%prof2d)
        end do
        deallocate(in)
    end if
  end subroutine free_struct_prof2d

  subroutine free_extra_data_gen(in)
    type(extra_data_gen), pointer :: in(:)
    integer :: j
    if (associated(in)) then
        do j=1,size(in)
            if (associated(in(j)%prof0d_list)) then
                deallocate(in(j)%prof0d_list)
            end if
            if (associated(in(j)%prof1d_list)) then
                call free_struct_prof1d(in(j)%prof1d_list)
            end if
            if (associated(in(j)%prof2d_list)) then
                call free_struct_prof2d(in(j)%prof2d_list)
            end if
        end do
        deallocate(in)
    end if
  end subroutine free_extra_data_gen

end module itm_ids_utilities
