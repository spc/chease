! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*DECK C2SP08
!*CALL PROCESS
SUBROUTINE BLTEST
  !        #################
  !
  !                                        AUTHORS:
  !                                        H. LUTJENS,  CRPP-EPFL
  !                                        A. BONDESON, CRPP-EPFL
  !**********************************************************************
  !                                                                     *
  !  C2SP08  LEAD EVALUATION OF SURFACE QUANTITIES, BALLOONING STABILITY*
  !          AND LOCAL INTERCHANGE CRITERIA AND GLOBAL EQUILIBRIUM      *
  !          QUANTITIES DURING BALLOONING OPTIMIZATION                  *
  !          LIMIT P' ACCORDING TO EQ. (41) IN PUBLICATION              *
  !                                                                     *
  !**********************************************************************
  !
  USE globals
  USE interpol
  IMPLICIT NONE
  !
  !----*----*----*---*----*----*----*----*----*----*----*----*----*----*-
  !
  INTEGER          ::     J4
  INTEGER          ::     J3
  INTEGER          ::     J2
  INTEGER          ::     JEND
  INTEGER          ::     J5
  INTEGER          ::     J1
  INTEGER          ::     ISRCHFGE
  INTEGER          ::     IP
  !
  IP = ISRCHFGE(NPPR+1,PSIISO,1,CPSICL(1))
  !
  IF (IP.LT.1)      IP = 1
  IF (IP.GT.NPPR+1) IP = NPPR+1
  !
  DO J1=IP,NPPR+1
     !
     CALL SURFACE(J1,SIGPSI(1,J1),TETPSI(1,J1),WGTPSI(1,J1), &
          &                PCSM(J1))
     CALL CHIPSI(NPPR+1,J1)
     !
  END DO
  !
  !%OS         IF (NPPR+1-IP+1 .LE. NPPSBAL) THEN
  !%OS           CALL BALOON(IP,NPPR+1,PCSM)
  !%OS         ELSE
  DO J5=IP,NPPR+1,NPPSBAL
    JEND = MIN(J5+NPPSBAL-1,NPPR+1)
    CALL BALOON(J5,JEND,PCSM)
  ENDDO
  !%OS         ENDIF
  !
  IF (IP .GT. 1) THEN
     !
     DO J2=1,IP-1
        !
        CPR(J2)   = CPR(IP)
        CPPR(J2)  = 0._RKIND
        NCBAL(J2) = - 1
        SMERCI(J2) = 0.25_RKIND
        !
        RJ1(J2) = RJ1(IP)
        RJ2(J2) = RJ2(IP)
        RJ3(J2) = RJ3(IP)
        RJ4(J2) = RJ4(IP)
        RJ5(J2) = RJ5(IP)
        RJ5P(J2)= RJ5P(IP)
        RJ6(J2) = RJ6(IP)
        CDQ(J2) = CDQ(IP)
        QPSI(J2)= QPSI(IP)
     END DO
     !
  ENDIF
  !
  CP0   = FCCCC0(CPR(IP),CPR(IP+1),CPR(IP+2),CPR(IP+3), &
       &                  PCSM(IP),PCSM(IP+1),PCSM(IP+2),PCSM(IP+3),RC0P)
  DPDP0 = FCCCC0(CPPR(IP),CPPR(IP+1),CPPR(IP+2),CPPR(IP+3), &
       &                  PCSM(IP),PCSM(IP+1),PCSM(IP+2),PCSM(IP+3),RC0P)
  !
  CALL GLOQUA(PCSM,NPPR+1,2)
  CALL RESETI(N2BAL,NPPR+1,0)
  !
  DO J3=IP,NPPR+1
     !
    IF ((PCSM(J3) .LE. 0.2_RKIND) .OR. (QPSI(J3) .LE. 1._RKIND) .OR. &
          &       (ABS(CPPR(J3)) .GT. PPRIME_BAL_MAX)) THEN
        !
        NCBAL(J3) = 1
        N2BAL(J3) = 1
        !
     ENDIF
     !
  END DO
  !
  DO J4=NPPR-2,NPPR+1
     !
     IF (ABS(CPPR(J4)) .GT. ABS(CPPR(J4-1))) THEN
        !
        CPPR(J4) = CPPR(J4-1)
        !
        PRINT*,'*****WARNING***** : CPPR(',J4,') = CPPR(',J4-1,')'
        PRINT*,' IMPOSED. NPPR = ',NPPR
        !
        NCBAL(J4) = 1
        N2BAL(J4) = 1
        !
     ENDIF
     !
  END DO
  !
  RETURN
END SUBROUTINE BLTEST
