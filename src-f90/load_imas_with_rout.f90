! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
subroutine load_imas(equil_in,kitmopt,kitmshot,kitmrun,kitmocc,citmtree,kflag)
  !
  use globals
  use ids_schemas                       ! module containing the equilibrium type definitions
  use ids_routines
  use ids_utilities
  IMPLICIT NONE
  type(ids_equilibrium)     :: equil_in
  character*120  :: citmtree
  integer        :: kitmopt, kitmshot, kitmrun, kitmocc
  integer        :: idx, i, i_interpol, ilast_time_ref_eff, kflag, status, ids_backend
  !
  character*11  :: signal_name ='equilibrium'
  character*16  :: str_equil = 'equilibrium'
  character*5   :: treename
  character(strmaxlen) :: uri
  !
  treename = trim(citmtree)
  kflag = 0
  !
  if (kitmocc .eq. 0) then
    str_equil = signal_name
  else
    write(str_equil,'(A11,A1,I0)') signal_name,'/',kitmocc
  end if
  if (nverbose .ge. 3) then
    print *,'signal_name= ',str_equil
    print *,'treename= ',treename
    print *,'before imas_open_env, kitmshot,kitmrun,kitmocc= ',kitmshot,kitmrun,kitmocc
    call flush(6)
  end if
  if (kitmshot .lt. 0) then
    kitmshot = abs(kitmshot)
  else
    if (nverbose .ge. 3) then
      write(*,*) 'before al_build_uri_..,  shot= ',kitmshot,' run= ',kitmrun,' kitmopt= ',kitmopt,' kitmocc= ',kitmocc, &
        ' tree_user(1)= ',trim(tree_user(1)),' tree_tokamak(1)= ',trim(tree_tokamak(1)),' tree_majorversion(1)= ',tree_majorversion(1)
      call flush(6)
    end if
    if ((tree_backend(1) .lt. 12) .or. (tree_backend(1) .gt. 13)) then
      if (nverbose .ge. 1) then
        write(0,*) ' WARNING: tree_backend(1) = ',tree_backend(1),' not well defined, should be  13 (hdf5) or 12 (mdsplus), set to default=hdf5'
      end if
      tree_backend(1) = 13
    end if
    ids_backend = tree_backend(1)
    ! create uri
    call al_build_uri_from_legacy_parameters(ids_backend, kitmshot, kitmrun, trim(tree_user(1)), trim(tree_tokamak(1)),tree_majorversion(1), '', uri, status)
    if (nverbose .ge. 3) then
      write(*,*) 'Prepared uri: ', trim(uri)
      write(*,*) 'status: ', status
    end if
    call imas_open(uri, OPEN_PULSE, idx , status)
    if (status .ne. 0) then
      write(0,*) ' ERROR: could not open pulse to add new IDS, idx= ', idx,' status= ',status
      kflag = -401
      return
    end if
    if (nverbose .ge. 3) then
      write(*,*) 'after imas_open, uri = ',trim(uri) ! trim important since strmaxlen very large
      write(*,*) 'after imas_open, idx= ',idx,' tree= ',treename,'trim(tree_user(1)),trim(tree_tokamak(1)), &
        & tree_majorversion(1)= ',trim(tree_user(1)),', ',trim(tree_tokamak(1)),', ',tree_majorversion(1),', status = ',status
      call flush(6)
    end if
  end if
  if (nverbose .ge. 3) then
    print *,'time_ref= ',time_ref
    call flush(6)
  end if
  ! find time_ref effective input (same check as in chease subroutine)
  ilast_time_ref_eff = 0
  if (abs(time_ref(1)+999) .gt. 1.e-4) then
    ! valid input if and only if 1st time_ref not default
    ilast_time_ref_eff = 1
    i = 2 ! do also i=2 since can be special with negative value from delta_t info
    if ((abs(time_ref(i)+999) .gt. 1.e-4) .and. (time_ref(i) .lt. 0._rkind) .and. (time_ref(i+1) .gt. time_ref(i-1))) then
      ! case with 3 relevant inputs including delta_t
      i=3
      ilast_time_ref_eff = 3
    else
      ! standard search for increasing inputs
      do while ((i .le. size(time_ref)) .and. ((abs(time_ref(i)+999) .gt. 1.e-4) .and. (time_ref(i) .gt. time_ref(i-1))))
        ilast_time_ref_eff = i
        i = i + 1
      end do
    end if
  end if
  !
  if (ilast_time_ref_eff .le. 1) then
    ! only one time_ref to consider
    if (nverbose .ge. 3) then
      print *,'time_ref(1)= ',time_ref(1)
      call flush(6)
    end if
    i_interpol = 1 ! closest time
    call ids_get_slice(idx,trim(str_equil),equil_in,time_ref(1),i_interpol)
  else
    call ids_get(idx,trim(str_equil),equil_in)
  end if
  call imas_close(idx)
  if (associated(equil_in%time)) then
    if (nverbose .ge. 3) then
      print *,'size(equil_in%time)= ',size(equil_in%time)
      print *,'time_ref(1)= ',time_ref(1)
      ! print *,'equil_in%time= ',equil_in%time(1:min(4,size(equil_in%time)))
    end if
  else
    if (nverbose .ge. 1) then
      write(0,*) 'in load_imas_with_rout: cannot read equil_in, time not associated'
      print *,'in load_imas_with_rout: cannot read equil_in, time not associated'
      call flush(6)
    end if
    kflag = -402
    return
  end if
  !
  return
end subroutine load_imas
