! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*DECK U37
!*CALL PROCESS
SUBROUTINE WITEXT(IVAR,CTEXT,TEXT,K)
  !        ------------------------------------
  !
  IMPLICIT NONE
  INTEGER          ::     IVAR
  INTEGER          ::     K
  CHARACTER*(*) TEXT,CTEXT
  !
  IF (K .EQ. 1) THEN
     WRITE(TEXT,9900) CTEXT,IVAR
  ELSE IF (K .EQ. 2) THEN
     WRITE(TEXT,9901) CTEXT,IVAR
  ELSE IF (K .EQ. 3) THEN
     WRITE(TEXT,9902) CTEXT,IVAR
  ENDIF
  !
  RETURN
9900 FORMAT(A,' = ',I1)
9901 FORMAT(A,' = ',I4)
9902 FORMAT(A,' = ',I2)
END SUBROUTINE WITEXT
