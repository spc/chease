! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*DECK C2SF04
!*CALL PROCESS
SUBROUTINE PRNORM(PC,KN)
  !        ########################
  !
  !                                        AUTHORS:
  !                                        H. LUTJENS,  CRPP-EPFL
  !                                        A. BONDESON, CRPP-EPFL
  !**********************************************************************
  !                                                                     *
  ! C2SF04 SCALE EQUILIBRIUM. (SEE EQ. (32) IN PUBLICATION)             *
  !                                                                     *
  !**********************************************************************
  !
  USE globals
  IMPLICIT NONE
  !
  !----*----*----*---*----*----*----*----*----*----*----*----*----*----*-
  !
  INTEGER          ::     KN
  REAL(RKIND)      ::     PC
  CALL DSCAL(N4NSNT,PC,CPSI,1)
  CALL DSCAL(N4NSNT,PC,CPSICL,1)
  CALL DSCAL(KN,PC,PSIISO,1)
  CALL DSCAL(KN,PC,TMF,1)
  CALL DSCAL(KN,PC,TTP,1)
  CALL DSCAL(KN,PC**2,CPR,1)
  CALL DSCAL(KN,PC,CPPR,1)
  !
  SPSIM = PC * SPSIM
  T0    = PC * T0
  !
  RETURN
END SUBROUTINE PRNORM
