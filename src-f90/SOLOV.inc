! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*COMDECK SOLOV
! ----------------------------------------------------------------------
! --     STATEMENT FUNCTION FOR SOLVEV PLASMA SECTION                 --
! --                         25.01.89            HL        CRPP       --
! --                                                                  --
! ----------------------------------------------------------------------
! --  SOLOVEV ANALYTICAL PSI = PSI(R,Z)                               --
! ----------------------------------------------------------------------
!
 REAL(rkind) :: SPS,DSPSDR,DSPSDZ,D2SPSR,DSPSRZ,D2SPSZ,pr,pz
!
         SPS(PR,PZ) = SPSI0 * ((PR * PZ / ELONG)**2 + .25 * &
     &                       (PR**2 - RC**2)**2) / (ASPCT*RC)**2
!
! ----------------------------------------------------------------------
! --  SOLOVEV ANALYTICAL DPSI / DR (PR,PZ)                              --
! ----------------------------------------------------------------------
!
         DSPSDR(PR,PZ) = SPSI0 * (2 * PR * (PZ / ELONG)**2 + &
     &                          PR * (PR**2 - RC**2)) / (ASPCT * RC)**2
!
! ----------------------------------------------------------------------
! --  SOLOVEV ANALYTICAL DPSI / DZ (PR,PZ)                              --
! ----------------------------------------------------------------------
!
         DSPSDZ(PR,PZ) = 2 * SPSI0 * PZ * (PR / (RC * ASPCT * ELONG))**2
!
! ----------------------------------------------------------------------
! --  SOLOVEV ANALYTICAL D2PSI / DR2 (PR,PZ)                            --
! ----------------------------------------------------------------------
!
         D2SPSR(PR,PZ) = SPSI0 * (2 * (PZ / ELONG)**2 + &
     &                          3. * PR**2 - RC**2) / (ASPCT * RC)**2
!
! ----------------------------------------------------------------------
! --  SOLOVEV ANALYTICAL D2PSI / DZ2 (PR,PZ)                            --
! ----------------------------------------------------------------------
!
         D2SPSZ(PR,PZ) = 2 * SPSI0 * (PR / (RC * ASPCT * ELONG))**2
!
! ----------------------------------------------------------------------
! --  SOLOVEV ANALYTICAL D2PSI / (DR * DZ) (PR,PZ)                      --
! ----------------------------------------------------------------------
!
         DSPSRZ(PR,PZ) = 4 * SPSI0 * PR * PZ / (RC * ASPCT * ELONG)**2
!
