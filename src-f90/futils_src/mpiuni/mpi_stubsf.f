c Copyright 2024 SPC-EPFL
c
c Licensed under the Apache License, Version 2.0 (the "License");
c you may not use this file except in compliance with the License.
c You may obtain a copy of the License at
c
c     http://www.apache.org/licenses/LICENSE-2.0
c
c Unless required by applicable law or agreed to in writing, software
c distributed under the License is distributed on an "AS IS" BASIS,
c WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
c See the License for the specific language governing permissions and
c limitations under the License.
c----------------------------------------------------------------------
c
c MPI fortran routine stubs
c
      subroutine mpi_abort
      print*,'mpi_abort: your are using a non-parallel HDF5!'
      end
c---------------------------------------------------------------------
      subroutine mpi_allreduce
      print*,'mpi_allreduce: your are using a non-parallel HDF5!'
      end
c---------------------------------------------------------------------
      subroutine mpi_cart_sub
      print*,'mpi_cart_sub: your are using a non-parallel HDF5!'
      end
c---------------------------------------------------------------------
      subroutine mpi_cartdim_get
      print*,'mpi_cartdim_get: your are using a non-parallel HDF5!'
      end
c---------------------------------------------------------------------
      subroutine mpi_comm_free
      print*,'mpi_comm_free: your are using a non-parallel HDF5!'
      end
c---------------------------------------------------------------------
      subroutine mpi_comm_rank(comm, rank, ierr)
      integer comm, rank, ierr
      rank = 0
c$$$      print*,'mpi_comm_rank: your are using a non-parallel HDF5!'
      end
c---------------------------------------------------------------------
      subroutine mpi_scan
      print*,'mpi_scan: your are using a non-parallel HDF5!'
      end
c---------------------------------------------------------------------
      subroutine mpi_topo_test
      print*,'mpi_scan: your are using a non-parallel HDF5!'
      end
c---------------------------------------------------------------------
