! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
PROGRAM main
!
!   Extract files from HDF5 datasets
!
  USE futils
  IMPLICIT NONE
  CHARACTER(len=256) :: file, path, name
  INTEGER :: n, fid, ierr, nargs
!===========================================================================
!
  nargs = command_argument_count()
  IF( nargs .GE. 2 ) THEN
     CALL get_command_argument(1, file, n, ierr)
     CALL get_command_argument(2, name, n, ierr)
     IF (nargs .EQ. 3) THEN
        CALL get_command_argument(3, path, n, ierr)
     END IF
  ELSE
     WRITE(*,'(a)') 'Usage: getfile <file> <name> [path]'
     STOP
  END IF
!
  CALL openf(file, fid, mode='r')
  IF( nargs .EQ. 2 )  THEN
     CALL getfile(fid, name)
  ELSE
     CALL getfile(fid, name, path)
  END IF
!
  CALL closef(fid)
END PROGRAM main
