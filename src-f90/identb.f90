! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*DECK C2SD06
!*CALL PROCESS
SUBROUTINE IDENTB(KVAR,PC)
  !        ##########################
  !
  !                                        AUTHORS:
  !                                        H. LUTJENS,  CRPP-EPFL
  !                                        A. BONDESON, CRPP-EPFL
  !**********************************************************************
  !                                                                     *
  ! C2SD06 PERFORM ROW OPERATIONS REQUIRED TO IMPOSE BOUNDARY           *
  !        CONDITIONS IN B                                              *
  !                                                                     *
  !**********************************************************************
  !
  USE globals
  IMPLICIT NONE
  !
  INTEGER          ::     J3
  REAL(RKIND)      ::     PC
  INTEGER          ::     JBROW
  INTEGER          ::     KVAR
  INTEGER          ::     J1
  INTEGER          ::     J2
  REAL(RKIND)      ::     PB
  DIMENSION &
       &   PB(3),   PC(N4NT,3)
  !
  !----*-----*-----*-----*-----*-----*-----*-----*-----*-----*-----*----
  !
  CALL VZERO(PB,3)
  !
  DO J2=1,4*NT
     !
     DO J1=1,KVAR
        !
        JBROW = J2
        !
        PB(J1) = PB(J1) + PC(J2,J1) * B(JBROW)
        !
     END DO
  END DO
  !
  DO J3=1,KVAR
     !
     JBROW = 4 * NT - KVAR + J3
     !
     B(JBROW) = PB(J3)
     !
  END DO
  !
  RETURN
END SUBROUTINE IDENTB
