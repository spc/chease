! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!
SUBROUTINE PROFILE(KN)
  !        ######################
  !
  !                                        AUTHORS:
  !                                        H. LUTJENS,  CRPP-EPFL
  !                                        A. BONDESON, CRPP-EPFL
  !**********************************************************************
  !                                                                     *
  ! C2SM10 TRACE CONSTANT FLUX SURFACES AND EVALUATE PROFILES           *
  !                                                                     *
  !**********************************************************************
  !
  !
  USE globals
  USE interpol
  use interpos_module
  IMPLICIT NONE
  !
  INTEGER          ::     KN
  !
  !----*----*----*---*----*----*----*----*----*----*----*----*----*----*-
  !     ZPSITEST IN BALLIT IS A GUESS FOR PSIISO TO CHECK IF
  !     PSIISO(1).LT.CPSICL(1). WRITE THESE VALUES SO THAT ONE CAN COMPARE
  !     ON THE OUTPUT WITH ZPSITEST WRITTEN BY BALLIT.
  !
  REAL(RKIND)      ::     ZWORK(KN+1)
  REAL(RKIND)      ::     ZCID0, ZCID2, ZCID3, TENS_DEF
  INTEGER          ::     IP, J1, J2
  ! FUNCTION
  INTEGER          ::     ISRCHFGE
  !
  IF (NVERBOSE .GE. 2) WRITE(6,'(/,"IN PROFILE: PSIISO(1)= ",1PE15.8,"  CPSICL(1)= ", &
    &     E15.8)') PSIISO(1), CPSICL(1)
  !
  IP = ISRCHFGE(KN,PSIISO,1,CPSICL(1))
  !
  IF (IP.LT.1)  IP = 1
  IF (IP.GT.KN) IP = KN
  !
  !     IF IP.NE.1 THEN SIGPSI,.. NOT DEFINED IN ISOFIND WHICH MAY CAUSE
  !     A PROBLEM IN SURFACE FOR EXAMPLE
  !
  IF (IP .NE. 1) THEN
    IF (NVERBOSE .GE. 2) WRITE(6,'(//,"********************  WARNING ***************", &
      &       /,5X,"IN PROFILE: IP=",I4,/,5X, &
      &       "IP SHOULD BE 1, MAY BE R0 TOO FAR FROM RMAG,", &
      &       " DO AN EXTRA ITERATION",/,5x,"USE FILE NOUT WITH NOPT=-2", &
      &       /,"********************  WARNING ***************"//)') IP
  ENDIF
  !
  CALL ISOFIND(IP,KN,SIGPSI,TETPSI,WGTPSI,SPSIM,RC0P)
  if (eqchease_out(index_out)%codeparam%output_flag .le. -10) then
    eqchease_out(index_out)%codeparam%output_diag(3) = 'in profile: error after isofind'
    return
  end if
  !
  DO J1=IP,KN
    !
    CALL CINT(J1,SIGPSI(1,J1),TETPSI(1,J1),WGTPSI(1,J1))
    !
  END DO
  !
  IF (IP .GT. 1) THEN
    if (IP+3 .GT. KN) then
      write(0,*) 'in profile: (IP+3 .GT. KN)'
      write(eqchease_out(index_out)%codeparam%output_diag(1),*) 'in profile: (IP+3 .GT. KN)'
      eqchease_out(index_out)%codeparam%output_flag = -221
      return
    end if
    !
    IF (NSTTP .LE. 2) THEN
      !
      ZCID0 = RMAG
      ZCID2 = RMAG**2
      !
    ELSE IF ((NSTTP.EQ.3) .OR. (NSTTP.EQ.4)) THEN
      !
      ZCID0 = RMAG**2
      ZCID2 = 0._RKIND
      !
    ELSE IF (NSTTP .EQ. 5) THEN
      !
      ZCID0 = RMAG
      ZCID2 = RMAG**2
      ZCID3 = 0._RKIND
      !
    ENDIF
    !
    DO J2=1,IP-1
      !
      CID0(J2) = FCCCC0(ZCID0,CID0(IP),CID0(IP+1),CID0(IP+2), &
        &                        SPSIM,PSIISO(IP),PSIISO(IP+1), &
        &                        PSIISO(IP+2),PSIISO(J2))
      CIDR(J2) = FCCCC0(CIDR(IP),CIDR(IP+1),CIDR(IP+2),CIDR(IP+3), &
        &                        PSIISO(IP),PSIISO(IP+1),PSIISO(IP+2), &
        &                        PSIISO(IP+3),PSIISO(J2))
      CIDQ(J2) = FCCCC0(CIDQ(IP),CIDQ(IP+1),CIDQ(IP+2),CIDQ(IP+3), &
        &                        PSIISO(IP),PSIISO(IP+1),PSIISO(IP+2), &
        &                        PSIISO(IP+3),PSIISO(J2))
      CID2(J2) = FCCCC0(ZCID2,CID2(IP),CID2(IP+1),CID2(IP+2), &
        &                        SPSIM,PSIISO(IP),PSIISO(IP+1), &
        &                        PSIISO(IP+2),PSIISO(J2))
      CID3(J2) = FCCCC0(ZCID3,CID3(IP),CID3(IP+1),CID3(IP+2), &
        &                        SPSIM,PSIISO(IP),PSIISO(IP+1), &
        &                        PSIISO(IP+2),PSIISO(J2))
      !
    END DO
    !
  ENDIF
  !
  !        COMPUTE RHO_TOR MESH IF NEEDED TO GET PROFILES
  !
  IF (NRHOMESH .GT. 0) THEN
    tens_def = -0.1_rkind
    IF (KN .NE. NISO) THEN
      IF (NVERBOSE .GE. 1) PRINT *,'KN= ',KN,' .NE. NISO= ',NISO, &
           & ' => CIDRTOR NOT RECALCULATED SINCE TMF NOT YET DEFINED ON FULL MESH'
      IF (NVERBOSE .GE. 1) PRINT *,' TMF(KN)= ',TMF(1:KN)
      !        CASE OF MAPPING ON WHATEVER PSIISO SURFACES AND KN CAN BE WHATEVER. BETTER TO KEEP LAST
      !        VALUES OF CIDRTOR WITH LAST TMF DURING ITERATIONS. MIGHT BE THAT ONE SHOULD RECALCULATED
      !        ONCE EQUILIBRIUM HAS CONVERGED.
      !        NOTE, ONE COULD CALCULATE HERE TMF ON PRESENT PSIISO MESH...
      !
    ELSE
      IF (NVERBOSE .GE. 3) PRINT *,'KN= ',KN,' , NISO= ',NISO,' CIDRTOR recalculated'
      IF (ABS(TMF(KN)) .LE. 1.E-08_RKIND) THEN
        IF (NVERBOSE .GE. 1) print *,' profile special case, KN= ',KN,'  NISO= ',NISO,' TMF(KN)= ',TMF(KN)
      endif
      IF (ABS(TMF(KN)) .GT. 1.E-08_RKIND) THEN
        ZWORK(2:KN+1) = CIDQ(1:KN) * TMF(1:KN)
      ELSE
        ! ASSUME TMF=1
        IF (NVERBOSE .GE. 1) PRINT *,' ASSUME T=1 TO COMPUTE CIDRTOR=RHOTOR ON CSIPRI MESH'
        ZWORK(2:KN+1) = CIDQ(1:KN)
      ENDIF
      ! CIDQ, TMF on SMISO/PSIISO, axis value not yet computed, assume same as 1st point
      ZWORK(1) = FCCCC0(ZWORK(2),ZWORK(3),ZWORK(4),ZWORK(5), &
        &                SMISOP1(2),SMISOP1(3),SMISOP1(4),SMISOP1(5),SMISOP1(1))
      !ZWORK(1) = ZWORK(2)
      !        COMPUTE RHOTOR ON CSIPRI MESH TO GET FULL RANGE (nt same as CSIPR)
      CALL INTERPOS(SMISOP1(1:kn+1)**2,ZWORK(1:kn+1),nin=KN+1,nout=KN,TENSION=TENS_DEF,XOUT=CSIPRI(1:KN)**2,YOUTINT=CIDRTOR(1:KN), &
        & nbc=(/0, 2/), ybc=(/ 0._rkind, ZWORK(KN+1) /) )
      CIDRTOR(1:KN) = sqrt(CIDRTOR(1:KN)/CIDRTOR(KN))
      CIDRTOR(1)=0._RKIND
      CIDRTOR(KN)=1._RKIND
!!$      rewind(42)
!!$      write(42,'(A)') '% CSIPRI cidq CIDRTOR PSIISO SMISO TMF CSIPR SMISOP1 ZWORK'
!!$      write(42,'(1p9e14.6)') (CSIPRI(J2),cidq(J2),CIDRTOR(J2),PSIISO(J2),SMISO(J2),TMF(J2),CSIPR(J2),SMISOP1(J2),ZWORK(J2),J2=1,KN)
!!$      write(42,'(A,1pe14.6)') 'SMISOP1(KN+1) = ',SMISOP1(KN+1),' ZWORK(KN+1) = ',ZWORK(KN+1),' Q0= ',Q0
      CALL SPLINE(KN,CSIPRI,CIDRTOR,D2CIDRTOR,ZWORK)
    ENDIF
    IF (KN .GE. NISO+5) THEN
      !        TMF NOT YET DEFINED (ON NEW MESH) COMPUTE RHOTOR ON CSIPRI MESH ASSUME TMF=1
      !        THIS CASE WAS WHILE DEVELOPING, NOT SURE RELEVANT ACTION TO DO, BETTER KEEP LAST VALUE CALCULATED
      !        IF THIS CASE STOPS HERE, CONTACT O. SAUTER FOR FURTHER DEVELOPMENT
      IF (NVERBOSE .GE. 0) write(0,*) ' contact O. Sauter, profile special case, KN= ',KN,'  NISO= ',NISO
      write(eqchease_out(index_out)%codeparam%output_diag(1),*) &
        & ' contact O. Sauter, profile special case, KN= ',KN,'  NISO= ',NISO
      eqchease_out(index_out)%codeparam%output_diag(2) = 'not sure how can get there, contact O. Sauter'
      eqchease_out(index_out)%codeparam%output_flag = -222
      return
    ENDIF
  ENDIF
  !
  !        COMPUTE PROFILES
  !
  CALL ISOFUN(KN)
  !
  RETURN
END SUBROUTINE PROFILE
