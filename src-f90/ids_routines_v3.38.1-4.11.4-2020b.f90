! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------

module ids_routines
use ids_schemas
use ual_low_level_wrap
use utilities_copy_struct
use utilities_deallocate_struct



use amns_data_put_struct
use amns_data_put_slice_struct
use amns_data_get_struct
use amns_data_get_slice_struct
use amns_data_delete
use amns_data_copy_struct
use amns_data_deallocate_struct

use barometry_put_struct
use barometry_put_slice_struct
use barometry_get_struct
use barometry_get_slice_struct
use barometry_delete
use barometry_copy_struct
use barometry_deallocate_struct

use bolometer_put_struct
use bolometer_put_slice_struct
use bolometer_get_struct
use bolometer_get_slice_struct
use bolometer_delete
use bolometer_copy_struct
use bolometer_deallocate_struct

use bremsstrahlung_visible_put_struct
use bremsstrahlung_visible_put_slice_struct
use bremsstrahlung_visible_get_struct
use bremsstrahlung_visible_get_slice_struct
use bremsstrahlung_visible_delete
use bremsstrahlung_visible_copy_struct
use bremsstrahlung_visible_deallocate_struct

use calorimetry_put_struct
use calorimetry_put_slice_struct
use calorimetry_get_struct
use calorimetry_get_slice_struct
use calorimetry_delete
use calorimetry_copy_struct
use calorimetry_deallocate_struct

use camera_ir_put_struct
use camera_ir_put_slice_struct
use camera_ir_get_struct
use camera_ir_get_slice_struct
use camera_ir_delete
use camera_ir_copy_struct
use camera_ir_deallocate_struct

use camera_visible_put_struct
use camera_visible_put_slice_struct
use camera_visible_get_struct
use camera_visible_get_slice_struct
use camera_visible_delete
use camera_visible_copy_struct
use camera_visible_deallocate_struct

use camera_x_rays_put_struct
use camera_x_rays_put_slice_struct
use camera_x_rays_get_struct
use camera_x_rays_get_slice_struct
use camera_x_rays_delete
use camera_x_rays_copy_struct
use camera_x_rays_deallocate_struct

use charge_exchange_put_struct
use charge_exchange_put_slice_struct
use charge_exchange_get_struct
use charge_exchange_get_slice_struct
use charge_exchange_delete
use charge_exchange_copy_struct
use charge_exchange_deallocate_struct

use coils_non_axisymmetric_put_struct
use coils_non_axisymmetric_put_slice_struct
use coils_non_axisymmetric_get_struct
use coils_non_axisymmetric_get_slice_struct
use coils_non_axisymmetric_delete
use coils_non_axisymmetric_copy_struct
use coils_non_axisymmetric_deallocate_struct

use controllers_put_struct
use controllers_put_slice_struct
use controllers_get_struct
use controllers_get_slice_struct
use controllers_delete
use controllers_copy_struct
use controllers_deallocate_struct

use core_instant_changes_put_struct
use core_instant_changes_put_slice_struct
use core_instant_changes_get_struct
use core_instant_changes_get_slice_struct
use core_instant_changes_delete
use core_instant_changes_copy_struct
use core_instant_changes_deallocate_struct

use core_profiles_put_struct
use core_profiles_put_slice_struct
use core_profiles_get_struct
use core_profiles_get_slice_struct
use core_profiles_delete
use core_profiles_copy_struct
use core_profiles_deallocate_struct

use core_sources_put_struct
use core_sources_put_slice_struct
use core_sources_get_struct
use core_sources_get_slice_struct
use core_sources_delete
use core_sources_copy_struct
use core_sources_deallocate_struct

use core_transport_put_struct
use core_transport_put_slice_struct
use core_transport_get_struct
use core_transport_get_slice_struct
use core_transport_delete
use core_transport_copy_struct
use core_transport_deallocate_struct

use cryostat_put_struct
use cryostat_put_slice_struct
use cryostat_get_struct
use cryostat_get_slice_struct
use cryostat_delete
use cryostat_copy_struct
use cryostat_deallocate_struct

use dataset_description_put_struct
use dataset_description_put_slice_struct
use dataset_description_get_struct
use dataset_description_get_slice_struct
use dataset_description_delete
use dataset_description_copy_struct
use dataset_description_deallocate_struct

use dataset_fair_put_struct
use dataset_fair_put_slice_struct
use dataset_fair_get_struct
use dataset_fair_get_slice_struct
use dataset_fair_delete
use dataset_fair_copy_struct
use dataset_fair_deallocate_struct

use disruption_put_struct
use disruption_put_slice_struct
use disruption_get_struct
use disruption_get_slice_struct
use disruption_delete
use disruption_copy_struct
use disruption_deallocate_struct

use distribution_sources_put_struct
use distribution_sources_put_slice_struct
use distribution_sources_get_struct
use distribution_sources_get_slice_struct
use distribution_sources_delete
use distribution_sources_copy_struct
use distribution_sources_deallocate_struct

use distributions_put_struct
use distributions_put_slice_struct
use distributions_get_struct
use distributions_get_slice_struct
use distributions_delete
use distributions_copy_struct
use distributions_deallocate_struct

use divertors_put_struct
use divertors_put_slice_struct
use divertors_get_struct
use divertors_get_slice_struct
use divertors_delete
use divertors_copy_struct
use divertors_deallocate_struct

use ec_launchers_put_struct
use ec_launchers_put_slice_struct
use ec_launchers_get_struct
use ec_launchers_get_slice_struct
use ec_launchers_delete
use ec_launchers_copy_struct
use ec_launchers_deallocate_struct

use ece_put_struct
use ece_put_slice_struct
use ece_get_struct
use ece_get_slice_struct
use ece_delete
use ece_copy_struct
use ece_deallocate_struct

use edge_profiles_put_struct
use edge_profiles_put_slice_struct
use edge_profiles_get_struct
use edge_profiles_get_slice_struct
use edge_profiles_delete
use edge_profiles_copy_struct
use edge_profiles_deallocate_struct

use edge_sources_put_struct
use edge_sources_put_slice_struct
use edge_sources_get_struct
use edge_sources_get_slice_struct
use edge_sources_delete
use edge_sources_copy_struct
use edge_sources_deallocate_struct

use edge_transport_put_struct
use edge_transport_put_slice_struct
use edge_transport_get_struct
use edge_transport_get_slice_struct
use edge_transport_delete
use edge_transport_copy_struct
use edge_transport_deallocate_struct

use em_coupling_put_struct
use em_coupling_put_slice_struct
use em_coupling_get_struct
use em_coupling_get_slice_struct
use em_coupling_delete
use em_coupling_copy_struct
use em_coupling_deallocate_struct

use equilibrium_put_struct
use equilibrium_put_slice_struct
use equilibrium_get_struct
use equilibrium_get_slice_struct
use equilibrium_delete
use equilibrium_copy_struct
use equilibrium_deallocate_struct

use gas_injection_put_struct
use gas_injection_put_slice_struct
use gas_injection_get_struct
use gas_injection_get_slice_struct
use gas_injection_delete
use gas_injection_copy_struct
use gas_injection_deallocate_struct

use gas_pumping_put_struct
use gas_pumping_put_slice_struct
use gas_pumping_get_struct
use gas_pumping_get_slice_struct
use gas_pumping_delete
use gas_pumping_copy_struct
use gas_pumping_deallocate_struct

use gyrokinetics_put_struct
use gyrokinetics_put_slice_struct
use gyrokinetics_get_struct
use gyrokinetics_get_slice_struct
use gyrokinetics_delete
use gyrokinetics_copy_struct
use gyrokinetics_deallocate_struct

use hard_x_rays_put_struct
use hard_x_rays_put_slice_struct
use hard_x_rays_get_struct
use hard_x_rays_get_slice_struct
use hard_x_rays_delete
use hard_x_rays_copy_struct
use hard_x_rays_deallocate_struct

use ic_antennas_put_struct
use ic_antennas_put_slice_struct
use ic_antennas_get_struct
use ic_antennas_get_slice_struct
use ic_antennas_delete
use ic_antennas_copy_struct
use ic_antennas_deallocate_struct

use interferometer_put_struct
use interferometer_put_slice_struct
use interferometer_get_struct
use interferometer_get_slice_struct
use interferometer_delete
use interferometer_copy_struct
use interferometer_deallocate_struct

use iron_core_put_struct
use iron_core_put_slice_struct
use iron_core_get_struct
use iron_core_get_slice_struct
use iron_core_delete
use iron_core_copy_struct
use iron_core_deallocate_struct

use langmuir_probes_put_struct
use langmuir_probes_put_slice_struct
use langmuir_probes_get_struct
use langmuir_probes_get_slice_struct
use langmuir_probes_delete
use langmuir_probes_copy_struct
use langmuir_probes_deallocate_struct

use lh_antennas_put_struct
use lh_antennas_put_slice_struct
use lh_antennas_get_struct
use lh_antennas_get_slice_struct
use lh_antennas_delete
use lh_antennas_copy_struct
use lh_antennas_deallocate_struct

use magnetics_put_struct
use magnetics_put_slice_struct
use magnetics_get_struct
use magnetics_get_slice_struct
use magnetics_delete
use magnetics_copy_struct
use magnetics_deallocate_struct

use mhd_put_struct
use mhd_put_slice_struct
use mhd_get_struct
use mhd_get_slice_struct
use mhd_delete
use mhd_copy_struct
use mhd_deallocate_struct

use mhd_linear_put_struct
use mhd_linear_put_slice_struct
use mhd_linear_get_struct
use mhd_linear_get_slice_struct
use mhd_linear_delete
use mhd_linear_copy_struct
use mhd_linear_deallocate_struct

use mse_put_struct
use mse_put_slice_struct
use mse_get_struct
use mse_get_slice_struct
use mse_delete
use mse_copy_struct
use mse_deallocate_struct

use nbi_put_struct
use nbi_put_slice_struct
use nbi_get_struct
use nbi_get_slice_struct
use nbi_delete
use nbi_copy_struct
use nbi_deallocate_struct

use neutron_diagnostic_put_struct
use neutron_diagnostic_put_slice_struct
use neutron_diagnostic_get_struct
use neutron_diagnostic_get_slice_struct
use neutron_diagnostic_delete
use neutron_diagnostic_copy_struct
use neutron_diagnostic_deallocate_struct

use ntms_put_struct
use ntms_put_slice_struct
use ntms_get_struct
use ntms_get_slice_struct
use ntms_delete
use ntms_copy_struct
use ntms_deallocate_struct

use pellets_put_struct
use pellets_put_slice_struct
use pellets_get_struct
use pellets_get_slice_struct
use pellets_delete
use pellets_copy_struct
use pellets_deallocate_struct

use pf_active_put_struct
use pf_active_put_slice_struct
use pf_active_get_struct
use pf_active_get_slice_struct
use pf_active_delete
use pf_active_copy_struct
use pf_active_deallocate_struct

use pf_passive_put_struct
use pf_passive_put_slice_struct
use pf_passive_get_struct
use pf_passive_get_slice_struct
use pf_passive_delete
use pf_passive_copy_struct
use pf_passive_deallocate_struct

use plasma_initiation_put_struct
use plasma_initiation_put_slice_struct
use plasma_initiation_get_struct
use plasma_initiation_get_slice_struct
use plasma_initiation_delete
use plasma_initiation_copy_struct
use plasma_initiation_deallocate_struct

use polarimeter_put_struct
use polarimeter_put_slice_struct
use polarimeter_get_struct
use polarimeter_get_slice_struct
use polarimeter_delete
use polarimeter_copy_struct
use polarimeter_deallocate_struct

use pulse_schedule_put_struct
use pulse_schedule_put_slice_struct
use pulse_schedule_get_struct
use pulse_schedule_get_slice_struct
use pulse_schedule_delete
use pulse_schedule_copy_struct
use pulse_schedule_deallocate_struct

use radiation_put_struct
use radiation_put_slice_struct
use radiation_get_struct
use radiation_get_slice_struct
use radiation_delete
use radiation_copy_struct
use radiation_deallocate_struct

use real_time_data_put_struct
use real_time_data_put_slice_struct
use real_time_data_get_struct
use real_time_data_get_slice_struct
use real_time_data_delete
use real_time_data_copy_struct
use real_time_data_deallocate_struct

use reflectometer_profile_put_struct
use reflectometer_profile_put_slice_struct
use reflectometer_profile_get_struct
use reflectometer_profile_get_slice_struct
use reflectometer_profile_delete
use reflectometer_profile_copy_struct
use reflectometer_profile_deallocate_struct

use refractometer_put_struct
use refractometer_put_slice_struct
use refractometer_get_struct
use refractometer_get_slice_struct
use refractometer_delete
use refractometer_copy_struct
use refractometer_deallocate_struct

use sawteeth_put_struct
use sawteeth_put_slice_struct
use sawteeth_get_struct
use sawteeth_get_slice_struct
use sawteeth_delete
use sawteeth_copy_struct
use sawteeth_deallocate_struct

use soft_x_rays_put_struct
use soft_x_rays_put_slice_struct
use soft_x_rays_get_struct
use soft_x_rays_get_slice_struct
use soft_x_rays_delete
use soft_x_rays_copy_struct
use soft_x_rays_deallocate_struct

use spectrometer_mass_put_struct
use spectrometer_mass_put_slice_struct
use spectrometer_mass_get_struct
use spectrometer_mass_get_slice_struct
use spectrometer_mass_delete
use spectrometer_mass_copy_struct
use spectrometer_mass_deallocate_struct

use spectrometer_uv_put_struct
use spectrometer_uv_put_slice_struct
use spectrometer_uv_get_struct
use spectrometer_uv_get_slice_struct
use spectrometer_uv_delete
use spectrometer_uv_copy_struct
use spectrometer_uv_deallocate_struct

use spectrometer_visible_put_struct
use spectrometer_visible_put_slice_struct
use spectrometer_visible_get_struct
use spectrometer_visible_get_slice_struct
use spectrometer_visible_delete
use spectrometer_visible_copy_struct
use spectrometer_visible_deallocate_struct

use spectrometer_x_ray_crystal_put_struct
use spectrometer_x_ray_crystal_put_slice_struct
use spectrometer_x_ray_crystal_get_struct
use spectrometer_x_ray_crystal_get_slice_struct
use spectrometer_x_ray_crystal_delete
use spectrometer_x_ray_crystal_copy_struct
use spectrometer_x_ray_crystal_deallocate_struct

use summary_put_struct
use summary_put_slice_struct
use summary_get_struct
use summary_get_slice_struct
use summary_delete
use summary_copy_struct
use summary_deallocate_struct

use temporary_put_struct
use temporary_put_slice_struct
use temporary_get_struct
use temporary_get_slice_struct
use temporary_delete
use temporary_copy_struct
use temporary_deallocate_struct

use thomson_scattering_put_struct
use thomson_scattering_put_slice_struct
use thomson_scattering_get_struct
use thomson_scattering_get_slice_struct
use thomson_scattering_delete
use thomson_scattering_copy_struct
use thomson_scattering_deallocate_struct

use tf_put_struct
use tf_put_slice_struct
use tf_get_struct
use tf_get_slice_struct
use tf_delete
use tf_copy_struct
use tf_deallocate_struct

use transport_solver_numerics_put_struct
use transport_solver_numerics_put_slice_struct
use transport_solver_numerics_get_struct
use transport_solver_numerics_get_slice_struct
use transport_solver_numerics_delete
use transport_solver_numerics_copy_struct
use transport_solver_numerics_deallocate_struct

use turbulence_put_struct
use turbulence_put_slice_struct
use turbulence_get_struct
use turbulence_get_slice_struct
use turbulence_delete
use turbulence_copy_struct
use turbulence_deallocate_struct

use wall_put_struct
use wall_put_slice_struct
use wall_get_struct
use wall_get_slice_struct
use wall_delete
use wall_copy_struct
use wall_deallocate_struct

use waves_put_struct
use waves_put_slice_struct
use waves_get_struct
use waves_get_slice_struct
use waves_delete
use waves_copy_struct
use waves_deallocate_struct

use workflow_put_struct
use workflow_put_slice_struct
use workflow_get_struct
use workflow_get_slice_struct
use workflow_delete
use workflow_copy_struct
use workflow_deallocate_struct


#if defined(__INTEL_COMPILER)
use ifport, only : getpid  ! required for getpid() in ifort
#endif
#if defined(NAGFOR)
use f90_unix, only : getpid  ! required for getpid() in nagfor
#endif

contains

subroutine ids_get_times(pulseCtx,path,time)
use ual_low_level_wrap
use ids_types
implicit none

integer(ids_int) :: pulsectx, opctx, status
character*(*) :: path
real(ids_real), pointer :: time(:)
integer(ids_int) :: dim1

call ual_begin_global_action(pulsectx, path, READ_OP, opctx, status)
if (status.ne.0) then
   STOP 'Error in ual_begin_global_action from ids_get_times'
end if

call get_vect1d_double(opctx, "time", "time", time, dim1, status)

call ual_end_action(opctx, status)

end subroutine

! Turn an IDS into a bunch of bytes
subroutine ids_serialize(ids_in, buffer, protocol)
  class(IDS_base) :: ids_in ! no intent(in) because ids_put also does not have that
  integer(ids_int), intent(in), optional :: protocol
  character(len=1), dimension(:), allocatable :: buffer

  character(len=:), allocatable :: fname
  integer(ids_int) :: my_protocol
  integer(ids_int) :: pulsectx
  integer(ids_int) :: status
  integer(ids_int) :: unit
  integer(ids_int) :: file_size

  my_protocol = DEFAULT_SERIALIZER_PROTOCOL
  if (present(protocol)) my_protocol = protocol

  if (my_protocol .eq. ASCII_SERIALIZER_PROTOCOL) then
    fname = generate_tmp_file()
    if (len_trim(fname) .eq. 0) then
      write(*,*) 'SERIALIZE: ERROR generating temporary file name'
      return
    end if

    ! Write to file
    call ual_begin_pulse_action(ASCII_BACKEND, 0, 0, 'serialize', 'serialize', '3', pulsectx)
    call ual_open_pulse(pulsectx, FORCE_CREATE_PULSE, '-fullpath ' // fname, status)
    if (status .ne. 0) then
      write(*,*) "SERIALIZE: ERROR opening ASCII backend - ual_open_pulse"
      buffer = ''
      return
    end if

    ! I think if we implement an object-oriented ids_in->put the select type here becomes unnecessary
    select type (ids_in)

    class is (ids_amns_data)
      call ids_put(pulsectx, 'amns_data', ids_in)

    class is (ids_barometry)
      call ids_put(pulsectx, 'barometry', ids_in)

    class is (ids_bolometer)
      call ids_put(pulsectx, 'bolometer', ids_in)

    class is (ids_bremsstrahlung_visible)
      call ids_put(pulsectx, 'bremsstrahlung_visible', ids_in)

    class is (ids_calorimetry)
      call ids_put(pulsectx, 'calorimetry', ids_in)

    class is (ids_camera_ir)
      call ids_put(pulsectx, 'camera_ir', ids_in)

    class is (ids_camera_visible)
      call ids_put(pulsectx, 'camera_visible', ids_in)

    class is (ids_camera_x_rays)
      call ids_put(pulsectx, 'camera_x_rays', ids_in)

    class is (ids_charge_exchange)
      call ids_put(pulsectx, 'charge_exchange', ids_in)

    class is (ids_coils_non_axisymmetric)
      call ids_put(pulsectx, 'coils_non_axisymmetric', ids_in)

    class is (ids_controllers)
      call ids_put(pulsectx, 'controllers', ids_in)

    class is (ids_core_instant_changes)
      call ids_put(pulsectx, 'core_instant_changes', ids_in)

    class is (ids_core_profiles)
      call ids_put(pulsectx, 'core_profiles', ids_in)

    class is (ids_core_sources)
      call ids_put(pulsectx, 'core_sources', ids_in)

    class is (ids_core_transport)
      call ids_put(pulsectx, 'core_transport', ids_in)

    class is (ids_cryostat)
      call ids_put(pulsectx, 'cryostat', ids_in)

    class is (ids_dataset_description)
      call ids_put(pulsectx, 'dataset_description', ids_in)

    class is (ids_dataset_fair)
      call ids_put(pulsectx, 'dataset_fair', ids_in)

    class is (ids_disruption)
      call ids_put(pulsectx, 'disruption', ids_in)

    class is (ids_distribution_sources)
      call ids_put(pulsectx, 'distribution_sources', ids_in)

    class is (ids_distributions)
      call ids_put(pulsectx, 'distributions', ids_in)

    class is (ids_divertors)
      call ids_put(pulsectx, 'divertors', ids_in)

    class is (ids_ec_launchers)
      call ids_put(pulsectx, 'ec_launchers', ids_in)

    class is (ids_ece)
      call ids_put(pulsectx, 'ece', ids_in)

    class is (ids_edge_profiles)
      call ids_put(pulsectx, 'edge_profiles', ids_in)

    class is (ids_edge_sources)
      call ids_put(pulsectx, 'edge_sources', ids_in)

    class is (ids_edge_transport)
      call ids_put(pulsectx, 'edge_transport', ids_in)

    class is (ids_em_coupling)
      call ids_put(pulsectx, 'em_coupling', ids_in)

    class is (ids_equilibrium)
      call ids_put(pulsectx, 'equilibrium', ids_in)

    class is (ids_gas_injection)
      call ids_put(pulsectx, 'gas_injection', ids_in)

    class is (ids_gas_pumping)
      call ids_put(pulsectx, 'gas_pumping', ids_in)

    class is (ids_gyrokinetics)
      call ids_put(pulsectx, 'gyrokinetics', ids_in)

    class is (ids_hard_x_rays)
      call ids_put(pulsectx, 'hard_x_rays', ids_in)

    class is (ids_ic_antennas)
      call ids_put(pulsectx, 'ic_antennas', ids_in)

    class is (ids_interferometer)
      call ids_put(pulsectx, 'interferometer', ids_in)

    class is (ids_iron_core)
      call ids_put(pulsectx, 'iron_core', ids_in)

    class is (ids_langmuir_probes)
      call ids_put(pulsectx, 'langmuir_probes', ids_in)

    class is (ids_lh_antennas)
      call ids_put(pulsectx, 'lh_antennas', ids_in)

    class is (ids_magnetics)
      call ids_put(pulsectx, 'magnetics', ids_in)

    class is (ids_mhd)
      call ids_put(pulsectx, 'mhd', ids_in)

    class is (ids_mhd_linear)
      call ids_put(pulsectx, 'mhd_linear', ids_in)

    class is (ids_mse)
      call ids_put(pulsectx, 'mse', ids_in)

    class is (ids_nbi)
      call ids_put(pulsectx, 'nbi', ids_in)

    class is (ids_neutron_diagnostic)
      call ids_put(pulsectx, 'neutron_diagnostic', ids_in)

    class is (ids_ntms)
      call ids_put(pulsectx, 'ntms', ids_in)

    class is (ids_pellets)
      call ids_put(pulsectx, 'pellets', ids_in)

    class is (ids_pf_active)
      call ids_put(pulsectx, 'pf_active', ids_in)

    class is (ids_pf_passive)
      call ids_put(pulsectx, 'pf_passive', ids_in)

    class is (ids_plasma_initiation)
      call ids_put(pulsectx, 'plasma_initiation', ids_in)

    class is (ids_polarimeter)
      call ids_put(pulsectx, 'polarimeter', ids_in)

    class is (ids_pulse_schedule)
      call ids_put(pulsectx, 'pulse_schedule', ids_in)

    class is (ids_radiation)
      call ids_put(pulsectx, 'radiation', ids_in)

    class is (ids_real_time_data)
      call ids_put(pulsectx, 'real_time_data', ids_in)

    class is (ids_reflectometer_profile)
      call ids_put(pulsectx, 'reflectometer_profile', ids_in)

    class is (ids_refractometer)
      call ids_put(pulsectx, 'refractometer', ids_in)

    class is (ids_sawteeth)
      call ids_put(pulsectx, 'sawteeth', ids_in)

    class is (ids_soft_x_rays)
      call ids_put(pulsectx, 'soft_x_rays', ids_in)

    class is (ids_spectrometer_mass)
      call ids_put(pulsectx, 'spectrometer_mass', ids_in)

    class is (ids_spectrometer_uv)
      call ids_put(pulsectx, 'spectrometer_uv', ids_in)

    class is (ids_spectrometer_visible)
      call ids_put(pulsectx, 'spectrometer_visible', ids_in)

    class is (ids_spectrometer_x_ray_crystal)
      call ids_put(pulsectx, 'spectrometer_x_ray_crystal', ids_in)

    class is (ids_summary)
      call ids_put(pulsectx, 'summary', ids_in)

    class is (ids_temporary)
      call ids_put(pulsectx, 'temporary', ids_in)

    class is (ids_thomson_scattering)
      call ids_put(pulsectx, 'thomson_scattering', ids_in)

    class is (ids_tf)
      call ids_put(pulsectx, 'tf', ids_in)

    class is (ids_transport_solver_numerics)
      call ids_put(pulsectx, 'transport_solver_numerics', ids_in)

    class is (ids_turbulence)
      call ids_put(pulsectx, 'turbulence', ids_in)

    class is (ids_wall)
      call ids_put(pulsectx, 'wall', ids_in)

    class is (ids_waves)
      call ids_put(pulsectx, 'waves', ids_in)

    class is (ids_workflow)
      call ids_put(pulsectx, 'workflow', ids_in)

    class default
      write(*,*) "SERIALIZE: ERROR selecting IDS type"
    end select


    call ual_close_pulse(pulsectx, FORCE_CREATE_PULSE, '', status)
    if (status .ne. 0) then
      write(*,*) "SERIALIZE: ERROR closing ASCII backend - ual_close_pulse"
      buffer = ''
      call ual_end_action(pulsectx, status)
      return
    end if
    call ual_end_action(pulsectx, status)
    if (status .ne. 0) then
      write(*,*) "SERIALIZE: ERROR closing ASCII backend - ual_end_action"
      buffer = ''
      return
    end if


    ! Read from file
    unit = get_file_unit()
    open(unit=unit, file=fname, action='read', status='old', form='unformatted', access='stream')
    inquire(unit=unit, size=file_size)
    allocate(character(1) :: buffer(file_size + 1))
    buffer(1) = char(ASCII_SERIALIZER_PROTOCOL)
    read(unit) buffer(2:)
    close(unit, status='delete')
  else
    write(*,*) 'SERIALIZE: ERROR, unrecognized serialization protocol'
  end if
end subroutine ids_serialize

! Turn a bunch of bytes into an IDS
subroutine ids_deserialize(buffer, ids_out)
  class(IDS_base) :: ids_out ! it is up to you to pass the correct buffer and ids type
  character(len=1), dimension(:), allocatable, intent(in) :: buffer

  integer(ids_int) :: protocol
  character(len=:), allocatable :: fname
  integer(ids_int) :: pulsectx
  integer(ids_int) :: status
  integer(ids_int) :: unit
  integer(ids_int) :: file_size

  protocol = ichar(buffer(1))

  if (protocol .eq. ASCII_SERIALIZER_PROTOCOL) then
    fname = generate_tmp_file()
    if (len_trim(fname) .eq. 0) then
      write(*,*) 'SERIALIZE: ERROR generating temporary file name'
      return
    end if

    ! Write to file
    unit = get_file_unit()
    open(unit=unit, file=fname, action='write', status='new', form='unformatted', access='stream')
    write(unit) buffer(2:)
    ! keep the file open, so we can delete it later in one go
    flush(unit)


    call ual_begin_pulse_action(ASCII_BACKEND, 0, 0, 'serialize', 'serialize', '3', pulsectx)
    call ual_open_pulse(pulsectx, FORCE_OPEN_PULSE, '-fullpath ' // fname, status)
    if (status .ne. 0) then
      write(*,*) "SERIALIZE: ERROR opening ASCII backend - ual_open_pulse"
      return
    end if

    ! I think if we implement an object-oriented ids_in->put the select type here becomes unnecessary
    select type (ids_out)

    class is (ids_amns_data)
      call ids_get(pulsectx, 'amns_data', ids_out)

    class is (ids_barometry)
      call ids_get(pulsectx, 'barometry', ids_out)

    class is (ids_bolometer)
      call ids_get(pulsectx, 'bolometer', ids_out)

    class is (ids_bremsstrahlung_visible)
      call ids_get(pulsectx, 'bremsstrahlung_visible', ids_out)

    class is (ids_calorimetry)
      call ids_get(pulsectx, 'calorimetry', ids_out)

    class is (ids_camera_ir)
      call ids_get(pulsectx, 'camera_ir', ids_out)

    class is (ids_camera_visible)
      call ids_get(pulsectx, 'camera_visible', ids_out)

    class is (ids_camera_x_rays)
      call ids_get(pulsectx, 'camera_x_rays', ids_out)

    class is (ids_charge_exchange)
      call ids_get(pulsectx, 'charge_exchange', ids_out)

    class is (ids_coils_non_axisymmetric)
      call ids_get(pulsectx, 'coils_non_axisymmetric', ids_out)

    class is (ids_controllers)
      call ids_get(pulsectx, 'controllers', ids_out)

    class is (ids_core_instant_changes)
      call ids_get(pulsectx, 'core_instant_changes', ids_out)

    class is (ids_core_profiles)
      call ids_get(pulsectx, 'core_profiles', ids_out)

    class is (ids_core_sources)
      call ids_get(pulsectx, 'core_sources', ids_out)

    class is (ids_core_transport)
      call ids_get(pulsectx, 'core_transport', ids_out)

    class is (ids_cryostat)
      call ids_get(pulsectx, 'cryostat', ids_out)

    class is (ids_dataset_description)
      call ids_get(pulsectx, 'dataset_description', ids_out)

    class is (ids_dataset_fair)
      call ids_get(pulsectx, 'dataset_fair', ids_out)

    class is (ids_disruption)
      call ids_get(pulsectx, 'disruption', ids_out)

    class is (ids_distribution_sources)
      call ids_get(pulsectx, 'distribution_sources', ids_out)

    class is (ids_distributions)
      call ids_get(pulsectx, 'distributions', ids_out)

    class is (ids_divertors)
      call ids_get(pulsectx, 'divertors', ids_out)

    class is (ids_ec_launchers)
      call ids_get(pulsectx, 'ec_launchers', ids_out)

    class is (ids_ece)
      call ids_get(pulsectx, 'ece', ids_out)

    class is (ids_edge_profiles)
      call ids_get(pulsectx, 'edge_profiles', ids_out)

    class is (ids_edge_sources)
      call ids_get(pulsectx, 'edge_sources', ids_out)

    class is (ids_edge_transport)
      call ids_get(pulsectx, 'edge_transport', ids_out)

    class is (ids_em_coupling)
      call ids_get(pulsectx, 'em_coupling', ids_out)

    class is (ids_equilibrium)
      call ids_get(pulsectx, 'equilibrium', ids_out)

    class is (ids_gas_injection)
      call ids_get(pulsectx, 'gas_injection', ids_out)

    class is (ids_gas_pumping)
      call ids_get(pulsectx, 'gas_pumping', ids_out)

    class is (ids_gyrokinetics)
      call ids_get(pulsectx, 'gyrokinetics', ids_out)

    class is (ids_hard_x_rays)
      call ids_get(pulsectx, 'hard_x_rays', ids_out)

    class is (ids_ic_antennas)
      call ids_get(pulsectx, 'ic_antennas', ids_out)

    class is (ids_interferometer)
      call ids_get(pulsectx, 'interferometer', ids_out)

    class is (ids_iron_core)
      call ids_get(pulsectx, 'iron_core', ids_out)

    class is (ids_langmuir_probes)
      call ids_get(pulsectx, 'langmuir_probes', ids_out)

    class is (ids_lh_antennas)
      call ids_get(pulsectx, 'lh_antennas', ids_out)

    class is (ids_magnetics)
      call ids_get(pulsectx, 'magnetics', ids_out)

    class is (ids_mhd)
      call ids_get(pulsectx, 'mhd', ids_out)

    class is (ids_mhd_linear)
      call ids_get(pulsectx, 'mhd_linear', ids_out)

    class is (ids_mse)
      call ids_get(pulsectx, 'mse', ids_out)

    class is (ids_nbi)
      call ids_get(pulsectx, 'nbi', ids_out)

    class is (ids_neutron_diagnostic)
      call ids_get(pulsectx, 'neutron_diagnostic', ids_out)

    class is (ids_ntms)
      call ids_get(pulsectx, 'ntms', ids_out)

    class is (ids_pellets)
      call ids_get(pulsectx, 'pellets', ids_out)

    class is (ids_pf_active)
      call ids_get(pulsectx, 'pf_active', ids_out)

    class is (ids_pf_passive)
      call ids_get(pulsectx, 'pf_passive', ids_out)

    class is (ids_plasma_initiation)
      call ids_get(pulsectx, 'plasma_initiation', ids_out)

    class is (ids_polarimeter)
      call ids_get(pulsectx, 'polarimeter', ids_out)

    class is (ids_pulse_schedule)
      call ids_get(pulsectx, 'pulse_schedule', ids_out)

    class is (ids_radiation)
      call ids_get(pulsectx, 'radiation', ids_out)

    class is (ids_real_time_data)
      call ids_get(pulsectx, 'real_time_data', ids_out)

    class is (ids_reflectometer_profile)
      call ids_get(pulsectx, 'reflectometer_profile', ids_out)

    class is (ids_refractometer)
      call ids_get(pulsectx, 'refractometer', ids_out)

    class is (ids_sawteeth)
      call ids_get(pulsectx, 'sawteeth', ids_out)

    class is (ids_soft_x_rays)
      call ids_get(pulsectx, 'soft_x_rays', ids_out)

    class is (ids_spectrometer_mass)
      call ids_get(pulsectx, 'spectrometer_mass', ids_out)

    class is (ids_spectrometer_uv)
      call ids_get(pulsectx, 'spectrometer_uv', ids_out)

    class is (ids_spectrometer_visible)
      call ids_get(pulsectx, 'spectrometer_visible', ids_out)

    class is (ids_spectrometer_x_ray_crystal)
      call ids_get(pulsectx, 'spectrometer_x_ray_crystal', ids_out)

    class is (ids_summary)
      call ids_get(pulsectx, 'summary', ids_out)

    class is (ids_temporary)
      call ids_get(pulsectx, 'temporary', ids_out)

    class is (ids_thomson_scattering)
      call ids_get(pulsectx, 'thomson_scattering', ids_out)

    class is (ids_tf)
      call ids_get(pulsectx, 'tf', ids_out)

    class is (ids_transport_solver_numerics)
      call ids_get(pulsectx, 'transport_solver_numerics', ids_out)

    class is (ids_turbulence)
      call ids_get(pulsectx, 'turbulence', ids_out)

    class is (ids_wall)
      call ids_get(pulsectx, 'wall', ids_out)

    class is (ids_waves)
      call ids_get(pulsectx, 'waves', ids_out)

    class is (ids_workflow)
      call ids_get(pulsectx, 'workflow', ids_out)

    class default
      write(*,*) "SERIALIZE: ERROR selecting IDS type"
    end select


    call ual_close_pulse(pulsectx, FORCE_OPEN_PULSE, '', status)
    if (status .ne. 0) then
      write(*,*) "SERIALIZE: ERROR closing ASCII backend - ual_close_pulse"
      call ual_end_action(pulsectx, status)
      return
    end if
    call ual_end_action(pulsectx, status)
    if (status .ne. 0) then
      write(*,*) "SERIALIZE: ERROR closing ASCII backend - ual_end_action"
      return
    end if

    ! delete file
    close(unit, status='delete')
  else
    write(*,*) 'SERIALIZE: ERROR, unrecognized serialization protocol'
  end if
end subroutine ids_deserialize

function generate_tmp_file() result(fname)
  character(len=:), allocatable :: fname
  ! Follow same approach as the Python standard library in generating a random temporary file
  character(len=*), parameter :: fs_safe_characters = 'abcdefghijklmnopqrstuvwxyz0123456789_'
  integer, parameter :: n = 8 ! number of random characters in the file
  integer, parameter :: MAX_TMP_FILES = 1000

  ! On Windows and Mac OSX, use the current working directory as temporary directory (since /dev/shm does not exist).
  ! On any recent Linux (2.6 or later according to Wikipedia [1]) the /dev/shm folder exists for shared memory.
  ! Since glibc assumes this to exist anyway [2], we will as well.
  ! [1] https://en.wikipedia.org/wiki/Shared_memory
  ! [2] https://www.kernel.org/doc/Documentation/filesystems/tmpfs.txt
#if defined(_Linux)
#  define SERIALIZE_TEMPORARY_DIRECTORY '/dev/shm/'
#else
#  define SERIALIZE_TEMPORARY_DIRECTORY ''
#endif

  real, dimension(n) :: rd
  integer :: string_base_length
  integer :: i, j, k
  integer :: unit ! Unit number to open file with
  integer :: iostat
  integer :: ipid
  character(10) :: cpid

  ipid = getpid()
  ! Convert to characters, using I0 to left-justify without leading 0s
  write(cpid, '(I0)') ipid

  ! Setup the base of the filename
  string_base_length = len(SERIALIZE_TEMPORARY_DIRECTORY) + len('al_serialize_') + len_trim(cpid) + 1
  fname = SERIALIZE_TEMPORARY_DIRECTORY // 'al_serialize_' // trim(cpid) // "_"  // repeat(' ', n) ! implicitly allocates to the right size

  ! get a free unit number
  unit = get_file_unit()

  do i=1,MAX_TMP_FILES
    call random_number(rd)
    do j=1,n
      k = ceiling(rd(j)*len(fs_safe_characters))
      fname(string_base_length + j:string_base_length + j) = fs_safe_characters(k:k)
    end do

    open(unit=unit, action='write', file=fname, status='new', iostat=iostat)
    if (iostat .gt. 0) cycle

    ! if we get here the file was opened successfully. Delete it, and return the filename found
    close(unit=unit, status='delete')
    return ! implies fname
  end do
  fname = ''
end function generate_tmp_file

function get_file_unit() result(unit)
  ! Get a free file unit description number without using Fortran 2008 newunit feature.
  integer :: unit, iostat
  logical :: opened

  do unit = 97,1,-1
    inquire (unit=unit, opened=opened, iostat=iostat)
    if (iostat .ne. 0) cycle
    if (.not. opened) exit
  end do
end function get_file_unit

end module
