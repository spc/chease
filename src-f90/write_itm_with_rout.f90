! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
subroutine write_itm(equil_out,kitmopt,kitmshot,kitmrun,kitmocc,citmtree)
  !
  use globals
  use euITM_schemas                       ! module containing the equilibrium type definitions
  use euITM_routines
  IMPLICIT NONE
  type(type_equilibrium),pointer      :: equil_out(:)
  character*120  :: citmtree
  integer        :: kitmopt, kitmshot, kitmrun, kitmocc
  integer        :: idx, irefrun, i
  !
  character(len=11) :: signal_name ="equilibrium"
  character(len=16) :: str_equil = 'equilibrium'
  character(len=5)  :: citmtree2
  !
  if (kitmshot .eq. 22) then
    print *,' Do not write since shot number to write: ',kitmshot
    return
  end if
  if (kitmocc .eq. 0) then
    str_equil = signal_name
  else
    write(str_equil,'(A11,A1,I0)') signal_name,'/',kitmocc
  end if
  print *,'signal_name= ',str_equil
  !
  !equil_out%datainfo%comment='CHEASE: Reconstruction of \TOP.equilibria.eq.NNN001 in euitm_imp1 tree'
  !equil_out%coord_sys%grid_type='CHEASE: Straight field line coordinates, Power of 2'
  !equil_out%coord_sys%type%dat='CHEASE: Straight field line coordinates'
  !equil_out%coord_sys%type%dat='CHEASE: Equal arc coordinates'
  !equil_out%coord_sys%type%dat='CHEASE: Theta-Psi coordinates'

  !  signal_name ='\\TOP.equilibria.eq.NNN006.'

  citmtree2 = trim(citmtree)
  ! At this stage, seems to need to do create_pulse in any case, so changed else part, but may be different in future
  if (kitmshot .lt. 0) then
    irefrun = 0 ! should always have refshot=shot and refrun=0?
    kitmshot = abs(kitmshot)
    call euitm_create(citmtree2,kitmshot,kitmrun,kitmshot,irefrun,idx)
    ! print *,'salut32, idx from create= ',idx,' tree= ',citmtree2
  else
    !     call euitm_open(citmtree2,kitmshot,kitmrun,idx)
    !     print *,'salut32, idx from open= ',idx
    irefrun = 0 ! should always have refshot=shot and refrun=0?
    call euitm_create(citmtree2,kitmshot,kitmrun,kitmshot,irefrun,idx)
    ! print *,'salut32, idx from create= ',idx,' tree= ',citmtree2
  end if
  ! print *,' equil_out(1)%global_param%volume= ',equil_out(1)%global_param%volume
  ! print *,' equil_out(1)%eqgeometry%geom_axis%r= ',equil_out(1)%eqgeometry%geom_axis%r
  ! print *,' equil_out(1)%eqgeometry%geom_axis%z= ',equil_out(1)%eqgeometry%geom_axis%z
  print *,' equil_out(1)%eqgeometry%boundary(1)%r(1:2)= ',equil_out(1)%eqgeometry%boundary(1)%r(1:2)
  !

  call euitm_put(idx,trim(str_equil),equil_out)

  call euitm_close(idx,"euitm",kitmshot,kitmrun)

  return
end subroutine write_itm
