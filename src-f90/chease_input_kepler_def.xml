<!-- Copyright 2024 SPC-EPFL -->
<!--  -->
<!-- Licensed under the Apache License, Version 2.0 (the "License"); -->
<!-- you may not use this file except in compliance with the License. -->
<!-- You may obtain a copy of the License at -->
<!--  -->
<!--     http://www.apache.org/licenses/LICENSE-2.0 -->
<!--  -->
<!-- Unless required by applicable law or agreed to in writing, software -->
<!-- distributed under the License is distributed on an "AS IS" BASIS, -->
<!-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. -->
<!-- See the License for the specific language governing permissions and -->
<!-- limitations under the License. -->
<!-- ********************************************************************** -->
<parameters>
    <aplace> .00 .70 1.0</aplace>
    <aspct> 0.28  </aspct>
    <awidth> .05 .07 .05 </awidth>
    <comments>
     *** chease_input_kepler_def.xml
     ***
    </comments>
    <cplace> .95 .99 1.0 </cplace>
    <cpress> 1.0  </cpress>
    <csspec> 0.0  </csspec>
    <cwidth> .10 .02 .05 </cwidth>
    <dplace> -1.80 -1.80 4.0 </dplace>
    <dwidth> .18 .08 .05 </dwidth>
    <elong> 2.05  </elong>
    <eplace> -1.70 -1.70  1.70  1.70 </eplace>
    <epslon> 1.0e-8 </epslon>
    <ewidth> .18 .08 .18 .08 </ewidth>

    <cocos_in> 13 </cocos_in> <!-- 13 for original ITM,11 for ITER, 2 for eqdsk/chease standard -->
    <cocos_out> 13 </cocos_out> <!-- 13 for original ITM, 11 for ITER, 2 for eqdsk/chease standard -->
    <signipxp> -9 </signipxp> <!-- to let the code follow the cocos_in -> _out transformation for sign of Ip-->
    <signb0xp> -9 </signb0xp> <!-- to let the code follow the cocos_in -> _out transformation for sign of Ip-->
    <nbpsout> 300 </nbpsout>
    <msmax> 1 </msmax>
    <nchi> 80  </nchi>
    <ncscal> 4  </ncscal>
    <ndiagop> 1  </ndiagop>
    <negp> -1 </negp> <!-- straight field line, NER=2, NEGP=0, since Jacobian=C(psi) R^NER grad(psi)^NEGP -->
    <neqdsk> 0 </neqdsk>
    <neqdxtpo> 1 </neqdxtpo> <!-- set to -1 to shift R,Z psi to zmag, 1 is for linear extrapolation -->
    <ner> 1 </ner> <!-- straight field line, NER=2, NEGP=0, since Jacobian=C(psi) R^NER grad(psi)^NEGP -->
    <nfunc> 4  </nfunc>
    <nideal> 6  </nideal>
    <ninmap> 40 </ninmap>
    <ninsca> 40 </ninsca>
    <niso> 80  </niso>
    <nitmopt> 22 </nitmopt> <!-- 22 within kepler, 1 to read from ITM database, 11 to read and write, 10 to write, 0 to be independent from ITM database -->
    <nitmrun> 11 12 </nitmrun> <!-- 1st numbers for input, 2nd for output -->
    <nitmshot> 191 191 </nitmshot> <!-- 1st numbers for input, 2nd for output -->
    <nmesha> 0  </nmesha>
    <nmeshc> 1  </nmeshc>
    <nmeshd> 1  </nmeshd>
    <nmeshe> 0  </nmeshe>
    <nmeshpol> 1  </nmeshpol>
    <nopt> 0  </nopt>
    <npoida> 1  </npoida>
    <npoidc> 2  </npoidc>
    <npoidd> 2  </npoidd>
    <npoide> 4  </npoide>
    <npoidq> 10  </npoidq>
    <npp> 1  </npp>
    <nppfun> 4  </nppfun> <!-- nppfun=4 for pprime and 8 for p as input (needs nsttp.ge.2 for nppfun=8) -->
    <npropt> 2  </npropt>
    <npsi> 80  </npsi>
    <nrbox> 101 </nrbox>
    <ns> 20  </ns>
    <nsttp> 4  </nsttp>  <!-- set nsttp=1 for ffprime, 2 for Istar=jphi, 3 for Ipar and 4 for jpar=<j.B>/B0 -->
    <nsurf> 6  </nsurf>
    <nt> 20  </nt>
    <nturn> 20  </nturn>
    <nzbox> 101 </nzbox>

    <qplace> 1.00 1.00 2.00 2.00 3.00 3.00 4.00 4.00 4.41 4.41 </qplace>
    <qspec> 0.9  </qspec>
    <qwidth> 0.13 0.04 0.09 0.04 0.07 0.02 0.04 0.01 0.01 0.001 </qwidth>
    <relax> 0.  </relax>
    <solpda> 0.10 </solpda>
    <solpdc> 0.70 </solpdc>
    <solpdd> 0.60 </solpdd>
    <solpde> 0.50 </solpde>
    <solpdpol> 0.25 </solpdpol>
    <triang> 0.5  </triang>
    <tensbnd> -0.3 </tensbnd>
    <tensprof> -0.3 </tensprof>

    <nprof2d> 1 </nprof2d> <!-- 1: compute simple profiles_2d(2), 2: complete, 11: as 1 and _2d(3) on rhotor_norm mesh, 12: complete _2d(3) -->

</parameters>
