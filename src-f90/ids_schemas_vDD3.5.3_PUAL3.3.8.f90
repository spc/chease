! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
! IDS FORTRAN 90 type definitions
! Contains the type definition of all IDSs


module ids_utilities    ! declare the set of types common to all sub-trees

integer, parameter, public :: DP = kind(1.0d0)

type ids_identifier  !    Standard type for identifiers (constant). The three fields: name, index and description are all representations of the same inform
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Short string identifier
  integer  :: index=-999999999       ! /index - Integer identifier (enumeration index within a list)
  character(len=132), dimension(:), pointer ::description => null()       ! /description - Verbose description
endtype

type ids_identifier_static  !    Standard type for identifiers (static). The three fields: name, index and description are all representations of the same informat
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Short string identifier
  integer  :: index=-999999999       ! /index - Integer identifier (enumeration index within a list)
  character(len=132), dimension(:), pointer ::description => null()       ! /description - Verbose description
endtype

type ids_identifier_static_1d  !    Standard type for identifiers (static, 1D). The three fields: name, index and description are all representations of the same info
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Short string identifier
  integer  :: index=-999999999       ! /index - Integer identifier (enumeration index within a list)
  character(len=132), dimension(:), pointer ::description => null()       ! /description - Verbose description
endtype

type ids_identifier_dynamic_aos3  !    Standard type for identifiers (dynamic within type 3 array of structure (index on time)). The three fields: name, index and descri
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Short string identifier
  integer  :: index=-999999999       ! /index - Integer identifier (enumeration index within a list)
  character(len=132), dimension(:), pointer ::description => null()       ! /description - Verbose description
endtype

type ids_plasma_composition_ion_state_constant  !    Definition of an ion state (when describing the plasma composition) (constant)
  real(DP)  :: z_min=-9.0D40       ! /z_min - Minimum Z of the charge state bundle (z_min = z_max = 0 for a neutral)
  real(DP)  :: z_min_error_upper=-9.0D40
  real(DP)  :: z_min_error_lower=-9.0D40
  integer :: z_min_error_index=-999999999

  real(DP)  :: z_max=-9.0D40       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(DP)  :: z_max_error_upper=-9.0D40
  real(DP)  :: z_max_error_lower=-9.0D40
  integer :: z_max_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying ion state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
endtype

type ids_plasma_composition_neutral_state_constant  !    Definition of a neutral state (when describing the plasma composition) (constant)
  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying neutral state
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  type (ids_identifier) :: neutral_type  ! /neutral_type - Neutral type, in terms of energy. ID =1: cold; 2: thermal; 3: fast; 4: NBI
endtype

type ids_plasma_composition_ion_state  !    Definition of an ion state (when describing the plasma composition) (within a type 3 AoS)
  real(DP)  :: z_min=-9.0D40       ! /z_min - Minimum Z of the charge state bundle (z_min = z_max = 0 for a neutral)
  real(DP)  :: z_min_error_upper=-9.0D40
  real(DP)  :: z_min_error_lower=-9.0D40
  integer :: z_min_error_index=-999999999

  real(DP)  :: z_max=-9.0D40       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(DP)  :: z_max_error_upper=-9.0D40
  real(DP)  :: z_max_error_lower=-9.0D40
  integer :: z_max_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying ion state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
endtype

type ids_plasma_composition_ions_constant  !    Description of plasma ions (constant)
  type (ids_plasma_composition_neutral_element_constant),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(DP)  :: z_ion=-9.0D40       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed)
  real(DP)  :: z_ion_error_upper=-9.0D40
  real(DP)  :: z_ion_error_lower=-9.0D40
  integer :: z_ion_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  type (ids_plasma_composition_ion_state_constant) :: state  ! /state - Quantities related to the different states of the species (ionisation, energy, excitation, ...)
endtype

type ids_plasma_composition_neutral_constant  !    Definition of plasma neutral (constant)
  type (ids_plasma_composition_neutral_element_constant),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying neutral (e.g. H, D, T, He, C, ...)
  type (ids_plasma_composition_neutral_state_constant) :: state  ! /state - State of the species (energy, excitation, ...)
endtype

type ids_plasma_composition_ions  !    Array of plasma ions (within a type 3 AoS)
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(DP)  :: z_ion=-9.0D40       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed)
  real(DP)  :: z_ion_error_upper=-9.0D40
  real(DP)  :: z_ion_error_lower=-9.0D40
  integer :: z_ion_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  integer  :: multiple_states_flag=-999999999       ! /multiple_states_flag - Multiple states calculation flag : 0-Only one state is considered; 1-Multiple states are considered
  type (ids_plasma_composition_ion_state),pointer :: state(:) => null()  ! /state(i) - Quantities related to the different states of the species (ionisation, energy, excitation, ...)
endtype

type ids_plasma_composition_species  !    Description of simple species (elements) without declaration of their ionisation state
  real(DP)  :: a=-9.0D40       ! /a - Mass of atom
  real(DP)  :: a_error_upper=-9.0D40
  real(DP)  :: a_error_lower=-9.0D40
  integer :: a_error_index=-999999999

  real(DP)  :: z_n=-9.0D40       ! /z_n - Nuclear charge
  real(DP)  :: z_n_error_upper=-9.0D40
  real(DP)  :: z_n_error_lower=-9.0D40
  integer :: z_n_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H, D, T, ...)
endtype

type ids_plasma_composition_neutral_element_constant  !    Element entering in the composition of the neutral atom or molecule (constant)
  real(DP)  :: a=-9.0D40       ! /a - Mass of atom
  real(DP)  :: a_error_upper=-9.0D40
  real(DP)  :: a_error_lower=-9.0D40
  integer :: a_error_index=-999999999

  real(DP)  :: z_n=-9.0D40       ! /z_n - Nuclear charge
  real(DP)  :: z_n_error_upper=-9.0D40
  real(DP)  :: z_n_error_lower=-9.0D40
  integer :: z_n_error_index=-999999999

  real(DP)  :: multiplicity=-9.0D40       ! /multiplicity - Multiplicity of the atom
  real(DP)  :: multiplicity_error_upper=-9.0D40
  real(DP)  :: multiplicity_error_lower=-9.0D40
  integer :: multiplicity_error_index=-999999999

endtype

type ids_plasma_composition_neutral_element  !    Element entering in the composition of the neutral atom or molecule (within a type 3 AoS)
  real(DP)  :: a=-9.0D40       ! /a - Mass of atom
  real(DP)  :: a_error_upper=-9.0D40
  real(DP)  :: a_error_lower=-9.0D40
  integer :: a_error_index=-999999999

  real(DP)  :: z_n=-9.0D40       ! /z_n - Nuclear charge
  real(DP)  :: z_n_error_upper=-9.0D40
  real(DP)  :: z_n_error_lower=-9.0D40
  integer :: z_n_error_index=-999999999

  real(DP)  :: multiplicity=-9.0D40       ! /multiplicity - Multiplicity of the atom
  real(DP)  :: multiplicity_error_upper=-9.0D40
  real(DP)  :: multiplicity_error_lower=-9.0D40
  integer :: multiplicity_error_index=-999999999

endtype

type ids_plasma_composition_neutral  !    Definition of a neutral atom or molecule
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  type (ids_identifier),pointer :: type(:) => null()  ! /type(i) - List of neutral types, in terms of energy, considered for that neutral species. ID =1: cold; 2: ther
  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying the atom or molecule (e.g. D2, DT, CD4, ...)
endtype

! SPECIAL STRUCTURE data / time
type ids_schedule_waveform_value  !    Values of the reference for the timebase
  real(DP), pointer  :: data(:) => null()     ! /value - Values of the reference for the timebase
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_schedule_waveform  !    Description of a named waveform, normally for scheduling
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Reference name
  type (ids_schedule_waveform_value) :: value  ! /value - Values of the reference for the timebase
endtype

type ids_b_tor_vacuum_1  !    Characteristics of the vacuum toroidal field. time assumed to be one level above
  real(DP)  :: r0=-9.0D40       ! /r0 - Reference major radius where the vacuum toroidal magnetic field is given (usually a fixed position s
  real(DP)  :: r0_error_upper=-9.0D40
  real(DP)  :: r0_error_lower=-9.0D40
  integer :: r0_error_index=-999999999

  real(DP),pointer  :: b0(:) => null()     ! /b0 - Vacuum toroidal field at R0 [T]; Positive sign means anti-clockwise when viewed from above. The prod
  real(DP),pointer  :: b0_error_upper(:) => null()
  real(DP),pointer  :: b0_error_lower(:) => null()
  integer :: b0_error_index=-999999999

endtype

type ids_b_tor_vacuum_2  !    Characteristics of the vacuum toroidal field. time assumed to be two levels above
  real(DP)  :: r0=-9.0D40       ! /r0 - Reference major radius where the vacuum toroidal magnetic field is given (usually a fixed position s
  real(DP)  :: r0_error_upper=-9.0D40
  real(DP)  :: r0_error_lower=-9.0D40
  integer :: r0_error_index=-999999999

  real(DP),pointer  :: b0(:) => null()     ! /b0 - Vacuum toroidal field at b0. Positive sign means anti-clockwise when viewed from above. The product
  real(DP),pointer  :: b0_error_upper(:) => null()
  real(DP),pointer  :: b0_error_lower(:) => null()
  integer :: b0_error_index=-999999999

endtype

type ids_b_tor_vacuum_aos3  !    Characteristics of the vacuum toroidal field, dynamic within a type 3 AoS
  real(DP)  :: r0=-9.0D40       ! /r0 - Reference major radius where the vacuum toroidal magnetic field is given (usually a fixed position s
  real(DP)  :: r0_error_upper=-9.0D40
  real(DP)  :: r0_error_lower=-9.0D40
  integer :: r0_error_index=-999999999

  real(DP)  :: b0=-9.0D40       ! /b0 - Vacuum toroidal field at b0. Positive sign means anti-clockwise when viewed from above. The product
  real(DP)  :: b0_error_upper=-9.0D40
  real(DP)  :: b0_error_lower=-9.0D40
  integer :: b0_error_index=-999999999

endtype

type ids_core_profiles_vector_components_1  !    Vector components in predefined directions for 1D profiles, assuming core_radial_grid one level above
  real(DP),pointer  :: radial(:) => null()     ! /radial - Radial component
  real(DP),pointer  :: radial_error_upper(:) => null()
  real(DP),pointer  :: radial_error_lower(:) => null()
  integer :: radial_error_index=-999999999

  real(DP),pointer  :: diamagnetic(:) => null()     ! /diamagnetic - Diamagnetic component
  real(DP),pointer  :: diamagnetic_error_upper(:) => null()
  real(DP),pointer  :: diamagnetic_error_lower(:) => null()
  integer :: diamagnetic_error_index=-999999999

  real(DP),pointer  :: parallel(:) => null()     ! /parallel - Parallel component
  real(DP),pointer  :: parallel_error_upper(:) => null()
  real(DP),pointer  :: parallel_error_lower(:) => null()
  integer :: parallel_error_index=-999999999

  real(DP),pointer  :: poloidal(:) => null()     ! /poloidal - Poloidal component
  real(DP),pointer  :: poloidal_error_upper(:) => null()
  real(DP),pointer  :: poloidal_error_lower(:) => null()
  integer :: poloidal_error_index=-999999999

  real(DP),pointer  :: toroidal(:) => null()     ! /toroidal - Toroidal component
  real(DP),pointer  :: toroidal_error_upper(:) => null()
  real(DP),pointer  :: toroidal_error_lower(:) => null()
  integer :: toroidal_error_index=-999999999

endtype

type ids_core_profiles_vector_components_2  !    Vector components in predefined directions for 1D profiles, assuming core_radial_grid two levels above
  real(DP),pointer  :: radial(:) => null()     ! /radial - Radial component
  real(DP),pointer  :: radial_error_upper(:) => null()
  real(DP),pointer  :: radial_error_lower(:) => null()
  integer :: radial_error_index=-999999999

  real(DP),pointer  :: diamagnetic(:) => null()     ! /diamagnetic - Diamagnetic component
  real(DP),pointer  :: diamagnetic_error_upper(:) => null()
  real(DP),pointer  :: diamagnetic_error_lower(:) => null()
  integer :: diamagnetic_error_index=-999999999

  real(DP),pointer  :: parallel(:) => null()     ! /parallel - Parallel component
  real(DP),pointer  :: parallel_error_upper(:) => null()
  real(DP),pointer  :: parallel_error_lower(:) => null()
  integer :: parallel_error_index=-999999999

  real(DP),pointer  :: poloidal(:) => null()     ! /poloidal - Poloidal component
  real(DP),pointer  :: poloidal_error_upper(:) => null()
  real(DP),pointer  :: poloidal_error_lower(:) => null()
  integer :: poloidal_error_index=-999999999

  real(DP),pointer  :: toroidal(:) => null()     ! /toroidal - Toroidal component
  real(DP),pointer  :: toroidal_error_upper(:) => null()
  real(DP),pointer  :: toroidal_error_lower(:) => null()
  integer :: toroidal_error_index=-999999999

endtype

type ids_core_profiles_vector_components_3  !    Vector components in predefined directions for 1D profiles, assuming core_radial_grid 3 levels above
  real(DP),pointer  :: radial(:) => null()     ! /radial - Radial component
  real(DP),pointer  :: radial_error_upper(:) => null()
  real(DP),pointer  :: radial_error_lower(:) => null()
  integer :: radial_error_index=-999999999

  real(DP),pointer  :: diamagnetic(:) => null()     ! /diamagnetic - Diamagnetic component
  real(DP),pointer  :: diamagnetic_error_upper(:) => null()
  real(DP),pointer  :: diamagnetic_error_lower(:) => null()
  integer :: diamagnetic_error_index=-999999999

  real(DP),pointer  :: parallel(:) => null()     ! /parallel - Parallel component
  real(DP),pointer  :: parallel_error_upper(:) => null()
  real(DP),pointer  :: parallel_error_lower(:) => null()
  integer :: parallel_error_index=-999999999

  real(DP),pointer  :: poloidal(:) => null()     ! /poloidal - Poloidal component
  real(DP),pointer  :: poloidal_error_upper(:) => null()
  real(DP),pointer  :: poloidal_error_lower(:) => null()
  integer :: poloidal_error_index=-999999999

  real(DP),pointer  :: toroidal(:) => null()     ! /toroidal - Toroidal component
  real(DP),pointer  :: toroidal_error_upper(:) => null()
  real(DP),pointer  :: toroidal_error_lower(:) => null()
  integer :: toroidal_error_index=-999999999

endtype

type ids_core_radial_grid  !    1D radial grid for core* IDSs
  real(DP),pointer  :: rho_tor_norm(:) => null()     ! /rho_tor_norm - Normalised toroidal flux coordinate. The normalizing value for rho_tor_norm, is the toroidal flux co
  real(DP),pointer  :: rho_tor_norm_error_upper(:) => null()
  real(DP),pointer  :: rho_tor_norm_error_lower(:) => null()
  integer :: rho_tor_norm_error_index=-999999999

  real(DP),pointer  :: rho_tor(:) => null()     ! /rho_tor - Toroidal flux coordinate. rho_tor = sqrt(b_flux_tor/(pi*b0)) ~ sqrt(pi*r^2*b0/(pi*b0)) ~ r [m]. The
  real(DP),pointer  :: rho_tor_error_upper(:) => null()
  real(DP),pointer  :: rho_tor_error_lower(:) => null()
  integer :: rho_tor_error_index=-999999999

  real(DP),pointer  :: psi(:) => null()     ! /psi - Poloidal magnetic flux
  real(DP),pointer  :: psi_error_upper(:) => null()
  real(DP),pointer  :: psi_error_lower(:) => null()
  integer :: psi_error_index=-999999999

  real(DP),pointer  :: volume(:) => null()     ! /volume - Volume enclosed inside the magnetic surface
  real(DP),pointer  :: volume_error_upper(:) => null()
  real(DP),pointer  :: volume_error_lower(:) => null()
  integer :: volume_error_index=-999999999

  real(DP),pointer  :: area(:) => null()     ! /area - Cross-sectional area of the flux surface
  real(DP),pointer  :: area_error_upper(:) => null()
  real(DP),pointer  :: area_error_lower(:) => null()
  integer :: area_error_index=-999999999

endtype

type ids_core_profiles_ions_charge_states2  !    Quantities related to the a given state of the ion species
  real(DP)  :: z_min=-9.0D40       ! /z_min - Minimum Z of the charge state bundle
  real(DP)  :: z_min_error_upper=-9.0D40
  real(DP)  :: z_min_error_lower=-9.0D40
  integer :: z_min_error_index=-999999999

  real(DP)  :: z_max=-9.0D40       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(DP)  :: z_max_error_upper=-9.0D40
  real(DP)  :: z_max_error_lower=-9.0D40
  integer :: z_max_error_index=-999999999

  real(DP)  :: z_average=-9.0D40       ! /z_average - Average Z of the charge state bundle (equal to z_min if no bundle), = sum (Z*x_z) where x_z is the r
  real(DP)  :: z_average_error_upper=-9.0D40
  real(DP)  :: z_average_error_lower=-9.0D40
  integer :: z_average_error_index=-999999999

  real(DP)  :: z_square_average=-9.0D40       ! /z_square_average - Average Z square of the charge state bundle (equal to z_min if no bundle), = sum (Z^2*x_z) where x_z
  real(DP)  :: z_square_average_error_upper=-9.0D40
  real(DP)  :: z_square_average_error_lower=-9.0D40
  integer :: z_square_average_error_index=-999999999

  real(DP)  :: ionisation_potential=-9.0D40       ! /ionisation_potential - Cumulative and average ionisation potential to reach a given bundle. Defined as sum (x_z* (sum of Ep
  real(DP)  :: ionisation_potential_error_upper=-9.0D40
  real(DP)  :: ionisation_potential_error_lower=-9.0D40
  integer :: ionisation_potential_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  integer  :: is_neutral=-999999999       ! /is_neutral - Flag specifying if this state corresponds to a neutral (1) or not (0)
  type (ids_identifier) :: neutral_type  ! /neutral_type - Neutral type (if the considered state is a neutral), in terms of energy. ID =1: cold; 2: thermal; 3:
  type (ids_core_profiles_vector_components_3) :: velocity  ! /velocity - Velocity
  real(DP),pointer  :: temperature(:) => null()     ! /temperature - Temperature
  real(DP),pointer  :: temperature_error_upper(:) => null()
  real(DP),pointer  :: temperature_error_lower(:) => null()
  integer :: temperature_error_index=-999999999

  real(DP),pointer  :: density(:) => null()     ! /density - Density (thermal+non-thermal)
  real(DP),pointer  :: density_error_upper(:) => null()
  real(DP),pointer  :: density_error_lower(:) => null()
  integer :: density_error_index=-999999999

  real(DP),pointer  :: density_fast(:) => null()     ! /density_fast - Density of fast (non-thermal) particles
  real(DP),pointer  :: density_fast_error_upper(:) => null()
  real(DP),pointer  :: density_fast_error_lower(:) => null()
  integer :: density_fast_error_index=-999999999

  real(DP),pointer  :: pressure(:) => null()     ! /pressure - Pressure
  real(DP),pointer  :: pressure_error_upper(:) => null()
  real(DP),pointer  :: pressure_error_lower(:) => null()
  integer :: pressure_error_index=-999999999

  real(DP),pointer  :: pressure_fast_perpendicular(:) => null()     ! /pressure_fast_perpendicular - Fast (non-thermal) perpendicular pressure
  real(DP),pointer  :: pressure_fast_perpendicular_error_upper(:) => null()
  real(DP),pointer  :: pressure_fast_perpendicular_error_lower(:) => null()
  integer :: pressure_fast_perpendicular_error_index=-999999999

  real(DP),pointer  :: pressure_fast_parallel(:) => null()     ! /pressure_fast_parallel - Fast (non-thermal) parallel pressure
  real(DP),pointer  :: pressure_fast_parallel_error_upper(:) => null()
  real(DP),pointer  :: pressure_fast_parallel_error_lower(:) => null()
  integer :: pressure_fast_parallel_error_index=-999999999

endtype

type ids_core_profile_ions  !    Quantities related to a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(DP)  :: z_ion=-9.0D40       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed)
  real(DP)  :: z_ion_error_upper=-9.0D40
  real(DP)  :: z_ion_error_lower=-9.0D40
  integer :: z_ion_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  integer  :: neutral_index=-999999999       ! /neutral_index - Index of the corresponding neutral species in the ../../neutral array
  real(DP),pointer  :: temperature(:) => null()     ! /temperature - Temperature (average over charge states when multiple charge states are considered)
  real(DP),pointer  :: temperature_error_upper(:) => null()
  real(DP),pointer  :: temperature_error_lower(:) => null()
  integer :: temperature_error_index=-999999999

  real(DP),pointer  :: density(:) => null()     ! /density - Density (thermal+non-thermal) (sum over charge states when multiple charge states are considered)
  real(DP),pointer  :: density_error_upper(:) => null()
  real(DP),pointer  :: density_error_lower(:) => null()
  integer :: density_error_index=-999999999

  real(DP),pointer  :: density_fast(:) => null()     ! /density_fast - Density of fast (non-thermal) particles (sum over charge states when multiple charge states are cons
  real(DP),pointer  :: density_fast_error_upper(:) => null()
  real(DP),pointer  :: density_fast_error_lower(:) => null()
  integer :: density_fast_error_index=-999999999

  real(DP),pointer  :: pressure(:) => null()     ! /pressure - Pressure (sum over charge states when multiple charge states are considered)
  real(DP),pointer  :: pressure_error_upper(:) => null()
  real(DP),pointer  :: pressure_error_lower(:) => null()
  integer :: pressure_error_index=-999999999

  real(DP),pointer  :: pressure_fast_perpendicular(:) => null()     ! /pressure_fast_perpendicular - Fast (non-thermal) perpendicular pressure  (sum over charge states when multiple charge states are c
  real(DP),pointer  :: pressure_fast_perpendicular_error_upper(:) => null()
  real(DP),pointer  :: pressure_fast_perpendicular_error_lower(:) => null()
  integer :: pressure_fast_perpendicular_error_index=-999999999

  real(DP),pointer  :: pressure_fast_parallel(:) => null()     ! /pressure_fast_parallel - Fast (non-thermal) parallel pressure  (sum over charge states when multiple charge states are consid
  real(DP),pointer  :: pressure_fast_parallel_error_upper(:) => null()
  real(DP),pointer  :: pressure_fast_parallel_error_lower(:) => null()
  integer :: pressure_fast_parallel_error_index=-999999999

  real(DP),pointer  :: velocity_tor(:) => null()     ! /velocity_tor - Toroidal velocity (average over charge states when multiple charge states are considered)
  real(DP),pointer  :: velocity_tor_error_upper(:) => null()
  real(DP),pointer  :: velocity_tor_error_lower(:) => null()
  integer :: velocity_tor_error_index=-999999999

  real(DP),pointer  :: velocity_pol(:) => null()     ! /velocity_pol - Poloidal velocity (average over charge states when multiple charge states are considered)
  real(DP),pointer  :: velocity_pol_error_upper(:) => null()
  real(DP),pointer  :: velocity_pol_error_lower(:) => null()
  integer :: velocity_pol_error_index=-999999999

  type (ids_core_profiles_vector_components_2) :: velocity  ! /velocity - Velocity (average over charge states when multiple charge states are considered)
  integer  :: multiple_states_flag=-999999999       ! /multiple_states_flag - Multiple states calculation flag : 0-Only one state is considered; 1-Multiple states are considered
  type (ids_core_profiles_ions_charge_states2),pointer :: state(:) => null()  ! /state(i) - Quantities related to the different states of the species (ionisation, energy, excitation, ...)
endtype

type ids_core_profiles_neutral_state  !    Quantities related to the a given state of the neutral species
  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  type (ids_identifier) :: neutral_type  ! /neutral_type - Neutral type (if the considered state is a neutral), in terms of energy. ID =1: cold; 2: thermal; 3:
  type (ids_core_profiles_vector_components_3) :: velocity  ! /velocity - Velocity
  real(DP),pointer  :: temperature(:) => null()     ! /temperature - Temperature
  real(DP),pointer  :: temperature_error_upper(:) => null()
  real(DP),pointer  :: temperature_error_lower(:) => null()
  integer :: temperature_error_index=-999999999

  real(DP),pointer  :: density(:) => null()     ! /density - Density (thermal+non-thermal)
  real(DP),pointer  :: density_error_upper(:) => null()
  real(DP),pointer  :: density_error_lower(:) => null()
  integer :: density_error_index=-999999999

  real(DP),pointer  :: density_fast(:) => null()     ! /density_fast - Density of fast (non-thermal) particles
  real(DP),pointer  :: density_fast_error_upper(:) => null()
  real(DP),pointer  :: density_fast_error_lower(:) => null()
  integer :: density_fast_error_index=-999999999

  real(DP),pointer  :: pressure(:) => null()     ! /pressure - Pressure
  real(DP),pointer  :: pressure_error_upper(:) => null()
  real(DP),pointer  :: pressure_error_lower(:) => null()
  integer :: pressure_error_index=-999999999

  real(DP),pointer  :: pressure_fast_perpendicular(:) => null()     ! /pressure_fast_perpendicular - Fast (non-thermal) perpendicular pressure
  real(DP),pointer  :: pressure_fast_perpendicular_error_upper(:) => null()
  real(DP),pointer  :: pressure_fast_perpendicular_error_lower(:) => null()
  integer :: pressure_fast_perpendicular_error_index=-999999999

  real(DP),pointer  :: pressure_fast_parallel(:) => null()     ! /pressure_fast_parallel - Fast (non-thermal) parallel pressure
  real(DP),pointer  :: pressure_fast_parallel_error_upper(:) => null()
  real(DP),pointer  :: pressure_fast_parallel_error_lower(:) => null()
  integer :: pressure_fast_parallel_error_index=-999999999

endtype

type ids_core_profile_neutral  !    Quantities related to a given neutral species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  integer  :: ion_index=-999999999       ! /ion_index - Index of the corresponding ion species in the ../../ion array
  real(DP),pointer  :: temperature(:) => null()     ! /temperature - Temperature (average over charge states when multiple charge states are considered)
  real(DP),pointer  :: temperature_error_upper(:) => null()
  real(DP),pointer  :: temperature_error_lower(:) => null()
  integer :: temperature_error_index=-999999999

  real(DP),pointer  :: density(:) => null()     ! /density - Density (thermal+non-thermal) (sum over charge states when multiple charge states are considered)
  real(DP),pointer  :: density_error_upper(:) => null()
  real(DP),pointer  :: density_error_lower(:) => null()
  integer :: density_error_index=-999999999

  real(DP),pointer  :: density_fast(:) => null()     ! /density_fast - Density of fast (non-thermal) particles (sum over charge states when multiple charge states are cons
  real(DP),pointer  :: density_fast_error_upper(:) => null()
  real(DP),pointer  :: density_fast_error_lower(:) => null()
  integer :: density_fast_error_index=-999999999

  real(DP),pointer  :: pressure(:) => null()     ! /pressure - Pressure (sum over charge states when multiple charge states are considered)
  real(DP),pointer  :: pressure_error_upper(:) => null()
  real(DP),pointer  :: pressure_error_lower(:) => null()
  integer :: pressure_error_index=-999999999

  real(DP),pointer  :: pressure_fast_perpendicular(:) => null()     ! /pressure_fast_perpendicular - Fast (non-thermal) perpendicular pressure  (sum over charge states when multiple charge states are c
  real(DP),pointer  :: pressure_fast_perpendicular_error_upper(:) => null()
  real(DP),pointer  :: pressure_fast_perpendicular_error_lower(:) => null()
  integer :: pressure_fast_perpendicular_error_index=-999999999

  real(DP),pointer  :: pressure_fast_parallel(:) => null()     ! /pressure_fast_parallel - Fast (non-thermal) parallel pressure  (sum over charge states when multiple charge states are consid
  real(DP),pointer  :: pressure_fast_parallel_error_upper(:) => null()
  real(DP),pointer  :: pressure_fast_parallel_error_lower(:) => null()
  integer :: pressure_fast_parallel_error_index=-999999999

  type (ids_core_profiles_vector_components_2) :: velocity  ! /velocity - Velocity (average over charge states when multiple charge states are considered)
  integer  :: multiple_states_flag=-999999999       ! /multiple_states_flag - Multiple states calculation flag : 0-Only one state is considered; 1-Multiple states are considered
  type (ids_core_profiles_neutral_state),pointer :: state(:) => null()  ! /state(i) - Quantities related to the different states of the species (energy, excitation, ...)
endtype

type ids_core_profiles_profiles_1d_electrons  !    Quantities related to electrons
  real(DP),pointer  :: temperature(:) => null()     ! /temperature - Temperature
  real(DP),pointer  :: temperature_error_upper(:) => null()
  real(DP),pointer  :: temperature_error_lower(:) => null()
  integer :: temperature_error_index=-999999999

  real(DP),pointer  :: density(:) => null()     ! /density - Density (thermal+non-thermal)
  real(DP),pointer  :: density_error_upper(:) => null()
  real(DP),pointer  :: density_error_lower(:) => null()
  integer :: density_error_index=-999999999

  real(DP),pointer  :: density_fast(:) => null()     ! /density_fast - Density of fast (non-thermal) particles
  real(DP),pointer  :: density_fast_error_upper(:) => null()
  real(DP),pointer  :: density_fast_error_lower(:) => null()
  integer :: density_fast_error_index=-999999999

  real(DP),pointer  :: pressure(:) => null()     ! /pressure - Pressure
  real(DP),pointer  :: pressure_error_upper(:) => null()
  real(DP),pointer  :: pressure_error_lower(:) => null()
  integer :: pressure_error_index=-999999999

  real(DP),pointer  :: pressure_fast_perpendicular(:) => null()     ! /pressure_fast_perpendicular - Fast (non-thermal) perpendicular pressure
  real(DP),pointer  :: pressure_fast_perpendicular_error_upper(:) => null()
  real(DP),pointer  :: pressure_fast_perpendicular_error_lower(:) => null()
  integer :: pressure_fast_perpendicular_error_index=-999999999

  real(DP),pointer  :: pressure_fast_parallel(:) => null()     ! /pressure_fast_parallel - Fast (non-thermal) parallel pressure
  real(DP),pointer  :: pressure_fast_parallel_error_upper(:) => null()
  real(DP),pointer  :: pressure_fast_parallel_error_lower(:) => null()
  integer :: pressure_fast_parallel_error_index=-999999999

  real(DP),pointer  :: velocity_tor(:) => null()     ! /velocity_tor - Toroidal velocity
  real(DP),pointer  :: velocity_tor_error_upper(:) => null()
  real(DP),pointer  :: velocity_tor_error_lower(:) => null()
  integer :: velocity_tor_error_index=-999999999

  real(DP),pointer  :: velocity_pol(:) => null()     ! /velocity_pol - Poloidal velocity
  real(DP),pointer  :: velocity_pol_error_upper(:) => null()
  real(DP),pointer  :: velocity_pol_error_lower(:) => null()
  integer :: velocity_pol_error_index=-999999999

  type (ids_core_profiles_vector_components_2) :: velocity  ! /velocity - Velocity
endtype

type ids_core_profiles_profiles_1d  !    1D radial profiles for core and edge
  type (ids_core_radial_grid) :: grid  ! /grid - Radial grid
  type (ids_core_profiles_profiles_1d_electrons) :: electrons  ! /electrons - Quantities related to the electrons
  type (ids_core_profile_ions),pointer :: ion(:) => null()  ! /ion(i) - Quantities related to the different ion species
  type (ids_core_profile_neutral),pointer :: neutral(:) => null()  ! /neutral(i) - Quantities related to the different neutral species
  real(DP),pointer  :: t_i_average(:) => null()     ! /t_i_average - Ion temperature (averaged on charge states and ion species)
  real(DP),pointer  :: t_i_average_error_upper(:) => null()
  real(DP),pointer  :: t_i_average_error_lower(:) => null()
  integer :: t_i_average_error_index=-999999999

  real(DP),pointer  :: n_i_total_over_n_e(:) => null()     ! /n_i_total_over_n_e - Ratio of total ion density (sum over species and charge states) over electron density. (thermal+non-
  real(DP),pointer  :: n_i_total_over_n_e_error_upper(:) => null()
  real(DP),pointer  :: n_i_total_over_n_e_error_lower(:) => null()
  integer :: n_i_total_over_n_e_error_index=-999999999

  real(DP),pointer  :: momentum_tor(:) => null()     ! /momentum_tor - Total plasma toroidal momentum, summed over ion species and electrons
  real(DP),pointer  :: momentum_tor_error_upper(:) => null()
  real(DP),pointer  :: momentum_tor_error_lower(:) => null()
  integer :: momentum_tor_error_index=-999999999

  real(DP),pointer  :: zeff(:) => null()     ! /zeff - Effective charge
  real(DP),pointer  :: zeff_error_upper(:) => null()
  real(DP),pointer  :: zeff_error_lower(:) => null()
  integer :: zeff_error_index=-999999999

  real(DP),pointer  :: pressure_ion_total(:) => null()     ! /pressure_ion_total - Total (sum over ion species) thermal ion pressure
  real(DP),pointer  :: pressure_ion_total_error_upper(:) => null()
  real(DP),pointer  :: pressure_ion_total_error_lower(:) => null()
  integer :: pressure_ion_total_error_index=-999999999

  real(DP),pointer  :: pressure_thermal(:) => null()     ! /pressure_thermal - Thermal pressure (electrons+ions)
  real(DP),pointer  :: pressure_thermal_error_upper(:) => null()
  real(DP),pointer  :: pressure_thermal_error_lower(:) => null()
  integer :: pressure_thermal_error_index=-999999999

  real(DP),pointer  :: pressure_perpendicular(:) => null()     ! /pressure_perpendicular - Total perpendicular pressure (electrons+ions, thermal+non-thermal)
  real(DP),pointer  :: pressure_perpendicular_error_upper(:) => null()
  real(DP),pointer  :: pressure_perpendicular_error_lower(:) => null()
  integer :: pressure_perpendicular_error_index=-999999999

  real(DP),pointer  :: pressure_parallel(:) => null()     ! /pressure_parallel - Total parallel pressure (electrons+ions, thermal+non-thermal)
  real(DP),pointer  :: pressure_parallel_error_upper(:) => null()
  real(DP),pointer  :: pressure_parallel_error_lower(:) => null()
  integer :: pressure_parallel_error_index=-999999999

  real(DP),pointer  :: j_total(:) => null()     ! /j_total - Total parallel current density = average(jtot.B) / B0, where B0 = Core_Profiles/Vacuum_Toroidal_Fiel
  real(DP),pointer  :: j_total_error_upper(:) => null()
  real(DP),pointer  :: j_total_error_lower(:) => null()
  integer :: j_total_error_index=-999999999

  real(DP),pointer  :: j_tor(:) => null()     ! /j_tor - Total toroidal current density = average(J_Tor/R) / average(1/R)
  real(DP),pointer  :: j_tor_error_upper(:) => null()
  real(DP),pointer  :: j_tor_error_lower(:) => null()
  integer :: j_tor_error_index=-999999999

  real(DP),pointer  :: j_ohmic(:) => null()     ! /j_ohmic - Ohmic parallel current density = average(J_Ohmic.B) / B0, where B0 = Core_Profiles/Vacuum_Toroidal_F
  real(DP),pointer  :: j_ohmic_error_upper(:) => null()
  real(DP),pointer  :: j_ohmic_error_lower(:) => null()
  integer :: j_ohmic_error_index=-999999999

  real(DP),pointer  :: j_non_inductive(:) => null()     ! /j_non_inductive - Non-inductive (includes bootstrap) parallel current density = average(jni.B) / B0, where B0 = Core_P
  real(DP),pointer  :: j_non_inductive_error_upper(:) => null()
  real(DP),pointer  :: j_non_inductive_error_lower(:) => null()
  integer :: j_non_inductive_error_index=-999999999

  real(DP),pointer  :: j_bootstrap(:) => null()     ! /j_bootstrap - Bootstrap current density = average(J_Bootstrap.B) / B0, where B0 = Core_Profiles/Vacuum_Toroidal_Fi
  real(DP),pointer  :: j_bootstrap_error_upper(:) => null()
  real(DP),pointer  :: j_bootstrap_error_lower(:) => null()
  integer :: j_bootstrap_error_index=-999999999

  real(DP),pointer  :: conductivity_parallel(:) => null()     ! /conductivity_parallel - Parallel conductivity
  real(DP),pointer  :: conductivity_parallel_error_upper(:) => null()
  real(DP),pointer  :: conductivity_parallel_error_lower(:) => null()
  integer :: conductivity_parallel_error_index=-999999999

  real(DP),pointer  :: e_field_parallel(:) => null()     ! /e_field_parallel - Parallel electric field = average(E.B) / B0, where Core_Profiles/Vacuum_Toroidal_Field/ B0
  real(DP),pointer  :: e_field_parallel_error_upper(:) => null()
  real(DP),pointer  :: e_field_parallel_error_lower(:) => null()
  integer :: e_field_parallel_error_index=-999999999

  type (ids_core_profiles_vector_components_1) :: e_field  ! /e_field - Electric field, averaged on the magnetic surface. E.g for the parellel component, average(E.B) / B0,
  real(DP),pointer  :: q(:) => null()     ! /q - Safety factor
  real(DP),pointer  :: q_error_upper(:) => null()
  real(DP),pointer  :: q_error_lower(:) => null()
  integer :: q_error_index=-999999999

  real(DP),pointer  :: magnetic_shear(:) => null()     ! /magnetic_shear - Magnetic shear, defined as rho_tor/q . dq/drho_tor
  real(DP),pointer  :: magnetic_shear_error_upper(:) => null()
  real(DP),pointer  :: magnetic_shear_error_lower(:) => null()
  integer :: magnetic_shear_error_index=-999999999

  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_distribution_markers  !    Test particles for a given time slice
  type (ids_identifier_dynamic_aos3),pointer :: coordinate_identifier(:) => null()  ! /coordinate_identifier(i) - Set of coordinate identifiers, coordinates on which the markers are represented
  real(DP),pointer  :: weights(:) => null()     ! /weights - Weight of the markers, i.e. number of real particles represented by each marker. The dimension of th
  real(DP),pointer  :: weights_error_upper(:) => null()
  real(DP),pointer  :: weights_error_lower(:) => null()
  integer :: weights_error_index=-999999999

  real(DP),pointer  :: positions(:,:) => null()     ! /positions - Position of the markers in the set of coordinates. The first dimension corresponds to the number of
  real(DP),pointer  :: positions_error_upper(:,:) => null()
  real(DP),pointer  :: positions_error_lower(:,:) => null()
  integer :: positions_error_index=-999999999

  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_distribution_species  !    Description of a species in a distribution function related IDS
  type (ids_identifier) :: type  ! /type - Species type. index=1 for electron; index=2 for ion species in a single/average state (refer to ion
  type (ids_plasma_composition_ions_constant) :: ion  ! /ion - Description of the ion or neutral species, used if type/index = 2 or 3
  type (ids_plasma_composition_neutral_constant) :: neutral  ! /neutral - Description of the neutral species, used if type/index = 4 or 5
endtype

type ids_distribution_process_identifier  !    Identifier an NBI or fusion reaction process intervening affecting a distribution function
  type (ids_identifier) :: type  ! /type - Process type. index=1 for NBI; index=2 for nuclear reaction (reaction unspecified); index=3 for nucl
  type (ids_identifier) :: reactant_energy  ! /reactant_energy - For nuclear reaction source, energy of the reactants. index = 0 for a sum over all energies; index =
  type (ids_identifier) :: nbi_energy  ! /nbi_energy - For NBI source, energy of the accelerated species considered. index = 0 for a sum over all energies;
  integer  :: nbi_unit=-999999999       ! /nbi_unit - Index of the NBI unit considered. Refers to the "unit" array of the NBI IDS. 0 means sum over all NB
  integer  :: nbi_beamlets_group=-999999999       ! /nbi_beamlets_group - Index of the NBI beamlets group considered. Refers to the "unit/beamlets_group" array of the NBI IDS
endtype

type ids_generic_grid_scalar  !    Scalar values on a generic grid (dynamic within a type 3 AoS)
  integer  :: grid_index=-999999999       ! /grid_index - Index of the grid used to represent this quantity
  integer  :: grid_subset_index=-999999999       ! /grid_subset_index - Index of the grid subset the data is provided on
  real(DP),pointer  :: values(:) => null()     ! /values - One scalar value is provided per element in the grid subset.
  real(DP),pointer  :: values_error_upper(:) => null()
  real(DP),pointer  :: values_error_lower(:) => null()
  integer :: values_error_index=-999999999

  real(DP),pointer  :: coefficients(:,:) => null()     ! /coefficients - Interpolation coefficients, to be used for a high precision evaluation of the physical quantity with
  real(DP),pointer  :: coefficients_error_upper(:,:) => null()
  real(DP),pointer  :: coefficients_error_lower(:,:) => null()
  integer :: coefficients_error_index=-999999999

endtype

type ids_generic_grid_vector  !    Vector values on a generic grid (dynamic within a type 3 AoS)
  integer  :: grid_index=-999999999       ! /grid_index - Index of the grid used to represent this quantity
  integer  :: grid_subset_index=-999999999       ! /grid_subset_index - Index of the grid subset the data is provided on
  real(DP),pointer  :: values(:,:) => null()     ! /values - List of vector components, one list per element in the grid subset. First dimenstion: element index.
  real(DP),pointer  :: values_error_upper(:,:) => null()
  real(DP),pointer  :: values_error_lower(:,:) => null()
  integer :: values_error_index=-999999999

  real(DP),pointer  :: coefficients(:,:,:) => null()     ! /coefficients - Interpolation coefficients, to be used for a high precision evaluation of the physical quantity with
  real(DP),pointer  :: coefficients_error_upper(:,:,:) => null()
  real(DP),pointer  :: coefficients_error_lower(:,:,:) => null()
  integer :: coefficients_error_index=-999999999

endtype

type ids_generic_grid_vector_components  !    Vector components in predefined directions on a generic grid (dynamic within a type 3 AoS)
  integer  :: grid_index=-999999999       ! /grid_index - Index of the grid used to represent this quantity
  integer  :: grid_subset_index=-999999999       ! /grid_subset_index - Index of the grid subset the data is provided on
  real(DP),pointer  :: radial(:) => null()     ! /radial - Radial component, one scalar value is provided per element in the grid subset.
  real(DP),pointer  :: radial_error_upper(:) => null()
  real(DP),pointer  :: radial_error_lower(:) => null()
  integer :: radial_error_index=-999999999

  real(DP),pointer  :: radial_coefficients(:,:) => null()     ! /radial_coefficients - Interpolation coefficients for the radial component, to be used for a high precision evaluation of t
  real(DP),pointer  :: radial_coefficients_error_upper(:,:) => null()
  real(DP),pointer  :: radial_coefficients_error_lower(:,:) => null()
  integer :: radial_coefficients_error_index=-999999999

  real(DP),pointer  :: diamagnetic(:) => null()     ! /diamagnetic - Diamagnetic component, one scalar value is provided per element in the grid subset.
  real(DP),pointer  :: diamagnetic_error_upper(:) => null()
  real(DP),pointer  :: diamagnetic_error_lower(:) => null()
  integer :: diamagnetic_error_index=-999999999

  real(DP),pointer  :: diamagnetic_coefficients(:,:) => null()     ! /diamagnetic_coefficients - Interpolation coefficients for the diamagnetic component, to be used for a high precision evaluation
  real(DP),pointer  :: diamagnetic_coefficients_error_upper(:,:) => null()
  real(DP),pointer  :: diamagnetic_coefficients_error_lower(:,:) => null()
  integer :: diamagnetic_coefficients_error_index=-999999999

  real(DP),pointer  :: parallel(:) => null()     ! /parallel - Parallel component, one scalar value is provided per element in the grid subset.
  real(DP),pointer  :: parallel_error_upper(:) => null()
  real(DP),pointer  :: parallel_error_lower(:) => null()
  integer :: parallel_error_index=-999999999

  real(DP),pointer  :: parallel_coefficients(:,:) => null()     ! /parallel_coefficients - Interpolation coefficients for the parallel component, to be used for a high precision evaluation of
  real(DP),pointer  :: parallel_coefficients_error_upper(:,:) => null()
  real(DP),pointer  :: parallel_coefficients_error_lower(:,:) => null()
  integer :: parallel_coefficients_error_index=-999999999

  real(DP),pointer  :: poloidal(:) => null()     ! /poloidal - Poloidal component, one scalar value is provided per element in the grid subset.
  real(DP),pointer  :: poloidal_error_upper(:) => null()
  real(DP),pointer  :: poloidal_error_lower(:) => null()
  integer :: poloidal_error_index=-999999999

  real(DP),pointer  :: poloidal_coefficients(:,:) => null()     ! /poloidal_coefficients - Interpolation coefficients for the poloidal component, to be used for a high precision evaluation of
  real(DP),pointer  :: poloidal_coefficients_error_upper(:,:) => null()
  real(DP),pointer  :: poloidal_coefficients_error_lower(:,:) => null()
  integer :: poloidal_coefficients_error_index=-999999999

  real(DP),pointer  :: toroidal(:) => null()     ! /toroidal - Toroidal component, one scalar value is provided per element in the grid subset.
  real(DP),pointer  :: toroidal_error_upper(:) => null()
  real(DP),pointer  :: toroidal_error_lower(:) => null()
  integer :: toroidal_error_index=-999999999

  real(DP),pointer  :: toroidal_coefficients(:,:) => null()     ! /toroidal_coefficients - Interpolation coefficients for the toroidal component, to be used for a high precision evaluation of
  real(DP),pointer  :: toroidal_coefficients_error_upper(:,:) => null()
  real(DP),pointer  :: toroidal_coefficients_error_lower(:,:) => null()
  integer :: toroidal_coefficients_error_index=-999999999

endtype

type ids_generic_grid_matrix  !    Matrix values on a generic grid (dynamic within a type 3 AoS)
  integer  :: grid_index=-999999999       ! /grid_index - Index of the grid used to represent this quantity
  integer  :: grid_subset_index=-999999999       ! /grid_subset_index - Index of the grid subset the data is provided on
  real(DP),pointer  :: values(:,:,:) => null()     ! /values - List of matrix components, one list per element in the grid subset. First dimenstion: element index.
  real(DP),pointer  :: values_error_upper(:,:,:) => null()
  real(DP),pointer  :: values_error_lower(:,:,:) => null()
  integer :: values_error_index=-999999999

  real(DP),pointer  :: coefficients(:,:,:,:) => null()     ! /coefficients - Interpolation coefficients, to be used for a high precision evaluation of the physical quantity with
  real(DP),pointer  :: coefficients_error_upper(:,:,:,:) => null()
  real(DP),pointer  :: coefficients_error_lower(:,:,:,:) => null()
  integer :: coefficients_error_index=-999999999

endtype

type ids_generic_grid_dynamic_space_dimension_object_boundary  !    Generic grid, description of an object boundary and its neighbours (dynamic within a type 3 AoS)
  integer  :: index=-999999999       ! /index - Index of this (n-1)-dimensional boundary object
  integer,pointer  :: neighbours(:) => null()      ! /neighbours - List of indices of the n-dimensional objects adjacent to the given n-dimensional object. An object c
endtype

type ids_generic_grid_dynamic_space_dimension_object  !    Generic grid, list of objects of a given dimension within a space (dynamic within a type 3 AoS)
  type (ids_generic_grid_dynamic_space_dimension_object_boundary),pointer :: boundary(:) => null()  ! /boundary(i) - Set of  (n-1)-dimensional objects defining the boundary of this n-dimensional object
  real(DP),pointer  :: geometry(:) => null()     ! /geometry - Geometry data associated with the object. Its dimension depends on the type of object, geometry and
  real(DP),pointer  :: geometry_error_upper(:) => null()
  real(DP),pointer  :: geometry_error_lower(:) => null()
  integer :: geometry_error_index=-999999999

  integer,pointer  :: nodes(:) => null()      ! /nodes - List of nodes forming this object (indices to objects_per_dimension(1)%object(:) in Fortran notation
  real(DP)  :: measure=-9.0D40       ! /measure - Measure of the space object, i.e. physical size (length for 1d, area for 2d, volume for 3d objects,.
  real(DP)  :: measure_error_upper=-9.0D40
  real(DP)  :: measure_error_lower=-9.0D40
  integer :: measure_error_index=-999999999

endtype

type ids_generic_grid_dynamic_space_dimension  !    Generic grid, list of dimensions within a space (dynamic within a type 3 AoS)
  type (ids_generic_grid_dynamic_space_dimension_object),pointer :: object(:) => null()  ! /object(i) - Set of objects for a given dimension
endtype

type ids_generic_grid_dynamic_space  !    Generic grid space (dynamic within a type 3 AoS)
  type (ids_identifier_dynamic_aos3) :: geometry_type  ! /geometry_type - Type of space geometry (0: standard, 1:Fourier)
  integer,pointer  :: coordinates_type(:) => null()      ! /coordinates_type - Type of coordinates describing the physical space, for every coordinate of the space. The size of th
  type (ids_generic_grid_dynamic_space_dimension),pointer :: objects_per_dimension(:) => null()  ! /objects_per_dimension(i) - Definition of the space objects for every dimension (from one to the dimension of the highest-dimens
endtype

type ids_generic_grid_dynamic_grid_subset_element_object  !    Generic grid, object part of an element part of a grid_subset (dynamic within a type 3 AoS)
  integer  :: space=-999999999       ! /space - Index of the space from which that object is taken
  integer  :: dimension=-999999999       ! /dimension - Dimension of the object
  integer  :: index=-999999999       ! /index - Object index
endtype

type ids_generic_grid_dynamic_grid_subset_element  !    Generic grid, element part of a grid_subset (dynamic within a type 3 AoS)
  type (ids_generic_grid_dynamic_grid_subset_element_object),pointer :: object(:) => null()  ! /object(i) - Set of objects defining the element
endtype

type ids_generic_grid_dynamic_grid_subset_metric  !    Generic grid, metric description for a given grid_subset and base (dynamic within a type 3 AoS)
  real(DP),pointer  :: jacobian(:) => null()     ! /jacobian - Metric Jacobian
  real(DP),pointer  :: jacobian_error_upper(:) => null()
  real(DP),pointer  :: jacobian_error_lower(:) => null()
  integer :: jacobian_error_index=-999999999

  real(DP),pointer  :: tensor_covariant(:,:,:) => null()     ! /tensor_covariant - Covariant metric tensor, given on each element of the subgrid (first dimension)
  real(DP),pointer  :: tensor_covariant_error_upper(:,:,:) => null()
  real(DP),pointer  :: tensor_covariant_error_lower(:,:,:) => null()
  integer :: tensor_covariant_error_index=-999999999

  real(DP),pointer  :: tensor_contravariant(:,:,:) => null()     ! /tensor_contravariant - Contravariant metric tensor, given on each element of the subgrid (first dimension)
  real(DP),pointer  :: tensor_contravariant_error_upper(:,:,:) => null()
  real(DP),pointer  :: tensor_contravariant_error_lower(:,:,:) => null()
  integer :: tensor_contravariant_error_index=-999999999

endtype

type ids_generic_grid_dynamic_grid_subset  !    Generic grid grid_subset (dynamic within a type 3 AoS)
  type (ids_identifier_dynamic_aos3) :: identifier  ! /identifier - Grid subset identifier
  integer  :: dimension=-999999999       ! /dimension - Space dimension of the grid subset elements. This must be equal to the sum of the dimensions of the
  type (ids_generic_grid_dynamic_grid_subset_element),pointer :: element(:) => null()  ! /element(i) - Set of elements defining the grid subset. An element is defined by a combination of objects from pot
  type (ids_generic_grid_dynamic_grid_subset_metric),pointer :: base(:) => null()  ! /base(i) - Set of bases for the grid subset. For each base, the structure describes the projection of the base
  type (ids_generic_grid_dynamic_grid_subset_metric) :: metric  ! /metric - Metric of the canonical frame onto Cartesian coordinates
endtype

type ids_generic_grid_dynamic  !    Generic grid (dynamic within a type 3 AoS)
  type (ids_identifier_dynamic_aos3) :: identifier  ! /identifier - Grid identifier
  type (ids_generic_grid_dynamic_space),pointer :: space(:) => null()  ! /space(i) - Set of grid spaces
  type (ids_generic_grid_dynamic_grid_subset),pointer :: grid_subset(:) => null()  ! /grid_subset(i) - Grid subsets
endtype

type ids_equilibrium_profiles_2d_grid  !    Definition of the 2D grid
  real(DP),pointer  :: dim1(:) => null()     ! /dim1 - First dimension values
  real(DP),pointer  :: dim1_error_upper(:) => null()
  real(DP),pointer  :: dim1_error_lower(:) => null()
  integer :: dim1_error_index=-999999999

  real(DP),pointer  :: dim2(:) => null()     ! /dim2 - Second dimension values
  real(DP),pointer  :: dim2_error_upper(:) => null()
  real(DP),pointer  :: dim2_error_lower(:) => null()
  integer :: dim2_error_index=-999999999

  real(DP),pointer  :: volume_element(:,:) => null()     ! /volume_element - Elementary plasma volume of plasma enclosed in the cell formed by the nodes [dim1(i) dim2(j)], [dim1
  real(DP),pointer  :: volume_element_error_upper(:,:) => null()
  real(DP),pointer  :: volume_element_error_lower(:,:) => null()
  integer :: volume_element_error_index=-999999999

endtype

type ids_equilibrium_coordinate_system  !    Flux surface coordinate system on a square grid of flux and poloidal angle
  type (ids_identifier) :: grid_type  ! /grid_type - Type of coordinate system
  type (ids_equilibrium_profiles_2d_grid) :: grid  ! /grid - Definition of the 2D grid
  real(DP),pointer  :: r(:,:) => null()     ! /r - Values of the major radius on the grid
  real(DP),pointer  :: r_error_upper(:,:) => null()
  real(DP),pointer  :: r_error_lower(:,:) => null()
  integer :: r_error_index=-999999999

  real(DP),pointer  :: z(:,:) => null()     ! /z - Values of the Height on the grid
  real(DP),pointer  :: z_error_upper(:,:) => null()
  real(DP),pointer  :: z_error_lower(:,:) => null()
  integer :: z_error_index=-999999999

  real(DP),pointer  :: jacobian(:,:) => null()     ! /jacobian - Jacobian of the coordinate system
  real(DP),pointer  :: jacobian_error_upper(:,:) => null()
  real(DP),pointer  :: jacobian_error_lower(:,:) => null()
  integer :: jacobian_error_index=-999999999

  real(DP),pointer  :: tensor_covariant(:,:,:,:) => null()     ! /tensor_covariant - Covariant metric tensor on every point of the grid described by grid_type
  real(DP),pointer  :: tensor_covariant_error_upper(:,:,:,:) => null()
  real(DP),pointer  :: tensor_covariant_error_lower(:,:,:,:) => null()
  integer :: tensor_covariant_error_index=-999999999

  real(DP),pointer  :: tensor_contravariant(:,:,:,:) => null()     ! /tensor_contravariant - Contravariant metric tensor on every point of the grid described by grid_type
  real(DP),pointer  :: tensor_contravariant_error_upper(:,:,:,:) => null()
  real(DP),pointer  :: tensor_contravariant_error_lower(:,:,:,:) => null()
  integer :: tensor_contravariant_error_index=-999999999

  real(DP),pointer  :: g11_covariant(:,:) => null()     ! /g11_covariant - metric coefficients g11,  covariant metric tensor for the grid described by grid_type
  real(DP),pointer  :: g11_covariant_error_upper(:,:) => null()
  real(DP),pointer  :: g11_covariant_error_lower(:,:) => null()
  integer :: g11_covariant_error_index=-999999999

  real(DP),pointer  :: g12_covariant(:,:) => null()     ! /g12_covariant - metric coefficients g12,  covariant metric tensor for the grid described by grid_type
  real(DP),pointer  :: g12_covariant_error_upper(:,:) => null()
  real(DP),pointer  :: g12_covariant_error_lower(:,:) => null()
  integer :: g12_covariant_error_index=-999999999

  real(DP),pointer  :: g13_covariant(:,:) => null()     ! /g13_covariant - metric coefficients g13,  covariant metric tensor for the grid described by grid_type
  real(DP),pointer  :: g13_covariant_error_upper(:,:) => null()
  real(DP),pointer  :: g13_covariant_error_lower(:,:) => null()
  integer :: g13_covariant_error_index=-999999999

  real(DP),pointer  :: g22_covariant(:,:) => null()     ! /g22_covariant - metric coefficients g22,  covariant metric tensor for the grid described by grid_type
  real(DP),pointer  :: g22_covariant_error_upper(:,:) => null()
  real(DP),pointer  :: g22_covariant_error_lower(:,:) => null()
  integer :: g22_covariant_error_index=-999999999

  real(DP),pointer  :: g23_covariant(:,:) => null()     ! /g23_covariant - metric coefficients g23,  covariant metric tensor for the grid described by grid_type
  real(DP),pointer  :: g23_covariant_error_upper(:,:) => null()
  real(DP),pointer  :: g23_covariant_error_lower(:,:) => null()
  integer :: g23_covariant_error_index=-999999999

  real(DP),pointer  :: g33_covariant(:,:) => null()     ! /g33_covariant - metric coefficients g33,  covariant metric tensor for the grid described by grid_type
  real(DP),pointer  :: g33_covariant_error_upper(:,:) => null()
  real(DP),pointer  :: g33_covariant_error_lower(:,:) => null()
  integer :: g33_covariant_error_index=-999999999

  real(DP),pointer  :: g11_contravariant(:,:) => null()     ! /g11_contravariant - metric coefficients g11,  contravariant metric tensor for the grid described by grid_type
  real(DP),pointer  :: g11_contravariant_error_upper(:,:) => null()
  real(DP),pointer  :: g11_contravariant_error_lower(:,:) => null()
  integer :: g11_contravariant_error_index=-999999999

  real(DP),pointer  :: g12_contravariant(:,:) => null()     ! /g12_contravariant - metric coefficients g12,  contravariant metric tensor for the grid described by grid_type
  real(DP),pointer  :: g12_contravariant_error_upper(:,:) => null()
  real(DP),pointer  :: g12_contravariant_error_lower(:,:) => null()
  integer :: g12_contravariant_error_index=-999999999

  real(DP),pointer  :: g13_contravariant(:,:) => null()     ! /g13_contravariant - metric coefficients g13,  contravariant metric tensor for the grid described by grid_type
  real(DP),pointer  :: g13_contravariant_error_upper(:,:) => null()
  real(DP),pointer  :: g13_contravariant_error_lower(:,:) => null()
  integer :: g13_contravariant_error_index=-999999999

  real(DP),pointer  :: g22_contravariant(:,:) => null()     ! /g22_contravariant - metric coefficients g22,  contravariant metric tensor for the grid described by grid_type
  real(DP),pointer  :: g22_contravariant_error_upper(:,:) => null()
  real(DP),pointer  :: g22_contravariant_error_lower(:,:) => null()
  integer :: g22_contravariant_error_index=-999999999

  real(DP),pointer  :: g23_contravariant(:,:) => null()     ! /g23_contravariant - metric coefficients g23,  contravariant metric tensor for the grid described by grid_type
  real(DP),pointer  :: g23_contravariant_error_upper(:,:) => null()
  real(DP),pointer  :: g23_contravariant_error_lower(:,:) => null()
  integer :: g23_contravariant_error_index=-999999999

  real(DP),pointer  :: g33_contravariant(:,:) => null()     ! /g33_contravariant - metric coefficients g33,  contravariant metric tensor for the grid described by grid_type
  real(DP),pointer  :: g33_contravariant_error_upper(:,:) => null()
  real(DP),pointer  :: g33_contravariant_error_lower(:,:) => null()
  integer :: g33_contravariant_error_index=-999999999

endtype

type ids_delta_rzphi0d_static  !    Structure for R, Z, Phi relative positions (0D, static)
  real(DP)  :: delta_r=-9.0D40       ! /delta_r - Major radius (relative to a reference point)
  real(DP)  :: delta_r_error_upper=-9.0D40
  real(DP)  :: delta_r_error_lower=-9.0D40
  integer :: delta_r_error_index=-999999999

  real(DP)  :: delta_z=-9.0D40       ! /delta_z - Height (relative to a reference point)
  real(DP)  :: delta_z_error_upper=-9.0D40
  real(DP)  :: delta_z_error_lower=-9.0D40
  integer :: delta_z_error_index=-999999999

  real(DP)  :: delta_phi=-9.0D40       ! /delta_phi - Toroidal angle (relative to a reference point)
  real(DP)  :: delta_phi_error_upper=-9.0D40
  real(DP)  :: delta_phi_error_lower=-9.0D40
  integer :: delta_phi_error_index=-999999999

endtype

type ids_delta_rzphi1d_static  !    Structure for R, Z, Phi relative positions (1D, static)
  real(DP),pointer  :: delta_r(:) => null()     ! /delta_r - Major radii (relative to a reference point)
  real(DP),pointer  :: delta_r_error_upper(:) => null()
  real(DP),pointer  :: delta_r_error_lower(:) => null()
  integer :: delta_r_error_index=-999999999

  real(DP),pointer  :: delta_z(:) => null()     ! /delta_z - Heights (relative to a reference point)
  real(DP),pointer  :: delta_z_error_upper(:) => null()
  real(DP),pointer  :: delta_z_error_lower(:) => null()
  integer :: delta_z_error_index=-999999999

  real(DP),pointer  :: delta_phi(:) => null()     ! /delta_phi - Toroidal angles (relative to a reference point)
  real(DP),pointer  :: delta_phi_error_upper(:) => null()
  real(DP),pointer  :: delta_phi_error_lower(:) => null()
  integer :: delta_phi_error_index=-999999999

endtype

type ids_rzphi0d_static  !    Structure for R, Z, Phi positions (0D, static)
  real(DP)  :: r=-9.0D40       ! /r - Major radius
  real(DP)  :: r_error_upper=-9.0D40
  real(DP)  :: r_error_lower=-9.0D40
  integer :: r_error_index=-999999999

  real(DP)  :: z=-9.0D40       ! /z - Height
  real(DP)  :: z_error_upper=-9.0D40
  real(DP)  :: z_error_lower=-9.0D40
  integer :: z_error_index=-999999999

  real(DP)  :: phi=-9.0D40       ! /phi - Toroidal angle
  real(DP)  :: phi_error_upper=-9.0D40
  real(DP)  :: phi_error_lower=-9.0D40
  integer :: phi_error_index=-999999999

endtype

type ids_rzphi0d_dynamic_aos3  !    Structure for R, Z, Phi positions (0D, dynamic within a type 3 array of structure (index on time))
  real(DP)  :: r=-9.0D40       ! /r - Major radius
  real(DP)  :: r_error_upper=-9.0D40
  real(DP)  :: r_error_lower=-9.0D40
  integer :: r_error_index=-999999999

  real(DP)  :: z=-9.0D40       ! /z - Height
  real(DP)  :: z_error_upper=-9.0D40
  real(DP)  :: z_error_lower=-9.0D40
  integer :: z_error_index=-999999999

  real(DP)  :: phi=-9.0D40       ! /phi - Toroidal angle
  real(DP)  :: phi_error_upper=-9.0D40
  real(DP)  :: phi_error_lower=-9.0D40
  integer :: phi_error_index=-999999999

endtype

type ids_rzphi1d_static  !    Structure for list of R, Z, Phi positions (1D, static)
  real(DP),pointer  :: r(:) => null()     ! /r - Major radius
  real(DP),pointer  :: r_error_upper(:) => null()
  real(DP),pointer  :: r_error_lower(:) => null()
  integer :: r_error_index=-999999999

  real(DP),pointer  :: z(:) => null()     ! /z - Height
  real(DP),pointer  :: z_error_upper(:) => null()
  real(DP),pointer  :: z_error_lower(:) => null()
  integer :: z_error_index=-999999999

  real(DP),pointer  :: phi(:) => null()     ! /phi - Toroidal angle
  real(DP),pointer  :: phi_error_upper(:) => null()
  real(DP),pointer  :: phi_error_lower(:) => null()
  integer :: phi_error_index=-999999999

endtype

! SPECIAL STRUCTURE data / time
type ids_rzphi1d_dynamic_aos1_r  !    Major radius
  real(DP), pointer  :: data(:) => null()     ! /r - Major radius
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_rzphi1d_dynamic_aos1_z  !    Height
  real(DP), pointer  :: data(:) => null()     ! /z - Height
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_rzphi1d_dynamic_aos1_phi  !    Toroidal angle
  real(DP), pointer  :: data(:) => null()     ! /phi - Toroidal angle
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_rzphi1d_dynamic_aos1  !    Structure for list of R, Z, Phi positions (1D, dynamic within a type 1 array of structure (indexed on objects, data/time structure
  type (ids_rzphi1d_dynamic_aos1_r) :: r  ! /r - Major radius
  type (ids_rzphi1d_dynamic_aos1_z) :: z  ! /z - Height
  type (ids_rzphi1d_dynamic_aos1_phi) :: phi  ! /phi - Toroidal angle
endtype

type ids_rzphi1d_dynamic_aos3  !    Structure for R, Z, Phi positions (1D, dynamic within a type 3 array of structure)
  real(DP),pointer  :: r(:) => null()     ! /r - Major radius
  real(DP),pointer  :: r_error_upper(:) => null()
  real(DP),pointer  :: r_error_lower(:) => null()
  integer :: r_error_index=-999999999

  real(DP),pointer  :: z(:) => null()     ! /z - Height
  real(DP),pointer  :: z_error_upper(:) => null()
  real(DP),pointer  :: z_error_lower(:) => null()
  integer :: z_error_index=-999999999

  real(DP),pointer  :: phi(:) => null()     ! /phi - Toroidal angle
  real(DP),pointer  :: phi_error_upper(:) => null()
  real(DP),pointer  :: phi_error_lower(:) => null()
  integer :: phi_error_index=-999999999

endtype

type ids_rzphipsitheta1d_dynamic_aos3  !    Structure for R, Z, Phi, Psi, Theta positions (1D, dynamic within a type 3 array of structure)
  real(DP),pointer  :: r(:) => null()     ! /r - Major radius
  real(DP),pointer  :: r_error_upper(:) => null()
  real(DP),pointer  :: r_error_lower(:) => null()
  integer :: r_error_index=-999999999

  real(DP),pointer  :: z(:) => null()     ! /z - Height
  real(DP),pointer  :: z_error_upper(:) => null()
  real(DP),pointer  :: z_error_lower(:) => null()
  integer :: z_error_index=-999999999

  real(DP),pointer  :: phi(:) => null()     ! /phi - Toroidal angle
  real(DP),pointer  :: phi_error_upper(:) => null()
  real(DP),pointer  :: phi_error_lower(:) => null()
  integer :: phi_error_index=-999999999

  real(DP),pointer  :: psi(:) => null()     ! /psi - Poloidal flux
  real(DP),pointer  :: psi_error_upper(:) => null()
  real(DP),pointer  :: psi_error_lower(:) => null()
  integer :: psi_error_index=-999999999

  real(DP),pointer  :: theta(:) => null()     ! /theta - Poloidal angle
  real(DP),pointer  :: theta_error_upper(:) => null()
  real(DP),pointer  :: theta_error_lower(:) => null()
  integer :: theta_error_index=-999999999

endtype

type ids_rz1d_constant  !    Structure for list of R, Z positions (1D, constant)
  real(DP),pointer  :: r(:) => null()     ! /r - Major radius
  real(DP),pointer  :: r_error_upper(:) => null()
  real(DP),pointer  :: r_error_lower(:) => null()
  integer :: r_error_index=-999999999

  real(DP),pointer  :: z(:) => null()     ! /z - Height
  real(DP),pointer  :: z_error_upper(:) => null()
  real(DP),pointer  :: z_error_lower(:) => null()
  integer :: z_error_index=-999999999

endtype

type ids_rz0d_dynamic_aos  !    Structure for scalar R, Z positions, dynamic within a type 3 array of structure (index on time)
  real(DP)  :: r=-9.0D40       ! /r - Major radius
  real(DP)  :: r_error_upper=-9.0D40
  real(DP)  :: r_error_lower=-9.0D40
  integer :: r_error_index=-999999999

  real(DP)  :: z=-9.0D40       ! /z - Height
  real(DP)  :: z_error_upper=-9.0D40
  real(DP)  :: z_error_lower=-9.0D40
  integer :: z_error_index=-999999999

endtype

type ids_rz1d_dynamic_aos  !    Structure for list of R, Z positions (1D list of Npoints, dynamic within a type 3 array of structure (index on time))
  real(DP),pointer  :: r(:) => null()     ! /r - Major radius
  real(DP),pointer  :: r_error_upper(:) => null()
  real(DP),pointer  :: r_error_lower(:) => null()
  integer :: r_error_index=-999999999

  real(DP),pointer  :: z(:) => null()     ! /z - Height
  real(DP),pointer  :: z_error_upper(:) => null()
  real(DP),pointer  :: z_error_lower(:) => null()
  integer :: z_error_index=-999999999

endtype

type ids_rz1d_dynamic_2  !    Structure for list of R, Z positions (1D, dynamic), time assumed to be 2 levels above
  real(DP),pointer  :: r(:) => null()     ! /r - Major radius
  real(DP),pointer  :: r_error_upper(:) => null()
  real(DP),pointer  :: r_error_lower(:) => null()
  integer :: r_error_index=-999999999

  real(DP),pointer  :: z(:) => null()     ! /z - Height
  real(DP),pointer  :: z_error_upper(:) => null()
  real(DP),pointer  :: z_error_lower(:) => null()
  integer :: z_error_index=-999999999

endtype

type ids_rz1d_dynamic_1  !    Structure for list of R, Z positions (1D, dynamic), time assumed to be 1 level above
  real(DP),pointer  :: r(:) => null()     ! /r - Major radius
  real(DP),pointer  :: r_error_upper(:) => null()
  real(DP),pointer  :: r_error_lower(:) => null()
  integer :: r_error_index=-999999999

  real(DP),pointer  :: z(:) => null()     ! /z - Height
  real(DP),pointer  :: z_error_upper(:) => null()
  real(DP),pointer  :: z_error_lower(:) => null()
  integer :: z_error_index=-999999999

endtype

type ids_rz1d_static  !    Structure for list of R, Z positions (1D, constant)
  real(DP),pointer  :: r(:) => null()     ! /r - Major radius
  real(DP),pointer  :: r_error_upper(:) => null()
  real(DP),pointer  :: r_error_lower(:) => null()
  integer :: r_error_index=-999999999

  real(DP),pointer  :: z(:) => null()     ! /z - Height
  real(DP),pointer  :: z_error_upper(:) => null()
  real(DP),pointer  :: z_error_lower(:) => null()
  integer :: z_error_index=-999999999

endtype

type ids_rz0d_static  !    Structure for a single R, Z position (0D, static)
  real(DP)  :: r=-9.0D40       ! /r - Major radius
  real(DP)  :: r_error_upper=-9.0D40
  real(DP)  :: r_error_lower=-9.0D40
  integer :: r_error_index=-999999999

  real(DP)  :: z=-9.0D40       ! /z - Height
  real(DP)  :: z_error_upper=-9.0D40
  real(DP)  :: z_error_lower=-9.0D40
  integer :: z_error_index=-999999999

endtype

type ids_rz0d_constant  !    Structure for a single R, Z position (0D, constant)
  real(DP)  :: r=-9.0D40       ! /r - Major radius
  real(DP)  :: r_error_upper=-9.0D40
  real(DP)  :: r_error_lower=-9.0D40
  integer :: r_error_index=-999999999

  real(DP)  :: z=-9.0D40       ! /z - Height
  real(DP)  :: z_error_upper=-9.0D40
  real(DP)  :: z_error_lower=-9.0D40
  integer :: z_error_index=-999999999

endtype

type ids_line_of_sight_2points  !    Generic description of a line of sight, defined by two points
  type (ids_rzphi0d_static) :: first_point  ! /first_point - Position of the first point
  type (ids_rzphi0d_static) :: second_point  ! /second_point - Position of the second point
endtype

type ids_line_of_sight_2points_dynamic_aos3  !    Generic description of a line of sight, defined by two points, dynamic within a type 3 array of structure (index on time)
  type (ids_rzphi0d_dynamic_aos3) :: first_point  ! /first_point - Position of the first point
  type (ids_rzphi0d_dynamic_aos3) :: second_point  ! /second_point - Position of the second point
endtype

type ids_line_of_sight_3points  !    Generic description of a line of sight, defined by two points (one way) and an optional third point to indicate the direction of r
  type (ids_rzphi0d_static) :: first_point  ! /first_point - Position of the first point
  type (ids_rzphi0d_static) :: second_point  ! /second_point - Position of the second point
  type (ids_rzphi0d_static) :: third_point  ! /third_point - Position of the third point
endtype

type ids_oblique_static  !    Oblique description of a 2D object
  real(DP)  :: r=-9.0D40       ! /r - Geometric centre R
  real(DP)  :: r_error_upper=-9.0D40
  real(DP)  :: r_error_lower=-9.0D40
  integer :: r_error_index=-999999999

  real(DP)  :: z=-9.0D40       ! /z - Geometric centre Z
  real(DP)  :: z_error_upper=-9.0D40
  real(DP)  :: z_error_lower=-9.0D40
  integer :: z_error_index=-999999999

  real(DP)  :: length=-9.0D40       ! /length - Length
  real(DP)  :: length_error_upper=-9.0D40
  real(DP)  :: length_error_lower=-9.0D40
  integer :: length_error_index=-999999999

  real(DP)  :: thickness=-9.0D40       ! /thickness - Thickness
  real(DP)  :: thickness_error_upper=-9.0D40
  real(DP)  :: thickness_error_lower=-9.0D40
  integer :: thickness_error_index=-999999999

  real(DP)  :: alpha=-9.0D40       ! /alpha - Inclination of first angle TBD
  real(DP)  :: alpha_error_upper=-9.0D40
  real(DP)  :: alpha_error_lower=-9.0D40
  integer :: alpha_error_index=-999999999

  real(DP)  :: beta=-9.0D40       ! /beta - Inclination of second angle TBD
  real(DP)  :: beta_error_upper=-9.0D40
  real(DP)  :: beta_error_lower=-9.0D40
  integer :: beta_error_index=-999999999

endtype

type ids_rectangle_static  !    Rectangular description of a 2D object
  real(DP)  :: r=-9.0D40       ! /r - Geometric centre R
  real(DP)  :: r_error_upper=-9.0D40
  real(DP)  :: r_error_lower=-9.0D40
  integer :: r_error_index=-999999999

  real(DP)  :: z=-9.0D40       ! /z - Geometric centre Z
  real(DP)  :: z_error_upper=-9.0D40
  real(DP)  :: z_error_lower=-9.0D40
  integer :: z_error_index=-999999999

  real(DP)  :: width=-9.0D40       ! /width - Horizontal full width
  real(DP)  :: width_error_upper=-9.0D40
  real(DP)  :: width_error_lower=-9.0D40
  integer :: width_error_index=-999999999

  real(DP)  :: height=-9.0D40       ! /height - Vertical full height
  real(DP)  :: height_error_upper=-9.0D40
  real(DP)  :: height_error_lower=-9.0D40
  integer :: height_error_index=-999999999

endtype

type ids_outline_2d_geometry_static  !    Description of 2D geometry
  integer  :: geometry_type=-999999999       ! /geometry_type - Type used to describe the element shape (1:'outline', 2:'rectangle', 3:'oblique')
  type (ids_rz1d_static) :: outline  ! /outline - Irregular outline of the element
  type (ids_rectangle_static) :: rectangle  ! /rectangle - Rectangular description of the element
  type (ids_oblique_static) :: oblique  ! /oblique - Trapezoidal description of the element
endtype

type ids_waves_coherent_wave_identifier  !    Wave identifier
  type (ids_identifier) :: type  ! /type - Wave/antenna type. index=1 for name=EC; index=2 for name=LH; index=3 for name=IC
  character(len=132), dimension(:), pointer ::antenna_name => null()       ! /antenna_name - Name of the antenna that launches this wave. Corresponds to the name specified in antennas/ec(i)/nam
  integer  :: index_in_antenna=-999999999       ! /index_in_antenna - Index of the wave (starts at 1), separating different waves generated from a single antenna.
endtype

type ids_data_entry  !    Definition of a data entry
  character(len=132), dimension(:), pointer ::user => null()       ! /user - Username
  character(len=132), dimension(:), pointer ::machine => null()       ! /machine - Name of the experimental device to which this data is related
  character(len=132), dimension(:), pointer ::pulse_type => null()       ! /pulse_type - Type of the data entry, e.g. "pulse", "simulation", ...
  integer  :: pulse=-999999999       ! /pulse - Pulse number
  integer  :: run=-999999999       ! /run - Run number
endtype

type ids_plasma_composition  !    Generic declaration of Plasma Composition for a simulation
  type (ids_plasma_composition_ions),pointer :: ion(:) => null()  ! /plasma_composition/ion(i) - Array of plasma ions
endtype

type ids_code  !    Generic decription of the code specific parameters for the code that has produced this IDS
  character(len=132), dimension(:), pointer ::name => null()       ! /code/name - Name of the code
  character(len=132), dimension(:), pointer ::version => null()       ! /code/version - Version of the code
  character(len=132), dimension(:), pointer ::parameters => null()       ! /code/parameters - List of the code specific parameters in XML format
  integer,pointer  :: output_flag(:) => null()      ! /code/output_flag - Output flag : 0 means the run is successful, other values mean some difficulty has been encountered,
endtype

type ids_parameters_input  !    Code parameters block passed from the wrapper to the subroutine. Does not appear as such in the data structure. This is inserted i
  character(len=132), dimension(:), pointer ::parameters_value => null()       ! /parameters_input/parameters_value - Actual value of the code parameters (instance of Code_Parameters/Parameters in XML format)
  character(len=132), dimension(:), pointer ::parameters_default => null()       ! /parameters_input/parameters_default - Default value of the code parameters (instance of Code_Parameters/Parameters in XML format)
  character(len=132), dimension(:), pointer ::schema => null()       ! /parameters_input/schema - Code parameters schema
endtype

type ids_error_description  !    Error description, an array of this structure is passed as argument of the access layer calls (get and put) for handling errorbars
  integer  :: symmetric=-999999999       ! /error_description/symmetric - Flag indicating whether the error is “+/-“ symmetric (1) or not (0)
  type (ids_identifier) :: type  ! /error_description/type - Type of error bar description which is used (assumed to be identical for the lower and upper error):
  character(len=132), dimension(:), pointer ::expression_upper_0d => null()       ! /error_description/expression_upper_0d - Upper error expression (absolute value taken), for 1D dynamic quantities
  character(len=132), dimension(:), pointer ::expression_upper_1d => null()       ! /error_description/expression_upper_1d - Upper error expression (absolute value taken), for 2D dynamic quantities. If its dimension is equal
  character(len=132), dimension(:), pointer ::expression_lower_0d => null()       ! /error_description/expression_lower_0d - Lower error expression (absolute value taken), for 1D dynamic quantities
  character(len=132), dimension(:), pointer ::expression_lower_1d => null()       ! /error_description/expression_lower_1d - Lower error expression (absolute value taken), for 2D dynamic quantities. If its dimension is equal
endtype

type ids_ids_properties  !    Interface Data Structure properties. This element identifies the node above as an IDS
  character(len=132), dimension(:), pointer ::comment => null()       ! /ids_properties/comment - Any comment describing the content of this IDS
  integer  :: homogeneous_time=-999999999       ! /ids_properties/homogeneous_time - 1 if the time of this IDS is homogeneous. In this case, the time values for this IDS are stored in .
endtype


end module ! end of the utilities module

module ids_schemas       ! declaration of all IDSs

use ids_utilities

integer, parameter :: NON_TIMED=0
integer, parameter :: TIMED=1
integer, parameter :: TIMED_CLEAR=2


! ***********  Include actuator/dd_actuator.xsd
type ids_actuator  !    Generic simple description of a heating/current drive actuator, for a first simplified version of the Plasma Simulator component
  type (ids_ids_properties) :: ids_properties  ! /actuator/ids_properties -
  character(len=132), dimension(:), pointer ::name => null()       ! /actuator/name - Name of the actuator (IC, EC, NBI, LH)
  character(len=132), dimension(:), pointer ::channels => null()       ! /actuator/channels - ID of the multiple channels of the actuator: Beam boxes for NBI, EC or IC launcher, ...
  real(DP),pointer  :: power(:,:) => null()     ! /actuator/power - Power delivered at the output of the actuator, in the vessel (NB this is before the coupling / beam
  real(DP),pointer  :: power_error_upper(:,:) => null()
  real(DP),pointer  :: power_error_lower(:,:) => null()
  integer :: power_error_index=-999999999

  real(DP),pointer  :: generic_dynamic(:,:) => null()     ! /actuator/generic_dynamic - Generic 2D dynamic slot for storing an actuator control parameter (e.g. the angle of an ECRH mirror)
  real(DP),pointer  :: generic_dynamic_error_upper(:,:) => null()
  real(DP),pointer  :: generic_dynamic_error_lower(:,:) => null()
  integer :: generic_dynamic_error_index=-999999999

  type (ids_code) :: code  ! /actuator/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include antennas/dd_antennas.xsd
! SPECIAL STRUCTURE data / time
type ids_antennas_beam_spot_size  !    Size of the spot ellipse
  real(DP), pointer  :: data(:,:) => null()     ! /size - Size of the spot ellipse
  real(DP), pointer  :: data_error_upper(:,:) => null()
  real(DP), pointer  :: data_error_lower(:,:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_antennas_beam_spot_angle  !    Rotation angle for the spot ellipse
  real(DP), pointer  :: data(:) => null()     ! /angle - Rotation angle for the spot ellipse
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_antennas_beam_spot  !    Spot ellipse characteristics
  type (ids_antennas_beam_spot_size) :: size  ! /size - Size of the spot ellipse
  type (ids_antennas_beam_spot_angle) :: angle  ! /angle - Rotation angle for the spot ellipse
endtype

! SPECIAL STRUCTURE data / time
type ids_antennas_beam_phase_curvature  !    Inverse curvature radii for the phase ellipse, positive/negative for divergent/convergent beams
  real(DP), pointer  :: data(:,:) => null()     ! /curvature - Inverse curvature radii for the phase ellipse, positive/negative for divergent/convergent beams
  real(DP), pointer  :: data_error_upper(:,:) => null()
  real(DP), pointer  :: data_error_lower(:,:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_antennas_beam_phase_angle  !    Rotation angle for the phase ellipse
  real(DP), pointer  :: data(:) => null()     ! /angle - Rotation angle for the phase ellipse
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_antennas_beam_phase  !    Phase ellipse characteristics
  type (ids_antennas_beam_phase_curvature) :: curvature  ! /curvature - Inverse curvature radii for the phase ellipse, positive/negative for divergent/convergent beams
  type (ids_antennas_beam_phase_angle) :: angle  ! /angle - Rotation angle for the phase ellipse
endtype

type ids_antennas_beam  !    Beam characteristics
  type (ids_antennas_beam_spot) :: spot  ! /spot - Spot ellipse characteristics
  type (ids_antennas_beam_phase) :: phase  ! /phase - Phase ellipse characteristics
endtype

! SPECIAL STRUCTURE data / time
type ids_antennas_ec_power  !    Power
  real(DP), pointer  :: data(:) => null()     ! /power - Power
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_antennas_ec_mode  !    Incoming wave mode (+ or -1 for O/X mode)
  integer, pointer  :: data(:) => null()      ! /mode - Incoming wave mode (+ or -1 for O/X mode)
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_antennas_ec_launching_angle_pol  !    Poloidal launching angle between the horizontal plane and the poloidal component of the nominal beam centerline. tan(angle_pol)=-k
  real(DP), pointer  :: data(:) => null()     ! /launching_angle_pol - Poloidal launching angle between the horizontal plane and the poloidal component of the nominal beam
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_antennas_ec_launching_angle_tor  !    Toroidal launching angle between the poloidal plane and the nominal beam centerline. sin(angle_tor)=k_phi
  real(DP), pointer  :: data(:) => null()     ! /launching_angle_tor - Toroidal launching angle between the poloidal plane and the nominal beam centerline. sin(angle_tor)=
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_antennas_ec  !    Electron Cyclotron Antenna
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of the antenna
  real(DP)  :: frequency=-9.0D40       ! /frequency - Frequency
  real(DP)  :: frequency_error_upper=-9.0D40
  real(DP)  :: frequency_error_lower=-9.0D40
  integer :: frequency_error_index=-999999999

  type (ids_antennas_ec_power) :: power  ! /power - Power
  type (ids_antennas_ec_mode) :: mode  ! /mode - Incoming wave mode (+ or -1 for O/X mode)
  type (ids_rzphi1d_dynamic_aos1) :: launching_position  ! /launching_position - Launching position of the beam
  type (ids_antennas_ec_launching_angle_pol) :: launching_angle_pol  ! /launching_angle_pol - Poloidal launching angle between the horizontal plane and the poloidal component of the nominal beam
  type (ids_antennas_ec_launching_angle_tor) :: launching_angle_tor  ! /launching_angle_tor - Toroidal launching angle between the poloidal plane and the nominal beam centerline. sin(angle_tor)=
  type (ids_antennas_beam) :: beam  ! /beam - Beam characteristics
endtype

! SPECIAL STRUCTURE data / time
type ids_antennas_ic_strap_current  !    Root mean square current flowing along the strap
  real(DP), pointer  :: data(:) => null()     ! /current - Root mean square current flowing along the strap
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_antennas_ic_strap_phase  !    Phase of the strap current
  real(DP), pointer  :: data(:) => null()     ! /phase - Phase of the strap current
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_antennas_ic_strap  !    Properties of IC antenna strap
  type (ids_rzphi0d_static) :: position  ! /position - R, Z, Phi position of the strap centre
  real(DP)  :: width_tor=-9.0D40       ! /width_tor - Width of strap in the toroidal direction
  real(DP)  :: width_tor_error_upper=-9.0D40
  real(DP)  :: width_tor_error_lower=-9.0D40
  integer :: width_tor_error_index=-999999999

  real(DP)  :: distance_to_conductor=-9.0D40       ! /distance_to_conductor - Distance to conducting wall or other conductor behind the antenna strap
  real(DP)  :: distance_to_conductor_error_upper=-9.0D40
  real(DP)  :: distance_to_conductor_error_lower=-9.0D40
  integer :: distance_to_conductor_error_index=-999999999

  type (ids_outline_2d_geometry_static) :: geometry  ! /geometry - Cross-sectional shape of the strap
  type (ids_antennas_ic_strap_current) :: current  ! /current - Root mean square current flowing along the strap
  type (ids_antennas_ic_strap_phase) :: phase  ! /phase - Phase of the strap current
endtype

type ids_antennas_ic_surface_current  !    Description of the IC surface current on the antenna straps and on passive components.
  integer,pointer  :: m_pol(:) => null()      ! /m_pol - Poloidal mode numbers, used to describe the spectrum of the antenna current. The poloidal angle is d
  integer,pointer  :: n_tor(:) => null()      ! /n_tor - Toroidal mode numbers, used to describe the spectrum of the antenna current
  integer,pointer  :: spectrum(:) => null()      ! /spectrum - Spectrum of the total surface current on the antenna strap and passive components expressed in poloi
  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

! SPECIAL STRUCTURE data / time
type ids_antennas_ic_frequency  !    Frequency
  real(DP), pointer  :: data(:) => null()     ! /frequency - Frequency
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_antennas_ic_power  !    Power
  real(DP), pointer  :: data(:) => null()     ! /power - Power
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_antennas_ic  !    Ion Cyclotron Antenna
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of the antenna
  type (ids_antennas_ic_frequency) :: frequency  ! /frequency - Frequency
  type (ids_antennas_ic_power) :: power  ! /power - Power
  type (ids_antennas_ic_strap),pointer :: strap(:) => null()  ! /strap(i) - Properties of IC antenna straps
  type (ids_antennas_ic_surface_current),pointer :: surface_current(:) => null()  ! /surface_current(i) - Description of the IC surface current on the antenna straps and on passive components, for every tim
endtype

type ids_antennas  !    Antenna systems for heating and current drive in the electron cyclotron (EC) and ion cylcotron (IC) frequencies.
  type (ids_ids_properties) :: ids_properties  ! /antennas/ids_properties -
  type (ids_antennas_ec),pointer :: ec(:) => null()  ! /antennas/ec(i) - Electron Cyclotron antennas
  type (ids_rz0d_constant) :: reference_point  ! /antennas/reference_point - Reference point used to define the poloidal angle, e.g. the geometrical centre of the vacuum vessel.
  type (ids_antennas_ic),pointer :: ic(:) => null()  ! /antennas/ic(i) - Ion Cyclotron antennas
  type (ids_code) :: code  ! /antennas/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include atomic_data/dd_atomic_data.xsd
type ids_atomic_data_process_charge_state  !    Process tables for a given charge state. Only one table is used for that process, defined by process(:)/table_dimension
  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  real(DP)  :: z_min=-9.0D40       ! /z_min - Minimum Z of the charge state bundle
  real(DP)  :: z_min_error_upper=-9.0D40
  real(DP)  :: z_min_error_lower=-9.0D40
  integer :: z_min_error_index=-999999999

  real(DP)  :: z_max=-9.0D40       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(DP)  :: z_max_error_upper=-9.0D40
  real(DP)  :: z_max_error_lower=-9.0D40
  integer :: z_max_error_index=-999999999

  real(DP)  :: table_0d=-9.0D40       ! /table_0d - 0D table describing the process data
  real(DP)  :: table_0d_error_upper=-9.0D40
  real(DP)  :: table_0d_error_lower=-9.0D40
  integer :: table_0d_error_index=-999999999

  real(DP),pointer  :: table_1d(:) => null()     ! /table_1d - 1D table describing the process data
  real(DP),pointer  :: table_1d_error_upper(:) => null()
  real(DP),pointer  :: table_1d_error_lower(:) => null()
  integer :: table_1d_error_index=-999999999

  real(DP),pointer  :: table_2d(:,:) => null()     ! /table_2d - 2D table describing the process data
  real(DP),pointer  :: table_2d_error_upper(:,:) => null()
  real(DP),pointer  :: table_2d_error_lower(:,:) => null()
  integer :: table_2d_error_index=-999999999

  real(DP),pointer  :: table_3d(:,:,:) => null()     ! /table_3d - 3D table describing the process data
  real(DP),pointer  :: table_3d_error_upper(:,:,:) => null()
  real(DP),pointer  :: table_3d_error_lower(:,:,:) => null()
  integer :: table_3d_error_index=-999999999

  real(DP),pointer  :: table_4d(:,:,:,:) => null()     ! /table_4d - 4D table describing the process data
  real(DP),pointer  :: table_4d_error_upper(:,:,:,:) => null()
  real(DP),pointer  :: table_4d_error_lower(:,:,:,:) => null()
  integer :: table_4d_error_index=-999999999

  real(DP),pointer  :: table_5d(:,:,:,:,:) => null()     ! /table_5d - 5D table describing the process data
  real(DP),pointer  :: table_5d_error_upper(:,:,:,:,:) => null()
  real(DP),pointer  :: table_5d_error_lower(:,:,:,:,:) => null()
  integer :: table_5d_error_index=-999999999

  real(DP),pointer  :: table_6d(:,:,:,:,:,:) => null()     ! /table_6d - 6D table describing the process data
  real(DP),pointer  :: table_6d_error_upper(:,:,:,:,:,:) => null()
  real(DP),pointer  :: table_6d_error_lower(:,:,:,:,:,:) => null()
  integer :: table_6d_error_index=-999999999

endtype

type ids_atomic_data_process  !    Definition of a process and its data
  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying the process (e.g. EI, RC, ...)
  integer  :: table_dimension=-999999999       ! /table_dimension - Table dimensionality of the process (1 to 6), valid for all charge states. Indicates which of the ta
  integer  :: coordinate_index=-999999999       ! /coordinate_index - Index in tables_coord, specifying what coordinate systems to use for this process (valid for all tab
  character(len=132), dimension(:), pointer ::result_label => null()       ! /result_label - Description of the process result (rate, cross section, sputtering yield, ...)
  character(len=132), dimension(:), pointer ::result_units => null()       ! /result_units - Units of the process result
  integer  :: result_transformation=-999999999       ! /result_transformation - Transformation of the process result. Integer flag: 0=no transformation; 1=10^; 2=exp()
  type (ids_atomic_data_process_charge_state),pointer :: charge_state(:) => null()  ! /charge_state(i) - Process tables for a set of charge states. Only one table is used for that process, defined by proce
endtype

type ids_atomic_data_coordinate_system_coordinate  !    Description of a coordinate for atomic data tables. Can be either a range of real values or a set of discrete values (if interp_ty
  character(len=132), dimension(:), pointer ::label => null()       ! /label - Description of coordinate (e.g. "Electron temperature")
  real(DP),pointer  :: values(:) => null()     ! /values - Coordinate values
  real(DP),pointer  :: values_error_upper(:) => null()
  real(DP),pointer  :: values_error_lower(:) => null()
  integer :: values_error_index=-999999999

  integer  :: interpolation_type=-999999999       ! /interpolation_type - Interpolation strategy in this coordinate direction. Integer flag: 0=discrete (no interpolation); 1=
  integer,pointer  :: extrapolation_type(:) => null()      ! /extrapolation_type - Extrapolation strategy when leaving the domain. The first value of the vector describes the behaviou
  character(len=132), dimension(:), pointer ::value_labels => null()       ! /value_labels - String description of discrete coordinate values (if interpolation_type=0). E.g., for spectroscopic
  character(len=132), dimension(:), pointer ::units => null()       ! /units - Units of coordinate (e.g. eV)
  integer  :: transformation=-999999999       ! /transformation - Coordinate transformation applied to coordinate values stored in coord. Integer flag: 0=none; 1=log1
  integer  :: spacing=-999999999       ! /spacing - Flag for specific coordinate spacing (for optimization purposes). Integer flag: 0=undefined; 1=unifo
endtype

type ids_atomic_data_coordinate_system  !    Description of a coordinate system for atomic data tables
  type (ids_atomic_data_coordinate_system_coordinate),pointer :: coordinate(:) => null()  ! /coordinate(i) - Set of coordinates for that coordinate system. A coordinate an be either a range of real values or a
endtype

type ids_atomic_data  !    Atomic, molecular, nuclear and surface physics data. Each occurrence contains the atomic data for a given element (nuclear charge)
  type (ids_ids_properties) :: ids_properties  ! /atomic_data/ids_properties -
  real(DP)  :: z_n=-9.0D40       ! /atomic_data/z_n - Nuclear charge
  real(DP)  :: z_n_error_upper=-9.0D40
  real(DP)  :: z_n_error_lower=-9.0D40
  integer :: z_n_error_index=-999999999

  real(DP)  :: a=-9.0D40       ! /atomic_data/a - Mass of atom
  real(DP)  :: a_error_upper=-9.0D40
  real(DP)  :: a_error_lower=-9.0D40
  integer :: a_error_index=-999999999

  type (ids_atomic_data_process),pointer :: process(:) => null()  ! /atomic_data/process(i) - Description and data for a set of physical processes.
  type (ids_atomic_data_coordinate_system),pointer :: coordinate_system(:) => null()  ! /atomic_data/coordinate_system(i) - Array of possible coordinate systems for process tables
  type (ids_code) :: code  ! /atomic_data/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include charge_exchange/dd_charge_exchange.xsd
! SPECIAL STRUCTURE data / time
type ids_charge_exchange_channel_temperature  !    Ion temperature (of the emmitting impurity)
  real(DP), pointer  :: data(:) => null()     ! /temperature - Ion temperature (of the emmitting impurity)
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_charge_exchange_channel_velocity_tor  !    Toroidal velocity (of the emmitting impurity)
  real(DP), pointer  :: data(:) => null()     ! /velocity_tor - Toroidal velocity (of the emmitting impurity)
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_charge_exchange_channel_velocity_pol  !    Poloidal velocity (of the emmitting impurity)
  real(DP), pointer  :: data(:) => null()     ! /velocity_pol - Poloidal velocity (of the emmitting impurity)
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_charge_exchange_channel  !    Charge exchange channel
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of the channel
  character(len=132), dimension(:), pointer ::identifier => null()       ! /identifier - ID of the channel
  real(DP),pointer  :: a(:) => null()     ! /a - Mass of atom of the emmitting impurity. Varies according to channels since they are spanning differe
  real(DP),pointer  :: a_error_upper(:) => null()
  real(DP),pointer  :: a_error_lower(:) => null()
  integer :: a_error_index=-999999999

  real(DP),pointer  :: z_ion(:) => null()     ! /z_ion - Ion charge of the emmitting impurity. Varies according to channels since they are spanning different
  real(DP),pointer  :: z_ion_error_upper(:) => null()
  real(DP),pointer  :: z_ion_error_lower(:) => null()
  integer :: z_ion_error_index=-999999999

  real(DP),pointer  :: z_n(:) => null()     ! /z_n - Nuclear charge of the emmitting impurity. Varies according to channels since they are spanning diffe
  real(DP),pointer  :: z_n_error_upper(:) => null()
  real(DP),pointer  :: z_n_error_lower(:) => null()
  integer :: z_n_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying the emmitting impurity (e.g. H+, D+, T+, He+2, C+6, ...)
  type (ids_rzphi1d_dynamic_aos1) :: position  ! /position - Position of the measurements
  type (ids_charge_exchange_channel_temperature) :: temperature  ! /temperature - Ion temperature (of the emmitting impurity)
  type (ids_charge_exchange_channel_velocity_tor) :: velocity_tor  ! /velocity_tor - Toroidal velocity (of the emmitting impurity)
  type (ids_charge_exchange_channel_velocity_pol) :: velocity_pol  ! /velocity_pol - Poloidal velocity (of the emmitting impurity)
endtype

type ids_charge_exchange  !    Charge exchange spectroscopy diagnostic
  type (ids_ids_properties) :: ids_properties  ! /charge_exchange/ids_properties -
  type (ids_charge_exchange_channel),pointer :: channel(:) => null()  ! /charge_exchange/channel(i) - Set of channels (lines-of-sight)
  type (ids_code) :: code  ! /charge_exchange/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include controllers/dd_controllers.xsd
! SPECIAL STRUCTURE data / time
type ids_controllers_statespace_a  !    A matrix
  real(DP), pointer  :: data(:,:,:) => null()     ! /a - A matrix
  real(DP), pointer  :: data_error_upper(:,:,:) => null()
  real(DP), pointer  :: data_error_lower(:,:,:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_controllers_statespace_b  !    B matrix
  real(DP), pointer  :: data(:,:,:) => null()     ! /b - B matrix
  real(DP), pointer  :: data_error_upper(:,:,:) => null()
  real(DP), pointer  :: data_error_lower(:,:,:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_controllers_statespace_c  !    C matrix
  real(DP), pointer  :: data(:,:,:) => null()     ! /c - C matrix
  real(DP), pointer  :: data_error_upper(:,:,:) => null()
  real(DP), pointer  :: data_error_lower(:,:,:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_controllers_statespace_d  !    D matrix, normally proper and D=0
  real(DP), pointer  :: data(:,:,:) => null()     ! /d - D matrix, normally proper and D=0
  real(DP), pointer  :: data_error_upper(:,:,:) => null()
  real(DP), pointer  :: data_error_lower(:,:,:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_controllers_statespace_deltat  !    Discrete time sampling interval ; if less than 1e-10, the controller is considered to be expressed in continuous time
  real(DP), pointer  :: data(:) => null()     ! /deltat - Discrete time sampling interval ; if less than 1e-10, the controller is considered to be expressed i
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_controllers_statespace  !    type for a statespace controller
  character(len=132), dimension(:), pointer ::state_names => null()       ! /state_names - Names of the states
  type (ids_controllers_statespace_a) :: a  ! /a - A matrix
  type (ids_controllers_statespace_b) :: b  ! /b - B matrix
  type (ids_controllers_statespace_c) :: c  ! /c - C matrix
  type (ids_controllers_statespace_d) :: d  ! /d - D matrix, normally proper and D=0
  type (ids_controllers_statespace_deltat) :: deltat  ! /deltat - Discrete time sampling interval ; if less than 1e-10, the controller is considered to be expressed i
endtype

! SPECIAL STRUCTURE data / time
type ids_controllers_pid_p  !    Proportional term
  real(DP), pointer  :: data(:,:,:) => null()     ! /p - Proportional term
  real(DP), pointer  :: data_error_upper(:,:,:) => null()
  real(DP), pointer  :: data_error_lower(:,:,:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_controllers_pid_i  !    Integral term
  real(DP), pointer  :: data(:,:,:) => null()     ! /i - Integral term
  real(DP), pointer  :: data_error_upper(:,:,:) => null()
  real(DP), pointer  :: data_error_lower(:,:,:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_controllers_pid_d  !    Derivative term
  real(DP), pointer  :: data(:,:,:) => null()     ! /d - Derivative term
  real(DP), pointer  :: data_error_upper(:,:,:) => null()
  real(DP), pointer  :: data_error_lower(:,:,:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_controllers_pid_tau  !    Filter time-constant for the D-term
  real(DP), pointer  :: data(:) => null()     ! /tau - Filter time-constant for the D-term
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_controllers_pid  !    type for a MIMO PID controller
  type (ids_controllers_pid_p) :: p  ! /p - Proportional term
  type (ids_controllers_pid_i) :: i  ! /i - Integral term
  type (ids_controllers_pid_d) :: d  ! /d - Derivative term
  type (ids_controllers_pid_tau) :: tau  ! /tau - Filter time-constant for the D-term
endtype

! SPECIAL STRUCTURE data / time
type ids_controllers_linear_controller_inputs  !    Input signals; the timebase is common to inputs and outputs for any particular controller
  real(DP), pointer  :: data(:,:) => null()     ! /inputs - Input signals; the timebase is common to inputs and outputs for any particular controller
  real(DP), pointer  :: data_error_upper(:,:) => null()
  real(DP), pointer  :: data_error_lower(:,:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_controllers_linear_controller_outputs  !    Output signals; the timebase is common to inputs and outputs for any particular controller
  real(DP), pointer  :: data(:,:) => null()     ! /outputs - Output signals; the timebase is common to inputs and outputs for any particular controller
  real(DP), pointer  :: data_error_upper(:,:) => null()
  real(DP), pointer  :: data_error_lower(:,:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_controllers_linear_controller  !    type for a linear controller
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of this controller
  character(len=132), dimension(:), pointer ::description => null()       ! /description - Description of this controller
  character(len=132), dimension(:), pointer ::controller_class => null()       ! /controller_class - One of a known class of controllers
  character(len=132), dimension(:), pointer ::input_names => null()       ! /input_names - Names of the input signals, following the SDN convention
  character(len=132), dimension(:), pointer ::output_names => null()       ! /output_names - Names of the output signals following the SDN convention
  type (ids_controllers_statespace) :: statespace  ! /statespace - Statespace controller in discrete or continuous time
  type (ids_controllers_pid) :: pid  ! /pid - Filtered PID controller
  type (ids_controllers_linear_controller_inputs) :: inputs  ! /inputs - Input signals; the timebase is common to inputs and outputs for any particular controller
  type (ids_controllers_linear_controller_outputs) :: outputs  ! /outputs - Output signals; the timebase is common to inputs and outputs for any particular controller
endtype

! SPECIAL STRUCTURE data / time
type ids_controllers_nonlinear_controller_inputs  !    Input signals; the timebase is common  to inputs and outputs for any particular controller
  real(DP), pointer  :: data(:,:) => null()     ! /inputs - Input signals; the timebase is common  to inputs and outputs for any particular controller
  real(DP), pointer  :: data_error_upper(:,:) => null()
  real(DP), pointer  :: data_error_lower(:,:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_controllers_nonlinear_controller_outputs  !    Output signals; the timebase is common  to inputs and outputs for any particular controller
  real(DP), pointer  :: data(:,:) => null()     ! /outputs - Output signals; the timebase is common  to inputs and outputs for any particular controller
  real(DP), pointer  :: data_error_upper(:,:) => null()
  real(DP), pointer  :: data_error_lower(:,:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_controllers_nonlinear_controller  !    Type for a nonlinear controller
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of this controller
  character(len=132), dimension(:), pointer ::description => null()       ! /description - Description of this controller
  character(len=132), dimension(:), pointer ::controller_class => null()       ! /controller_class - One of a known class of controllers
  character(len=132), dimension(:), pointer ::input_names => null()       ! /input_names - Names of the input signals, following the SDN convention
  character(len=132), dimension(:), pointer ::output_names => null()       ! /output_names - Output signal names following the SDN convention
  character(len=132), dimension(:), pointer ::function => null()       ! /function - Method to be defined
  type (ids_controllers_nonlinear_controller_inputs) :: inputs  ! /inputs - Input signals; the timebase is common  to inputs and outputs for any particular controller
  type (ids_controllers_nonlinear_controller_outputs) :: outputs  ! /outputs - Output signals; the timebase is common  to inputs and outputs for any particular controller
endtype

type ids_controllers  !    Feedback and feedforward controllers
  type (ids_ids_properties) :: ids_properties  ! /controllers/ids_properties -
  type (ids_controllers_linear_controller),pointer :: linear_controller(:) => null()  ! /controllers/linear_controller(i) - A linear controller, this is rather conventional
  type (ids_controllers_nonlinear_controller),pointer :: nonlinear_controller(:) => null()  ! /controllers/nonlinear_controller(i) - A non-linear controller, this is less conventional and will have to be developed
  real(DP), pointer  :: time(:) => null()  ! time
  type (ids_code) :: code  ! /controllers/code -
endtype

! ***********  Include core_instant_changes/dd_core_instant_changes.xsd
type ids_core_instant_changes_change_profiles  !    instant_change terms for a given time slice
  type (ids_core_radial_grid) :: grid  ! /grid - Radial grid
  type (ids_core_profiles_profiles_1d_electrons) :: electrons  ! /electrons - Change of electrons-related quantities
  real(DP),pointer  :: t_i_average(:) => null()     ! /t_i_average - change of average ion temperature
  real(DP),pointer  :: t_i_average_error_upper(:) => null()
  real(DP),pointer  :: t_i_average_error_lower(:) => null()
  integer :: t_i_average_error_index=-999999999

  real(DP),pointer  :: momentum_tor(:) => null()     ! /momentum_tor - change of total toroidal momentum
  real(DP),pointer  :: momentum_tor_error_upper(:) => null()
  real(DP),pointer  :: momentum_tor_error_lower(:) => null()
  integer :: momentum_tor_error_index=-999999999

  type (ids_core_profile_ions),pointer :: ion(:) => null()  ! /ion(i) - changes related to the different ions species
  type (ids_core_profile_neutral),pointer :: neutral(:) => null()  ! /neutral(i) - changes related to the different neutral species
  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_core_instant_changes_change  !    instant_change terms for a given instant_change
  type (ids_identifier) :: identifier  ! /identifier - Instant change term identifier
  type (ids_core_instant_changes_change_profiles),pointer :: profiles_1d(:) => null()  ! /profiles_1d(i) - instant_change profiles for various time slices
endtype

type ids_core_instant_changes  !    Instant changes of the radial core plasma profiles due to pellet, MHD, ...
  type (ids_ids_properties) :: ids_properties  ! /core_instant_changes/ids_properties -
  type (ids_b_tor_vacuum_1) :: vacuum_toroidal_field  ! /core_instant_changes/vacuum_toroidal_field - Characteristics of the vacuum toroidal field (used in Rho_Tor definition and in the normalization of
  type (ids_core_instant_changes_change),pointer :: change(:) => null()  ! /core_instant_changes/change(i) - Set of instant change terms
  type (ids_code) :: code  ! /core_instant_changes/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include core_profiles/dd_core_profiles.xsd
type ids_core_profiles_global_quantities  !    Various global quantities calculated from the fields solved in the transport equations and from the Derived Profiles
  real(DP),pointer  :: ip(:) => null()     ! /ip - Total plasma current
  real(DP),pointer  :: ip_error_upper(:) => null()
  real(DP),pointer  :: ip_error_lower(:) => null()
  integer :: ip_error_index=-999999999

  real(DP),pointer  :: current_non_inductive(:) => null()     ! /current_non_inductive - Total non-inductive parallel current
  real(DP),pointer  :: current_non_inductive_error_upper(:) => null()
  real(DP),pointer  :: current_non_inductive_error_lower(:) => null()
  integer :: current_non_inductive_error_index=-999999999

  real(DP),pointer  :: current_bootstrap(:) => null()     ! /current_bootstrap - Bootstrap current
  real(DP),pointer  :: current_bootstrap_error_upper(:) => null()
  real(DP),pointer  :: current_bootstrap_error_lower(:) => null()
  integer :: current_bootstrap_error_index=-999999999

  real(DP),pointer  :: v_loop(:) => null()     ! /v_loop - LCFS loop voltage
  real(DP),pointer  :: v_loop_error_upper(:) => null()
  real(DP),pointer  :: v_loop_error_lower(:) => null()
  integer :: v_loop_error_index=-999999999

  real(DP),pointer  :: li(:) => null()     ! /li - Internal inductance. The li_3 definition is used, i.e. li_3 = 2/R0/mu0^2/Ip^2 * int(Bp^2 dV).
  real(DP),pointer  :: li_error_upper(:) => null()
  real(DP),pointer  :: li_error_lower(:) => null()
  integer :: li_error_index=-999999999

  real(DP),pointer  :: beta_tor(:) => null()     ! /beta_tor - Toroidal beta, defined as the volume-averaged total perpendicular pressure divided by (B0^2/(2*mu0))
  real(DP),pointer  :: beta_tor_error_upper(:) => null()
  real(DP),pointer  :: beta_tor_error_lower(:) => null()
  integer :: beta_tor_error_index=-999999999

  real(DP),pointer  :: beta_tor_norm(:) => null()     ! /beta_tor_norm - Normalised toroidal beta, defined as 100 * beta_tor * a[m] * B0 [T] / ip [MA]
  real(DP),pointer  :: beta_tor_norm_error_upper(:) => null()
  real(DP),pointer  :: beta_tor_norm_error_lower(:) => null()
  integer :: beta_tor_norm_error_index=-999999999

  real(DP),pointer  :: beta_pol(:) => null()     ! /beta_pol - Poloidal beta. Defined as betap = 4 int(p dV) / [R_0 * mu_0 * Ip^2]
  real(DP),pointer  :: beta_pol_error_upper(:) => null()
  real(DP),pointer  :: beta_pol_error_lower(:) => null()
  integer :: beta_pol_error_index=-999999999

  real(DP),pointer  :: energy_diamagnetic(:) => null()     ! /energy_diamagnetic - Plasma energy content = 3/2 * integral over the plasma volume of the total perpendicular pressure
  real(DP),pointer  :: energy_diamagnetic_error_upper(:) => null()
  real(DP),pointer  :: energy_diamagnetic_error_lower(:) => null()
  integer :: energy_diamagnetic_error_index=-999999999

endtype

type ids_core_profiles  !    Core plasma radial profiles
  type (ids_ids_properties) :: ids_properties  ! /core_profiles/ids_properties -
  type (ids_core_profiles_profiles_1d),pointer :: profiles_1d(:) => null()  ! /core_profiles/profiles_1d(i) - Core plasma radial profiles for various time slices
  type (ids_core_profiles_global_quantities) :: global_quantities  ! /core_profiles/global_quantities - Various global quantities derived from the profiles
  type (ids_b_tor_vacuum_1) :: vacuum_toroidal_field  ! /core_profiles/vacuum_toroidal_field - Characteristics of the vacuum toroidal field (used in rho_tor definition and in the normalization of
  type (ids_code) :: code  ! /core_profiles/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include core_sources/dd_core_sources.xsd
type ids_core_sources_source_profiles_1d_neutral_state  !    Source terms related to the a given state of the neutral species
  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying state
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  type (ids_identifier) :: neutral_type  ! /neutral_type - Neutral type (if the considered state is a neutral), in terms of energy. ID =1: cold; 2: thermal; 3:
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(DP),pointer  :: particles(:) => null()     ! /particles - Source term for the charge state density transport equation
  real(DP),pointer  :: particles_error_upper(:) => null()
  real(DP),pointer  :: particles_error_lower(:) => null()
  integer :: particles_error_index=-999999999

  real(DP),pointer  :: energy(:) => null()     ! /energy - Source terms for the charge state energy transport equation
  real(DP),pointer  :: energy_error_upper(:) => null()
  real(DP),pointer  :: energy_error_lower(:) => null()
  integer :: energy_error_index=-999999999

endtype

type ids_core_sources_source_profiles_1d_neutral  !    Source terms related to a given neutral species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying the neutral species (e.g. H, D, T, He, C, ...)
  integer  :: ion_index=-999999999       ! /ion_index - Index of the corresponding ion species in the ../../ion array
  real(DP),pointer  :: particles(:) => null()     ! /particles - Source term for ion density equation
  real(DP),pointer  :: particles_error_upper(:) => null()
  real(DP),pointer  :: particles_error_lower(:) => null()
  integer :: particles_error_index=-999999999

  real(DP),pointer  :: energy(:) => null()     ! /energy - Source term for the ion energy transport equation.
  real(DP),pointer  :: energy_error_upper(:) => null()
  real(DP),pointer  :: energy_error_lower(:) => null()
  integer :: energy_error_index=-999999999

  integer  :: multiple_states_flag=-999999999       ! /multiple_states_flag - Multiple states calculation flag : 0-Only one state is considered; 1-Multiple states are considered
  type (ids_core_sources_source_profiles_1d_neutral_state),pointer :: state(:) => null()  ! /state(i) - Source terms related to the different charge states of the species (energy, excitation, ...)
endtype

type ids_core_sources_source_profiles_1d_ions_charge_states  !    Source terms related to the a given state of the ion species
  real(DP)  :: z_min=-9.0D40       ! /z_min - Minimum Z of the charge state bundle
  real(DP)  :: z_min_error_upper=-9.0D40
  real(DP)  :: z_min_error_lower=-9.0D40
  integer :: z_min_error_index=-999999999

  real(DP)  :: z_max=-9.0D40       ! /z_max - Maximum Z of the charge state bundle
  real(DP)  :: z_max_error_upper=-9.0D40
  real(DP)  :: z_max_error_lower=-9.0D40
  integer :: z_max_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  integer  :: is_neutral=-999999999       ! /is_neutral - Flag specifying if this state corresponds to a neutral (1) or not (0)
  type (ids_identifier) :: neutral_type  ! /neutral_type - Neutral type (if the considered state is a neutral), in terms of energy. ID =1: cold; 2: thermal; 3:
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(DP),pointer  :: particles(:) => null()     ! /particles - Source term for the charge state density transport equation
  real(DP),pointer  :: particles_error_upper(:) => null()
  real(DP),pointer  :: particles_error_lower(:) => null()
  integer :: particles_error_index=-999999999

  real(DP),pointer  :: energy(:) => null()     ! /energy - Source terms for the charge state energy transport equation
  real(DP),pointer  :: energy_error_upper(:) => null()
  real(DP),pointer  :: energy_error_lower(:) => null()
  integer :: energy_error_index=-999999999

endtype

type ids_core_sources_source_profiles_1d_ions  !    Source terms related to a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(DP)  :: z_ion=-9.0D40       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed)
  real(DP)  :: z_ion_error_upper=-9.0D40
  real(DP)  :: z_ion_error_lower=-9.0D40
  integer :: z_ion_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  integer  :: neutral_index=-999999999       ! /neutral_index - Index of the corresponding neutral species in the ../../neutral array
  real(DP),pointer  :: particles(:) => null()     ! /particles - Source term for ion density equation
  real(DP),pointer  :: particles_error_upper(:) => null()
  real(DP),pointer  :: particles_error_lower(:) => null()
  integer :: particles_error_index=-999999999

  real(DP),pointer  :: energy(:) => null()     ! /energy - Source term for the ion energy transport equation.
  real(DP),pointer  :: energy_error_upper(:) => null()
  real(DP),pointer  :: energy_error_lower(:) => null()
  integer :: energy_error_index=-999999999

  integer  :: multiple_states_flag=-999999999       ! /multiple_states_flag - Multiple states calculation flag : 0-Only one state is considered; 1-Multiple states are considered
  type (ids_core_sources_source_profiles_1d_ions_charge_states),pointer :: state(:) => null()  ! /state(i) - Source terms related to the different charge states of the species (ionisation, energy, excitation,
endtype

type ids_core_sources_source_profiles_1d_electrons  !    Source terms related to electrons
  real(DP),pointer  :: particles(:) => null()     ! /particles - Source term for electron density equation
  real(DP),pointer  :: particles_error_upper(:) => null()
  real(DP),pointer  :: particles_error_lower(:) => null()
  integer :: particles_error_index=-999999999

  real(DP),pointer  :: energy(:) => null()     ! /energy - Source term for the electron energy equation
  real(DP),pointer  :: energy_error_upper(:) => null()
  real(DP),pointer  :: energy_error_lower(:) => null()
  integer :: energy_error_index=-999999999

endtype

type ids_core_sources_source_profiles_1d  !    Source terms for a given time slice
  type (ids_core_radial_grid) :: grid  ! /grid - Radial grid
  type (ids_core_sources_source_profiles_1d_electrons) :: electrons  ! /electrons - Sources for electrons
  real(DP),pointer  :: total_ion_energy(:) => null()     ! /total_ion_energy - Source term for the total (summed over ion  species) energy equation
  real(DP),pointer  :: total_ion_energy_error_upper(:) => null()
  real(DP),pointer  :: total_ion_energy_error_lower(:) => null()
  integer :: total_ion_energy_error_index=-999999999

  real(DP),pointer  :: momentum_tor(:) => null()     ! /momentum_tor - Source term for total toroidal momentum equation
  real(DP),pointer  :: momentum_tor_error_upper(:) => null()
  real(DP),pointer  :: momentum_tor_error_lower(:) => null()
  integer :: momentum_tor_error_index=-999999999

  real(DP),pointer  :: j_parallel(:) => null()     ! /j_parallel - Parallel current density source, average(J.B) / B0, where B0 = core_sources/vacuum_toroidal_field/b0
  real(DP),pointer  :: j_parallel_error_upper(:) => null()
  real(DP),pointer  :: j_parallel_error_lower(:) => null()
  integer :: j_parallel_error_index=-999999999

  real(DP),pointer  :: conductivity_parallel(:) => null()     ! /conductivity_parallel - Parallel conductivity due to this source
  real(DP),pointer  :: conductivity_parallel_error_upper(:) => null()
  real(DP),pointer  :: conductivity_parallel_error_lower(:) => null()
  integer :: conductivity_parallel_error_index=-999999999

  type (ids_core_sources_source_profiles_1d_ions),pointer :: ion(:) => null()  ! /ion(i) - Source terms related to the different ions species
  type (ids_core_sources_source_profiles_1d_neutral),pointer :: neutral(:) => null()  ! /neutral(i) - Source terms related to the different neutral species
  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_core_sources_source  !    Source terms for a given actuator
  type (ids_identifier) :: identifier  ! /identifier - Source term identifier
  type (ids_core_sources_source_profiles_1d),pointer :: profiles_1d(:) => null()  ! /profiles_1d(i) - Source profiles for various time slices
endtype

type ids_core_sources  !    Core plasma source terms (for the transport equations). Energy terms correspond to the full kinetic energy equation (i.e. the ener
  type (ids_ids_properties) :: ids_properties  ! /core_sources/ids_properties -
  type (ids_b_tor_vacuum_1) :: vacuum_toroidal_field  ! /core_sources/vacuum_toroidal_field - Characteristics of the vacuum toroidal field (used in Rho_Tor definition and in the normalization of
  type (ids_core_sources_source),pointer :: source(:) => null()  ! /core_sources/source(i) - Set of source terms
  type (ids_code) :: code  ! /core_sources/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include core_transport/dd_core_transport.xsd
type ids_core_transport_model_1_density  !    Transport coefficients for density equations. Coordinates one level above.
  real(DP),pointer  :: d(:) => null()     ! /d - Effective diffusivity
  real(DP),pointer  :: d_error_upper(:) => null()
  real(DP),pointer  :: d_error_lower(:) => null()
  integer :: d_error_index=-999999999

  real(DP),pointer  :: v(:) => null()     ! /v - Effective convection
  real(DP),pointer  :: v_error_upper(:) => null()
  real(DP),pointer  :: v_error_lower(:) => null()
  integer :: v_error_index=-999999999

  real(DP),pointer  :: flux(:) => null()     ! /flux - Flux
  real(DP),pointer  :: flux_error_upper(:) => null()
  real(DP),pointer  :: flux_error_lower(:) => null()
  integer :: flux_error_index=-999999999

endtype

type ids_core_transport_model_1_energy  !    Transport coefficients for energy equations. Coordinates one level above.
  real(DP),pointer  :: d(:) => null()     ! /d - Effective diffusivity
  real(DP),pointer  :: d_error_upper(:) => null()
  real(DP),pointer  :: d_error_lower(:) => null()
  integer :: d_error_index=-999999999

  real(DP),pointer  :: v(:) => null()     ! /v - Effective convection
  real(DP),pointer  :: v_error_upper(:) => null()
  real(DP),pointer  :: v_error_lower(:) => null()
  integer :: v_error_index=-999999999

  real(DP),pointer  :: flux(:) => null()     ! /flux - Flux
  real(DP),pointer  :: flux_error_upper(:) => null()
  real(DP),pointer  :: flux_error_lower(:) => null()
  integer :: flux_error_index=-999999999

endtype

type ids_core_transport_model_1_momentum  !    Transport coefficients for momentum equations. Coordinates one level above.
  real(DP),pointer  :: d(:) => null()     ! /d - Effective diffusivity
  real(DP),pointer  :: d_error_upper(:) => null()
  real(DP),pointer  :: d_error_lower(:) => null()
  integer :: d_error_index=-999999999

  real(DP),pointer  :: v(:) => null()     ! /v - Effective convection
  real(DP),pointer  :: v_error_upper(:) => null()
  real(DP),pointer  :: v_error_lower(:) => null()
  integer :: v_error_index=-999999999

  real(DP),pointer  :: flux(:) => null()     ! /flux - Flux
  real(DP),pointer  :: flux_error_upper(:) => null()
  real(DP),pointer  :: flux_error_lower(:) => null()
  integer :: flux_error_index=-999999999

endtype

type ids_core_transport_model_2_density  !    Transport coefficients for density equations. Coordinates two levels above.
  real(DP),pointer  :: d(:) => null()     ! /d - Effective diffusivity
  real(DP),pointer  :: d_error_upper(:) => null()
  real(DP),pointer  :: d_error_lower(:) => null()
  integer :: d_error_index=-999999999

  real(DP),pointer  :: v(:) => null()     ! /v - Effective convection
  real(DP),pointer  :: v_error_upper(:) => null()
  real(DP),pointer  :: v_error_lower(:) => null()
  integer :: v_error_index=-999999999

  real(DP),pointer  :: flux(:) => null()     ! /flux - Flux
  real(DP),pointer  :: flux_error_upper(:) => null()
  real(DP),pointer  :: flux_error_lower(:) => null()
  integer :: flux_error_index=-999999999

endtype

type ids_core_transport_model_2_energy  !    Transport coefficients for energy equations. Coordinates two levels above.
  real(DP),pointer  :: d(:) => null()     ! /d - Effective diffusivity
  real(DP),pointer  :: d_error_upper(:) => null()
  real(DP),pointer  :: d_error_lower(:) => null()
  integer :: d_error_index=-999999999

  real(DP),pointer  :: v(:) => null()     ! /v - Effective convection
  real(DP),pointer  :: v_error_upper(:) => null()
  real(DP),pointer  :: v_error_lower(:) => null()
  integer :: v_error_index=-999999999

  real(DP),pointer  :: flux(:) => null()     ! /flux - Flux
  real(DP),pointer  :: flux_error_upper(:) => null()
  real(DP),pointer  :: flux_error_lower(:) => null()
  integer :: flux_error_index=-999999999

endtype

type ids_core_transport_model_3_density  !    Transport coefficients for density equations. Coordinates three levels above.
  real(DP),pointer  :: d(:) => null()     ! /d - Effective diffusivity
  real(DP),pointer  :: d_error_upper(:) => null()
  real(DP),pointer  :: d_error_lower(:) => null()
  integer :: d_error_index=-999999999

  real(DP),pointer  :: v(:) => null()     ! /v - Effective convection
  real(DP),pointer  :: v_error_upper(:) => null()
  real(DP),pointer  :: v_error_lower(:) => null()
  integer :: v_error_index=-999999999

  real(DP),pointer  :: flux(:) => null()     ! /flux - Flux
  real(DP),pointer  :: flux_error_upper(:) => null()
  real(DP),pointer  :: flux_error_lower(:) => null()
  integer :: flux_error_index=-999999999

endtype

type ids_core_transport_model_3_energy  !    Transport coefficients for energy equations. Coordinates three levels above.
  real(DP),pointer  :: d(:) => null()     ! /d - Effective diffusivity
  real(DP),pointer  :: d_error_upper(:) => null()
  real(DP),pointer  :: d_error_lower(:) => null()
  integer :: d_error_index=-999999999

  real(DP),pointer  :: v(:) => null()     ! /v - Effective convection
  real(DP),pointer  :: v_error_upper(:) => null()
  real(DP),pointer  :: v_error_lower(:) => null()
  integer :: v_error_index=-999999999

  real(DP),pointer  :: flux(:) => null()     ! /flux - Flux
  real(DP),pointer  :: flux_error_upper(:) => null()
  real(DP),pointer  :: flux_error_lower(:) => null()
  integer :: flux_error_index=-999999999

endtype

type ids_core_transport_model_ions_charge_states  !    Transport coefficients related to the a given state of the ion species
  real(DP)  :: z_min=-9.0D40       ! /z_min - Minimum Z of the charge state bundle
  real(DP)  :: z_min_error_upper=-9.0D40
  real(DP)  :: z_min_error_lower=-9.0D40
  integer :: z_min_error_index=-999999999

  real(DP)  :: z_max=-9.0D40       ! /z_max - Maximum Z of the charge state bundle
  real(DP)  :: z_max_error_upper=-9.0D40
  real(DP)  :: z_max_error_lower=-9.0D40
  integer :: z_max_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  integer  :: is_neutral=-999999999       ! /is_neutral - Flag specifying if this state corresponds to a neutral (1) or not (0)
  type (ids_identifier) :: neutral_type  ! /neutral_type - Neutral type (if the considered state is a neutral), in terms of energy. ID =1: cold; 2: thermal; 3:
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  type (ids_core_transport_model_3_density) :: particles  ! /particles - Transport quantities related to density equation of the charge state considered (thermal+non-thermal
  type (ids_core_transport_model_3_energy) :: energy  ! /energy - Transport quantities related to the energy equation of the charge state considered
endtype

type ids_core_transport_model_neutral_state  !    Transport coefficients related to the a given state of the neutral species
  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying state
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  type (ids_core_transport_model_3_density) :: particles  ! /particles - Transport quantities related to density equation of the charge state considered (thermal+non-thermal
  type (ids_core_transport_model_3_energy) :: energy  ! /energy - Transport quantities related to the energy equation of the charge state considered
endtype

type ids_core_transport_model_ions  !    Transport coefficients related to a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(DP)  :: z_ion=-9.0D40       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed)
  real(DP)  :: z_ion_error_upper=-9.0D40
  real(DP)  :: z_ion_error_lower=-9.0D40
  integer :: z_ion_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  integer  :: neutral_index=-999999999       ! /neutral_index - Index of the corresponding neutral species in the ../../neutral array
  type (ids_core_transport_model_2_density) :: particles  ! /particles - Transport related to the ion density equation
  type (ids_core_transport_model_2_energy) :: energy  ! /energy - Transport coefficients related to the ion energy equation
  integer  :: multiple_states_flag=-999999999       ! /multiple_states_flag - Multiple states calculation flag : 0-Only one state is considered; 1-Multiple states are considered
  type (ids_core_transport_model_ions_charge_states),pointer :: state(:) => null()  ! /state(i) - Transport coefficients related to the different states of the species
endtype

type ids_core_transport_model_neutral  !    Transport coefficients related to a given neutral species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  integer  :: ion_index=-999999999       ! /ion_index - Index of the corresponding ion species in the ../../ion array
  type (ids_core_transport_model_2_density) :: particles  ! /particles - Transport related to the neutral density equation
  type (ids_core_transport_model_2_energy) :: energy  ! /energy - Transport coefficients related to the neutral energy equation
  integer  :: multiple_states_flag=-999999999       ! /multiple_states_flag - Multiple states calculation flag : 0-Only one state is considered; 1-Multiple states are considered
  type (ids_core_transport_model_neutral_state),pointer :: state(:) => null()  ! /state(i) - Transport coefficients related to the different states of the species
endtype

type ids_core_transport_model_electrons  !    Transport coefficients related to electrons
  type (ids_core_transport_model_2_density) :: particles  ! /particles - Transport quantities for the electron density equation
  type (ids_core_transport_model_2_energy) :: energy  ! /energy - Transport quantities for the electron energy equation
endtype

type ids_core_transport_model_profiles_1d  !    Transport coefficient profiles at a given time slice
  type (ids_core_radial_grid) :: grid_d  ! /grid_d - Grid for effective diffusivities and parallel conductivity
  type (ids_core_radial_grid) :: grid_v  ! /grid_v - Grid for effective convections
  type (ids_core_radial_grid) :: grid_flux  ! /grid_flux - Grid for fluxes
  real(DP),pointer  :: conductivity_parallel(:) => null()     ! /conductivity_parallel - Parallel conductivity
  real(DP),pointer  :: conductivity_parallel_error_upper(:) => null()
  real(DP),pointer  :: conductivity_parallel_error_lower(:) => null()
  integer :: conductivity_parallel_error_index=-999999999

  type (ids_core_transport_model_electrons) :: electrons  ! /electrons - Transport quantities related to the electrons
  type (ids_core_transport_model_1_energy) :: total_ion_energy  ! /total_ion_energy - Transport coefficients for the total (summed over ion  species) energy equation
  type (ids_core_transport_model_1_momentum) :: momentum_tor  ! /momentum_tor - Transport coefficients for total toroidal momentum equation
  type (ids_core_transport_model_ions),pointer :: ion(:) => null()  ! /ion(i) - Transport coefficients related to the various ion species
  type (ids_core_transport_model_neutral),pointer :: neutral(:) => null()  ! /neutral(i) - Transport coefficients related to the various neutral species
  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_core_transport_model  !    Transport coefficients for a given model
  type (ids_identifier) :: identifier  ! /identifier - Transport model identifier
  real(DP)  :: flux_multiplier=-9.0D40       ! /flux_multiplier - Multiplier applied to the particule flux when adding its contribution in the expression of the heat
  real(DP)  :: flux_multiplier_error_upper=-9.0D40
  real(DP)  :: flux_multiplier_error_lower=-9.0D40
  integer :: flux_multiplier_error_index=-999999999

  type (ids_core_transport_model_profiles_1d),pointer :: profiles_1d(:) => null()  ! /profiles_1d(i) - Transport coefficient profiles for various time slices
endtype

type ids_core_transport  !    Core plasma transport. Energy terms correspond to the full kinetic energy equation (i.e. the energy flux takes into account the en
  type (ids_ids_properties) :: ids_properties  ! /core_transport/ids_properties -
  type (ids_b_tor_vacuum_1) :: vacuum_toroidal_field  ! /core_transport/vacuum_toroidal_field - Characteristics of the vacuum toroidal field (used in Rho_Tor definition and in the normalization of
  type (ids_core_transport_model),pointer :: model(:) => null()  ! /core_transport/model(i) - Transport is described by a combination of various transport models
  type (ids_code) :: code  ! /core_transport/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include dataset_description/dd_dataset_description.xsd
type ids_dataset_description_simulation  !    Description of the general simulation characteristics, if this data entry has been produced by a simulation. Several nodes describ
  character(len=132), dimension(:), pointer ::comment_before => null()       ! /comment_before - Comment made when launching a simulation
  character(len=132), dimension(:), pointer ::comment_after => null()       ! /comment_after - Comment made at the end of a simulation
  real(DP)  :: time_begin=-9.0D40       ! /time_begin - Start time
  real(DP)  :: time_begin_error_upper=-9.0D40
  real(DP)  :: time_begin_error_lower=-9.0D40
  integer :: time_begin_error_index=-999999999

  real(DP)  :: time_step=-9.0D40       ! /time_step - Time interval between main steps, e.g. storage step (if relevant and constant)
  real(DP)  :: time_step_error_upper=-9.0D40
  real(DP)  :: time_step_error_lower=-9.0D40
  integer :: time_step_error_index=-999999999

  real(DP)  :: time_end=-9.0D40       ! /time_end - Stop time
  real(DP)  :: time_end_error_upper=-9.0D40
  real(DP)  :: time_end_error_lower=-9.0D40
  integer :: time_end_error_index=-999999999

  real(DP)  :: time_restart=-9.0D40       ! /time_restart - Time of the last restart done during the simulation
  real(DP)  :: time_restart_error_upper=-9.0D40
  real(DP)  :: time_restart_error_lower=-9.0D40
  integer :: time_restart_error_index=-999999999

  real(DP)  :: time_current=-9.0D40       ! /time_current - Current time of the simulation
  real(DP)  :: time_current_error_upper=-9.0D40
  real(DP)  :: time_current_error_lower=-9.0D40
  integer :: time_current_error_index=-999999999

  character(len=132), dimension(:), pointer ::time_begun => null()       ! /time_begun - Actual wall-clock time simulation started
  character(len=132), dimension(:), pointer ::time_ended => null()       ! /time_ended - Actual wall-clock time simulation finished
  character(len=132), dimension(:), pointer ::workflow => null()       ! /workflow - Description of the workflow which has been used to produce this data entry (e.g. copy of the Kepler
endtype

type ids_dataset_description  !    General description of the dataset (collection of all IDSs within the given database entry). Main description text to be put in id
  type (ids_ids_properties) :: ids_properties  ! /dataset_description/ids_properties -
  type (ids_data_entry) :: data_entry  ! /dataset_description/data_entry - Definition of this data entry
  type (ids_data_entry) :: parent_entry  ! /dataset_description/parent_entry - Definition of the parent data entry, if the present data entry has been generated by applying a give
  character(len=132), dimension(:), pointer ::imas_version => null()       ! /dataset_description/imas_version - Version of the IMAS infrastructure used to produce this data entry. Refers to the global IMAS reposi
  character(len=132), dimension(:), pointer ::dd_version => null()       ! /dataset_description/dd_version - Version of the physics data dictionary of this dataset
  type (ids_dataset_description_simulation) :: simulation  ! /dataset_description/simulation - Description of the general simulation characteristics, if this data entry has been produced by a sim
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include distribution_sources/dd_distribution_sources.xsd
type ids_distribution_sources_source_ggd  !    Source terms for a given time slice, using a GGD representation
  type (ids_generic_grid_dynamic) :: grid  ! /grid - Grid description
  type (ids_generic_grid_scalar),pointer :: particles(:) => null()  ! /particles(i) - Source density of particles in phase space, for various grid subsets
  integer,pointer  :: discrete(:) => null()      ! /discrete - List of indices of grid spaces (refers to ../grid/space) for which the source is discretely distribu
  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_distribution_sources_source_profiles_1d  !    Radial profile of source terms for a given time slice
  type (ids_core_radial_grid) :: grid  ! /grid - Radial grid
  real(DP),pointer  :: energy(:) => null()     ! /energy - Source term for the energy transport equation
  real(DP),pointer  :: energy_error_upper(:) => null()
  real(DP),pointer  :: energy_error_lower(:) => null()
  integer :: energy_error_index=-999999999

  real(DP),pointer  :: momentum_tor(:) => null()     ! /momentum_tor - Source term for the toroidal momentum equation
  real(DP),pointer  :: momentum_tor_error_upper(:) => null()
  real(DP),pointer  :: momentum_tor_error_lower(:) => null()
  integer :: momentum_tor_error_index=-999999999

  real(DP),pointer  :: particles(:) => null()     ! /particles - Source term for the density transport equation
  real(DP),pointer  :: particles_error_upper(:) => null()
  real(DP),pointer  :: particles_error_lower(:) => null()
  integer :: particles_error_index=-999999999

  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_distribution_sources_source_global_quantities  !    Global quantities of distribution_source for a given time slice
  real(DP)  :: power=-9.0D40       ! /power - Total power of the source
  real(DP)  :: power_error_upper=-9.0D40
  real(DP)  :: power_error_lower=-9.0D40
  integer :: power_error_index=-999999999

  real(DP)  :: torque_tor=-9.0D40       ! /torque_tor - Total toroidal torque of the source
  real(DP)  :: torque_tor_error_upper=-9.0D40
  real(DP)  :: torque_tor_error_lower=-9.0D40
  integer :: torque_tor_error_index=-999999999

  real(DP)  :: particles=-9.0D40       ! /particles - Particle source rate
  real(DP)  :: particles_error_upper=-9.0D40
  real(DP)  :: particles_error_lower=-9.0D40
  integer :: particles_error_index=-999999999

  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_distribution_sources_source  !    Source terms for a given actuator
  type (ids_distribution_process_identifier),pointer :: process(:) => null()  ! /process(i) - Set of processes (NBI units, fusion reactions, ...) that provide the source.
  integer  :: gyro_type=-999999999       ! /gyro_type - Defines how to interpret the spatial coordinates: 1 = given at the actual particle birth point; 2 =g
  type (ids_distribution_species) :: species  ! /species - Species injected or consumed by this source/sink
  type (ids_distribution_sources_source_global_quantities),pointer :: global_quantities(:) => null()  ! /global_quantities(i) - Global quantities for various time slices
  type (ids_distribution_sources_source_profiles_1d),pointer :: profiles_1d(:) => null()  ! /profiles_1d(i) - Source radial profiles (flux surface averaged quantities) for various time slices
  type (ids_distribution_sources_source_ggd),pointer :: ggd(:) => null()  ! /ggd(i) - Source terms in phase space (real space, velocity space, spin state), represented using the ggd, for
  type (ids_distribution_markers),pointer :: markers(:) => null()  ! /markers(i) - Source given as a group of markers (test particles) born per second, for various time slices
endtype

type ids_distribution_sources  !    Sources of particles for input to kinetic equations, e.g. Fokker-Planck calculation. The sources could originate from e.g. NBI or
  type (ids_ids_properties) :: ids_properties  ! /distribution_sources/ids_properties -
  type (ids_distribution_sources_source),pointer :: source(:) => null()  ! /distribution_sources/source(i) - Set of source/sink terms. A source/sink term corresponds to the particle source due to an NBI inject
  type (ids_b_tor_vacuum_1) :: vacuum_toroidal_field  ! /distribution_sources/vacuum_toroidal_field - Characteristics of the vacuum toroidal field (used in rho_tor definition)
  type (ids_rz1d_dynamic_1) :: magnetic_axis  ! /distribution_sources/magnetic_axis - Magnetic axis position (used to define a poloidal angle for the 2D profiles)
  type (ids_code) :: code  ! /distribution_sources/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include distributions/dd_distributions.xsd
type ids_distributions_d_ggd_expansion  !    Expansion of the distribution function for a given time slice, using a GGD representation
  type (ids_generic_grid_scalar),pointer :: grid_subset(:) => null()  ! /grid_subset(i) - Values of the distribution function expansion, for various grid subsets
endtype

type ids_distributions_d_ggd  !    Distribution function for a given time slice, using a GGD representation
  type (ids_generic_grid_dynamic) :: grid  ! /grid - Grid description
  real(DP),pointer  :: temperature(:) => null()     ! /temperature - Reference temperature profile used to define the local thermal energy and the thermal velocity (for
  real(DP),pointer  :: temperature_error_upper(:) => null()
  real(DP),pointer  :: temperature_error_lower(:) => null()
  integer :: temperature_error_index=-999999999

  type (ids_distributions_d_ggd_expansion),pointer :: expansion(:) => null()  ! /expansion(i) - Distribution function expanded into a vector of successive approximations. The first element in the
  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_distributions_d_source_identifier  !    Identifier of the source/sink term (wave or particle source process)
  type (ids_identifier_dynamic_aos3) :: type  ! /type - Type of the source term. Index  = 1 for a wave, index = 2 for a particle source process
  integer  :: wave_index=-999999999       ! /wave_index - Index into distribution/wave
  integer  :: process_index=-999999999       ! /process_index - Index into distribution/process
endtype

type ids_distributions_d_global_quantities_source  !    Global quantities for a given source/sink term
  type (ids_distributions_d_source_identifier) :: identifier  ! /identifier - Identifier of the wave or particle source process, defined respectively in distribution/wave or dist
  real(DP)  :: particles=-9.0D40       ! /particles - Particle source rate
  real(DP)  :: particles_error_upper=-9.0D40
  real(DP)  :: particles_error_lower=-9.0D40
  integer :: particles_error_index=-999999999

  real(DP)  :: power=-9.0D40       ! /power - Total power of the source
  real(DP)  :: power_error_upper=-9.0D40
  real(DP)  :: power_error_lower=-9.0D40
  integer :: power_error_index=-999999999

  real(DP)  :: torque_tor=-9.0D40       ! /torque_tor - Total toroidal torque of the source
  real(DP)  :: torque_tor_error_upper=-9.0D40
  real(DP)  :: torque_tor_error_lower=-9.0D40
  integer :: torque_tor_error_index=-999999999

endtype

type ids_distributions_d_global_quantities_collisions_ion_state  !    Global quantities for collisions with a given ion species state
  real(DP)  :: z_min=-9.0D40       ! /z_min - Minimum Z of the charge state bundle (z_min = z_max = 0 for a neutral)
  real(DP)  :: z_min_error_upper=-9.0D40
  real(DP)  :: z_min_error_lower=-9.0D40
  integer :: z_min_error_index=-999999999

  real(DP)  :: z_max=-9.0D40       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(DP)  :: z_max_error_upper=-9.0D40
  real(DP)  :: z_max_error_lower=-9.0D40
  integer :: z_max_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  real(DP)  :: power_thermal=-9.0D40       ! /power_thermal - Collisional power to the thermal particle population
  real(DP)  :: power_thermal_error_upper=-9.0D40
  real(DP)  :: power_thermal_error_lower=-9.0D40
  integer :: power_thermal_error_index=-999999999

  real(DP)  :: power_fast=-9.0D40       ! /power_fast - Collisional power to the fast particle population
  real(DP)  :: power_fast_error_upper=-9.0D40
  real(DP)  :: power_fast_error_lower=-9.0D40
  integer :: power_fast_error_index=-999999999

  real(DP)  :: torque_thermal_tor=-9.0D40       ! /torque_thermal_tor - Collisional toroidal torque to the thermal particle population
  real(DP)  :: torque_thermal_tor_error_upper=-9.0D40
  real(DP)  :: torque_thermal_tor_error_lower=-9.0D40
  integer :: torque_thermal_tor_error_index=-999999999

  real(DP)  :: torque_fast_tor=-9.0D40       ! /torque_fast_tor - Collisional toroidal torque to the fast particle population
  real(DP)  :: torque_fast_tor_error_upper=-9.0D40
  real(DP)  :: torque_fast_tor_error_lower=-9.0D40
  integer :: torque_fast_tor_error_index=-999999999

endtype

type ids_distributions_d_global_quantities_collisions_ion  !    Global quantities for collisions with a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(DP)  :: z_ion=-9.0D40       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed).
  real(DP)  :: z_ion_error_upper=-9.0D40
  real(DP)  :: z_ion_error_lower=-9.0D40
  integer :: z_ion_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H+, D+, T+, He+2, C+, D2, DT, CD4, ...)
  integer  :: neutral_index=-999999999       ! /neutral_index - Index of the corresponding neutral species in the ../../neutral array
  real(DP)  :: power_thermal=-9.0D40       ! /power_thermal - Collisional power to the thermal particle population
  real(DP)  :: power_thermal_error_upper=-9.0D40
  real(DP)  :: power_thermal_error_lower=-9.0D40
  integer :: power_thermal_error_index=-999999999

  real(DP)  :: power_fast=-9.0D40       ! /power_fast - Collisional power to the fast particle population
  real(DP)  :: power_fast_error_upper=-9.0D40
  real(DP)  :: power_fast_error_lower=-9.0D40
  integer :: power_fast_error_index=-999999999

  real(DP)  :: torque_thermal_tor=-9.0D40       ! /torque_thermal_tor - Collisional toroidal torque to the thermal particle population
  real(DP)  :: torque_thermal_tor_error_upper=-9.0D40
  real(DP)  :: torque_thermal_tor_error_lower=-9.0D40
  integer :: torque_thermal_tor_error_index=-999999999

  real(DP)  :: torque_fast_tor=-9.0D40       ! /torque_fast_tor - Collisional toroidal torque to the fast particle population
  real(DP)  :: torque_fast_tor_error_upper=-9.0D40
  real(DP)  :: torque_fast_tor_error_lower=-9.0D40
  integer :: torque_fast_tor_error_index=-999999999

  integer  :: multiple_states_flag=-999999999       ! /multiple_states_flag - Multiple state calculation flag : 0-Only one state is considered; 1-Multiple states are considered a
  type (ids_distributions_d_global_quantities_collisions_ion_state),pointer :: state(:) => null()  ! /state(i) - Collisional exchange with the various states of the ion species (ionisation, energy, excitation, ...
endtype

type ids_distributions_d_global_quantities_collisions_electrons  !    Global quantities for collisions with electrons
  real(DP)  :: power_thermal=-9.0D40       ! /power_thermal - Collisional power to the thermal particle population
  real(DP)  :: power_thermal_error_upper=-9.0D40
  real(DP)  :: power_thermal_error_lower=-9.0D40
  integer :: power_thermal_error_index=-999999999

  real(DP)  :: power_fast=-9.0D40       ! /power_fast - Collisional power to the fast particle population
  real(DP)  :: power_fast_error_upper=-9.0D40
  real(DP)  :: power_fast_error_lower=-9.0D40
  integer :: power_fast_error_index=-999999999

  real(DP)  :: torque_thermal_tor=-9.0D40       ! /torque_thermal_tor - Collisional toroidal torque to the thermal particle population
  real(DP)  :: torque_thermal_tor_error_upper=-9.0D40
  real(DP)  :: torque_thermal_tor_error_lower=-9.0D40
  integer :: torque_thermal_tor_error_index=-999999999

  real(DP)  :: torque_fast_tor=-9.0D40       ! /torque_fast_tor - Collisional toroidal torque to the fast particle population
  real(DP)  :: torque_fast_tor_error_upper=-9.0D40
  real(DP)  :: torque_fast_tor_error_lower=-9.0D40
  integer :: torque_fast_tor_error_index=-999999999

endtype

type ids_distributions_d_global_quantities_collisions  !    Global quantities for collisions
  type (ids_distributions_d_global_quantities_collisions_electrons) :: electrons  ! /electrons - Collisional exchange with electrons
  type (ids_distributions_d_global_quantities_collisions_ion),pointer :: ion(:) => null()  ! /ion(i) - Collisional exchange with the various ion species
endtype

type ids_distributions_d_global_quantities  !    Global quantities from the distribution, for a given time slice
  real(DP)  :: particles_n=-9.0D40       ! /particles_n - Number of particles in the distribution, i.e. the volume integral of the density (note: this is the
  real(DP)  :: particles_n_error_upper=-9.0D40
  real(DP)  :: particles_n_error_lower=-9.0D40
  integer :: particles_n_error_index=-999999999

  real(DP)  :: particles_fast_n=-9.0D40       ! /particles_fast_n - Number of fast particles in the distribution, i.e. the volume integral of the density (note: this is
  real(DP)  :: particles_fast_n_error_upper=-9.0D40
  real(DP)  :: particles_fast_n_error_lower=-9.0D40
  integer :: particles_fast_n_error_index=-999999999

  real(DP)  :: energy=-9.0D40       ! /energy - Total energy in the distribution
  real(DP)  :: energy_error_upper=-9.0D40
  real(DP)  :: energy_error_lower=-9.0D40
  integer :: energy_error_index=-999999999

  real(DP)  :: energy_fast=-9.0D40       ! /energy_fast - Total energy of the fast particles in the distribution
  real(DP)  :: energy_fast_error_upper=-9.0D40
  real(DP)  :: energy_fast_error_lower=-9.0D40
  integer :: energy_fast_error_index=-999999999

  real(DP)  :: energy_fast_parallel=-9.0D40       ! /energy_fast_parallel - Parallel energy of the fast particles in the distribution
  real(DP)  :: energy_fast_parallel_error_upper=-9.0D40
  real(DP)  :: energy_fast_parallel_error_lower=-9.0D40
  integer :: energy_fast_parallel_error_index=-999999999

  real(DP)  :: torque_tor_j_radial=-9.0D40       ! /torque_tor_j_radial - Toroidal torque due to radial currents
  real(DP)  :: torque_tor_j_radial_error_upper=-9.0D40
  real(DP)  :: torque_tor_j_radial_error_lower=-9.0D40
  integer :: torque_tor_j_radial_error_index=-999999999

  real(DP)  :: current_tor=-9.0D40       ! /current_tor - Toroidal current driven by the distribution
  real(DP)  :: current_tor_error_upper=-9.0D40
  real(DP)  :: current_tor_error_lower=-9.0D40
  integer :: current_tor_error_index=-999999999

  type (ids_distributions_d_global_quantities_collisions) :: collisions  ! /collisions - Power and torque exchanged between the species described by the distribution and the different plasm
  type (ids_distributions_d_global_quantities_source),pointer :: source(:) => null()  ! /source(i) - Set of volume integrated sources and sinks of particles, momentum and energy included in the Fokker-
  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_distributions_d_profiles_1d_thermalised  !    1D profiles of termalisation source/sinks
  real(DP),pointer  :: particles(:) => null()     ! /particles - Source rate of thermal particle density due to the thermalisation of fast particles
  real(DP),pointer  :: particles_error_upper(:) => null()
  real(DP),pointer  :: particles_error_lower(:) => null()
  integer :: particles_error_index=-999999999

  real(DP),pointer  :: energy(:) => null()     ! /energy - Source rate of energy density within the thermal particle population due to the thermalisation of fa
  real(DP),pointer  :: energy_error_upper(:) => null()
  real(DP),pointer  :: energy_error_lower(:) => null()
  integer :: energy_error_index=-999999999

  real(DP),pointer  :: momentum_tor(:) => null()     ! /momentum_tor - Source rate of toroidal angular momentum density within the thermal particle population due to the t
  real(DP),pointer  :: momentum_tor_error_upper(:) => null()
  real(DP),pointer  :: momentum_tor_error_lower(:) => null()
  integer :: momentum_tor_error_index=-999999999

endtype

type ids_distributions_d_profiles_1d_source  !    1D profiles for a given source/sink term
  type (ids_distributions_d_source_identifier) :: identifier  ! /identifier - Identifier of the wave or particle source process, defined respectively in distribution/wave or dist
  real(DP),pointer  :: particles(:) => null()     ! /particles - Source rate of thermal particle density
  real(DP),pointer  :: particles_error_upper(:) => null()
  real(DP),pointer  :: particles_error_lower(:) => null()
  integer :: particles_error_index=-999999999

  real(DP),pointer  :: energy(:) => null()     ! /energy - Source rate of energy density
  real(DP),pointer  :: energy_error_upper(:) => null()
  real(DP),pointer  :: energy_error_lower(:) => null()
  integer :: energy_error_index=-999999999

  real(DP),pointer  :: momentum_tor(:) => null()     ! /momentum_tor - Source rate of toroidal angular momentum density
  real(DP),pointer  :: momentum_tor_error_upper(:) => null()
  real(DP),pointer  :: momentum_tor_error_lower(:) => null()
  integer :: momentum_tor_error_index=-999999999

endtype

type ids_distributions_d_profiles_1d_partial_source  !    1D profiles for a given source/sink term
  type (ids_distributions_d_source_identifier) :: identifier  ! /identifier - Identifier of the wave or particle source process, defined respectively in distribution/wave or dist
  real(DP),pointer  :: particles(:) => null()     ! /particles - Source rate of thermal particle density
  real(DP),pointer  :: particles_error_upper(:) => null()
  real(DP),pointer  :: particles_error_lower(:) => null()
  integer :: particles_error_index=-999999999

  real(DP),pointer  :: energy(:) => null()     ! /energy - Source rate of energy density
  real(DP),pointer  :: energy_error_upper(:) => null()
  real(DP),pointer  :: energy_error_lower(:) => null()
  integer :: energy_error_index=-999999999

  real(DP),pointer  :: momentum_tor(:) => null()     ! /momentum_tor - Source rate of toroidal angular momentum density
  real(DP),pointer  :: momentum_tor_error_upper(:) => null()
  real(DP),pointer  :: momentum_tor_error_lower(:) => null()
  integer :: momentum_tor_error_index=-999999999

endtype

type ids_distributions_d_profiles_2d_collisions_ion  !    2D profiles for collisions with a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(DP)  :: z_ion=-9.0D40       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed).
  real(DP)  :: z_ion_error_upper=-9.0D40
  real(DP)  :: z_ion_error_lower=-9.0D40
  integer :: z_ion_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H+, D+, T+, He+2, C+, D2, DT, CD4, ...)
  integer  :: neutral_index=-999999999       ! /neutral_index - Index of the corresponding neutral species in the ../../neutral array
  real(DP),pointer  :: power_thermal(:,:) => null()     ! /power_thermal - Collisional power density to the thermal particle population
  real(DP),pointer  :: power_thermal_error_upper(:,:) => null()
  real(DP),pointer  :: power_thermal_error_lower(:,:) => null()
  integer :: power_thermal_error_index=-999999999

  real(DP),pointer  :: power_fast(:,:) => null()     ! /power_fast - Collisional power density to the fast particle population
  real(DP),pointer  :: power_fast_error_upper(:,:) => null()
  real(DP),pointer  :: power_fast_error_lower(:,:) => null()
  integer :: power_fast_error_index=-999999999

  real(DP),pointer  :: torque_thermal_tor(:,:) => null()     ! /torque_thermal_tor - Collisional toroidal torque density to the thermal particle population
  real(DP),pointer  :: torque_thermal_tor_error_upper(:,:) => null()
  real(DP),pointer  :: torque_thermal_tor_error_lower(:,:) => null()
  integer :: torque_thermal_tor_error_index=-999999999

  real(DP),pointer  :: torque_fast_tor(:,:) => null()     ! /torque_fast_tor - Collisional toroidal torque density to the fast particle population
  real(DP),pointer  :: torque_fast_tor_error_upper(:,:) => null()
  real(DP),pointer  :: torque_fast_tor_error_lower(:,:) => null()
  integer :: torque_fast_tor_error_index=-999999999

  integer  :: multiple_states_flag=-999999999       ! /multiple_states_flag - Multiple state calculation flag : 0-Only one state is considered; 1-Multiple states are considered a
  type (ids_distributions_d_profiles_2d_collisions_ion_state),pointer :: state(:) => null()  ! /state(i) - Collisional exchange with the various states of the ion species (ionisation, energy, excitation, ...
endtype

type ids_distributions_d_profiles_2d_partial_collisions_ion  !    2D profiles for collisions with a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(DP)  :: z_ion=-9.0D40       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed).
  real(DP)  :: z_ion_error_upper=-9.0D40
  real(DP)  :: z_ion_error_lower=-9.0D40
  integer :: z_ion_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H+, D+, T+, He+2, C+, D2, DT, CD4, ...)
  integer  :: neutral_index=-999999999       ! /neutral_index - Index of the corresponding neutral species in the ../../neutral array
  real(DP),pointer  :: power_thermal(:,:) => null()     ! /power_thermal - Collisional power density to the thermal particle population
  real(DP),pointer  :: power_thermal_error_upper(:,:) => null()
  real(DP),pointer  :: power_thermal_error_lower(:,:) => null()
  integer :: power_thermal_error_index=-999999999

  real(DP),pointer  :: power_fast(:,:) => null()     ! /power_fast - Collisional power density to the fast particle population
  real(DP),pointer  :: power_fast_error_upper(:,:) => null()
  real(DP),pointer  :: power_fast_error_lower(:,:) => null()
  integer :: power_fast_error_index=-999999999

  real(DP),pointer  :: torque_thermal_tor(:,:) => null()     ! /torque_thermal_tor - Collisional toroidal torque density to the thermal particle population
  real(DP),pointer  :: torque_thermal_tor_error_upper(:,:) => null()
  real(DP),pointer  :: torque_thermal_tor_error_lower(:,:) => null()
  integer :: torque_thermal_tor_error_index=-999999999

  real(DP),pointer  :: torque_fast_tor(:,:) => null()     ! /torque_fast_tor - Collisional toroidal torque density to the fast particle population
  real(DP),pointer  :: torque_fast_tor_error_upper(:,:) => null()
  real(DP),pointer  :: torque_fast_tor_error_lower(:,:) => null()
  integer :: torque_fast_tor_error_index=-999999999

  integer  :: multiple_states_flag=-999999999       ! /multiple_states_flag - Multiple state calculation flag : 0-Only one state is considered; 1-Multiple states are considered a
  type (ids_distributions_d_profiles_2d_partial_collisions_ion_state),pointer :: state(:) => null()  ! /state(i) - Collisional exchange with the various states of the ion species (ionisation, energy, excitation, ...
endtype

type ids_distributions_d_profiles_2d_collisions_ion_state  !    2D profiles for collisions with a given ion species state
  real(DP)  :: z_min=-9.0D40       ! /z_min - Minimum Z of the charge state bundle (z_min = z_max = 0 for a neutral)
  real(DP)  :: z_min_error_upper=-9.0D40
  real(DP)  :: z_min_error_lower=-9.0D40
  integer :: z_min_error_index=-999999999

  real(DP)  :: z_max=-9.0D40       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(DP)  :: z_max_error_upper=-9.0D40
  real(DP)  :: z_max_error_lower=-9.0D40
  integer :: z_max_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  real(DP),pointer  :: power_thermal(:,:) => null()     ! /power_thermal - Collisional power density to the thermal particle population
  real(DP),pointer  :: power_thermal_error_upper(:,:) => null()
  real(DP),pointer  :: power_thermal_error_lower(:,:) => null()
  integer :: power_thermal_error_index=-999999999

  real(DP),pointer  :: power_fast(:,:) => null()     ! /power_fast - Collisional power density to the fast particle population
  real(DP),pointer  :: power_fast_error_upper(:,:) => null()
  real(DP),pointer  :: power_fast_error_lower(:,:) => null()
  integer :: power_fast_error_index=-999999999

  real(DP),pointer  :: torque_thermal_tor(:,:) => null()     ! /torque_thermal_tor - Collisional toroidal torque density to the thermal particle population
  real(DP),pointer  :: torque_thermal_tor_error_upper(:,:) => null()
  real(DP),pointer  :: torque_thermal_tor_error_lower(:,:) => null()
  integer :: torque_thermal_tor_error_index=-999999999

  real(DP),pointer  :: torque_fast_tor(:,:) => null()     ! /torque_fast_tor - Collisional toroidal torque density to the fast particle population
  real(DP),pointer  :: torque_fast_tor_error_upper(:,:) => null()
  real(DP),pointer  :: torque_fast_tor_error_lower(:,:) => null()
  integer :: torque_fast_tor_error_index=-999999999

endtype

type ids_distributions_d_profiles_2d_partial_collisions_ion_state  !    2D profiles for collisions with a given ion species state
  real(DP)  :: z_min=-9.0D40       ! /z_min - Minimum Z of the charge state bundle (z_min = z_max = 0 for a neutral)
  real(DP)  :: z_min_error_upper=-9.0D40
  real(DP)  :: z_min_error_lower=-9.0D40
  integer :: z_min_error_index=-999999999

  real(DP)  :: z_max=-9.0D40       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(DP)  :: z_max_error_upper=-9.0D40
  real(DP)  :: z_max_error_lower=-9.0D40
  integer :: z_max_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  real(DP),pointer  :: power_thermal(:,:) => null()     ! /power_thermal - Collisional power density to the thermal particle population
  real(DP),pointer  :: power_thermal_error_upper(:,:) => null()
  real(DP),pointer  :: power_thermal_error_lower(:,:) => null()
  integer :: power_thermal_error_index=-999999999

  real(DP),pointer  :: power_fast(:,:) => null()     ! /power_fast - Collisional power density to the fast particle population
  real(DP),pointer  :: power_fast_error_upper(:,:) => null()
  real(DP),pointer  :: power_fast_error_lower(:,:) => null()
  integer :: power_fast_error_index=-999999999

  real(DP),pointer  :: torque_thermal_tor(:,:) => null()     ! /torque_thermal_tor - Collisional toroidal torque density to the thermal particle population
  real(DP),pointer  :: torque_thermal_tor_error_upper(:,:) => null()
  real(DP),pointer  :: torque_thermal_tor_error_lower(:,:) => null()
  integer :: torque_thermal_tor_error_index=-999999999

  real(DP),pointer  :: torque_fast_tor(:,:) => null()     ! /torque_fast_tor - Collisional toroidal torque density to the fast particle population
  real(DP),pointer  :: torque_fast_tor_error_upper(:,:) => null()
  real(DP),pointer  :: torque_fast_tor_error_lower(:,:) => null()
  integer :: torque_fast_tor_error_index=-999999999

endtype

type ids_distributions_d_profiles_2d_collisions_electrons  !    2D profiles for collisions with electrons
  real(DP),pointer  :: power_thermal(:,:) => null()     ! /power_thermal - Collisional power density to the thermal particle population
  real(DP),pointer  :: power_thermal_error_upper(:,:) => null()
  real(DP),pointer  :: power_thermal_error_lower(:,:) => null()
  integer :: power_thermal_error_index=-999999999

  real(DP),pointer  :: power_fast(:,:) => null()     ! /power_fast - Collisional power density to the fast particle population
  real(DP),pointer  :: power_fast_error_upper(:,:) => null()
  real(DP),pointer  :: power_fast_error_lower(:,:) => null()
  integer :: power_fast_error_index=-999999999

  real(DP),pointer  :: torque_thermal_tor(:,:) => null()     ! /torque_thermal_tor - Collisional toroidal torque density to the thermal particle population
  real(DP),pointer  :: torque_thermal_tor_error_upper(:,:) => null()
  real(DP),pointer  :: torque_thermal_tor_error_lower(:,:) => null()
  integer :: torque_thermal_tor_error_index=-999999999

  real(DP),pointer  :: torque_fast_tor(:,:) => null()     ! /torque_fast_tor - Collisional toroidal torque density to the fast particle population
  real(DP),pointer  :: torque_fast_tor_error_upper(:,:) => null()
  real(DP),pointer  :: torque_fast_tor_error_lower(:,:) => null()
  integer :: torque_fast_tor_error_index=-999999999

endtype

type ids_distributions_d_profiles_2d_partial_collisions_electrons  !    2D profiles for collisions with electrons
  real(DP),pointer  :: power_thermal(:,:) => null()     ! /power_thermal - Collisional power density to the thermal particle population
  real(DP),pointer  :: power_thermal_error_upper(:,:) => null()
  real(DP),pointer  :: power_thermal_error_lower(:,:) => null()
  integer :: power_thermal_error_index=-999999999

  real(DP),pointer  :: power_fast(:,:) => null()     ! /power_fast - Collisional power density to the fast particle population
  real(DP),pointer  :: power_fast_error_upper(:,:) => null()
  real(DP),pointer  :: power_fast_error_lower(:,:) => null()
  integer :: power_fast_error_index=-999999999

  real(DP),pointer  :: torque_thermal_tor(:,:) => null()     ! /torque_thermal_tor - Collisional toroidal torque density to the thermal particle population
  real(DP),pointer  :: torque_thermal_tor_error_upper(:,:) => null()
  real(DP),pointer  :: torque_thermal_tor_error_lower(:,:) => null()
  integer :: torque_thermal_tor_error_index=-999999999

  real(DP),pointer  :: torque_fast_tor(:,:) => null()     ! /torque_fast_tor - Collisional toroidal torque density to the fast particle population
  real(DP),pointer  :: torque_fast_tor_error_upper(:,:) => null()
  real(DP),pointer  :: torque_fast_tor_error_lower(:,:) => null()
  integer :: torque_fast_tor_error_index=-999999999

endtype

type ids_distributions_d_profiles_2d_collisions  !    2D profiles for collisions
  type (ids_distributions_d_profiles_2d_collisions_electrons) :: electrons  ! /electrons - Collisional exchange with electrons
  type (ids_distributions_d_profiles_2d_collisions_ion),pointer :: ion(:) => null()  ! /ion(i) - Collisional exchange with the various ion species
endtype

type ids_distributions_d_profiles_2d_partial_collisions  !    2D profiles for collisions
  type (ids_distributions_d_profiles_2d_partial_collisions_electrons) :: electrons  ! /electrons - Collisional exchange with electrons
  type (ids_distributions_d_profiles_2d_partial_collisions_ion),pointer :: ion(:) => null()  ! /ion(i) - Collisional exchange with the various ion species
endtype

type ids_distributions_d_profiles_2d_partial  !    2D profiles from specific particles in the distribution (trapped, co or counter-passing)
  real(DP),pointer  :: density(:,:) => null()     ! /density - Density (thermal+non-thermal)
  real(DP),pointer  :: density_error_upper(:,:) => null()
  real(DP),pointer  :: density_error_lower(:,:) => null()
  integer :: density_error_index=-999999999

  real(DP),pointer  :: density_fast(:,:) => null()     ! /density_fast - Density of fast particles
  real(DP),pointer  :: density_fast_error_upper(:,:) => null()
  real(DP),pointer  :: density_fast_error_lower(:,:) => null()
  integer :: density_fast_error_index=-999999999

  real(DP),pointer  :: pressure(:,:) => null()     ! /pressure - Pressure (thermal+non-thermal)
  real(DP),pointer  :: pressure_error_upper(:,:) => null()
  real(DP),pointer  :: pressure_error_lower(:,:) => null()
  integer :: pressure_error_index=-999999999

  real(DP),pointer  :: pressure_fast(:,:) => null()     ! /pressure_fast - Pressure of fast particles
  real(DP),pointer  :: pressure_fast_error_upper(:,:) => null()
  real(DP),pointer  :: pressure_fast_error_lower(:,:) => null()
  integer :: pressure_fast_error_index=-999999999

  real(DP),pointer  :: pressure_fast_parallel(:,:) => null()     ! /pressure_fast_parallel - Pressure of fast particles in the parallel direction
  real(DP),pointer  :: pressure_fast_parallel_error_upper(:,:) => null()
  real(DP),pointer  :: pressure_fast_parallel_error_lower(:,:) => null()
  integer :: pressure_fast_parallel_error_index=-999999999

  real(DP),pointer  :: current_tor(:,:) => null()     ! /current_tor - Total toroidal driven current density (including electron and thermal ion back-current, or drag-curr
  real(DP),pointer  :: current_tor_error_upper(:,:) => null()
  real(DP),pointer  :: current_tor_error_lower(:,:) => null()
  integer :: current_tor_error_index=-999999999

  real(DP),pointer  :: current_fast_tor(:,:) => null()     ! /current_fast_tor - Total toroidal driven current density of fast (non-thermal) particles (excluding electron and therma
  real(DP),pointer  :: current_fast_tor_error_upper(:,:) => null()
  real(DP),pointer  :: current_fast_tor_error_lower(:,:) => null()
  integer :: current_fast_tor_error_index=-999999999

  real(DP),pointer  :: torque_tor_j_radial(:,:) => null()     ! /torque_tor_j_radial - Toroidal torque due to radial currents
  real(DP),pointer  :: torque_tor_j_radial_error_upper(:,:) => null()
  real(DP),pointer  :: torque_tor_j_radial_error_lower(:,:) => null()
  integer :: torque_tor_j_radial_error_index=-999999999

  type (ids_distributions_d_profiles_2d_partial_collisions) :: collisions  ! /collisions - Power and torque exchanged between the species described by the distribution and the different plasm
endtype

type ids_distributions_d_profiles_1d_collisions_ion  !    1D profiles for collisions with a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(DP)  :: z_ion=-9.0D40       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed).
  real(DP)  :: z_ion_error_upper=-9.0D40
  real(DP)  :: z_ion_error_lower=-9.0D40
  integer :: z_ion_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H+, D+, T+, He+2, C+, D2, DT, CD4, ...)
  integer  :: neutral_index=-999999999       ! /neutral_index - Index of the corresponding neutral species in the ../../neutral array
  real(DP),pointer  :: power_thermal(:) => null()     ! /power_thermal - Collisional power density to the thermal particle population
  real(DP),pointer  :: power_thermal_error_upper(:) => null()
  real(DP),pointer  :: power_thermal_error_lower(:) => null()
  integer :: power_thermal_error_index=-999999999

  real(DP),pointer  :: power_fast(:) => null()     ! /power_fast - Collisional power density to the fast particle population
  real(DP),pointer  :: power_fast_error_upper(:) => null()
  real(DP),pointer  :: power_fast_error_lower(:) => null()
  integer :: power_fast_error_index=-999999999

  real(DP),pointer  :: torque_thermal_tor(:) => null()     ! /torque_thermal_tor - Collisional toroidal torque density to the thermal particle population
  real(DP),pointer  :: torque_thermal_tor_error_upper(:) => null()
  real(DP),pointer  :: torque_thermal_tor_error_lower(:) => null()
  integer :: torque_thermal_tor_error_index=-999999999

  real(DP),pointer  :: torque_fast_tor(:) => null()     ! /torque_fast_tor - Collisional toroidal torque density to the fast particle population
  real(DP),pointer  :: torque_fast_tor_error_upper(:) => null()
  real(DP),pointer  :: torque_fast_tor_error_lower(:) => null()
  integer :: torque_fast_tor_error_index=-999999999

  integer  :: multiple_states_flag=-999999999       ! /multiple_states_flag - Multiple state calculation flag : 0-Only one state is considered; 1-Multiple states are considered a
  type (ids_distributions_d_profiles_1d_collisions_ion_state),pointer :: state(:) => null()  ! /state(i) - Collisional exchange with the various states of the ion species (ionisation, energy, excitation, ...
endtype

type ids_distributions_d_profiles_1d_partial_collisions_ion  !    1D profiles for collisions with a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(DP)  :: z_ion=-9.0D40       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed).
  real(DP)  :: z_ion_error_upper=-9.0D40
  real(DP)  :: z_ion_error_lower=-9.0D40
  integer :: z_ion_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H+, D+, T+, He+2, C+, D2, DT, CD4, ...)
  integer  :: neutral_index=-999999999       ! /neutral_index - Index of the corresponding neutral species in the ../../neutral array
  real(DP),pointer  :: power_thermal(:) => null()     ! /power_thermal - Collisional power density to the thermal particle population
  real(DP),pointer  :: power_thermal_error_upper(:) => null()
  real(DP),pointer  :: power_thermal_error_lower(:) => null()
  integer :: power_thermal_error_index=-999999999

  real(DP),pointer  :: power_fast(:) => null()     ! /power_fast - Collisional power density to the fast particle population
  real(DP),pointer  :: power_fast_error_upper(:) => null()
  real(DP),pointer  :: power_fast_error_lower(:) => null()
  integer :: power_fast_error_index=-999999999

  real(DP),pointer  :: torque_thermal_tor(:) => null()     ! /torque_thermal_tor - Collisional toroidal torque density to the thermal particle population
  real(DP),pointer  :: torque_thermal_tor_error_upper(:) => null()
  real(DP),pointer  :: torque_thermal_tor_error_lower(:) => null()
  integer :: torque_thermal_tor_error_index=-999999999

  real(DP),pointer  :: torque_fast_tor(:) => null()     ! /torque_fast_tor - Collisional toroidal torque density to the fast particle population
  real(DP),pointer  :: torque_fast_tor_error_upper(:) => null()
  real(DP),pointer  :: torque_fast_tor_error_lower(:) => null()
  integer :: torque_fast_tor_error_index=-999999999

  integer  :: multiple_states_flag=-999999999       ! /multiple_states_flag - Multiple state calculation flag : 0-Only one state is considered; 1-Multiple states are considered a
  type (ids_distributions_d_profiles_1d_partial_collisions_ion_state),pointer :: state(:) => null()  ! /state(i) - Collisional exchange with the various states of the ion species (ionisation, energy, excitation, ...
endtype

type ids_distributions_d_profiles_1d_collisions_ion_state  !    1D profiles for collisions with a given ion species state
  real(DP)  :: z_min=-9.0D40       ! /z_min - Minimum Z of the charge state bundle (z_min = z_max = 0 for a neutral)
  real(DP)  :: z_min_error_upper=-9.0D40
  real(DP)  :: z_min_error_lower=-9.0D40
  integer :: z_min_error_index=-999999999

  real(DP)  :: z_max=-9.0D40       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(DP)  :: z_max_error_upper=-9.0D40
  real(DP)  :: z_max_error_lower=-9.0D40
  integer :: z_max_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  real(DP),pointer  :: power_thermal(:) => null()     ! /power_thermal - Collisional power density to the thermal particle population
  real(DP),pointer  :: power_thermal_error_upper(:) => null()
  real(DP),pointer  :: power_thermal_error_lower(:) => null()
  integer :: power_thermal_error_index=-999999999

  real(DP),pointer  :: power_fast(:) => null()     ! /power_fast - Collisional power density to the fast particle population
  real(DP),pointer  :: power_fast_error_upper(:) => null()
  real(DP),pointer  :: power_fast_error_lower(:) => null()
  integer :: power_fast_error_index=-999999999

  real(DP),pointer  :: torque_thermal_tor(:) => null()     ! /torque_thermal_tor - Collisional toroidal torque density to the thermal particle population
  real(DP),pointer  :: torque_thermal_tor_error_upper(:) => null()
  real(DP),pointer  :: torque_thermal_tor_error_lower(:) => null()
  integer :: torque_thermal_tor_error_index=-999999999

  real(DP),pointer  :: torque_fast_tor(:) => null()     ! /torque_fast_tor - Collisional toroidal torque density to the fast particle population
  real(DP),pointer  :: torque_fast_tor_error_upper(:) => null()
  real(DP),pointer  :: torque_fast_tor_error_lower(:) => null()
  integer :: torque_fast_tor_error_index=-999999999

endtype

type ids_distributions_d_profiles_1d_partial_collisions_ion_state  !    1D profiles for collisions with a given ion species state
  real(DP)  :: z_min=-9.0D40       ! /z_min - Minimum Z of the charge state bundle (z_min = z_max = 0 for a neutral)
  real(DP)  :: z_min_error_upper=-9.0D40
  real(DP)  :: z_min_error_lower=-9.0D40
  integer :: z_min_error_index=-999999999

  real(DP)  :: z_max=-9.0D40       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(DP)  :: z_max_error_upper=-9.0D40
  real(DP)  :: z_max_error_lower=-9.0D40
  integer :: z_max_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  real(DP),pointer  :: power_thermal(:) => null()     ! /power_thermal - Collisional power density to the thermal particle population
  real(DP),pointer  :: power_thermal_error_upper(:) => null()
  real(DP),pointer  :: power_thermal_error_lower(:) => null()
  integer :: power_thermal_error_index=-999999999

  real(DP),pointer  :: power_fast(:) => null()     ! /power_fast - Collisional power density to the fast particle population
  real(DP),pointer  :: power_fast_error_upper(:) => null()
  real(DP),pointer  :: power_fast_error_lower(:) => null()
  integer :: power_fast_error_index=-999999999

  real(DP),pointer  :: torque_thermal_tor(:) => null()     ! /torque_thermal_tor - Collisional toroidal torque density to the thermal particle population
  real(DP),pointer  :: torque_thermal_tor_error_upper(:) => null()
  real(DP),pointer  :: torque_thermal_tor_error_lower(:) => null()
  integer :: torque_thermal_tor_error_index=-999999999

  real(DP),pointer  :: torque_fast_tor(:) => null()     ! /torque_fast_tor - Collisional toroidal torque density to the fast particle population
  real(DP),pointer  :: torque_fast_tor_error_upper(:) => null()
  real(DP),pointer  :: torque_fast_tor_error_lower(:) => null()
  integer :: torque_fast_tor_error_index=-999999999

endtype

type ids_distributions_d_profiles_1d_collisions_electrons  !    1D profiles for collisions with electrons
  real(DP),pointer  :: power_thermal(:) => null()     ! /power_thermal - Collisional power density to the thermal particle population
  real(DP),pointer  :: power_thermal_error_upper(:) => null()
  real(DP),pointer  :: power_thermal_error_lower(:) => null()
  integer :: power_thermal_error_index=-999999999

  real(DP),pointer  :: power_fast(:) => null()     ! /power_fast - Collisional power density to the fast particle population
  real(DP),pointer  :: power_fast_error_upper(:) => null()
  real(DP),pointer  :: power_fast_error_lower(:) => null()
  integer :: power_fast_error_index=-999999999

  real(DP),pointer  :: torque_thermal_tor(:) => null()     ! /torque_thermal_tor - Collisional toroidal torque density to the thermal particle population
  real(DP),pointer  :: torque_thermal_tor_error_upper(:) => null()
  real(DP),pointer  :: torque_thermal_tor_error_lower(:) => null()
  integer :: torque_thermal_tor_error_index=-999999999

  real(DP),pointer  :: torque_fast_tor(:) => null()     ! /torque_fast_tor - Collisional toroidal torque density to the fast particle population
  real(DP),pointer  :: torque_fast_tor_error_upper(:) => null()
  real(DP),pointer  :: torque_fast_tor_error_lower(:) => null()
  integer :: torque_fast_tor_error_index=-999999999

endtype

type ids_distributions_d_profiles_1d_partial_collisions_electrons  !    1D profiles for collisions with electrons
  real(DP),pointer  :: power_thermal(:) => null()     ! /power_thermal - Collisional power density to the thermal particle population
  real(DP),pointer  :: power_thermal_error_upper(:) => null()
  real(DP),pointer  :: power_thermal_error_lower(:) => null()
  integer :: power_thermal_error_index=-999999999

  real(DP),pointer  :: power_fast(:) => null()     ! /power_fast - Collisional power density to the fast particle population
  real(DP),pointer  :: power_fast_error_upper(:) => null()
  real(DP),pointer  :: power_fast_error_lower(:) => null()
  integer :: power_fast_error_index=-999999999

  real(DP),pointer  :: torque_thermal_tor(:) => null()     ! /torque_thermal_tor - Collisional toroidal torque density to the thermal particle population
  real(DP),pointer  :: torque_thermal_tor_error_upper(:) => null()
  real(DP),pointer  :: torque_thermal_tor_error_lower(:) => null()
  integer :: torque_thermal_tor_error_index=-999999999

  real(DP),pointer  :: torque_fast_tor(:) => null()     ! /torque_fast_tor - Collisional toroidal torque density to the fast particle population
  real(DP),pointer  :: torque_fast_tor_error_upper(:) => null()
  real(DP),pointer  :: torque_fast_tor_error_lower(:) => null()
  integer :: torque_fast_tor_error_index=-999999999

endtype

type ids_distributions_d_profiles_1d_collisions  !    1D profiles for collisions
  type (ids_distributions_d_profiles_1d_collisions_electrons) :: electrons  ! /electrons - Collisional exchange with electrons
  type (ids_distributions_d_profiles_1d_collisions_ion),pointer :: ion(:) => null()  ! /ion(i) - Collisional exchange with the various ion species
endtype

type ids_distributions_d_profiles_1d_partial_collisions  !    1D profiles for collisions
  type (ids_distributions_d_profiles_1d_partial_collisions_electrons) :: electrons  ! /electrons - Collisional exchange with electrons
  type (ids_distributions_d_profiles_1d_partial_collisions_ion),pointer :: ion(:) => null()  ! /ion(i) - Collisional exchange with the various ion species
endtype

type ids_distributions_d_profiles_1d_partial  !    1D profiles from specific particles in the distribution (trapped, co or counter-passing)
  real(DP),pointer  :: density(:) => null()     ! /density - Density (thermal+non-thermal)
  real(DP),pointer  :: density_error_upper(:) => null()
  real(DP),pointer  :: density_error_lower(:) => null()
  integer :: density_error_index=-999999999

  real(DP),pointer  :: density_fast(:) => null()     ! /density_fast - Density of fast particles
  real(DP),pointer  :: density_fast_error_upper(:) => null()
  real(DP),pointer  :: density_fast_error_lower(:) => null()
  integer :: density_fast_error_index=-999999999

  real(DP),pointer  :: pressure(:) => null()     ! /pressure - Pressure (thermal+non-thermal)
  real(DP),pointer  :: pressure_error_upper(:) => null()
  real(DP),pointer  :: pressure_error_lower(:) => null()
  integer :: pressure_error_index=-999999999

  real(DP),pointer  :: pressure_fast(:) => null()     ! /pressure_fast - Pressure of fast particles
  real(DP),pointer  :: pressure_fast_error_upper(:) => null()
  real(DP),pointer  :: pressure_fast_error_lower(:) => null()
  integer :: pressure_fast_error_index=-999999999

  real(DP),pointer  :: pressure_fast_parallel(:) => null()     ! /pressure_fast_parallel - Pressure of fast particles in the parallel direction
  real(DP),pointer  :: pressure_fast_parallel_error_upper(:) => null()
  real(DP),pointer  :: pressure_fast_parallel_error_lower(:) => null()
  integer :: pressure_fast_parallel_error_index=-999999999

  real(DP),pointer  :: current_tor(:) => null()     ! /current_tor - Total toroidal driven current density (including electron and thermal ion back-current, or drag-curr
  real(DP),pointer  :: current_tor_error_upper(:) => null()
  real(DP),pointer  :: current_tor_error_lower(:) => null()
  integer :: current_tor_error_index=-999999999

  real(DP),pointer  :: current_fast_tor(:) => null()     ! /current_fast_tor - Total toroidal driven current density of fast (non-thermal) particles (excluding electron and therma
  real(DP),pointer  :: current_fast_tor_error_upper(:) => null()
  real(DP),pointer  :: current_fast_tor_error_lower(:) => null()
  integer :: current_fast_tor_error_index=-999999999

  real(DP),pointer  :: torque_tor_j_radial(:) => null()     ! /torque_tor_j_radial - Toroidal torque due to radial currents
  real(DP),pointer  :: torque_tor_j_radial_error_upper(:) => null()
  real(DP),pointer  :: torque_tor_j_radial_error_lower(:) => null()
  integer :: torque_tor_j_radial_error_index=-999999999

  type (ids_distributions_d_profiles_1d_partial_collisions) :: collisions  ! /collisions - Power and torque exchanged between the species described by the distribution and the different plasm
  type (ids_distributions_d_profiles_1d_partial_source),pointer :: source(:) => null()  ! /source(i) - Set of flux averaged sources and sinks of particles, momentum and energy included in the Fokker-Plan
endtype

type ids_distributions_d_profiles_1d  !    1D profiles from the distribution, for a given time slice
  type (ids_core_radial_grid) :: grid  ! /grid - Radial grid
  real(DP),pointer  :: density(:) => null()     ! /density - Density (thermal+non-thermal)
  real(DP),pointer  :: density_error_upper(:) => null()
  real(DP),pointer  :: density_error_lower(:) => null()
  integer :: density_error_index=-999999999

  real(DP),pointer  :: density_fast(:) => null()     ! /density_fast - Density of fast particles
  real(DP),pointer  :: density_fast_error_upper(:) => null()
  real(DP),pointer  :: density_fast_error_lower(:) => null()
  integer :: density_fast_error_index=-999999999

  real(DP),pointer  :: pressure(:) => null()     ! /pressure - Pressure (thermal+non-thermal)
  real(DP),pointer  :: pressure_error_upper(:) => null()
  real(DP),pointer  :: pressure_error_lower(:) => null()
  integer :: pressure_error_index=-999999999

  real(DP),pointer  :: pressure_fast(:) => null()     ! /pressure_fast - Pressure of fast particles
  real(DP),pointer  :: pressure_fast_error_upper(:) => null()
  real(DP),pointer  :: pressure_fast_error_lower(:) => null()
  integer :: pressure_fast_error_index=-999999999

  real(DP),pointer  :: pressure_fast_parallel(:) => null()     ! /pressure_fast_parallel - Pressure of fast particles in the parallel direction
  real(DP),pointer  :: pressure_fast_parallel_error_upper(:) => null()
  real(DP),pointer  :: pressure_fast_parallel_error_lower(:) => null()
  integer :: pressure_fast_parallel_error_index=-999999999

  real(DP),pointer  :: current_tor(:) => null()     ! /current_tor - Total toroidal driven current density (including electron and thermal ion back-current, or drag-curr
  real(DP),pointer  :: current_tor_error_upper(:) => null()
  real(DP),pointer  :: current_tor_error_lower(:) => null()
  integer :: current_tor_error_index=-999999999

  real(DP),pointer  :: current_fast_tor(:) => null()     ! /current_fast_tor - Total toroidal driven current density of fast (non-thermal) particles (excluding electron and therma
  real(DP),pointer  :: current_fast_tor_error_upper(:) => null()
  real(DP),pointer  :: current_fast_tor_error_lower(:) => null()
  integer :: current_fast_tor_error_index=-999999999

  real(DP),pointer  :: torque_tor_j_radial(:) => null()     ! /torque_tor_j_radial - Toroidal torque due to radial currents
  real(DP),pointer  :: torque_tor_j_radial_error_upper(:) => null()
  real(DP),pointer  :: torque_tor_j_radial_error_lower(:) => null()
  integer :: torque_tor_j_radial_error_index=-999999999

  type (ids_distributions_d_profiles_1d_collisions) :: collisions  ! /collisions - Power and torque exchanged between the species described by the distribution and the different plasm
  type (ids_distributions_d_profiles_1d_thermalised) :: thermalisation  ! /thermalisation - Flux surface averaged source of thermal particles, momentum and energy due to thermalisation. Here t
  type (ids_distributions_d_profiles_1d_source),pointer :: source(:) => null()  ! /source(i) - Set of flux averaged sources and sinks of particles, momentum and energy included in the Fokker-Plan
  type (ids_distributions_d_profiles_1d_partial) :: trapped  ! /trapped - Flux surface averaged profile evaluated using the trapped particle part of the distribution.
  type (ids_distributions_d_profiles_1d_partial) :: co_passing  ! /co_passing - Flux surface averaged profile evaluated using the co-passing particle part of the distribution.
  type (ids_distributions_d_profiles_1d_partial) :: counter_passing  ! /counter_passing - Flux surface averaged profile evaluated using the counter-passing particle part of the distribution.
  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_distributions_d_profiles_2d_grid  !    2D grid for the distribution
  type (ids_identifier_dynamic_aos3) :: type  ! /type - Grid type: index=0: Rectangular grid in the (R,Z) coordinates; index=1: Rectangular grid in the (rad
  real(DP),pointer  :: r(:) => null()     ! /r - Major radius
  real(DP),pointer  :: r_error_upper(:) => null()
  real(DP),pointer  :: r_error_lower(:) => null()
  integer :: r_error_index=-999999999

  real(DP),pointer  :: z(:) => null()     ! /z - Height
  real(DP),pointer  :: z_error_upper(:) => null()
  real(DP),pointer  :: z_error_lower(:) => null()
  integer :: z_error_index=-999999999

  real(DP),pointer  :: theta_straight(:) => null()     ! /theta_straight - Straight field line poloidal angle
  real(DP),pointer  :: theta_straight_error_upper(:) => null()
  real(DP),pointer  :: theta_straight_error_lower(:) => null()
  integer :: theta_straight_error_index=-999999999

  real(DP),pointer  :: theta_geometric(:) => null()     ! /theta_geometric - Geometrical poloidal angle
  real(DP),pointer  :: theta_geometric_error_upper(:) => null()
  real(DP),pointer  :: theta_geometric_error_lower(:) => null()
  integer :: theta_geometric_error_index=-999999999

  real(DP),pointer  :: rho_tor_norm(:) => null()     ! /rho_tor_norm - Normalised toroidal flux coordinate. The normalizing value for rho_tor_norm, is the toroidal flux co
  real(DP),pointer  :: rho_tor_norm_error_upper(:) => null()
  real(DP),pointer  :: rho_tor_norm_error_lower(:) => null()
  integer :: rho_tor_norm_error_index=-999999999

  real(DP),pointer  :: rho_tor(:) => null()     ! /rho_tor - Toroidal flux coordinate. The toroidal field used in its definition is indicated under vacuum_toroid
  real(DP),pointer  :: rho_tor_error_upper(:) => null()
  real(DP),pointer  :: rho_tor_error_lower(:) => null()
  integer :: rho_tor_error_index=-999999999

  real(DP),pointer  :: psi(:) => null()     ! /psi - Poloidal magnetic flux
  real(DP),pointer  :: psi_error_upper(:) => null()
  real(DP),pointer  :: psi_error_lower(:) => null()
  integer :: psi_error_index=-999999999

  real(DP),pointer  :: volume(:) => null()     ! /volume - Volume enclosed inside the magnetic surface
  real(DP),pointer  :: volume_error_upper(:) => null()
  real(DP),pointer  :: volume_error_lower(:) => null()
  integer :: volume_error_index=-999999999

  real(DP),pointer  :: area(:) => null()     ! /area - Cross-sectional area of the flux surface
  real(DP),pointer  :: area_error_upper(:) => null()
  real(DP),pointer  :: area_error_lower(:) => null()
  integer :: area_error_index=-999999999

endtype

type ids_distributions_d_profiles_2d  !    2D profiles from the distribution, for a given time slice
  type (ids_distributions_d_profiles_2d_grid) :: grid  ! /grid - Grid. The grid has to be rectangular in a pair of coordinates, as specified in type
  real(DP),pointer  :: density(:,:) => null()     ! /density - Density (thermal+non-thermal)
  real(DP),pointer  :: density_error_upper(:,:) => null()
  real(DP),pointer  :: density_error_lower(:,:) => null()
  integer :: density_error_index=-999999999

  real(DP),pointer  :: density_fast(:,:) => null()     ! /density_fast - Density of fast particles
  real(DP),pointer  :: density_fast_error_upper(:,:) => null()
  real(DP),pointer  :: density_fast_error_lower(:,:) => null()
  integer :: density_fast_error_index=-999999999

  real(DP),pointer  :: pressure(:,:) => null()     ! /pressure - Pressure (thermal+non-thermal)
  real(DP),pointer  :: pressure_error_upper(:,:) => null()
  real(DP),pointer  :: pressure_error_lower(:,:) => null()
  integer :: pressure_error_index=-999999999

  real(DP),pointer  :: pressure_fast(:,:) => null()     ! /pressure_fast - Pressure of fast particles
  real(DP),pointer  :: pressure_fast_error_upper(:,:) => null()
  real(DP),pointer  :: pressure_fast_error_lower(:,:) => null()
  integer :: pressure_fast_error_index=-999999999

  real(DP),pointer  :: pressure_fast_parallel(:,:) => null()     ! /pressure_fast_parallel - Pressure of fast particles in the parallel direction
  real(DP),pointer  :: pressure_fast_parallel_error_upper(:,:) => null()
  real(DP),pointer  :: pressure_fast_parallel_error_lower(:,:) => null()
  integer :: pressure_fast_parallel_error_index=-999999999

  real(DP),pointer  :: current_tor(:,:) => null()     ! /current_tor - Total toroidal driven current density (including electron and thermal ion back-current, or drag-curr
  real(DP),pointer  :: current_tor_error_upper(:,:) => null()
  real(DP),pointer  :: current_tor_error_lower(:,:) => null()
  integer :: current_tor_error_index=-999999999

  real(DP),pointer  :: current_fast_tor(:,:) => null()     ! /current_fast_tor - Total toroidal driven current density of fast (non-thermal) particles (excluding electron and therma
  real(DP),pointer  :: current_fast_tor_error_upper(:,:) => null()
  real(DP),pointer  :: current_fast_tor_error_lower(:,:) => null()
  integer :: current_fast_tor_error_index=-999999999

  real(DP),pointer  :: torque_tor_j_radial(:,:) => null()     ! /torque_tor_j_radial - Toroidal torque due to radial currents
  real(DP),pointer  :: torque_tor_j_radial_error_upper(:,:) => null()
  real(DP),pointer  :: torque_tor_j_radial_error_lower(:,:) => null()
  integer :: torque_tor_j_radial_error_index=-999999999

  type (ids_distributions_d_profiles_2d_collisions) :: collisions  ! /collisions - Power and torque exchanged between the species described by the distribution and the different plasm
  type (ids_distributions_d_profiles_2d_partial) :: trapped  ! /trapped - Flux surface averaged profile evaluated using the trapped particle part of the distribution.
  type (ids_distributions_d_profiles_2d_partial) :: co_passing  ! /co_passing - Flux surface averaged profile evaluated using the co-passing particle part of the distribution.
  type (ids_distributions_d_profiles_2d_partial) :: counter_passing  ! /counter_passing - Flux surface averaged profile evaluated using the counter-passing particle part of the distribution.
  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_distributions_d_fast_filter  !    Description of how the fast and the thermal particle populations are separated
  type (ids_identifier) :: method  ! /method - Method used to separate the fast and thermal particle population (indices TBD)
  real(DP),pointer  :: energy(:) => null()     ! /energy - Energy at which the fast and thermal particle populations were separated, as a function of radius
  real(DP),pointer  :: energy_error_upper(:) => null()
  real(DP),pointer  :: energy_error_lower(:) => null()
  integer :: energy_error_index=-999999999

endtype

type ids_distributions_d  !    Description of a given distribution function
  type (ids_waves_coherent_wave_identifier),pointer :: wave(:) => null()  ! /wave(i) - List all waves affecting the distribution, identified as in waves/coherent_wave(i)/identifier in the
  type (ids_distribution_process_identifier),pointer :: process(:) => null()  ! /process(i) - List all processes (NBI units, fusion reactions, ...) affecting the distribution, identified as in d
  integer  :: gyro_type=-999999999       ! /gyro_type - Defines how to interpret the spatial coordinates: 1 = given at the actual particle birth point; 2 =g
  type (ids_distribution_species) :: species  ! /species - Species described by this distribution
  type (ids_distributions_d_fast_filter) :: fast_filter  ! /fast_filter - Description of how the fast and the thermal particle populations are separated
  type (ids_distributions_d_global_quantities),pointer :: global_quantities(:) => null()  ! /global_quantities(i) - Global quantities (integrated over plasma volume for moments of the distribution, collisional exchan
  type (ids_distributions_d_profiles_1d),pointer :: profiles_1d(:) => null()  ! /profiles_1d(i) - Radial profiles (flux surface averaged quantities) for various time slices
  type (ids_distributions_d_profiles_2d),pointer :: profiles_2d(:) => null()  ! /profiles_2d(i) - 2D profiles in the poloidal plane for various time slices
  integer  :: is_delta_f=-999999999       ! /is_delta_f - If is_delta_f=1, then the distribution represents the deviation from a Maxwellian; is_delta_f=0, the
  type (ids_distributions_d_ggd),pointer :: ggd(:) => null()  ! /ggd(i) - Distribution represented using the ggd, for various time slices
  type (ids_distribution_markers),pointer :: markers(:) => null()  ! /markers(i) - Distribution represented by a set of markers (test particles)
endtype

type ids_distributions  !    Distribution function(s) of one or many particle species. This structure is specifically designed to handle non-Maxwellian distrib
  type (ids_ids_properties) :: ids_properties  ! /distributions/ids_properties -
  type (ids_distributions_d),pointer :: distribution(:) => null()  ! /distributions/distribution(i) - Set of distribution functions. Every distribution function has to be associated with only one partic
  type (ids_b_tor_vacuum_1) :: vacuum_toroidal_field  ! /distributions/vacuum_toroidal_field - Characteristics of the vacuum toroidal field (used in rho_tor definition and in the normalization of
  type (ids_rz1d_dynamic_1) :: magnetic_axis  ! /distributions/magnetic_axis - Magnetic axis position (used to define a poloidal angle for the 2D profiles)
  type (ids_code) :: code  ! /distributions/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include ece/dd_ece.xsd
! SPECIAL STRUCTURE data / time
type ids_ece_channel_harmonic  !    Harmonic detected by the channel
  integer, pointer  :: data(:) => null()      ! /harmonic - Harmonic detected by the channel
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_ece_channel_t_e  !    Electron temperature
  real(DP), pointer  :: data(:) => null()     ! /t_e - Electron temperature
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_ece_channel  !    Charge exchange channel
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of the channel
  character(len=132), dimension(:), pointer ::identifier => null()       ! /identifier - ID of the channel
  real(DP)  :: frequency=-9.0D40       ! /frequency - Frequency of the channel
  real(DP)  :: frequency_error_upper=-9.0D40
  real(DP)  :: frequency_error_lower=-9.0D40
  integer :: frequency_error_index=-999999999

  type (ids_ece_channel_harmonic) :: harmonic  ! /harmonic - Harmonic detected by the channel
  type (ids_rzphi1d_dynamic_aos1) :: position  ! /position - Position of the measurements
  type (ids_ece_channel_t_e) :: t_e  ! /t_e - Electron temperature
endtype

type ids_ece  !    Electron cyclotron emission diagnostic
  type (ids_ids_properties) :: ids_properties  ! /ece/ids_properties -
  type (ids_line_of_sight_2points) :: line_of_sight  ! /ece/line_of_sight - Description of the line of sight of the diagnostic (valid for all channels), defined by two points
  type (ids_ece_channel),pointer :: channel(:) => null()  ! /ece/channel(i) - Set of channels (frequency)
  type (ids_code) :: code  ! /ece/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include edge_profiles/dd_edge_profiles.xsd
type ids_edge_profiles_time_slice_neutral_state  !    Quantities related to a given state of the neutral species
  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying state
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  type (ids_identifier) :: neutral_type  ! /neutral_type - Neutral type, in terms of energy. ID =1: cold; 2: thermal; 3: fast; 4: NBI
  type (ids_generic_grid_scalar),pointer :: temperature(:) => null()  ! /temperature(i) - Temperature, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: density(:) => null()  ! /density(i) - Density (thermal+non-thermal), given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: density_fast(:) => null()  ! /density_fast(i) - Density of fast (non-thermal) particles, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure(:) => null()  ! /pressure(i) - Pressure, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure_fast_perpendicular(:) => null()  ! /pressure_fast_perpendicular(i) - Fast (non-thermal) perpendicular pressure, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure_fast_parallel(:) => null()  ! /pressure_fast_parallel(i) - Fast (non-thermal) parallel pressure, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: velocity(:) => null()  ! /velocity(i) - Velocity, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: velocity_diamagnetic(:) => null()  ! /velocity_diamagnetic(i) - Velocity due to the diamagnetic drift, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: velocity_exb(:) => null()  ! /velocity_exb(i) - Velocity due to the ExB drift, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: energy_density_kinetic(:) => null()  ! /energy_density_kinetic(i) - Kinetic energy density, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: distribution_function(:) => null()  ! /distribution_function(i) - Distribution function, given on various grid subsets
endtype

type ids_edge_profiles_time_slice_ion_charge_state  !    Quantities related to a given charge state of the ion species
  real(DP)  :: z_min=-9.0D40       ! /z_min - Minimum Z of the charge state bundle (z_min = z_max = 0 for a neutral)
  real(DP)  :: z_min_error_upper=-9.0D40
  real(DP)  :: z_min_error_lower=-9.0D40
  integer :: z_min_error_index=-999999999

  real(DP)  :: z_max=-9.0D40       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(DP)  :: z_max_error_upper=-9.0D40
  real(DP)  :: z_max_error_lower=-9.0D40
  integer :: z_max_error_index=-999999999

  type (ids_generic_grid_scalar),pointer :: z_average(:) => null()  ! /z_average(i) - Average Z of the charge state bundle (equal to z_min if no bundle), = sum (Z*x_z) where x_z is the r
  type (ids_generic_grid_scalar),pointer :: z_square_average(:) => null()  ! /z_square_average(i) - Average Z square of the charge state bundle (equal to z_min if no bundle), = sum (Z^2*x_z) where x_z
  type (ids_generic_grid_scalar),pointer :: ionisation_potential(:) => null()  ! /ionisation_potential(i) - Cumulative and average ionisation potential to reach a given bundle. Defined as sum (x_z* (sum of Ep
  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  type (ids_generic_grid_scalar),pointer :: temperature(:) => null()  ! /temperature(i) - Temperature, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: density(:) => null()  ! /density(i) - Density (thermal+non-thermal), given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: density_fast(:) => null()  ! /density_fast(i) - Density of fast (non-thermal) particles, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure(:) => null()  ! /pressure(i) - Pressure, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure_fast_perpendicular(:) => null()  ! /pressure_fast_perpendicular(i) - Fast (non-thermal) perpendicular pressure, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure_fast_parallel(:) => null()  ! /pressure_fast_parallel(i) - Fast (non-thermal) parallel pressure, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: velocity(:) => null()  ! /velocity(i) - Velocity, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: velocity_diamagnetic(:) => null()  ! /velocity_diamagnetic(i) - Velocity due to the diamagnetic drift, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: velocity_exb(:) => null()  ! /velocity_exb(i) - Velocity due to the ExB drift, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: energy_density_kinetic(:) => null()  ! /energy_density_kinetic(i) - Kinetic energy density, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: distribution_function(:) => null()  ! /distribution_function(i) - Distribution function, given on various grid subsets
endtype

type ids_edge_profiles_time_slice_neutral  !    Quantities related to a given neutral species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H, D, T, He, C, D2, DT, CD4, ...)
  integer  :: ion_index=-999999999       ! /ion_index - Index of the corresponding ion species in the ../../ion array
  type (ids_generic_grid_scalar),pointer :: temperature(:) => null()  ! /temperature(i) - Temperature (average over states when multiple states are considered), given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: density(:) => null()  ! /density(i) - Density (thermal+non-thermal) (sum over states when multiple states are considered), given on variou
  type (ids_generic_grid_scalar),pointer :: density_fast(:) => null()  ! /density_fast(i) - Density of fast (non-thermal) particles (sum over states when multiple states are considered), given
  type (ids_generic_grid_scalar),pointer :: pressure(:) => null()  ! /pressure(i) - Pressure (average over states when multiple states are considered), given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure_fast_perpendicular(:) => null()  ! /pressure_fast_perpendicular(i) - Fast (non-thermal) perpendicular pressure (average over states when multiple states are considered),
  type (ids_generic_grid_scalar),pointer :: pressure_fast_parallel(:) => null()  ! /pressure_fast_parallel(i) - Fast (non-thermal) parallel pressure (average over states when multiple states are considered), give
  type (ids_generic_grid_vector_components),pointer :: velocity(:) => null()  ! /velocity(i) - Velocity (average over states when multiple states are considered), given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: energy_density_kinetic(:) => null()  ! /energy_density_kinetic(i) - Kinetic energy density (sum over states when multiple states are considered), given on various grid
  integer  :: multiple_states_flag=-999999999       ! /multiple_states_flag - Multiple state calculation flag : 0-Only one state is considered; 1-Multiple states are considered a
  type (ids_edge_profiles_time_slice_neutral_state),pointer :: state(:) => null()  ! /state(i) - Quantities related to the different states of the species (energy, excitation, ...)
endtype

type ids_edge_profiles_time_slice_ion  !    Quantities related to a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(DP)  :: z_ion=-9.0D40       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed).
  real(DP)  :: z_ion_error_upper=-9.0D40
  real(DP)  :: z_ion_error_lower=-9.0D40
  integer :: z_ion_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H+, D+, T+, He+2, C+, D2, DT, CD4, ...)
  integer  :: neutral_index=-999999999       ! /neutral_index - Index of the corresponding neutral species in the ../../neutral array
  type (ids_generic_grid_scalar),pointer :: temperature(:) => null()  ! /temperature(i) - Temperature (average over states when multiple states are considered), given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: density(:) => null()  ! /density(i) - Density (thermal+non-thermal) (sum over states when multiple states are considered), given on variou
  type (ids_generic_grid_scalar),pointer :: density_fast(:) => null()  ! /density_fast(i) - Density of fast (non-thermal) particles (sum over states when multiple states are considered), given
  type (ids_generic_grid_scalar),pointer :: pressure(:) => null()  ! /pressure(i) - Pressure (average over states when multiple states are considered), given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure_fast_perpendicular(:) => null()  ! /pressure_fast_perpendicular(i) - Fast (non-thermal) perpendicular pressure (average over states when multiple states are considered),
  type (ids_generic_grid_scalar),pointer :: pressure_fast_parallel(:) => null()  ! /pressure_fast_parallel(i) - Fast (non-thermal) parallel pressure (average over states when multiple states are considered), give
  type (ids_generic_grid_vector_components),pointer :: velocity(:) => null()  ! /velocity(i) - Velocity (average over states when multiple states are considered), given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: energy_density_kinetic(:) => null()  ! /energy_density_kinetic(i) - Kinetic energy density (sum over states when multiple states are considered), given on various grid
  integer  :: multiple_states_flag=-999999999       ! /multiple_states_flag - Multiple state calculation flag : 0-Only one state is considered; 1-Multiple states are considered a
  type (ids_edge_profiles_time_slice_ion_charge_state),pointer :: state(:) => null()  ! /state(i) - Quantities related to the different states of the species (ionisation, energy, excitation, ...)
endtype

type ids_edge_profiles_time_slice_electrons  !    Quantities related to electrons
  type (ids_generic_grid_scalar),pointer :: temperature(:) => null()  ! /temperature(i) - Temperature, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: density(:) => null()  ! /density(i) - Density (thermal+non-thermal), given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: density_fast(:) => null()  ! /density_fast(i) - Density of fast (non-thermal) particles, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure(:) => null()  ! /pressure(i) - Pressure, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure_fast_perpendicular(:) => null()  ! /pressure_fast_perpendicular(i) - Fast (non-thermal) perpendicular pressure, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure_fast_parallel(:) => null()  ! /pressure_fast_parallel(i) - Fast (non-thermal) parallel pressure, given on various grid subsets
  type (ids_generic_grid_vector_components) :: velocity  ! /velocity - Velocity, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: distribution_function(:) => null()  ! /distribution_function(i) - Distribution function, given on various grid subsets
endtype

type ids_edge_profiles_time_slice  !    edge plasma description for a given time slice
  type (ids_generic_grid_dynamic) :: grid  ! /grid - Grid description
  type (ids_edge_profiles_time_slice_electrons) :: electrons  ! /electrons - Quantities related to the electrons
  type (ids_edge_profiles_time_slice_ion),pointer :: ion(:) => null()  ! /ion(i) - Quantities related to the different ion species
  type (ids_edge_profiles_time_slice_neutral),pointer :: neutral(:) => null()  ! /neutral(i) - Quantities related to the different neutral species
  type (ids_generic_grid_scalar),pointer :: t_i_average(:) => null()  ! /t_i_average(i) - Ion temperature (averaged on ion species), given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: n_i_total_over_n_e(:) => null()  ! /n_i_total_over_n_e(i) - Ratio of total ion density (sum over ion species) over electron density. (thermal+non-thermal), give
  type (ids_generic_grid_scalar),pointer :: zeff(:) => null()  ! /zeff(i) - Effective charge, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure_thermal(:) => null()  ! /pressure_thermal(i) - Thermal pressure (electrons+ions), given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure_perpendicular(:) => null()  ! /pressure_perpendicular(i) - Total perpendicular pressure (electrons+ions, thermal+non-thermal), given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: pressure_parallel(:) => null()  ! /pressure_parallel(i) - Total parallel pressure (electrons+ions, thermal+non-thermal), given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: j_anomalous(:) => null()  ! /j_anomalous(i) - Anomalous current density, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: j_inertial(:) => null()  ! /j_inertial(i) - Inertial current density, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: j_ion_neutral_friction(:) => null()  ! /j_ion_neutral_friction(i) - Current density due to ion neutral friction, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: j_parallel_viscosity(:) => null()  ! /j_parallel_viscosity(i) - Current density due to the parallel viscosity, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: j_perpendicular_viscosity(:) => null()  ! /j_perpendicular_viscosity(i) - Current density due to the perpendicular viscosity, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: j_heat_viscosity(:) => null()  ! /j_heat_viscosity(i) - Current density due to the heat viscosity, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: j_pfirsch_schlueter(:) => null()  ! /j_pfirsch_schlueter(i) - Current density due to Pfirsch-Schlüter effects, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: j_diamagnetic(:) => null()  ! /j_diamagnetic(i) - Current density due to the diamgnetic drift, given on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: e_field(:) => null()  ! /e_field(i) - Electric field, given on various grid subsets
  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_edge_profiles  !    Edge plasma profiles (includes the scrape-off layer and possibly part of the confined plasma)
  type (ids_ids_properties) :: ids_properties  ! /edge_profiles/ids_properties -
  type (ids_core_profiles_profiles_1d),pointer :: profiles_1d(:) => null()  ! /edge_profiles/profiles_1d(i) - SOL radial profiles for various time slices, taken on outboard equatorial mid-plane
  type (ids_edge_profiles_time_slice),pointer :: ggd(:) => null()  ! /edge_profiles/ggd(i) - Edge plasma quantities represented using the general grid description, for various time slices
  type (ids_code) :: code  ! /edge_profiles/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include edge_sources/dd_edge_sources.xsd
type ids_edge_sources_source_ggd_neutral_state  !    Source terms related to the a given state of the neutral species
  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying state
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  type (ids_identifier) :: neutral_type  ! /neutral_type - Neutral type, in terms of energy. ID =1: cold; 2: thermal; 3: fast; 4: NBI
  type (ids_generic_grid_scalar),pointer :: particles(:) => null()  ! /particles(i) - Source term for the charge state density transport equation
  type (ids_generic_grid_scalar),pointer :: energy(:) => null()  ! /energy(i) - Source terms for the charge state energy transport equation
  type (ids_generic_grid_vector_components),pointer :: momentum(:) => null()  ! /momentum(i) - Source term for momentum equations, on various grid subsets
endtype

type ids_edge_sources_source_ggd_ion_state  !    Source terms related to the a given state of the ion species
  real(DP)  :: z_min=-9.0D40       ! /z_min - Minimum Z of the charge state bundle
  real(DP)  :: z_min_error_upper=-9.0D40
  real(DP)  :: z_min_error_lower=-9.0D40
  integer :: z_min_error_index=-999999999

  real(DP)  :: z_max=-9.0D40       ! /z_max - Maximum Z of the charge state bundle
  real(DP)  :: z_max_error_upper=-9.0D40
  real(DP)  :: z_max_error_lower=-9.0D40
  integer :: z_max_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  type (ids_generic_grid_scalar),pointer :: particles(:) => null()  ! /particles(i) - Source term for the charge state density transport equation
  type (ids_generic_grid_scalar),pointer :: energy(:) => null()  ! /energy(i) - Source terms for the charge state energy transport equation
  type (ids_generic_grid_vector_components),pointer :: momentum(:) => null()  ! /momentum(i) - Source term for momentum equations, on various grid subsets
endtype

type ids_edge_sources_source_ggd_ion  !    Source terms related to a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(DP)  :: z_ion=-9.0D40       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed)
  real(DP)  :: z_ion_error_upper=-9.0D40
  real(DP)  :: z_ion_error_lower=-9.0D40
  integer :: z_ion_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  integer  :: neutral_index=-999999999       ! /neutral_index - Index of the corresponding neutral species in the ../../neutral array
  type (ids_generic_grid_scalar),pointer :: particles(:) => null()  ! /particles(i) - Source term for ion density equation, on various grid subsets
  type (ids_generic_grid_scalar),pointer :: energy(:) => null()  ! /energy(i) - Source term for the ion energy transport equation, on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: momentum(:) => null()  ! /momentum(i) - Source term for momentum equations (sum over states when multiple states are considered), on various
  integer  :: multiple_states_flag=-999999999       ! /multiple_states_flag - Multiple states calculation flag : 0-Only one state is considered; 1-Multiple states are considered
  type (ids_edge_sources_source_ggd_ion_state),pointer :: state(:) => null()  ! /state(i) - Source terms related to the different charge states of the species (ionisation, energy, excitation,
endtype

type ids_edge_sources_source_ggd_neutral  !    Source terms related to a given neutral species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying neutral (e.g. H, D, T, He, C, ...)
  integer  :: ion_index=-999999999       ! /ion_index - Index of the corresponding ion species in the ../../ion array
  type (ids_generic_grid_scalar),pointer :: particles(:) => null()  ! /particles(i) - Source term for ion density equation, on various grid subsets
  type (ids_generic_grid_scalar),pointer :: energy(:) => null()  ! /energy(i) - Source term for the ion energy transport equation, on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: momentum(:) => null()  ! /momentum(i) - Source term for momentum equations (sum over states when multiple states are considered), on various
  integer  :: multiple_states_flag=-999999999       ! /multiple_states_flag - Multiple states calculation flag : 0-Only one state is considered; 1-Multiple states are considered
  type (ids_edge_sources_source_ggd_neutral_state),pointer :: state(:) => null()  ! /state(i) - Source terms related to the different charge states of the species (energy, excitation, ...)
endtype

type ids_edge_sources_source_ggd_electrons  !    Source terms related to electrons
  type (ids_generic_grid_scalar),pointer :: particles(:) => null()  ! /particles(i) - Source term for electron density equation, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: energy(:) => null()  ! /energy(i) - Source term for the electron energy equation, given on various grid subsets
endtype

type ids_edge_sources_source_ggd  !    Source terms for a given time slice
  type (ids_generic_grid_dynamic) :: grid  ! /grid - Grid description
  type (ids_edge_sources_source_ggd_electrons) :: electrons  ! /electrons - Sources for electrons
  type (ids_edge_sources_source_ggd_ion),pointer :: ion(:) => null()  ! /ion(i) - Source terms related to the different ion species
  type (ids_edge_sources_source_ggd_neutral),pointer :: neutral(:) => null()  ! /neutral(i) - Source terms related to the different neutral species
  type (ids_generic_grid_scalar),pointer :: total_ion_energy(:) => null()  ! /total_ion_energy(i) - Source term for the total (summed over ion  species) energy equation, on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: momentum(:) => null()  ! /momentum(i) - Source term for total momentum equations, on various grid subsets
  type (ids_generic_grid_scalar),pointer :: current(:) => null()  ! /current(i) - Current density source
  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_edge_sources_source  !    Source terms for a given actuator
  type (ids_identifier) :: identifier  ! /identifier - Source term identifier
  type (ids_edge_sources_source_ggd),pointer :: ggd(:) => null()  ! /ggd(i) - Source terms represented using the general grid description, for various time slices
endtype

type ids_edge_sources  !    Edge plasma sources. Energy terms correspond to the full kinetic energy equation (i.e. the energy flux takes into account the ener
  type (ids_ids_properties) :: ids_properties  ! /edge_sources/ids_properties -
  type (ids_edge_sources_source),pointer :: source(:) => null()  ! /edge_sources/source(i) - Set of source terms
  type (ids_code) :: code  ! /edge_sources/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include edge_transport/dd_edge_transport.xsd
type ids_edge_transport_model_energy  !    Transport coefficients for energy equations.
  type (ids_generic_grid_scalar),pointer :: d(:) => null()  ! /d(i) - Effective diffusivity, on various grid subsets
  type (ids_generic_grid_scalar),pointer :: v(:) => null()  ! /v(i) - Effective convection, on various grid subsets
  type (ids_generic_grid_scalar),pointer :: flux(:) => null()  ! /flux(i) - Flux, on various grid subsets
  type (ids_generic_grid_scalar),pointer :: flux_limiter(:) => null()  ! /flux_limiter(i) - Flux limiter coefficient, on various grid subsets
endtype

type ids_edge_transport_model_momentum  !    Transport coefficients for momentum equations.
  type (ids_generic_grid_vector_components),pointer :: d(:) => null()  ! /d(i) - Effective diffusivity, on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: v(:) => null()  ! /v(i) - Effective convection, on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: flux(:) => null()  ! /flux(i) - Flux, on various grid subsets
  type (ids_generic_grid_vector_components),pointer :: flux_limiter(:) => null()  ! /flux_limiter(i) - Flux limiter coefficient, on various grid subsets
endtype

type ids_edge_transport_model_density  !    Transport coefficients for energy equations.
  type (ids_generic_grid_scalar),pointer :: d(:) => null()  ! /d(i) - Effective diffusivity, on various grid subsets
  type (ids_generic_grid_scalar),pointer :: v(:) => null()  ! /v(i) - Effective convection, on various grid subsets
  type (ids_generic_grid_scalar),pointer :: flux(:) => null()  ! /flux(i) - Flux, on various grid subsets
  type (ids_generic_grid_scalar),pointer :: flux_limiter(:) => null()  ! /flux_limiter(i) - Flux limiter coefficient, on various grid subsets
endtype

type ids_edge_transport_model_neutral_state  !    Transport coefficients related to a given state of the neutral species
  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying state
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  type (ids_identifier) :: neutral_type  ! /neutral_type - Neutral type, in terms of energy. ID =1: cold; 2: thermal; 3: fast; 4: NBI
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  type (ids_edge_transport_model_density) :: particles  ! /particles - Transport quantities related to density equation of the charge state considered (thermal+non-thermal
  type (ids_edge_transport_model_energy) :: energy  ! /energy - Transport quantities related to the energy equation of the charge state considered
  type (ids_edge_transport_model_momentum) :: momentum  ! /momentum - Transport coefficients related to the momentum equations of the charge state considered
endtype

type ids_edge_transport_model_ion_state  !    Transport coefficients related to a given state of the ion species
  real(DP)  :: z_min=-9.0D40       ! /z_min - Minimum Z of the charge state bundle
  real(DP)  :: z_min_error_upper=-9.0D40
  real(DP)  :: z_min_error_lower=-9.0D40
  integer :: z_min_error_index=-999999999

  real(DP)  :: z_max=-9.0D40       ! /z_max - Maximum Z of the charge state bundle
  real(DP)  :: z_max_error_upper=-9.0D40
  real(DP)  :: z_max_error_lower=-9.0D40
  integer :: z_max_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  type (ids_edge_transport_model_density) :: particles  ! /particles - Transport quantities related to density equation of the charge state considered (thermal+non-thermal
  type (ids_edge_transport_model_energy) :: energy  ! /energy - Transport quantities related to the energy equation of the charge state considered
  type (ids_edge_transport_model_momentum) :: momentum  ! /momentum - Transport coefficients related to the momentum equations of the charge state considered
endtype

type ids_edge_transport_model_neutral  !    Transport coefficients related to a given neutral species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying neutral (e.g. H, D, T, He, C, ...)
  integer  :: ion_index=-999999999       ! /ion_index - Index of the corresponding ion species in the ../../ion array
  type (ids_edge_transport_model_density) :: particles  ! /particles - Transport related to the ion density equation
  type (ids_edge_transport_model_energy) :: energy  ! /energy - Transport coefficients related to the ion energy equation
  type (ids_edge_transport_model_momentum) :: momentum  ! /momentum - Transport coefficients for the ion momentum equations
  integer  :: multiple_states_flag=-999999999       ! /multiple_states_flag - Multiple states calculation flag : 0-Only one state is considered; 1-Multiple states are considered
  type (ids_edge_transport_model_neutral_state),pointer :: state(:) => null()  ! /state(i) - Transport coefficients related to the different states of the species
endtype

type ids_edge_transport_model_ion  !    Transport coefficients related to a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(DP)  :: z_ion=-9.0D40       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed)
  real(DP)  :: z_ion_error_upper=-9.0D40
  real(DP)  :: z_ion_error_lower=-9.0D40
  integer :: z_ion_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  integer  :: neutral_index=-999999999       ! /neutral_index - Index of the corresponding neutral species in the ../../neutral array
  type (ids_edge_transport_model_density) :: particles  ! /particles - Transport related to the ion density equation
  type (ids_edge_transport_model_energy) :: energy  ! /energy - Transport coefficients related to the ion energy equation
  type (ids_edge_transport_model_momentum) :: momentum  ! /momentum - Transport coefficients for the ion momentum equations
  integer  :: multiple_states_flag=-999999999       ! /multiple_states_flag - Multiple states calculation flag : 0-Only one state is considered; 1-Multiple states are considered
  type (ids_edge_transport_model_ion_state),pointer :: state(:) => null()  ! /state(i) - Transport coefficients related to the different states of the species
endtype

type ids_edge_transport_model_electrons  !    Transport coefficients related to electrons
  type (ids_edge_transport_model_density) :: particles  ! /particles - Transport quantities for the electron density equation
  type (ids_edge_transport_model_energy) :: energy  ! /energy - Transport quantities for the electron energy equation
endtype

type ids_edge_transport_model_ggd  !    Transport coefficient given on the ggd at a given time slice
  type (ids_generic_grid_dynamic) :: grid  ! /grid - Grid description
  type (ids_generic_grid_vector_components),pointer :: conductivity(:) => null()  ! /conductivity(i) - Conductivity, on various grid subsets
  type (ids_edge_transport_model_electrons) :: electrons  ! /electrons - Transport quantities related to the electrons
  type (ids_edge_transport_model_energy) :: total_ion_energy  ! /total_ion_energy - Transport coefficients for the total (summed over ion  species) energy equation
  type (ids_edge_transport_model_momentum) :: momentum  ! /momentum - Transport coefficients for total momentum equation
  type (ids_edge_transport_model_ion),pointer :: ion(:) => null()  ! /ion(i) - Transport coefficients related to the various ion and neutral species
  type (ids_edge_transport_model_neutral),pointer :: neutral(:) => null()  ! /neutral(i) - Transport coefficients related to the various ion and neutral species
  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_edge_transport_model  !    Transport coefficients for a given model
  type (ids_identifier) :: identifier  ! /identifier - Transport model identifier
  real(DP)  :: flux_multiplier=-9.0D40       ! /flux_multiplier - Multiplier applied to the particule flux when adding its contribution in the expression of the heat
  real(DP)  :: flux_multiplier_error_upper=-9.0D40
  real(DP)  :: flux_multiplier_error_lower=-9.0D40
  integer :: flux_multiplier_error_index=-999999999

  type (ids_edge_transport_model_ggd),pointer :: ggd(:) => null()  ! /ggd(i) - Transport coefficients represented using the general grid description, for various time slices
endtype

type ids_edge_transport  !    Edge plasma transport. Energy terms correspond to the full kinetic energy equation (i.e. the energy flux takes into account the en
  type (ids_ids_properties) :: ids_properties  ! /edge_transport/ids_properties -
  type (ids_edge_transport_model),pointer :: model(:) => null()  ! /edge_transport/model(i) - Transport is described by a combination of various transport models
  type (ids_code) :: code  ! /edge_transport/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include em_coupling/dd_em_coupling.xsd
type ids_em_coupling  !    Description of the axisymmetric mutual electromagnetics; does not include non-axisymmetric coil systems; the convention is Quantit
  type (ids_ids_properties) :: ids_properties  ! /em_coupling/ids_properties -
  real(DP),pointer  :: mutual_active_active(:,:) => null()     ! /em_coupling/mutual_active_active - Mutual inductance coupling from active coils to active coils
  real(DP),pointer  :: mutual_active_active_error_upper(:,:) => null()
  real(DP),pointer  :: mutual_active_active_error_lower(:,:) => null()
  integer :: mutual_active_active_error_index=-999999999

  real(DP),pointer  :: mutual_passive_active(:,:) => null()     ! /em_coupling/mutual_passive_active - Mutual inductance coupling from active coils to passive loops
  real(DP),pointer  :: mutual_passive_active_error_upper(:,:) => null()
  real(DP),pointer  :: mutual_passive_active_error_lower(:,:) => null()
  integer :: mutual_passive_active_error_index=-999999999

  real(DP),pointer  :: mutual_loops_active(:,:) => null()     ! /em_coupling/mutual_loops_active - Mutual inductance coupling from active coils to poloidal flux loops
  real(DP),pointer  :: mutual_loops_active_error_upper(:,:) => null()
  real(DP),pointer  :: mutual_loops_active_error_lower(:,:) => null()
  integer :: mutual_loops_active_error_index=-999999999

  real(DP),pointer  :: field_probes_active(:,:) => null()     ! /em_coupling/field_probes_active - Poloidal field coupling from active coils to poloidal field probes
  real(DP),pointer  :: field_probes_active_error_upper(:,:) => null()
  real(DP),pointer  :: field_probes_active_error_lower(:,:) => null()
  integer :: field_probes_active_error_index=-999999999

  real(DP),pointer  :: mutual_passive_passive(:,:) => null()     ! /em_coupling/mutual_passive_passive - Mutual inductance coupling from passive loops to passive loops
  real(DP),pointer  :: mutual_passive_passive_error_upper(:,:) => null()
  real(DP),pointer  :: mutual_passive_passive_error_lower(:,:) => null()
  integer :: mutual_passive_passive_error_index=-999999999

  real(DP),pointer  :: mutual_loops_passive(:,:) => null()     ! /em_coupling/mutual_loops_passive - Mutual  inductance coupling from passive  loops to poloidal flux loops
  real(DP),pointer  :: mutual_loops_passive_error_upper(:,:) => null()
  real(DP),pointer  :: mutual_loops_passive_error_lower(:,:) => null()
  integer :: mutual_loops_passive_error_index=-999999999

  real(DP),pointer  :: field_probes_passive(:,:) => null()     ! /em_coupling/field_probes_passive - Poloidal field coupling from passive loops to poloidal field probes
  real(DP),pointer  :: field_probes_passive_error_upper(:,:) => null()
  real(DP),pointer  :: field_probes_passive_error_lower(:,:) => null()
  integer :: field_probes_passive_error_index=-999999999

  real(DP),pointer  :: mutual_grid_grid(:,:) => null()     ! /em_coupling/mutual_grid_grid - Mutual inductance from equilibrium grid to itself
  real(DP),pointer  :: mutual_grid_grid_error_upper(:,:) => null()
  real(DP),pointer  :: mutual_grid_grid_error_lower(:,:) => null()
  integer :: mutual_grid_grid_error_index=-999999999

  real(DP),pointer  :: mutual_grid_active(:,:) => null()     ! /em_coupling/mutual_grid_active - Mutual inductance coupling from active coils to equilibrium grid
  real(DP),pointer  :: mutual_grid_active_error_upper(:,:) => null()
  real(DP),pointer  :: mutual_grid_active_error_lower(:,:) => null()
  integer :: mutual_grid_active_error_index=-999999999

  real(DP),pointer  :: mutual_grid_passive(:,:) => null()     ! /em_coupling/mutual_grid_passive - Mutual inductance coupling from passive loops to equilibrium grid
  real(DP),pointer  :: mutual_grid_passive_error_upper(:,:) => null()
  real(DP),pointer  :: mutual_grid_passive_error_lower(:,:) => null()
  integer :: mutual_grid_passive_error_index=-999999999

  real(DP),pointer  :: field_probes_grid(:,:) => null()     ! /em_coupling/field_probes_grid - Poloidal field coupling from equilibrium grid to poloidal field probes
  real(DP),pointer  :: field_probes_grid_error_upper(:,:) => null()
  real(DP),pointer  :: field_probes_grid_error_lower(:,:) => null()
  integer :: field_probes_grid_error_index=-999999999

  real(DP),pointer  :: mutual_loops_grid(:,:) => null()     ! /em_coupling/mutual_loops_grid - Mutual inductance from equilibrium grid to poloidal flux loops
  real(DP),pointer  :: mutual_loops_grid_error_upper(:,:) => null()
  real(DP),pointer  :: mutual_loops_grid_error_lower(:,:) => null()
  integer :: mutual_loops_grid_error_index=-999999999

  character(len=132), dimension(:), pointer ::active_coils => null()       ! /em_coupling/active_coils - List of the names of the active PF+CS coils
  character(len=132), dimension(:), pointer ::passive_loops => null()       ! /em_coupling/passive_loops - List of the names of the passive loops
  character(len=132), dimension(:), pointer ::poloidal_probes => null()       ! /em_coupling/poloidal_probes - List of the names of poloidal field probes
  character(len=132), dimension(:), pointer ::flux_loops => null()       ! /em_coupling/flux_loops - List of the names of the axisymmetric flux loops
  character(len=132), dimension(:), pointer ::grid_points => null()       ! /em_coupling/grid_points - List of the names of the plasma region grid points
  type (ids_code) :: code  ! /em_coupling/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include equilibrium/dd_equilibrium.xsd
type ids_equilibrium_boundary  !    Geometry of the plasma boundary
  integer  :: type=-999999999       ! /type - 0 (limiter) or 1 (diverted)
  type (ids_rz1d_dynamic_aos) :: outline  ! /outline - RZ outline of the plasma boundary
  type (ids_rz1d_dynamic_aos) :: lcfs  ! /lcfs - RZ description of the plasma boundary
  real(DP)  :: b_flux_pol_norm=-9.0D40       ! /b_flux_pol_norm - Value of the normalised poloidal flux at which the boundary is taken
  real(DP)  :: b_flux_pol_norm_error_upper=-9.0D40
  real(DP)  :: b_flux_pol_norm_error_lower=-9.0D40
  integer :: b_flux_pol_norm_error_index=-999999999

  type (ids_rz0d_dynamic_aos) :: geometric_axis  ! /geometric_axis - RZ position of the geometric axis (defined as (Rmin+Rmax) / 2 and (Zmin+Zmax) / 2 of the boundary)
  real(DP)  :: minor_radius=-9.0D40       ! /minor_radius - Minor radius of the plasma boundary (defined as (Rmax-Rmin) / 2 of the boundary)
  real(DP)  :: minor_radius_error_upper=-9.0D40
  real(DP)  :: minor_radius_error_lower=-9.0D40
  integer :: minor_radius_error_index=-999999999

  real(DP)  :: elongation=-9.0D40       ! /elongation - Elongation of the plasma boundary
  real(DP)  :: elongation_error_upper=-9.0D40
  real(DP)  :: elongation_error_lower=-9.0D40
  integer :: elongation_error_index=-999999999

  real(DP)  :: elongation_upper=-9.0D40       ! /elongation_upper - Elongation (upper half w.r.t. geometric axis) of the plasma boundary
  real(DP)  :: elongation_upper_error_upper=-9.0D40
  real(DP)  :: elongation_upper_error_lower=-9.0D40
  integer :: elongation_upper_error_index=-999999999

  real(DP)  :: elongation_lower=-9.0D40       ! /elongation_lower - Elongation (lower half w.r.t. geometric axis) of the plasma boundary
  real(DP)  :: elongation_lower_error_upper=-9.0D40
  real(DP)  :: elongation_lower_error_lower=-9.0D40
  integer :: elongation_lower_error_index=-999999999

  real(DP)  :: triangularity=-9.0D40       ! /triangularity - Triangularity of the plasma boundary
  real(DP)  :: triangularity_error_upper=-9.0D40
  real(DP)  :: triangularity_error_lower=-9.0D40
  integer :: triangularity_error_index=-999999999

  real(DP)  :: triangularity_upper=-9.0D40       ! /triangularity_upper - Upper triangularity of the plasma boundary
  real(DP)  :: triangularity_upper_error_upper=-9.0D40
  real(DP)  :: triangularity_upper_error_lower=-9.0D40
  integer :: triangularity_upper_error_index=-999999999

  real(DP)  :: triangularity_lower=-9.0D40       ! /triangularity_lower - Lower triangularity of the plasma boundary
  real(DP)  :: triangularity_lower_error_upper=-9.0D40
  real(DP)  :: triangularity_lower_error_lower=-9.0D40
  integer :: triangularity_lower_error_index=-999999999

  type (ids_rz0d_dynamic_aos),pointer :: x_point(:) => null()  ! /x_point(i) - Array of X-points, for each of them the RZ position is given
  type (ids_rz0d_dynamic_aos),pointer :: strike_point(:) => null()  ! /strike_point(i) - Array of strike points, for each of them the RZ position is given
  type (ids_rz0d_dynamic_aos) :: active_limiter_point  ! /active_limiter_point - RZ position of the active limiter point (point of the plasma boundary in contact with the limiter)
endtype

type ids_equilibrium_global_quantities_magnetic_axis  !    R, Z, and Btor at magnetic axis, dynamic within a type 3 array of structure (index on time)
  real(DP)  :: r=-9.0D40       ! /r - Major radius of the magnetic axis
  real(DP)  :: r_error_upper=-9.0D40
  real(DP)  :: r_error_lower=-9.0D40
  integer :: r_error_index=-999999999

  real(DP)  :: z=-9.0D40       ! /z - Height of the magnetic axis
  real(DP)  :: z_error_upper=-9.0D40
  real(DP)  :: z_error_lower=-9.0D40
  integer :: z_error_index=-999999999

  real(DP)  :: b_tor=-9.0D40       ! /b_tor - Total toroidal magnetic field at the magnetic axis
  real(DP)  :: b_tor_error_upper=-9.0D40
  real(DP)  :: b_tor_error_lower=-9.0D40
  integer :: b_tor_error_index=-999999999

  real(DP)  :: b_field_tor=-9.0D40       ! /b_field_tor - Total toroidal magnetic field at the magnetic axis
  real(DP)  :: b_field_tor_error_upper=-9.0D40
  real(DP)  :: b_field_tor_error_lower=-9.0D40
  integer :: b_field_tor_error_index=-999999999

endtype

type ids_equilibrium_global_quantities_qmin  !    Position and value of q_min
  real(DP)  :: value=-9.0D40       ! /value - Minimum q value
  real(DP)  :: value_error_upper=-9.0D40
  real(DP)  :: value_error_lower=-9.0D40
  integer :: value_error_index=-999999999

  real(DP)  :: rho_tor_norm=-9.0D40       ! /rho_tor_norm - Minimum q position in normalised toroidal flux coordinate
  real(DP)  :: rho_tor_norm_error_upper=-9.0D40
  real(DP)  :: rho_tor_norm_error_lower=-9.0D40
  integer :: rho_tor_norm_error_index=-999999999

endtype

type ids_equlibrium_global_quantities  !    0D parameters of the equilibrium
  real(DP)  :: beta_pol=-9.0D40       ! /beta_pol - Poloidal beta. Defined as betap = 4 int(p dV) / [R_0 * mu_0 * Ip^2]
  real(DP)  :: beta_pol_error_upper=-9.0D40
  real(DP)  :: beta_pol_error_lower=-9.0D40
  integer :: beta_pol_error_index=-999999999

  real(DP)  :: beta_tor=-9.0D40       ! /beta_tor - Toroidal beta, defined as the volume-averaged total perpendicular pressure divided by (B0^2/(2*mu0))
  real(DP)  :: beta_tor_error_upper=-9.0D40
  real(DP)  :: beta_tor_error_lower=-9.0D40
  integer :: beta_tor_error_index=-999999999

  real(DP)  :: beta_normal=-9.0D40       ! /beta_normal - Normalised toroidal beta, defined as 100 * beta_tor * a[m] * B0 [T] / ip [MA]
  real(DP)  :: beta_normal_error_upper=-9.0D40
  real(DP)  :: beta_normal_error_lower=-9.0D40
  integer :: beta_normal_error_index=-999999999

  real(DP)  :: ip=-9.0D40       ! /ip - Plasma current. Positive sign means anti-clockwise when viewed from above.
  real(DP)  :: ip_error_upper=-9.0D40
  real(DP)  :: ip_error_lower=-9.0D40
  integer :: ip_error_index=-999999999

  real(DP)  :: li_3=-9.0D40       ! /li_3 - Internal inductance
  real(DP)  :: li_3_error_upper=-9.0D40
  real(DP)  :: li_3_error_lower=-9.0D40
  integer :: li_3_error_index=-999999999

  real(DP)  :: volume=-9.0D40       ! /volume - Total plasma volume
  real(DP)  :: volume_error_upper=-9.0D40
  real(DP)  :: volume_error_lower=-9.0D40
  integer :: volume_error_index=-999999999

  real(DP)  :: area=-9.0D40       ! /area - Area of the LCFS poloidal cross section
  real(DP)  :: area_error_upper=-9.0D40
  real(DP)  :: area_error_lower=-9.0D40
  integer :: area_error_index=-999999999

  real(DP)  :: surface=-9.0D40       ! /surface - Surface area of the toroidal flux surface
  real(DP)  :: surface_error_upper=-9.0D40
  real(DP)  :: surface_error_lower=-9.0D40
  integer :: surface_error_index=-999999999

  real(DP)  :: length_pol=-9.0D40       ! /length_pol - Poloidal length of the magnetic surface
  real(DP)  :: length_pol_error_upper=-9.0D40
  real(DP)  :: length_pol_error_lower=-9.0D40
  integer :: length_pol_error_index=-999999999

  real(DP)  :: psi_axis=-9.0D40       ! /psi_axis - Poloidal flux at the magnetic axis
  real(DP)  :: psi_axis_error_upper=-9.0D40
  real(DP)  :: psi_axis_error_lower=-9.0D40
  integer :: psi_axis_error_index=-999999999

  real(DP)  :: psi_boundary=-9.0D40       ! /psi_boundary - Poloidal flux at the selected plasma boundary
  real(DP)  :: psi_boundary_error_upper=-9.0D40
  real(DP)  :: psi_boundary_error_lower=-9.0D40
  integer :: psi_boundary_error_index=-999999999

  type (ids_equilibrium_global_quantities_magnetic_axis) :: magnetic_axis  ! /magnetic_axis - Magnetic axis position and toroidal field
  real(DP)  :: q_axis=-9.0D40       ! /q_axis - q at the magnetic axis
  real(DP)  :: q_axis_error_upper=-9.0D40
  real(DP)  :: q_axis_error_lower=-9.0D40
  integer :: q_axis_error_index=-999999999

  real(DP)  :: q_95=-9.0D40       ! /q_95 - q at the 95% poloidal flux surface
  real(DP)  :: q_95_error_upper=-9.0D40
  real(DP)  :: q_95_error_lower=-9.0D40
  integer :: q_95_error_index=-999999999

  type (ids_equilibrium_global_quantities_qmin) :: q_min  ! /q_min - Minimum q value and position
  real(DP)  :: w_mhd=-9.0D40       ! /w_mhd - Plasma energy content = 3/2 * int(p,dV) with p being the total pressure (thermal + fast particles) [
  real(DP)  :: w_mhd_error_upper=-9.0D40
  real(DP)  :: w_mhd_error_lower=-9.0D40
  integer :: w_mhd_error_index=-999999999

endtype

type ids_equilibrium_profiles_1d  !    Equilibrium profiles (1D radial grid) as a function of the poloidal flux
  real(DP),pointer  :: psi(:) => null()     ! /psi - Poloidal flux
  real(DP),pointer  :: psi_error_upper(:) => null()
  real(DP),pointer  :: psi_error_lower(:) => null()
  integer :: psi_error_index=-999999999

  real(DP),pointer  :: phi(:) => null()     ! /phi - Toroidal flux
  real(DP),pointer  :: phi_error_upper(:) => null()
  real(DP),pointer  :: phi_error_lower(:) => null()
  integer :: phi_error_index=-999999999

  real(DP),pointer  :: pressure(:) => null()     ! /pressure - Pressure
  real(DP),pointer  :: pressure_error_upper(:) => null()
  real(DP),pointer  :: pressure_error_lower(:) => null()
  integer :: pressure_error_index=-999999999

  real(DP),pointer  :: f(:) => null()     ! /f - Diamagnetic function (F=R B_Phi)
  real(DP),pointer  :: f_error_upper(:) => null()
  real(DP),pointer  :: f_error_lower(:) => null()
  integer :: f_error_index=-999999999

  real(DP),pointer  :: dpressure_dpsi(:) => null()     ! /dpressure_dpsi - Derivative of pressure w.r.t. psi
  real(DP),pointer  :: dpressure_dpsi_error_upper(:) => null()
  real(DP),pointer  :: dpressure_dpsi_error_lower(:) => null()
  integer :: dpressure_dpsi_error_index=-999999999

  real(DP),pointer  :: f_df_dpsi(:) => null()     ! /f_df_dpsi - Derivative of F w.r.t. Psi, multiplied with F
  real(DP),pointer  :: f_df_dpsi_error_upper(:) => null()
  real(DP),pointer  :: f_df_dpsi_error_lower(:) => null()
  integer :: f_df_dpsi_error_index=-999999999

  real(DP),pointer  :: j_tor(:) => null()     ! /j_tor - Flux surface averaged toroidal current density = average(j_tor/R) / average(1/R)
  real(DP),pointer  :: j_tor_error_upper(:) => null()
  real(DP),pointer  :: j_tor_error_lower(:) => null()
  integer :: j_tor_error_index=-999999999

  real(DP),pointer  :: j_parallel(:) => null()     ! /j_parallel - Flux surface averaged parallel current density = average(j.B) / B0, where B0 = Equilibrium/Global/To
  real(DP),pointer  :: j_parallel_error_upper(:) => null()
  real(DP),pointer  :: j_parallel_error_lower(:) => null()
  integer :: j_parallel_error_index=-999999999

  real(DP),pointer  :: q(:) => null()     ! /q - Safety factor
  real(DP),pointer  :: q_error_upper(:) => null()
  real(DP),pointer  :: q_error_lower(:) => null()
  integer :: q_error_index=-999999999

  real(DP),pointer  :: magnetic_shear(:) => null()     ! /magnetic_shear - Magnetic shear, defined as rho_tor/q . dq/drho_tor
  real(DP),pointer  :: magnetic_shear_error_upper(:) => null()
  real(DP),pointer  :: magnetic_shear_error_lower(:) => null()
  integer :: magnetic_shear_error_index=-999999999

  real(DP),pointer  :: r_inboard(:) => null()     ! /r_inboard - Radial coordinate (major radius) on the inboard side of the magnetic axis
  real(DP),pointer  :: r_inboard_error_upper(:) => null()
  real(DP),pointer  :: r_inboard_error_lower(:) => null()
  integer :: r_inboard_error_index=-999999999

  real(DP),pointer  :: r_outboard(:) => null()     ! /r_outboard - Radial coordinate (major radius) on the outboard side of the magnetic axis
  real(DP),pointer  :: r_outboard_error_upper(:) => null()
  real(DP),pointer  :: r_outboard_error_lower(:) => null()
  integer :: r_outboard_error_index=-999999999

  real(DP),pointer  :: rho_tor(:) => null()     ! /rho_tor - Toroidal flux coordinate. The toroidal field used in its definition is indicated under vacuum_toroid
  real(DP),pointer  :: rho_tor_error_upper(:) => null()
  real(DP),pointer  :: rho_tor_error_lower(:) => null()
  integer :: rho_tor_error_index=-999999999

  real(DP),pointer  :: rho_tor_norm(:) => null()     ! /rho_tor_norm - Normalised toroidal flux coordinate. The normalizing value for rho_tor_norm, is the toroidal flux co
  real(DP),pointer  :: rho_tor_norm_error_upper(:) => null()
  real(DP),pointer  :: rho_tor_norm_error_lower(:) => null()
  integer :: rho_tor_norm_error_index=-999999999

  real(DP),pointer  :: dpsi_drho_tor(:) => null()     ! /dpsi_drho_tor - Derivative of Psi with respect to Rho_Tor
  real(DP),pointer  :: dpsi_drho_tor_error_upper(:) => null()
  real(DP),pointer  :: dpsi_drho_tor_error_lower(:) => null()
  integer :: dpsi_drho_tor_error_index=-999999999

  real(DP),pointer  :: elongation(:) => null()     ! /elongation - Elongation
  real(DP),pointer  :: elongation_error_upper(:) => null()
  real(DP),pointer  :: elongation_error_lower(:) => null()
  integer :: elongation_error_index=-999999999

  real(DP),pointer  :: triangularity_upper(:) => null()     ! /triangularity_upper - Upper triangularity w.r.t. magnetic axis
  real(DP),pointer  :: triangularity_upper_error_upper(:) => null()
  real(DP),pointer  :: triangularity_upper_error_lower(:) => null()
  integer :: triangularity_upper_error_index=-999999999

  real(DP),pointer  :: triangularity_lower(:) => null()     ! /triangularity_lower - Lower triangularity w.r.t. magnetic axis
  real(DP),pointer  :: triangularity_lower_error_upper(:) => null()
  real(DP),pointer  :: triangularity_lower_error_lower(:) => null()
  integer :: triangularity_lower_error_index=-999999999

  real(DP),pointer  :: volume(:) => null()     ! /volume - Volume enclosed in the flux surface
  real(DP),pointer  :: volume_error_upper(:) => null()
  real(DP),pointer  :: volume_error_lower(:) => null()
  integer :: volume_error_index=-999999999

  real(DP),pointer  :: dvolume_dpsi(:) => null()     ! /dvolume_dpsi - Radial derivative of the volume enclosed in the flux surface with respect to Psi
  real(DP),pointer  :: dvolume_dpsi_error_upper(:) => null()
  real(DP),pointer  :: dvolume_dpsi_error_lower(:) => null()
  integer :: dvolume_dpsi_error_index=-999999999

  real(DP),pointer  :: dvolume_drho_tor(:) => null()     ! /dvolume_drho_tor - Radial derivative of the volume enclosed in the flux surface with respect to Rho_Tor
  real(DP),pointer  :: dvolume_drho_tor_error_upper(:) => null()
  real(DP),pointer  :: dvolume_drho_tor_error_lower(:) => null()
  integer :: dvolume_drho_tor_error_index=-999999999

  real(DP),pointer  :: area(:) => null()     ! /area - Cross-sectional area of the flux surface
  real(DP),pointer  :: area_error_upper(:) => null()
  real(DP),pointer  :: area_error_lower(:) => null()
  integer :: area_error_index=-999999999

  real(DP),pointer  :: darea_dpsi(:) => null()     ! /darea_dpsi - Radial derivative of the cross-sectional area of the flux surface with respect to psi
  real(DP),pointer  :: darea_dpsi_error_upper(:) => null()
  real(DP),pointer  :: darea_dpsi_error_lower(:) => null()
  integer :: darea_dpsi_error_index=-999999999

  real(DP),pointer  :: darea_drho_tor(:) => null()     ! /darea_drho_tor - Radial derivative of the cross-sectional area of the flux surface with respect to rho_tor
  real(DP),pointer  :: darea_drho_tor_error_upper(:) => null()
  real(DP),pointer  :: darea_drho_tor_error_lower(:) => null()
  integer :: darea_drho_tor_error_index=-999999999

  real(DP),pointer  :: surface(:) => null()     ! /surface - Surface area of the toroidal flux surface
  real(DP),pointer  :: surface_error_upper(:) => null()
  real(DP),pointer  :: surface_error_lower(:) => null()
  integer :: surface_error_index=-999999999

  real(DP),pointer  :: trapped_fraction(:) => null()     ! /trapped_fraction - Trapped particle fraction
  real(DP),pointer  :: trapped_fraction_error_upper(:) => null()
  real(DP),pointer  :: trapped_fraction_error_lower(:) => null()
  integer :: trapped_fraction_error_index=-999999999

  real(DP),pointer  :: gm1(:) => null()     ! /gm1 - Flux surface averaged 1/R^2
  real(DP),pointer  :: gm1_error_upper(:) => null()
  real(DP),pointer  :: gm1_error_lower(:) => null()
  integer :: gm1_error_index=-999999999

  real(DP),pointer  :: gm2(:) => null()     ! /gm2 - Flux surface averaged grad_rho^2/R^2
  real(DP),pointer  :: gm2_error_upper(:) => null()
  real(DP),pointer  :: gm2_error_lower(:) => null()
  integer :: gm2_error_index=-999999999

  real(DP),pointer  :: gm3(:) => null()     ! /gm3 - Flux surface averaged grad_rho^2
  real(DP),pointer  :: gm3_error_upper(:) => null()
  real(DP),pointer  :: gm3_error_lower(:) => null()
  integer :: gm3_error_index=-999999999

  real(DP),pointer  :: gm4(:) => null()     ! /gm4 - Flux surface averaged 1/B^2
  real(DP),pointer  :: gm4_error_upper(:) => null()
  real(DP),pointer  :: gm4_error_lower(:) => null()
  integer :: gm4_error_index=-999999999

  real(DP),pointer  :: gm5(:) => null()     ! /gm5 - Flux surface averaged B^2
  real(DP),pointer  :: gm5_error_upper(:) => null()
  real(DP),pointer  :: gm5_error_lower(:) => null()
  integer :: gm5_error_index=-999999999

  real(DP),pointer  :: gm6(:) => null()     ! /gm6 - Flux surface averaged grad_rho^2/B^2
  real(DP),pointer  :: gm6_error_upper(:) => null()
  real(DP),pointer  :: gm6_error_lower(:) => null()
  integer :: gm6_error_index=-999999999

  real(DP),pointer  :: gm7(:) => null()     ! /gm7 - Flux surface averaged grad_rho
  real(DP),pointer  :: gm7_error_upper(:) => null()
  real(DP),pointer  :: gm7_error_lower(:) => null()
  integer :: gm7_error_index=-999999999

  real(DP),pointer  :: gm8(:) => null()     ! /gm8 - Flux surface averaged R
  real(DP),pointer  :: gm8_error_upper(:) => null()
  real(DP),pointer  :: gm8_error_lower(:) => null()
  integer :: gm8_error_index=-999999999

  real(DP),pointer  :: gm9(:) => null()     ! /gm9 - Flux surface averaged 1/R
  real(DP),pointer  :: gm9_error_upper(:) => null()
  real(DP),pointer  :: gm9_error_lower(:) => null()
  integer :: gm9_error_index=-999999999

  real(DP),pointer  :: b_average(:) => null()     ! /b_average - Flux surface averaged B
  real(DP),pointer  :: b_average_error_upper(:) => null()
  real(DP),pointer  :: b_average_error_lower(:) => null()
  integer :: b_average_error_index=-999999999

  real(DP),pointer  :: b_field_average(:) => null()     ! /b_field_average - Flux surface averaged B
  real(DP),pointer  :: b_field_average_error_upper(:) => null()
  real(DP),pointer  :: b_field_average_error_lower(:) => null()
  integer :: b_field_average_error_index=-999999999

  real(DP),pointer  :: b_min(:) => null()     ! /b_min - Minimum(B) on the flux surface
  real(DP),pointer  :: b_min_error_upper(:) => null()
  real(DP),pointer  :: b_min_error_lower(:) => null()
  integer :: b_min_error_index=-999999999

  real(DP),pointer  :: b_field_min(:) => null()     ! /b_field_min - Minimum(B) on the flux surface
  real(DP),pointer  :: b_field_min_error_upper(:) => null()
  real(DP),pointer  :: b_field_min_error_lower(:) => null()
  integer :: b_field_min_error_index=-999999999

  real(DP),pointer  :: b_max(:) => null()     ! /b_max - Maximum(B) on the flux surface
  real(DP),pointer  :: b_max_error_upper(:) => null()
  real(DP),pointer  :: b_max_error_lower(:) => null()
  integer :: b_max_error_index=-999999999

  real(DP),pointer  :: b_field_max(:) => null()     ! /b_field_max - Maximum(B) on the flux surface
  real(DP),pointer  :: b_field_max_error_upper(:) => null()
  real(DP),pointer  :: b_field_max_error_lower(:) => null()
  integer :: b_field_max_error_index=-999999999

endtype

type ids_equilibrium_profiles_2d  !    Equilibrium 2D profiles in the poloidal plane
  type (ids_identifier) :: grid_type  ! /grid_type - Selection of one of a set of grid types. 1-rectangular (R,Z) grid, in this case the position arrays
  type (ids_equilibrium_profiles_2d_grid) :: grid  ! /grid - Definition of the 2D grid
  real(DP),pointer  :: r(:,:) => null()     ! /r - Values of the major radius on the grid
  real(DP),pointer  :: r_error_upper(:,:) => null()
  real(DP),pointer  :: r_error_lower(:,:) => null()
  integer :: r_error_index=-999999999

  real(DP),pointer  :: z(:,:) => null()     ! /z - Values of the Height on the grid
  real(DP),pointer  :: z_error_upper(:,:) => null()
  real(DP),pointer  :: z_error_lower(:,:) => null()
  integer :: z_error_index=-999999999

  real(DP),pointer  :: psi(:,:) => null()     ! /psi - Values of the poloidal flux at the grid in the poloidal plane
  real(DP),pointer  :: psi_error_upper(:,:) => null()
  real(DP),pointer  :: psi_error_lower(:,:) => null()
  integer :: psi_error_index=-999999999

  real(DP),pointer  :: theta(:,:) => null()     ! /theta - Values of the poloidal angle on the grid
  real(DP),pointer  :: theta_error_upper(:,:) => null()
  real(DP),pointer  :: theta_error_lower(:,:) => null()
  integer :: theta_error_index=-999999999

  real(DP),pointer  :: phi(:,:) => null()     ! /phi - Toroidal flux
  real(DP),pointer  :: phi_error_upper(:,:) => null()
  real(DP),pointer  :: phi_error_lower(:,:) => null()
  integer :: phi_error_index=-999999999

  real(DP),pointer  :: j_tor(:,:) => null()     ! /j_tor - Toroidal plasma current density
  real(DP),pointer  :: j_tor_error_upper(:,:) => null()
  real(DP),pointer  :: j_tor_error_lower(:,:) => null()
  integer :: j_tor_error_index=-999999999

  real(DP),pointer  :: j_parallel(:,:) => null()     ! /j_parallel - Parallel (to magnetic field) plasma current density
  real(DP),pointer  :: j_parallel_error_upper(:,:) => null()
  real(DP),pointer  :: j_parallel_error_lower(:,:) => null()
  integer :: j_parallel_error_index=-999999999

  real(DP),pointer  :: b_r(:,:) => null()     ! /b_r - R component of the poloidal magnetic field
  real(DP),pointer  :: b_r_error_upper(:,:) => null()
  real(DP),pointer  :: b_r_error_lower(:,:) => null()
  integer :: b_r_error_index=-999999999

  real(DP),pointer  :: b_field_r(:,:) => null()     ! /b_field_r - R component of the poloidal magnetic field
  real(DP),pointer  :: b_field_r_error_upper(:,:) => null()
  real(DP),pointer  :: b_field_r_error_lower(:,:) => null()
  integer :: b_field_r_error_index=-999999999

  real(DP),pointer  :: b_z(:,:) => null()     ! /b_z - Z component of the poloidal magnetic field
  real(DP),pointer  :: b_z_error_upper(:,:) => null()
  real(DP),pointer  :: b_z_error_lower(:,:) => null()
  integer :: b_z_error_index=-999999999

  real(DP),pointer  :: b_field_z(:,:) => null()     ! /b_field_z - Z component of the poloidal magnetic field
  real(DP),pointer  :: b_field_z_error_upper(:,:) => null()
  real(DP),pointer  :: b_field_z_error_lower(:,:) => null()
  integer :: b_field_z_error_index=-999999999

  real(DP),pointer  :: b_tor(:,:) => null()     ! /b_tor - Toroidal component of the magnetic field
  real(DP),pointer  :: b_tor_error_upper(:,:) => null()
  real(DP),pointer  :: b_tor_error_lower(:,:) => null()
  integer :: b_tor_error_index=-999999999

  real(DP),pointer  :: b_field_tor(:,:) => null()     ! /b_field_tor - Toroidal component of the magnetic field
  real(DP),pointer  :: b_field_tor_error_upper(:,:) => null()
  real(DP),pointer  :: b_field_tor_error_lower(:,:) => null()
  integer :: b_field_tor_error_index=-999999999

endtype

type ids_equilibrium_ggd  !    Equilibrium ggd representation
  type (ids_generic_grid_dynamic) :: grid  ! /grid - Grid description
  type (ids_generic_grid_scalar),pointer :: r(:) => null()  ! /r(i) - Values of the major radius on various grid subsets
  type (ids_generic_grid_scalar),pointer :: z(:) => null()  ! /z(i) - Values of the Height on various grid subsets
  type (ids_generic_grid_scalar),pointer :: psi(:) => null()  ! /psi(i) - Values of the poloidal flux, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: phi(:) => null()  ! /phi(i) - Values of the toroidal flux, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: theta(:) => null()  ! /theta(i) - Values of the poloidal angle, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: j_tor(:) => null()  ! /j_tor(i) - Toroidal plasma current density, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: j_parallel(:) => null()  ! /j_parallel(i) - Parallel (to magnetic field) plasma current density, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: b_field_r(:) => null()  ! /b_field_r(i) - R component of the poloidal magnetic field, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: b_field_z(:) => null()  ! /b_field_z(i) - Z component of the poloidal magnetic field, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: b_field_tor(:) => null()  ! /b_field_tor(i) - Toroidal component of the magnetic field, given on various grid subsets
endtype

type ids_equilibrium_time_slice  !    Equilibrium at a given time slice
  type (ids_equilibrium_boundary) :: boundary  ! /boundary - Description of the plasma boundary
  type (ids_equlibrium_global_quantities) :: global_quantities  ! /global_quantities - 0D parameters of the equilibrium
  type (ids_equilibrium_profiles_1d) :: profiles_1d  ! /profiles_1d - Equilibrium profiles (1D radial grid) as a function of the poloidal flux
  type (ids_equilibrium_profiles_2d),pointer :: profiles_2d(:) => null()  ! /profiles_2d(i) - Equilibrium 2D profiles in the poloidal plane. Multiple 2D representations of the equilibrium can be
  type (ids_equilibrium_ggd),pointer :: ggd(:) => null()  ! /ggd(i) - Equilibrium representation using the generic grid description. Multiple GGD representations of the e
  type (ids_equilibrium_coordinate_system) :: coordinate_system  ! /coordinate_system - Flux surface coordinate system on a square grid of flux and poloidal angle
  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_equilibrium  !    Description of a 2D, axi-symmetric, tokamak equilibrium; result of an equilibrium code.
  type (ids_ids_properties) :: ids_properties  ! /equilibrium/ids_properties -
  type (ids_b_tor_vacuum_1) :: vacuum_toroidal_field  ! /equilibrium/vacuum_toroidal_field - Characteristics of the vacuum toroidal field (used in rho_tor definition and in the normalization of
  type (ids_equilibrium_time_slice),pointer :: time_slice(:) => null()  ! /equilibrium/time_slice(i) - Set of equilibria at various time slices
  type (ids_code) :: code  ! /equilibrium/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include interfero_polarimeter/dd_interfero_polarimeter.xsd
! SPECIAL STRUCTURE data / time
type ids_interfero_polarimeter_channel_wavelength_interf_n_e_line  !    Line integrated density estimated from this wavelength
  real(DP), pointer  :: data(:) => null()     ! /n_e_line - Line integrated density estimated from this wavelength
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_interfero_polarimeter_channel_wavelength_interf  !    Value of the wavelength and density estimators associated to an interferometry wavelength
  real(DP)  :: value=-9.0D40       ! /value - Wavelength value
  real(DP)  :: value_error_upper=-9.0D40
  real(DP)  :: value_error_lower=-9.0D40
  integer :: value_error_index=-999999999

  type (ids_interfero_polarimeter_channel_wavelength_interf_n_e_line) :: n_e_line  ! /n_e_line - Line integrated density estimated from this wavelength
endtype

! SPECIAL STRUCTURE data / time
type ids_interfero_polarimeter_channel_n_e_line  !    Line integrated density, possibly obtained by a combination of multiple interferometry wavelengths
  real(DP), pointer  :: data(:) => null()     ! /n_e_line - Line integrated density, possibly obtained by a combination of multiple interferometry wavelengths
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_interfero_polarimeter_channel_faraday_angle  !    Faraday angle (variation of the Faraday angle induced by crossing the plasma)
  real(DP), pointer  :: data(:) => null()     ! /faraday_angle - Faraday angle (variation of the Faraday angle induced by crossing the plasma)
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_interfero_polarimeter_channel_ellipticity  !    Ellipticity
  real(DP), pointer  :: data(:) => null()     ! /ellipticity - Ellipticity
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_interfero_polarimeter_channel_validity_timed  !    Indicator of the validity of the channel as a function of time (0 means valid, negative values mean non-valid)
  integer, pointer  :: data(:) => null()      ! /validity_timed - Indicator of the validity of the channel as a function of time (0 means valid, negative values mean
  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_interfero_polarimeter_channel  !    Charge exchange channel
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of the channel
  character(len=132), dimension(:), pointer ::identifier => null()       ! /identifier - ID of the channel
  type (ids_line_of_sight_3points) :: line_of_sight  ! /line_of_sight - Description of the line of sight of the channel, defined by two points when the beam is not reflecte
  real(DP)  :: wavelength_polarimetry=-9.0D40       ! /wavelength_polarimetry - Wavelength used for polarimetry
  real(DP)  :: wavelength_polarimetry_error_upper=-9.0D40
  real(DP)  :: wavelength_polarimetry_error_lower=-9.0D40
  integer :: wavelength_polarimetry_error_index=-999999999

  type (ids_interfero_polarimeter_channel_wavelength_interf),pointer :: wavelength_interferometry(:) => null()  ! /wavelength_interferometry(i) - Set of wavelengths used for interferometry
  real(DP)  :: polarisation_initial=-9.0D40       ! /polarisation_initial - Initial polarisation vector before entering the plasma
  real(DP)  :: polarisation_initial_error_upper=-9.0D40
  real(DP)  :: polarisation_initial_error_lower=-9.0D40
  integer :: polarisation_initial_error_index=-999999999

  real(DP)  :: ellipticity_initial=-9.0D40       ! /ellipticity_initial - Initial ellipticity before entering the plasma
  real(DP)  :: ellipticity_initial_error_upper=-9.0D40
  real(DP)  :: ellipticity_initial_error_lower=-9.0D40
  integer :: ellipticity_initial_error_index=-999999999

  type (ids_interfero_polarimeter_channel_n_e_line) :: n_e_line  ! /n_e_line - Line integrated density, possibly obtained by a combination of multiple interferometry wavelengths
  type (ids_interfero_polarimeter_channel_faraday_angle) :: faraday_angle  ! /faraday_angle - Faraday angle (variation of the Faraday angle induced by crossing the plasma)
  type (ids_interfero_polarimeter_channel_ellipticity) :: ellipticity  ! /ellipticity - Ellipticity
  type (ids_interfero_polarimeter_channel_validity_timed) :: validity_timed  ! /validity_timed - Indicator of the validity of the channel as a function of time (0 means valid, negative values mean
  integer  :: validity=-999999999       ! /validity - Indicator of the validity of the channel for the whole acquisition period (0 means valid, negative v
endtype

type ids_interfero_polarimeter  !    Interfero-polarimeter diagnostic
  type (ids_ids_properties) :: ids_properties  ! /interfero_polarimeter/ids_properties -
  type (ids_interfero_polarimeter_channel),pointer :: channel(:) => null()  ! /interfero_polarimeter/channel(i) - Set of channels (lines-of-sight)
  type (ids_code) :: code  ! /interfero_polarimeter/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include iron_core/dd_iron_core.xsd
! SPECIAL STRUCTURE data / time
type ids_iron_core_segment_magnetisation_r  !    Magnetisation M of the iron segment along the major radius axis, assumed to be constant inside a given iron segment. Reminder : H
  real(DP), pointer  :: data(:) => null()     ! /magnetisation_r - Magnetisation M of the iron segment along the major radius axis, assumed to be constant inside a giv
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_iron_core_segment_magnetisation_z  !    Magnetisation M of the iron segment along the vertical axis, assumed to be constant inside a given iron segment. Reminder : H = 1/
  real(DP), pointer  :: data(:) => null()     ! /magnetisation_z - Magnetisation M of the iron segment along the vertical axis, assumed to be constant inside a given i
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_iron_core_segment  !    Segment of the iron core
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of the segment
  character(len=132), dimension(:), pointer ::identifier => null()       ! /identifier - ID of the segment
  real(DP),pointer  :: b_field(:) => null()     ! /b_field - Array of magnetic field values, for each of which the relative permeability is given
  real(DP),pointer  :: b_field_error_upper(:) => null()
  real(DP),pointer  :: b_field_error_lower(:) => null()
  integer :: b_field_error_index=-999999999

  real(DP),pointer  :: permeability_relative(:) => null()     ! /permeability_relative - Relative permeability of the iron segment
  real(DP),pointer  :: permeability_relative_error_upper(:) => null()
  real(DP),pointer  :: permeability_relative_error_lower(:) => null()
  integer :: permeability_relative_error_index=-999999999

  type (ids_outline_2d_geometry_static) :: geometry  ! /geometry - Cross-sectional shape of the segment
  type (ids_iron_core_segment_magnetisation_r) :: magnetisation_r  ! /magnetisation_r - Magnetisation M of the iron segment along the major radius axis, assumed to be constant inside a giv
  type (ids_iron_core_segment_magnetisation_z) :: magnetisation_z  ! /magnetisation_z - Magnetisation M of the iron segment along the vertical axis, assumed to be constant inside a given i
endtype

type ids_iron_core  !    Iron core description
  type (ids_ids_properties) :: ids_properties  ! /iron_core/ids_properties -
  type (ids_iron_core_segment),pointer :: segment(:) => null()  ! /iron_core/segment(i) - The iron core is describred as a set of segments
  type (ids_code) :: code  ! /iron_core/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include magnetics/dd_magnetics.xsd
! SPECIAL STRUCTURE data / time
type ids_magnetics_flux_loop_flux  !    Measured flux
  real(DP), pointer  :: data(:) => null()     ! /flux - Measured flux
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_magnetics_flux_loop  !    Flux loops
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of the flux loop
  character(len=132), dimension(:), pointer ::identifier => null()       ! /identifier - ID of the flux loop
  type (ids_rzphi0d_static),pointer :: position(:) => null()  ! /position(i) - List of (R,Z,phi) points defining the position of the loop (see data structure documentation FLUXLOO
  type (ids_magnetics_flux_loop_flux) :: flux  ! /flux - Measured flux
endtype

! SPECIAL STRUCTURE data / time
type ids_magnetics_bpol_probe_field  !    Measured magnetic field
  real(DP), pointer  :: data(:) => null()     ! /field - Measured magnetic field
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_magnetics_bpol_probe  !    Poloidal field probes
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of the probe
  character(len=132), dimension(:), pointer ::identifier => null()       ! /identifier - ID of the probe
  type (ids_rzphi0d_static) :: position  ! /position - R, Z, Phi position of the coil centre
  real(DP)  :: poloidal_angle=-9.0D40       ! /poloidal_angle - Poloidal angle of the coil orientation
  real(DP)  :: poloidal_angle_error_upper=-9.0D40
  real(DP)  :: poloidal_angle_error_lower=-9.0D40
  integer :: poloidal_angle_error_index=-999999999

  real(DP)  :: toroidal_angle=-9.0D40       ! /toroidal_angle - Toroidal angle of coil orientation (0 if fully in the poloidal plane)
  real(DP)  :: toroidal_angle_error_upper=-9.0D40
  real(DP)  :: toroidal_angle_error_lower=-9.0D40
  integer :: toroidal_angle_error_index=-999999999

  real(DP)  :: area=-9.0D40       ! /area - Area of each turn of the coil
  real(DP)  :: area_error_upper=-9.0D40
  real(DP)  :: area_error_lower=-9.0D40
  integer :: area_error_index=-999999999

  real(DP)  :: length=-9.0D40       ! /length - Length of the coil
  real(DP)  :: length_error_upper=-9.0D40
  real(DP)  :: length_error_lower=-9.0D40
  integer :: length_error_index=-999999999

  integer  :: turns=-999999999       ! /turns - Turns in the coil, including sign
  type (ids_magnetics_bpol_probe_field) :: field  ! /field - Measured magnetic field
endtype

! SPECIAL STRUCTURE data / time
type ids_magnetics_method_ip  !    Plasma current. Positive sign means anti-clockwise when viewed from above.
  real(DP), pointer  :: data(:) => null()     ! /ip - Plasma current. Positive sign means anti-clockwise when viewed from above.
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_magnetics_method_diamagnetic_flux  !    Diamagnetic flux
  real(DP), pointer  :: data(:) => null()     ! /diamagnetic_flux - Diamagnetic flux
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_magnetics_method  !    Processed quantities derived from the magnetic measurements, using various methods
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of the data processing method
  type (ids_magnetics_method_ip) :: ip  ! /ip - Plasma current. Positive sign means anti-clockwise when viewed from above.
  type (ids_magnetics_method_diamagnetic_flux) :: diamagnetic_flux  ! /diamagnetic_flux - Diamagnetic flux
endtype

type ids_magnetics  !    Magnetic diagnostics for equilibrium identification and plasma shape control.
  type (ids_ids_properties) :: ids_properties  ! /magnetics/ids_properties -
  type (ids_magnetics_flux_loop),pointer :: flux_loop(:) => null()  ! /magnetics/flux_loop(i) - Flux loops; partial flux loops can be described
  type (ids_magnetics_bpol_probe),pointer :: bpol_probe(:) => null()  ! /magnetics/bpol_probe(i) - Poloidal field probes
  type (ids_magnetics_method),pointer :: method(:) => null()  ! /magnetics/method(i) - A method generating processed quantities derived from the magnetic measurements
  type (ids_code) :: code  ! /magnetics/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include mhd_linear/dd_mhd_linear.xsd
type ids_mhd_linear_vector  !    Vector structure for the MHD IDS
  real(DP),pointer  :: coordinate1(:,:) => null()     ! /coordinate1 - First coordinate (radial)
  real(DP),pointer  :: coordinate1_error_upper(:,:) => null()
  real(DP),pointer  :: coordinate1_error_lower(:,:) => null()
  integer :: coordinate1_error_index=-999999999

  real(DP),pointer  :: coordinate2(:,:) => null()     ! /coordinate2 - Second coordinate (poloidal)
  real(DP),pointer  :: coordinate2_error_upper(:,:) => null()
  real(DP),pointer  :: coordinate2_error_lower(:,:) => null()
  integer :: coordinate2_error_index=-999999999

  real(DP),pointer  :: coordinate3(:,:) => null()     ! /coordinate3 - Third coordinate (toroidal)
  real(DP),pointer  :: coordinate3_error_upper(:,:) => null()
  real(DP),pointer  :: coordinate3_error_lower(:,:) => null()
  integer :: coordinate3_error_index=-999999999

endtype

type ids_mhd_linear_time_slice_toroidal_mode_vaccuum  !    MHD modes in the vaccuum
  type (ids_identifier) :: poloidal_grid_type  ! /poloidal_grid_type - Selection of one of a set of grid types. 1: poloidal mode number; 2: poloidal angle
  integer,pointer  :: poloidal_grid(:,:) => null()     ! /poloidal_grid - Array of poloidal grid values for the various radial positions
  type (ids_mhd_linear_vector) :: a_field_perturbed  ! /a_field_perturbed - Pertubed vector potential for given toroidal mode number
  type (ids_mhd_linear_vector) :: b_field_perturbed  ! /b_field_perturbed - Pertubed magnetic field for given toroidal mode number
endtype

type ids_mhd_linear_time_slice_toroidal_mode_plasma  !    MHD modes in the confined plasma
  type (ids_identifier) :: poloidal_grid_type  ! /poloidal_grid_type - Selection of one of a set of grid types. 1: poloidal mode number; 2: poloidal angle
  integer,pointer  :: poloidal_grid(:,:) => null()     ! /poloidal_grid - Array of poloidal grid values for the various radial positions
  real(DP),pointer  :: displacement_perpendicular(:,:) => null()     ! /displacement_perpendicular - Perpendicular displacement of the modes
  real(DP),pointer  :: displacement_perpendicular_error_upper(:,:) => null()
  real(DP),pointer  :: displacement_perpendicular_error_lower(:,:) => null()
  integer :: displacement_perpendicular_error_index=-999999999

  real(DP),pointer  :: displacement_parallel(:,:) => null()     ! /displacement_parallel - Parallel displacement of the modes
  real(DP),pointer  :: displacement_parallel_error_upper(:,:) => null()
  real(DP),pointer  :: displacement_parallel_error_lower(:,:) => null()
  integer :: displacement_parallel_error_index=-999999999

  real(DP),pointer  :: tau_alfven(:) => null()     ! /tau_alfven - Alven time=R/vA=R0 sqrt(mi ni(rho))/B0
  real(DP),pointer  :: tau_alfven_error_upper(:) => null()
  real(DP),pointer  :: tau_alfven_error_lower(:) => null()
  integer :: tau_alfven_error_index=-999999999

  real(DP),pointer  :: tau_resistive(:) => null()     ! /tau_resistive - Resistive time = mu_0 rho*rho/1.22/eta_neo
  real(DP),pointer  :: tau_resistive_error_upper(:) => null()
  real(DP),pointer  :: tau_resistive_error_lower(:) => null()
  integer :: tau_resistive_error_index=-999999999

  type (ids_mhd_linear_vector) :: a_field_perturbed  ! /a_field_perturbed - Pertubed vector potential for given toroidal mode number
  type (ids_mhd_linear_vector) :: b_field_perturbed  ! /b_field_perturbed - Pertubed magnetic field for given toroidal mode number
  type (ids_mhd_linear_vector) :: velocity_perturbed  ! /velocity_perturbed - Pertubed velocity for given toroidal mode number
  real(DP),pointer  :: pressure_perturbed(:,:) => null()     ! /pressure_perturbed - Perturbed pressure for given toroidal mode number
  real(DP),pointer  :: pressure_perturbed_error_upper(:,:) => null()
  real(DP),pointer  :: pressure_perturbed_error_lower(:,:) => null()
  integer :: pressure_perturbed_error_index=-999999999

  real(DP),pointer  :: mass_density_perturbed(:,:) => null()     ! /mass_density_perturbed - Perturbed mass density for given toroidal mode number
  real(DP),pointer  :: mass_density_perturbed_error_upper(:,:) => null()
  real(DP),pointer  :: mass_density_perturbed_error_lower(:,:) => null()
  integer :: mass_density_perturbed_error_index=-999999999

  real(DP),pointer  :: temperature_perturbed(:,:) => null()     ! /temperature_perturbed - Perturbed temperature for given toroidal mode number
  real(DP),pointer  :: temperature_perturbed_error_upper(:,:) => null()
  real(DP),pointer  :: temperature_perturbed_error_lower(:,:) => null()
  integer :: temperature_perturbed_error_index=-999999999

endtype

type ids_mhd_linear_time_slice_toroidal_modes  !    Vector of toroidal modes
  integer  :: n_tor=-999999999       ! /n_tor - Toroidal mode number of the MHD mode
  real(DP)  :: growthrate=-9.0D40       ! /growthrate - Linear growthrate of the mode
  real(DP)  :: growthrate_error_upper=-9.0D40
  real(DP)  :: growthrate_error_lower=-9.0D40
  integer :: growthrate_error_index=-999999999

  real(DP)  :: frequency=-9.0D40       ! /frequency - Frequency of the mode
  real(DP)  :: frequency_error_upper=-9.0D40
  real(DP)  :: frequency_error_lower=-9.0D40
  integer :: frequency_error_index=-999999999

  type (ids_mhd_linear_time_slice_toroidal_mode_plasma) :: plasma  ! /plasma - MHD modes in the confined plasma
  type (ids_mhd_linear_time_slice_toroidal_mode_vaccuum) :: vaccuum  ! /vaccuum - MHD modes in the vaccum
endtype

type ids_mhd_linear_time_slice  !    Time slice description of linear MHD stability
  type (ids_core_radial_grid) :: grid_plasma  ! /grid_plasma - Radial grid within plasma (for quantities below toroidal_mode/plasma)
  type (ids_mhd_linear_time_slice_toroidal_modes),pointer :: toroidal_mode(:) => null()  ! /toroidal_mode(i) - Vector of toroidal modes
  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_mhd_linear  !    Magnetohydronamic linear stability
  type (ids_ids_properties) :: ids_properties  ! /mhd_linear/ids_properties -
  type (ids_b_tor_vacuum_1) :: vacuum_toroidal_field  ! /mhd_linear/vacuum_toroidal_field - Characteristics of the vacuum toroidal field (used in rho_tor definition and in the normalization of
  type (ids_mhd_linear_time_slice),pointer :: time_slice(:) => null()  ! /mhd_linear/time_slice(i) - Core plasma radial profiles for various time slices
  type (ids_code) :: code  ! /mhd_linear/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include nbi/dd_nbi.xsd
type ids_nbi_unit_beamlets_group_beamlets  !    Detailed information on beamlets
  type (ids_rzphi1d_static) :: positions  ! /positions - Position of each beamlet
  real(DP),pointer  :: tangency_radii(:) => null()     ! /tangency_radii - Tangency radius (major radius where the central line of a beamlet is tangent to a circle around the
  real(DP),pointer  :: tangency_radii_error_upper(:) => null()
  real(DP),pointer  :: tangency_radii_error_lower(:) => null()
  integer :: tangency_radii_error_index=-999999999

  real(DP),pointer  :: angles(:) => null()     ! /angles - Angle of inclination between a line at the centre of a beamlet and the horiontal plane, for each bea
  real(DP),pointer  :: angles_error_upper(:) => null()
  real(DP),pointer  :: angles_error_lower(:) => null()
  integer :: angles_error_index=-999999999

  real(DP),pointer  :: power_fractions(:) => null()     ! /power_fractions - Fraction of power of a unit injected by each beamlet
  real(DP),pointer  :: power_fractions_error_upper(:) => null()
  real(DP),pointer  :: power_fractions_error_lower(:) => null()
  integer :: power_fractions_error_index=-999999999

endtype

type ids_nbi_unit_beamlets_group_divergence  !    Describes a divergence component of a group of beamlets
  real(DP)  :: particles_fraction=-9.0D40       ! /particles_fraction - Fraction of injected particles in the component
  real(DP)  :: particles_fraction_error_upper=-9.0D40
  real(DP)  :: particles_fraction_error_lower=-9.0D40
  integer :: particles_fraction_error_index=-999999999

  real(DP)  :: vertical=-9.0D40       ! /vertical - The vertical beamlet divergence of the component. Here the divergence is defined for Gaussian beams
  real(DP)  :: vertical_error_upper=-9.0D40
  real(DP)  :: vertical_error_lower=-9.0D40
  integer :: vertical_error_index=-999999999

  real(DP)  :: horizontal=-9.0D40       ! /horizontal - The horiztonal beamlet divergence of the component. Here the divergence is defined for Gaussian beam
  real(DP)  :: horizontal_error_upper=-9.0D40
  real(DP)  :: horizontal_error_lower=-9.0D40
  integer :: horizontal_error_index=-999999999

endtype

type ids_nbi_unit_beamlets_group_focus  !    Describes of a group of beamlets is focused
  real(DP)  :: focal_length_horizontal=-9.0D40       ! /focal_length_horizontal - Horizontal focal length along the beam line, i.e. the point along the centre of the beamlet-group wh
  real(DP)  :: focal_length_horizontal_error_upper=-9.0D40
  real(DP)  :: focal_length_horizontal_error_lower=-9.0D40
  integer :: focal_length_horizontal_error_index=-999999999

  real(DP)  :: focal_length_vertical=-9.0D40       ! /focal_length_vertical - Vertical focal length along the beam line, i.e. the point along the centre of the beamlet-group wher
  real(DP)  :: focal_length_vertical_error_upper=-9.0D40
  real(DP)  :: focal_length_vertical_error_lower=-9.0D40
  integer :: focal_length_vertical_error_index=-999999999

  real(DP)  :: width_min_horizontal=-9.0D40       ! /width_min_horizontal - The horizontal width of the beamlets group at the at the horizontal focal point
  real(DP)  :: width_min_horizontal_error_upper=-9.0D40
  real(DP)  :: width_min_horizontal_error_lower=-9.0D40
  integer :: width_min_horizontal_error_index=-999999999

  real(DP)  :: width_min_vertical=-9.0D40       ! /width_min_vertical - The vertical width of the beamlets group at the at the vertical focal point
  real(DP)  :: width_min_vertical_error_upper=-9.0D40
  real(DP)  :: width_min_vertical_error_lower=-9.0D40
  integer :: width_min_vertical_error_index=-999999999

endtype

type ids_nbi_unit_beamlets_group  !    Group of beamlets
  type (ids_rzphi0d_static) :: position  ! /position - R, Z, Phi position of the coil centre
  real(DP)  :: tangency_radius=-9.0D40       ! /tangency_radius - Tangency radius (major radius where the central line of a NBI unit is tangent to a circle around the
  real(DP)  :: tangency_radius_error_upper=-9.0D40
  real(DP)  :: tangency_radius_error_lower=-9.0D40
  integer :: tangency_radius_error_index=-999999999

  real(DP)  :: angle=-9.0D40       ! /angle - Angle of inclination between a beamlet at the centre of the injection unit surface and the horiontal
  real(DP)  :: angle_error_upper=-9.0D40
  real(DP)  :: angle_error_lower=-9.0D40
  integer :: angle_error_index=-999999999

  integer  :: direction=-999999999       ! /direction - Direction of the beam seen from above the torus: -1 = clockwise; 1 = counter clockwise
  real(DP)  :: width_horizontal=-9.0D40       ! /width_horizontal - Horizontal width of the beam group at the injection unit surface (or grounded grid)
  real(DP)  :: width_horizontal_error_upper=-9.0D40
  real(DP)  :: width_horizontal_error_lower=-9.0D40
  integer :: width_horizontal_error_index=-999999999

  real(DP)  :: width_vertical=-9.0D40       ! /width_vertical - Vertical width of the beam group at the injection unit surface (or grounded grid)
  real(DP)  :: width_vertical_error_upper=-9.0D40
  real(DP)  :: width_vertical_error_lower=-9.0D40
  integer :: width_vertical_error_index=-999999999

  type (ids_nbi_unit_beamlets_group_focus) :: focus  ! /focus - Describes how the beamlet group is focused
  type (ids_nbi_unit_beamlets_group_divergence),pointer :: divergence_component(:) => null()  ! /divergence_component(i) - Detailed information on beamlet divergence. Divergence is described as a superposition of Gaussian c
  type (ids_nbi_unit_beamlets_group_beamlets) :: beamlets  ! /beamlets - Detailed information on beamlets
endtype

! SPECIAL STRUCTURE data / time
type ids_nbi_unit_power  !    Power
  real(DP), pointer  :: data(:) => null()     ! /power - Power
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_nbi_unit_energy  !    Full energy of the injected species (acceleration of a single atom)
  real(DP), pointer  :: data(:) => null()     ! /energy - Full energy of the injected species (acceleration of a single atom)
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_nbi_unit_beam_current_fraction  !    Fractions of beam current distributed among the different energies, the first index corresponds to the fast neutrals energy (1:ful
  real(DP), pointer  :: data(:,:) => null()     ! /beam_current_fraction - Fractions of beam current distributed among the different energies, the first index corresponds to t
  real(DP), pointer  :: data_error_upper(:,:) => null()
  real(DP), pointer  :: data_error_lower(:,:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_nbi_unit_beam_power_fraction  !    Fractions of beam power distributed among the different energies, the first index corresponds to the fast neutrals energy (1:full,
  real(DP), pointer  :: data(:,:) => null()     ! /beam_power_fraction - Fractions of beam power distributed among the different energies, the first index corresponds to the
  real(DP), pointer  :: data_error_upper(:,:) => null()
  real(DP), pointer  :: data_error_lower(:,:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_nbi_unit  !    NBI unit
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of the NBI unit
  character(len=132), dimension(:), pointer ::identifier => null()       ! /identifier - ID of the NBI unit
  type (ids_plasma_composition_species) :: species  ! /species - Injected species
  type (ids_nbi_unit_power) :: power  ! /power - Power
  type (ids_nbi_unit_energy) :: energy  ! /energy - Full energy of the injected species (acceleration of a single atom)
  type (ids_nbi_unit_beam_current_fraction) :: beam_current_fraction  ! /beam_current_fraction - Fractions of beam current distributed among the different energies, the first index corresponds to t
  type (ids_nbi_unit_beam_power_fraction) :: beam_power_fraction  ! /beam_power_fraction - Fractions of beam power distributed among the different energies, the first index corresponds to the
  type (ids_nbi_unit_beamlets_group),pointer :: beamlets_group(:) => null()  ! /beamlets_group(i) - Group of beamlets with common vertical and horizontal focal point. If there are no common focal poin
endtype

type ids_nbi  !    Neutral Beam Injection systems and description of the fast neutrals that arrive into the torus
  type (ids_ids_properties) :: ids_properties  ! /nbi/ids_properties -
  type (ids_nbi_unit),pointer :: unit(:) => null()  ! /nbi/unit(i) - The NBI system is described as a set of units of which the power can be controlled individually.
  type (ids_code) :: code  ! /nbi/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include ntms/dd_ntms.xsd
type ids_ntm_time_slice_mode_detailed_evolution_deltaw  !    deltaw contribution to the Rutherford equation (detailed evolution)
  real(DP),pointer  :: value(:) => null()     ! /value - Value of the contribution
  real(DP),pointer  :: value_error_upper(:) => null()
  real(DP),pointer  :: value_error_lower(:) => null()
  integer :: value_error_index=-999999999

  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of the contribution
endtype

type ids_ntm_time_slice_mode_detailed_evolution_torque  !    torque contribution to the Rutherford equation (detailed evolution)
  real(DP),pointer  :: value(:) => null()     ! /value - Value of the contribution
  real(DP),pointer  :: value_error_upper(:) => null()
  real(DP),pointer  :: value_error_lower(:) => null()
  integer :: value_error_index=-999999999

  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of the contribution
endtype

type ids_ntm_time_slice_mode_detailed_evolution  !    Detailed NTM evolution on a finer timebase than the time_slice array of structure
  real(DP),pointer  :: time_detailed(:) => null()     ! /time_detailed - Time array used to describe the detailed evolution of the NTM
  real(DP),pointer  :: time_detailed_error_upper(:) => null()
  real(DP),pointer  :: time_detailed_error_lower(:) => null()
  integer :: time_detailed_error_index=-999999999

  real(DP),pointer  :: width(:) => null()     ! /width - Full width of the mode
  real(DP),pointer  :: width_error_upper(:) => null()
  real(DP),pointer  :: width_error_lower(:) => null()
  integer :: width_error_index=-999999999

  real(DP),pointer  :: dwidth_dt(:) => null()     ! /dwidth_dt - Time derivative of the full width of the mode
  real(DP),pointer  :: dwidth_dt_error_upper(:) => null()
  real(DP),pointer  :: dwidth_dt_error_lower(:) => null()
  integer :: dwidth_dt_error_index=-999999999

  real(DP),pointer  :: phase(:) => null()     ! /phase - Phase of the mode
  real(DP),pointer  :: phase_error_upper(:) => null()
  real(DP),pointer  :: phase_error_lower(:) => null()
  integer :: phase_error_index=-999999999

  real(DP),pointer  :: dphase_dt(:) => null()     ! /dphase_dt - Time derivative of the phase of the mode
  real(DP),pointer  :: dphase_dt_error_upper(:) => null()
  real(DP),pointer  :: dphase_dt_error_lower(:) => null()
  integer :: dphase_dt_error_index=-999999999

  real(DP),pointer  :: frequency(:) => null()     ! /frequency - Frequency of the mode
  real(DP),pointer  :: frequency_error_upper(:) => null()
  real(DP),pointer  :: frequency_error_lower(:) => null()
  integer :: frequency_error_index=-999999999

  real(DP),pointer  :: dfrequency_dt(:) => null()     ! /dfrequency_dt - Time derivative of the frequency of the mode
  real(DP),pointer  :: dfrequency_dt_error_upper(:) => null()
  real(DP),pointer  :: dfrequency_dt_error_lower(:) => null()
  integer :: dfrequency_dt_error_index=-999999999

  integer  :: n_tor=-999999999       ! /n_tor - Toroidal mode number
  integer  :: m_pol=-999999999       ! /m_pol - Poloidal mode number
  type (ids_ntm_time_slice_mode_detailed_evolution_deltaw),pointer :: deltaw(:) => null()  ! /deltaw(i) - deltaw contributions to the Rutherford equation
  type (ids_ntm_time_slice_mode_detailed_evolution_torque),pointer :: torque(:) => null()  ! /torque(i) - torque contributions to the Rutherford equation
  character(len=132), dimension(:), pointer ::calculation_method => null()       ! /calculation_method - Description of how the mode evolution is calculated
  real(DP),pointer  :: delta_diff(:,:) => null()     ! /delta_diff - Extra diffusion coefficient for the transport equations of Te, ne, Ti
  real(DP),pointer  :: delta_diff_error_upper(:,:) => null()
  real(DP),pointer  :: delta_diff_error_lower(:,:) => null()
  integer :: delta_diff_error_index=-999999999

  real(DP),pointer  :: rho_tor_norm(:) => null()     ! /rho_tor_norm - Normalised flux coordinate on which the mode is centred
  real(DP),pointer  :: rho_tor_norm_error_upper(:) => null()
  real(DP),pointer  :: rho_tor_norm_error_lower(:) => null()
  integer :: rho_tor_norm_error_index=-999999999

  real(DP),pointer  :: rho_tor(:) => null()     ! /rho_tor - Flux coordinate on which the mode is centred
  real(DP),pointer  :: rho_tor_error_upper(:) => null()
  real(DP),pointer  :: rho_tor_error_lower(:) => null()
  integer :: rho_tor_error_index=-999999999

endtype

type ids_ntm_time_slice_mode_evolution_deltaw  !    deltaw contribution to the Rutherford equation
  real(DP)  :: value=-9.0D40       ! /value - Value of the contribution
  real(DP)  :: value_error_upper=-9.0D40
  real(DP)  :: value_error_lower=-9.0D40
  integer :: value_error_index=-999999999

  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of the contribution
endtype

type ids_ntm_time_slice_mode_evolution_torque  !    torque contribution to the Rutherford equation
  real(DP)  :: value=-9.0D40       ! /value - Value of the contribution
  real(DP)  :: value_error_upper=-9.0D40
  real(DP)  :: value_error_lower=-9.0D40
  integer :: value_error_index=-999999999

  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of the contribution
endtype

type ids_ntm_time_slice_mode_onset  !    Onset characteristics of an NTM
  real(DP)  :: width=-9.0D40       ! /width - Seed island full width at onset time
  real(DP)  :: width_error_upper=-9.0D40
  real(DP)  :: width_error_lower=-9.0D40
  integer :: width_error_index=-999999999

  real(DP)  :: time_onset=-9.0D40       ! /time_onset - Onset time
  real(DP)  :: time_onset_error_upper=-9.0D40
  real(DP)  :: time_onset_error_lower=-9.0D40
  integer :: time_onset_error_index=-999999999

  real(DP)  :: time_offset=-9.0D40       ! /time_offset - Offset time (when a mode disappears). If the mode reappears later in the simulation, use another ind
  real(DP)  :: time_offset_error_upper=-9.0D40
  real(DP)  :: time_offset_error_lower=-9.0D40
  integer :: time_offset_error_index=-999999999

  real(DP)  :: phase=-9.0D40       ! /phase - Phase of the mode at onset
  real(DP)  :: phase_error_upper=-9.0D40
  real(DP)  :: phase_error_lower=-9.0D40
  integer :: phase_error_index=-999999999

  integer  :: n_tor=-999999999       ! /n_tor - Toroidal mode number
  integer  :: m_pol=-999999999       ! /m_pol - Poloidal mode number
  character(len=132), dimension(:), pointer ::cause => null()       ! /cause - Cause of the mode onset
endtype

type ids_ntm_time_slice_mode  !    Description of an NTM
  type (ids_ntm_time_slice_mode_onset) :: onset  ! /onset - NTM onset characteristics
  real(DP)  :: width=-9.0D40       ! /width - Full width of the mode
  real(DP)  :: width_error_upper=-9.0D40
  real(DP)  :: width_error_lower=-9.0D40
  integer :: width_error_index=-999999999

  real(DP)  :: dwidth_dt=-9.0D40       ! /dwidth_dt - Time derivative of the full width of the mode
  real(DP)  :: dwidth_dt_error_upper=-9.0D40
  real(DP)  :: dwidth_dt_error_lower=-9.0D40
  integer :: dwidth_dt_error_index=-999999999

  real(DP)  :: phase=-9.0D40       ! /phase - Phase of the mode
  real(DP)  :: phase_error_upper=-9.0D40
  real(DP)  :: phase_error_lower=-9.0D40
  integer :: phase_error_index=-999999999

  real(DP)  :: dphase_dt=-9.0D40       ! /dphase_dt - Time derivative of the phase of the mode
  real(DP)  :: dphase_dt_error_upper=-9.0D40
  real(DP)  :: dphase_dt_error_lower=-9.0D40
  integer :: dphase_dt_error_index=-999999999

  real(DP)  :: frequency=-9.0D40       ! /frequency - Frequency of the mode
  real(DP)  :: frequency_error_upper=-9.0D40
  real(DP)  :: frequency_error_lower=-9.0D40
  integer :: frequency_error_index=-999999999

  real(DP)  :: dfrequency_dt=-9.0D40       ! /dfrequency_dt - Time derivative of the frequency of the mode
  real(DP)  :: dfrequency_dt_error_upper=-9.0D40
  real(DP)  :: dfrequency_dt_error_lower=-9.0D40
  integer :: dfrequency_dt_error_index=-999999999

  integer  :: n_tor=-999999999       ! /n_tor - Toroidal mode number
  integer  :: m_pol=-999999999       ! /m_pol - Poloidal mode number
  type (ids_ntm_time_slice_mode_evolution_deltaw),pointer :: deltaw(:) => null()  ! /deltaw(i) - deltaw contributions to the Rutherford equation
  type (ids_ntm_time_slice_mode_evolution_torque),pointer :: torque(:) => null()  ! /torque(i) - torque contributions to the Rutherford equation
  character(len=132), dimension(:), pointer ::calculation_method => null()       ! /calculation_method - Description of how the mode evolution is calculated
  real(DP),pointer  :: delta_diff(:) => null()     ! /delta_diff - Extra diffusion coefficient for the transport equations of Te, ne, Ti
  real(DP),pointer  :: delta_diff_error_upper(:) => null()
  real(DP),pointer  :: delta_diff_error_lower(:) => null()
  integer :: delta_diff_error_index=-999999999

  real(DP)  :: rho_tor_norm=-9.0D40       ! /rho_tor_norm - Normalised flux coordinate on which the mode is centred
  real(DP)  :: rho_tor_norm_error_upper=-9.0D40
  real(DP)  :: rho_tor_norm_error_lower=-9.0D40
  integer :: rho_tor_norm_error_index=-999999999

  real(DP)  :: rho_tor=-9.0D40       ! /rho_tor - Flux coordinate on which the mode is centred
  real(DP)  :: rho_tor_error_upper=-9.0D40
  real(DP)  :: rho_tor_error_lower=-9.0D40
  integer :: rho_tor_error_index=-999999999

  type (ids_ntm_time_slice_mode_detailed_evolution) :: detailed_evolution  ! /detailed_evolution - Detailed NTM evolution on a finer timebase than the time_slice array of structure
endtype

type ids_ntm_time_slice  !    Time slice description of NTMs
  type (ids_ntm_time_slice_mode),pointer :: mode(:) => null()  ! /mode(i) - List of the various NTM modes appearing during the simulation. If a mode appears several times, use
  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_ntms  !    Description of neoclassical tearing modes
  type (ids_ids_properties) :: ids_properties  ! /ntms/ids_properties -
  type (ids_b_tor_vacuum_1) :: vacuum_toroidal_field  ! /ntms/vacuum_toroidal_field - Characteristics of the vacuum toroidal field (used in rho_tor definition)
  type (ids_ntm_time_slice),pointer :: time_slice(:) => null()  ! /ntms/time_slice(i) - Description of neoclassical tearing modes for various time slices
  type (ids_code) :: code  ! /ntms/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include pellets/dd_pellets.xsd
type ids_pellets_time_slice_pellet_shape  !    Initial shape of a pellet at launch
  type (ids_identifier) :: type  ! /type - Identifier structure for the shape type: 1-spherical; 2-cylindrical; 3-rectangular
  real(DP),pointer  :: size(:) => null()     ! /size - Size of the pellet in the various dimensions, depending on the shape type. Spherical pellets: size(1
  real(DP),pointer  :: size_error_upper(:) => null()
  real(DP),pointer  :: size_error_lower(:) => null()
  integer :: size_error_index=-999999999

endtype

type ids_pellets_time_slice_pellet_species  !    Species included in pellet compoisition
  real(DP)  :: a=-9.0D40       ! /a - Mass of atom
  real(DP)  :: a_error_upper=-9.0D40
  real(DP)  :: a_error_lower=-9.0D40
  integer :: a_error_index=-999999999

  real(DP)  :: z_n=-9.0D40       ! /z_n - Nuclear charge
  real(DP)  :: z_n_error_upper=-9.0D40
  real(DP)  :: z_n_error_lower=-9.0D40
  integer :: z_n_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H, D, T, ...)
  real(DP)  :: density=-9.0D40       ! /density - Material density of the species in the pellet
  real(DP)  :: density_error_upper=-9.0D40
  real(DP)  :: density_error_lower=-9.0D40
  integer :: density_error_index=-999999999

  real(DP)  :: fraction=-9.0D40       ! /fraction - Fraction of the species atoms in the pellet
  real(DP)  :: fraction_error_upper=-9.0D40
  real(DP)  :: fraction_error_lower=-9.0D40
  integer :: fraction_error_index=-999999999

  real(DP)  :: sublimation_energy=-9.0D40       ! /sublimation_energy - Sublimation energy per atom
  real(DP)  :: sublimation_energy_error_upper=-9.0D40
  real(DP)  :: sublimation_energy_error_lower=-9.0D40
  integer :: sublimation_energy_error_index=-999999999

endtype

type ids_pellets_time_slice_pellet_path_profiles  !    1-D profiles of plasma and pellet along the pellet path
  real(DP),pointer  :: distance(:) => null()     ! /distance - Distance along the pellet path, with the origin taken at path_geometry/first_point. Used as the main
  real(DP),pointer  :: distance_error_upper(:) => null()
  real(DP),pointer  :: distance_error_lower(:) => null()
  integer :: distance_error_index=-999999999

  real(DP),pointer  :: rho_tor_norm(:) => null()     ! /rho_tor_norm - Normalised toroidal coordinate along the pellet path
  real(DP),pointer  :: rho_tor_norm_error_upper(:) => null()
  real(DP),pointer  :: rho_tor_norm_error_lower(:) => null()
  integer :: rho_tor_norm_error_index=-999999999

  real(DP),pointer  :: psi(:) => null()     ! /psi - Poloidal flux along the pellet path
  real(DP),pointer  :: psi_error_upper(:) => null()
  real(DP),pointer  :: psi_error_lower(:) => null()
  integer :: psi_error_index=-999999999

  real(DP),pointer  :: velocity(:) => null()     ! /velocity - Pellet velocity along the pellet path
  real(DP),pointer  :: velocity_error_upper(:) => null()
  real(DP),pointer  :: velocity_error_lower(:) => null()
  integer :: velocity_error_index=-999999999

  real(DP),pointer  :: n_e(:) => null()     ! /n_e - Electron density along the pellet path
  real(DP),pointer  :: n_e_error_upper(:) => null()
  real(DP),pointer  :: n_e_error_lower(:) => null()
  integer :: n_e_error_index=-999999999

  real(DP),pointer  :: t_e(:) => null()     ! /t_e - Electron temperature along the pellet path
  real(DP),pointer  :: t_e_error_upper(:) => null()
  real(DP),pointer  :: t_e_error_lower(:) => null()
  integer :: t_e_error_index=-999999999

  real(DP),pointer  :: ablation_rate(:) => null()     ! /ablation_rate - Ablation rate (electrons) along the pellet path
  real(DP),pointer  :: ablation_rate_error_upper(:) => null()
  real(DP),pointer  :: ablation_rate_error_lower(:) => null()
  integer :: ablation_rate_error_index=-999999999

  real(DP),pointer  :: ablated_particles(:) => null()     ! /ablated_particles - Number of ablated particles (electrons) along the pellet path
  real(DP),pointer  :: ablated_particles_error_upper(:) => null()
  real(DP),pointer  :: ablated_particles_error_lower(:) => null()
  integer :: ablated_particles_error_index=-999999999

  real(DP),pointer  :: rho_tor_norm_drift(:) => null()     ! /rho_tor_norm_drift - Difference to due ExB drifts between the ablation and the final deposition locations, in terms of th
  real(DP),pointer  :: rho_tor_norm_drift_error_upper(:) => null()
  real(DP),pointer  :: rho_tor_norm_drift_error_lower(:) => null()
  integer :: rho_tor_norm_drift_error_index=-999999999

  type (ids_rzphi1d_dynamic_aos3) :: position  ! /position - Position along the pellet path
endtype

type ids_pellets_time_slice_pellet  !    Description of a pellet
  type (ids_pellets_time_slice_pellet_shape) :: shape  ! /shape - Initial shape of a pellet at launch
  type (ids_pellets_time_slice_pellet_species),pointer :: species(:) => null()  ! /species(i) - Set of species included in the pellet composition
  real(DP)  :: velocity_initial=-9.0D40       ! /velocity_initial - Initial velocity of the pellet as it enters the vaccum chamber
  real(DP)  :: velocity_initial_error_upper=-9.0D40
  real(DP)  :: velocity_initial_error_lower=-9.0D40
  integer :: velocity_initial_error_index=-999999999

  type (ids_line_of_sight_2points_dynamic_aos3) :: path_geometry  ! /path_geometry - Geometry of the pellet path in the vaccuum chamber
  type (ids_pellets_time_slice_pellet_path_profiles) :: path_profiles  ! /path_profiles - 1-D profiles of plasma and pellet along the pellet path
endtype

type ids_pellets_time_slice  !    Time slice description of pellets
  type (ids_pellets_time_slice_pellet),pointer :: pellet(:) => null()  ! /pellet(i) - Set of pellets ablated in the plasma at a given time
  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_pellets  !    Description of pellets launched into the plasma
  type (ids_ids_properties) :: ids_properties  ! /pellets/ids_properties -
  type (ids_pellets_time_slice),pointer :: time_slice(:) => null()  ! /pellets/time_slice(i) - Description of the pellets launched at various time slices. The time of this structure corresponds t
  type (ids_code) :: code  ! /pellets/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include pf_active/dd_pf_active.xsd
! SPECIAL STRUCTURE data / time
type ids_pf_supplies_voltage  !    Voltage at the supply output
  real(DP), pointer  :: data(:) => null()     ! /voltage - Voltage at the supply output
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_pf_supplies_current  !    Current at the supply output, defined positive if it flows from point 1 to point 2 of the component in the pfcircuit description
  real(DP), pointer  :: data(:) => null()     ! /current - Current at the supply output, defined positive if it flows from point 1 to point 2 of the component
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_pf_supplies  !    PF power supplies
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of the PF supply
  character(len=132), dimension(:), pointer ::identifier => null()       ! /identifier - Identifier of the supply
  integer  :: type=-999999999       ! /type - Type of the supply; TBD add free description of non-linear power supplies
  real(DP)  :: resistance=-9.0D40       ! /resistance - Power supply internal resistance
  real(DP)  :: resistance_error_upper=-9.0D40
  real(DP)  :: resistance_error_lower=-9.0D40
  integer :: resistance_error_index=-999999999

  real(DP)  :: delay=-9.0D40       ! /delay - Pure delay in the supply
  real(DP)  :: delay_error_upper=-9.0D40
  real(DP)  :: delay_error_lower=-9.0D40
  integer :: delay_error_index=-999999999

  real(DP),pointer  :: filter_numerator(:) => null()     ! /filter_numerator - Coefficients of the numerator, in increasing order : a0 + a1*s + ... + an*s^n; used for a linear sup
  real(DP),pointer  :: filter_numerator_error_upper(:) => null()
  real(DP),pointer  :: filter_numerator_error_lower(:) => null()
  integer :: filter_numerator_error_index=-999999999

  real(DP),pointer  :: filter_denominator(:) => null()     ! /filter_denominator - Coefficients of the denominator, in increasing order : b0 + b1*s + ... + bm*s^m; used for a linear s
  real(DP),pointer  :: filter_denominator_error_upper(:) => null()
  real(DP),pointer  :: filter_denominator_error_lower(:) => null()
  integer :: filter_denominator_error_index=-999999999

  real(DP)  :: current_limit_max=-9.0D40       ! /current_limit_max - Maximum current in the supply
  real(DP)  :: current_limit_max_error_upper=-9.0D40
  real(DP)  :: current_limit_max_error_lower=-9.0D40
  integer :: current_limit_max_error_index=-999999999

  real(DP)  :: current_limit_min=-9.0D40       ! /current_limit_min - Minimum current in the supply
  real(DP)  :: current_limit_min_error_upper=-9.0D40
  real(DP)  :: current_limit_min_error_lower=-9.0D40
  integer :: current_limit_min_error_index=-999999999

  real(DP)  :: voltage_limit_max=-9.0D40       ! /voltage_limit_max - Maximum voltage from the supply
  real(DP)  :: voltage_limit_max_error_upper=-9.0D40
  real(DP)  :: voltage_limit_max_error_lower=-9.0D40
  integer :: voltage_limit_max_error_index=-999999999

  real(DP)  :: voltage_limit_min=-9.0D40       ! /voltage_limit_min - Minimum voltage from the supply
  real(DP)  :: voltage_limit_min_error_upper=-9.0D40
  real(DP)  :: voltage_limit_min_error_lower=-9.0D40
  integer :: voltage_limit_min_error_index=-999999999

  real(DP)  :: current_limiter_gain=-9.0D40       ! /current_limiter_gain - Gain to prevent overcurrent in a linear model of the supply
  real(DP)  :: current_limiter_gain_error_upper=-9.0D40
  real(DP)  :: current_limiter_gain_error_lower=-9.0D40
  integer :: current_limiter_gain_error_index=-999999999

  real(DP)  :: energy_limit_max=-9.0D40       ! /energy_limit_max - Maximum energy to be dissipated in the supply during a pulse
  real(DP)  :: energy_limit_max_error_upper=-9.0D40
  real(DP)  :: energy_limit_max_error_lower=-9.0D40
  integer :: energy_limit_max_error_index=-999999999

  character(len=132), dimension(:), pointer ::nonlinear_model => null()       ! /nonlinear_model - Description of the nonlinear transfer function of the supply
  type (ids_pf_supplies_voltage) :: voltage  ! /voltage - Voltage at the supply output
  type (ids_pf_supplies_current) :: current  ! /current - Current at the supply output, defined positive if it flows from point 1 to point 2 of the component
endtype

! SPECIAL STRUCTURE data / time
type ids_pf_circuits_voltage  !    Voltage on the circuit
  real(DP), pointer  :: data(:) => null()     ! /voltage - Voltage on the circuit
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_pf_circuits_current  !    Current in the circuit
  real(DP), pointer  :: data(:) => null()     ! /current - Current in the circuit
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_pf_circuits  !    Circuits, connecting multiple PF coils to multiple supplies, defining the current and voltage relationships in the system
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of the circuit
  character(len=132), dimension(:), pointer ::identifier => null()       ! /identifier - ID of the circuit
  character(len=132), dimension(:), pointer ::type => null()       ! /type - Type of the circuit
  integer,pointer  :: connections(:,:) => null()     ! /connections - Description of the supplies and coils connections (nodes) across the circuit. The matrix describing
  type (ids_pf_circuits_voltage) :: voltage  ! /voltage - Voltage on the circuit
  type (ids_pf_circuits_current) :: current  ! /current - Current in the circuit
endtype

type ids_pf_coils_elements  !    Each PF coil is comprised of a number of cross-section elements described  individually
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of this element of this coil
  character(len=132), dimension(:), pointer ::identifier => null()       ! /identifier - Identifier of this element of this coil
  integer  :: turns_with_sign=-999999999       ! /turns_with_sign - Number of effective turns in the element for calculating magnetic fields of the coil; includes the s
  real(DP)  :: area=-9.0D40       ! /area - Cross-sectional areas of the element
  real(DP)  :: area_error_upper=-9.0D40
  real(DP)  :: area_error_lower=-9.0D40
  integer :: area_error_index=-999999999

  type (ids_outline_2d_geometry_static) :: geometry  ! /geometry - Cross-sectional shape of the element
endtype

! SPECIAL STRUCTURE data / time
type ids_pf_coils_current  !    Current in the coil
  real(DP), pointer  :: data(:) => null()     ! /current - Current in the coil
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_pf_coils_voltage  !    Voltage on the coil terminals
  real(DP), pointer  :: data(:) => null()     ! /voltage - Voltage on the coil terminals
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_pf_coils  !    Active PF coils
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of the coil
  character(len=132), dimension(:), pointer ::identifier => null()       ! /identifier - Alphanumeric identifier of coils used for convenience
  real(DP)  :: resistance=-9.0D40       ! /resistance - Coil resistance
  real(DP)  :: resistance_error_upper=-9.0D40
  real(DP)  :: resistance_error_lower=-9.0D40
  integer :: resistance_error_index=-999999999

  real(DP)  :: energy_limit_max=-9.0D40       ! /energy_limit_max - Maximum Energy to be dissipated in the coil
  real(DP)  :: energy_limit_max_error_upper=-9.0D40
  real(DP)  :: energy_limit_max_error_lower=-9.0D40
  integer :: energy_limit_max_error_index=-999999999

  type (ids_pf_coils_elements),pointer :: element(:) => null()  ! /element(i) - Each PF coil is comprised of a number of cross-section elements described  individually
  type (ids_pf_coils_current) :: current  ! /current - Current in the coil
  type (ids_pf_coils_voltage) :: voltage  ! /voltage - Voltage on the coil terminals
endtype

! SPECIAL STRUCTURE data / time
type ids_pf_vertical_forces_force  !    Force
  real(DP), pointer  :: data(:) => null()     ! /force - Force
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_pf_vertical_forces  !    Vertical forces on the axisymmetric PF+CS coil system
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of the force combination
  real(DP),pointer  :: combination(:) => null()     ! /combination - Coils involved in the force combinations. Normally the vertical force would be the full set of coils
  real(DP),pointer  :: combination_error_upper(:) => null()
  real(DP),pointer  :: combination_error_lower(:) => null()
  integer :: combination_error_index=-999999999

  real(DP)  :: limit_max=-9.0D40       ! /limit_max - Vertical force combination limit
  real(DP)  :: limit_max_error_upper=-9.0D40
  real(DP)  :: limit_max_error_lower=-9.0D40
  integer :: limit_max_error_index=-999999999

  real(DP)  :: limit_min=-9.0D40       ! /limit_min - Vertical force combination limit
  real(DP)  :: limit_min_error_upper=-9.0D40
  real(DP)  :: limit_min_error_lower=-9.0D40
  integer :: limit_min_error_index=-999999999

  type (ids_pf_vertical_forces_force) :: force  ! /force - Force
endtype

type ids_pf_active  !    Description of the axisymmetric active poloidal field (PF) coils and supplies; includes the limits of these systems; includes the
  type (ids_ids_properties) :: ids_properties  ! /pf_active/ids_properties -
  type (ids_pf_coils),pointer :: coil(:) => null()  ! /pf_active/coil(i) - Active PF coils
  type (ids_pf_vertical_forces),pointer :: vertical_force(:) => null()  ! /pf_active/vertical_force(i) - Vertical forces on the axisymmetric PF coil system
  type (ids_pf_circuits),pointer :: circuit(:) => null()  ! /pf_active/circuit(i) - Circuits, connecting multiple PF coils to multiple supplies, defining the current and voltage relati
  type (ids_pf_supplies),pointer :: supply(:) => null()  ! /pf_active/supply(i) - PF power supplies
  type (ids_code) :: code  ! /pf_active/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include pf_passive/dd_pf_passive.xsd
type ids_pf_passive_loops  !    Passive axisymmetric conductor description in the form of non-connected loops; any connected loops are expressed as active coil ci
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of the loop
  real(DP)  :: area=-9.0D40       ! /area - Surface area of the passive loop
  real(DP)  :: area_error_upper=-9.0D40
  real(DP)  :: area_error_lower=-9.0D40
  integer :: area_error_index=-999999999

  real(DP)  :: resistance=-9.0D40       ! /resistance - Passive loop resistance
  real(DP)  :: resistance_error_upper=-9.0D40
  real(DP)  :: resistance_error_lower=-9.0D40
  integer :: resistance_error_index=-999999999

  type (ids_outline_2d_geometry_static) :: geometry  ! /geometry - Shape of the passive loop
  real(DP),pointer  :: current(:) => null()     ! /current - Passive loop current
  real(DP),pointer  :: current_error_upper(:) => null()
  real(DP),pointer  :: current_error_lower(:) => null()
  integer :: current_error_index=-999999999

endtype

type ids_pf_passive  !    Description of the axisymmetric passive conductors, currents flowing in them
  type (ids_ids_properties) :: ids_properties  ! /pf_passive/ids_properties -
  type (ids_pf_passive_loops),pointer :: loop(:) => null()  ! /pf_passive/loop(i) - Passive axisymmetric conductor description in the form of non-connected loops; any connected loops a
  type (ids_code) :: code  ! /pf_passive/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include sawteeth/dd_sawteeth.xsd
type ids_sawteeth_profiles_1d  !    Core profiles after sawtooth crash
  type (ids_core_radial_grid) :: grid  ! /grid - Radial grid
  real(DP),pointer  :: t_e(:) => null()     ! /t_e - Electron temperature
  real(DP),pointer  :: t_e_error_upper(:) => null()
  real(DP),pointer  :: t_e_error_lower(:) => null()
  integer :: t_e_error_index=-999999999

  real(DP),pointer  :: t_i_average(:) => null()     ! /t_i_average - Ion temperature (averaged on charge states and ion species)
  real(DP),pointer  :: t_i_average_error_upper(:) => null()
  real(DP),pointer  :: t_i_average_error_lower(:) => null()
  integer :: t_i_average_error_index=-999999999

  real(DP),pointer  :: n_e(:) => null()     ! /n_e - Electron density (thermal+non-thermal)
  real(DP),pointer  :: n_e_error_upper(:) => null()
  real(DP),pointer  :: n_e_error_lower(:) => null()
  integer :: n_e_error_index=-999999999

  real(DP),pointer  :: n_e_fast(:) => null()     ! /n_e_fast - Density of fast (non-thermal) electrons
  real(DP),pointer  :: n_e_fast_error_upper(:) => null()
  real(DP),pointer  :: n_e_fast_error_lower(:) => null()
  integer :: n_e_fast_error_index=-999999999

  real(DP),pointer  :: n_i_total_over_n_e(:) => null()     ! /n_i_total_over_n_e - Ratio of total ion density (sum over species and charge states) over electron density. (thermal+non-
  real(DP),pointer  :: n_i_total_over_n_e_error_upper(:) => null()
  real(DP),pointer  :: n_i_total_over_n_e_error_lower(:) => null()
  integer :: n_i_total_over_n_e_error_index=-999999999

  real(DP),pointer  :: momentum_tor(:) => null()     ! /momentum_tor - Total plasma toroidal momentum, summed over ion species and electrons
  real(DP),pointer  :: momentum_tor_error_upper(:) => null()
  real(DP),pointer  :: momentum_tor_error_lower(:) => null()
  integer :: momentum_tor_error_index=-999999999

  real(DP),pointer  :: zeff(:) => null()     ! /zeff - Effective charge
  real(DP),pointer  :: zeff_error_upper(:) => null()
  real(DP),pointer  :: zeff_error_lower(:) => null()
  integer :: zeff_error_index=-999999999

  real(DP),pointer  :: p_e(:) => null()     ! /p_e - Electron pressure
  real(DP),pointer  :: p_e_error_upper(:) => null()
  real(DP),pointer  :: p_e_error_lower(:) => null()
  integer :: p_e_error_index=-999999999

  real(DP),pointer  :: p_e_fast_perpendicular(:) => null()     ! /p_e_fast_perpendicular - Fast (non-thermal) electron perpendicular pressure
  real(DP),pointer  :: p_e_fast_perpendicular_error_upper(:) => null()
  real(DP),pointer  :: p_e_fast_perpendicular_error_lower(:) => null()
  integer :: p_e_fast_perpendicular_error_index=-999999999

  real(DP),pointer  :: p_e_fast_parallel(:) => null()     ! /p_e_fast_parallel - Fast (non-thermal) electron parallel pressure
  real(DP),pointer  :: p_e_fast_parallel_error_upper(:) => null()
  real(DP),pointer  :: p_e_fast_parallel_error_lower(:) => null()
  integer :: p_e_fast_parallel_error_index=-999999999

  real(DP),pointer  :: p_i_total(:) => null()     ! /p_i_total - Total ion pressure (sum over the ion species)
  real(DP),pointer  :: p_i_total_error_upper(:) => null()
  real(DP),pointer  :: p_i_total_error_lower(:) => null()
  integer :: p_i_total_error_index=-999999999

  real(DP),pointer  :: p_i_total_fast_perpendicular(:) => null()     ! /p_i_total_fast_perpendicular - Fast (non-thermal) total ion (sum over the ion species) perpendicular pressure
  real(DP),pointer  :: p_i_total_fast_perpendicular_error_upper(:) => null()
  real(DP),pointer  :: p_i_total_fast_perpendicular_error_lower(:) => null()
  integer :: p_i_total_fast_perpendicular_error_index=-999999999

  real(DP),pointer  :: p_i_total_fast_parallel(:) => null()     ! /p_i_total_fast_parallel - Fast (non-thermal) total ion (sum over the ion species) parallel pressure
  real(DP),pointer  :: p_i_total_fast_parallel_error_upper(:) => null()
  real(DP),pointer  :: p_i_total_fast_parallel_error_lower(:) => null()
  integer :: p_i_total_fast_parallel_error_index=-999999999

  real(DP),pointer  :: pressure_thermal(:) => null()     ! /pressure_thermal - Thermal pressure (electrons+ions)
  real(DP),pointer  :: pressure_thermal_error_upper(:) => null()
  real(DP),pointer  :: pressure_thermal_error_lower(:) => null()
  integer :: pressure_thermal_error_index=-999999999

  real(DP),pointer  :: pressure_perpendicular(:) => null()     ! /pressure_perpendicular - Total perpendicular pressure (electrons+ions, thermal+non-thermal)
  real(DP),pointer  :: pressure_perpendicular_error_upper(:) => null()
  real(DP),pointer  :: pressure_perpendicular_error_lower(:) => null()
  integer :: pressure_perpendicular_error_index=-999999999

  real(DP),pointer  :: pressure_parallel(:) => null()     ! /pressure_parallel - Total parallel pressure (electrons+ions, thermal+non-thermal)
  real(DP),pointer  :: pressure_parallel_error_upper(:) => null()
  real(DP),pointer  :: pressure_parallel_error_lower(:) => null()
  integer :: pressure_parallel_error_index=-999999999

  real(DP),pointer  :: j_total(:) => null()     ! /j_total - Total parallel current density = average(jtot.B) / B0, where B0 = Core_Profiles/Vacuum_Toroidal_Fiel
  real(DP),pointer  :: j_total_error_upper(:) => null()
  real(DP),pointer  :: j_total_error_lower(:) => null()
  integer :: j_total_error_index=-999999999

  real(DP),pointer  :: j_tor(:) => null()     ! /j_tor - Total toroidal current density = average(J_Tor/R) / average(1/R)
  real(DP),pointer  :: j_tor_error_upper(:) => null()
  real(DP),pointer  :: j_tor_error_lower(:) => null()
  integer :: j_tor_error_index=-999999999

  real(DP),pointer  :: j_ohmic(:) => null()     ! /j_ohmic - Ohmic parallel current density = average(J_Ohmic.B) / B0, where B0 = Core_Profiles/Vacuum_Toroidal_F
  real(DP),pointer  :: j_ohmic_error_upper(:) => null()
  real(DP),pointer  :: j_ohmic_error_lower(:) => null()
  integer :: j_ohmic_error_index=-999999999

  real(DP),pointer  :: j_non_inductive(:) => null()     ! /j_non_inductive - Non-inductive (includes bootstrap) parallel current density = average(jni.B) / B0, where B0 = Core_P
  real(DP),pointer  :: j_non_inductive_error_upper(:) => null()
  real(DP),pointer  :: j_non_inductive_error_lower(:) => null()
  integer :: j_non_inductive_error_index=-999999999

  real(DP),pointer  :: j_bootstrap(:) => null()     ! /j_bootstrap - Bootstrap current density = average(J_Bootstrap.B) / B0, where B0 = Core_Profiles/Vacuum_Toroidal_Fi
  real(DP),pointer  :: j_bootstrap_error_upper(:) => null()
  real(DP),pointer  :: j_bootstrap_error_lower(:) => null()
  integer :: j_bootstrap_error_index=-999999999

  real(DP),pointer  :: conductivity_parallel(:) => null()     ! /conductivity_parallel - Parallel conductivity
  real(DP),pointer  :: conductivity_parallel_error_upper(:) => null()
  real(DP),pointer  :: conductivity_parallel_error_lower(:) => null()
  integer :: conductivity_parallel_error_index=-999999999

  real(DP),pointer  :: e_field_parallel(:) => null()     ! /e_field_parallel - Parallel electric field = average(E.B) / B0, where Core_Profiles/Vacuum_Toroidal_Field/ B0
  real(DP),pointer  :: e_field_parallel_error_upper(:) => null()
  real(DP),pointer  :: e_field_parallel_error_lower(:) => null()
  integer :: e_field_parallel_error_index=-999999999

  real(DP),pointer  :: q(:) => null()     ! /q - Safety factor
  real(DP),pointer  :: q_error_upper(:) => null()
  real(DP),pointer  :: q_error_lower(:) => null()
  integer :: q_error_index=-999999999

  real(DP),pointer  :: magnetic_shear(:) => null()     ! /magnetic_shear - Magnetic shear, defined as rho_tor/q . dq/drho_tor
  real(DP),pointer  :: magnetic_shear_error_upper(:) => null()
  real(DP),pointer  :: magnetic_shear_error_lower(:) => null()
  integer :: magnetic_shear_error_index=-999999999

  real(DP),pointer  :: phi(:) => null()     ! /phi - Toroidal flux
  real(DP),pointer  :: phi_error_upper(:) => null()
  real(DP),pointer  :: phi_error_lower(:) => null()
  integer :: phi_error_index=-999999999

  real(DP),pointer  :: psi_star_pre_crash(:) => null()     ! /psi_star_pre_crash - Psi* = psi - phi, just before the sawtooth crash
  real(DP),pointer  :: psi_star_pre_crash_error_upper(:) => null()
  real(DP),pointer  :: psi_star_pre_crash_error_lower(:) => null()
  integer :: psi_star_pre_crash_error_index=-999999999

  real(DP),pointer  :: psi_star_post_crash(:) => null()     ! /psi_star_post_crash - Psi* = psi - phi, after the sawtooth crash
  real(DP),pointer  :: psi_star_post_crash_error_upper(:) => null()
  real(DP),pointer  :: psi_star_post_crash_error_lower(:) => null()
  integer :: psi_star_post_crash_error_index=-999999999

  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_sawteeth_diagnostics  !    Detailed information about the sawtooth characteristics
  real(DP),pointer  :: magnetic_shear_q1(:) => null()     ! /magnetic_shear_q1 - Magnetic shear at surface q = 1, defined as rho_tor/q . dq/drho_tor
  real(DP),pointer  :: magnetic_shear_q1_error_upper(:) => null()
  real(DP),pointer  :: magnetic_shear_q1_error_lower(:) => null()
  integer :: magnetic_shear_q1_error_index=-999999999

  real(DP),pointer  :: rho_tor_norm_q1(:) => null()     ! /rho_tor_norm_q1 - Normalised toroidal flux coordinate at surface q = 1
  real(DP),pointer  :: rho_tor_norm_q1_error_upper(:) => null()
  real(DP),pointer  :: rho_tor_norm_q1_error_lower(:) => null()
  integer :: rho_tor_norm_q1_error_index=-999999999

  real(DP),pointer  :: rho_tor_norm_inversion(:) => null()     ! /rho_tor_norm_inversion - Normalised toroidal flux coordinate at inversion radius
  real(DP),pointer  :: rho_tor_norm_inversion_error_upper(:) => null()
  real(DP),pointer  :: rho_tor_norm_inversion_error_lower(:) => null()
  integer :: rho_tor_norm_inversion_error_index=-999999999

  real(DP),pointer  :: rho_tor_norm_mixing(:) => null()     ! /rho_tor_norm_mixing - Normalised toroidal flux coordinate at mixing radius
  real(DP),pointer  :: rho_tor_norm_mixing_error_upper(:) => null()
  real(DP),pointer  :: rho_tor_norm_mixing_error_lower(:) => null()
  integer :: rho_tor_norm_mixing_error_index=-999999999

endtype

type ids_sawteeth  !    Description of sawtooth events. This IDS must be used in homogeneous_time = 1 mode
  type (ids_ids_properties) :: ids_properties  ! /sawteeth/ids_properties -
  type (ids_b_tor_vacuum_1) :: vacuum_toroidal_field  ! /sawteeth/vacuum_toroidal_field - Characteristics of the vacuum toroidal field (used in rho_tor definition)
  type (ids_sawteeth_profiles_1d),pointer :: profiles_1d(:) => null()  ! /sawteeth/profiles_1d(i) - Core profiles after sawtooth crash for various time slices
  type (ids_sawteeth_diagnostics) :: diagnostics  ! /sawteeth/diagnostics - Detailed information about the sawtooth characteristics
  type (ids_code) :: code  ! /sawteeth/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include schedule/dd_schedule.xsd
type ids_schedule  !    Description of Pulse Schedule, to be enhanced as we go; we have chosen initially to have the controllers, schedule and SN defined
  type (ids_ids_properties) :: ids_properties  ! /schedule/ids_properties -
  type (ids_schedule_waveform),pointer :: waveform(:) => null()  ! /schedule/waveform(i) - Reference waveform to be used to drive feedback or feedforward controllers, or to control the contro
  type (ids_code) :: code  ! /schedule/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include sdn/dd_sdn.xsd
type ids_sdn_topic_list  !    List of the groups of signals used at different reading and writing points
  character(len=132), dimension(:), pointer ::names => null()       ! /names - Names of the group of SDN signals
  integer,pointer  :: indices(:) => null()      ! /indices - Indices into the current SDN allocated list; it must be updated when the allocated list is changed
endtype

type ids_sdn_allocatable_signals  !    Dictionary of the signal names and definitions which can be allocated to the SDN
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of the allocatable signal
  character(len=132), dimension(:), pointer ::definition => null()       ! /definition - Definition of the allocatable signal
  integer  :: ip_normalise=-999999999       ! /ip_normalise - 0 or 1 if this signal is multiplied by Ip to generate a control variable; this usage is specific to
  integer  :: allocated_position=-999999999       ! /allocated_position - Allocation of signal to a position in the SDN (1..N); this will be implementation specific
  real(DP),pointer  :: value(:) => null()     ! /value - Signal value
  real(DP),pointer  :: value_error_upper(:) => null()
  real(DP),pointer  :: value_error_lower(:) => null()
  integer :: value_error_index=-999999999

endtype

type ids_sdn  !    Description of the Synchronous Data Network parameters and the signals on it
  type (ids_ids_properties) :: ids_properties  ! /sdn/ids_properties -
  type (ids_sdn_allocatable_signals),pointer :: signal(:) => null()  ! /sdn/signal(i) - Dictionary of the signal names and definitions which can be allocated to the SDN
  type (ids_sdn_topic_list),pointer :: topic_list(:) => null()  ! /sdn/topic_list(i) - List of the groups of signals used at different reading and writing points; this is an implementatio
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include spectrometer_visible/dd_spectrometer_visible.xsd
! SPECIAL STRUCTURE data / time
type ids_spectro_vis_channel_filter_line_radiances  !    Calibrated, background subtracted line integrals
  real(DP), pointer  :: data(:,:) => null()     ! /line_radiances - Calibrated, background subtracted line integrals
  real(DP), pointer  :: data_error_upper(:,:) => null()
  real(DP), pointer  :: data_error_lower(:,:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_spectro_vis_channel_filter_line_intensities  !    Line gross integral intensities
  real(DP), pointer  :: data(:,:) => null()     ! /line_intensities - Line gross integral intensities
  real(DP), pointer  :: data_error_upper(:,:) => null()
  real(DP), pointer  :: data_error_lower(:,:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_spectro_vis_channel_filter_calibrated_line_integrals  !    Calibrated line gross areas integrals
  real(DP), pointer  :: data(:,:) => null()     ! /calibrated_line_integrals - Calibrated line gross areas integrals
  real(DP), pointer  :: data_error_upper(:,:) => null()
  real(DP), pointer  :: data_error_lower(:,:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_spectro_vis_channel_filter  !    Filter spectrometer
  real(DP),pointer  :: processed_lines(:) => null()     ! /processed_lines - Central wavelength of the processed lines
  real(DP),pointer  :: processed_lines_error_upper(:) => null()
  real(DP),pointer  :: processed_lines_error_lower(:) => null()
  integer :: processed_lines_error_index=-999999999

  type (ids_spectro_vis_channel_filter_line_radiances) :: line_radiances  ! /line_radiances - Calibrated, background subtracted line integrals
  real(DP),pointer  :: raw_lines(:) => null()     ! /raw_lines - Central wavelength of the raw lines
  real(DP),pointer  :: raw_lines_error_upper(:) => null()
  real(DP),pointer  :: raw_lines_error_lower(:) => null()
  integer :: raw_lines_error_index=-999999999

  type (ids_spectro_vis_channel_filter_line_intensities) :: line_intensities  ! /line_intensities - Line gross integral intensities
  real(DP),pointer  :: calibrated_lines(:) => null()     ! /calibrated_lines - Central wavelength of the calibrated lines
  real(DP),pointer  :: calibrated_lines_error_upper(:) => null()
  real(DP),pointer  :: calibrated_lines_error_lower(:) => null()
  integer :: calibrated_lines_error_index=-999999999

  type (ids_spectro_vis_channel_filter_calibrated_line_integrals) :: calibrated_line_integrals  ! /calibrated_line_integrals - Calibrated line gross areas integrals
  real(DP)  :: exposure_time=-9.0D40       ! /exposure_time - Exposure time
  real(DP)  :: exposure_time_error_upper=-9.0D40
  real(DP)  :: exposure_time_error_lower=-9.0D40
  integer :: exposure_time_error_index=-999999999

  real(DP)  :: radiance_calibration=-9.0D40       ! /radiance_calibration - Radiance calibration
  real(DP)  :: radiance_calibration_error_upper=-9.0D40
  real(DP)  :: radiance_calibration_error_lower=-9.0D40
  integer :: radiance_calibration_error_index=-999999999

  character(len=132), dimension(:), pointer ::radiance_calibration_date => null()       ! /radiance_calibration_date - Date of the radiance calibration (yyyy_mm_dd)
endtype

! SPECIAL STRUCTURE data / time
type ids_spectro_vis_channel_grating_line_radiances  !    Calibrated, background subtracted line integrals
  real(DP), pointer  :: data(:,:) => null()     ! /line_radiances - Calibrated, background subtracted line integrals
  real(DP), pointer  :: data_error_upper(:,:) => null()
  real(DP), pointer  :: data_error_lower(:,:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_spectro_vis_channel_grating_radiance_spectra  !    Calibrated spectra
  real(DP), pointer  :: data(:,:) => null()     ! /radiance_spectra - Calibrated spectra
  real(DP), pointer  :: data_error_upper(:,:) => null()
  real(DP), pointer  :: data_error_lower(:,:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_spectro_vis_channel_grating_intensity_spectra  !    Intensity spectra (not calibrated)
  real(DP), pointer  :: data(:,:) => null()     ! /intensity_spectra - Intensity spectra (not calibrated)
  real(DP), pointer  :: data_error_upper(:,:) => null()
  real(DP), pointer  :: data_error_lower(:,:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_spectro_vis_channel_grating  !    Grating spectrometer
  real(DP),pointer  :: processed_lines(:) => null()     ! /processed_lines - Central wavelength of the processed lines
  real(DP),pointer  :: processed_lines_error_upper(:) => null()
  real(DP),pointer  :: processed_lines_error_lower(:) => null()
  integer :: processed_lines_error_index=-999999999

  type (ids_spectro_vis_channel_grating_line_radiances) :: line_radiances  ! /line_radiances - Calibrated, background subtracted line integrals
  real(DP),pointer  :: wavelengths(:) => null()     ! /wavelengths - Measured wavelengths
  real(DP),pointer  :: wavelengths_error_upper(:) => null()
  real(DP),pointer  :: wavelengths_error_lower(:) => null()
  integer :: wavelengths_error_index=-999999999

  type (ids_spectro_vis_channel_grating_radiance_spectra) :: radiance_spectra  ! /radiance_spectra - Calibrated spectra
  type (ids_spectro_vis_channel_grating_intensity_spectra) :: intensity_spectra  ! /intensity_spectra - Intensity spectra (not calibrated)
  real(DP)  :: exposure_time=-9.0D40       ! /exposure_time - Exposure time
  real(DP)  :: exposure_time_error_upper=-9.0D40
  real(DP)  :: exposure_time_error_lower=-9.0D40
  integer :: exposure_time_error_index=-999999999

  real(DP),pointer  :: radiance_calibration(:) => null()     ! /radiance_calibration - Radiance calibration
  real(DP),pointer  :: radiance_calibration_error_upper(:) => null()
  real(DP),pointer  :: radiance_calibration_error_lower(:) => null()
  integer :: radiance_calibration_error_index=-999999999

  character(len=132), dimension(:), pointer ::radiance_calibration_date => null()       ! /radiance_calibration_date - Date of the radiance calibration (yyyy_mm_dd)
  character(len=132), dimension(:), pointer ::wavelength_calibration_date => null()       ! /wavelength_calibration_date - Date of the wavelength calibration (yyyy_mm_dd)
endtype

! SPECIAL STRUCTURE data / time
type ids_spectro_vis_channel_validity_timed  !    Indicator of the validity of the channel as a function of time (0 means valid, negative values mean non-valid)
  integer, pointer  :: data(:) => null()      ! /validity_timed - Indicator of the validity of the channel as a function of time (0 means valid, negative values mean
  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_spectro_vis_channel  !    Charge exchange channel
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of the channel
  type (ids_identifier_static) :: type  ! /type - Type of spectrometer the channel is connected to (index=1: grating, 2: filter)
  type (ids_line_of_sight_2points) :: line_of_sight  ! /line_of_sight - Description of the line of sight of the channel, given by 2 points
  real(DP)  :: spot_diameter=-9.0D40       ! /spot_diameter - Spot diameter
  real(DP)  :: spot_diameter_error_upper=-9.0D40
  real(DP)  :: spot_diameter_error_lower=-9.0D40
  integer :: spot_diameter_error_index=-999999999

  type (ids_spectro_vis_channel_grating) :: grating  ! /grating - Quantities measured by the channel if connected to a grating spectrometer
  type (ids_spectro_vis_channel_filter) :: filter  ! /filter - Quantities measured by the channel if connected to a filter spectrometer
  type (ids_spectro_vis_channel_validity_timed) :: validity_timed  ! /validity_timed - Indicator of the validity of the channel as a function of time (0 means valid, negative values mean
  integer  :: validity=-999999999       ! /validity - Indicator of the validity of the channel for the whole acquisition period (0 means valid, negative v
endtype

type ids_spectrometer_visible  !    Spectrometer in visible light range diagnostic
  type (ids_ids_properties) :: ids_properties  ! /spectrometer_visible/ids_properties -
  type (ids_spectro_vis_channel),pointer :: channel(:) => null()  ! /spectrometer_visible/channel(i) - Set of channels (lines-of-sight)
  type (ids_code) :: code  ! /spectrometer_visible/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include temporary/dd_temporary.xsd
type ids_temporary_constant_quantities_float_0d  !    Temporary constant Float_0D
  real(DP)  :: value=-9.0D40       ! /value - Value
  real(DP)  :: value_error_upper=-9.0D40
  real(DP)  :: value_error_lower=-9.0D40
  integer :: value_error_index=-999999999

  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

type ids_temporary_constant_quantities_int_0d  !    Temporary constant INT_0D
  integer  :: value=-999999999       ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

type ids_temporary_constant_quantities_string_0d  !    Temporary constant STR_0D
  character(len=132), dimension(:), pointer ::value => null()       ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

type ids_temporary_constant_quantities_float_1d  !    Temporary constant Float_1D
  real(DP),pointer  :: value(:) => null()     ! /value - Value
  real(DP),pointer  :: value_error_upper(:) => null()
  real(DP),pointer  :: value_error_lower(:) => null()
  integer :: value_error_index=-999999999

  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

type ids_temporary_constant_quantities_int_1d  !    Temporary constant INT_1D
  integer,pointer  :: value(:) => null()      ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

type ids_temporary_constant_quantities_string_1d  !    Temporary constant STR_1D
  character(len=132), dimension(:), pointer ::value => null()       ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

! SPECIAL STRUCTURE data / time
type ids_temporary_dynamic_quantities_float_1d_value  !    Value
  real(DP), pointer  :: data(:) => null()     ! /value - Value
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_temporary_dynamic_quantities_float_1d  !    Temporary dynamic Float_1D
  type (ids_temporary_dynamic_quantities_float_1d_value) :: value  ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

! SPECIAL STRUCTURE data / time
type ids_temporary_dynamic_quantities_int_1d_value  !    Value
  integer, pointer  :: data(:) => null()      ! /value - Value
  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_temporary_dynamic_quantities_int_1d  !    Temporary dynamic Int_1D
  type (ids_temporary_dynamic_quantities_int_1d_value) :: value  ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

type ids_temporary_constant_quantities_float_2d  !    Temporary constant Float_2D
  real(DP),pointer  :: value(:,:) => null()     ! /value - Value
  real(DP),pointer  :: value_error_upper(:,:) => null()
  real(DP),pointer  :: value_error_lower(:,:) => null()
  integer :: value_error_index=-999999999

  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

type ids_temporary_constant_quantities_int_2d  !    Temporary constant INT_2D
  integer,pointer  :: value(:,:) => null()     ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

! SPECIAL STRUCTURE data / time
type ids_temporary_dynamic_quantities_float_2d_value  !    Value
  real(DP), pointer  :: data(:,:) => null()     ! /value - Value
  real(DP), pointer  :: data_error_upper(:,:) => null()
  real(DP), pointer  :: data_error_lower(:,:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_temporary_dynamic_quantities_float_2d  !    Temporary dynamic Float_2D
  type (ids_temporary_dynamic_quantities_float_2d_value) :: value  ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

! SPECIAL STRUCTURE data / time
type ids_temporary_dynamic_quantities_int_2d_value  !    Value
  integer, pointer  :: data(:,:) => null()     ! /value - Value
  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_temporary_dynamic_quantities_int_2d  !    Temporary dynamic INT_2D
  type (ids_temporary_dynamic_quantities_int_2d_value) :: value  ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

type ids_temporary_constant_quantities_float_3d  !    Temporary constant Float_3D
  real(DP),pointer  :: value(:,:,:) => null()     ! /value - Value
  real(DP),pointer  :: value_error_upper(:,:,:) => null()
  real(DP),pointer  :: value_error_lower(:,:,:) => null()
  integer :: value_error_index=-999999999

  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

type ids_temporary_constant_quantities_int_3d  !    Temporary constant INT_3D
  integer,pointer  :: value(:,:,:) => null()     ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

! SPECIAL STRUCTURE data / time
type ids_temporary_dynamic_quantities_float_3d_value  !    Value
  real(DP), pointer  :: data(:,:,:) => null()     ! /value - Value
  real(DP), pointer  :: data_error_upper(:,:,:) => null()
  real(DP), pointer  :: data_error_lower(:,:,:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_temporary_dynamic_quantities_float_3d  !    Temporary dynamic Float_3D
  type (ids_temporary_dynamic_quantities_float_3d_value) :: value  ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

! SPECIAL STRUCTURE data / time
type ids_temporary_dynamic_quantities_int_3d_value  !    Value
  integer, pointer  :: data(:,:,:) => null()     ! /value - Value
  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_temporary_dynamic_quantities_int_3d  !    Temporary dynamic INT_3D
  type (ids_temporary_dynamic_quantities_int_3d_value) :: value  ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

! SPECIAL STRUCTURE data / time
type ids_temporary_dynamic_quantities_float_4d_value  !    Value
  real(DP), pointer  :: data(:,:,:,:) => null()     ! /value - Value
  real(DP), pointer  :: data_error_upper(:,:,:,:) => null()
  real(DP), pointer  :: data_error_lower(:,:,:,:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_temporary_dynamic_quantities_float_4d  !    Temporary dynamic Float_4D
  type (ids_temporary_dynamic_quantities_float_4d_value) :: value  ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

type ids_temporary_constant_quantities_float_4d  !    Temporary constant Float_4D
  real(DP),pointer  :: value(:,:,:,:) => null()     ! /value - Value
  real(DP),pointer  :: value_error_upper(:,:,:,:) => null()
  real(DP),pointer  :: value_error_lower(:,:,:,:) => null()
  integer :: value_error_index=-999999999

  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

! SPECIAL STRUCTURE data / time
type ids_temporary_dynamic_quantities_float_5d_value  !    Value
  real(DP), pointer  :: data(:,:,:,:,:) => null()     ! /value - Value
  real(DP), pointer  :: data_error_upper(:,:,:,:,:) => null()
  real(DP), pointer  :: data_error_lower(:,:,:,:,:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_temporary_dynamic_quantities_float_5d  !    Temporary dynamic Float_5D
  type (ids_temporary_dynamic_quantities_float_5d_value) :: value  ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

type ids_temporary_constant_quantities_float_5d  !    Temporary constant Float_5D
  real(DP),pointer  :: value(:,:,:,:,:) => null()     ! /value - Value
  real(DP),pointer  :: value_error_upper(:,:,:,:,:) => null()
  real(DP),pointer  :: value_error_lower(:,:,:,:,:) => null()
  integer :: value_error_index=-999999999

  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

! SPECIAL STRUCTURE data / time
type ids_temporary_dynamic_quantities_float_6d_value  !    Value
  real(DP), pointer  :: data(:,:,:,:,:,:) => null()     ! /value - Value
  real(DP), pointer  :: data_error_upper(:,:,:,:,:,:) => null()
  real(DP), pointer  :: data_error_lower(:,:,:,:,:,:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_temporary_dynamic_quantities_float_6d  !    Temporary dynamic Float_6D
  type (ids_temporary_dynamic_quantities_float_6d_value) :: value  ! /value - Value
  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

type ids_temporary_constant_quantities_float_6d  !    Temporary constant Float_6D
  real(DP),pointer  :: value(:,:,:,:,:,:) => null()     ! /value - Value
  real(DP),pointer  :: value_error_upper(:,:,:,:,:,:) => null()
  real(DP),pointer  :: value_error_lower(:,:,:,:,:,:) => null()
  integer :: value_error_index=-999999999

  type (ids_identifier) :: identifier  ! /identifier - Description of the quantity using the standard identifier structure
endtype

type ids_temporary  !    Storage of undeclared data model components
  type (ids_ids_properties) :: ids_properties  ! /temporary/ids_properties -
  type (ids_temporary_constant_quantities_float_0d),pointer :: constant_float0d(:) => null()  ! /temporary/constant_float0d(i) - Constant 0D float
  type (ids_temporary_constant_quantities_int_0d),pointer :: constant_integer0d(:) => null()  ! /temporary/constant_integer0d(i) - Constant 0D integer
  type (ids_temporary_constant_quantities_string_0d),pointer :: constant_string0d(:) => null()  ! /temporary/constant_string0d(i) - Constant 0D string
  type (ids_temporary_constant_quantities_int_1d),pointer :: constant_integer1d(:) => null()  ! /temporary/constant_integer1d(i) - Constant 1D integer
  type (ids_temporary_constant_quantities_string_1d),pointer :: constant_string1d(:) => null()  ! /temporary/constant_string1d(i) - Constant 1D string
  type (ids_temporary_constant_quantities_float_1d),pointer :: constant_float1d(:) => null()  ! /temporary/constant_float1d(i) - Constant 1D float
  type (ids_temporary_dynamic_quantities_float_1d),pointer :: dynamic_float1d(:) => null()  ! /temporary/dynamic_float1d(i) - Dynamic 1D float
  type (ids_temporary_dynamic_quantities_int_1d),pointer :: dynamic_integer1d(:) => null()  ! /temporary/dynamic_integer1d(i) - Dynamic 1D integer
  type (ids_temporary_constant_quantities_float_2d),pointer :: constant_float2d(:) => null()  ! /temporary/constant_float2d(i) - Constant 2D float
  type (ids_temporary_constant_quantities_int_2d),pointer :: constant_integer2d(:) => null()  ! /temporary/constant_integer2d(i) - Constant 2D integer
  type (ids_temporary_dynamic_quantities_float_2d),pointer :: dynamic_float2d(:) => null()  ! /temporary/dynamic_float2d(i) - Dynamic 2D float
  type (ids_temporary_dynamic_quantities_int_2d),pointer :: dynamic_integer2d(:) => null()  ! /temporary/dynamic_integer2d(i) - Dynamic 2D integer
  type (ids_temporary_constant_quantities_float_3d),pointer :: constant_float3d(:) => null()  ! /temporary/constant_float3d(i) - Constant 3D float
  type (ids_temporary_constant_quantities_int_3d),pointer :: constant_integer3d(:) => null()  ! /temporary/constant_integer3d(i) - Constant 3D integer
  type (ids_temporary_dynamic_quantities_float_3d),pointer :: dynamic_float3d(:) => null()  ! /temporary/dynamic_float3d(i) - Dynamic 3D float
  type (ids_temporary_dynamic_quantities_int_3d),pointer :: dynamic_integer3d(:) => null()  ! /temporary/dynamic_integer3d(i) - Dynamic 3D integer
  type (ids_temporary_constant_quantities_float_4d),pointer :: constant_float4d(:) => null()  ! /temporary/constant_float4d(i) - Constant 4D float
  type (ids_temporary_dynamic_quantities_float_4d),pointer :: dynamic_float4d(:) => null()  ! /temporary/dynamic_float4d(i) - Dynamic 4D float
  type (ids_temporary_constant_quantities_float_5d),pointer :: constant_float5d(:) => null()  ! /temporary/constant_float5d(i) - Constant 5D float
  type (ids_temporary_dynamic_quantities_float_5d),pointer :: dynamic_float5d(:) => null()  ! /temporary/dynamic_float5d(i) - Dynamic 5D float
  type (ids_temporary_constant_quantities_float_6d),pointer :: constant_float6d(:) => null()  ! /temporary/constant_float6d(i) - Constant 6D float
  type (ids_temporary_dynamic_quantities_float_6d),pointer :: dynamic_float6d(:) => null()  ! /temporary/dynamic_float6d(i) - Dynamic 6D float
  type (ids_code) :: code  ! /temporary/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include tf/dd_tf.xsd
type ids_tf_coil_conductor_elements  !    Elements descibring the conductor contour
  character(len=132), dimension(:), pointer ::names => null()       ! /names - Name or description of every element
  integer,pointer  :: types(:) => null()      ! /types - Type of every element: 1: line segment, its ends are given by the start and end points; index = 2: a
  type (ids_rzphi1d_static) :: start_points  ! /start_points - Position of the start point of every element
  type (ids_rzphi1d_static) :: intermediate_points  ! /intermediate_points - Position of an intermediate point along the arc of circle, for every element, providing the orientat
  type (ids_rzphi1d_static) :: end_points  ! /end_points - Position of the end point of every element. Meaningful only if type/index = 1 or 2, fill with defaul
  type (ids_rzphi1d_static) :: centres  ! /centres - Position of the centre of the arc of a circle of every element (meaningful only if type/index = 2 or
endtype

! SPECIAL STRUCTURE data / time
type ids_tf_coil_conductor_current  !    Current in the conductor
  real(DP), pointer  :: data(:) => null()     ! /current - Current in the conductor
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_tf_coil_conductor_voltage  !    Voltage on the conductor terminals
  real(DP), pointer  :: data(:) => null()     ! /voltage - Voltage on the conductor terminals
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_tf_coil_conductor  !    Description of a conductor
  type (ids_tf_coil_conductor_elements) :: elements  ! /elements - Set of geometrical elements (line segments and/or arcs of a circle) describing the contour of the TF
  type (ids_delta_rzphi1d_static) :: cross_section  ! /cross_section - The cross-section perpendicular to the TF conductor contour is described by a series of contour poin
  real(DP)  :: resistance=-9.0D40       ! /resistance - conductor resistance
  real(DP)  :: resistance_error_upper=-9.0D40
  real(DP)  :: resistance_error_lower=-9.0D40
  integer :: resistance_error_index=-999999999

  type (ids_tf_coil_conductor_current) :: current  ! /current - Current in the conductor
  type (ids_tf_coil_conductor_voltage) :: voltage  ! /voltage - Voltage on the conductor terminals
endtype

! SPECIAL STRUCTURE data / time
type ids_tf_coil_current  !    Current in the coil
  real(DP), pointer  :: data(:) => null()     ! /current - Current in the coil
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_tf_coil_voltage  !    Voltage on the coil terminals
  real(DP), pointer  :: data(:) => null()     ! /voltage - Voltage on the coil terminals
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_tf_coil  !    Description of a given coil
  type (ids_tf_coil_conductor),pointer :: conductor(:) => null()  ! /conductor(i) - Set of conductors inside the coil. The structure can be used with size 1 for a simplified descriptio
  real(DP)  :: turns=-9.0D40       ! /turns - Number of total turns in a toroidal field coil. May be a fraction when describing the coil connectio
  real(DP)  :: turns_error_upper=-9.0D40
  real(DP)  :: turns_error_lower=-9.0D40
  integer :: turns_error_index=-999999999

  real(DP)  :: resistance=-9.0D40       ! /resistance - Coil resistance
  real(DP)  :: resistance_error_upper=-9.0D40
  real(DP)  :: resistance_error_lower=-9.0D40
  integer :: resistance_error_index=-999999999

  type (ids_tf_coil_current) :: current  ! /current - Current in the coil
  type (ids_tf_coil_voltage) :: voltage  ! /voltage - Voltage on the coil terminals
endtype

type ids_tf_ggd  !    Toroidal field map represented on ggd
  type (ids_generic_grid_dynamic) :: grid  ! /grid - Grid description
  type (ids_generic_grid_scalar),pointer :: b_field_r(:) => null()  ! /b_field_r(i) - R component of the vacuum magnetic field, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: b_field_z(:) => null()  ! /b_field_z(i) - Z component of the vacuum magnetic field, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: b_field_tor(:) => null()  ! /b_field_tor(i) - Toroidal component of the vacuum magnetic field, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: a_field_r(:) => null()  ! /a_field_r(i) - R component of the vacuum vector potential, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: a_field_z(:) => null()  ! /a_field_z(i) - Z component of the vacuum vector potential, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: a_field_tor(:) => null()  ! /a_field_tor(i) - Toroidal component of the vacuum vector potential, given on various grid subsets
  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

! SPECIAL STRUCTURE data / time
type ids_tf_b_field_tor_vacuum_r  !    Vacuum field times major radius in the toroidal field magnet. Positive sign means anti-clockwise when viewed from above
  real(DP), pointer  :: data(:) => null()     ! /tf/b_field_tor_vacuum_r - Vacuum field times major radius in the toroidal field magnet. Positive sign means anti-clockwise whe
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_tf  !    Toroidal field coils
  type (ids_ids_properties) :: ids_properties  ! /tf/ids_properties -
  integer  :: is_periodic=-999999999       ! /tf/is_periodic - Flag indicating whether coils are described one by one in the coil() structure (flag=0) or whether t
  integer  :: coils_n=-999999999       ! /tf/coils_n - Number of coils around the torus, in case is_periodic = 1
  type (ids_tf_coil),pointer :: coil(:) => null()  ! /tf/coil(i) - Set of coils around the tokamak
  type (ids_tf_ggd),pointer :: field_map(:) => null()  ! /tf/field_map(i) - Map of the vacuum field at various time slices, represented using the generic grid description
  type (ids_tf_b_field_tor_vacuum_r) :: b_field_tor_vacuum_r  ! /tf/b_field_tor_vacuum_r - Vacuum field times major radius in the toroidal field magnet. Positive sign means anti-clockwise whe
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include thomson_scattering/dd_thomson_scattering.xsd
! SPECIAL STRUCTURE data / time
type ids_thomson_scattering_channel_t_e  !    Electron temperature
  real(DP), pointer  :: data(:) => null()     ! /t_e - Electron temperature
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

! SPECIAL STRUCTURE data / time
type ids_thomson_scattering_channel_n_e  !    Electron density
  real(DP), pointer  :: data(:) => null()     ! /n_e - Electron density
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_thomson_scattering_channel  !    Thomson scattering channel
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of the channel
  character(len=132), dimension(:), pointer ::identifier => null()       ! /identifier - ID of the channel
  type (ids_rzphi0d_static) :: position  ! /position - Position of the measurements (intersection between laser beam and line of sight)
  type (ids_thomson_scattering_channel_t_e) :: t_e  ! /t_e - Electron temperature
  type (ids_thomson_scattering_channel_n_e) :: n_e  ! /n_e - Electron density
endtype

type ids_thomson_scattering  !    Thomson scattering diagnostic
  type (ids_ids_properties) :: ids_properties  ! /thomson_scattering/ids_properties -
  type (ids_thomson_scattering_channel),pointer :: channel(:) => null()  ! /thomson_scattering/channel(i) - Set of channels (lines-of-sight)
  type (ids_code) :: code  ! /thomson_scattering/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include transport_solver_numerics/dd_transport_solver_numerics.xsd
type ids_numerics_profiles_1d_derivatives_charge_state_d  !    Quantities related to a given charge state, derivatives with respect to a given quantity
  real(DP),pointer  :: temperature(:) => null()     ! /temperature - Temperature
  real(DP),pointer  :: temperature_error_upper(:) => null()
  real(DP),pointer  :: temperature_error_lower(:) => null()
  integer :: temperature_error_index=-999999999

  real(DP),pointer  :: density(:) => null()     ! /density - Density (thermal+non-thermal)
  real(DP),pointer  :: density_error_upper(:) => null()
  real(DP),pointer  :: density_error_lower(:) => null()
  integer :: density_error_index=-999999999

  real(DP),pointer  :: density_fast(:) => null()     ! /density_fast - Density of fast (non-thermal) particles
  real(DP),pointer  :: density_fast_error_upper(:) => null()
  real(DP),pointer  :: density_fast_error_lower(:) => null()
  integer :: density_fast_error_index=-999999999

  real(DP),pointer  :: pressure(:) => null()     ! /pressure - Pressure
  real(DP),pointer  :: pressure_error_upper(:) => null()
  real(DP),pointer  :: pressure_error_lower(:) => null()
  integer :: pressure_error_index=-999999999

  real(DP),pointer  :: pressure_fast_perpendicular(:) => null()     ! /pressure_fast_perpendicular - Fast (non-thermal) perpendicular pressure
  real(DP),pointer  :: pressure_fast_perpendicular_error_upper(:) => null()
  real(DP),pointer  :: pressure_fast_perpendicular_error_lower(:) => null()
  integer :: pressure_fast_perpendicular_error_index=-999999999

  real(DP),pointer  :: pressure_fast_parallel(:) => null()     ! /pressure_fast_parallel - Fast (non-thermal) parallel pressure
  real(DP),pointer  :: pressure_fast_parallel_error_upper(:) => null()
  real(DP),pointer  :: pressure_fast_parallel_error_lower(:) => null()
  integer :: pressure_fast_parallel_error_index=-999999999

  real(DP),pointer  :: velocity_tor(:) => null()     ! /velocity_tor - Toroidal velocity
  real(DP),pointer  :: velocity_tor_error_upper(:) => null()
  real(DP),pointer  :: velocity_tor_error_lower(:) => null()
  integer :: velocity_tor_error_index=-999999999

  real(DP),pointer  :: velocity_pol(:) => null()     ! /velocity_pol - Poloidal velocity
  real(DP),pointer  :: velocity_pol_error_upper(:) => null()
  real(DP),pointer  :: velocity_pol_error_lower(:) => null()
  integer :: velocity_pol_error_index=-999999999

endtype

type ids_numerics_profiles_1d_derivatives_charge_state  !    Quantities related to a given charge state
  real(DP)  :: z_min=-9.0D40       ! /z_min - Minimum Z of the charge state bundle
  real(DP)  :: z_min_error_upper=-9.0D40
  real(DP)  :: z_min_error_lower=-9.0D40
  integer :: z_min_error_index=-999999999

  real(DP)  :: z_max=-9.0D40       ! /z_max - Maximum Z of the charge state bundle
  real(DP)  :: z_max_error_upper=-9.0D40
  real(DP)  :: z_max_error_lower=-9.0D40
  integer :: z_max_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  integer  :: is_neutral=-999999999       ! /is_neutral - Flag specifying if this state corresponds to a neutral (1) or not (0)
  type (ids_identifier) :: neutral_type  ! /neutral_type - Neutral type (if the considered state is a neutral), in terms of energy. ID =1: cold; 2: thermal; 3:
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  type (ids_numerics_profiles_1d_derivatives_charge_state_d) :: d_drho_tor_norm  ! /d_drho_tor_norm - Derivatives with respect to the normalised toroidal flux
  type (ids_numerics_profiles_1d_derivatives_charge_state_d) :: d2_drho_tor_norm2  ! /d2_drho_tor_norm2 - Second derivatives with respect to the normalised toroidal flux
  type (ids_numerics_profiles_1d_derivatives_charge_state_d) :: d_dt  ! /d_dt - Derivatives with respect to time
endtype

type ids_numerics_profiles_1d_derivatives_ion_d  !    Quantities related to an ion species, derivatives with respect to a given quantity
  real(DP),pointer  :: temperature(:) => null()     ! /temperature - Temperature (average over charge states when multiple charge states are considered)
  real(DP),pointer  :: temperature_error_upper(:) => null()
  real(DP),pointer  :: temperature_error_lower(:) => null()
  integer :: temperature_error_index=-999999999

  real(DP),pointer  :: density(:) => null()     ! /density - Density (thermal+non-thermal) (sum over charge states when multiple charge states are considered)
  real(DP),pointer  :: density_error_upper(:) => null()
  real(DP),pointer  :: density_error_lower(:) => null()
  integer :: density_error_index=-999999999

  real(DP),pointer  :: density_fast(:) => null()     ! /density_fast - Density of fast (non-thermal) particles (sum over charge states when multiple charge states are cons
  real(DP),pointer  :: density_fast_error_upper(:) => null()
  real(DP),pointer  :: density_fast_error_lower(:) => null()
  integer :: density_fast_error_index=-999999999

  real(DP),pointer  :: pressure(:) => null()     ! /pressure - Pressure (average over charge states when multiple charge states are considered)
  real(DP),pointer  :: pressure_error_upper(:) => null()
  real(DP),pointer  :: pressure_error_lower(:) => null()
  integer :: pressure_error_index=-999999999

  real(DP),pointer  :: pressure_fast_perpendicular(:) => null()     ! /pressure_fast_perpendicular - Fast (non-thermal) perpendicular pressure  (average over charge states when multiple charge states a
  real(DP),pointer  :: pressure_fast_perpendicular_error_upper(:) => null()
  real(DP),pointer  :: pressure_fast_perpendicular_error_lower(:) => null()
  integer :: pressure_fast_perpendicular_error_index=-999999999

  real(DP),pointer  :: pressure_fast_parallel(:) => null()     ! /pressure_fast_parallel - Fast (non-thermal) parallel pressure  (average over charge states when multiple charge states are co
  real(DP),pointer  :: pressure_fast_parallel_error_upper(:) => null()
  real(DP),pointer  :: pressure_fast_parallel_error_lower(:) => null()
  integer :: pressure_fast_parallel_error_index=-999999999

  real(DP),pointer  :: velocity_tor(:) => null()     ! /velocity_tor - Toroidal velocity (average over charge states when multiple charge states are considered)
  real(DP),pointer  :: velocity_tor_error_upper(:) => null()
  real(DP),pointer  :: velocity_tor_error_lower(:) => null()
  integer :: velocity_tor_error_index=-999999999

  real(DP),pointer  :: velocity_pol(:) => null()     ! /velocity_pol - Poloidal velocity (average over charge states when multiple charge states are considered)
  real(DP),pointer  :: velocity_pol_error_upper(:) => null()
  real(DP),pointer  :: velocity_pol_error_lower(:) => null()
  integer :: velocity_pol_error_index=-999999999

endtype

type ids_numerics_profiles_1d_derivatives_ion  !    Quantities related to an ion species
  real(DP)  :: a=-9.0D40       ! /a - Mass of atom
  real(DP)  :: a_error_upper=-9.0D40
  real(DP)  :: a_error_lower=-9.0D40
  integer :: a_error_index=-999999999

  real(DP)  :: z_ion=-9.0D40       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed)
  real(DP)  :: z_ion_error_upper=-9.0D40
  real(DP)  :: z_ion_error_lower=-9.0D40
  integer :: z_ion_error_index=-999999999

  real(DP)  :: z_n=-9.0D40       ! /z_n - Nuclear charge
  real(DP)  :: z_n_error_upper=-9.0D40
  real(DP)  :: z_n_error_lower=-9.0D40
  integer :: z_n_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  type (ids_numerics_profiles_1d_derivatives_ion_d) :: d_drho_tor_norm  ! /d_drho_tor_norm - Derivatives with respect to the normalised toroidal flux
  type (ids_numerics_profiles_1d_derivatives_ion_d) :: d2_drho_tor_norm2  ! /d2_drho_tor_norm2 - Second derivatives with respect to the normalised toroidal flux
  type (ids_numerics_profiles_1d_derivatives_ion_d) :: d_dt  ! /d_dt - Derivatives with respect to time
  integer  :: multiple_states_flag=-999999999       ! /multiple_states_flag - Multiple state calculation flag : 0-Only one state is considered; 1-Multiple states are considered a
  type (ids_numerics_profiles_1d_derivatives_charge_state),pointer :: state(:) => null()  ! /state(i) - Quantities related to the different states of the species (ionisation, energy, excitation, ...)
endtype

type ids_numerics_profiles_1d_derivatives_electrons_d  !    Quantities related to electrons, derivatives with respect to a given quantity
  real(DP),pointer  :: temperature(:) => null()     ! /temperature - Temperature
  real(DP),pointer  :: temperature_error_upper(:) => null()
  real(DP),pointer  :: temperature_error_lower(:) => null()
  integer :: temperature_error_index=-999999999

  real(DP),pointer  :: density(:) => null()     ! /density - Density (thermal+non-thermal)
  real(DP),pointer  :: density_error_upper(:) => null()
  real(DP),pointer  :: density_error_lower(:) => null()
  integer :: density_error_index=-999999999

  real(DP),pointer  :: density_fast(:) => null()     ! /density_fast - Density of fast (non-thermal) particles
  real(DP),pointer  :: density_fast_error_upper(:) => null()
  real(DP),pointer  :: density_fast_error_lower(:) => null()
  integer :: density_fast_error_index=-999999999

  real(DP),pointer  :: pressure(:) => null()     ! /pressure - Pressure
  real(DP),pointer  :: pressure_error_upper(:) => null()
  real(DP),pointer  :: pressure_error_lower(:) => null()
  integer :: pressure_error_index=-999999999

  real(DP),pointer  :: pressure_fast_perpendicular(:) => null()     ! /pressure_fast_perpendicular - Fast (non-thermal) perpendicular pressure
  real(DP),pointer  :: pressure_fast_perpendicular_error_upper(:) => null()
  real(DP),pointer  :: pressure_fast_perpendicular_error_lower(:) => null()
  integer :: pressure_fast_perpendicular_error_index=-999999999

  real(DP),pointer  :: pressure_fast_parallel(:) => null()     ! /pressure_fast_parallel - Fast (non-thermal) parallel pressure
  real(DP),pointer  :: pressure_fast_parallel_error_upper(:) => null()
  real(DP),pointer  :: pressure_fast_parallel_error_lower(:) => null()
  integer :: pressure_fast_parallel_error_index=-999999999

  real(DP),pointer  :: velocity_tor(:) => null()     ! /velocity_tor - Toroidal velocity
  real(DP),pointer  :: velocity_tor_error_upper(:) => null()
  real(DP),pointer  :: velocity_tor_error_lower(:) => null()
  integer :: velocity_tor_error_index=-999999999

  real(DP),pointer  :: velocity_pol(:) => null()     ! /velocity_pol - Poloidal velocity
  real(DP),pointer  :: velocity_pol_error_upper(:) => null()
  real(DP),pointer  :: velocity_pol_error_lower(:) => null()
  integer :: velocity_pol_error_index=-999999999

endtype

type ids_numerics_profiles_1d_derivatives_electrons  !    Quantities related to electrons
  type (ids_numerics_profiles_1d_derivatives_electrons_d) :: d_drho_tor_norm  ! /d_drho_tor_norm - Derivatives with respect to the normalised toroidal flux
  type (ids_numerics_profiles_1d_derivatives_electrons_d) :: d2_drho_tor_norm2  ! /d2_drho_tor_norm2 - Second derivatives with respect to the normalised toroidal flux
  type (ids_numerics_profiles_1d_derivatives_electrons_d) :: d_dt  ! /d_dt - Derivatives with respect to time
endtype

type ids_numerics_profiles_1d_derivatives_total_ions  !    Quantities related to total ion quantities, derivatives with respect to a given quantity
  real(DP),pointer  :: n_i_total_over_n_e(:) => null()     ! /n_i_total_over_n_e - Ratio of total ion density (sum over species and charge states) over electron density. (thermal+non-
  real(DP),pointer  :: n_i_total_over_n_e_error_upper(:) => null()
  real(DP),pointer  :: n_i_total_over_n_e_error_lower(:) => null()
  integer :: n_i_total_over_n_e_error_index=-999999999

  real(DP),pointer  :: pressure_ion_total(:) => null()     ! /pressure_ion_total - Total thermal ion pressure
  real(DP),pointer  :: pressure_ion_total_error_upper(:) => null()
  real(DP),pointer  :: pressure_ion_total_error_lower(:) => null()
  integer :: pressure_ion_total_error_index=-999999999

endtype

type ids_numerics_profiles_1d_derivatives  !    Radial profiles derivatives for a given time slice
  type (ids_core_radial_grid) :: grid  ! /grid - Radial grid
  type (ids_numerics_profiles_1d_derivatives_electrons) :: electrons  ! /electrons - Quantities related to the electrons
  type (ids_numerics_profiles_1d_derivatives_ion),pointer :: ion(:) => null()  ! /ion(i) - Quantities related to the different ion species
  type (ids_numerics_profiles_1d_derivatives_total_ions) :: d_drho_tor_norm  ! /d_drho_tor_norm - Derivatives of total ion quantities with respect to the normalised toroidal flux
  type (ids_numerics_profiles_1d_derivatives_total_ions) :: d2_drho_tor_norm2  ! /d2_drho_tor_norm2 - Second derivatives of total ion quantities with respect to the normalised toroidal flux
  type (ids_numerics_profiles_1d_derivatives_total_ions) :: d_dt  ! /d_dt - Derivatives of total ion quantities with respect to time
  real(DP),pointer  :: dpsi_dt(:) => null()     ! /dpsi_dt - Derivative of the poloidal flux profile with respect to time
  real(DP),pointer  :: dpsi_dt_error_upper(:) => null()
  real(DP),pointer  :: dpsi_dt_error_lower(:) => null()
  integer :: dpsi_dt_error_index=-999999999

  real(DP),pointer  :: dpsi_dt_cphi(:) => null()     ! /dpsi_dt_cphi - Derivative of the poloidal flux profile with respect to time, at constant toroidal flux
  real(DP),pointer  :: dpsi_dt_cphi_error_upper(:) => null()
  real(DP),pointer  :: dpsi_dt_cphi_error_lower(:) => null()
  integer :: dpsi_dt_cphi_error_index=-999999999

  real(DP),pointer  :: dpsi_dt_crho_tor_norm(:) => null()     ! /dpsi_dt_crho_tor_norm - Derivative of the poloidal flux profile with respect to time, at constant normalised toroidal flux c
  real(DP),pointer  :: dpsi_dt_crho_tor_norm_error_upper(:) => null()
  real(DP),pointer  :: dpsi_dt_crho_tor_norm_error_lower(:) => null()
  integer :: dpsi_dt_crho_tor_norm_error_index=-999999999

  real(DP),pointer  :: drho_tor_dt(:) => null()     ! /drho_tor_dt - Partial derivative of the toroidal flux coordinate profile with respect to time
  real(DP),pointer  :: drho_tor_dt_error_upper(:) => null()
  real(DP),pointer  :: drho_tor_dt_error_lower(:) => null()
  integer :: drho_tor_dt_error_index=-999999999

  real(DP),pointer  :: d_dvolume_drho_tor_dt(:) => null()     ! /d_dvolume_drho_tor_dt - Partial derivative with respect to time of the derivative of the volume with respect to the toroidal
  real(DP),pointer  :: d_dvolume_drho_tor_dt_error_upper(:) => null()
  real(DP),pointer  :: d_dvolume_drho_tor_dt_error_lower(:) => null()
  integer :: d_dvolume_drho_tor_dt_error_index=-999999999

  real(DP),pointer  :: dpsi_drho_tor(:) => null()     ! /dpsi_drho_tor - Derivative of the poloidal flux profile with respect to the toroidal flux coordinate
  real(DP),pointer  :: dpsi_drho_tor_error_upper(:) => null()
  real(DP),pointer  :: dpsi_drho_tor_error_lower(:) => null()
  integer :: dpsi_drho_tor_error_index=-999999999

  real(DP),pointer  :: d2psi_drho_tor2(:) => null()     ! /d2psi_drho_tor2 - Second derivative of the poloidal flux profile with respect to the toroidal flux coordinate
  real(DP),pointer  :: d2psi_drho_tor2_error_upper(:) => null()
  real(DP),pointer  :: d2psi_drho_tor2_error_lower(:) => null()
  integer :: d2psi_drho_tor2_error_index=-999999999

  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_numerics_bc_1d_current  !    Boundary conditions for the current diffusion equation
  type (ids_identifier) :: identifier  ! /identifier - Identifier of the boundary condition type. ID = 1: poloidal flux; 2: ip; 3: loop voltage; 4: undefin
  real(DP),pointer  :: value(:) => null()     ! /value - Value of the boundary condition. For ID = 1 to 3, only the first position in the vector is used. For
  real(DP),pointer  :: value_error_upper(:) => null()
  real(DP),pointer  :: value_error_lower(:) => null()
  integer :: value_error_index=-999999999

  real(DP)  :: rho_tor_norm=-9.0D40       ! /rho_tor_norm - Position, in normalised toroidal flux, at which the boundary condition is imposed. Outside this posi
  real(DP)  :: rho_tor_norm_error_upper=-9.0D40
  real(DP)  :: rho_tor_norm_error_lower=-9.0D40
  integer :: rho_tor_norm_error_index=-999999999

endtype

type ids_numerics_bc_ggd_current  !    Boundary conditions for the current diffusion equation
  type (ids_identifier) :: identifier  ! /identifier - Identifier of the boundary condition type. List of options TBD.
  integer  :: grid_index=-999999999       ! /grid_index - Index of the grid used to represent this quantity
  integer  :: grid_subset_index=-999999999       ! /grid_subset_index - Index of the grid subset the data is provided on
  real(DP),pointer  :: values(:,:) => null()     ! /values - List of vector components, one list per element in the grid subset. First dimenstion: element index.
  real(DP),pointer  :: values_error_upper(:,:) => null()
  real(DP),pointer  :: values_error_lower(:,:) => null()
  integer :: values_error_index=-999999999

endtype

type ids_numerics_bc_ggd_bc  !    Boundary conditions for a given transport equation
  type (ids_identifier) :: identifier  ! /identifier - Identifier of the boundary condition type. List of options TBD.
  integer  :: grid_index=-999999999       ! /grid_index - Index of the grid used to represent this quantity
  integer  :: grid_subset_index=-999999999       ! /grid_subset_index - Index of the grid subset the data is provided on
  real(DP),pointer  :: values(:,:) => null()     ! /values - List of vector components, one list per element in the grid subset. First dimenstion: element index.
  real(DP),pointer  :: values_error_upper(:,:) => null()
  real(DP),pointer  :: values_error_lower(:,:) => null()
  integer :: values_error_index=-999999999

endtype

type ids_numerics_bc_1d_bc  !    Boundary conditions for a given transport equation
  type (ids_identifier) :: identifier  ! /identifier - Identifier of the boundary condition type. ID = 1: value of the field y; 2: radial derivative of the
  real(DP),pointer  :: value(:) => null()     ! /value - Value of the boundary condition. For ID = 1 to 4, only the first position in the vector is used. For
  real(DP),pointer  :: value_error_upper(:) => null()
  real(DP),pointer  :: value_error_lower(:) => null()
  integer :: value_error_index=-999999999

  real(DP)  :: rho_tor_norm=-9.0D40       ! /rho_tor_norm - Position, in normalised toroidal flux, at which the boundary condition is imposed. Outside this posi
  real(DP)  :: rho_tor_norm_error_upper=-9.0D40
  real(DP)  :: rho_tor_norm_error_lower=-9.0D40
  integer :: rho_tor_norm_error_index=-999999999

endtype

type ids_numerics_bc_1d_electrons  !    Boundary conditions for the electron related transport equations
  type (ids_numerics_bc_1d_bc) :: particles  ! /particles - Boundary condition for the electron density equation (density if ID = 1)
  type (ids_numerics_bc_1d_bc) :: energy  ! /energy - Boundary condition for the electron energy equation (temperature if ID = 1)
endtype

type ids_numerics_bc_ggd_electrons  !    Boundary conditions for the electron related transport equations
  type (ids_numerics_bc_ggd_bc),pointer :: particles(:) => null()  ! /particles(i) - Boundary condition for the electron density equation (density if ID = 1), on various grid subsets
  type (ids_numerics_bc_ggd_bc),pointer :: energy(:) => null()  ! /energy(i) - Boundary condition for the electron energy equation (temperature if ID = 1), on various grid subsets
endtype

type ids_numerics_bc_1d_ion_charge_state  !    Boundary conditions for a given charge state related transport equations
  real(DP)  :: z_min=-9.0D40       ! /z_min - Minimum Z of the charge state bundle
  real(DP)  :: z_min_error_upper=-9.0D40
  real(DP)  :: z_min_error_lower=-9.0D40
  integer :: z_min_error_index=-999999999

  real(DP)  :: z_max=-9.0D40       ! /z_max - Maximum Z of the charge state bundle
  real(DP)  :: z_max_error_upper=-9.0D40
  real(DP)  :: z_max_error_lower=-9.0D40
  integer :: z_max_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  integer  :: is_neutral=-999999999       ! /is_neutral - Flag specifying if this state corresponds to a neutral (1) or not (0)
  type (ids_identifier) :: neutral_type  ! /neutral_type - Neutral type (if the considered state is a neutral), in terms of energy. ID =1: cold; 2: thermal; 3:
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  type (ids_numerics_bc_1d_bc) :: particles  ! /particles - Boundary condition for the charge state density equation (density if ID = 1)
  type (ids_numerics_bc_1d_bc) :: energy  ! /energy - Boundary condition for the charge state energy equation (temperature if ID = 1)
endtype

type ids_numerics_bc_ggd_ion_charge_state  !    Boundary conditions for a given charge state related transport equations
  real(DP)  :: z_min=-9.0D40       ! /z_min - Minimum Z of the charge state bundle
  real(DP)  :: z_min_error_upper=-9.0D40
  real(DP)  :: z_min_error_lower=-9.0D40
  integer :: z_min_error_index=-999999999

  real(DP)  :: z_max=-9.0D40       ! /z_max - Maximum Z of the charge state bundle
  real(DP)  :: z_max_error_upper=-9.0D40
  real(DP)  :: z_max_error_lower=-9.0D40
  integer :: z_max_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  integer  :: is_neutral=-999999999       ! /is_neutral - Flag specifying if this state corresponds to a neutral (1) or not (0)
  type (ids_identifier) :: neutral_type  ! /neutral_type - Neutral type (if the considered state is a neutral), in terms of energy. ID =1: cold; 2: thermal; 3:
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  type (ids_numerics_bc_ggd_bc),pointer :: particles(:) => null()  ! /particles(i) - Boundary condition for the charge state density equation (density if ID = 1), on various grid subset
  type (ids_numerics_bc_ggd_bc),pointer :: energy(:) => null()  ! /energy(i) - Boundary condition for the charge state energy equation (temperature if ID = 1), on various grid sub
endtype

type ids_numerics_bc_1d_ion  !    Boundary conditions for a given ion species related transport equations
  real(DP)  :: a=-9.0D40       ! /a - Mass of atom
  real(DP)  :: a_error_upper=-9.0D40
  real(DP)  :: a_error_lower=-9.0D40
  integer :: a_error_index=-999999999

  real(DP)  :: z_ion=-9.0D40       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed)
  real(DP)  :: z_ion_error_upper=-9.0D40
  real(DP)  :: z_ion_error_lower=-9.0D40
  integer :: z_ion_error_index=-999999999

  real(DP)  :: z_n=-9.0D40       ! /z_n - Nuclear charge
  real(DP)  :: z_n_error_upper=-9.0D40
  real(DP)  :: z_n_error_lower=-9.0D40
  integer :: z_n_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  type (ids_numerics_bc_1d_bc) :: particles  ! /particles - Boundary condition for the ion density equation (density if ID = 1)
  type (ids_numerics_bc_1d_bc) :: energy  ! /energy - Boundary condition for the ion energy equation (temperature if ID = 1)
  integer  :: multiple_states_flag=-999999999       ! /multiple_states_flag - Multiple states calculation flag : 0-Only one state is considered; 1-Multiple states are considered
  type (ids_numerics_bc_1d_ion_charge_state),pointer :: state(:) => null()  ! /state(i) - Quantities related to the different states of the species (ionisation, energy, excitation, ...)
endtype

type ids_numerics_bc_ggd_ion  !    Boundary conditions for a given ion species related transport equations
  real(DP)  :: a=-9.0D40       ! /a - Mass of atom
  real(DP)  :: a_error_upper=-9.0D40
  real(DP)  :: a_error_lower=-9.0D40
  integer :: a_error_index=-999999999

  real(DP)  :: z_ion=-9.0D40       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed)
  real(DP)  :: z_ion_error_upper=-9.0D40
  real(DP)  :: z_ion_error_lower=-9.0D40
  integer :: z_ion_error_index=-999999999

  real(DP)  :: z_n=-9.0D40       ! /z_n - Nuclear charge
  real(DP)  :: z_n_error_upper=-9.0D40
  real(DP)  :: z_n_error_lower=-9.0D40
  integer :: z_n_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  type (ids_numerics_bc_ggd_bc),pointer :: particles(:) => null()  ! /particles(i) - Boundary condition for the ion density equation (density if ID = 1), on various grid subsets
  type (ids_numerics_bc_ggd_bc),pointer :: energy(:) => null()  ! /energy(i) - Boundary condition for the ion energy equation (temperature if ID = 1), on various grid subsets
  integer  :: multiple_states_flag=-999999999       ! /multiple_states_flag - Multiple states calculation flag : 0-Only one state is considered; 1-Multiple states are considered
  type (ids_numerics_bc_ggd_ion_charge_state),pointer :: state(:) => null()  ! /state(i) - Quantities related to the different states of the species (ionisation, energy, excitation, ...)
endtype

type ids_numerics_bc_1d  !    Boundary conditions of radial transport equations for a given time slice
  type (ids_numerics_bc_1d_current) :: current  ! /current - Boundary condition for the current diffusion equation.
  type (ids_numerics_bc_1d_electrons) :: electrons  ! /electrons - Quantities related to the electrons
  type (ids_numerics_bc_1d_ion),pointer :: ion(:) => null()  ! /ion(i) - Quantities related to the different ion species
  type (ids_numerics_bc_1d_bc) :: energy_ion_total  ! /energy_ion_total - Boundary condition for the ion total (sum over ion species) energy equation (temperature if ID = 1)
  type (ids_numerics_bc_1d_bc) :: momentum_tor  ! /momentum_tor - Boundary condition for the total plasma toroidal momentum equation (summed over ion species and elec
  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_numerics_bc_ggd  !    Boundary conditions of radial transport equations for a given time slice
  type (ids_generic_grid_dynamic) :: grid  ! /grid - Grid description
  type (ids_numerics_bc_ggd_current),pointer :: current(:) => null()  ! /current(i) - Boundary condition for the current diffusion equation, on various grid subsets
  type (ids_numerics_bc_ggd_electrons) :: electrons  ! /electrons - Quantities related to the electrons
  type (ids_numerics_bc_ggd_ion),pointer :: ion(:) => null()  ! /ion(i) - Quantities related to the different ion species
  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_numerics_convergence_equations_single  !    Convergence details of a given transport equation
  integer  :: iterations_n=-999999999       ! /iterations_n - Number of iterations carried out in the convergence loop
endtype

type ids_numerics_convergence_equations_ion_charge_state  !    Boundary conditions for a given charge state related transport equations
  real(DP)  :: z_min=-9.0D40       ! /z_min - Minimum Z of the charge state bundle
  real(DP)  :: z_min_error_upper=-9.0D40
  real(DP)  :: z_min_error_lower=-9.0D40
  integer :: z_min_error_index=-999999999

  real(DP)  :: z_max=-9.0D40       ! /z_max - Maximum Z of the charge state bundle
  real(DP)  :: z_max_error_upper=-9.0D40
  real(DP)  :: z_max_error_lower=-9.0D40
  integer :: z_max_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  integer  :: is_neutral=-999999999       ! /is_neutral - Flag specifying if this state corresponds to a neutral (1) or not (0)
  type (ids_identifier) :: neutral_type  ! /neutral_type - Neutral type (if the considered state is a neutral), in terms of energy. ID =1: cold; 2: thermal; 3:
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  type (ids_numerics_convergence_equations_single) :: particles  ! /particles - Convergence details of the charge state density equation
  type (ids_numerics_convergence_equations_single) :: energy  ! /energy - Convergence details of the charge state energy equation
endtype

type ids_numerics_convergence_equations_ion  !    Convergence details of a given ion species related transport equations
  real(DP)  :: a=-9.0D40       ! /a - Mass of atom
  real(DP)  :: a_error_upper=-9.0D40
  real(DP)  :: a_error_lower=-9.0D40
  integer :: a_error_index=-999999999

  real(DP)  :: z_ion=-9.0D40       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed)
  real(DP)  :: z_ion_error_upper=-9.0D40
  real(DP)  :: z_ion_error_lower=-9.0D40
  integer :: z_ion_error_index=-999999999

  real(DP)  :: z_n=-9.0D40       ! /z_n - Nuclear charge
  real(DP)  :: z_n_error_upper=-9.0D40
  real(DP)  :: z_n_error_lower=-9.0D40
  integer :: z_n_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying ion (e.g. H+, D+, T+, He+2, C+, ...)
  type (ids_numerics_convergence_equations_single) :: particles  ! /particles - Convergence details of the  ion density equation
  type (ids_numerics_convergence_equations_single) :: energy  ! /energy - Convergence details of the ion energy equation
  integer  :: multiple_states_flag=-999999999       ! /multiple_states_flag - Multiple state calculation flag : 0-Only one state is considered; 1-Multiple states are considered a
  type (ids_numerics_convergence_equations_ion_charge_state),pointer :: state(:) => null()  ! /state(i) - Convergence details of the related to the different states transport equations
endtype

type ids_numerics_convergence_equations_electrons  !    Convergence details for the electron related equations
  type (ids_numerics_convergence_equations_single) :: particles  ! /particles - Convergence details of the electron density equation
  type (ids_numerics_convergence_equations_single) :: energy  ! /energy - Convergence details of the electron energy equation
endtype

type ids_numerics_convergence_equations  !    Convergence details of a given transport equation
  type (ids_numerics_convergence_equations_single) :: current  ! /current - Convergence details of the current diffusion equation
  type (ids_numerics_convergence_equations_electrons) :: electrons  ! /electrons - Quantities related to the electrons
  type (ids_numerics_convergence_equations_ion),pointer :: ion(:) => null()  ! /ion(i) - Quantities related to the different ion species
  type (ids_numerics_convergence_equations_single) :: energy_ion_total  ! /energy_ion_total - Convergence details of the ion total (sum over ion species) energy equation
  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

! SPECIAL STRUCTURE data / time
type ids_numerics_convergence_time_step  !    Internal time step used by the transport solver (assuming all transport equations are solved with the same time step)
  real(DP), pointer  :: data(:) => null()     ! /time_step - Internal time step used by the transport solver (assuming all transport equations are solved with th
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_numerics_convergence  !    Convergence details
  type (ids_numerics_convergence_time_step) :: time_step  ! /time_step - Internal time step used by the transport solver (assuming all transport equations are solved with th
  type (ids_numerics_convergence_equations),pointer :: equations(:) => null()  ! /equations(i) - Convergence details of the transport equations, for various time slices
endtype

type ids_transport_solver_numerics  !    Numerical quantities used by transport solvers and convergence details
  type (ids_ids_properties) :: ids_properties  ! /transport_solver_numerics/ids_properties -
  type (ids_numerics_profiles_1d_derivatives),pointer :: derivatives_1d(:) => null()  ! /transport_solver_numerics/derivatives_1d(i) - Radial profiles derivatives for various time slices
  type (ids_numerics_bc_1d),pointer :: boundary_conditions_1d(:) => null()  ! /transport_solver_numerics/boundary_conditions_1d(i) - Boundary conditions of the radial transport equations for various time slices
  type (ids_numerics_bc_ggd),pointer :: boundary_conditions_ggd(:) => null()  ! /transport_solver_numerics/boundary_conditions_ggd(i) - Boundary conditions of the transport equations, provided on the GGD, for various time slices
  type (ids_numerics_convergence) :: convergence  ! /transport_solver_numerics/convergence - Convergence details
  type (ids_b_tor_vacuum_1) :: vacuum_toroidal_field  ! /transport_solver_numerics/vacuum_toroidal_field - Characteristics of the vacuum toroidal field (used in rho_tor definition and in the normalization of
  type (ids_code) :: code  ! /transport_solver_numerics/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include wall/dd_wall.xsd
type ids_wall_global_quantitites_electrons  !    Simple 0D description of plasma-wall interaction, related to electrons
  real(DP),pointer  :: pumping_speed(:) => null()     ! /pumping_speed - Pumped particle flux (in equivalent electrons)
  real(DP),pointer  :: pumping_speed_error_upper(:) => null()
  real(DP),pointer  :: pumping_speed_error_lower(:) => null()
  integer :: pumping_speed_error_index=-999999999

  real(DP),pointer  :: particle_flux_from_plasma(:) => null()     ! /particle_flux_from_plasma - Particle flux from the plasma (in equivalent electrons)
  real(DP),pointer  :: particle_flux_from_plasma_error_upper(:) => null()
  real(DP),pointer  :: particle_flux_from_plasma_error_lower(:) => null()
  integer :: particle_flux_from_plasma_error_index=-999999999

  real(DP),pointer  :: particle_flux_from_wall(:,:) => null()     ! /particle_flux_from_wall - Particle flux from the wall corresponding to the conversion into various neutral types (first dimens
  real(DP),pointer  :: particle_flux_from_wall_error_upper(:,:) => null()
  real(DP),pointer  :: particle_flux_from_wall_error_lower(:,:) => null()
  integer :: particle_flux_from_wall_error_index=-999999999

  real(DP),pointer  :: gas_puff(:) => null()     ! /gas_puff - Gas puff rate (in equivalent electrons)
  real(DP),pointer  :: gas_puff_error_upper(:) => null()
  real(DP),pointer  :: gas_puff_error_lower(:) => null()
  integer :: gas_puff_error_index=-999999999

endtype

type ids_wall_global_quantitites_neutral  !    Simple 0D description of plasma-wall interaction, related to a given neutral species
  type (ids_plasma_composition_neutral_element_constant),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H, D, CD4, ...)
  real(DP),pointer  :: pumping_speed(:) => null()     ! /pumping_speed - Pumped particle flux for that species
  real(DP),pointer  :: pumping_speed_error_upper(:) => null()
  real(DP),pointer  :: pumping_speed_error_lower(:) => null()
  integer :: pumping_speed_error_index=-999999999

  real(DP),pointer  :: particle_flux_from_plasma(:) => null()     ! /particle_flux_from_plasma - Particle flux from the plasma for that species
  real(DP),pointer  :: particle_flux_from_plasma_error_upper(:) => null()
  real(DP),pointer  :: particle_flux_from_plasma_error_lower(:) => null()
  integer :: particle_flux_from_plasma_error_index=-999999999

  real(DP),pointer  :: particle_flux_from_wall(:,:) => null()     ! /particle_flux_from_wall - Particle flux from the wall corresponding to the conversion into various neutral types (first dimens
  real(DP),pointer  :: particle_flux_from_wall_error_upper(:,:) => null()
  real(DP),pointer  :: particle_flux_from_wall_error_lower(:,:) => null()
  integer :: particle_flux_from_wall_error_index=-999999999

  real(DP),pointer  :: gas_puff(:) => null()     ! /gas_puff - Gas puff rate for that species
  real(DP),pointer  :: gas_puff_error_upper(:) => null()
  real(DP),pointer  :: gas_puff_error_lower(:) => null()
  integer :: gas_puff_error_index=-999999999

  real(DP),pointer  :: wall_inventory(:) => null()     ! /wall_inventory - Wall inventory, i.e. cumulated exchange of neutral species between plasma and wall from t = 0, posit
  real(DP),pointer  :: wall_inventory_error_upper(:) => null()
  real(DP),pointer  :: wall_inventory_error_lower(:) => null()
  integer :: wall_inventory_error_index=-999999999

  real(DP),pointer  :: recycling_particles_coefficient(:,:) => null()     ! /recycling_particles_coefficient - Particle recycling coefficient corresponding to the conversion into various neutral types (first dim
  real(DP),pointer  :: recycling_particles_coefficient_error_upper(:,:) => null()
  real(DP),pointer  :: recycling_particles_coefficient_error_lower(:,:) => null()
  integer :: recycling_particles_coefficient_error_index=-999999999

  real(DP),pointer  :: recycling_energy_coefficient(:,:) => null()     ! /recycling_energy_coefficient - Energy recycling coefficient corresponding to the conversion into various neutral types (first dimen
  real(DP),pointer  :: recycling_energy_coefficient_error_upper(:,:) => null()
  real(DP),pointer  :: recycling_energy_coefficient_error_lower(:,:) => null()
  integer :: recycling_energy_coefficient_error_index=-999999999

  real(DP),pointer  :: sputtering_physical_coefficient(:,:) => null()     ! /sputtering_physical_coefficient - Effective coefficient of physical sputtering for various neutral types (first dimension: 1: cold; 2:
  real(DP),pointer  :: sputtering_physical_coefficient_error_upper(:,:) => null()
  real(DP),pointer  :: sputtering_physical_coefficient_error_lower(:,:) => null()
  integer :: sputtering_physical_coefficient_error_index=-999999999

  real(DP),pointer  :: sputtering_chemical_coefficient(:,:) => null()     ! /sputtering_chemical_coefficient - Effective coefficient of chemical sputtering for various neutral types (first dimension: 1: cold; 2:
  real(DP),pointer  :: sputtering_chemical_coefficient_error_upper(:,:) => null()
  real(DP),pointer  :: sputtering_chemical_coefficient_error_lower(:,:) => null()
  integer :: sputtering_chemical_coefficient_error_index=-999999999

endtype

type ids_wall_global_quantitites  !    Simple 0D description of plasma-wall interaction
  type (ids_wall_global_quantitites_electrons) :: electrons  ! /electrons - Quantities related to electrons
  type (ids_wall_global_quantitites_neutral),pointer :: neutral(:) => null()  ! /neutral(i) - Quantities related to the various neutral species
  real(DP),pointer  :: temperature(:) => null()     ! /temperature - Wall temperature
  real(DP),pointer  :: temperature_error_upper(:) => null()
  real(DP),pointer  :: temperature_error_lower(:) => null()
  integer :: temperature_error_index=-999999999

  real(DP),pointer  :: power_from_plasma(:) => null()     ! /power_from_plasma - Power flowing from the plasma to the wall
  real(DP),pointer  :: power_from_plasma_error_upper(:) => null()
  real(DP),pointer  :: power_from_plasma_error_lower(:) => null()
  integer :: power_from_plasma_error_index=-999999999

  real(DP),pointer  :: power_to_cooling(:) => null()     ! /power_to_cooling - Power to cooling systems
  real(DP),pointer  :: power_to_cooling_error_upper(:) => null()
  real(DP),pointer  :: power_to_cooling_error_lower(:) => null()
  integer :: power_to_cooling_error_index=-999999999

endtype

! SPECIAL STRUCTURE data / time
type ids_wall_2d_vessel_element_j_tor  !    Toroidal current induced in this block element
  real(DP), pointer  :: data(:) => null()     ! /j_tor - Toroidal current induced in this block element
  real(DP), pointer  :: data_error_upper(:) => null()
  real(DP), pointer  :: data_error_lower(:) => null()
  integer  :: data_error_index=-999999999

  real(DP), pointer  :: time(:) => null()  ! time
endtype

type ids_wall_2d_vessel_element  !    2D vessel block element description
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of the block element
  type (ids_rz1d_static) :: outline  ! /outline - Outline of the block element. Do NOT repeat the first point for closed contours
  real(DP)  :: resistivity=-9.0D40       ! /resistivity - Resistivity of the block element
  real(DP)  :: resistivity_error_upper=-9.0D40
  real(DP)  :: resistivity_error_lower=-9.0D40
  integer :: resistivity_error_index=-999999999

  type (ids_wall_2d_vessel_element_j_tor) :: j_tor  ! /j_tor - Toroidal current induced in this block element
  real(DP)  :: resistance=-9.0D40       ! /resistance - Resistivity of the block element
  real(DP)  :: resistance_error_upper=-9.0D40
  real(DP)  :: resistance_error_lower=-9.0D40
  integer :: resistance_error_index=-999999999

endtype

type ids_wall_2d_vessel_annular  !    2D vessel annular description
  type (ids_rz1d_static) :: outline_inner  ! /outline_inner - Inner vessel outline. Do NOT repeat the first point for closed contours
  type (ids_rz1d_static) :: outline_outer  ! /outline_outer - Outer vessel outline. Do NOT repeat the first point for closed contours
  real(DP)  :: resistivity=-9.0D40       ! /resistivity - Resistivity of the vessel unit
  real(DP)  :: resistivity_error_upper=-9.0D40
  real(DP)  :: resistivity_error_lower=-9.0D40
  integer :: resistivity_error_index=-999999999

endtype

type ids_wall_2d_vessel_unit  !    2D vessel unit description
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of the vessel unit
  type (ids_wall_2d_vessel_annular) :: annular  ! /annular - Annular representation of a vessel layer by two contours, inner and outer
  type (ids_wall_2d_vessel_element),pointer :: element(:) => null()  ! /element(i) - Set of block elements
endtype

type ids_wall_2d_vessel  !    2D vessel description
  type (ids_identifier_static) :: type  ! /type - Type of the vessel description. index = 0 for the official single/multiple annular vessel and 1 for
  type (ids_wall_2d_vessel_unit),pointer :: unit(:) => null()  ! /unit(i) - Set of vessel units
endtype

type ids_wall_2d_limiter_unit  !    2D limiter unit description
  character(len=132), dimension(:), pointer ::name => null()       ! /name - Name of the limiter unit
  integer  :: closed=-999999999       ! /closed - Flag identifying whether the contour is closed (1) or open (0)
  type (ids_rz1d_static) :: outline  ! /outline - Irregular outline of the limiting surface. Do NOT repeat the first point for closed contours
  real(DP)  :: resistivity=-9.0D40       ! /resistivity - Resistivity of the limiter unit
  real(DP)  :: resistivity_error_upper=-9.0D40
  real(DP)  :: resistivity_error_lower=-9.0D40
  integer :: resistivity_error_index=-999999999

endtype

type ids_wall_2d_limiter  !    2D limiter description
  type (ids_identifier_static) :: type  ! /type - Type of the limiter description. index = 0 for the official single contour limiter and 1 for the off
  type (ids_wall_2d_limiter_unit),pointer :: unit(:) => null()  ! /unit(i) - Set of limiter units
endtype

type ids_wall_2d  !    2D wall description
  type (ids_identifier_static) :: type  ! /type - Type of the description. index = 0 for equilibrium codes (single closed limiter and vessel); 1 for g
  type (ids_wall_2d_limiter) :: limiter  ! /limiter - Description of the immobile limiting surface(s) or plasma facing components for defining the Last Cl
  type (ids_wall_2d_vessel) :: vessel  ! /vessel - Mechanical structure of the vacuum vessel. The vessel is described as a set of nested layers with gi
endtype

type ids_wall  !    Description of the torus wall and its interaction with the plasma
  type (ids_ids_properties) :: ids_properties  ! /wall/ids_properties -
  type (ids_wall_global_quantitites) :: global_quantities  ! /wall/global_quantities - Simple 0D description of plasma-wall interaction
  type (ids_wall_2d),pointer :: description_2d(:) => null()  ! /wall/description_2d(i) - Set of 2D wall descriptions, for each type of possible physics or engineering configurations necessa
  type (ids_code) :: code  ! /wall/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

! ***********  Include waves/dd_waves.xsd
type ids_waves_CPX_1D  !    Structure for 1D complex number
  real(DP),pointer  :: real(:) => null()     ! /real - Real part
  real(DP),pointer  :: real_error_upper(:) => null()
  real(DP),pointer  :: real_error_lower(:) => null()
  integer :: real_error_index=-999999999

  real(DP),pointer  :: imaginary(:) => null()     ! /imaginary - Imaginary part
  real(DP),pointer  :: imaginary_error_upper(:) => null()
  real(DP),pointer  :: imaginary_error_lower(:) => null()
  integer :: imaginary_error_index=-999999999

endtype

type ids_waves_rzphipsitheta1d_dynamic_aos3  !    Structure for R, Z, Phi, Psi, Theta positions (1D, dynamic within a type 3 array of structure)
  real(DP),pointer  :: r(:) => null()     ! /r - Major radius
  real(DP),pointer  :: r_error_upper(:) => null()
  real(DP),pointer  :: r_error_lower(:) => null()
  integer :: r_error_index=-999999999

  real(DP),pointer  :: z(:) => null()     ! /z - Height
  real(DP),pointer  :: z_error_upper(:) => null()
  real(DP),pointer  :: z_error_lower(:) => null()
  integer :: z_error_index=-999999999

  real(DP),pointer  :: phi(:) => null()     ! /phi - Toroidal angle
  real(DP),pointer  :: phi_error_upper(:) => null()
  real(DP),pointer  :: phi_error_lower(:) => null()
  integer :: phi_error_index=-999999999

  real(DP),pointer  :: psi(:) => null()     ! /psi - Poloidal flux
  real(DP),pointer  :: psi_error_upper(:) => null()
  real(DP),pointer  :: psi_error_lower(:) => null()
  integer :: psi_error_index=-999999999

  real(DP),pointer  :: theta(:) => null()     ! /theta - Poloidal angle
  real(DP),pointer  :: theta_error_upper(:) => null()
  real(DP),pointer  :: theta_error_lower(:) => null()
  integer :: theta_error_index=-999999999

endtype

type ids_waves_coherent_wave_beam_tracing_ion_state  !    State related quantities for beam tracing
  real(DP)  :: z_min=-9.0D40       ! /z_min - Minimum Z of the charge state bundle (z_min = z_max = 0 for a neutral)
  real(DP)  :: z_min_error_upper=-9.0D40
  real(DP)  :: z_min_error_lower=-9.0D40
  integer :: z_min_error_index=-999999999

  real(DP)  :: z_max=-9.0D40       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(DP)  :: z_max_error_upper=-9.0D40
  real(DP)  :: z_max_error_lower=-9.0D40
  integer :: z_max_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  real(DP),pointer  :: power(:) => null()     ! /power - Power absorbed along the beam by the species
  real(DP),pointer  :: power_error_upper(:) => null()
  real(DP),pointer  :: power_error_lower(:) => null()
  integer :: power_error_index=-999999999

endtype

type ids_waves_coherent_wave_beam_tracing_ion  !    Ion related quantities for beam tracing
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(DP)  :: z_ion=-9.0D40       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed).
  real(DP)  :: z_ion_error_upper=-9.0D40
  real(DP)  :: z_ion_error_lower=-9.0D40
  integer :: z_ion_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H+, D+, T+, He+2, C+, D2, DT, CD4, ...)
  real(DP),pointer  :: power(:) => null()     ! /power - Power absorbed along the beam by the species
  real(DP),pointer  :: power_error_upper(:) => null()
  real(DP),pointer  :: power_error_lower(:) => null()
  integer :: power_error_index=-999999999

  integer  :: multiple_states_flag=-999999999       ! /multiple_states_flag - Multiple state calculation flag : 0-Only one state is considered; 1-Multiple states are considered a
  type (ids_waves_coherent_wave_beam_tracing_ion_state),pointer :: state(:) => null()  ! /state(i) - Collisional exchange with the various states of the ion species (ionisation, energy, excitation, ...
endtype

type ids_waves_coherent_wave_beam_tracing_electrons  !    Electrons related quantities for beam tracing
  real(DP),pointer  :: power(:) => null()     ! /power - Power absorbed along the beam by the species
  real(DP),pointer  :: power_error_upper(:) => null()
  real(DP),pointer  :: power_error_lower(:) => null()
  integer :: power_error_index=-999999999

endtype

type ids_waves_coherent_wave_beam_tracing_power_flow  !    Power flow for beam tracing
  real(DP),pointer  :: perpendicular(:) => null()     ! /perpendicular - Normalized power flow in the direction perpendicular to the magnetic field
  real(DP),pointer  :: perpendicular_error_upper(:) => null()
  real(DP),pointer  :: perpendicular_error_lower(:) => null()
  integer :: perpendicular_error_index=-999999999

  real(DP),pointer  :: parallel(:) => null()     ! /parallel - Normalized power flow in the direction parallel to the magnetic field
  real(DP),pointer  :: parallel_error_upper(:) => null()
  real(DP),pointer  :: parallel_error_lower(:) => null()
  integer :: parallel_error_index=-999999999

endtype

type ids_waves_coherent_wave_beam_tracing_beam_e_field  !    Components of the electric field for beam tracing
  type (ids_waves_CPX_1D) :: plus  ! /plus - Left hand polarised electric field component
  type (ids_waves_CPX_1D) :: minus  ! /minus - Right hand polarised electric field component
  type (ids_waves_CPX_1D) :: parallel  ! /parallel - Parallel to magnetic field polarised electric field component
endtype

type ids_waves_coherent_wave_beam_tracing_beam_k  !    Beam wave vector
  real(DP),pointer  :: k_r(:) => null()     ! /k_r - Wave vector component in the major radius direction
  real(DP),pointer  :: k_r_error_upper(:) => null()
  real(DP),pointer  :: k_r_error_lower(:) => null()
  integer :: k_r_error_index=-999999999

  real(DP),pointer  :: k_z(:) => null()     ! /k_z - Wave vector component in the vertical direction
  real(DP),pointer  :: k_z_error_upper(:) => null()
  real(DP),pointer  :: k_z_error_lower(:) => null()
  integer :: k_z_error_index=-999999999

  real(DP),pointer  :: k_tor(:) => null()     ! /k_tor - Wave vector component in the toroidal direction
  real(DP),pointer  :: k_tor_error_upper(:) => null()
  real(DP),pointer  :: k_tor_error_lower(:) => null()
  integer :: k_tor_error_index=-999999999

  real(DP),pointer  :: n_parallel(:) => null()     ! /n_parallel - Parallel refractive index
  real(DP),pointer  :: n_parallel_error_upper(:) => null()
  real(DP),pointer  :: n_parallel_error_lower(:) => null()
  integer :: n_parallel_error_index=-999999999

  real(DP),pointer  :: n_perpendicular(:) => null()     ! /n_perpendicular - Perpendicular refractive index
  real(DP),pointer  :: n_perpendicular_error_upper(:) => null()
  real(DP),pointer  :: n_perpendicular_error_lower(:) => null()
  integer :: n_perpendicular_error_index=-999999999

  integer,pointer  :: n_tor(:) => null()      ! /n_tor - Toroidal wave number, contains a single value if varying_ntor = 1 to avoid useless repetition consta
  integer  :: varying_n_tor=-999999999       ! /varying_n_tor - Flag telling whether n_tor is constant along the ray path (0) or varying (1)
endtype

type ids_waves_coherent_wave_beam_tracing_beam  !    Beam description
  real(DP)  :: power_initial=-9.0D40       ! /power_initial - Initial power in the ray/beam
  real(DP)  :: power_initial_error_upper=-9.0D40
  real(DP)  :: power_initial_error_lower=-9.0D40
  integer :: power_initial_error_index=-999999999

  real(DP),pointer  :: length(:) => null()     ! /length - Ray/beam curvilinear length
  real(DP),pointer  :: length_error_upper(:) => null()
  real(DP),pointer  :: length_error_lower(:) => null()
  integer :: length_error_index=-999999999

  type (ids_waves_rzphipsitheta1d_dynamic_aos3) :: position  ! /position - Position of the ray/beam along its path
  type (ids_waves_coherent_wave_beam_tracing_beam_k) :: wave_vector  ! /wave_vector - Wave vector of the ray/beam along its path
  type (ids_waves_coherent_wave_beam_tracing_beam_e_field) :: e_field  ! /e_field - Electric field polarization of the ray/beam along its path
  type (ids_waves_coherent_wave_beam_tracing_power_flow) :: power_flow_norm  ! /power_flow_norm - Normalised power flow
  type (ids_waves_coherent_wave_beam_tracing_electrons) :: electrons  ! /electrons - Quantities related to the electrons
  type (ids_waves_coherent_wave_beam_tracing_ion),pointer :: ion(:) => null()  ! /ion(i) - Quantities related to the different ion species
endtype

type ids_waves_coherent_wave_beam_tracing  !    Beam tracing calculations for a given time slice
  type (ids_waves_coherent_wave_beam_tracing_beam),pointer :: beam(:) => null()  ! /beam(i) - Set of rays/beams describing the wave propagation
  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_waves_coherent_wave_global_quantities_ion_state  !    Global quantities related to a given ion species state
  real(DP)  :: z_min=-9.0D40       ! /z_min - Minimum Z of the charge state bundle (z_min = z_max = 0 for a neutral)
  real(DP)  :: z_min_error_upper=-9.0D40
  real(DP)  :: z_min_error_lower=-9.0D40
  integer :: z_min_error_index=-999999999

  real(DP)  :: z_max=-9.0D40       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(DP)  :: z_max_error_upper=-9.0D40
  real(DP)  :: z_max_error_lower=-9.0D40
  integer :: z_max_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  real(DP)  :: power_thermal=-9.0D40       ! /power_thermal - Wave power absorbed by the thermal particle population
  real(DP)  :: power_thermal_error_upper=-9.0D40
  real(DP)  :: power_thermal_error_lower=-9.0D40
  integer :: power_thermal_error_index=-999999999

  real(DP),pointer  :: power_thermal_n_tor(:) => null()     ! /power_thermal_n_tor - Wave power absorbed by the thermal particle population per toroidal mode number
  real(DP),pointer  :: power_thermal_n_tor_error_upper(:) => null()
  real(DP),pointer  :: power_thermal_n_tor_error_lower(:) => null()
  integer :: power_thermal_n_tor_error_index=-999999999

  real(DP)  :: power_fast=-9.0D40       ! /power_fast - Wave power absorbed by the fast particle population
  real(DP)  :: power_fast_error_upper=-9.0D40
  real(DP)  :: power_fast_error_lower=-9.0D40
  integer :: power_fast_error_index=-999999999

  real(DP),pointer  :: power_fast_n_tor(:) => null()     ! /power_fast_n_tor - Wave power absorbed by the fast particle population per toroidal mode number
  real(DP),pointer  :: power_fast_n_tor_error_upper(:) => null()
  real(DP),pointer  :: power_fast_n_tor_error_lower(:) => null()
  integer :: power_fast_n_tor_error_index=-999999999

endtype

type ids_waves_coherent_wave_global_quantities_ion  !    Global quantities related to a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(DP)  :: z_ion=-9.0D40       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed).
  real(DP)  :: z_ion_error_upper=-9.0D40
  real(DP)  :: z_ion_error_lower=-9.0D40
  integer :: z_ion_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H+, D+, T+, He+2, C+, D2, DT, CD4, ...)
  real(DP)  :: power_thermal=-9.0D40       ! /power_thermal - Wave power absorbed by the thermal particle population
  real(DP)  :: power_thermal_error_upper=-9.0D40
  real(DP)  :: power_thermal_error_lower=-9.0D40
  integer :: power_thermal_error_index=-999999999

  real(DP),pointer  :: power_thermal_n_tor(:) => null()     ! /power_thermal_n_tor - Wave power absorbed by the thermal particle population per toroidal mode number
  real(DP),pointer  :: power_thermal_n_tor_error_upper(:) => null()
  real(DP),pointer  :: power_thermal_n_tor_error_lower(:) => null()
  integer :: power_thermal_n_tor_error_index=-999999999

  real(DP)  :: power_fast=-9.0D40       ! /power_fast - Wave power absorbed by the fast particle population
  real(DP)  :: power_fast_error_upper=-9.0D40
  real(DP)  :: power_fast_error_lower=-9.0D40
  integer :: power_fast_error_index=-999999999

  real(DP),pointer  :: power_fast_n_tor(:) => null()     ! /power_fast_n_tor - Wave power absorbed by the fast particle population per toroidal mode number
  real(DP),pointer  :: power_fast_n_tor_error_upper(:) => null()
  real(DP),pointer  :: power_fast_n_tor_error_lower(:) => null()
  integer :: power_fast_n_tor_error_index=-999999999

  integer  :: multiple_states_flag=-999999999       ! /multiple_states_flag - Multiple state calculation flag : 0-Only one state is considered; 1-Multiple states are considered a
  integer  :: distribution_assumption=-999999999       ! /distribution_assumption - Assumption on the distribution function used by the wave solver to calculate the power deposition on
  type (ids_waves_coherent_wave_global_quantities_ion_state),pointer :: state(:) => null()  ! /state(i) - Collisional exchange with the various states of the ion species (ionisation, energy, excitation, ...
endtype

type ids_waves_coherent_wave_global_quantities_electrons  !    Global quantities related to electrons
  real(DP)  :: power_thermal=-9.0D40       ! /power_thermal - Wave power absorbed by the thermal particle population
  real(DP)  :: power_thermal_error_upper=-9.0D40
  real(DP)  :: power_thermal_error_lower=-9.0D40
  integer :: power_thermal_error_index=-999999999

  real(DP),pointer  :: power_thermal_n_tor(:) => null()     ! /power_thermal_n_tor - Wave power absorbed by the thermal particle population per toroidal mode number
  real(DP),pointer  :: power_thermal_n_tor_error_upper(:) => null()
  real(DP),pointer  :: power_thermal_n_tor_error_lower(:) => null()
  integer :: power_thermal_n_tor_error_index=-999999999

  real(DP)  :: power_fast=-9.0D40       ! /power_fast - Wave power absorbed by the fast particle population
  real(DP)  :: power_fast_error_upper=-9.0D40
  real(DP)  :: power_fast_error_lower=-9.0D40
  integer :: power_fast_error_index=-999999999

  real(DP),pointer  :: power_fast_n_tor(:) => null()     ! /power_fast_n_tor - Wave power absorbed by the fast particle population per toroidal mode number
  real(DP),pointer  :: power_fast_n_tor_error_upper(:) => null()
  real(DP),pointer  :: power_fast_n_tor_error_lower(:) => null()
  integer :: power_fast_n_tor_error_index=-999999999

  integer  :: distribution_assumption=-999999999       ! /distribution_assumption - Assumption on the distribution function used by the wave solver to calculate the power deposition on
endtype

type ids_waves_coherent_wave_global_quantities  !    Global quantities (RF waves) for a given time slice
  real(DP)  :: frequency=-9.0D40       ! /frequency - Wave frequency
  real(DP)  :: frequency_error_upper=-9.0D40
  real(DP)  :: frequency_error_lower=-9.0D40
  integer :: frequency_error_index=-999999999

  integer,pointer  :: n_tor(:) => null()      ! /n_tor - Toroidal mode numbers
  real(DP)  :: power=-9.0D40       ! /power - Total absorbed wave power
  real(DP)  :: power_error_upper=-9.0D40
  real(DP)  :: power_error_lower=-9.0D40
  integer :: power_error_index=-999999999

  real(DP),pointer  :: power_n_tor(:) => null()     ! /power_n_tor - Absorbed wave power per toroidal mode number
  real(DP),pointer  :: power_n_tor_error_upper(:) => null()
  real(DP),pointer  :: power_n_tor_error_lower(:) => null()
  integer :: power_n_tor_error_index=-999999999

  real(DP)  :: current_tor=-9.0D40       ! /current_tor - Wave driven toroidal current from a stand alone calculation (not consistent with other sources)
  real(DP)  :: current_tor_error_upper=-9.0D40
  real(DP)  :: current_tor_error_lower=-9.0D40
  integer :: current_tor_error_index=-999999999

  real(DP),pointer  :: current_tor_n_tor(:) => null()     ! /current_tor_n_tor - Wave driven toroidal current from a stand alone calculation (not consistent with other sources) per
  real(DP),pointer  :: current_tor_n_tor_error_upper(:) => null()
  real(DP),pointer  :: current_tor_n_tor_error_lower(:) => null()
  integer :: current_tor_n_tor_error_index=-999999999

  type (ids_waves_coherent_wave_global_quantities_electrons) :: electrons  ! /electrons - Quantities related to the electrons
  type (ids_waves_coherent_wave_global_quantities_ion),pointer :: ion(:) => null()  ! /ion(i) - Quantities related to the different ion species
  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_waves_coherent_wave_profiles_1d_ion_state  !    Radial profiles (RF waves) related to a given ion species state
  real(DP)  :: z_min=-9.0D40       ! /z_min - Minimum Z of the charge state bundle (z_min = z_max = 0 for a neutral)
  real(DP)  :: z_min_error_upper=-9.0D40
  real(DP)  :: z_min_error_lower=-9.0D40
  integer :: z_min_error_index=-999999999

  real(DP)  :: z_max=-9.0D40       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(DP)  :: z_max_error_upper=-9.0D40
  real(DP)  :: z_max_error_lower=-9.0D40
  integer :: z_max_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  real(DP),pointer  :: power_density_thermal(:) => null()     ! /power_density_thermal - Flux surface averaged absorbed wave power density on the thermal species
  real(DP),pointer  :: power_density_thermal_error_upper(:) => null()
  real(DP),pointer  :: power_density_thermal_error_lower(:) => null()
  integer :: power_density_thermal_error_index=-999999999

  real(DP),pointer  :: power_density_thermal_n_tor(:,:) => null()     ! /power_density_thermal_n_tor - Flux surface averaged absorbed wave power density on the thermal species, per toroidal mode number
  real(DP),pointer  :: power_density_thermal_n_tor_error_upper(:,:) => null()
  real(DP),pointer  :: power_density_thermal_n_tor_error_lower(:,:) => null()
  integer :: power_density_thermal_n_tor_error_index=-999999999

  real(DP),pointer  :: power_density_fast(:) => null()     ! /power_density_fast - Flux surface averaged absorbed wave power density on the fast species
  real(DP),pointer  :: power_density_fast_error_upper(:) => null()
  real(DP),pointer  :: power_density_fast_error_lower(:) => null()
  integer :: power_density_fast_error_index=-999999999

  real(DP),pointer  :: power_density_fast_n_tor(:,:) => null()     ! /power_density_fast_n_tor - Flux surface averaged absorbed wave power density on the fast species, per toroidal mode number
  real(DP),pointer  :: power_density_fast_n_tor_error_upper(:,:) => null()
  real(DP),pointer  :: power_density_fast_n_tor_error_lower(:,:) => null()
  integer :: power_density_fast_n_tor_error_index=-999999999

  real(DP),pointer  :: power_inside_thermal(:) => null()     ! /power_inside_thermal - Absorbed wave power on thermal species inside a flux surface (cumulative volume integral of the abso
  real(DP),pointer  :: power_inside_thermal_error_upper(:) => null()
  real(DP),pointer  :: power_inside_thermal_error_lower(:) => null()
  integer :: power_inside_thermal_error_index=-999999999

  real(DP),pointer  :: power_inside_thermal_n_tor(:,:) => null()     ! /power_inside_thermal_n_tor - Absorbed wave power on thermal species inside a flux surface (cumulative volume integral of the abso
  real(DP),pointer  :: power_inside_thermal_n_tor_error_upper(:,:) => null()
  real(DP),pointer  :: power_inside_thermal_n_tor_error_lower(:,:) => null()
  integer :: power_inside_thermal_n_tor_error_index=-999999999

  real(DP),pointer  :: power_inside_fast(:) => null()     ! /power_inside_fast - Absorbed wave power on thermal species inside a flux surface (cumulative volume integral of the abso
  real(DP),pointer  :: power_inside_fast_error_upper(:) => null()
  real(DP),pointer  :: power_inside_fast_error_lower(:) => null()
  integer :: power_inside_fast_error_index=-999999999

  real(DP),pointer  :: power_inside_fast_n_tor(:,:) => null()     ! /power_inside_fast_n_tor - Absorbed wave power on thermal species inside a flux surface (cumulative volume integral of the abso
  real(DP),pointer  :: power_inside_fast_n_tor_error_upper(:,:) => null()
  real(DP),pointer  :: power_inside_fast_n_tor_error_lower(:,:) => null()
  integer :: power_inside_fast_n_tor_error_index=-999999999

endtype

type ids_waves_coherent_wave_profiles_1d_ion  !    Radial profiles (RF waves) related to a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(DP)  :: z_ion=-9.0D40       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed).
  real(DP)  :: z_ion_error_upper=-9.0D40
  real(DP)  :: z_ion_error_lower=-9.0D40
  integer :: z_ion_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H+, D+, T+, He+2, C+, D2, DT, CD4, ...)
  real(DP),pointer  :: power_density_thermal(:) => null()     ! /power_density_thermal - Flux surface averaged absorbed wave power density on the thermal species
  real(DP),pointer  :: power_density_thermal_error_upper(:) => null()
  real(DP),pointer  :: power_density_thermal_error_lower(:) => null()
  integer :: power_density_thermal_error_index=-999999999

  real(DP),pointer  :: power_density_thermal_n_tor(:,:) => null()     ! /power_density_thermal_n_tor - Flux surface averaged absorbed wave power density on the thermal species, per toroidal mode number
  real(DP),pointer  :: power_density_thermal_n_tor_error_upper(:,:) => null()
  real(DP),pointer  :: power_density_thermal_n_tor_error_lower(:,:) => null()
  integer :: power_density_thermal_n_tor_error_index=-999999999

  real(DP),pointer  :: power_density_fast(:) => null()     ! /power_density_fast - Flux surface averaged absorbed wave power density on the fast species
  real(DP),pointer  :: power_density_fast_error_upper(:) => null()
  real(DP),pointer  :: power_density_fast_error_lower(:) => null()
  integer :: power_density_fast_error_index=-999999999

  real(DP),pointer  :: power_density_fast_n_tor(:,:) => null()     ! /power_density_fast_n_tor - Flux surface averaged absorbed wave power density on the fast species, per toroidal mode number
  real(DP),pointer  :: power_density_fast_n_tor_error_upper(:,:) => null()
  real(DP),pointer  :: power_density_fast_n_tor_error_lower(:,:) => null()
  integer :: power_density_fast_n_tor_error_index=-999999999

  real(DP),pointer  :: power_inside_thermal(:) => null()     ! /power_inside_thermal - Absorbed wave power on thermal species inside a flux surface (cumulative volume integral of the abso
  real(DP),pointer  :: power_inside_thermal_error_upper(:) => null()
  real(DP),pointer  :: power_inside_thermal_error_lower(:) => null()
  integer :: power_inside_thermal_error_index=-999999999

  real(DP),pointer  :: power_inside_thermal_n_tor(:,:) => null()     ! /power_inside_thermal_n_tor - Absorbed wave power on thermal species inside a flux surface (cumulative volume integral of the abso
  real(DP),pointer  :: power_inside_thermal_n_tor_error_upper(:,:) => null()
  real(DP),pointer  :: power_inside_thermal_n_tor_error_lower(:,:) => null()
  integer :: power_inside_thermal_n_tor_error_index=-999999999

  real(DP),pointer  :: power_inside_fast(:) => null()     ! /power_inside_fast - Absorbed wave power on thermal species inside a flux surface (cumulative volume integral of the abso
  real(DP),pointer  :: power_inside_fast_error_upper(:) => null()
  real(DP),pointer  :: power_inside_fast_error_lower(:) => null()
  integer :: power_inside_fast_error_index=-999999999

  real(DP),pointer  :: power_inside_fast_n_tor(:,:) => null()     ! /power_inside_fast_n_tor - Absorbed wave power on thermal species inside a flux surface (cumulative volume integral of the abso
  real(DP),pointer  :: power_inside_fast_n_tor_error_upper(:,:) => null()
  real(DP),pointer  :: power_inside_fast_n_tor_error_lower(:,:) => null()
  integer :: power_inside_fast_n_tor_error_index=-999999999

  integer  :: multiple_states_flag=-999999999       ! /multiple_states_flag - Multiple state calculation flag : 0-Only one state is considered; 1-Multiple states are considered a
  type (ids_waves_coherent_wave_profiles_1d_ion_state),pointer :: state(:) => null()  ! /state(i) - Collisional exchange with the various states of the ion species (ionisation, energy, excitation, ...
endtype

type ids_waves_coherent_wave_profiles_1d_electrons  !    Radial profiles (RF waves) related to electrons
  real(DP),pointer  :: power_density_thermal(:) => null()     ! /power_density_thermal - Flux surface averaged absorbed wave power density on the thermal species
  real(DP),pointer  :: power_density_thermal_error_upper(:) => null()
  real(DP),pointer  :: power_density_thermal_error_lower(:) => null()
  integer :: power_density_thermal_error_index=-999999999

  real(DP),pointer  :: power_density_thermal_n_tor(:,:) => null()     ! /power_density_thermal_n_tor - Flux surface averaged absorbed wave power density on the thermal species, per toroidal mode number
  real(DP),pointer  :: power_density_thermal_n_tor_error_upper(:,:) => null()
  real(DP),pointer  :: power_density_thermal_n_tor_error_lower(:,:) => null()
  integer :: power_density_thermal_n_tor_error_index=-999999999

  real(DP),pointer  :: power_density_fast(:) => null()     ! /power_density_fast - Flux surface averaged absorbed wave power density on the fast species
  real(DP),pointer  :: power_density_fast_error_upper(:) => null()
  real(DP),pointer  :: power_density_fast_error_lower(:) => null()
  integer :: power_density_fast_error_index=-999999999

  real(DP),pointer  :: power_density_fast_n_tor(:,:) => null()     ! /power_density_fast_n_tor - Flux surface averaged absorbed wave power density on the fast species, per toroidal mode number
  real(DP),pointer  :: power_density_fast_n_tor_error_upper(:,:) => null()
  real(DP),pointer  :: power_density_fast_n_tor_error_lower(:,:) => null()
  integer :: power_density_fast_n_tor_error_index=-999999999

  real(DP),pointer  :: power_inside_thermal(:) => null()     ! /power_inside_thermal - Absorbed wave power on thermal species inside a flux surface (cumulative volume integral of the abso
  real(DP),pointer  :: power_inside_thermal_error_upper(:) => null()
  real(DP),pointer  :: power_inside_thermal_error_lower(:) => null()
  integer :: power_inside_thermal_error_index=-999999999

  real(DP),pointer  :: power_inside_thermal_n_tor(:,:) => null()     ! /power_inside_thermal_n_tor - Absorbed wave power on thermal species inside a flux surface (cumulative volume integral of the abso
  real(DP),pointer  :: power_inside_thermal_n_tor_error_upper(:,:) => null()
  real(DP),pointer  :: power_inside_thermal_n_tor_error_lower(:,:) => null()
  integer :: power_inside_thermal_n_tor_error_index=-999999999

  real(DP),pointer  :: power_inside_fast(:) => null()     ! /power_inside_fast - Absorbed wave power on thermal species inside a flux surface (cumulative volume integral of the abso
  real(DP),pointer  :: power_inside_fast_error_upper(:) => null()
  real(DP),pointer  :: power_inside_fast_error_lower(:) => null()
  integer :: power_inside_fast_error_index=-999999999

  real(DP),pointer  :: power_inside_fast_n_tor(:,:) => null()     ! /power_inside_fast_n_tor - Absorbed wave power on thermal species inside a flux surface (cumulative volume integral of the abso
  real(DP),pointer  :: power_inside_fast_n_tor_error_upper(:,:) => null()
  real(DP),pointer  :: power_inside_fast_n_tor_error_lower(:,:) => null()
  integer :: power_inside_fast_n_tor_error_index=-999999999

endtype

type ids_waves_coherent_wave_profiles_1d_e_field  !    Components of the surface averaged electric field
  real(DP),pointer  :: plus(:,:) => null()     ! /plus - Left hand polarised electric field component, for every flux surface and every toroidal number
  real(DP),pointer  :: plus_error_upper(:,:) => null()
  real(DP),pointer  :: plus_error_lower(:,:) => null()
  integer :: plus_error_index=-999999999

  real(DP),pointer  :: minus(:,:) => null()     ! /minus - Right hand polarised electric field component, for every flux surface and every toroidal number
  real(DP),pointer  :: minus_error_upper(:,:) => null()
  real(DP),pointer  :: minus_error_lower(:,:) => null()
  integer :: minus_error_index=-999999999

  real(DP),pointer  :: parallel(:,:) => null()     ! /parallel - Parallel electric field component, for every flux surface and every toroidal number
  real(DP),pointer  :: parallel_error_upper(:,:) => null()
  real(DP),pointer  :: parallel_error_lower(:,:) => null()
  integer :: parallel_error_index=-999999999

endtype

type ids_waves_coherent_wave_profiles_1d  !    Radial profiles (RF waves) for a given time slice
  type (ids_core_radial_grid) :: grid  ! /grid - Radial grid
  integer,pointer  :: n_tor(:) => null()      ! /n_tor - Toroidal mode numbers
  real(DP),pointer  :: power_density(:) => null()     ! /power_density - Flux surface averaged total absorbed wave power density (electrons + ion + fast populations)
  real(DP),pointer  :: power_density_error_upper(:) => null()
  real(DP),pointer  :: power_density_error_lower(:) => null()
  integer :: power_density_error_index=-999999999

  real(DP),pointer  :: power_density_n_tor(:,:) => null()     ! /power_density_n_tor - Flux surface averaged absorbed wave power density per toroidal mode number
  real(DP),pointer  :: power_density_n_tor_error_upper(:,:) => null()
  real(DP),pointer  :: power_density_n_tor_error_lower(:,:) => null()
  integer :: power_density_n_tor_error_index=-999999999

  real(DP),pointer  :: power_inside(:) => null()     ! /power_inside - Total absorbed wave power (electrons + ion + fast populations) inside a flux surface (cumulative vol
  real(DP),pointer  :: power_inside_error_upper(:) => null()
  real(DP),pointer  :: power_inside_error_lower(:) => null()
  integer :: power_inside_error_index=-999999999

  real(DP),pointer  :: power_inside_n_tor(:,:) => null()     ! /power_inside_n_tor - Total absorbed wave power (electrons + ion + fast populations) inside a flux surface (cumulative vol
  real(DP),pointer  :: power_inside_n_tor_error_upper(:,:) => null()
  real(DP),pointer  :: power_inside_n_tor_error_lower(:,:) => null()
  integer :: power_inside_n_tor_error_index=-999999999

  real(DP),pointer  :: current_tor_inside(:) => null()     ! /current_tor_inside - Wave driven toroidal current from a stand alone calculation (not consistent with other sources), ins
  real(DP),pointer  :: current_tor_inside_error_upper(:) => null()
  real(DP),pointer  :: current_tor_inside_error_lower(:) => null()
  integer :: current_tor_inside_error_index=-999999999

  real(DP),pointer  :: current_tor_inside_n_tor(:,:) => null()     ! /current_tor_inside_n_tor - Wave driven toroidal current from a stand alone calculation (not consistent with other sources), ins
  real(DP),pointer  :: current_tor_inside_n_tor_error_upper(:,:) => null()
  real(DP),pointer  :: current_tor_inside_n_tor_error_lower(:,:) => null()
  integer :: current_tor_inside_n_tor_error_index=-999999999

  real(DP),pointer  :: current_parallel_density(:) => null()     ! /current_parallel_density - Flux surface averaged wave driven parallel current density = average(j.B) / B0, where B0 = vacuum_to
  real(DP),pointer  :: current_parallel_density_error_upper(:) => null()
  real(DP),pointer  :: current_parallel_density_error_lower(:) => null()
  integer :: current_parallel_density_error_index=-999999999

  real(DP),pointer  :: current_parallel_density_n_tor(:,:) => null()     ! /current_parallel_density_n_tor - Flux surface averaged wave driven parallel current density, per toroidal mode number
  real(DP),pointer  :: current_parallel_density_n_tor_error_upper(:,:) => null()
  real(DP),pointer  :: current_parallel_density_n_tor_error_lower(:,:) => null()
  integer :: current_parallel_density_n_tor_error_index=-999999999

  type (ids_waves_coherent_wave_profiles_1d_e_field) :: e_field  ! /e_field - Components of the electric field, averaged over the flux surface, where the averaged is weighted wit
  real(DP),pointer  :: k_perpendicular(:,:) => null()     ! /k_perpendicular - Perpendicular wave vector,  averaged over the flux surface, where the averaged is weighted with the
  real(DP),pointer  :: k_perpendicular_error_upper(:,:) => null()
  real(DP),pointer  :: k_perpendicular_error_lower(:,:) => null()
  integer :: k_perpendicular_error_index=-999999999

  type (ids_waves_coherent_wave_profiles_1d_electrons) :: electrons  ! /electrons - Quantities related to the electrons
  type (ids_waves_coherent_wave_profiles_1d_ion),pointer :: ion(:) => null()  ! /ion(i) - Quantities related to the different ion species
  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_waves_coherent_wave_full_wave_e_field  !    Components of the full wave electric field
  type (ids_generic_grid_scalar),pointer :: plus(:) => null()  ! /plus(i) - Left hand circularly polarised component of the perpendicular (to the static magnetic field) electri
  type (ids_generic_grid_scalar),pointer :: minus(:) => null()  ! /minus(i) - Right hand circularly polarised component of the perpendicular (to the static magnetic field) electr
  type (ids_generic_grid_scalar),pointer :: parallel(:) => null()  ! /parallel(i) - Parallel (to the static magnetic field) component of electric field, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: normal(:) => null()  ! /normal(i) - Magnitude of wave electric field normal to a flux surface, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: bi_normal(:) => null()  ! /bi_normal(i) - Magnitude of perpendicular (to the static magnetic field) wave electric field tangent to a flux surf
endtype

type ids_waves_coherent_wave_full_wave_b_field  !    Components of the full wave magnetic field
  type (ids_generic_grid_scalar),pointer :: parallel(:) => null()  ! /parallel(i) - Parallel (to the static magnetic field) component of the wave magnetic field, given on various grid
  type (ids_generic_grid_scalar),pointer :: normal(:) => null()  ! /normal(i) - Magnitude of wave magnetic field normal to a flux surface, given on various grid subsets
  type (ids_generic_grid_scalar),pointer :: bi_normal(:) => null()  ! /bi_normal(i) - Magnitude of perpendicular (to the static magnetic field) wave magnetic field tangent to a flux surf
endtype

type ids_waves_coherent_wave_full_wave  !    Full wave solution for a given time slice
  type (ids_generic_grid_dynamic) :: grid  ! /grid - Grid description
  type (ids_waves_coherent_wave_full_wave_e_field) :: e_field  ! /e_field - Components of the wave electric field
  type (ids_waves_coherent_wave_full_wave_b_field) :: b_field  ! /b_field - Components of the wave magnetic field
  type (ids_generic_grid_scalar),pointer :: k_perpendicular(:) => null()  ! /k_perpendicular(i) - Perpendicular wave vector, given on various grid subsets
  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_waves_coherent_wave_profiles_2d_grid  !    2D grid for waves
  type (ids_identifier_dynamic_aos3) :: type  ! /type - Grid type: index=0: Rectangular grid in the (R,Z) coordinates; index=1: Rectangular grid in the (rad
  real(DP),pointer  :: r(:,:) => null()     ! /r - Major radius
  real(DP),pointer  :: r_error_upper(:,:) => null()
  real(DP),pointer  :: r_error_lower(:,:) => null()
  integer :: r_error_index=-999999999

  real(DP),pointer  :: z(:,:) => null()     ! /z - Height
  real(DP),pointer  :: z_error_upper(:,:) => null()
  real(DP),pointer  :: z_error_lower(:,:) => null()
  integer :: z_error_index=-999999999

  real(DP),pointer  :: theta_straight(:,:) => null()     ! /theta_straight - Straight field line poloidal angle
  real(DP),pointer  :: theta_straight_error_upper(:,:) => null()
  real(DP),pointer  :: theta_straight_error_lower(:,:) => null()
  integer :: theta_straight_error_index=-999999999

  real(DP),pointer  :: theta_geometric(:,:) => null()     ! /theta_geometric - Geometrical poloidal angle
  real(DP),pointer  :: theta_geometric_error_upper(:,:) => null()
  real(DP),pointer  :: theta_geometric_error_lower(:,:) => null()
  integer :: theta_geometric_error_index=-999999999

  real(DP),pointer  :: rho_tor_norm(:,:) => null()     ! /rho_tor_norm - Normalised toroidal flux coordinate. The normalizing value for rho_tor_norm, is the toroidal flux co
  real(DP),pointer  :: rho_tor_norm_error_upper(:,:) => null()
  real(DP),pointer  :: rho_tor_norm_error_lower(:,:) => null()
  integer :: rho_tor_norm_error_index=-999999999

  real(DP),pointer  :: rho_tor(:,:) => null()     ! /rho_tor - Toroidal flux coordinate. The toroidal field used in its definition is indicated under vacuum_toroid
  real(DP),pointer  :: rho_tor_error_upper(:,:) => null()
  real(DP),pointer  :: rho_tor_error_lower(:,:) => null()
  integer :: rho_tor_error_index=-999999999

  real(DP),pointer  :: psi(:,:) => null()     ! /psi - Poloidal magnetic flux
  real(DP),pointer  :: psi_error_upper(:,:) => null()
  real(DP),pointer  :: psi_error_lower(:,:) => null()
  integer :: psi_error_index=-999999999

  real(DP),pointer  :: volume(:,:) => null()     ! /volume - Volume enclosed inside the magnetic surface
  real(DP),pointer  :: volume_error_upper(:,:) => null()
  real(DP),pointer  :: volume_error_lower(:,:) => null()
  integer :: volume_error_index=-999999999

  real(DP),pointer  :: area(:,:) => null()     ! /area - Cross-sectional area of the flux surface
  real(DP),pointer  :: area_error_upper(:,:) => null()
  real(DP),pointer  :: area_error_lower(:,:) => null()
  integer :: area_error_index=-999999999

endtype

type ids_waves_coherent_wave_profiles_2d_ion_state  !    Global quantities related to a given ion species state
  real(DP)  :: z_min=-9.0D40       ! /z_min - Minimum Z of the charge state bundle (z_min = z_max = 0 for a neutral)
  real(DP)  :: z_min_error_upper=-9.0D40
  real(DP)  :: z_min_error_lower=-9.0D40
  integer :: z_min_error_index=-999999999

  real(DP)  :: z_max=-9.0D40       ! /z_max - Maximum Z of the charge state bundle (equal to z_min if no bundle)
  real(DP)  :: z_max_error_upper=-9.0D40
  real(DP)  :: z_max_error_lower=-9.0D40
  integer :: z_max_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying charge state (e.g. C+, C+2 , C+3, C+4, C+5, C+6, ...)
  character(len=132), dimension(:), pointer ::electron_configuration => null()       ! /electron_configuration - Configuration of atomic orbitals of this state, e.g. 1s2-2s1
  real(DP)  :: vibrational_level=-9.0D40       ! /vibrational_level - Vibrational level (can be bundled)
  real(DP)  :: vibrational_level_error_upper=-9.0D40
  real(DP)  :: vibrational_level_error_lower=-9.0D40
  integer :: vibrational_level_error_index=-999999999

  character(len=132), dimension(:), pointer ::vibrational_mode => null()       ! /vibrational_mode - Vibrational mode of this state, e.g. "A_g". Need to define, or adopt a standard nomenclature.
  real(DP),pointer  :: power_density_thermal(:,:) => null()     ! /power_density_thermal - Absorbed wave power density on the thermal species
  real(DP),pointer  :: power_density_thermal_error_upper(:,:) => null()
  real(DP),pointer  :: power_density_thermal_error_lower(:,:) => null()
  integer :: power_density_thermal_error_index=-999999999

  real(DP),pointer  :: power_density_thermal_n_tor(:,:,:) => null()     ! /power_density_thermal_n_tor - Absorbed wave power density on the thermal species, per toroidal mode number
  real(DP),pointer  :: power_density_thermal_n_tor_error_upper(:,:,:) => null()
  real(DP),pointer  :: power_density_thermal_n_tor_error_lower(:,:,:) => null()
  integer :: power_density_thermal_n_tor_error_index=-999999999

  real(DP),pointer  :: power_density_fast(:,:) => null()     ! /power_density_fast - Absorbed wave power density on the fast species
  real(DP),pointer  :: power_density_fast_error_upper(:,:) => null()
  real(DP),pointer  :: power_density_fast_error_lower(:,:) => null()
  integer :: power_density_fast_error_index=-999999999

  real(DP),pointer  :: power_density_fast_n_tor(:,:,:) => null()     ! /power_density_fast_n_tor - Absorbed wave power density on the fast species, per toroidal mode number
  real(DP),pointer  :: power_density_fast_n_tor_error_upper(:,:,:) => null()
  real(DP),pointer  :: power_density_fast_n_tor_error_lower(:,:,:) => null()
  integer :: power_density_fast_n_tor_error_index=-999999999

endtype

type ids_waves_coherent_wave_profiles_2d_ion  !    Global quantities related to a given ion species
  type (ids_plasma_composition_neutral_element),pointer :: element(:) => null()  ! /element(i) - List of elements forming the atom or molecule
  real(DP)  :: z_ion=-9.0D40       ! /z_ion - Ion charge (of the dominant ionisation state; lumped ions are allowed).
  real(DP)  :: z_ion_error_upper=-9.0D40
  real(DP)  :: z_ion_error_lower=-9.0D40
  integer :: z_ion_error_index=-999999999

  character(len=132), dimension(:), pointer ::label => null()       ! /label - String identifying the species (e.g. H+, D+, T+, He+2, C+, D2, DT, CD4, ...)
  real(DP),pointer  :: power_density_thermal(:,:) => null()     ! /power_density_thermal - Absorbed wave power density on the thermal species
  real(DP),pointer  :: power_density_thermal_error_upper(:,:) => null()
  real(DP),pointer  :: power_density_thermal_error_lower(:,:) => null()
  integer :: power_density_thermal_error_index=-999999999

  real(DP),pointer  :: power_density_thermal_n_tor(:,:,:) => null()     ! /power_density_thermal_n_tor - Absorbed wave power density on the thermal species, per toroidal mode number
  real(DP),pointer  :: power_density_thermal_n_tor_error_upper(:,:,:) => null()
  real(DP),pointer  :: power_density_thermal_n_tor_error_lower(:,:,:) => null()
  integer :: power_density_thermal_n_tor_error_index=-999999999

  real(DP),pointer  :: power_density_fast(:,:) => null()     ! /power_density_fast - Absorbed wave power density on the fast species
  real(DP),pointer  :: power_density_fast_error_upper(:,:) => null()
  real(DP),pointer  :: power_density_fast_error_lower(:,:) => null()
  integer :: power_density_fast_error_index=-999999999

  real(DP),pointer  :: power_density_fast_n_tor(:,:,:) => null()     ! /power_density_fast_n_tor - Absorbed wave power density on the fast species, per toroidal mode number
  real(DP),pointer  :: power_density_fast_n_tor_error_upper(:,:,:) => null()
  real(DP),pointer  :: power_density_fast_n_tor_error_lower(:,:,:) => null()
  integer :: power_density_fast_n_tor_error_index=-999999999

  integer  :: multiple_states_flag=-999999999       ! /multiple_states_flag - Multiple state calculation flag : 0-Only one state is considered; 1-Multiple states are considered a
  type (ids_waves_coherent_wave_profiles_2d_ion_state),pointer :: state(:) => null()  ! /state(i) - Collisional exchange with the various states of the ion species (ionisation, energy, excitation, ...
endtype

type ids_waves_coherent_wave_profiles_2d_electrons  !    Global quantities related to electrons
  real(DP),pointer  :: power_density_thermal(:,:) => null()     ! /power_density_thermal - Absorbed wave power density on the thermal species
  real(DP),pointer  :: power_density_thermal_error_upper(:,:) => null()
  real(DP),pointer  :: power_density_thermal_error_lower(:,:) => null()
  integer :: power_density_thermal_error_index=-999999999

  real(DP),pointer  :: power_density_thermal_n_tor(:,:,:) => null()     ! /power_density_thermal_n_tor - Absorbed wave power density on the thermal species, per toroidal mode number
  real(DP),pointer  :: power_density_thermal_n_tor_error_upper(:,:,:) => null()
  real(DP),pointer  :: power_density_thermal_n_tor_error_lower(:,:,:) => null()
  integer :: power_density_thermal_n_tor_error_index=-999999999

  real(DP),pointer  :: power_density_fast(:,:) => null()     ! /power_density_fast - Absorbed wave power density on the fast species
  real(DP),pointer  :: power_density_fast_error_upper(:,:) => null()
  real(DP),pointer  :: power_density_fast_error_lower(:,:) => null()
  integer :: power_density_fast_error_index=-999999999

  real(DP),pointer  :: power_density_fast_n_tor(:,:,:) => null()     ! /power_density_fast_n_tor - Absorbed wave power density on the fast species, per toroidal mode number
  real(DP),pointer  :: power_density_fast_n_tor_error_upper(:,:,:) => null()
  real(DP),pointer  :: power_density_fast_n_tor_error_lower(:,:,:) => null()
  integer :: power_density_fast_n_tor_error_index=-999999999

endtype

type ids_waves_coherent_wave_profiles_2d  !    2D profiles (RF waves) for a given time slice
  type (ids_waves_coherent_wave_profiles_2d_grid) :: grid  ! /grid - 2D grid in a poloidal cross-section
  integer,pointer  :: n_tor(:) => null()      ! /n_tor - Toroidal mode numbers
  real(DP),pointer  :: power_density(:,:) => null()     ! /power_density - Total absorbed wave power density (electrons + ion + fast populations)
  real(DP),pointer  :: power_density_error_upper(:,:) => null()
  real(DP),pointer  :: power_density_error_lower(:,:) => null()
  integer :: power_density_error_index=-999999999

  real(DP),pointer  :: power_density_n_tor(:,:,:) => null()     ! /power_density_n_tor - Absorbed wave power density per toroidal mode number
  real(DP),pointer  :: power_density_n_tor_error_upper(:,:,:) => null()
  real(DP),pointer  :: power_density_n_tor_error_lower(:,:,:) => null()
  integer :: power_density_n_tor_error_index=-999999999

  type (ids_waves_coherent_wave_profiles_2d_electrons) :: electrons  ! /electrons - Quantities related to the electrons
  type (ids_waves_coherent_wave_profiles_2d_ion),pointer :: ion(:) => null()  ! /ion(i) - Quantities related to the different ion species
  real(DP)  :: time=-9.0D40       ! /time - Time
  real(DP)  :: time_error_upper=-9.0D40
  real(DP)  :: time_error_lower=-9.0D40
  integer :: time_error_index=-999999999

endtype

type ids_waves_coherent_wave  !    Source terms for a given actuator
  type (ids_waves_coherent_wave_identifier) :: identifier  ! /identifier - Identifier of the coherent wave, in terms of the type and name of the antenna driving the wave and a
  type (ids_identifier) :: wave_solver_type  ! /wave_solver_type - Type of wave deposition solver used for this wave. Index = 1 for beam/ray tracing; index = 2 for ful
  type (ids_waves_coherent_wave_global_quantities),pointer :: global_quantities(:) => null()  ! /global_quantities(i) - Global quantities for various time slices
  type (ids_waves_coherent_wave_profiles_1d),pointer :: profiles_1d(:) => null()  ! /profiles_1d(i) - Source radial profiles (flux surface averaged quantities) for various time slices
  type (ids_waves_coherent_wave_profiles_2d),pointer :: profiles_2d(:) => null()  ! /profiles_2d(i) - 2D profiles in poloidal cross-section, for various time slices
  type (ids_waves_coherent_wave_beam_tracing),pointer :: beam_tracing(:) => null()  ! /beam_tracing(i) - Beam tracing calculations, for various time slices
  type (ids_waves_coherent_wave_full_wave),pointer :: full_wave(:) => null()  ! /full_wave(i) - Solution by a full wave code, given on a generic grid description, for various time slices
endtype

type ids_waves  !    RF wave propagation and deposition
  type (ids_ids_properties) :: ids_properties  ! /waves/ids_properties -
  type (ids_waves_coherent_wave),pointer :: coherent_wave(:) => null()  ! /waves/coherent_wave(i) - Wave description for each frequency
  type (ids_b_tor_vacuum_1) :: vacuum_toroidal_field  ! /waves/vacuum_toroidal_field - Characteristics of the vacuum toroidal field (used in rho_tor definition)
  type (ids_rz1d_dynamic_1) :: magnetic_axis  ! /waves/magnetic_axis - Magnetic axis position (used to define a poloidal angle for the 2D profiles)
  type (ids_code) :: code  ! /waves/code -
  real(DP), pointer  :: time(:) => null()  ! time
endtype

end module
