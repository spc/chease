! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
SUBROUTINE WRTBIN
  !        #####################
  !
  !                                        AUTHORS:
  !                                        H. Lutjens,
  !                                        P. Hennequin
  !                                        R. Arslanbekov 10/99
  !                      from WRTPLOT in chease.f
  !**********************************************************************
  !                                                                     *
  ! C3SB02  WRITE DATA NECESSARY FOR PLOT
  !                            ON THE FILE chease.bin (binary format)   *
  !                                                                     *
  !**********************************************************************
  !
  USE globals
  USE interpol
  use interpos_module
  IMPLICIT NONE
  INCLUDE 'COMDAT.inc'
  !
  integer          ::      mp      ! <wrtbin.f90>
  integer          ::      nnbbpp  ! <wrtbin.f90>
  INTEGER          ::     I    ! <wrtplot.f90>
  INTEGER          ::     INR    ! <wrtplot.f90>
  INTEGER          ::     INSUR    ! <wrtplot.f90>
  INTEGER          ::     J289    ! <wrtplot.f90>
  INTEGER          ::     INBCHI    ! <wrtplot.f90>
  INTEGER          ::     J287    ! <wrtplot.f90>
  INTEGER          ::     JNB    ! <wrtplot.f90>
  INTEGER          ::     J288    ! <wrtplot.f90>
  INTEGER          ::     JJ    ! <wrtplot.f90>
  INTEGER          ::     JSCHI    ! <wrtplot.f90>
  INTEGER          ::     I2    ! <wrtplot.f90>
  INTEGER          ::     I1    ! <wrtplot.f90>
  INTEGER          ::     J284    ! <wrtplot.f90>
  ! INTEGER          ::     J283    ! <wrtplot.f90>
  REAL(RKIND)      ::     ZBND2(1)    ! <wrtplot.f90>
  REAL(RKIND)      ::     ZBND1(1)    ! <wrtplot.f90>
  REAL(RKIND)      ::     ZDT    ! <wrtplot.f90>
  REAL(RKIND)      ::     ZZMIN    ! <wrtplot.f90>
  REAL(RKIND)      ::     ZZMAX    ! <wrtplot.f90>
  REAL(RKIND)      ::     ZRMIN    ! <wrtplot.f90>
  REAL(RKIND)      ::     ZRMAX    ! <wrtplot.f90>
  INTEGER          ::     J281    ! <wrtplot.f90>
  REAL(RKIND)      ::     ZBBS    ! <wrtplot.f90>
  REAL(RKIND)      ::     ZCBS2    ! <wrtplot.f90>
  REAL(RKIND)      ::     ZCBS1    ! <wrtplot.f90>
  REAL(RKIND)      ::     ZBSFC    ! <wrtplot.f90>
  REAL(RKIND)      ::     ZBSF    ! <wrtplot.f90>
  REAL(RKIND)      ::     ZLI1    ! <wrtplot.f90>
  REAL(RKIND)      ::     ZBPOL1    ! <wrtplot.f90>
  REAL(RKIND)      ::     ZGMX    ! <wrtplot.f90>
  REAL(RKIND)      ::     ZGMSTA    ! <wrtplot.f90>
  REAL(RKIND)      ::     ZGM    ! <wrtplot.f90>
  REAL(RKIND)      ::     ZIBSNO    ! <wrtplot.f90>
  REAL(RKIND)      ::     ZINORM    ! <wrtplot.f90>
  REAL(RKIND)      ::     ZBXPER    ! <wrtplot.f90>
  REAL(RKIND)      ::     ZBSPER    ! <wrtplot.f90>
  REAL(RKIND)      ::     ZBPERC    ! <wrtplot.f90>
  REAL(RKIND)      ::     ZBAXEPER! <wrtplot.f90>
  REAL(RKIND)      ::     ZMU0    ! <wrtplot.f90>
  REAL(RKIND)      ::     ZANGLE(1)
  !
  INTEGER, dimension(:), allocatable         ::     IBALL    ! <wrtplot.f90>
  INTEGER, dimension(:), allocatable         ::     IMERCR    ! <wrtplot.f90>
  INTEGER, dimension(:), allocatable         ::     IMERCI    ! <wrtplot.f90>
  !
  REAL(RKIND), dimension(:), allocatable     ::     ZABR    ! <wrtplot.f90>
  REAL(RKIND), dimension(:), allocatable     ::     ZPAR    ! <wrtplot.f90>
  REAL(RKIND), dimension(:,:), allocatable   ::     ZRCHI    ! <wrtplot.f90>
  REAL(RKIND), dimension(:,:), allocatable   ::     ZZCHI    ! <wrtplot.f90>
  REAL(RKIND), dimension(:), allocatable     ::     ZRHOS    ! <wrtplot.f90>
  REAL(RKIND), dimension(:), allocatable     ::     ZRSUR    ! <wrtplot.f90>
  REAL(RKIND), dimension(:), allocatable     ::     ZTSUR    ! <wrtplot.f90>
  REAL(RKIND), dimension(:), allocatable     ::     ZSIG1    ! <wrtplot.f90>
  REAL(RKIND), dimension(:), allocatable     ::     ZTET1    ! <wrtplot.f90>
  !
  real(rkind), dimension(:), allocatable     ::      aux
  !
  integer :: state
  !
  !----*----*----*---*----*----*----*----*----*----*----*----*----*----*-
  !  28. WRITE QUANTITIES FOR PLOTS in binary format (IEEE)
  !
  !     Open bin and dat-files for writing
  !
  write(6,*) 'Creating bin and dat files chease.* ...'
  mp = 101
  !  open(mp,  file ='chease.bin', & ! when "convert" not accepted by compiler
  open(mp,convert = 'big_endian', file ='chease.bin', &
       & FORM = 'unformatted')
  open(mp+1, file ='chease.dat', FORM = 'formatted')
  !
  !  ---------------------------------------------------------------------
  ZMU0 = 1.256_rkind
  ZBPERC = 100._rkind * BETA
  ZBSPER = 100._rkind * BETAS
  ZBXPER = 100._rkind * BETAX
  ZBAXEPER = 2._rkind*100._rkind*cp0*( eqchease_out_add_1d(NISO1EFF1,iirgeo)/T0 )**2
  ZINORM = RINOR/ZMU0
  ZIBSNO = RIBSNOR/ZMU0
  ZGM    = ZBPERC/ZINORM
  ZGMSTA = ZBSPER/ZINORM
  ZGMX   = ZBXPER/ZINORM
  ZBPOL1 = BETAP*CONVF
  ZLI1   = eqchease_out(index_out)%profiles_1d%li(NISO1EFF1)*CONVF
  ZBSF   = RITBS/RITOT
  ZBSFC  = RITBSC/RITOT
  ZCBS1  = 0._RKIND
  ZCBS2  = 0._RKIND
  IF (BETAX .GT. 0._RKIND) ZCBS1 = ZIBSNO*ZINORM/(ZBXPER*SQRT(ASPCT))
  IF (BETAP .GT. 0._RKIND) ZCBS2 = ZBSF/(SQRT(ASPCT)*ZBPOL1)
  ZBBS  = ZBXPER * ZBSF
  !  ---------------------------------------------------------------------
  !   variables de caracterisation du run (pour ecriture texte)
  !  ---------------------------------------------------------------------
  !          'PLASMA SURFACE :'
  !
  call binwrtIscalar(mp,'NSURF',NSURF)
  call binwrtMscalar(mp,'ASPCT',ASPCT)
  call binwrtMscalar(mp,'RC',RC)
  call binwrtMscalar(mp,'ELONG',ELONG)
  call binwrtMscalar(mp,'TRIANG',TRIANG)
  call binwrtMscalar(mp,'DELTA',DELTA)
  call binwrtMscalar(mp,'THETA0',THETA0)
  call binwrtMscalar(mp,'BEANS',BEANS)
  call binwrtMscalar(mp,'CETA',CETA)
  call binwrtMscalar(mp,'SGMA',SGMA)
  call binwrtMscalar(mp,'TRIPLT',TRIPLT)
  call binwrtMscalar(mp,'RNU',RNU)
  call binwrtMscalar(mp,'XI',XI)
  call binwrtMscalar(mp,'AREA',AREA)
  call binwrtMscalar(mp,'REXT',REXT)
  call binwrtMscalar(mp,'TENSBND',TENSBND)
  call binwrtIscalar(mp,'NBPSOUT',NBPSOUT)
  call binwrtIscalar(mp,'NBSEXPQ',NBSEXPQ)
  call binwrtIscalar(mp,'NFIXWALL',NFIXWALL)
  call binwrtIscalar(mp,'NSYM',NSYM)
  !          'EQUILIBRIUM SOLUTION :'
  call binwrtMscalar(mp,'B0EXP',B0EXP)
  call binwrtMscalar(mp,'R0EXP',R0EXP)
  call binwrtMscalar(mp,'SCEXP',SCEXP)
  call binwrtMscalar(mp,'SIGNB0XP',SIGNB0XP)
  call binwrtMscalar(mp,'SIGNIPXP',SIGNIPXP)
  call binwrtMscalar(mp,'EPSLON',EPSLON)
  call binwrtMscalar(mp,'RELAX',RELAX)
  call binwrtMscalar(mp,'RMAG',RMAG)
  call binwrtMscalar(mp,'RZMAG',RZMAG)
  call binwrtMscalar(mp,'PSIMIN',PSI0)
  call binwrtMscalar(mp,'PSISCL',PSISCL)
  call binwrtMscalar(mp,'SNUMBER',SNUMBER)
  call binwrtMscalar(mp,'SLIMIT',SLIMIT)
  call binwrtMscalar(mp,'Q0',Q0)
  call binwrtMscalar(mp,'QSURF',QPSI(NISO1EFF))
  call binwrtMscalar(mp,'QCYL',QCYL)
  call binwrtMscalar(mp,'QMIN',QMIN)
  call binwrtMscalar(mp,'CSQMIN',CSQMIN)
  call binwrtMscalar(mp,'Q95',Q95)
  call binwrtMscalar(mp,'T0',T0)
  call binwrtMscalar(mp,'TSURF',TMF(NISO1EFF))
  call binwrtMscalar(mp,'RITOT',RITOT)
  call binwrtMscalar(mp,'RINOR',RINOR)
  call binwrtMscalar(mp,'ZINORM',ZINORM)
  call binwrtMscalar(mp,'ZBSF',ZBSF)
  call binwrtMscalar(mp,'ZBSFC',ZBSFC)
  call binwrtMscalar(mp,'RZION',RZION)
  call binwrtMscalar(mp,'RPEOP',RPEOP)
  call binwrtMscalar(mp,'SCALNE',SCALNE)
  call binwrtMscalar(mp,'ETAEI',ETAEI)
  call binwrtMscalar(mp,'GAMMA',GAMMA)
  call binwrtMscalar(mp,'CBS1',ZCBS1)
  call binwrtMscalar(mp,'CBS2',ZCBS2)
  call binwrtMscalar(mp,'BBS',ZBBS)
  call binwrtMscalar(mp,'CONV_FACT',CONVF)
  call binwrtMscalar(mp,'LI',eqchease_out(index_out)%profiles_1d%li(NISO1EFF1))
  call binwrtMscalar(mp,'LI_GA',ZLI1)
  call binwrtMscalar(mp,'BETA',ZBPERC)
  call binwrtMscalar(mp,'BETAstar',ZBSPER)
  call binwrtMscalar(mp,'BETA_EXP',ZBXPER)
  call binwrtMscalar(mp,'BETA_AXIS',ZBAXEPER)
  call binwrtMscalar(mp,'G_in_MA_T_M',ZGM)
  call binwrtMscalar(mp,'Gstar_in_MA_T_M',ZGMSTA)
  call binwrtMscalar(mp,'G_EXP_MA_T_M',ZGMX)
  call binwrtMscalar(mp,'BETA_POL',BETAP)
  call binwrtMscalar(mp,'BETA_POL_GA',ZBPOL1)
  call binwrtMscalar(mp,'CPBAR',CPBAR)
  call binwrtMscalar(mp,'CPPF',CPPF)

  call binwrtIscalar(mp,'NANAL',NANAL)
  call binwrtIscalar(mp,'NDIAGOP',NDIAGOP)
  call binwrtIscalar(mp,'NINMAP',NINMAP)
  call binwrtIscalar(mp,'NINSCA',NINSCA)
  call binwrtIscalar(mp,'NOPT',NOPT)
  call binwrtIscalar(mp,'NSMOOTH',NSMOOTH)
  call binwrtIscalar(mp,'NTCASE',NTCASE)
  call binwrtIscalar(mp,'NTEST',NTEST)
  !            'PROFILES :'
  call binwrtIscalar(mp,'NFUNC',NFUNC)
  call binwrtIscalar(mp,'NSTTP',NSTTP)
  call binwrtIscalar(mp,'NIPR',NIPR)
  call binwrtIscalar(mp,'NSOUR',NSOUR)
  call binwrtIscalar(mp,'NPPFUN',NPPFUN)
  call binwrtIscalar(mp,'NPP',NPP)
  call binwrtMscalar(mp,'PREDGE',PREDGE)
  call binwrtIscalar(mp,'NBSOPT',NBSOPT)
  call binwrtIscalar(mp,'NBSFUN',NBSFUN)
  call binwrtIscalar(mp,'NBSTRP',NBSTRP)
  call binwrtIscalar(mp,'NBLOPT',NBLOPT)
  call binwrtIscalar(mp,'NBLC0',NBLC0)
  call binwrtIscalar(mp,'NTURN',NTURN)
  call binwrtIscalar(mp,'NPPR',NPPR)
  call binwrtIscalar(mp,'NBAL',NBAL)
  call binwrtIscalar(mp,'NEONBQS',NEONBQS)
  call binwrtIscalar(mp,'NFUNRHO',NFUNRHO)
  call binwrtMscalar(mp,'CFBAL',CFBAL)
  call binwrtMscalar(mp,'CPRESS',CPRESS)
  call binwrtMscalar(mp,'CPRESSO',CPRESSO)
  call binwrtMscalar(mp,'CQ0',CQ0)
  call binwrtMscalar(mp,'BSFRAC',BSFRAC)
  call binwrtMscalar(mp,'CFNRESS',CFNRESS)
  call binwrtMscalar(mp,'CFNRESSO',CFNRESSO)
  call binwrtIscalar(mp,'COCOS_IN',COCOS_IN)
  call binwrtIscalar(mp,'COCOS_OUT',COCOS_OUT)
  call binwrtM(mp,'AT',size(AT),1,AT)
  call binwrtM(mp,'AT2',size(AT2),1,AT2)
  call binwrtM(mp,'AT3',size(AT3),1,AT3)
  call binwrtM(mp,'AT4',size(AT4),1,AT4)
  call binwrtM(mp,'AP',size(AP),1,AP)
  call binwrtM(mp,'AP2',size(AP2),1,AP2)
  call binwrtM(mp,'AFBS',size(AFBS),1,AFBS)
  call binwrtM(mp,'AFBS2',size(AFBS2),1,AFBS2)
  call binwrtMscalar(mp,'BENTAXIS',BENTAXIS)
  call binwrtMscalar(mp,'BENTQPROFILE',BENTQPROFILE)
  call binwrtMscalar(mp,'BENTRADIUS',BENTRADIUS)
  call binwrtMscalar(mp,'RODABYROD0',RODABYROD0)
  call binwrtMscalar(mp,'TENSPROF',TENSPROF)
  call binwrtIscalar(mp,'NPOPULATIONS',NPOPULATIONS)
  call binwrtIscalar(mp,'NPROF2D',NPROF2D)
  call binwrtIscalar(mp,'NPROFZ',NPROFZ)
  call binwrtIscalar(mp,'NPROPT',NPROPT)
  call binwrtIscalar(mp,'NPRPSI',NPRPSI)
  call binwrtIscalar(mp,'NRFP',NRFP)
  !                'MESHES :'
  call binwrtMscalar(mp,'R0',R0)
  call binwrtMscalar(mp,'R0W',R0)
  call binwrtMscalar(mp,'RZ0',RZ0)
  call binwrtMscalar(mp,'RZ0W',RZ0W)
  call binwrtMscalar(mp,'RBOXLEN',RBOXLEN)
  call binwrtMscalar(mp,'ZBOXLEN',ZBOXLEN)
  call binwrtMscalar(mp,'RBOXLFT',RBOXLFT)
  call binwrtIscalar(mp,'NS',NS)
  call binwrtIscalar(mp,'NT',NT)
  call binwrtIscalar(mp,'NISO',NISO)
  call binwrtIscalar(mp,'NTNOVA',NTNOVA)
  call binwrtIscalar(mp,'NPSI',NISO1EFF-1)
  call binwrtIscalar(mp,'NCHI',NCHI)
  call binwrtIscalar(mp,'NER',NER)
  call binwrtIscalar(mp,'NEGP',NEGP)
  call binwrtIscalar(mp,'MDT',MDT)
  call binwrtIscalar(mp,'MSMAX',MSMAX)
  call binwrtIscalar(mp,'NRBOX',NRBOX)
  call binwrtIscalar(mp,'NZBOX',NZBOX)
  call binwrtI(mp,'NRBOX_XTOR',size(NRBOX_XTOR),1,NRBOX_XTOR)
  call binwrtI(mp,'NZBOX_XTOR',size(NZBOX_XTOR),1,NZBOX_XTOR)
  call binwrtIscalar(mp,'NRHOMESH',NRHOMESH)
  call binwrtIscalar(mp,'NV',NV)
  call binwrtIscalar(mp,'NVEXP',NVEXP)
  call binwrtIscalar(mp,'SHIFT_P',SHIFT_P)
  !
  call binwrtIscalar(mp,'NEQDSK',NEQDSK)
  ! call binwrtI(mp,'NITMOCC',size(NITMOCC),1,NITMOCC) ! should be added when can add to read as well
  call binwrtIscalar(mp,'NITMOPT',NITMOPT)
  call binwrtI(mp,'NITMRUN',size(NITMRUN),1,NITMRUN)
  call binwrtI(mp,'NITMSHOT',size(NITMSHOT),1,NITMSHOT)
  call binwrtIscalar(mp,'NIDEAL',NIDEAL)
  call binwrtIscalar(mp,'NOUTXTOR',NOUTXTOR)
  call binwrtIscalar(mp,'NPLOT',NPLOT)
  call binwrtIscalar(mp,'NFFTOPT',NFFTOPT)
  call binwrtIscalar(mp,'NEQDXTPO',NEQDXTPO)
  call binwrtIscalar(mp,'NVERBOSE',NVERBOSE)
  !                   'S-PACKING :'
  call binwrtIscalar(mp,'NMESHA',NMESHA)
  call binwrtMscalar(mp,'SOLPDA',SOLPDA)
  call binwrtIscalar(mp,'NPOIDA',NPOIDA)
  call binwrtIscalar(mp,'NPOIDQ',NPOIDQ)
  call binwrtIscalar(mp,'NDIFPS',NDIFPS)
  !              'I*-PACKING :'
  call binwrtIscalar(mp,'NMESHB',NMESHB)
  call binwrtMscalar(mp,'SOLPDB',SOLPDB)
  call binwrtIscalar(mp,'NPOIDB',NPOIDB)
  !                 'SIGMA-PACKING :'
  call binwrtIscalar(mp,'NMESHB',NMESHB)
  call binwrtMscalar(mp,'SOLPDB',SOLPDB)
  call binwrtIscalar(mp,'NPOIDB',NPOIDB)
  !                'THETA-PACKING :'
  call binwrtIscalar(mp,'NMESHC',NMESHC)
  call binwrtMscalar(mp,'SOLPDC',SOLPDC)
  call binwrtIscalar(mp,'NPOIDC',NPOIDC)
  !                 'THETA-PACKING :'
  call binwrtIscalar(mp,'NMESHD',NMESHD)
  call binwrtMscalar(mp,'SOLPDD',SOLPDD)
  call binwrtIscalar(mp,'NPOIDD',NPOIDD)
  call binwrtIscalar(mp,'NDIFT',NDIFT)
  !                 'CHI-PACKING :'
  call binwrtIscalar(mp,'NMESHE',NMESHE)
  call binwrtMscalar(mp,'SOLPDE',SOLPDE)
  call binwrtIscalar(mp,'NPOIDE',NPOIDE)
  !                    'NORMALIZATION :'
  call binwrtIscalar(mp,'NCSCAL',NCSCAL)
  call binwrtIscalar(mp,'NTMF0',NTMF0)
  call binwrtIscalar(mp,'NRSCAL',NRSCAL)
  call binwrtMscalar(mp,'SCALE',SCALE)
  call binwrtMscalar(mp,'CSSPEC',CSSPEC)
  call binwrtMscalar(mp,'QSPEC',QSPEC)
  call binwrtMscalar(mp,'CURRT',CURRT)
  !
  !  COMPUTE MAXIMUM AND MINIMUM OF R AND Z
  !  REFERENCE IS THE MAGNETIC AXIS
  !
  allocate(aux(NTP1),ZRHOS(NTP1),stat=state)
  if (state.ne.0) print*,'Allocation problem in WRTBIN ! stat 1 =',state
  !
  CALL BOUND(NT1,CT,ZRHOS)
  !
  aux(1) = 0._rkind
  DO J281=1,NT
     !
     aux(J281+1) = aux(J281) + .25_rkind * (CT(J281+1) - CT(J281)) * &
          (ZRHOS(J281)**2 + ZRHOS(J281+1)**2)
     !
  END DO
  !
  call binwrtM(mp,'ZOART',NT1,1,aux)
  !
  aux(:)= (/ 0._rkind, (REAL(i,rkind)/real(NT,rkind), i=1,NT) /)
  call binwrtM(mp,'ZABIT',NT1,1,aux)
  !
  aux(:)= (/ (ZRHOS(i) * COS(CT(i)), i=1,NT) /)
  call binwrtM(mp,'ZRTET',NT,1,aux)
  !
  ZRMAX=maxval(aux(1:NT))
  ZRMIN=minval(aux(1:NT))
  call binwrtMscalar(mp,'ZRMAX',ZRMAX)
  call binwrtMscalar(mp,'ZRMIN',ZRMIN)
  !
  aux(:)= (/ (ZRHOS(i) * SIN(CT(i)), i=1,NT) /)
  call binwrtM(mp,'ZZTET',NT,1,aux)
  !
  ZZMAX=maxval(aux(1:NT))
  ZZMIN=minval(aux(1:NT))
  call binwrtMscalar(mp,'ZZMAX',ZZMAX)
  call binwrtMscalar(mp,'ZZMIN',ZZMIN)
  !
  aux(:)= (/ (CT(i)-CT(1), i=1,NT), 2._rkind * CPI /)
  call binwrtM(mp,'ZTET',NT1,1,aux)
  !
  deallocate(aux,ZRHOS)
  !
  !  COMPUTE THE SURFACE
  !
  INSUR  = 6 * NT
  call binwrtIscalar(mp,'INSUR',INSUR)
  !
  allocate(aux(INSUR),ZTSUR(INSUR),ZRSUR(INSUR),stat=state)
  if (state.ne.0) print*,'Allocation problem in WRTBIN ! stat 2 =',state
  !
  BPS( 1) = RMAG
  BPS(12) = RZMAG
  !
  IF (NSURF .NE. 1) BPS(3) = BPS(2) - BPS(1)
  IF (NSURF .EQ. 6) CALL BNDSPL
  !
  ZDT = 2._rkind * CPI / real(6 * NT - 1,rkind)
  !
  ztsur(:) = (/ ((i-1)*ZDT, i=1,INSUR) /)
  call binwrtM(mp,'ZTSUR',INSUR,1,ZTSUR)
  !
  CALL BOUND(6*NT,ZTSUR,ZRSUR)
  ZANGLE(1) = PANGLE
  CALL BOUND(1,ZANGLE,ZBND1)
  ZANGLE(1) = PANGLE + CPI
  CALL BOUND(1,ZANGLE,ZBND2)
  !
  ! OS comment bug with ZTSUR(J283) used without being defined, assumed should be i
  aux(:) = (/ (ZRSUR(i) * COS(ZTSUR(i)), i=1,INSUR) /)
  call binwrtM(mp,'ZRSUR',INSUR,1,aux)
  !
  ! OS comment bug with ZTSUR(J283) used without being defined, assumed should be i
  aux(:) = (/ (ZRSUR(i) * SIN(ZTSUR(i)), i=1,INSUR) /)
  call binwrtM(mp,'ZZSUR',INSUR,1,aux)
  !
  BPS( 1) = R0
  BPS(12) = RZ0
  !
  IF (NSURF .NE. 1) BPS(3) = BPS(2) - BPS(1)
  IF (NSURF .EQ. 6) CALL BNDSPL
  !
  deallocate(aux,ztsur,zrsur)
  !
  !  COMPUTE THE VALUES OF R ON RADIUS USED FOR PROFILE DEFINITION
  !
  INR    = 2 * NISO1EFF + 1
  !
  call binwrtIscalar(mp,'INR',INR)
  call binwrtIscalar(mp,'INS',NISO1EFF1)
  !
  allocate(aux(INR),ZABR(INR),ZPAR(INR),ZSIG1(NPISOEFF),ZTET1(NPISOEFF),stat=state)
  if (state.ne.0) print*,'Allocation problem in WRTBIN ! stat 3 =',state
  !
  CALL RMRAD(NISO1EFF,RC0P,CPSRF,PANGLE,ZPAR(1),ZSIG1,ZTET1,1)
  CALL RMRAD(NISO1EFF,RC0P,CPSRF,PANGLE+CPI,ZPAR(NISO1EFF1),ZSIG1,ZTET1,1)
  !
  call binwrtMscalar(mp,'PANGLE',PANGLE)
  !
  !
  !  R-VECTOR FOR PROFILES (LENGTH = INR)
  ZABR(:) = (/ (-ZPAR(2*NISO1EFF+1-i)*ZBND2(1), i=1,NISO1EFF), 0._rkind, (ZPAR(i)*ZBND1(1), i=1,NISO1EFF) /)
  call binwrtM(mp,'ZABR',INR,1,ZABR)
  !
  !  Q(R)-PROFILE
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%q(i), i=NISO1EFF1,2,-1),  &
              (eqchease_out(index_out)%profiles_1d%q(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOQR',INR,1,aux)
  !
  !  Q'(R)-PROFILE
  aux(:) = (/ (eqchease_out_add_1d(i,iidqdpsi), i=NISO1EFF1,2,-1), &
              (eqchease_out_add_1d(i,iidqdpsi), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZODQR',INR,1,aux)
  !
  !  SHEAR S(R)-PROFILE
  aux(:) = (/ (eqchease_out_add_1d(i,iishear), i=NISO1EFF1,2,-1), &
              (eqchease_out_add_1d(i,iishear), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOSHR',INR,1,aux)
  !
  !  J//=<J.B>/B0(R) PROFILE
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%jparallel(i), i=NISO1EFF1,2,-1), &
              (eqchease_out(index_out)%profiles_1d%jparallel(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOJBR',INR,1,aux)
  !
  !  BOOTSTRAP CURRENT PROFILE's
  aux(:) = (/ (RJBSOS(i,1), i=NISO1EFF,1,-1), 0._rkind, (RJBSOS(i,1), i=1,NISO1EFF) /)
  call binwrtM(mp,'ZOJBSR1',INR,1,aux)
  aux(:) = (/ (RJBSOS(i,2), i=NISO1EFF,1,-1), 0._rkind, (RJBSOS(i,2), i=1,NISO1EFF) /)
  call binwrtM(mp,'ZOJBSR2',INR,1,aux)
  aux(:) = (/ (RJBSOS(i,3), i=NISO1EFF,1,-1), 0._rkind, (RJBSOS(i,3), i=1,NISO1EFF) /)
  call binwrtM(mp,'ZOJBSR3',INR,1,aux)
  aux(:) = (/ (RJBSOS(i,4), i=NISO1EFF,1,-1), 0._rkind, (RJBSOS(i,4), i=1,NISO1EFF) /)
  call binwrtM(mp,'ZOJBSR4',INR,1,aux)
  !
  ! NUSTAR (WITHOUT ZEFF)
  aux(:) = (/ (RNUSTAR(i), i=NISO1EFF,1,-1), 0._rkind, (RNUSTAR(i), i=1,NISO1EFF) /)
  call binwrtM(mp,'RNUSTARR',INR,1,aux)
  !
  !  TRAPPED FRACTION R-PROFILE
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%ftrap(i), i=NISO1EFF1,2,-1), &
              (eqchease_out(index_out)%profiles_1d%ftrap(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOTRR',INR,1,aux)
  !
  !  IDEAL MERCIER COEFFICIENT -DI(S), full formula
  aux(:) = (/ (SMERCI(i), i=NISO1EFF,1,-1), &
              flinear(eqchease_out(index_out)%profiles_1d%rho_vol(2),eqchease_out(index_out)%profiles_1d%rho_vol(3),SMERCI(1),SMERCI(2),0._rkind), &
              (SMERCI(i), i=1,NISO1EFF) /)
  call binwrtM(mp,'ZODIR',INR,1,aux)
  !
  !  IDEAL MERCIER COEFFICIENT -DI(S), L.A. with Elongation
  aux(:) = (/ (rdi(i), i=NISO1EFF,1,-1), rdi(1), (rdi(i), i=1,NISO1EFF) /)
  call binwrtM(mp,'ZODILAR',INR,1,aux)
  !
  !  IDEAL MERCIER COEFFICIENT -DI(S), Shafranov Yourchenko
  aux(:) = (/(rsy(i), i=NISO1EFF,1,-1), rsy(1), (rsy(i), i=1,NISO1EFF) /)
  call binwrtM(mp,'ZORSYR',INR,1,aux)
  !
  !  RESISTIVE MERCIER COEFFICIENT -DR(S)
  aux(:) = (/ (smercr(i), i=NISO1EFF,1,-1), SMERCR(1), (smercr(i), i=1,NISO1EFF) /)
  call binwrtM(mp,'ZODRR',INR,1,aux)
  !
  !  H OF GLASSER-GREENE-JOHNSON R-PROFILE
  aux(:) = (/ (hmercr(i), i=NISO1EFF,1,-1), HMERCR(1), (hmercr(i), i=1,NISO1EFF) /)
  call binwrtM(mp,'ZOHH',INR,1,aux)
  !
  !  P'(R)-PROFILE
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%pprime(i), i=NISO1EFF1,2,-1), &
              (eqchease_out(index_out)%profiles_1d%pprime(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOPPR',INR,1,aux)
  !
  !  P(R)-PROFILE
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%pressure(i), i=NISO1EFF1,2,-1), &
              (eqchease_out(index_out)%profiles_1d%pressure(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOPR',INR,1,aux)
  !
  !  TT'(R)-PROFILE
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%ffprime(i), i=NISO1EFF1,2,-1), &
              (eqchease_out(index_out)%profiles_1d%ffprime(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOTTR',INR,1,aux)
  !
  !  T(R)-PROFILE
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%F_dia(i), i=NISO1EFF1,2,-1), &
              (eqchease_out(index_out)%profiles_1d%F_dia(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOTR',INR,1,aux)
  !
  !  I*(R)-PROFILE
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%jphi(i), i=NISO1EFF1,2,-1), &
              (eqchease_out(index_out)%profiles_1d%jphi(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOIPR',INR,1,aux)
  !
  !  BETA POLOIDAL R-PROFILE
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%beta_pol(i), i=NISO1EFF1,2,-1), &
              (eqchease_out(index_out)%profiles_1d%beta_pol(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOBETR',INR,1,aux)
  !
  !  SQRT(VOLUME OF FLUX TUBE) S-PROFILE
  aux(:) = (/(eqchease_out(index_out)%profiles_1d%rho_vol(i), i=NISO1EFF1,2,-1), &
             (eqchease_out(index_out)%profiles_1d%rho_vol(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOARR',INR,1,aux)
  !
  !  Elongation
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%elongation(i), i=NISO1EFF1,2,-1), &
              (eqchease_out(index_out)%profiles_1d%elongation(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOELONGR',INR,1,aux)
  !
  !  ELLIPTICITY
  aux(:) = (/ (RELL(i), i=NISO1EFF,1,-1), &
               flinear(eqchease_out(index_out)%profiles_1d%rho_vol(2),eqchease_out(index_out)%profiles_1d%rho_vol(3),rell(1),rell(2),0._rkind), &
               (RELL(i), i=1,NISO1EFF) /)
  call binwrtM(mp,'ZELLR',INR,1,aux)
  !
  !  d(ELLIPTICITY)/dr
  aux(:) = (/ (RDEDR(i), i=NISO1EFF,1,-1), &
               flinear(eqchease_out(index_out)%profiles_1d%rho_vol(2),eqchease_out(index_out)%profiles_1d%rho_vol(3),rdedr(1),rdedr(2),0._rkind), &
              (RDEDR(i), i=1,NISO1EFF) /)
  call binwrtM(mp,'ZDEDRR',INR,1,aux)
  !
  !  Upper Triangularity
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%tria_upper(i), i=NISO1EFF1,2,-1), &
              (eqchease_out(index_out)%profiles_1d%tria_upper(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOTRIAUPR',INR,1,aux)
  !
  !  Lower Triangularity
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%tria_lower(i), i=NISO1EFF1,2,-1), &
              (eqchease_out(index_out)%profiles_1d%tria_lower(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOTRIALOWR',INR,1,aux)
  !
  !  Geometric center of flux tube S-PROFILE
  aux(:) = (/(eqchease_out_add_1d(i,iirgeo), i=NISO1EFF1,2,-1), &
             (eqchease_out_add_1d(i,iirgeo), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZORGEOMR',INR,1,aux)
  !
  !  Inverse Aspect Ratio of Flux Tube
  aux(:) = (/ (eqchease_out_add_1d(i,iiamin)/eqchease_out_add_1d(i,iirgeo), i=NISO1EFF1,2,-1), &
              (eqchease_out_add_1d(i,iiamin)/eqchease_out_add_1d(i,iirgeo), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOASPCTR',INR,1,aux)
  !
  !  AREA of flux tube
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%area(i), i=NISO1EFF1,2,-1), &
              (eqchease_out(index_out)%profiles_1d%area(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOAREAR',INR,1,aux)
  !
  !  Volume of flux tube
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%volume(i), i=NISO1EFF1,2,-1), &
              (eqchease_out(index_out)%profiles_1d%volume(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOVOLR',INR,1,aux)
  !
  !  Normalized Volume of flux tube
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%rho_vol(i), i=NISO1EFF1,2,-1), &
              (eqchease_out(index_out)%profiles_1d%rho_vol(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZORHOVOLR',INR,1,aux)
  !
  !  Volume-prime of flux tube
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%vprime(i), i=NISO1EFF1,2,-1), &
              (eqchease_out(index_out)%profiles_1d%vprime(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOVPRIMER',INR,1,aux)
  !
  !  rho_tor of flux tube
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%rho_tor(i), i=NISO1EFF1,2,-1), &
              (eqchease_out(index_out)%profiles_1d%rho_tor(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZORHOTORR',INR,1,aux)
  !
  !  Normalized rho_tor of flux tube
  aux(:) = (/ (CSMTOR(i), i=NISO1EFF,1,-1), 0._rkind, (CSMTOR(i), i=1,NISO1EFF) /)
  call binwrtM(mp,'ZORHOTORNR',INR,1,aux)
  !
  !  rho_tor-prime of flux tube
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%dvdrho(i), i=NISO1EFF1,2,-1), &
              (eqchease_out(index_out)%profiles_1d%dvdrho(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZORHOTORPRIMER',INR,1,aux)
  !
  !  dpsi/drho_tor/Rgeom(a) of flux tube
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%dpsidrho_tor(i)/eqchease_out_add_1d(NISO1EFF1,iirgeo), i=NISO1EFF1,2,-1), &
              (eqchease_out(index_out)%profiles_1d%dpsidrho_tor(i)/eqchease_out_add_1d(NISO1EFF1,iirgeo), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZODPSIRHOTORR',INR,1,aux)
  !
  !  Ip Profile
  aux(:) = (/ (eqchease_out_add_1d(i,iiIplas), i=NISO1EFF1,2,-1), &
              (eqchease_out_add_1d(i,iiIplas), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOIPLR',INR,1,aux)
  !
  !  Li Profile
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%li(i), i=NISO1EFF1,2,-1), &
              (eqchease_out(index_out)%profiles_1d%li(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOLIR',INR,1,aux)
  !
  !  Bmin Profile
  aux(:) = (/ (eqchease_out_add_1d(i,iiBmin), i=NISO1EFF1,2,-1), &
              (eqchease_out_add_1d(i,iiBmin), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOBMINR',INR,1,aux)
  !
  !  Bmax Profile
  aux(:) = (/ (eqchease_out_add_1d(i,iiBmax), i=NISO1EFF1,2,-1), &
              (eqchease_out_add_1d(i,iiBmax), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOBMAXR',INR,1,aux)
  !
  !  Rmin Profile
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%r_inboard(i), i=NISO1EFF1,2,-1), &
              (eqchease_out(index_out)%profiles_1d%r_inboard(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZORMINR',INR,1,aux)
  !
  !  Rmax Profile
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%r_outboard(i), i=NISO1EFF1,2,-1), &
              (eqchease_out(index_out)%profiles_1d%r_outboard(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZORMAXR',INR,1,aux)
  !
  !  POLOIDAL FLUX R-PROFILE
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%psi(i), i=NISO1EFF1,2,-1), &
              (eqchease_out(index_out)%profiles_1d%psi(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOFR',INR,1,aux)
  !
  !  TOROIDAL CURRENT DENSITY R-PROFILE
  !
  aux(NISO1EFF1)   = - (RMAG * DPDP0 + DTTP0 / RMAG)

  DO J284=1,NISO1EFF
     !
     I1 = NISO1EFF + J284 + 1
     I2 = NISO1EFF - J284 + 1
     !
     aux(I1)  = - (ZABR(I1) + RMAG) * CPPR(J284) - &
          TTP(J284) / (ZABR(I1) + RMAG)
     aux(I2)  = - (ZABR(I2) + RMAG) * CPPR(J284) - &
          TTP(J284) / (ZABR(I2) + RMAG)
     !
  END DO
  call binwrtM(mp,'ZOJR',INR,1,aux)
  !
  deallocate(aux,ZABR,ZPAR,ZSIG1,ZTET1)
  !
  !  SET S-VALUES IN ZABS
  !
  !
  allocate(aux(NISO1EFF1),stat=state)
  if (state.ne.0) print*,'Allocation problem in WRTBIN ! stat 4 =',state
  !
  !
  !  S-VECTOR FOR PROFILES (LENGTH = NISO1EFF1)
  aux(:) = (/0._rkind, (SMISO(i), i=1,NISO1EFF) /)
  call binwrtM(mp,'ZABSM',NISO1EFF1,1,aux)
  !
  !  Q(S)-PROFILE
  aux(:) = (/(eqchease_out(index_out)%profiles_1d%q(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOQS',NISO1EFF1,1,aux)
  !
  !  Q'(S)-PROFILE
  aux(:) = (/ (eqchease_out_add_1d(i,iidqdpsi), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZODQS',NISO1EFF1,1,aux)
  !
  !  SHEAR S(S)-PROFILE
  aux(:) = (/ (eqchease_out_add_1d(i,iishear), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOSHS',NISO1EFF1,1,aux)
  !
  !  J//=<J.B>/B0 S-PROFILE
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%jparallel(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOJBS',NISO1EFF1,1,aux)
  !
  !  BOOTSTRAP CURRENT <JBOOT.B> S-PROFILE's
  aux(:) = (/ 0._rkind, (RJBSOS(i,1), i=1,NISO1EFF) /)
  call binwrtM(mp,'ZOJBSS1',NISO1EFF1,1,aux)
  aux(:) = (/ 0._rkind, (RJBSOS(i,2), i=1,NISO1EFF) /)
  call binwrtM(mp,'ZOJBSS2',NISO1EFF1,1,aux)
  aux(:) = (/ 0._rkind, (RJBSOS(i,3), i=1,NISO1EFF) /)
  call binwrtM(mp,'ZOJBSS3',NISO1EFF1,1,aux)
  aux(:) = (/ 0._rkind, (RJBSOS(i,4), i=1,NISO1EFF) /)
  call binwrtM(mp,'ZOJBSS4',NISO1EFF1,1,aux)
  !
  ! NUSTAR (WITHOUT ZEFF)
  aux(:) = (/ 0._rkind, (RNUSTAR(i), i=1,NISO1EFF) /)
  call binwrtM(mp,'RNUSTARS',NISO1EFF1,1,aux)
  !
  ! TRAPPED FRACTION S-PROFILE
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%ftrap(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOTRS',NISO1EFF1,1,aux)
  !
  !  IDEAL MERCIER COEFFICIENT -DI(S), full formula
  aux(:) = (/ flinear(eqchease_out(index_out)%profiles_1d%rho_vol(2),eqchease_out(index_out)%profiles_1d%rho_vol(3),SMERCI(1),SMERCI(2),0._rkind), &
             (SMERCI(i), i=1,NISO1EFF) /)
  call binwrtM(mp,'ZODIS',NISO1EFF1,1,aux)
  !
  !  IDEAL MERCIER COEFFICIENT -DI(S), L.A. with Elongation
  aux(:) = (/rdi(1), (rdi(i), i=1,NISO1EFF) /)
  call binwrtM(mp,'ZODILAS',NISO1EFF1,1,aux)
  !
  !  IDEAL MERCIER COEFFICIENT -DI(S), Shafranov Yourchenko
  aux(:) = (/rsy(1), (rsy(i), i=1,NISO1EFF) /)
  call binwrtM(mp,'ZORSYS',NISO1EFF1,1,aux)
  !
  !  RESISTIVE MERCIER COEFFICIENT -DR(S)
  aux(:) = (/SMERCR(1), (smercr(i), i=1,NISO1EFF) /)
  call binwrtM(mp,'ZODRS',NISO1EFF1,1,aux)
  !
  !  H OF GLASSER-GREENE-JOHNSON S-PROFILE
  aux(:) = (/HMERCR(1), (hmercr(i), i=1,NISO1EFF) /)
  call binwrtM(mp,'ZOHS',NISO1EFF1,1,aux)
  !
  !  P'(S)-PROFILE
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%pprime(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOPPS',NISO1EFF1,1,aux)
  !
  !  P(S)-PROFILE
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%pressure(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOPS',NISO1EFF1,1,aux)
  !
  !  TT'(S)-PROFILE
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%ffprime(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOTTS',NISO1EFF1,1,aux)
  !
  !  T(S)-PROFILE
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%F_dia(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOTS',NISO1EFF1,1,aux)
  !  I*(S)-PROFILE
  !
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%jphi(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOIPS',NISO1EFF1,1,aux)
  !
  !  BETA POLOIDAL S-PROFILE
  aux(:) = (/(eqchease_out(index_out)%profiles_1d%beta_pol(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOBETS',NISO1EFF1,1,aux)
  !
  !  SQRT(VOLUME OF FLUX TUBE) S-PROFILE
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%rho_vol(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOARS',NISO1EFF1,1,aux)
  !
  !  POLOIDAL FLUX R-PROFILE
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%psi(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOFS',NISO1EFF1,1,aux)
  !
  !  Elongation
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%elongation(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOELONGS',NISO1EFF1,1,aux)
  !
  !  ELLIPTICITY
  aux(:) = (/ flinear(eqchease_out(index_out)%profiles_1d%rho_vol(2),eqchease_out(index_out)%profiles_1d%rho_vol(3),rell(1),rell(2),0._rkind), &
             (RELL(i), i=1,NISO1EFF) /)
  call binwrtM(mp,'ZELLS',NISO1EFF1,1,aux)
  !
  !  d(ELLIPTICITY)/dr
  aux(:) = (/ flinear(eqchease_out(index_out)%profiles_1d%rho_vol(2),eqchease_out(index_out)%profiles_1d%rho_vol(3),rdedr(1),rdedr(2),0._rkind), &
             (RDEDR(i), i=1,NISO1EFF) /)
  call binwrtM(mp,'ZDEDRS',NISO1EFF1,1,aux)
  !
  !  Lower Triangularity
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%tria_lower(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOTRIALOWS',NISO1EFF1,1,aux)
  !
  !  Upper Triangularity
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%tria_upper(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOTRIAUPS',NISO1EFF1,1,aux)
  !
  !  Geometric center of flux tube
  aux(:) = (/ (eqchease_out_add_1d(i,iirgeo), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZORGEOMS',NISO1EFF1,1,aux)
  !
  !  Inverse Aspect Ratio of Flux Tube
  aux(:) = (/ (eqchease_out_add_1d(i,iiamin)/eqchease_out_add_1d(i,iirgeo), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOASPCTS',NISO1EFF1,1,aux)
  !
  !  Area of flux tube
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%area(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOAREAS',NISO1EFF1,1,aux)
  !
  !  Volume of flux tube
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%volume(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOVOLS',NISO1EFF1,1,aux)
  !
  !  Normalized Volume of flux tube
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%rho_vol(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZORHOVOLS',NISO1EFF1,1,aux)
  !
  !  Volume-prime of flux tube
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%vprime(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOVPRIMES',NISO1EFF1,1,aux)
  !
  !  rho_tor of flux tube
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%rho_tor(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZORHOTORS',NISO1EFF1,1,aux)
  !
  !  Normalized rho_tor of flux tube
  aux(:) = (/ 0._rkind, (CSMTOR(i), i=1,NISO1EFF) /)
  call binwrtM(mp,'ZORHOTORNS',NISO1EFF1,1,aux)
  !
  !  rho_tor-prime of flux tube
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%dvdrho(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZORHOTORPRIMES',NISO1EFF1,1,aux)
  !
  !  dpsi/drho_tor/Rgeom(a) of flux tube
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%dpsidrho_tor(i)/eqchease_out_add_1d(NISO1EFF1,iirgeo), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZODPSIRHOTORS',NISO1EFF1,1,aux)
  !
  !  Ip Profile
  aux(:) = (/ (eqchease_out_add_1d(i,iiIplas), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOIPLS',NISO1EFF1,1,aux)
  !
  !  Li Profile
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%li(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOLIS',NISO1EFF1,1,aux)
  !
  !  Bmin Profile
  aux(:) = (/ (eqchease_out_add_1d(i,iiBmin), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOBMINS',NISO1EFF1,1,aux)
  !
  !  Bmax Profile
  aux(:) = (/ (eqchease_out_add_1d(i,iiBmax), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZOBMAXS',NISO1EFF1,1,aux)
  !
  !  Rmin Profile
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%r_inboard(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZORMINS',NISO1EFF1,1,aux)
  !
  !  Rmax Profile
  aux(:) = (/ (eqchease_out(index_out)%profiles_1d%r_outboard(i), i=1,NISO1EFF1) /)
  call binwrtM(mp,'ZORMAXS',NISO1EFF1,1,aux)
  !
  !  S-VECTOR FOR PROFILES (LENGTH = NISO1EFF)
  aux(:) = (/0._rkind, (smiso(i), i=1,NISO1EFF) /)
  call binwrtM(mp,'ZABS',NISO1EFF1,1,aux)
  !
  deallocate(aux)
  !
  !  CONSTANT CHI LINES
  !
  allocate(ZRCHI(NPCHI,NPISOEFF),ZZCHI(NPCHI,NPISOEFF),stat=state)
  if (state.ne.0) print*,'Allocation problem in WRTBIN ! stat 5 =',state
  !
  !%OS         JSCHI = NCHI / 25 + 1
  JSCHI = 1
  do jj=1,nchi
     write(14,'(a,i5,a,i5,a)') ' jchi = ',jj,' ipsi=1,',niso1eff,':'
     write(14,'(1p2e13.5)') (cr(jj,j288),cz(jj,j288),j288=1,niso1eff)
  end do
  !
  DO J288=1,NISO1EFF
     !
     JNB = 0
     !
     DO J287=1,NCHI,JSCHI
        !
        JNB             = JNB + 1
        ZRCHI(JNB,J288) = CR(J287,J288) - R0
        ZZCHI(JNB,J288) = CZ(J287,J288) - RZ0
        !
     END DO
  END DO
  !
  INBCHI = JNB
  call binwrtIscalar(mp,'INBCHI',INBCHI)
  !  R-MATRIX FOR ISOCHI LINES INTERSECTIONS WITH ISOPSI SURFACES
  call binwrtM(mp,'ZRCHI',INBCHI,NISO1EFF,ZRCHI)
  !  Z-MATRIX FOR ISOCHI LINES INTERSECTIONS WITH ISOPSI SURFACES
  call binwrtM(mp,'ZZCHI',INBCHI,NISO1EFF,ZZCHI)
  !
  deallocate(ZRCHI,ZZCHI)
  !
  !  BALLOONING AND MERCIER
  !
  allocate(IBALL(NPISOEFF),IMERCI(NPISOEFF),IMERCR(NPISOEFF),stat =state)
  if (state.ne.0) print*,'Allocation problem in WRTBIN ! stat 6 =',state
  !
  DO J289=1,NISO1EFF
     !
     IMERCI(J289) = 0
     IMERCR(J289) = 0
     IBALL(J289) = 0
     !
     IF (SMERCI(J289) .LT. RC0P) IMERCI(J289) = 1
     IF (SMERCR(J289) .LT. RC0P) IMERCR(J289) = 1
     IF (NCBAL(J289) .NE. 0)  IBALL(J289) = 1
     !
  END DO
  !
  !  BALLOONING (IN)STABILITY FLAG ON EACH FLUX SURFACE: (1)0
  call binwrtI(mp,'IBALL',NISO1EFF,1,IBALL)
  call binwrtI(mp,'NCBAL',NISO1EFF,1,NCBAL)

  !  IDEAL INTERCHANGE (IN)STABILITY FLAG: (1)0
  call binwrtI(mp,'IMERCI',NISO1EFF,1,IMERCI)

  !  RESISTIVE INTERCHANGE (IN)STABILITY FLAG: (1)0
  call binwrtI(mp,'IMERCR',NISO1EFF,1,IMERCR)
  deallocate(IBALL,IMERCI,IMERCR)
  !
  ! NCURV is evaluated in ERDATA, which is always called when NPLOT=1
  !
  allocate(aux(NCURV),stat=state)
  if (state.ne.0) print*,'Allocation problem in WRTBIN ! stat 7 =',state
  !
  !  R-VECTOR FOR ZERO-CURVATURE CURVE
  aux(:) = (/(RRCURV(i) - R0, i=1,NCURV) /)
  call binwrtM(mp,'ZRCURV',NCURV,1,aux)
  !
  !  Z-VECTOR FOR ZERO-CURVATURE CURVE
  aux(:) = (/(RZCURV(i) - RZ0, i=1,NCURV) /)
  call binwrtM(mp,'ZZCURV',NCURV,1,aux)
  !
  deallocate(aux)
  !
  call binwrtM(mp,'CSIG',NS1,1,CSIG)
  call binwrtM(mp,'SMISO',NISO1EFF,1,SMISO)
  !
  allocate(aux(NPCHI1),stat=state)
  if (state.ne.0) print*,'Allocation problem in WRTBIN ! stat 8 =',state
  !
  aux(:) = (/(CHI(i) - CHI(1), i=1,NCHI1) /)
  call binwrtM(mp,'ZCHI',NCHI1,1,aux)
  !
  !
  aux(:) = (/(REAL(i-1,RKIND) / REAL(NCHI,RKIND), i=1,NCHI1) /)
  call binwrtM(mp,'ZABIC',NCHI1,1,aux)
  !
  deallocate(aux)
  !
  ! NISO1EFF is linked to NPSI and not NISO, so NISO can be larger than NISO1EFF
  allocate(aux(NISO+1),stat=state)
  if (state.ne.0) print*,'Allocation problem in WRTBIN ! stat 9 =',state
  !
  aux(:) = (/0._rkind,(CSIPR(i), i=1,NISO) /)
  call binwrtM(mp,'CSIPR',NISO+1,1,aux)
  !
  aux(:) = (/(REAL(i-1,RKIND) / REAL(NISO,RKIND), i=1,NISO), 1._rkind /)
  call binwrtM(mp,'ZABIPR',NISO+1,1,aux)
  !
  deallocate(aux)
  !
  allocate(aux(NSP1),stat=state)
  if (state.ne.0) print*,'Allocation problem in WRTBIN ! stat 10 =',state
  !
  aux(:) = (/(REAL(i-1,RKIND) / REAL(NS,RKIND), i=1,NS1) /)
  call binwrtM(mp,'ZABISG',NS1,1,aux)
  !
  deallocate(aux)
  !
  call binwrtIscalar(mp,'NMGAUS',NMGAUS)
  call binwrtIscalar(mp,'NSGAUS',NSGAUS)
  call binwrtIscalar(mp,'NTGAUS',NTGAUS)
  !
  ! substract mesh center coordinates to (R,Z) coordinates for plotting
  !
  RRISO(:,1:NISO1EFF)=RRISO(:,1:NISO1EFF) - R0
  RZISO(:,1:NISO1EFF)=RZISO(:,1:NISO1EFF) - RZ0
  CR(:,:)=CR(:,:) - R0
  CZ(:,:)=CZ(:,:) - RZ0
  !
  !  R-MATRIX FOR ISOPSI-SURFACES
  NNBBPP = NMGAUS*NT1
  call binwrtIscalar(mp,'NNBBPP',NNBBPP)
  !
  call binwrtM(mp,'RRISO',NNBBPP,NISO1EFF,RRISO)
  !  Z-MATRIX FOR ISOPSI-SURFACES
  call binwrtM(mp,'RZISO',NNBBPP,NISO1EFF,RZISO)
  !  LOCAL_SHEAR-MATRIX ON (CR,CZ) MESH
  call binwrtM(mp,'RSHEAR',NCHI,NISO1EFF,RSHEAR)
  call binwrtM(mp,'CR',NCHI,NISO1EFF,CR)
  call binwrtM(mp,'CZ',NCHI,NISO1EFF,CZ)
  !
  ! Restore (R,Z) coodinates
  !
  RRISO(:,1:NISO1EFF)=RRISO(:,1:NISO1EFF) + R0
  RZISO(:,1:NISO1EFF)=RZISO(:,1:NISO1EFF) + RZ0
  CR(:,:)=CR(:,:) + R0
  CZ(:,:)=CZ(:,:) + RZ0
  !
  call binwrtM(mp,'APLACE',size(APLACE),1,APLACE)
  call binwrtM(mp,'AWIDTH',size(AWIDTH),1,AWIDTH)
  call binwrtM(mp,'BPLACE',size(BPLACE),1,BPLACE)
  call binwrtM(mp,'BWIDTH',size(BWIDTH),1,BWIDTH)
  call binwrtM(mp,'CPLACE',size(CPLACE),1,CPLACE)
  call binwrtM(mp,'CWIDTH',size(CWIDTH),1,CWIDTH)
  call binwrtM(mp,'DPLACE',size(DPLACE),1,DPLACE)
  call binwrtM(mp,'DWIDTH',size(DWIDTH),1,DWIDTH)
  call binwrtM(mp,'EPLACE',size(EPLACE),1,EPLACE)
  call binwrtM(mp,'EWIDTH',size(EWIDTH),1,EWIDTH)
  call binwrtM(mp,'QPLACE',size(QPLACE),1,QPLACE)
  call binwrtM(mp,'QWIDTH',size(QWIDTH),1,QWIDTH)
  call binwrtM(mp,'QVALNEO',size(QVALNEO),1,QVALNEO)
  !
  close(mp)
  close(mp+1)
  !
  RETURN
  !
END SUBROUTINE WRTBIN
subroutine binwrtIscalar(mf,name,intvalue)
  !
  !                                        AUTHORS:
  !                                        R. Arslanbekov
  ! some compilers cannot match scalar with dim(1)
  implicit none

  integer                 :: mf, n, m
  integer :: intvalue
  character*(*)           :: name
  character(1)            :: info = '*'
  !
  write(mf) real(intvalue, 4)
  n = 1
  m = 1
  write(mf+1,'(A, 2X, A, 1X, I10, 1X, I10)') name, info, n, m
  !
  return
end subroutine binwrtIscalar
subroutine binwrtI(mf,name,n,m,intvalue)
  !
  !                                        AUTHORS:
  !                                        R. Arslanbekov
  implicit none

  integer                 :: mf, n, m
  integer, dimension(n*m) :: intvalue
  character*(*)           :: name
  character(1)            :: info = '*'
  !
  write(mf) real(intvalue, 4)
  write(mf+1,'(A, 2X, A, 1X, I10, 1X, I10)') name, info, n, m
  !
  return
end subroutine binwrtI

subroutine binwrtMscalar(mf,name,realvalue)
  !
  !                                        AUTHORS:
  !                                        R. Arslanbekov
  ! some compilers cannot match scalar with dim(1)
  implicit none

  integer                     :: mf, n, m
  character*(*)               :: name
  character(1)                :: info = '*'
  real(8) :: realvalue
  !
  write(mf) real(realvalue,4)
  n = 1
  m = 1
  write(mf+1,'(A, 2X, A, 1X, I10, 1X, I10)') name, info, n, m
  !
  return
end subroutine binwrtMscalar
subroutine binwrtM(mf,name,n,m,realvalue)
  !
  !                                        AUTHORS:
  !                                        R. Arslanbekov
  implicit none

  integer                     :: mf, n, m
  character*(*)               :: name
  character(1)                :: info = '*'
  real(8), dimension(n*m) :: realvalue
  !
  write(mf) real(realvalue,4)
  write(mf+1,'(A, 2X, A, 1X, I10, 1X, I10)') name, info, n, m
  !
  return
end subroutine binwrtM

subroutine binwrtC(mf,name,charvalue)
  !
  !                                        AUTHORS:
  !                                        R. Arslanbekov
  implicit none

  integer                 :: mf, n = 1, m = 1
  character*(*)           :: name
  character*(*)           :: charvalue
  !
  write(mf) real(0._8,4)
  write(mf+1,'(A, 2X, A, 1X, I10, 1X, I10)') name, charvalue, n, m
  !
  return
end subroutine binwrtC
