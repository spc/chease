! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*DECK U23
!*CALL PROCESS
SUBROUTINE IVAR(KNAME,KVALUE)
  !        #############################
  !**********************************************************************
  !                                                                     *
  ! U.23   PRINT NAME AND VALUE OF INTEGER VARIABLE                     *
  !                                                                     *
  !**********************************************************************
  !
  USE globals
  IMPLICIT NONE
  INTEGER          ::     KVALUE
  CHARACTER*(*) KNAME
  !
  WRITE (6,9900) KNAME, KVALUE
  !
  RETURN
9900 FORMAT(/,1X,A,' =',I12)
END SUBROUTINE IVAR
