! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*DECK C0S01
!*CALL PROCESS
SUBROUTINE MASTER
  !        #################
  !
  !                                        AUTHORS:
  !                                        H. LUTJENS,  CRPP-EPFL
  !                                        A. BONDESON, CRPP-EPFL
  !
  !**********************************************************************
  !                                                                     *
  ! C0S01 CONTROLS THE RUN                                              *
  !                                                                     *
  !**********************************************************************
  !
  USE globals
  IMPLICIT NONE
  REAL(RKIND)      ::     STIME
  COMMON /COMTIM/ STIME
  !
  !----*----*----*----*----*----*----*----*----*----*----*----*----*----*
  !
  CALL RUNTIM
  !
  !**********************************************************************
  !                                                                     *
  ! 1. SET UP THE CASE                                                  *
  !                                                                     *
  !**********************************************************************
  !
  CALL LABRUN
  !TTM         CALL CLEAR
  CALL PRESET
  CALL DATA
  CALL globals_init
  CALL TCASE
  CALL COTROL
  CALL AUXVAL
  !
  !**********************************************************************
  !                                                                     *
  ! 3. STEP ON THE CALCULATION                                          *
  !                                                                     *
  !**********************************************************************
  !
  CALL STEPON
  CALL RUNTIM
  !
  RETURN
END SUBROUTINE MASTER
