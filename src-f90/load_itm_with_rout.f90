! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
subroutine load_itm(equil_in,kitmopt,kitmshot,kitmrun,kitmocc,citmtree)
  !
  use globals
  use euITM_schemas                       ! module containing the equilibrium type definitions
  use euITM_routines
  IMPLICIT NONE
integer, parameter :: DP=kind(1.0D0)
  type(type_equilibrium),pointer      :: equil_in(:)
  character*120  :: citmtree
  integer        :: kitmopt, kitmshot, kitmrun, kitmocc
  integer        :: idx, i, i_interpol, ilast_time_ref_eff
  !
  character*11  :: signal_name ='equilibrium'
  character*16  :: str_equil = 'equilibrium'
  character*5   :: treename
  !
  treename = trim(citmtree)
  !
  if (kitmocc .eq. 0) then
    str_equil = signal_name
  else
    write(str_equil,'(A11,A1,I0)') signal_name,'/',kitmocc
  end if
  print *,'signal_name= ',str_equil
  print *,'treename= ',treename
  print *,'kitmshot,kitmrun,kitmocc= ',kitmshot,kitmrun,kitmocc
  call euitm_open(citmtree,kitmshot,kitmrun,idx)
  print *,'time_ref= ',time_ref
  ! find time_ref effective input (same check as in chease subroutine)
  ilast_time_ref_eff = 0
  if (abs(time_ref(1)+999) .gt. 1.e-4) then
    ! valid input if and only if 1st time_ref not default
    ilast_time_ref_eff = 1
    i = 2 ! do also i=2 since can be special with negative value from delta_t info
    if ((abs(time_ref(i)+999) .gt. 1.e-4) .and. (time_ref(i) .lt. 0._rkind) .and. (time_ref(i+1) .gt. time_ref(i-1))) then
      ! case with 3 relevant inputs including delta_t
      i=3
      ilast_time_ref_eff = 3
    else
      ! standard search for increasing inputs
      do while ((i .le. size(time_ref)) .and. ((abs(time_ref(i)+999) .gt. 1.e-4) .and. (time_ref(i) .gt. time_ref(i-1))))
        ilast_time_ref_eff = i
        i = i + 1
      end do
    end if
  end if
  !
  if (ilast_time_ref_eff .le. 1) then
    ! only one time_ref to consider
    print *,'time_ref(1)= ',time_ref(1)
    allocate(equil_in(1))
    i_interpol = 1 ! closest time
    call euitm_get_slice(idx,trim(str_equil),equil_in(1),time_ref(1),i_interpol)
    print *,'size(equil_in)= ',size(equil_in)
    print *,'time_ref(1)= ',time_ref(1)
    print *,'equil_in(1)%time= ',equil_in(1)%time
  else
    call euitm_get(idx,trim(str_equil),equil_in)
  end if
  call euitm_close(idx)
  !
  if (associated(equil_in(1)%datainfo%comment)) then
     do i=1,size(equil_in(1)%datainfo%comment)
        write(6,'(A)') trim(equil_in(1)%datainfo%comment(i))
     end do
  end if
  !
  return
end subroutine load_itm
