! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!*DECK C2SE01
!*CALL PROCESS
SUBROUTINE SOLVIT
  !        #################
  !
  !                                        AUTHORS:
  !                                        H. LUTJENS,  CRPP-EPFL
  !                                        A. BONDESON, CRPP-EPFL
  !
  !**********************************************************************
  !                                                                     *
  ! C2SE01 LEAD GAUSS ELIMINATION FOR THE COMPUTATION OF PSI = A**(-1)*B*
  !                                                                     *
  !**********************************************************************
  !
  USE globals
  IMPLICIT NONE
  !
  INTEGER          ::     J3
  INTEGER          ::     I
  INTEGER          ::     J2
  REAL(RKIND)      ::     ZC
  REAL(RKIND)      ::     ZB
  INTEGER          ::     J1
  DIMENSION &
       &   ZB(3),  ZC(N4NT,3)
  !
  !---*----*----*----*----*----*----*----*----*----*----*----*----*----*
  !
  !**********************************************************************
  !                                                                     *
  ! 1. GAUSS ELIMINATION : SOLVE A * X = B                              *
  !                                                                     *
  !**********************************************************************
  !
  CALL DIRECT
  CALL DCOPY(N4NSNT,B,1,CPSILI,1)
  !
  !**********************************************************************
  !                                                                     *
  ! 2. IDENTIFICATIONS AT MESH CENTER                                   *
  !                                                                     *
  !**********************************************************************
  !
  DO J1=1,3
     !
     ZB(J1)       = B(4*NT-3+J1)
     B(4*NT-3+J1) = 0._RKIND
     !
  END DO
  !
  CALL CENTER(ZC)
  !
  DO J2=1,NT
     !
     I = 4 * (NUPDWN(J2) - 1)
     !
     B(I+1) = ZB(1)
     B(I+2) = ZC(I+2,2) * ZB(2) + ZC(I+2,3) * ZB(3)
     B(I+4) = ZC(I+4,2) * ZB(2) + ZC(I+4,3) * ZB(3)
     !
  END DO
  !
  !**********************************************************************
  !                                                                     *
  ! 3. NEW PSI - SOLUTION                                               *
  !                                                                     *
  !**********************************************************************
  !
  CALL DCOPY(N4NSNT,B,1,CPSI,1)
  !
  !**********************************************************************
  !                                                                     *
  ! 4. NEW PSI - SOLUTION IN INVERSE CLOCKWISE NUMEROTATION             *
  !                                                                     *
  !**********************************************************************
  !
  DO J3=1,NSTMAX
     !
     CPSICL(4*(J3-1)+1) = B(4*(NUPDWN(J3)-1)+1)
     CPSICL(4*(J3-1)+2) = B(4*(NUPDWN(J3)-1)+2)
     CPSICL(4*(J3-1)+3) = B(4*(NUPDWN(J3)-1)+3)
     CPSICL(4*(J3-1)+4) = B(4*(NUPDWN(J3)-1)+4)
     !
  END DO
  !
  RETURN
END SUBROUTINE SOLVIT
