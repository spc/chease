! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
SUBROUTINE gyro_profiles
  !   ######################
  !
  !                                        AUTHORS:
  !                                        X. LAPILLONNE,  CRPP-EPFL
  !
  ! Calculate profiles which haven't been calculated in chipsimetric
  ! - d2qdpsi2(psiiso) in array eqchease_out_add_1d(:,4)
  ! - VSURF( psiiso ) in array eqchease_out_add_1d(:,5)
  ! - PHI( psiiso ) in array eqchease_out_add_1d(:,6)
  ! - shear, sh in array eqchease_out_add_1d(:,7)
  ! - dshdpsi in array eqchease_out_add_1d(:,8)
  ! - kappa : eqchease_out_add_1d(:,9)
  ! - delta : eqchease_out_add_1d(:,10)
  ! - dVdpsi

  USE globals
  IMPLICIT NONE


END SUBROUTINE gyro_profiles
