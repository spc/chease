! Copyright 2024 SPC-EPFL
!
! Licensed under the Apache License, Version 2.0 (the "License");
! you may not use this file except in compliance with the License.
! You may obtain a copy of the License at
!
!     http://www.apache.org/licenses/LICENSE-2.0
!
! Unless required by applicable law or agreed to in writing, software
! distributed under the License is distributed on an "AS IS" BASIS,
! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
! See the License for the specific language governing permissions and
! limitations under the License.
!----------------------------------------------------------------------
!
SUBROUTINE ISOFUN(KN)
  !        ##########
  !                                        AUTHORS:
  !                                        H. LUTJENS,  CRPP-EPFL
  !                                        A. BONDESON, CRPP-EPFL
  !**********************************************************************
  !                                                                     *
  !  C2SJ02  COMPUTES T, T-TPRIME, PRESSURE AND P-PRIME PROFILES AT     *
  !          CONSTANT FLUX SURFACES                                     *
  !                                                                     *
  !**********************************************************************
  USE globals
  USE interpos_module
  IMPLICIT NONE
  !
  INTEGER          ::     J301
  INTEGER          ::     J9
  INTEGER          ::     J8P1
  INTEGER          ::     J8
  INTEGER          ::     J7P1
  INTEGER          ::     J7
  INTEGER          ::     J6
  INTEGER          ::     J5
  INTEGER          ::     J4
  INTEGER          ::     J3
  REAL(RKIND)      ::     ZDTEDP
  REAL(RKIND)      ::     ZDNEDP
  INTEGER          ::     J
  INTEGER          ::     I
  REAL(RKIND)      ::     ZFAC
  INTEGER          ::     J2P1
  INTEGER          ::     J2
  REAL(RKIND)      ::     ZCPR
  INTEGER          ::     J101
  INTEGER          ::     J100
  REAL(RKIND)      ::     ZTEMP
  REAL(RKIND)      ::     ZWORK
  REAL(RKIND)      ::     ZD2PPR
  INTEGER          ::     IN
  REAL(RKIND)      ::     ZPISO
  REAL(RKIND)      ::     ZS
  INTEGER          ::     J1, NPPF1
  REAL(RKIND)      ::     ZCPPR
  INTEGER          ::     KN
  !
  ! REAL(RKIND)      ::     X1(NISO1EFF)
  REAL(RKIND)      ::     X2(NISO1EFF), zrhotornorm(NISO1EFF)
  REAL(RKIND)      ::     Qtarget(NISO1EFF), dX2dpsi(NISO1EFF)
  INTEGER, SAVE    ::     ncount = 0
  REAL(RKIND)      ::     alpha, sigma_psi(NISO1EFF+1)
  !
  DIMENSION &
    &   ZDNEDP(KN+1), ZDTEDP(KN+1), &
    &   ZCPR(KN+1),   ZCPPR(KN+1), &
    &   ZD2PPR(KN+1), ZPISO(KN+1), &
    &   ZS(KN+1),     ZWORK(KN+1), &
    &   ztemp(KN+1)
  !
  !----*----*----*---*----*----*----*----*----*----*----*----*----*----*-
  !
  IF (NSURF .EQ. 1) GOTO 300
  !
  IF (NPROFZ .EQ. 0) THEN
    !
    CALL PPRIME(KN,PSIISO,ZCPPR)
    !
    !**********************************************************************
    !                                                                     *
    !  INTEGRATE P'   BY CUBIC SPLINE QUADRATURE (SEE FORSYTHE G.E.,      *
    !  MALCOLM M.A., MOLER C.B., COMPUTER METHODS FOR MATHEMATICAL        *
    !  COMPUTATIONS (ENGLEWOOD CLIFFS, N.J., PRENTICE HALL), $5.2,        *
    !  P. 89, (1977)                                                      *
    !                                                                     *
    !**********************************************************************
    !
    DO J1=1,KN
      !
      ZS(J1)    = SQRT(1 - PSIISO(J1) / SPSIM)
      ZPISO(J1) = PSIISO(J1)
      !
    END DO
    !
    IN = KN
    !
    IF (ZS(IN) .NE. 1._RKIND) THEN
      !
      IN        = KN + 1
      ZS(IN)    = 1._RKIND
      ZPISO(IN) = 0._RKIND
      !
      CALL PPRIME(1,ZPISO(IN),ZCPPR(IN))
      !
    ENDIF
    !
    !%OS            CALL SPLINE(IN,SMISO,ZCPPR,ZD2PPR,ZWORK)
    CALL SPLINE(IN,ZPISO,ZCPPR,ZD2PPR,ZWORK)
!!$    ztemp(1) = zd2ppr(1)
!!$    ztemp(in) = zd2ppr(in)
    ztemp(1:in) = zd2ppr(1:in)
    IF (NVERBOSE .GE. 3) THEN
      do j100 = 2,in-1
        if (abs(zd2ppr(j100)).lt.1.e3_RKIND) goto 100
        if (((zd2ppr(j100-1) .ge. 0._RKIND) .and. (zd2ppr(j100) .ge. 0._RKIND)) &
          &             .and. (zd2ppr(j100+1) .ge. 0._RKIND)) goto 100
        if (((zd2ppr(j100-1) .le. 0._RKIND) .and. (zd2ppr(j100) .le. 0._RKIND)) &
          &             .and. (zd2ppr(j100+1) .le. 0._RKIND)) goto 100
        write(*,*) ' warning inaccurate integration, i =',j100
100     CONTINUE
      END DO
    END IF
    if (ztemp(in-1)*ztemp(in).le.0._RKIND) ztemp(in) = 0._RKIND
    if (ztemp(1)*ztemp(2).le.0._RKIND) ztemp(1) = 0._RKIND
    zd2ppr(1:in) = ztemp(1:in)
    !
    ZCPR(IN) = 0._rkind
    !
    DO J2=IN-1,1,-1
      !
      J2P1 = J2 + 1
      !
      ZCPR(J2) = ZCPR(J2P1) - (ZPISO(J2P1) - ZPISO(J2)) * &
        &                            (.5_RKIND*(ZCPPR(J2) + ZCPPR(J2P1)) - &
        &                             (ZD2PPR(J2) + ZD2PPR(J2P1)) * &
        &                             (ZPISO(J2P1) - ZPISO(J2))**2/24._RKIND)
      !
    END DO
    !
    !
    !%OS  CHECK IF JUMP IN ZCPR (AS ZD2PPR CAN BE WILD)
    IF (NVERBOSE .GE. 3) THEN
      ZFAC = 3._RKIND
      DO I=11,IN-1
        IF (ABS(ZCPR(I)) .GE. 0.1) THEN
          IF (ABS(ZCPR(I+1)-ZCPR(I)) .GT. ABS( ZFAC * &
            &           0.5_RKIND*(ZCPPR(I)+ZCPPR(I+1))*(ZPISO(I+1)-ZPISO(I)))) THEN
            PRINT *,' JUMP IN PRESSURE AT I= ',I
            PRINT *,'  I     ZPISO     ZCPPR     ZD2PPR     ZCPR'
            WRITE(6,'(I3,1P4E14.5)') &
              &            (J,ZPISO(J),ZCPPR(J),ZD2PPR(J),ZCPR(J),J=1,IN)
          END IF
        END IF
      END DO
    END IF
    !%OS
    !
    CALL DCOPY(KN,ZCPR,1,CPR,1)
    CALL DCOPY(KN,ZCPPR,1,CPPR,1)
    !%OS            CALL DCOPY(KN,ZD2PPR,1,D2CPPR,1)
    CALL SPLINE(IN,SMISO,ZCPPR,D2CPPR,ZWORK)
    CALL SPLINE(KN,SMISO,CPR,D2CPR,ZWORK)
    !
  ELSE IF (NPROFZ .EQ. 1) THEN
    !
    CALL POLYNM(KN,AP,DENSTY,1)
    CALL POLYNM(KN,AT,TEMPER,1)
    CALL POLYNM(KN,AP,ZDNEDP,2)
    CALL POLYNM(KN,AT,ZDTEDP,2)
    CALL DSCAL(KN,SCALNE,DENSTY,1)
    CALL DSCAL(KN,SCALNE,ZDNEDP,1)
    !
    DO J3=1,KN
      !
      CPR(J3)  = DENSTY(J3) * TEMPER(J3)
      CPPR(J3) = ZDNEDP(J3) * TEMPER(J3) + &
        &                 DENSTY(J3) * ZDTEDP(J3)
      !
      IF (CPPR(J3) .GT. 0._RKIND) CPPR(J3) = 0._RKIND
      !
    END DO
    !
    CALL SPLINE(KN,SMISO,CPPR,D2CPPR,ZWORK)
    CALL SPLINE(KN,SMISO,CPR,D2CPR,ZWORK)
    !
  ENDIF
  !
  IF (NSTTP .EQ. 1) THEN
    !
    CALL PRFUNC(KN,PSIISO,TTP)
    !
  ELSE IF ((NSTTP .EQ. 2) .OR. (NSTTP .EQ. 5)) THEN
    !
    IF (NPROFZ .EQ. 0) THEN
      !
      !*******************Compute Istar for the case NSTTP = 5 **************************
      !
      IF (NSTTP .EQ. 5) THEN
        NPPF1 = NPPF + 1
        IF (NFUNRHO.EQ.0 .OR. NRHOMESH.EQ.0) THEN
          CALL INTERPOS(FCSM(1:NPPF1),RFUN(1:NPPF1),NPPF1,NISO1EFF,tension=tensprof,xout=SMISO(1:NISO1EFF),yout=Qtarget(1:NISO1EFF),nbc=(/2, 2/),ybc=(/RFUN(1), RFUN(NPPF1)/))
        ELSE IF (NFUNRHO.EQ.1 .AND. NRHOMESH.EQ.1) THEN
          ! RFUN(FCSM) is q(rhotornorm), hence use CIDRTOR(CSIPRI) to get rhotor(smiso) to get q(rhopol=smiso)
          CALL INTERPOS(CSIPRI(1:KN),CIDRTOR(1:KN),KN,NISO1EFF,tension=0._RKIND,xout=SMISO(1:NISO1EFF),yout=zrhotornorm(1:NISO1EFF))
          CALL INTERPOS(FCSM(1:NPPF1),RFUN(1:NPPF1),NPPF1,NISO1EFF,tension=tensprof,xout=zrhotornorm(1:NISO1EFF),yout=Qtarget(1:NISO1EFF),nbc=(/2, 2/),ybc=(/RFUN(1), RFUN(NPPF1)/))
        ELSE
          write(0,*) 'NFUNRHO= ',NFUNRHO,' AND NRHOMESH= ',NRHOMESH,' NOT YET CODED IN'
          write(eqchease_out(index_out)%codeparam%output_diag(1),*) &
            & 'in isofun: NFUNRHO= ',NFUNRHO,' AND NRHOMESH= ',NRHOMESH,' NOT YET CODED IN'
          eqchease_out(index_out)%codeparam%output_flag = -100
          return
        ENDIF
        !
!!$        rewind 76
!!$        write(76,'(A)') '% i FCSM RFUN'
!!$        write(76,'(I3,1p2e15.6)') (i,FCSM(i),RFUN(i),i=1,NPPF1)
!!$        rewind 77
!!$        write(77,'(A)') '% i SMISO Qtarget'
!!$        write(77,'(I3,1p2e15.6)') (i,SMISO(i),Qtarget(i),i=1,NISO1EFF)
!!$        rewind 78
!!$        write(78,'(A)') '% i CSIPRI CIDRTOR'
!!$        write(78,'(I3,1p2e15.6)') (i,CSIPRI(i),CIDRTOR(i),i=1,KN)
        !
        Do i=1,NISO1EFF
          X2(i) = CIDR(i)*CID3(i)*TMF(i)*CIDQ(i)/Qtarget(i)
        END DO
        if (n0jedge .eq. 1) then
          sigma_psi(1:NISO1EFF1) = 1._rkind + 1000._rkind*exp(-((/SPSIM,PSIISO(1:NISO1EFF)/)/0.05/SPSIM)**2)
          CALL INTERPOS((/SPSIM,PSIISO(1:NISO1EFF)/),(/0._RKIND,X2(1:NISO1EFF)/),NISO1EFF+1,NISO1EFF,tension=-1._rkind,xout=PSIISO(1:NISO1EFF),yout=zpiso(1:NISO1EFF),youtp=dX2dpsi(1:NISO1EFF), &
            & nbc=(/2, 1/),ybc=(/0._RKIND, RC0P/),sigma=sigma_psi(1:NISO1EFF1))
        else
          CALL INTERPOS((/SPSIM,PSIISO(1:NISO1EFF)/),(/0._RKIND,X2(1:NISO1EFF)/),NISO1EFF+1,NISO1EFF,tension=tensprof/10._rkind,xout=PSIISO(1:NISO1EFF),yout=zpiso(1:NISO1EFF),youtp=dX2dpsi(1:NISO1EFF), &
            & nbc=(/2, 2/),ybc=(/0._RKIND, X2(NISO1EFF)/))
        end if

        alpha=max(0.2_rkind,relax/2._rkind)
        ! print *,'ncount, nopt= ',ncount, nopt
        if (KN .NE. NISO1EFF) THEN
          write(0,*) 'KN= ',KN,' AND NISO1EFF= ',NISO1EFF,' do not match (isofun) but are expected to be equal'
          write(eqchease_out(index_out)%codeparam%output_diag(1),*) &
            & 'in isofun: KN= ',KN,' AND NISO1EFF= ',NISO1EFF,' do not match (isofun) but are expected to be equal'
          eqchease_out(index_out)%codeparam%output_flag = -111
          return
        ENDIF
        IF ((ncount .EQ. 0) .and. (abs(istar_target(1)) .LE. 1.E-15_RKIND)) THEN
          DO i=1,NISO1EFF
            istar_target(i) = 2.5*(1-CSIPR(i)*CSIPR(i)*CSIPR(i)*CSIPR(i))
          END DO
        ELSE
          DO i=1,NISO1EFF
            istar_target(i) = (1-alpha)*istar_target(i) + alpha*(CID2(i)/CID0(i))*dX2dpsi(i)/(2*pi*CIDR(i))
          END DO
        END IF
      END IF
      !**********************************************************************************
      !
      CALL PRFUNC(KN,PSIISO,CIPR)
      !
      ncount= ncount + 1
      !
      !
    ELSE IF (NPROFZ .EQ. 1) THEN
      !
      DO J4=1,KN
        !
        CIPR(J4) = TEMPER(J4) * SQRT(TEMPER(J4))
        !
        IF (CIPR(J4) .LT. 0._RKIND) CIPR(J4) = 0._RKIND
        !
      END DO
      !
    ENDIF
    !
    DO J5=1,KN
      !
      TTP(J5) = - CIPR(J5) * CID0(J5) - &
        &                  CPPR(J5) * CID2(J5)
      ZS(J5)  = SQRT(1 - PSIISO(J5) / SPSIM)
      !
    END DO
    !
  ELSE IF (NSTTP .EQ. 3) THEN
    !
    CALL PRFUNC(KN,PSIISO,CIPR)
    !
    DO J6=1,KN
      !
      TTP(J6) = - (CIPR(J6) + CPPR(J6) * CID0(J6)) / &
        &                  (1._RKIND + CID2(J6) / TMF(J6)**2)
      ZS(J6)  = SQRT(1 - PSIISO(J6) / SPSIM)
      !
    END DO
    !
  ELSE IF (NSTTP .EQ. 4) THEN
    !
    CALL PRFUNC(KN,PSIISO,CIPR)
    !
    DO J6=1,KN
      !
      TTP(J6) = - (CIPR(J6)/TMF(J6) + CPPR(J6)) * CID0(J6) / &
        &                  (1._RKIND + CID2(J6) / TMF(J6)**2)
      ZS(J6)  = SQRT(1 - PSIISO(J6) / SPSIM)
      !
    END DO
    !
  ENDIF
  !
  CALL SPLINE(KN,SMISO,CID0,D2CID0,ZWORK)
  CALL SPLINE(KN,SMISO,CID2,D2CID2,ZWORK)
  !CALL SPLINE(KN,SMISO,CID3,D2CID3,ZWORK)
  !
  !**********************************************************************
  !                                                                     *
  !  INTEGRATE T T' BY CUBIC SPLINE QUADRATURE (SEE FORSYTHE G.E.,      *
  !  MALCOLM M.A., MOLER C.B., COMPUTER METHODS FOR MATHEMATICAL        *
  !  COMPUTATIONS (ENGLEWOOD CLIFFS, N.J., PRENTICE HALL), $5.2,        *
  !  P. 89, (1977)                                                      *
  !                                                                     *
  !**********************************************************************
  !
  !%OS         CALL SPLINE(KN,SMISO,TTP,D2TTP,ZWORK)
  CALL SPLINE(KN,PSIISO,TTP,D2TTP,ZWORK)
  !
  IF (NTMF0.EQ.0) THEN
    !
    TMF(KN) = 0.5_RKIND
    !
    DO J7=KN-1,1,-1
      !
      J7P1 = J7 + 1
      !
      TMF(J7) = TMF(J7P1) - (PSIISO(J7P1) - PSIISO(J7)) * &
        &                            (.5_RKIND*(TTP(J7) + TTP(J7P1)) - &
        &                            (D2TTP(J7) + D2TTP(J7P1)) * &
        &                            (PSIISO(J7P1) - PSIISO(J7))**2 / 24._RKIND)
      !
    END DO
    !
  ELSE
    !
    TMF(1) = 0.5_RKIND
    !
    DO J8=1,KN-1
      !
      J8P1 = J8 + 1
      !
      TMF(J8P1) = TMF(J8) + (PSIISO(J8P1) - PSIISO(J8)) * &
        &                            (.5_RKIND*(TTP(J8) + TTP(J8P1)) - &
        &                             (D2TTP(J8) + D2TTP(J8P1)) * &
        &                             (PSIISO(J8P1) - PSIISO(J8))**2 / 24._RKIND)
      !
    END DO
    !
  ENDIF
  !
  DO J9=1,KN
    !
    IF (TMF(J9) .LT. 0) THEN
      PRINT*,'TMF(',J9,')**2 NEGATIVE. READ MESSAGE PRECEEDING', &
        &             'DO LOOP 8 IN SUBROUTINE ISOFUN'
      write(eqchease_out(index_out)%codeparam%output_diag(1),*) &
        & 'in isofun: TMF(',J9,')**2 NEGATIVE. READ MESSAGE PRECEEDING, probably bad input profiles'
      eqchease_out(index_out)%codeparam%output_flag = -131
      return
    ENDIF
    !
    TMF(J9) = SQRT(2._RKIND * TMF(J9))
    !
  END DO
  !
  CALL SPLINE(KN,SMISO,TTP,D2TTP,ZWORK)
  CALL SPLINE(KN,SMISO,TMF,D2TMF,ZWORK)
  !
  RETURN
  !
300 CONTINUE
  !
  !**********************************************************************
  !                                                                     *
  !  PROFILES FOR SOLOVEV CASE                                          *
  !                                                                     *
  !**********************************************************************
  !
  DO J301=1,KN
    !
    CPR(J301)  = PSIISO(J301) * CPP
    CPPR(J301) = CPP
    TMF(J301)  = 1._RKIND
    TTP(J301)  = 0._RKIND
    !
  END DO
  !
  RETURN
END SUBROUTINE ISOFUN
