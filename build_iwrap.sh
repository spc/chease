# Copyright 2024 SPC-EPFL
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#----------------------------------------------------------------------
# Start from clean environment
module purge

# IMAS and FC2K
module load IMAS iWrap FC2K

# Actor folder
export ACTOR_FOLDER=~/public/PYTHON_ACTORS
mkdir -p $ACTOR_FOLDER

# Intel as default compiler
export FCOMPILER=ifort

# Libraries needed for the compilation of the H&CD codes themselves
# should avoid specific modules, defaults should follow and be consistent with generic IMAS, otherwise mke an issue
module load INTERPOS # expects /9.1.0-intel-2020b when tested
module load XMLlib # expects /3.3.1-intel-2020b

# EXTEND PYTHON PATH AND AVOID DOUBLONS
export PYTHONPATH=$ACTOR_FOLDER:$PYTHONPATH        # FOR IWRAP
export PYTHONPATH="$(perl -e 'print join(":", grep { not $seen{$_}++ } split(/:/, $ENV{PYTHONPATH}))')"

cd src-f90
#make clean
make libchease_module_imas
cd ..

echo " "
echo "Can execute: iwrap -f iwrap/chease.yaml -i ~/public/PYTHON_ACTORS"
echo " "
echo "to test then:"
echo "    export PYTHONPATH=$ACTOR_FOLDER:\$PYTHONPATH # already done"
echo "    mkdir -p /tmp/$USER; cd /tmp/$USER; python $PWD/python/run_chease_iwrap.py > /tmp/$USER/o.run_chease_iwrap.py"
echo " "
