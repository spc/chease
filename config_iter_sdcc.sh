# Copyright 2024 SPC-EPFL
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#----------------------------------------------------------------------
# treats inputs/defaults
# source config_iter_sdcc.sh [ifort/gfortran [NO/YES [0/1]]] (defaults: ifort, NO and 0 (no module loaded))
#
export CHEASE_F90=ifort
export XML_USE_CHOICE=NO
export load_specific_modules=0
if [ $# -ge 1 ]
then
  export CHEASE_F90=$1
  if [ $# -ge 2 ]
  then
    export XML_USE_CHOICE=$2
    if [ $# -ge 3 ]
    then
      export load_specific_modules=$3
    else
      echo "default \$3: load_specific_modules=$load_specific_modules"
    fi
  else
    echo "default \$2: XML_USE_CHOICE=$XML_USE_CHOICE; \$3: load_specific_modules=$load_specific_modules"
  fi
else
  echo "default \$1: CHEASE_F90=$CHEASE_F90 ; \$2: XML_USE_CHOICE=$XML_USE_CHOICE ; \$3: load_specific_modules=$load_specific_modules"
fi
export FC=$CHEASE_F90

export on_gateway=`test -d /cineca ; echo $?`

# Start from clean environment if 3rd input is 1
if [ $load_specific_modules = '1' ]
then
  module purge

  # load standard modules, add module switch if need specific ones per compiler type
  if [ $on_gateway = '0' ]
  then
    module load cineca
    module load imasenv/3.40.0-test/intel/rc iwrap
    echo "executed purge then: module load cineca; module load imasenv/3.40.0-test/intel/rc iwrap"
  else
    module load IMAS iWrap XMLlib INTERPOS TotalView
    echo "executed purge then: module load IMAS iWrap XMLlib INTERPOS TotalView"
  fi
else
  echo "no modules loaded"
fi

if [ $CHEASE_F90 = 'ifort' ]
then
  echo "" # need a statement in then
  # ifort modules
  # ...
else
  if [ $CHEASE_F90 = 'gfortran' ]
  then
    echo ""
    if [ $on_gateway = '0' ]
    then
      echo 'on gateway: ONLY ifort works so far'
      break
    fi
    # gfortran modules:
    # ...
  else
    echo 'compiler ($1='$CHEASE_F90') not yet expected, choose ifort or gfortran'
  fi
fi

# Actor folder
export ACTOR_FOLDER=~/public/PYTHON_ACTORS
mkdir -p $ACTOR_FOLDER

# Need to remove the stack limit to avoid segmentation fault inside codes
ulimit -Ss unlimited

# EXTEND PYTHON PATH AND AVOID DOUBLONS
export PYTHONPATH=$ACTOR_FOLDER:$PYTHONPATH        # FOR IWRAP
export PYTHONPATH="$(perl -e 'print join(":", grep { not $seen{$_}++ } split(/:/, $ENV{PYTHONPATH}))')"

echo "now you can execute for example: build_imas.csh"
