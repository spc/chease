# Contributing to CHEASE

First of all thank you for contributing to the CHEASE code!

## Reporting bugs
If you encounter bugs you can report them using the GitLab [Issue Tracking system](https://gitlab.epfl.ch/spc/chease/-/issues). You can use the `Bug` tag to flag the issue as a potential bug.

## Suggesting Enhancements
You are welcome to discuss possible code enhancements with other users and developers, also by opening a GitLab [Issue](https://gitlab.epfl.ch/spc/chease/-/issues). Use the `feature_request` tag.

## Proposing changes to the code
**As per the license, we kindly ask that you do not fork the repository (e.g. place it on other publically available place like GitHub/GitLab.com). We would like to maintain 1 official repository**

All contributions to CHEASE MUST be Apache 2.0 licensed. A [contributor license agreement (CLA)](`CLA_CHEASE.md`) MUST be signed for any contribution to be accepted

To propose changes you will need at least `developer` access to the code repository. Please contact the code maintainers (see `README.md`) to arrange this. (you can create an epfl guest account at https://guests.epfl.ch and then activate a gitlab account at https://gitlab.epfl.ch/ which will be used to give you developer rights)

The `master` branch is protected and all code changes should pass through GitLab **merge requests**. If you are ready to propose a change, please proceed as follows:

* Clone the repository
* Create a new branch (most likely starting the latest version of `master`, make sure you local clone is up to date)
* Commit your changes to the local repository as you work.
* When you are ready to share the changes, push the branch to the [official repository](https://gitlab.epfl.ch/spc/chease).
* Open a [merge request]https://gitlab.epfl.ch/spc/chease/-/merge_requests for the branch.
* Assign someone to review it (typically the code maintainer listed in `README.md`)

### Testing before pushing
Before you push changes, it is recommended to run the tests locally. But it will be tested on gitlab.epfl.ch as well (see pipelines)
To run the tests do:
```
 <add test information here>
```
