% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

shot = 74304;
time_eqdsk = 0.7134;

ab=gdat(shot,'eqdsk','time',time_eqdsk,'write',0);                        % gets eqdsk for shot, time
[~,~,namelist_struct] = run_chease(2);                                 % gets default namelist for CHEASE
namelist_struct.nideal=9;namelist_struct.ner=2;namelist_struct.negp=0; % sets nideal=9 and straight field line coords

namelist_struct.solpdpol=1;                                            % to get equidistant chi mesh
namelist_struct.cocos_out=3; namelist_struct.signipxp=-1; namelist_struct.signb0xp=+1;

[fname_out,globalsvalues,namelist_struct,namelistfile_eff] = run_chease(namelist_struct,ab.eqdsk,ab.eqdsk.cocos,[],sprintf('_TCV%dt%.4f',shot,time_eqdsk)); % runs CHEASE
% then in fname_out you get the file, for examples:
fname_out{contains(fname_out,'ogyropsi.h5')}                           % displays the path to .h5 file
