function [H]=read_neoart(flnm,flpth)
% reads the CHEASE output file neoart.dat and builds a matlab structure with the information it contains
%	function [H]=read_neoart(flnm,flpth)
% Inputs:
%		flnm	file name
%		flpth file path, if no path is entered, the default path is used
% This routine is not written at all in a general way, therefore it only works for a specific output file structure
%

% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

% default path
if ~exist('flpth')||isempty(flpth)
  flpth='./';
end

% reads file
if unix(['test -e ' flpth flnm])==0
  fid = fopen([flpth flnm], 'r');
else
  error(['The file ' flpth flnm ' does not exist' ])
end
frewind(fid);
sss='';

% scalars
for ii=1:5
  sss=deblank(fgets(fid));
  eval(['H.' lower(sss) '=fscanf(fid,''%f'',1);'])
  fgets(fid);
end

% test if COCOS added
sss=deblank(fgets(fid));
if strcmp(lower(sss),'cocos')
  H.cocos = fscanf(fid,'%f',1);
  fgets(fid);
  sss=deblank(fgets(fid));
end

% grid
eval(['H.' lower(sss) '=fscanf(fid,''%f'',H.npsi);'])
fgets(fid);

% 1_D quantities
for ii=1:25
  sss=deblank(fgets(fid));
  eval(['H.' lower(sss) '=fscanf(fid,''%f'',H.npsi);'])
  fgets(fid);
end

% test if jdotB_over_B0 added
sss=deblank(fgets(fid));
if strcmp(lower(sss),'jdotb_over_b0')
  H.jdotb_over_b0 = fscanf(fid,'%f',H.npsi);
end

fclose(fid);
