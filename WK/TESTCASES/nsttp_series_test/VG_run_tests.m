% Copyright 2024 SPC-EPFL
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% -----------------------------------------------------------------------

% assume in $testdir: /tmp/$USER/nsttp_tests
inputmatscript=which('VG_run_tests');
[inputdir,funname,funext] = fileparts(inputmatscript);

tmpdir = ['/tmp/' getenv('USER') '/nsttp_tests'];
cocosin=11;

try
  eval(['!mkdir -p ' tmpdir])
  eval(['cd ' tmpdir])
  pwd
catch
  disp(['cannot reach ' tmpdir]);
  return
end

if ~exist('runtest')
  runtest=12
end

try

  eqd1=read_eqdsk(fullfile(inputdir,'EQDSK_COCOS_11.OUT_EXPEQtest_1'),cocosin);
  switch runtest
   case 0
    eqd2=read_eqdsk(fullfile(tmpdir,'EQDSK_COCOS_11.OUT'),cocosin);
    eqd3=read_eqdsk(fullfile(tmpdir,'EQDSK_COCOS_11.OUT_fromEQDSK_Q'),cocosin);
   case 1
    eqd2=read_eqdsk(fullfile(tmpdir,'EQDSK_COCOS_11.OUT_fromEQDSK'),cocosin);
   case 2
    eqd2=read_eqdsk(fullfile(tmpdir,'EQDSK_COCOS_11.OUT_fromnsttp2'),cocosin);
   case 12
    eqd2=read_eqdsk(fullfile(tmpdir,'EQDSK_COCOS_11.OUT.TOR_fromnsttp2'),cocosin);
   case 3
    eqd2=read_eqdsk(fullfile(tmpdir,'EQDSK_COCOS_11.OUT_fromnsttp3'),cocosin);
   case 4
    eqd2=read_eqdsk(fullfile(tmpdir,'EQDSK_COCOS_11.OUT_fromnsttp4'),cocosin);
   case 5
    eqd2=read_eqdsk(fullfile(tmpdir,'EQDSK_COCOS_11.OUT_fromnsttp5'),cocosin);
   otherwise
    disp('runtest not ok')
    return
  end
  set(0,'DefaultlineLineWidth',3);
  figure
  set(gcf,'pos',[460    60   720   710]);
  subplot(4,1,1)
  plot(eqd1.rhopsi,eqd1.p)
  hold on
  plot(eqd2.rhopsi,eqd2.p,'--')
  legend('EQDSK_orig',eqd2.fname)
  if exist('eqd3')
    plot(eqd3.rhopsi,eqd3.p,'--')
    legend('EQDSK_orig',eqd2.fname,eqd3.fname)
  end
  xlabel('rhopsi')
  ylabel('p')
  title(['runtest = ' num2str(runtest)])

  subplot(4,1,2)
  plot(eqd1.rhopsi,eqd1.pprime)
  hold on
  plot(eqd2.rhopsi,eqd2.pprime,'--')
  legend('EQDSK_orig',eqd2.fname)
  if exist('eqd3')
    plot(eqd3.rhopsi,eqd3.pprime,'--')
    legend('EQDSK_orig',eqd2.fname,eqd3.fname)
  end
  xlabel('rhopsi')
  ylabel('pprime')

  subplot(4,1,3)
  plot(eqd1.rhopsi,eqd1.FFprime)
  hold on
  plot(eqd2.rhopsi,eqd2.FFprime,'--')
  legend('EQDSK_orig',eqd2.fname)
  if exist('eqd3')
    plot(eqd3.rhopsi,eqd3.FFprime,'--')
    legend('EQDSK_orig',eqd2.fname,eqd3.fname)
  end
  xlabel('rhopsi')
  ylabel('FFprime')

  subplot(4,1,4)
  plot(eqd1.rhopsi,eqd1.q)
  hold on
  plot(eqd2.rhopsi,eqd2.q,'--')
  aaa = interpos(eqd2.rhopsi,eqd2.q,eqd1.rhopsi,-0.01,[2 2],[eqd2.q(1) eqd2.q(end)]);
  zaaa = sum(abs(eqd1.q-aaa).^2) / numel(aaa);
  disp(['diff on q sqrt(sum((q-y)^2)/N)/mean(q): ' num2str(sqrt(zaaa)/mean(eqd1.q),'%.2e')])
  legend('EQDSK_orig','EQDSK_out_fromeqdsk')
  if exist('eqd3')
    plot(eqd3.rhopsi,eqd3.q,'--')
    legend('EQDSK_orig',eqd2.fname,eqd3.fname)
    aaa = interpos(eqd3.rhopsi,eqd3.q,eqd1.rhopsi,-0.01,[2 2],[eqd3.q(1) eqd3.q(end)]);
    zaaa = sum(abs(eqd1.q-aaa).^2) / numel(aaa);
    disp(['diff on q eqd3 sqrt(sum((q-y)^2)/N)/mean(q): ' num2str(sqrt(zaaa)/mean(eqd1.q),'%.2e')])
  end
  xlabel('rhopsi')
  ylabel('q')

  figure
  plot(eqd1.rplas,eqd1.zplas)
  hold on
  plot(eqd2.rplas,eqd2.zplas,'--')
  legend('EQDSK_orig',eqd2.fname)
  if exist('eqd3')
    plot(eqd3.rplas,eqd3.zplas,'--')
    legend('EQDSK_orig',eqd2.fname,eqd3.fname)
  end
  xlabel('R [m]')
  ylabel('Z [m]')

  disp(['eqd1.R0= ' num2str(eqd1.r0)])
  disp(['eqd2.R0= ' num2str(eqd2.r0)])
  disp(['eqd1.syscode_formulas.R0 Rgeom[m]= ' num2str(eqd1.syscode_formulas.R0)])
  disp(['eqd2.syscode_formulas.R0 Rgeom[m]= ' num2str(eqd2.syscode_formulas.R0)])
  disp(['eqd1.Rgeom/r0exp= ' num2str(eqd1.syscode_formulas.R0/eqd1.r0)])
  disp(['eqd2.Rgeom/r0exp= ' num2str(eqd2.syscode_formulas.R0/eqd2.r0)])
  disp(['eqd1.q0= ' num2str(eqd1.q(1))])
  disp(['eqd2.q0= ' num2str(eqd2.q(1))])
  if exist('eqd3')
    disp(['eqd3.q0= ' num2str(eqd3.q(1))])
  end
  if abs(eqd1.q(1)-eqd2.q(1)) > 0.001
    disp('***********************************************************************')
    warning(['WARNING q0 different'])
    disp('***********************************************************************')
  end

catch
end

eval(['cd ' inputdir])
% plotdatafile(['/tmp/sauter/nsttp_tests/o.EXPEQ_nsttp' num2str(runtest) '.cols']);
